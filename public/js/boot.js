$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

$(document).ready(function(){
	$.ajaxSetup({
	  headers: {
	    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').prop('content')
	  }
	});
});	

function removeAlerts(container = 'body', selector = '.alert-ajax') {
	$(container).find(selector).remove();
}

function fadeAlerts(container = 'body', selector = '.alert-ajax') {
	var alerts = $(container).find(selector);
	alerts.delay(10000).fadeOut(function () {
		$(this).remove();
	});
}