$(document).ready(function () {
	removeAlerts();

	$(pageContainer).on('submit', '.form-search', function () {
		$(this).find('label').append('&nbsp;<i class="fa fa-spinner fa-spin"></a>');
	})

	$(pageContainer).on('show.bs.modal', '.modal', function (event) {
		$('.modal').not(this).modal('hide');
		var modal = $(this);
		var elem = $(event.relatedTarget); 
		if (elem.hasClass('disabled')) { return false; }
		modal.find('.modal-body').load(elem.prop('href'), function () {
			var footer = modal.find('.modal-footer');
			var modUrls = modal.find('.modal-body .modalUrls');
			footer.find('.btn').show().removeClass('disabled');
			if (modUrls.data('edit') == '') { footer.find('.btn-edit').addClass('disabled'); }
			if (modUrls.data('restore') == '') { footer.find('.btn-restore').hide(); } 
			if (modUrls.data('destroy') == '') { footer.find('.btn-destroy').hide(); }
			footer.removeClass('hidden-xs-up');
		});
	}).on('hidden.bs.modal', '.modal', function (event) {
		var modal = $(this);
		modal.find('.modal-footer').addClass('hidden-xs-up');
		modal.find('.modal-body').html('<div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x"></i></div>');
	});

	$(pageContainer).on('click', '.btn-edit', function (event) {
		var modalBody = $(this).parents('.modal:first').find('.modal-body');
		$(this).prop("href", modalBody.find('.modalUrls').data('edit'));
	});

	$(pageContainer).on('click', '.btn-search', function (event) {
		var btn = $(this);
		if (btn.data('search') == '') {
			event.preventDefault();
			var searchContainer = $('.searchContainer');
			if (searchContainer.is(':visible')) {
				searchContainer.slideUp(function () {
					$(this).addClass('hidden-xs-up');
					btn.find('span').addClass('hidden-xs-up')
				});
			} else {
				searchContainer.removeClass('hidden-xs-up');
				btn.find('span').removeClass('hidden-xs-up')
				searchContainer.slideDown('slow');	
			}
		}
	});
	
	$(pageContainer).on('click', '.btn-search', function (event) {
		if ($(this).find('span').hasClass('hidden-xs-up')) {
			event.preventDefault();
			var searchContainer = $('.searchContainer');
			searchContainer.removeClass('hidden-xs-up');
			searchContainer.slideDown('slow');	
		}
	});

	$(pageContainer).on('click', '.btn-destroy, .btn-delete, .btn-restore', function (event) {
		event.preventDefault();

		var btn = $(this);
		var delData = { 
			'destroy': {
				'message': 'Are you sure you want to temporarily delete this record?',
				'method': 'DELETE'
			},
			'delete': {
				'message': 'Are you sure you want to permanently delete this record?',
				'method': 'DELETE'
			},
			'restore': {
				'message': 'Are you sure you want to restore this record?',
				'method': 'PUT'
			},
		};

		var action = '';
		if (btn.hasClass('btn-destroy')) {
			action = 'destroy';
		} else if (btn.hasClass('btn-delete')) {
			action = 'delete';
		} else if (btn.hasClass('btn-restore')) {
			action = 'restore';
		} else {
			return false;
		}
				
		if (confirm(delData[action]['message'])) {
			var modalBody = '';
			var formData = { _method: delData[action]['method'] };
			
			if (btn.parents('.modal:first').length > 0) {
				modalBody = btn.parents('.modal:first').find('.modal-body');
				btn.prop("href", modalBody.find('.modalUrls').data(action));
			} 
			
			$.ajax({
				type: 'POST',
				url: btn.prop('href'),
				data: formData,
				dataType: 'json',
				success: function (response) {
					$('.modal').modal('hide');
					$('.loadingScreen').removeClass('hidden-xs-up');
					$(pageContainer).load(ajaxLoadUrl + ' ' + pageContent, function () {
						$('.alertArea').after('<div class="alert alert-ajax alert-success fade show mb-2"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.success + '</div>');
						$('.loadingScreen').addClass('hidden-xs-up');
					});
				}, 
				error: function (response) {
					$('.modal').modal('hide');
					$('.alertArea').after('<div class="alert alert-ajax alert-danger fade show mb-2"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error!</strong> Some error has been occured while deleteing this record, please contact admininistrator.</div>');
				}
			});
		}
	});

	$(pageContainer).on('change', 'select[name=product_category]', function (event) {
		var elem = $(this);
		var form = elem.parents('form:first');
		var subcategorySelect = form.find('select[name=product_subcategory]');
		subcategorySelect.html('');
		var prductCategoryId = elem.val();
		if (prductCategoryId == '') {
			return false;
		}
		if (prductCategoryId == '-1') {
			var otherOption = form.find('.other_option_wrap');
			otherOption.html('<input type="text" class="form-control" name="other_category" value="" autofocus>');
			var otherOption1 = form.find('.other_option_wrap2');
			otherOption1.html('<input type="text" class="form-control" name="other_subcategory" value="" autofocus>');
			return false;
		} else {
			var otherOption = form.find('.other_option_wrap');
			otherOption.html('');
			var otherOption1 = form.find('.other_option_wrap2');
			otherOption1.html('');
		}
		$.ajax({
			type: 'POST',
			url: pageUrl + '/product_subcategory',
			data: {
				product_category: prductCategoryId
			},
			dataType: 'json',
			success: function (response) {
				var option = new Option('', ''); 
				subcategorySelect.append($(option));
				$.each(response, function( key, value ) {
					var option = new Option(value.name, value._id); 
					subcategorySelect.append($(option));
				});
				var option = new Option('Other', '-1'); 
				subcategorySelect.append($(option));
			}, 
			error: function (response) {
				var errors = response.responseJSON;
				$.each(errors, function( key, value ) {
					alert(value);
				});
			}
		});
	});

	$(pageContainer).on('change', 'select[name=product_subcategory]', function (event) {
		var elem = $(this);
		var form = elem.parents('form:first');
		var prductSubcategoryId = elem.val();
		if (prductSubcategoryId == '') {
			return false;
		}
		if(prductSubcategoryId == '-1'){
			var otherOption = form.find('.other_option_wrap2');
			otherOption.html('<input type="text" class="form-control" name="other_subcategory" value="" autofocus>');
			return false;
		}else{
			var otherOption = form.find('.other_option_wrap2');
			otherOption.html('');
		}
		
	});

	$(pageContainer).on('change', 'select[name=shop]', function (event) {
		var elem = $(this);
		var form = elem.parents('form:first');
		var brandSelect = form.find('select[name=brand]');
		brandSelect.html('');
		var shopId = elem.val();
		if (shopId == '') {
			return false;
		}
		$.ajax({
			type: 'POST',
			url: pageUrl + '/shop_brands',
			data: {
				shop_id: shopId
			},
			dataType: 'json',
			success: function (response) {
				var option = new Option('', ''); 
				brandSelect.append($(option));
				$.each(response, function( key, value ) {
					var option = new Option(value.name, value._id); 
					brandSelect.append($(option));
				});
			}, 
			error: function (response) {
				var errors = response.responseJSON;
				$.each(errors, function( key, value ) {
					alert(value);
				});
			}
		});
	});



	$(pageContainer).on("click",".upload-image",function(e){
    
    var options = { 
    complete: function(response) 
    {
      if($.isEmptyObject(response.responseJSON.error)){
      
        $('.modal').modal('hide');
		$('.loadingScreen').removeClass('hidden-xs-up');
		$(pageContainer).load(ajaxLoadUrl + ' ' + pageContent, function () {
			$('.alertArea').after('<div class="alert alert-ajax alert-success fade show mb-2"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.responseJSON.success + '</div>');
			$('.loadingScreen').addClass('hidden-xs-up');
		});
      } else {
       var errors = response.responseJSON.error;
		$('.form-group').removeClass('has-danger');
		$('.form-control-feedback').text('');
		$.each(errors, function( key, value ) {
			var formGroup = $('[name=' + key + ']').parents('.form-group:first');
			formGroup.addClass('has-danger');
			formGroup.find('.form-control-feedback').text(value);
		});
     }
    }
  };
    $(this).parents("form").ajaxForm(options);


 });

$(pageContainer).on("click", "#addAttribute",function(e){
	$("#allAttributes").append('<div class="row">'+
				 '<div class="col-3"><div class="form-group">'+
				 '<input type="text" class="form-control" name="attrName[]" value="" autofocus placeholder="Attribute Name">'+
				'</div></div>'+
				'<div class="col-3"><div class="form-group">'+
				'<input type="text" class="form-control" name="attrVal[]" value="" autofocus placeholder="Attribute Value">'+
				'</div></div>'+
				'</div>');
});	
$(pageContainer).on("click", ".removeAttribute",function(e){
	 $("#allAttributes").children("div[class=row]:last").remove();
});
	
	
	
	$(pageContainer).on("click", "#addVariation", function (e) {
		var productColorData = JSON.parse(productColor.replace(/&quot;/g, '"'));
		var colorList = '';
		$.each(productColorData, function (index, value) {
			colorList += '<option value="' + value.id + '">' + value.name +'</option >';
		});

		var productSizeData = JSON.parse(productSize.replace(/&quot;/g, '"'));
		var sizeList = '';
		$.each(productSizeData, function (index, value) {
			sizeList += '<option value="' + value.id + '">' + value.name + '</option >';
		});
		$("#allVariation").append('<div class="row">'+
				'<div class="col-6">'+
					'<div class="form-group">'+
						'<label class="form-control-label" for="pcolor"><strong>Color</strong></label>'+
						'<select name="pcolor[]" class="form-control">'+
							'<option value="" selected="selected"></option>'+colorList+		    
						'</select>'+
						'<small class="form-control-feedback"></small>'+
					'</div>'+
			    '</div>' +
				'<div class="col-6">'+
					'<div class="form-group">'+
						'<label class="form-control-label" for="psize"><strong>Size</strong></label>'+
						'<select name="psize[]" class="form-control">'+
			                '<option value="" selected="selected"></option>'+sizeList+
						'</select>'+
						'<small class="form-control-feedback"></small>'+
					'</div>'+
				'</div>'+
				'<div class="col-4">'+
					'<div class="form-group">'+
						'<label class="form-control-label" for="pprice"><strong>Price</strong></label>'+
						'<input type="number" min="0" step="any" class="form-control" name="pprice[]" value="0.00">'+
						'<small class="form-control-feedback"></small>'+
					'</div>'+
				'</div>'+
				'<div class="col-4">'+
					'<div class="form-group">'+
						'<label class="form-control-label" for="pdprice"><strong>Discounted Price</strong></label>'+
						'<input type="number" min="0" step="any" class="form-control" name="pdprice[]" value="0.00">'+
						'<small class="form-control-feedback"></small>'+
					'</div>'+
				'</div>'+
				'<div class="col-4">'+
					'<div class="form-group">'+
						'<label class="form-control-label" for="pquantity"><strong>Quantity</strong></label>'+
						'<input type="number" class="form-control" name="pquantity[]" value="0">'+
						'<small class="form-control-feedback"></small>'+
					'</div>'+
				'</div>'+
				'<div class="col-4">'+
					'<div class="form-group">'+
						'<label class="form-control-label" for="images"><strong>Images (Size 990X990 px)</strong></label>'+
						'<input type="file" name="images[]" class="form-control" >' +
						'<input type="hidden" name="imagesurl[]" class="form-control" value="">'+
						'<small class="form-control-feedback"></small>'+
					'</div>'+
				'</div>'+
			'</div>');
	});
	$(pageContainer).on("click", ".removeVariation", function (e) {
		$("#allVariation").children("div[class=row]:last").remove();
	});	

	var hashView = window.location.hash.substr(1);
	if (hashView != '' && $.isNumeric(hashView)) {
		var hashViewLink = $("<a>", {id: "hasViewLink", "class": "hidden-xs-up"});
		hashViewLink.prop('href', pageUrl+'/'+hashView);
		hashViewLink.prop('data-toggle', 'modal');
		hashViewLink.prop('data-target', '#viewModal');
		$(pageContainer).append(hashViewLink);
		$('#hasViewLink').click();
	}
});