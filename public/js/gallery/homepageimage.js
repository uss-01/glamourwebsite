$(document).ready(function () {
	removeAlerts();
    // Date Calender
   
	$(pageContainer).on('submit', '.form-search', function () {
		$(this).find('label').append('&nbsp;<i class="fa fa-spinner fa-spin"></a>');
	})

	$(pageContainer).on('show.bs.modal', '.modal', function (event) {
		$('.modal').not(this).modal('hide');
		var modal = $(this);
		var elem = $(event.relatedTarget); 
		if (elem.hasClass('disabled')) { return false; }
		modal.find('.modal-body').load(elem.prop('href'), function () {
			var footer = modal.find('.modal-footer');
			var modUrls = modal.find('.modal-body .modalUrls');
			footer.find('.btn').show().removeClass('disabled');
			if (modUrls.data('edit') == '') { footer.find('.btn-edit').addClass('disabled'); }
			if (modUrls.data('restore') == '') { footer.find('.btn-restore').hide(); } 
			if (modUrls.data('destroy') == '') { footer.find('.btn-destroy').hide(); }
			footer.removeClass('hidden-xs-up');
		});
	}).on('hidden.bs.modal', '.modal', function (event) {
		var modal = $(this);
		modal.find('.modal-footer').addClass('hidden-xs-up');
		modal.find('.modal-body').html('<div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x"></i></div>');
	});

	$(pageContainer).on('click', '.btn-edit', function (event) {
		var modalBody = $(this).parents('.modal:first').find('.modal-body');
		$(this).prop("href", modalBody.find('.modalUrls').data('edit'));
	});

	$(pageContainer).on('click', '.btn-search', function (event) {
		var btn = $(this);
		if (btn.data('search') == '') {
			event.preventDefault();
			var searchContainer = $('.searchContainer');
			if (searchContainer.is(':visible')) {
				searchContainer.slideUp(function () {
					$(this).addClass('hidden-xs-up');
					btn.find('span').addClass('hidden-xs-up')
				});
			} else {
				searchContainer.removeClass('hidden-xs-up');
				btn.find('span').removeClass('hidden-xs-up')
				searchContainer.slideDown('slow');	
			}
		}
	});
	
	$(pageContainer).on('click', '.btn-search', function (event) {
		if ($(this).find('span').hasClass('hidden-xs-up')) {
			event.preventDefault();
			var searchContainer = $('.searchContainer');
			searchContainer.removeClass('hidden-xs-up');
			searchContainer.slideDown('slow');	
		}
	});

	$(pageContainer).on('click', '.btn-destroy, .btn-delete, .btn-restore', function (event) {
		event.preventDefault();

		var btn = $(this);
		var delData = { 
			'destroy': {
				'message': 'Are you sure you want to temporarily delete this record?',
				'method': 'DELETE'
			},
			'delete': {
				'message': 'Are you sure you want to permanently delete this record?',
				'method': 'DELETE'
			},
			'restore': {
				'message': 'Are you sure you want to restore this record?',
				'method': 'PUT'
			},
		};

		var action = '';
		if (btn.hasClass('btn-destroy')) {
			action = 'destroy';
		} else if (btn.hasClass('btn-delete')) {
			action = 'delete';
		} else if (btn.hasClass('btn-restore')) {
			action = 'restore';
		} else {
			return false;
		}
				
		if (confirm(delData[action]['message'])) {
			var modalBody = '';
			var formData = { _method: delData[action]['method'] };
			
			if (btn.parents('.modal:first').length > 0) {
				modalBody = btn.parents('.modal:first').find('.modal-body');
				btn.prop("href", modalBody.find('.modalUrls').data(action));
			} 
			
			$.ajax({
				type: 'POST',
				url: btn.prop('href'),
				data: formData,
				dataType: 'json',
				success: function (response) {
					$('.modal').modal('hide');
					$('.loadingScreen').removeClass('hidden-xs-up');
					$(pageContainer).load(ajaxLoadUrl + ' ' + pageContent, function () {
						$('.alertArea').after('<div class="alert alert-ajax alert-success fade show mb-2"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.success + '</div>');
						$('.loadingScreen').addClass('hidden-xs-up');
					});
				}, 
				error: function (response) {
					$('.modal').modal('hide');
					$('.alertArea').after('<div class="alert alert-ajax alert-danger fade show mb-2"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error!</strong> Some error has been occured while deleteing this record, please contact admininistrator.</div>');
				}
			});
		}
	});

	$(pageContainer).on('click', '.modal-footer .btn-submit', function (event) {
		removeAlerts();
		var btn = $(this);
		var parent = btn.parents('.modal-content:first');
		var modalBody = parent.find('.modal-body');
		var form = modalBody.find('form:first');
		$.ajax({
			type: 'POST',
			url: form.prop('action'),
			data: form.serializeArray(),
			dataType: 'json',
			success: function (response) {
				$('.modal').modal('hide');
				$('.loadingScreen').removeClass('hidden-xs-up');
				$(pageContainer).load(ajaxLoadUrl + ' ' + pageContent, function () {
					$('.alertArea').after('<div class="alert alert-ajax alert-success fade show mb-2"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.success + '</div>');
					$('.loadingScreen').addClass('hidden-xs-up');
				});
			}, 
			error: function (response) {
				var errors = response.responseJSON;
				$('.form-group').removeClass('has-danger');
				$('.form-control-feedback').text('');
				$.each(errors, function( key, value ) {
					var formGroup = $('[name=' + key + ']').parents('.form-group:first');
					formGroup.addClass('has-danger');
					formGroup.find('.form-control-feedback').text(value);
				});
			}
		});


	});
	// for image Upload ---------------

$(pageContainer).on("click",".upload-image",function(e){
   
    var options = { 
    complete: function(response) 
    {
      if($.isEmptyObject(response.responseJSON.error)){
      
        $('.modal').modal('hide');
		$('.loadingScreen').removeClass('hidden-xs-up');
		$(pageContainer).load(ajaxLoadUrl + ' ' + pageContent, function () {
			$('.alertArea').after('<div class="alert alert-ajax alert-success fade show mb-2"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.responseJSON.success + '</div>');
			$('.loadingScreen').addClass('hidden-xs-up');
		});
      } else {
       var errors = response.responseJSON.error;
		$('.form-group').removeClass('has-danger');
		$('.form-control-feedback').text('');
		$.each(errors, function( key, value ) {
			var formGroup = $('[name=' + key + ']').parents('.form-group:first');
			formGroup.addClass('has-danger');
			formGroup.find('.form-control-feedback').text(value);
		});
     }
    }
  };
    $(this).parents("form").ajaxForm(options);


 });

	

	

	var hashView = window.location.hash.substr(1);
	if (hashView != '' && $.isNumeric(hashView)) {
		var hashViewLink = $("<a>", {id: "hasViewLink", "class": "hidden-xs-up"});
		hashViewLink.prop('href', pageUrl+'/'+hashView);
		hashViewLink.prop('data-toggle', 'modal');
		hashViewLink.prop('data-target', '#viewModal');
		$(pageContainer).append(hashViewLink);
		$('#hasViewLink').click();
	}
});