$(document).ready(function () {
	removeAlerts();

	// Date Calender
	$(pageContainer).on('focus', 'input[type=text][name=lease_year]', function () {
		$(this).datepicker({
			dateFormat: "dd/mm/yy",
			altField: "input[type=hidden][name=lease_year_alt]",
			altFormat: "yy-mm-dd"
		});
	});

	$(pageContainer).on('submit', '.form-search', function () {
		$(this).find('label').append('&nbsp;<i class="fa fa-spinner fa-spin"></a>');
	})

	$(pageContainer).on('show.bs.modal', '.modal', function (event) {
		$('.modal').not(this).modal('hide');
		var modal = $(this);
		var elem = $(event.relatedTarget); 
		if (elem.hasClass('disabled')) { return false; }
		modal.find('.modal-body').load(elem.prop('href'), function () {
			var footer = modal.find('.modal-footer');
			var modUrls = modal.find('.modal-body .modalUrls');
			footer.find('.btn').show().removeClass('disabled');
			if (modUrls.data('edit') == '') { footer.find('.btn-edit').addClass('disabled'); }
			if (modUrls.data('restore') == '') { footer.find('.btn-restore').hide(); } 
			if (modUrls.data('destroy') == '') { footer.find('.btn-destroy').hide(); }
			footer.removeClass('hidden-xs-up');
		});
	}).on('hidden.bs.modal', '.modal', function (event) {
		var modal = $(this);
		modal.find('.modal-footer').addClass('hidden-xs-up');
		modal.find('.modal-body').html('<div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x"></i></div>');
	});

	$(pageContainer).on('click', '.btn-edit', function (event) {
		var modalBody = $(this).parents('.modal:first').find('.modal-body');
		$(this).prop("href", modalBody.find('.modalUrls').data('edit'));
	});

	$(pageContainer).on('click', '.btn-search', function (event) {
		var btn = $(this);
		if (btn.data('search') == '') {
			event.preventDefault();
			var searchContainer = $('.searchContainer');
			if (searchContainer.is(':visible')) {
				searchContainer.slideUp(function () {
					$(this).addClass('hidden-xs-up');
					btn.find('span').addClass('hidden-xs-up')
				});
			} else {
				searchContainer.removeClass('hidden-xs-up');
				btn.find('span').removeClass('hidden-xs-up')
				searchContainer.slideDown('slow');	
			}
		}
	});
	
	$(pageContainer).on('click', '.btn-search', function (event) {
		if ($(this).find('span').hasClass('hidden-xs-up')) {
			event.preventDefault();
			var searchContainer = $('.searchContainer');
			searchContainer.removeClass('hidden-xs-up');
			searchContainer.slideDown('slow');	
		}
	});

	$(pageContainer).on('click', '.btn-destroy, .btn-delete, .btn-restore', function (event) {
		event.preventDefault();

		var btn = $(this);
		var delData = { 
			'destroy': {
				'message': 'Are you sure you want to temporarily delete this record?',
				'method': 'DELETE'
			},
			'delete': {
				'message': 'Are you sure you want to permanently delete this record?',
				'method': 'DELETE'
			},
			'restore': {
				'message': 'Are you sure you want to restore this record?',
				'method': 'PUT'
			},
		};

		var action = '';
		if (btn.hasClass('btn-destroy')) {
			action = 'destroy';
		} else if (btn.hasClass('btn-delete')) {
			action = 'delete';
		} else if (btn.hasClass('btn-restore')) {
			action = 'restore';
		} else {
			return false;
		}
				
		if (confirm(delData[action]['message'])) {
			var modalBody = '';
			var formData = { _method: delData[action]['method'] };
			
			if (btn.parents('.modal:first').length > 0) {
				modalBody = btn.parents('.modal:first').find('.modal-body');
				btn.prop("href", modalBody.find('.modalUrls').data(action));
			} 
			
			$.ajax({
				type: 'POST',
				url: btn.prop('href'),
				data: formData,
				dataType: 'json',
				success: function (response) {
					$('.modal').modal('hide');
					$('.loadingScreen').removeClass('hidden-xs-up');
					$(pageContainer).load(ajaxLoadUrl + ' ' + pageContent, function () {
						$('.alertArea').after('<div class="alert alert-ajax alert-success fade show mb-2"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.success + '</div>');
						$('.loadingScreen').addClass('hidden-xs-up');
					});
				}, 
				error: function (response) {
					$('.modal').modal('hide');
					$('.alertArea').after('<div class="alert alert-ajax alert-danger fade show mb-2"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error!</strong> Some error has been occured while deleteing this record, please contact admininistrator.</div>');
				}
			});
		}
	});

	// Delete Shop Pics
	// $(pageContainer).on('click', '.btn-deletePic', function (event) {
	// 	event.preventDefault();
	// 	var btn = $(this);
	// 	if (btn.parents('.modal:first').length > 0) {
	// 			modalBody = btn.parents('.modal:first').find('.modal-body');
	// 			btn.prop("href", modalBody.find('.modalUrls').data('deletePic'));
	// 		} 
			
	// 		$.ajax({
	// 			type: 'POST',
	// 			url: btn.prop('href'),
	// 			data: formData,
	// 			dataType: 'json',
	// 			success: function (response) {
					
	// 				$(pageContainer).load(ajaxLoadUrl + ' ' + pageContent, function () {
	// 					$(modalBody+' .alertArea').after('<div class="alert alert-ajax alert-success fade show mb-2"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.success + '</div>');
						
	// 				});
	// 			}, 
	// 			error: function (response) {
	// 				$('.modal').modal('hide');
	// 				$(modalBody+' .alertArea').after('<div class="alert alert-ajax alert-danger fade show mb-2"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error!</strong> Some error has been occured while deleteing this record, please contact admininistrator.</div>');
	// 			}
	// 		});


	// });	

	$(pageContainer).on('change', 'input[name=selectall]', function (event) {
		var elem = $(this);
		var parent = elem.parents('div.row:first');
		var formGroup = parent.find('.form-group');
		var chks = formGroup.find('input[type=checkbox]');
		chks.prop('checked', '');
		if (elem.is(':checked')) {
			formGroup.find('input[type=checkbox]').prop('checked', 'checked');
		}
	});

	$(pageContainer).on('click', '.modal-footer .btn-submit', function (event) {
		removeAlerts();
		var btn = $(this);
		var parent = btn.parents('.modal-content:first');
		var modalBody = parent.find('.modal-body');
		var form = modalBody.find('form:first');
		$('.loadingScreen').removeClass('hidden-xs-up');
		$.ajax({
			type: 'POST',
			url: form.prop('action'),
			data: form.serializeArray(),
			dataType: 'json',
			success: function (response) {
				$('.modal').modal('hide');
				$(pageContainer).load(ajaxLoadUrl + ' ' + pageContent, function () {
					$('.alertArea').after('<div class="alert alert-ajax alert-success fade show mb-2"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.success + '</div>');
					$('.loadingScreen').addClass('hidden-xs-up');
				});
			}, 
			error: function (response) {
				var errors = response.responseJSON;
				$('.form-group').removeClass('has-danger');
				$('.form-control-feedback').text('');
				$.each(errors, function( key, value ) {
					var formGroup = $('[name=' + key + ']').parents('.form-group:first');
					if (key == 'brand') {
						formGroup = $('[name=\'' + key + '[]\']').parents('.row:first');
					}
					formGroup.addClass('has-danger');
					formGroup.find('.form-control-feedback').text(value);
				});
				$('.loadingScreen').addClass('hidden-xs-up');
			}
		});
	});

	// for image Upload ---------------

$(pageContainer).on("click",".upload-image",function(e){
    
    var options = { 
    complete: function(response) 
    {
      if($.isEmptyObject(response.responseJSON.error)){
      
        $('.modal').modal('hide');
		$('.loadingScreen').removeClass('hidden-xs-up');
		$(pageContainer).load(ajaxLoadUrl + ' ' + pageContent, function () {
			$('.alertArea').after('<div class="alert alert-ajax alert-success fade show mb-2"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.responseJSON.success + '</div>');
			$('.loadingScreen').addClass('hidden-xs-up');
		});
      } else {
       var errors = response.responseJSON.error;
		$('.form-group').removeClass('has-danger');
		$('.form-control-feedback').text('');
		$.each(errors, function( key, value ) {
			var formGroup = $('[name=' + key + ']').parents('.form-group:first');
			formGroup.addClass('has-danger');
			formGroup.find('.form-control-feedback').text(value);
		});
     }
    }
  };
    $(this).parents("form").ajaxForm(options);


 });

$(pageContainer).on('change', 'select[name=store_category]', function (event) {
		var elem = $(this);
		var form = elem.parents('form:first');
		var subcategorySelect = form.find('select[name=store_subcategory]');
		subcategorySelect.html('');
		var prductCategoryId = elem.val();
		if (prductCategoryId == '') {
			return false;
		}
		if(prductCategoryId == '-1'){ // For Other Option
			var otherOption = form.find('.other_option_wrap');
			otherOption.html('<input type="text" class="form-control" name="other_category" value="" autofocus>');
			subcategorySelect.html('<option value="">--Select--</option><option value="-1">Other</option>');
			return false;
		}else{
			var otherOption = form.find('.other_option_wrap');
			otherOption.html('');
		}
		$.ajax({
			type: 'POST',
			url: pageUrl + '/store_subcategory',
			data: {
				product_category: prductCategoryId
			},
			dataType: 'json',
			success: function (response) {
				var option = new Option('', ''); 
				subcategorySelect.append($(option));
				$.each(response, function( key, value ) {
					var option = new Option(value.name, value._id); 
					subcategorySelect.append($(option));
				});
				var option = new Option('other', '-1'); 
				subcategorySelect.append($(option));
			}, 
			error: function (response) {
				var errors = response.responseJSON;
				$.each(errors, function( key, value ) {
					alert(value);
				});
			}
		});
	});
    
    $(pageContainer).on('change', 'select[name=store_subcategory]', function (event) {
		var elem = $(this);
		var form = elem.parents('form:first');
		var prductSubcategoryId = elem.val();
		if (prductSubcategoryId == '') {
			return false;
		}
		if(prductSubcategoryId == '-1'){
			var otherOption = form.find('.other_option_wrap2');
			otherOption.html('<input type="text" class="form-control" name="other_subcategory" value="" autofocus>');
			return false;
		}else{
			var otherOption = form.find('.other_option_wrap2');
			otherOption.html('');
		}
		
	});

	var hashView = window.location.hash.substr(1);
	if (hashView != '' && $.isNumeric(hashView)) {
		var hashViewLink = $("<a>", {id: "hasViewLink", "class": "hidden-xs-up"});
		hashViewLink.prop('href', pageUrl+'/'+hashView);
		hashViewLink.prop('data-toggle', 'modal');
		hashViewLink.prop('data-target', '#viewModal');
		$(pageContainer).append(hashViewLink);
		$('#hasViewLink').click();
	}
});