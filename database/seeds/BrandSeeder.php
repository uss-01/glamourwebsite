<?php

use Illuminate\Database\Seeder;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $brand = [
            ['name' => 'Levis'],
            ['name' => 'Puma'],
            ['name' => 'Addidas'],
                   
        ];
        
        foreach ($brand as $brand) {
            DB::table('master_brand')->insert([
                'name' => $brand['name'],
                
            ]);
        }
    }
}
