<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $category = [
            ['name' => 'Entertainment'],
            ['name' => 'Dinning'],
            ['name' => 'Health'],
                   
        ];
        
        foreach ($category as $category) {
            DB::table('master_category')->insert([
                'name' => $category['name'],
                
            ]);
        }
    }
}
