<?php

use Illuminate\Database\Seeder;

class StoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $store = [
            ['name' => '101'],
            ['name' => '102'],
            ['name' => '103'],
                   
        ];
        
        foreach ($store as $store) {
            DB::table('master_store')->insert([
                'name' => $store['name'],
                
            ]);
        }
    }
}
