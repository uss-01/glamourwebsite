<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		
        $users = [
            ['name' => 'Prashant', 'email' => 'prashantvictory@gmail.com', 'password' => 'pass@123' ],
            ['name' => 'Nishant', 'email' => 'nishant@gmail.com', 'password' => 'pass@123'],
            ['name' => 'Vishal', 'email' => 'vishal@gmail.com', 'password' => 'pass@123'],
            ['name' => 'Shahid', 'email' => 'shahid@gmail.com', 'password' => 'pass@123'],
            ['name' => 'Sanjay', 'email' => 'sanjay@gmail.com', 'password' => 'pass@123'],            
        ];
        
        foreach ($users as $user) {
            DB::table('users')->insert([
                'name' => $user['name'],
                'email' => $user['email'],
                'password' => bcrypt($user['password'])
            ]);
        }
    }
}
