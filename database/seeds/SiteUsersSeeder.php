<?php

use Illuminate\Database\Seeder;

class SiteUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['fbid' => 'aaaa', 'token' => 'FDFG34567','fname' => 'Adam', 'lname' => 'Paul','email' =>'adam@rah.com', 'phone' => '98899','gender' => 'm'],
            ['fbid' => 'bbbb','token' => 'FDFG34567', 'fname' => 'Gilbert', 'lname' => 'Lane','email' =>'gilbert@gmail.com', 'phone' => '454454','gender' => 'm' ],
            ['fbid' => 'cccc','token' => 'FDFG34567', 'fname' => 'Mike', 'lname' => 'Pinock','email' =>'mike@gmail.com', 'phone' => '23454454','gender' => 'm' ],
            ['fbid' => 'dddd','token' => 'FDFG34567', 'fname' => 'Christ', 'lname' => 'Kelly','email' =>'christ@gmail.com', 'phone' => '23454454','gender' => 'm' ],
                       
        ];
        
        foreach ($users as $user) {
            DB::table('site_users')->insert([
                'fbid' => $user['fbid'],
                'token' => $user['token'],
                'fname' => $user['fname'],
                'lname' => $user['lname'],
                'email' => $user['email'],
                'phone' => $user['phone'],
                'gender' => $user['gender']
            ]);
        }
    }
}
