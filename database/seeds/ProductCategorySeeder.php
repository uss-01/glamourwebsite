<?php

use Illuminate\Database\Seeder;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $pcat = [
            ['name' => 'Clothes'],
            ['name' => 'Grocery'],
            ['name' => 'Shoes'],
                   
        ];
        
        foreach ($pcat as $pcat) {
            DB::table('product_categories')->insert([
                'name' => $pcat['name'],
                
            ]);
        }
    }
}
