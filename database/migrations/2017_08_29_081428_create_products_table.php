<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('_id');
            $table->string('name',150);
            $table->string('details',255)->nullable();
            $table->string('image')->nullable();
            $table->unsignedInteger('shop_id')->default(0);
            $table->unsignedInteger('category_id')->default(0);
            $table->unsignedInteger('product_category_id')->default(0);
            $table->unsignedInteger('product_subcategory_id')->default(0);
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
