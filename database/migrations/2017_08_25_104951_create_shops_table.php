<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('_id');
            $table->unsignedInteger('category_id')->default(0);
            $table->unsignedInteger('store_id')->default(0);
            $table->string('name',150);
            $table->string('phone',100);
            $table->string('email',150);
            $table->string('website',150)->nullable();
            $table->string('address',150)->nullable();
            $table->string('details',255)->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
