<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovieDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('movie_detail', function (Blueprint $table) {
            $table->increments('_id');
            $table->unsignedInteger('shop_id');
            $table->string('subtitle',150);
            $table->string('duration',100);
            $table->text('description');
            $table->string('2d_showtime',150)->nullable();
            $table->string('3d_showtime',150)->nullable();
            $table->text('cast',150)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movie_detail');
    }
}
