<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_users', function (Blueprint $table) {
            $table->increments('_id');
            $table->string('fbid', 255);
            $table->string('fname', 255);
            $table->string('lname', 255);
            $table->string('email', 255);
            $table->string('country', 150)->nullable();
            $table->string('phone',50)->nullable();
            $table->timestamp('dob')->nullable();
            $table->enum('gender', ['m', 'f'])->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
			$table->text('about')->nullable();	
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_users');
    }
}
