<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsForSiteUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('site_users', function($table) {			
			$table->boolean('notify_offers')->default(0)->after('gender');
			$table->boolean('notify_push')->default(0)->after('gender');
			$table->text('current_work')->nullable()->after('gender');			
            $table->string('instagram',255)->nullable()->after('gender');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
