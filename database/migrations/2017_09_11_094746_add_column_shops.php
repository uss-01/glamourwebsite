<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnShops extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('shops', function($table) {          
            $table->unsignedInteger('store_category_id')->nullable()->after('store_id');
            $table->unsignedInteger('store_subcategory_id')->nullable()->after('store_category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shops', function($table) {
            $table->dropColumn('store_category_id');
            $table->dropColumn('store_subcategory_id');
        });
    }
}
