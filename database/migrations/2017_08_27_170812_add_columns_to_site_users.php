<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToSiteUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('site_users', function($table) {
            $table->unsignedInteger('notify_push')->default(0)->after('gender');
            $table->unsignedInteger('notify_offers')->default(0)->after('gender');
            $table->string('current_work', 255)->nullable()->after('gender');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('site_users', function($table) {
            $table->dropColumn('notify_push');
            $table->dropColumn('notify_offers');        
            $table->dropColumn('current_work');        
        });
    }
}
