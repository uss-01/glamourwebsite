<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnImagesIncident extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('incidents', function($table) {          
           $table->renameColumn('image','image1');
           $table->string('image2', 255)->nullable()->after('message');
           $table->string('image3', 255)->nullable()->after('message');
        });
         Schema::table('lost_found_items', function($table) {          
           $table->renameColumn('image','image1');
           $table->string('image2', 255)->nullable()->after('message');
           $table->string('image3', 255)->nullable()->after('message');
        });
    }
  

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incidents', function($table) {
            $table->dropColumn('image2');
            $table->dropColumn('image3');
            $table->renameColumn('image1','image');
           
        });
        Schema::table('lost_found_items', function($table) {
            $table->dropColumn('image2');
            $table->dropColumn('image3');
            $table->renameColumn('image1','image');
           
        });
    }
}
