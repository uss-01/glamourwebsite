<?php echo $__env->make('frontend.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<style type="text/css">
    .has-error {
        border: 1px solid red !important;
    }
    #fnameError,
    #lnameError,
    #usernameError,
    #emailError,
    #currentpasswordError,
    #newpasswordError,
    #confirmpasswordError {
        color: red;
    }
    #responseMessage{
        margin-top: 8px;
    }
</style>
<main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">my-account</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->
        <!-- my account wrapper start -->
        <?php if(!empty($bladeVar['userDetails'])): ?>
        <div class="my-account-wrapper section-padding">
            <div class="container">
                <div class="section-bg-color">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- My Account Page Start -->
                            <div class="myaccount-page-wrapper">
                                <!-- My Account Tab Menu Start -->
                                <div class="row">
                                    <div class="col-lg-3 col-md-4">
                                        <div class="myaccount-tab-menu nav" role="tablist">
                                            <a href="#dashboad" class="active" data-toggle="tab"><i class="fa fa-dashboard"></i>
                                                Dashboard</a>
                                            <a href="#orders" data-toggle="tab"><i class="fa fa-cart-arrow-down"></i>
                                                Orders</a>
                                            <a href="#account-info" data-toggle="tab"><i class="fa fa-user"></i> Account
                                                Details</a>    
                                            <a href="#account-password" data-toggle="tab"><i class="fa fa-lock"></i>
                                                Password Change</a>
                                            <a href="#address-edit" data-toggle="tab"><i class="fa fa-map-marker"></i>
                                                Address</a>
                                            <a href="#" class="logout"><i class="fa fa-sign-out"></i> Logout</a>
                                        </div>
                                    </div>
                                    <!-- My Account Tab Menu End -->
                                    <!-- My Account Tab Content Start -->
                                    <div class="col-lg-9 col-md-8">
                                        <div class="tab-content" id="myaccountContent">
                                            <!-- Single Tab Content Start -->
                                            <div class="tab-pane fade show active" id="dashboad" role="tabpanel">
                                                <div class="myaccount-content">
                                                    <h5>Dashboard</h5>
                                                    <div class="welcome">
                                                        <p>Hello, <strong><?php echo e($bladeVar['userDetails']->username); ?></strong> (If Not <strong><?php echo e($bladeVar['userDetails']->username); ?> !</strong><a href="#" class="logout"> Logout</a>)</p>
                                                    </div>
                                                    <p class="mb-0">From your account dashboard. you can easily check &
                                                        view your recent orders, manage your shipping and billing addresses
                                                        and edit your password and account details.</p>
                                                </div>
                                            </div>
                                            <!-- Single Tab Content End -->
                                            <!-- Single Tab Content Start -->
                                            <?php if(!empty($bladeVar['orders'])){ ?>
                                            <div class="tab-pane fade" id="orders" role="tabpanel">
                                                <div class="myaccount-content">
                                                    <h5>Orders</h5>
                                                    <div class="myaccount-table table-responsive text-center">
                                                        <table class="table table-bordered">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>Order</th>
                                                                    <th>Name</th>
                                                                    <th>Date</th>
                                                                    <th>Payment Method</th>
                                                                    <th>Total</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach($bladeVar['orders'] as $order){ ?>
                                                                <tr>
                                                                    <td><?php echo $order['id']; ?></td>
                                                                    <td><?php echo $order['firstname'].' '.$order['lastname']; ?></td>
                                                                    <td><?php echo $order['orderDate']; ?></td>
                                                                    <td><?php echo $order['payment_method']; ?></td>
                                                                    <td><?php echo '$'.$order['amount']; ?></td>
                                                                    <td><a href="<?php echo e(url('/orderDetail/'.$order['id'])); ?>" class="btn btn-sqr">View</a>
                                                                    </td>
                                                                </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <!-- Single Tab Content End -->
                                            <!-- Single Tab Content Start -->
                                            <div class="tab-pane fade" id="account-info" role="tabpanel">
                                                <div class="myaccount-content">
                                                    <h5>Account Details</h5>
                                                    <p class="login-box-msg" id="responseMessage"></p>
                                                    <div class="account-details-form">
                                                        <form method="post" action="#" id="userProfileForm">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="single-input-item">
                                                                        <label for="fname" class="required">First Name</label>
                                                                        <input type="hidden" id="_token" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                                        <input type="text" id="fname" name="fname" value="<?php echo e($bladeVar['userDetails']->fname); ?>" placeholder="First Name" />
                                                                        <span class="error-text" id="fnameError"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="single-input-item">
                                                                        <label for="lname" class="required">Last Name</label>
                                                                        <input type="text" id="lname" name="lname" value="<?php echo e($bladeVar['userDetails']->lname); ?>" placeholder="Last Name" />
                                                                        <span class="error-text" id="lnameError"></span> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="single-input-item">
                                                                <label for="username" class="required">Display Name</label>
                                                                <input type="text" id="username" name="username" value="<?php echo e($bladeVar['userDetails']->username); ?>" placeholder="Display Name" />
                                                                <span class="error-text" id="usernameError"></span>
                                                            </div>
                                                            <div class="single-input-item">
                                                                <label for="email" class="required">Email Addres</label>
                                                                <input type="email" id="email" name="email" value="<?php echo e($bladeVar['userDetails']->email); ?>" placeholder="Email Address" readonly />
                                                                <span class="error-text" id="emailError"></span>
                                                            </div>
                                                            <div class="single-input-item">
                                                                <button class="btn btn-sqr" id="updateDetails">Save Changes</button>
                                                                <img src="<?php echo e(asset('images/ajax-loader.gif')); ?>" id="ajax-loader" style="display:none; margin-left: 10px; height: 40px;">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div> <!-- Single Tab Content End -->
                                            <!-- Single Tab Content Start -->
                                            <div class="tab-pane fade" id="account-password" role="tabpanel">
                                                <div class="myaccount-content">
                                                    <h5>Password change</h5>
                                                    <p class="login-box-msg" id="responsePMessage"></p>
                                                    <div class="account-details-form">
                                                        <form method="post" action="#" id="userchangePassword">
                                                            <div class="single-input-item">
                                                                <label for="currentpassword" class="required">Current Password</label>
                                                                <input type="hidden" id="_token" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                                <input type="hidden" id="email" name="email" value="<?php echo e($bladeVar['userDetails']->email); ?>">
                                                                <input type="password" id="currentpassword" name="currentpassword" placeholder="Current Password" />
                                                                <span class="error-text" id="currentpasswordError"></span> 
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="single-input-item">
                                                                        <label for="newpassword" class="required">New Password</label>
                                                                        <input type="password" id="newpassword" name="newpassword" placeholder="New Password" />
                                                                        <span class="error-text" id="newpasswordError"></span> 
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="single-input-item">
                                                                        <label for="confirmpassword" class="required">Confirm Password</label>
                                                                        <input type="password" id="confirmpassword" name="confirmpassword" placeholder="Confirm Password" />
                                                                        <span class="error-text" id="confirmpasswordError"></span> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="single-input-item">
                                                                <button class="btn btn-sqr" id="updatepassword">Save Changes</button>
                                                                <img src="<?php echo e(asset('images/ajax-loader.gif')); ?>" id="ajax-loader" style="display:none; margin-left: 10px; height: 40px;">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div> <!-- Single Tab Content End -->
                                            <!-- Single Tab Content Start -->
                                            <div class="tab-pane fade" id="address-edit" role="tabpanel">
                                                <div class="myaccount-content">
                                                    <h5>Billing Address</h5>
                                                    <p class="login-box-msg" id="responseAddressMessage"></p>
                                                    <div class="account-details-form">
                                                        <form method="post" action="#" id="userAddressForm">
                                                            <input type="hidden" id="_token" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                            <div class="single-input-item">
                                                                <label for="country" class="required">Country</label>
                                                                <select name="country nice-select" id="country">
                                                                    <option value="">Select Country</option>
                                                                    <?php if(!empty($bladeVar['userDetails']->country)){ ?><option value="<?php echo e($bladeVar['userDetails']->country); ?>" selected><?php echo e($bladeVar['userDetails']->country); ?></option><?php } ?>
                                                                    <option value="Afganistan">Afghanistan</option>
                                                                    <option value="Albania">Albania</option>
                                                                    <option value="Algeria">Algeria</option>
                                                                    <option value="American Samoa">American Samoa</option>
                                                                    <option value="Andorra">Andorra</option>
                                                                    <option value="Angola">Angola</option>
                                                                    <option value="Anguilla">Anguilla</option>
                                                                    <option value="Antigua & Barbuda">Antigua & Barbuda</option>
                                                                    <option value="Argentina">Argentina</option>
                                                                    <option value="Armenia">Armenia</option>
                                                                    <option value="Aruba">Aruba</option>
                                                                    <option value="Australia">Australia</option>
                                                                    <option value="Austria">Austria</option>
                                                                    <option value="Azerbaijan">Azerbaijan</option>
                                                                    <option value="Bahamas">Bahamas</option>
                                                                    <option value="Bahrain">Bahrain</option>
                                                                    <option value="Bangladesh">Bangladesh</option>
                                                                    <option value="Barbados">Barbados</option>
                                                                    <option value="Belarus">Belarus</option>
                                                                    <option value="Belgium">Belgium</option>
                                                                    <option value="Belize">Belize</option>
                                                                    <option value="Benin">Benin</option>
                                                                    <option value="Bermuda">Bermuda</option>
                                                                    <option value="Bhutan">Bhutan</option>
                                                                    <option value="Bolivia">Bolivia</option>
                                                                    <option value="Bonaire">Bonaire</option>
                                                                    <option value="Bosnia & Herzegovina">Bosnia & Herzegovina</option>
                                                                    <option value="Botswana">Botswana</option>
                                                                    <option value="Brazil">Brazil</option>
                                                                    <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                                                                    <option value="Brunei">Brunei</option>
                                                                    <option value="Bulgaria">Bulgaria</option>
                                                                    <option value="Burkina Faso">Burkina Faso</option>
                                                                    <option value="Burundi">Burundi</option>
                                                                    <option value="Cambodia">Cambodia</option>
                                                                    <option value="Cameroon">Cameroon</option>
                                                                    <option value="Canada">Canada</option>
                                                                    <option value="Canary Islands">Canary Islands</option>
                                                                    <option value="Cape Verde">Cape Verde</option>
                                                                    <option value="Cayman Islands">Cayman Islands</option>
                                                                    <option value="Central African Republic">Central African Republic</option>
                                                                    <option value="Chad">Chad</option>
                                                                    <option value="Channel Islands">Channel Islands</option>
                                                                    <option value="Chile">Chile</option>
                                                                    <option value="China">China</option>
                                                                    <option value="Christmas Island">Christmas Island</option>
                                                                    <option value="Cocos Island">Cocos Island</option>
                                                                    <option value="Colombia">Colombia</option>
                                                                    <option value="Comoros">Comoros</option>
                                                                    <option value="Congo">Congo</option>
                                                                    <option value="Cook Islands">Cook Islands</option>
                                                                    <option value="Costa Rica">Costa Rica</option>
                                                                    <option value="Cote DIvoire">Cote DIvoire</option>
                                                                    <option value="Croatia">Croatia</option>
                                                                    <option value="Cuba">Cuba</option>
                                                                    <option value="Curaco">Curacao</option>
                                                                    <option value="Cyprus">Cyprus</option>
                                                                    <option value="Czech Republic">Czech Republic</option>
                                                                    <option value="Denmark">Denmark</option>
                                                                    <option value="Djibouti">Djibouti</option>
                                                                    <option value="Dominica">Dominica</option>
                                                                    <option value="Dominican Republic">Dominican Republic</option>
                                                                    <option value="East Timor">East Timor</option>
                                                                    <option value="Ecuador">Ecuador</option>
                                                                    <option value="Egypt">Egypt</option>
                                                                    <option value="El Salvador">El Salvador</option>
                                                                    <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                                    <option value="Eritrea">Eritrea</option>
                                                                    <option value="Estonia">Estonia</option>
                                                                    <option value="Ethiopia">Ethiopia</option>
                                                                    <option value="Falkland Islands">Falkland Islands</option>
                                                                    <option value="Faroe Islands">Faroe Islands</option>
                                                                    <option value="Fiji">Fiji</option>
                                                                    <option value="Finland">Finland</option>
                                                                    <option value="France">France</option>
                                                                    <option value="French Guiana">French Guiana</option>
                                                                    <option value="French Polynesia">French Polynesia</option>
                                                                    <option value="French Southern Ter">French Southern Ter</option>
                                                                    <option value="Gabon">Gabon</option>
                                                                    <option value="Gambia">Gambia</option>
                                                                    <option value="Georgia">Georgia</option>
                                                                    <option value="Germany">Germany</option>
                                                                    <option value="Ghana">Ghana</option>
                                                                    <option value="Gibraltar">Gibraltar</option>
                                                                    <option value="Great Britain">Great Britain</option>
                                                                    <option value="Greece">Greece</option>
                                                                    <option value="Greenland">Greenland</option>
                                                                    <option value="Grenada">Grenada</option>
                                                                    <option value="Guadeloupe">Guadeloupe</option>
                                                                    <option value="Guam">Guam</option>
                                                                    <option value="Guatemala">Guatemala</option>
                                                                    <option value="Guinea">Guinea</option>
                                                                    <option value="Guyana">Guyana</option>
                                                                    <option value="Haiti">Haiti</option>
                                                                    <option value="Hawaii">Hawaii</option>
                                                                    <option value="Honduras">Honduras</option>
                                                                    <option value="Hong Kong">Hong Kong</option>
                                                                    <option value="Hungary">Hungary</option>
                                                                    <option value="Iceland">Iceland</option>
                                                                    <option value="Indonesia">Indonesia</option>
                                                                    <option value="India">India</option>
                                                                    <option value="Iran">Iran</option>
                                                                    <option value="Iraq">Iraq</option>
                                                                    <option value="Ireland">Ireland</option>
                                                                    <option value="Isle of Man">Isle of Man</option>
                                                                    <option value="Israel">Israel</option>
                                                                    <option value="Italy">Italy</option>
                                                                    <option value="Jamaica">Jamaica</option>
                                                                    <option value="Japan">Japan</option>
                                                                    <option value="Jordan">Jordan</option>
                                                                    <option value="Kazakhstan">Kazakhstan</option>
                                                                    <option value="Kenya">Kenya</option>
                                                                    <option value="Kiribati">Kiribati</option>
                                                                    <option value="Korea North">Korea North</option>
                                                                    <option value="Korea Sout">Korea South</option>
                                                                    <option value="Kuwait">Kuwait</option>
                                                                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                                    <option value="Laos">Laos</option>
                                                                    <option value="Latvia">Latvia</option>
                                                                    <option value="Lebanon">Lebanon</option>
                                                                    <option value="Lesotho">Lesotho</option>
                                                                    <option value="Liberia">Liberia</option>
                                                                    <option value="Libya">Libya</option>
                                                                    <option value="Liechtenstein">Liechtenstein</option>
                                                                    <option value="Lithuania">Lithuania</option>
                                                                    <option value="Luxembourg">Luxembourg</option>
                                                                    <option value="Macau">Macau</option>
                                                                    <option value="Macedonia">Macedonia</option>
                                                                    <option value="Madagascar">Madagascar</option>
                                                                    <option value="Malaysia">Malaysia</option>
                                                                    <option value="Malawi">Malawi</option>
                                                                    <option value="Maldives">Maldives</option>
                                                                    <option value="Mali">Mali</option>
                                                                    <option value="Malta">Malta</option>
                                                                    <option value="Marshall Islands">Marshall Islands</option>
                                                                    <option value="Martinique">Martinique</option>
                                                                    <option value="Mauritania">Mauritania</option>
                                                                    <option value="Mauritius">Mauritius</option>
                                                                    <option value="Mayotte">Mayotte</option>
                                                                    <option value="Mexico">Mexico</option>
                                                                    <option value="Midway Islands">Midway Islands</option>
                                                                    <option value="Moldova">Moldova</option>
                                                                    <option value="Monaco">Monaco</option>
                                                                    <option value="Mongolia">Mongolia</option>
                                                                    <option value="Montserrat">Montserrat</option>
                                                                    <option value="Morocco">Morocco</option>
                                                                    <option value="Mozambique">Mozambique</option>
                                                                    <option value="Myanmar">Myanmar</option>
                                                                    <option value="Nambia">Nambia</option>
                                                                    <option value="Nauru">Nauru</option>
                                                                    <option value="Nepal">Nepal</option>
                                                                    <option value="Netherland Antilles">Netherland Antilles</option>
                                                                    <option value="Netherlands">Netherlands (Holland, Europe)</option>
                                                                    <option value="Nevis">Nevis</option>
                                                                    <option value="New Caledonia">New Caledonia</option>
                                                                    <option value="New Zealand">New Zealand</option>
                                                                    <option value="Nicaragua">Nicaragua</option>
                                                                    <option value="Niger">Niger</option>
                                                                    <option value="Nigeria">Nigeria</option>
                                                                    <option value="Niue">Niue</option>
                                                                    <option value="Norfolk Island">Norfolk Island</option>
                                                                    <option value="Norway">Norway</option>
                                                                    <option value="Oman">Oman</option>
                                                                    <option value="Pakistan">Pakistan</option>
                                                                    <option value="Palau Island">Palau Island</option>
                                                                    <option value="Palestine">Palestine</option>
                                                                    <option value="Panama">Panama</option>
                                                                    <option value="Papua New Guinea">Papua New Guinea</option>
                                                                    <option value="Paraguay">Paraguay</option>
                                                                    <option value="Peru">Peru</option>
                                                                    <option value="Phillipines">Philippines</option>
                                                                    <option value="Pitcairn Island">Pitcairn Island</option>
                                                                    <option value="Poland">Poland</option>
                                                                    <option value="Portugal">Portugal</option>
                                                                    <option value="Puerto Rico">Puerto Rico</option>
                                                                    <option value="Qatar">Qatar</option>
                                                                    <option value="Republic of Montenegro">Republic of Montenegro</option>
                                                                    <option value="Republic of Serbia">Republic of Serbia</option>
                                                                    <option value="Reunion">Reunion</option>
                                                                    <option value="Romania">Romania</option>
                                                                    <option value="Russia">Russia</option>
                                                                    <option value="Rwanda">Rwanda</option>
                                                                    <option value="St Barthelemy">St Barthelemy</option>
                                                                    <option value="St Eustatius">St Eustatius</option>
                                                                    <option value="St Helena">St Helena</option>
                                                                    <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                                                                    <option value="St Lucia">St Lucia</option>
                                                                    <option value="St Maarten">St Maarten</option>
                                                                    <option value="St Pierre & Miquelon">St Pierre & Miquelon</option>
                                                                    <option value="St Vincent & Grenadines">St Vincent & Grenadines</option>
                                                                    <option value="Saipan">Saipan</option>
                                                                    <option value="Samoa">Samoa</option>
                                                                    <option value="Samoa American">Samoa American</option>
                                                                    <option value="San Marino">San Marino</option>
                                                                    <option value="Sao Tome & Principe">Sao Tome & Principe</option>
                                                                    <option value="Saudi Arabia">Saudi Arabia</option>
                                                                    <option value="Senegal">Senegal</option>
                                                                    <option value="Seychelles">Seychelles</option>
                                                                    <option value="Sierra Leone">Sierra Leone</option>
                                                                    <option value="Singapore">Singapore</option>
                                                                    <option value="Slovakia">Slovakia</option>
                                                                    <option value="Slovenia">Slovenia</option>
                                                                    <option value="Solomon Islands">Solomon Islands</option>
                                                                    <option value="Somalia">Somalia</option>
                                                                    <option value="South Africa">South Africa</option>
                                                                    <option value="Spain">Spain</option>
                                                                    <option value="Sri Lanka">Sri Lanka</option>
                                                                    <option value="Sudan">Sudan</option>
                                                                    <option value="Suriname">Suriname</option>
                                                                    <option value="Swaziland">Swaziland</option>
                                                                    <option value="Sweden">Sweden</option>
                                                                    <option value="Switzerland">Switzerland</option>
                                                                    <option value="Syria">Syria</option>
                                                                    <option value="Tahiti">Tahiti</option>
                                                                    <option value="Taiwan">Taiwan</option>
                                                                    <option value="Tajikistan">Tajikistan</option>
                                                                    <option value="Tanzania">Tanzania</option>
                                                                    <option value="Thailand">Thailand</option>
                                                                    <option value="Togo">Togo</option>
                                                                    <option value="Tokelau">Tokelau</option>
                                                                    <option value="Tonga">Tonga</option>
                                                                    <option value="Trinidad & Tobago">Trinidad & Tobago</option>
                                                                    <option value="Tunisia">Tunisia</option>
                                                                    <option value="Turkey">Turkey</option>
                                                                    <option value="Turkmenistan">Turkmenistan</option>
                                                                    <option value="Turks & Caicos Is">Turks & Caicos Is</option>
                                                                    <option value="Tuvalu">Tuvalu</option>
                                                                    <option value="Uganda">Uganda</option>
                                                                    <option value="United Kingdom">United Kingdom</option>
                                                                    <option value="Ukraine">Ukraine</option>
                                                                    <option value="United Arab Erimates">United Arab Emirates</option>
                                                                    <option value="United States of America">United States of America</option>
                                                                    <option value="Uraguay">Uruguay</option>
                                                                    <option value="Uzbekistan">Uzbekistan</option>
                                                                    <option value="Vanuatu">Vanuatu</option>
                                                                    <option value="Vatican City State">Vatican City State</option>
                                                                    <option value="Venezuela">Venezuela</option>
                                                                    <option value="Vietnam">Vietnam</option>
                                                                    <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                                                                    <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                                                                    <option value="Wake Island">Wake Island</option>
                                                                    <option value="Wallis & Futana Is">Wallis & Futana Is</option>
                                                                    <option value="Yemen">Yemen</option>
                                                                    <option value="Zaire">Zaire</option>
                                                                    <option value="Zambia">Zambia</option>
                                                                    <option value="Zimbabwe">Zimbabwe</option>  
                                                                </select>
                                                                <span class="error-text" id="countryError" style="display: block;"></span>
                                                            </div>
                                                            <div class="single-input-item">
                                                                <label for="streetAddress" class="required mt-20">Street address</label>
                                                                <input type="text" id="streetAddress" name="streetAddress" <?php if(!empty($bladeVar['userDetails']->streetAddress)){ ?>value="<?php echo e($bladeVar['userDetails']->streetAddress); ?>"<?php } ?> placeholder="Street address Line 1" />
                                                                <span class="error-text" id="streetAddressError"></span>
                                                            </div>
                                                            <div class="single-input-item">
                                                                <input type="text" id="streetAddress1" name="streetAddress1" <?php if(!empty($bladeVar['userDetails']->streetAddress1)){ ?>value="<?php echo e($bladeVar['userDetails']->streetAddress1); ?>"<?php } ?> placeholder="Street address Line 2 (Optional)">
                                                            </div>
                                                            <div class="single-input-item">
                                                                <label for="city" class="required">Town / City</label>
                                                                <input type="text" id="city" name="city" <?php if(!empty($bladeVar['userDetails']->city)){ ?>value="<?php echo e($bladeVar['userDetails']->city); ?>"<?php } ?> placeholder="Town / City" />
                                                                <span class="error-text" id="cityError"></span>
                                                            </div>
                                                            <div class="single-input-item">
                                                                <label for="state">State / Divition</label>
                                                                <input type="text" id="state" name="state" <?php if(!empty($bladeVar['userDetails']->state)){ ?>value="<?php echo e($bladeVar['userDetails']->state); ?>"<?php } ?> placeholder="State / Divition" />
                                                            </div>
                                                            <div class="single-input-item">
                                                                <label for="postcode" class="required">Postcode / ZIP</label>
                                                                <input type="text" id="postcode" name="postcode" <?php if(!empty($bladeVar['userDetails']->postcode)){ ?>value="<?php echo e($bladeVar['userDetails']->postcode); ?>"<?php } ?> placeholder="Postcode / ZIP" />
                                                                <span class="error-text" id="postcodeError"></span>  
                                                            </div>
                                                            <div class="single-input-item">
                                                                <button class="btn btn-sqr" id="updateAddress">Save Changes</button>
                                                                <img src="<?php echo e(asset('images/ajax-loader.gif')); ?>" id="ajax-loader" style="display:none; margin-left: 10px; height: 40px;">
                                                            </div>
                                                        </form>
                                                    </div>  
                                                </div>
                                            </div>
                                            <!-- Single Tab Content End -->
                                        </div>
                                    </div> <!-- My Account Tab Content End -->
                                </div>
                            </div> <!-- My Account Page End -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <!-- my account wrapper end -->
</main>
<?php echo $__env->make('frontend.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script type="text/javascript">
$(document).ready(function() {
    var baseUrl = '<?php echo e(url("/")); ?>';
	function isEmail(emailid) {
		var pattern = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
		return pattern.test(emailid);
    }
    
    $('#userProfileForm #fname').focusout(function() {
        var fnameValue = $(this).val();
        if (fnameValue == '') {
            $('#userProfileForm #fname').addClass('has-error');
            $('#userProfileForm #fnameError').show().text('Please enter a first name');
        } else if (fnameValue != '') {
            $('#userProfileForm #fname').removeClass('has-error');
            $('#userProfileForm #fnameError').hide().text('');
        }
    });
    
    $('#userProfileForm #lname').focusout(function() {
        var lnameValue = $(this).val();
        if (lnameValue == '') {
            $('#userProfileForm #lname').addClass('has-error');
            $('#userProfileForm #lnameError').show().text('Please enter a last name');
        } else if (lnameValue != '') {
            $('#userProfileForm #lname').removeClass('has-error');
            $('#userProfileForm #lnameError').hide().text('');
        }
    });

    $('#userProfileForm #username').focusout(function() {
        var usernameValue = $(this).val();
        if (usernameValue == '') {
            $('#userProfileForm #username').addClass('has-error');
            $('#userProfileForm #usernameError').show().text('Please enter a full name');
        } else if (usernameValue != '') {
            $('#userProfileForm #username').removeClass('has-error');
            $('#userProfileForm #usernameError').hide().text('');
        }
    });

    $('#userProfileForm #email').focusout(function() {
        var mailValue = $(this).val();            
        if ((mailValue == '') || (!isEmail($('#userProfileForm #email').val()))) {
            $('#userProfileForm #email').addClass('has-error');
            $('#userProfileForm #emailError').show().text('Please enter a valid email address');
        } else if ((mailValue != '') && (isEmail($('#userProfileForm #email').val()))) {
            $('#userProfileForm #email').removeClass('has-error');
            $('#userProfileForm #emailError').hide().text('');
        }
    });

    $('#updateDetails').on('click', function(event) {
        event.preventDefault();
        $('#responseMessage').html('');
        $('#userProfileForm #fname').trigger("focusout");
        $('#userProfileForm #lname').trigger("focusout");
        $('#userProfileForm #username').trigger("focusout");
        $('#userProfileForm #email').trigger("focusout");
        var usernameValue = $("#userProfileForm #username").val();
        var mailValue = $("#userProfileForm #email").val();
        var fnameValue = $("#userProfileForm #fname").val();
        var lnameValue = $("#userProfileForm #lname").val();
        if ((usernameValue != '') && (mailValue != '') && (isEmail($('#userProfileForm #email').val())) && (fnameValue != '') && (lnameValue != '')) {
            $('#userProfileForm #username').removeClass('has-error');
            $('#userProfileForm #email').removeClass('has-error');
            $('#userProfileForm #fname').removeClass('has-error');
            $('#userProfileForm #lname').removeClass('has-error');
            $('#userProfileForm #usernameError').text('').hide();
            $('#userProfileForm #emailError').text('').hide();
            $('#userProfileForm #fnameError').text('').hide();
            $('#userProfileForm #lnameError').text('').hide();
            updateDetails();
        }
    });

    function updateDetails() {
        $('#responseMessage').html('');
        $('#userProfileForm #ajax-loader').show();
        var token = $('#userProfileForm #_token').val();
        var username = $('#userProfileForm #username').val();
        var email = $('#userProfileForm #email').val();
        var fname = $('#userProfileForm #fname').val();
        var lname = $('#userProfileForm #lname').val(); 
        $.ajax({
            url: baseUrl + "/userDetails",
            type: "POST",
            data: {
                '_token': token,
                'username': username,
                'email': email,
                'fname': fname,
                'lname': lname,
            },
            dataType: "JSON",
            success: function(jsonStr) {
                var res_data = JSON.stringify(jsonStr);
                var response = JSON.parse(res_data);
                var responseData = response['responseData'];
                if ((responseData != null) && (responseData == 'user detail update successfully')) {
                    $('#responseMessage').html('Your account details update successfully');
                    setTimeout(function(){ $('#responseMessage').html('');  }, 3000);
                } else {
                    $('#responseMessage').html(responseData);
                }
                $('#userProfileForm #ajax-loader').hide();
            }
        });
    }
/*-----------------------------------User Address--------------------------------------------------*/
    $('#userAddressForm #streetAddress').focusout(function() {
        var streetAddress = $(this).val();
        if (streetAddress == '') {
            $('#userAddressForm #streetAddress').addClass('has-error');
            $('#userAddressForm #streetAddressError').show().text('Please enter a street address');
        } else if (streetAddress != '') {
            $('#userAddressForm #streetAddress').removeClass('has-error');
            $('#userAddressForm #streetAddressError').hide().text('');
        }
    });
    $('#userAddressForm #city').focusout(function() {
        var city = $(this).val();
        if (city == '') {
            $('#userAddressForm #city').addClass('has-error');
            $('#userAddressForm #cityError').show().text('Please enter a city');
        } else if (city != '') {
            $('#userAddressForm #city').removeClass('has-error');
            $('#userAddressForm #cityError').hide().text('');
        }
    });
    $('#userAddressForm #postcode').focusout(function() {
        var postcode = $(this).val();
        if (postcode == '') {
            $('#userAddressForm #postcode').addClass('has-error');
            $('#userAddressForm #postcodeError').show().text('Please enter a postcode / zip');
        } else if (postcode != '') {
            $('#userAddressForm #postcode').removeClass('has-error');
            $('#userAddressForm #postcodeError').hide().text('');
        }
    });

    $("#updateAddress").on('click', function(event) {
        event.preventDefault();
        $('#responseAddressMessage').html('');
        $('#userAddressForm #streetAddress').trigger("focusout");  
        $('#userAddressForm #city').trigger("focusout");
        $('#userAddressForm #postcode').trigger("focusout"); 
        var country = $('#userAddressForm #country').val();
        if (country == '') {
            $('#userAddressForm #country').parent().find('.nice-select').addClass('has-error');
            $('#userAddressForm #countryError').show().text('Please enter a country');
        } else if (country != '') {
            $('#userAddressForm #country').parent().find('.nice-select').removeClass('has-error');
            $('#userAddressForm #countryError').hide().text('');
        }
        var city = $("#userAddressForm #city").val();
        var postcode = $("#userAddressForm #postcode").val();
        var streetAddress = $("#userAddressForm #streetAddress").val();
        if ((country != '') && (city != '') && (postcode != '') && (streetAddress != '')) {
            updateAddressDetails();
        }
    });

    function updateAddressDetails() {
        $('#responseAddressMessage').html('');
        $('#userAddressForm #ajax-loader').show();
        var token = $('#userAddressForm #_token').val();
        var city = $("#userAddressForm #city").val();
        var state = $("#userAddressForm #state").val();
        var country = $('#userAddressForm #country').val();
        var postcode = $("#userAddressForm #postcode").val();
        var streetAddress = $("#userAddressForm #streetAddress").val(); 
        var streetAddress1 = $("#userAddressForm #streetAddress1").val();
        
        $.ajax({
            url: baseUrl + "/addressBook",
            type: "POST",
            data: {
                '_token': token,
                'city': city,
                'state': state,
                'country' : country,
                'postcode': postcode,
                'streetAddress': streetAddress,
                'streetAddress1': streetAddress1,
            },
            dataType: "JSON",
            success: function(jsonStr) {
                var res_data = JSON.stringify(jsonStr);
                var response = JSON.parse(res_data);
                var responseData = response['responseData'];
                if ((responseData != null) && (responseData == 'user address detail update successfully')) {
                    $('#responseAddressMessage').html('Your address details update successfully');
                    setTimeout(function(){ $('#responseAddressMessage').html('');  }, 3000);
                } else {
                    $('#responseAddressMessage').html(responseData);
                }
                $('#userAddressForm #ajax-loader').hide();
            }
        });
    }

/*--------------------------------change Password-----------------------------------------------------*/
    $('#userchangePassword #currentpassword').focusout(function(){		
        var currentpassword = $(this).val();
        if(currentpassword == ''){
            $('#userchangePassword #currentpassword').addClass('has-error');
            $('#userchangePassword #currentpasswordError').show().text('Current password empty or invalid');
        }else if(currentpassword.length < 6){
            $('#userchangePassword #currentpassword').addClass('has-error');
            $('#userchangePassword #currentpasswordError').show().text('Password must be at least 6 characters'); 
        }else if(currentpassword != ''){
            $('#userchangePassword #currentpassword').removeClass('has-error');
            $('#userchangePassword #currentpasswordError').hide().text(''); 
        }
    }); 

    $('#userchangePassword #newpassword').focusout(function(){		
        var newpassword = $(this).val();
        if(newpassword == ''){
            $('#userchangePassword #newpassword').addClass('has-error');
            $('#userchangePassword #newpasswordError').show().text('New password empty or invalid');
        }else if(newpassword.length < 6){
            $('#userchangePassword #newpassword').addClass('has-error');
            $('#userchangePassword #newpasswordError').show().text('Password must be at least 6 characters'); 
        }else if(newpassword != ''){
            $('#userchangePassword #newpassword').removeClass('has-error');
            $('#userchangePassword #newpasswordError').hide().text(''); 
        }
    });

    $('#userchangePassword #confirmpassword').focusout(function(){		
        var newpassword = $('#userchangePassword #newpassword').val();
        var confirmpassword = $(this).val();
        if(confirmpassword == ''){
            $('#userchangePassword #confirmpassword').addClass('has-error');
            $('#userchangePassword #confirmpasswordError').show().text('Confirm password empty or invalid');
        }else if(confirmpassword.length < 6){
            $('#userchangePassword #confirmpassword').addClass('has-error');
            $('#userchangePassword #confirmpasswordError').show().text('Password must be at least 6 characters'); 
        }else if(confirmpassword != newpassword){
            $('#userchangePassword #confirmpassword').addClass('has-error');
            $('#userchangePassword #confirmpasswordError').show().text('New passwords must match. Please retry.'); 
        }else if(confirmpassword != ''){
            $('#userchangePassword #confirmpassword').removeClass('has-error');
            $('#userchangePassword #confirmpasswordError').hide().text(''); 
        }
    });

    $('#updatepassword').on('click', function(event){
        event.preventDefault();
        $('#responsePMessage').html('');
        $('#userchangePassword #currentpassword').trigger("focusout");
        $('#userchangePassword #newpassword').trigger("focusout");
        $('#userchangePassword #confirmpassword').trigger("focusout");
        var currentpassword = $("#userchangePassword #currentpassword").val();	
        var newpassword = $("#userchangePassword #newpassword").val();
        var confirmpassword = $("#userchangePassword #confirmpassword").val();
        if(((currentpassword != '') && (currentpassword.length >= 6)) && ((newpassword != '') && (newpassword.length >= 6)) && ((confirmpassword != '') && (confirmpassword.length >= 6)) && (confirmpassword == newpassword)){
            $('#userchangePassword #currentpassword').removeClass('has-error');
            $('#userchangePassword #newpassword').removeClass('has-error');
            $('#userchangePassword #confirmpassword').removeClass('has-error');
            $('#userchangePassword #currentpasswordError').text('').hide();
            $('#userchangePassword #newpasswordError').text('').hide();
            $('#userchangePassword #confirmpasswordError').text('').hide();
            var email = $('#userchangePassword #email').val()
            if(email != ''){
                changeUserProfilePassword();
            }
        }
    });

    function changeUserProfilePassword(){	
        var token = $('#userchangePassword #_token').val();
        $.ajax({
            url: baseUrl+"/updatePassword",
            type: "POST",
            data: {'_token': token,'email' : $('#userchangePassword #email').val(),'currentpassword' : $('#userchangePassword #currentpassword').val(),'newpassword' : $('#userchangePassword #newpassword').val(),'confirmpassword' : $('#userchangePassword #confirmpassword').val(), 'changePassword': 'Change Password'},
            dataType: "JSON",
            success: function (jsonStr) {
                var res_data = JSON.stringify(jsonStr);
                var response = JSON.parse(res_data);
                var responseData = response['responseData'];
                if(responseData == 'Your account password update successfully'){
                    $('#responsePMessage').html('Your account password update successfully');
                    setTimeout(function(){ $('#responsePMessage').html('');  }, 3000);
                }else if(responseData == 'request to change password already exist'){
                    $('#responsePMessage').html('Your request to change password already exist');
                    setTimeout(function(){ $('#responsePMessage').html('');  }, 3000);
                    $('#userchangePassword #newpassword').addClass('has-error');
                    $('#userchangePassword #confirmpassword').addClass('has-error');
                }else if(responseData == 'Invalid password'){
                    $('#userchangePassword #currentpassword').addClass('has-error');
                    $('#userchangePassword #currentpasswordError').show().text('Your current password invalid'); 
                }else if(responseData == 'Somthing went wrong'){
                    $('#responsePMessage').html('An error occurred updating the password');
                    setTimeout(function(){ $('#responsePMessage').html('');  }, 3000);  
                }else if(responseData == 'You are not authorized user'){
                    window.location.href = baseUrl+'/login-register';
                }
            }
        });		
    }

});
</script>