<div class="row">
	<div class="col-12">
		<form action="<?php echo e(route('product_category.store')); ?>" method="POST">
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Category Name</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="name" value="<?php echo e(old('name')); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
		</form>
	</div>
</div>