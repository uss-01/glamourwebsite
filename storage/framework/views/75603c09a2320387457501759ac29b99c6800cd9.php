<?php $__env->startSection('header'); ?><?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container">
  <div class="row">
    <div class="col col-sm-4 col-md-6 col-lg-4 offset-sm-4 offset-md-3 offset-lg-4 mt-4 mt-sm-5">
      <img src="<?php echo e(asset('images/logo.png')); ?>" alt="Tawal Mall" width="97px" class="mx-auto d-block img-fluid mb-4">
      <div class="card p-4 mt-4">
        <form class="" action="<?php echo e(route('login')); ?>" method="post">
          <?php echo e(csrf_field()); ?>

          <div class="form-group<?php echo e($errors->has('email') ? ' has-danger' : ''); ?>">
            <label class="form-control-label" for="email"><strong>Email address</strong></label>
            <input type="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>" tabindex="1" autofocus>
            <?php if($errors->has('email')): ?>
              <small class="form-control-feedback"><?php echo e($errors->first('email')); ?></small>
            <?php endif; ?>
          </div>

          <div class="form-group<?php echo e($errors->has('email') ? ' has-danger' : ''); ?>">
            <a href="<?php echo e(route('password.request')); ?>" title="forgot password?" class="float-right" tabindex="5"><small>forgot your password?</small></a>
            <label class="form-control-label" for="password"><strong>Password</strong></label>
            <input type="password" class="form-control" name="password" tabindex="2">
            <?php if($errors->has('password')): ?>
              <small class="form-control-feedback"><?php echo e($errors->first('password')); ?></small>
            <?php endif; ?>
          </div>

          <div class="form-group mb-0">
            <button type="submit" class="btn btn-primary" tabindex="4">Sign in</button>
          </div>
        </form>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
<?php $__env->startComponent('auth.footer'); ?><?php echo $__env->renderComponent(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>