<?php echo $__env->make('frontend.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<style type="text/css">
    .has-error {
        border: 1px solid red !important;
    }
    #couponCodeError {
        color: red;
    }
    #responseMessage, #responseMSG, #resMessage{
        margin-top: 8px;
    }
    .error-text.success{
        color: #000 !important;
    }
    .couponDis{
        display : none;
    }
</style>
    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="<?php echo e(url('/shop')); ?>">shop</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">cart</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- cart main wrapper start -->
        
        <div class="cart-main-wrapper section-padding">
            <div class="container">
                <?php if(!empty($bladeVar['userCartProducts'])): ?>
                    <?php
                    $arrayCartlist = array();
                    if(!empty($bladeVar['userCartData'])){ 
                        foreach($bladeVar['userCartData'] as $cartlist){
                            $arrayCartlist[] = $cartlist->product_id;
                        } 
                    } ?> 
                    <?php if(!empty($arrayCartlist)): ?> 
                        <?php if(sizeof($bladeVar['userCartProducts']) > 0): ?>
                        <div class="section-bg-color">  
                            <div class="row">
                                <div class="col-lg-12">
                                    <!-- Cart Table Area -->
                                    <div class="cart-table table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="pro-thumbnail">Thumbnail</th>
                                                    <th class="pro-title">Product</th>
                                                    <th class="pro-price">Price</th>
                                                    <th class="pro-quantity">Quantity</th>
                                                    <th class="pro-subtotal">Total</th>
                                                    <th class="pro-remove">Remove</th>
                                                </tr>
                                            </thead>
                                            <tbody class="cartList">
                                                <?php $__currentLoopData = $bladeVar['userCartProducts']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($product->deleted_at == NULL): ?>
                                                        <?php if(in_array($product->_id, $arrayCartlist)): ?>  
                                                        <tr id="product<?php echo e($product->_id); ?>">
                                                            <td class="pro-thumbnail"><a href="<?php echo e(url('/shop/'.$product->slug)); ?>"><img class="img-fluid" src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>" /></a></td>
                                                            <td class="pro-title"><a href="<?php echo e(url('/shop/'.$product->slug)); ?>"><?php echo e($product->name); ?></a></td>
                                                            <td class="pro-price"><span>
                                                                <?php if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')): ?>
                                                                    $<?php echo e($product->discounted_price); ?>

                                                                <?php else: ?>
                                                                    $<?php echo e($product->price); ?>

                                                                <?php endif; ?>
                                                            </span></td>
                                                            <td class="pro-quantity">
                                                                <div class="pro-qty"><input type="text" class="proQuantity" value="<?php echo e($product->quantity); ?>"></div>
                                                            </td>
                                                            <td class="pro-subtotal"><span>
                                                                <?php if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')): ?>
                                                                    $<?php echo e(number_format($product->discounted_price * $product->quantity, 2)); ?>

                                                                <?php else: ?>
                                                                    $<?php echo e(number_format($product->price * $product->quantity, 2)); ?>

                                                                <?php endif; ?>
                                                            </span></td>
                                                            <td class="pro-remove"><a href="#" class="removeProduct" data-pid="<?php echo e($product->_id); ?>"><i class="fa fa-trash-o"></i></a></td>
                                                        </tr>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- Cart Update Option -->
                                    <div class="cart-update-option d-block d-md-flex justify-content-between">
                                        <div class="apply-coupon-wrapper">
                                            <form id="couponForm" action="#" method="post" class=" d-block d-md-flex">
                                                <input type="text" id="couponCode" name="couponCode" placeholder="Enter Your Coupon Code" />
                                                <button class="btn btn-sqr" id="applyCoupon">Apply Coupon</button>
                                            </form>
                                            <span class="error-text" id="couponCodeError"></span>
                                        </div>
                                        <div class="cart-update">
                                            <a href="#" id="updateCartItems" class="btn btn-sqr">Update Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 ml-auto">
                                    <!-- Cart Calculation Area -->
                                    <div class="cart-calculator-wrapper">
                                        <div class="cart-calculate-items">
                                            <h6>Cart Totals</h6>
                                            <div class="table-responsive">
                                                <?php 
                                                    $subTotal = array();
                                                    $shipping = '0.00';
                                                    foreach($bladeVar['userCartProducts'] as $product){
                                                        if($product->deleted_at == NULL){
                                                            if(in_array($product->_id, $arrayCartlist)){
                                                                if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')){
                                                                    $subTotal[] = number_format($product->discounted_price * $product->quantity, 2);
                                                                }else{
                                                                    $subTotal[] = number_format($product->price * $product->quantity, 2);
                                                                }
                                                            }
                                                        }
                                                    }
                                                ?>
                                                <table class="table">
                                                    <tr>
                                                        <td>Sub Total</td>
                                                        <td class="subTotal">$<?php if(!empty($subTotal)){ echo number_format(array_sum($subTotal), 2); }else{ echo '0.00'; } ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shipping</td>
                                                        <td class="shipping">$<?php if(!empty($shipping)){ echo $shipping; }else{ echo '0.00'; } ?></td>
                                                    </tr>
                                                    <tr class="couponDis">
                                                        <td>Coupon Discount</td>
                                                        <td class="totaldis"></td>
                                                    </tr>
                                                    <tr class="total">
                                                        <td>Total</td>
                                                        <td class="total-amount">$<?php if(!empty($subTotal)){ echo number_format((array_sum($subTotal)+$shipping), 2); }else{ echo '0.00'; } ?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <a href="<?php echo e(url('/checkout')); ?>" class="btn btn-sqr d-block">Proceed Checkout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    <?php else: ?>
                        <div class="row">
                            <div class="col-lg-12" style="text-align: center;">
                                <h3>Your cart is empty!</h3>
                                <p>Add items to it now.</p>
                                <a href="<?php echo e(url('/')); ?>" class="btn btn-sqr">Shop now</a>
                            </div>
                        </div>    
                    <?php endif; ?>
                <?php else: ?>
                    <div class="row">
                        <div class="col-lg-12" style="text-align: center;">
                            <h3>Your cart is empty!</h3>
                            <p>Add items to it now.</p>
                            <a href="<?php echo e(url('/')); ?>" class="btn btn-sqr">Shop now</a>
                        </div>
                    </div>    
                <?php endif; ?>
            </div>
        </div>
        
        <!-- cart main wrapper end -->
    </main>
<?php echo $__env->make('frontend.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script type="text/javascript">
$(document).ready(function() {
    var baseUrl = '<?php echo e(url("/")); ?>';
    var token = '<?php echo e(csrf_token()); ?>';
    $(".removeProduct").on('click', function(event) {
        event.preventDefault();
        var pid = $(this).attr('data-pid');
        var userID = '';
        <?php $data = Session::all(); if (Session::has('userLoggedIn')){ ?>
            userID = '<?php echo Session::get('userLoggedIn'); ?>';
        <?php } ?>
        if((userID != '') && (pid != '')){
            $.ajax({
				url: baseUrl + "/removecartitem",
				type: "POST",
				data: { 
                    '_token': token, 
					'product_id': pid,
				},
				dataType: "JSON",
				success: function(jsonStr) {
					var res_data = JSON.stringify(jsonStr);
					var response = JSON.parse(res_data);
                    var responseData = response['responseData'];
					if ((responseData != null) && (responseData == 'remove to cart successfully')) {
                          var redirecturl = baseUrl+'/cart';
                          location.href = redirecturl;
					} else {
						console.log(responseData);
                    }
				}
			});    
        }else{
           var redirecturl = baseUrl+'/login-register';
           location.href = redirecturl;  
        }
    });    

    $("#updateCartItems").on('click', function(event) {
        event.preventDefault();
        var userID = '';
        <?php $data = Session::all(); if (Session::has('userLoggedIn')){ ?>
            userID = '<?php echo Session::get('userLoggedIn'); ?>';
        <?php } ?>
        if(userID != ''){
            var proIdArray = [];
            $(".cartList tr").each(function( index, element ) {
                var data = {};
                var proId = $(this).find('.removeProduct').attr('data-pid'); 
                var proQ = $(this).find('.proQuantity').val();
                data.productId = proId;
                data.productQ = proQ;
                proIdArray.push(data);
            }); 
            if(proIdArray.length > 0){
                $.ajax({
                    url: baseUrl + "/updatecart",
                    type: "POST",
                    data: { 
                        '_token': token, 
                        'productIds': proIdArray,
                    },
                    dataType: "JSON",
                    success: function(jsonStr) {
                        var res_data = JSON.stringify(jsonStr);
                        var response = JSON.parse(res_data);
                        var responseData = response['responseData'];
                        if ((responseData != null) && (responseData == 'update cart successfully')) {
                            var redirecturl = baseUrl+'/cart';
                            location.href = redirecturl;
                        } else {
                            console.log(responseData);
                        }
                    }
                }); 
            }
        }else{
           var redirecturl = baseUrl+'/login-register';
           location.href = redirecturl;  
        }
    });    

});
</script>