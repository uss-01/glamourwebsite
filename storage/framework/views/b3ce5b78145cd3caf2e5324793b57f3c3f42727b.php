<?php $__env->startSection('head'); ?>
    <link type="text/css" href="<?php echo e(asset('thirdparty/editor/summernote.css')); ?>" rel="stylesheet"/>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container-fluid" id="pageContainer">
        <div class="row" id="pageContent">
            <div class="col-12 col-md-8 offset-md-2">
                <div class="float-right mt-2 hidden-xs-down">
                   <a href="<?php echo e(route('setting.index')); ?>" class="btn btn-secondary" role="button" title="Refresh List"><i class="fa fa-refresh"></i></a>
                </div>
                <div class="float-right mt-1 hidden-sm-up">
                    <a href="<?php echo e(route('setting.index')); ?>" class="btn btn-secondary btn-sm btn-search" title="Search" data-search="<?php echo e(!empty(Request::input('q')) ? 'yes' : ''); ?>"><i class="fa fa-search"></i><span<?php echo !empty(Request::input('q')) ? '' : ' class="hidden-xs-up"'; ?>> Close</span></a>
                    <a href="<?php echo e(route('setting.index')); ?>" class="btn btn-secondary btn-sm" role="button" title="Refresh List"><i class="fa fa-refresh"></i></a>
                </div>
                <h1 class="hidden-xs-down"><?php echo e(Request::input('q') ? 'Search ' : ''); ?>Setting </h1>
                <h1 class="hidden-sm-up h3"><?php echo e(Request::input('q') ? 'Search ' : ''); ?>Setting </h1>
                <div class="clearfix alertArea"></div>
                <div class="card mb-2 searchContainer<?php echo e(Request::input('q') ? '' : ' hidden-xs-up'); ?>">
                    <div class="card-block">
                      
                    </div>
                </div>
                <?php if($bladeVar['total_count'] > 0): ?>
                    <table class="table table-bordered table-hover table-sm">
                        <thead class="bg-faded">
                            <tr>
                                <th class="text-center" style="width: 4rem;"><nobr><a href="<?php echo e(url()->current()); ?>?<?php echo e(Request::input('q') ? 'q='.Request::input('q').'&' : ''); ?>sort=id&dir=<?php echo e(Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'desc' : 'asc' : 'asc'); ?>">Id</a> <i class="fa fa-<?php echo e(Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'sort-numeric-asc' : 'sort-numeric-desc' : 'sort'); ?>"></i></nobr></th>
                                <th><nobr>Title</nobr></th>
                                <th><nobr>Email</nobr></th>
                                <th><nobr>Contact No.</nobr></th>
                                <th class="w-td-action">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $bladeVar['results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr<?php echo $rs->deleted_at != null ? ' class="table-danger"' : ''; ?>>
                                    <td class="text-center"><span<?php echo $rs->deleted_at != null ? ' class="badge badge-danger"' : ''; ?>><?php echo e($rs->_id); ?></span></td>
                                    <td><?php echo e($rs->title); ?></td>
                                     <td><?php echo e($rs->email); ?></td>
                                    <td><?php echo e($rs->phone); ?></td>
                                  
                                    <td class="text-center text-sm-left">
                                        <div class="hidden-xs-down">
                                            <a href="<?php echo e(route('setting.show', $rs->_id)); ?>" class="btn btn-sm btn-info" title="View" data-toggle="modal" data-target="#viewModal"><i class="fa fa-eye"></i></a> 
                                            <a href="<?php echo e(route('setting.edit', $rs->_id)); ?>" class="btn btn-sm btn-info<?php echo e($rs->deleted_at != null ? ' disabled' : ''); ?>" title="Edit" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i></a> 
                                        </div>
                                        <div class="btn-group hidden-sm-up">
                                            <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Actions
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a href="<?php echo e(route('setting.show', $rs->_id)); ?>" class="dropdown-item" data-toggle="modal" data-target="#viewModal"><i class="fa fa-eye text-info"></i> View</a>
                                                <a href="<?php echo e(route('setting.edit', $rs->_id)); ?>" class="dropdown-item<?php echo e($rs->deleted_at != null ? ' disabled' : ''); ?>" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil text-<?php echo e($rs->deleted_at != null ? 'muted' : 'info'); ?>"></i> Edit</a> 
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                    <div class="mt-4">
                        <?php echo e($bladeVar['results']->appends(Request::query())->links()); ?>

                        <small class="d-block text-center text-muted">(showing 20 results per page)</small>
                    </div>
                <?php else: ?>
                    <div class="alert alert-success">
                        <strong>No records found!</strong>
                        <?php if(!empty(Request::input('q'))): ?>
                            No matching your search criteria. 
                        <?php else: ?> 
                            No info added yet, please click on "Create New" button to add a setting. 
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <!-- Deletetion code with Form -->
                <form class="formDelete">
                    <?php echo e(method_field('DELETE')); ?>

                </form>

                <!-- View modal code: START -->
                <div class="modal" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="viewModalTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="viewModalTitle">View Setting</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            </div>
                            <div class="modal-footer hidden-xs-up">
                                <a href="javascript:void(0);" class="btn btn-info btn-edit" title="Edit" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i> Edit</a>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- View modal code: END -->
                <!-- Add modal code: START -->
                <div class="modal" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="createModalTitle">Add Setting</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            </div>
                          
                        </div>
                    </div>
                </div>
                <!-- Add modal code: END -->

                <!-- Edit modal code: START -->
                <div class="modal" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="editModalTitle">Edit Setting</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            </div>
                            <div class="modal-footer hidden-xs-up">
                               <!--  <button type="button" class="btn btn-success btn-submit">Submit</button> -->
                               <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Edit modal code: END -->

               
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('foot'); ?>
    <script type="text/javascript">
        var pageUrl = '<?php echo e(route('setting.index')); ?>';
        var ajaxLoadUrl = '<?php echo e(url()->full()); ?>';
        var pageContainer = '#pageContainer';
        var pageContent = '#pageContent';
    </script>
    <script src="<?php echo e(asset('js/jquery.form.js')); ?>"></script>
    <script src="<?php echo e(asset('js/setting.js?v=1')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>