<div class="row">
	<div class="col-12">
		<form action="<?php echo e(route('coupons.store')); ?>" enctype="multipart/form-data" method="POST" id="brand_form">
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Coupon Name</strong><small class="text-danger"> (required)</small></label>
				<input type="text" class="form-control" name="name" value="<?php echo e(old('name')); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="product_status"><strong>Coupon Type</strong></label>
				<select name="type" class="form-control">
					<option value=""<?php echo e(empty(old('type')) ? ' selected="selected"' : ''); ?>></option>
					<?php $__currentLoopData = $bladeVar['couponType']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<option value="<?php echo e($type); ?>"<?php echo e($type == old('type') ? ' selected="selected"' : ''); ?>><?php echo e($type); ?></option>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="value"><strong>Coupon Value</strong></label>
				<input type="text" class="form-control" name="value" value="<?php echo e(old('value')); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="quantity"><strong>Coupon Quantity</strong></label>
				<input type="text" class="form-control" name="quantity" value="<?php echo e(old('quantity')); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="expiry"><strong>Coupon Expiry</strong></label>
				<div class="input-append date form_datetime">
					<input size="16" type="text" value="" readonly>
					<span class="add-on"><i class="icon-th"></i></span>
				</div>
				<!-- <input type="text" class="form-control" id="expiry" name="expiry" value="<?php echo e(old('expiry')); ?>" autofocus>
				<small class="form-control-feedback"></small> -->
			</div>
			<div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div> 
		</form>
	</div>
</div>
<script type="text/javascript">
//$('#expiry').datetimepicker();
</script> 
<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "dd MM yyyy - hh:ii"
    });
</script> 