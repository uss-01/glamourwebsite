<div class="row">
	<div class="col-12">
		<form action="<?php echo e(route('cms.store')); ?>" enctype="multipart/form-data" method="POST" id="cms_form">
		  <div class="row">
			<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Name</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="name" value="<?php echo e(old('name')); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			</div>
			<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="title"><strong>Title</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="title" value="<?php echo e(old('title')); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			</div>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="image"><strong>Banner</strong> <small class="text-danger">(optional)</small></label>
				<input type="file" name="image" class="form-control">
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="content"><strong>Content</strong></label>
				<textarea class="form-control" id="htmlTextEditor" name="content"><?php echo e(old('content')); ?></textarea>
				<small class="form-control-feedback"></small>
			</div>
			 <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
			
        
		</form>
	</div>
</div>
<script type="text/javascript">
     $(document).ready(function() {
	$('#htmlTextEditor').summernote({
              height:300,
              dialogsInBody: true
            });
  });
</script>