<!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->

    <!-- footer area start -->
    <footer class="footer-widget-area">
        <div class="footer-top section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="widget-item">
                            <?php if(!empty($bladeVar['site_setting']->count() > 0)): ?>
                                <?php $__currentLoopData = $bladeVar['site_setting']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="widget-title">
                                    <div class="widget-logo">
                                        <a href="<?php echo e(url('/')); ?>">
                                            <img src="<?php echo e(asset('images/cms').'/'.$setting->image); ?>" alt="<?php echo e($setting->title); ?>">
                                        </a>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <p><?php echo e($setting->message); ?></p>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="widget-item">
                            <h6 class="widget-title">Contact Us</h6>
                            <div class="widget-body">
                                <address class="contact-block">
                                    <ul>
                                        <?php if(!empty($bladeVar['site_setting']->count() > 0)): ?>
                                            <?php $__currentLoopData = $bladeVar['site_setting']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($setting->address != ''): ?><li><i class="pe-7s-home"></i> <?php echo e($setting->address); ?></li><?php endif; ?>
                                            <?php if($setting->email != ''): ?><li><i class="pe-7s-mail"></i> <a href="mailto:<?php echo e($setting->email); ?>"><?php echo e($setting->email); ?> </a></li><?php endif; ?>
                                            <?php if($setting->phone != ''): ?><li><i class="pe-7s-call"></i> <a href="tel:<?php echo e($setting->phone); ?>"><?php echo e($setting->phone); ?></a></li><?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </ul>
                                </address>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="widget-item">
                        <?php //print_r($bladeVar['cms_pages']); ?>
                        <?php if(!empty($bladeVar['cms_pages']->count() > 0)): ?>
                            <h6 class="widget-title">Information</h6>
                            <div class="widget-body">
                                <ul class="info-list">
                                <?php $__currentLoopData = $bladeVar['cms_pages']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><a href="<?php echo e(url('/'.$pages->slug)); ?>"><?php echo e($pages->name); ?></a></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        <?php endif; ?> 
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="widget-item">
                            <?php if(!empty($bladeVar['site_setting']->count() > 0)): ?>
                            <?php $__currentLoopData = $bladeVar['site_setting']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <h6 class="widget-title">Follow Us</h6>
                            <div class="widget-body social-link">
                                <?php if($setting->facebook_url != ''): ?><a href="<?php echo e($setting->facebook_url); ?>"><i class="fa fa-facebook"></i></a><?php endif; ?>
                                <?php if($setting->twitter_url != ''): ?><a href="<?php echo e($setting->twitter_url); ?>"><i class="fa fa-twitter"></i></a><?php endif; ?>
                                <?php if($setting->linkedin_url != ''): ?><a href="<?php echo e($setting->linkedin_url); ?>"><i class="fa fa-linkedin"></i></a><?php endif; ?>
                                <?php if($setting->pinterest_url != ''): ?><a href="<?php echo e($setting->pinterest_url); ?>"><i class="fa fa-pinterest-p"></i></a><?php endif; ?>
                                <?php if($setting->instagram_url != ''): ?><a href="<?php echo e($setting->instagram_url); ?>"><i class="fa fa-instagram"></i></a><?php endif; ?>
                                <?php if($setting->youtube_url != ''): ?><a href="<?php echo e($setting->youtube_url); ?>"><i class="fa fa-youtube"></i></a><?php endif; ?>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center mt-20">
                    <div class="col-md-6">
                        <div class="newsletter-wrapper">
                            <!-- <h6 class="widget-title-text">Signup for newsletter</h6>
                            <form class="newsletter-inner" id="mc-form">
                                <input type="email" class="news-field" id="mc-email" autocomplete="off" placeholder="Enter your email address">
                                <button class="news-btn" id="mc-submit">Subscribe</button>
                            </form> -->
                            <!-- mailchimp-alerts Start -->
                            <!-- <div class="mailchimp-alerts">
                                <div class="mailchimp-submitting"></div>
                                <div class="mailchimp-success"></div>
                                <div class="mailchimp-error"></div>
                            </div> -->
                            <!-- mailchimp-alerts end -->
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="footer-payment">
                            <img src="<?php echo e(asset('assets/img/payment.png')); ?>" alt="payment method">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="copyright-text text-center">
                            <p>Powered By <a href="<?php echo e(url('/')); ?>">Glamour Jewellery</a>. © 2020</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer area end -->

    <!-- Quick view modal start -->
    <div class="modal" id="quick_view">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <!-- product details inner end -->
                    <div class="product-details-inner">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="product-large-slider">
                                    <div class="pro-large-img img-zoom">
                                        <img src="assets/img/product/product-details-img1.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-large-img img-zoom">
                                        <img src="assets/img/product/product-details-img2.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-large-img img-zoom">
                                        <img src="assets/img/product/product-details-img3.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-large-img img-zoom">
                                        <img src="assets/img/product/product-details-img4.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-large-img img-zoom">
                                        <img src="assets/img/product/product-details-img5.jpg" alt="product-details" />
                                    </div>
                                </div>
                                <div class="pro-nav slick-row-10 slick-arrow-style">
                                    <div class="pro-nav-thumb">
                                        <img src="assets/img/product/product-details-img1.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-nav-thumb">
                                        <img src="assets/img/product/product-details-img2.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-nav-thumb">
                                        <img src="assets/img/product/product-details-img3.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-nav-thumb">
                                        <img src="assets/img/product/product-details-img4.jpg" alt="product-details" />
                                    </div>
                                    <div class="pro-nav-thumb">
                                        <img src="assets/img/product/product-details-img5.jpg" alt="product-details" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="product-details-des">
                                    <div class="manufacturer-name">
                                        <a href="product-details.html">HasTech</a>
                                    </div>
                                    <h3 class="product-name">Handmade Golden Necklace</h3>
                                    <div class="ratings d-flex">
                                        <span><i class="fa fa-star-o"></i></span>
                                        <span><i class="fa fa-star-o"></i></span>
                                        <span><i class="fa fa-star-o"></i></span>
                                        <span><i class="fa fa-star-o"></i></span>
                                        <span><i class="fa fa-star-o"></i></span>
                                        <div class="pro-review">
                                            <span>1 Reviews</span>
                                        </div>
                                    </div>
                                    <div class="price-box">
                                        <span class="price-regular">$70.00</span>
                                        <span class="price-old"><del>$90.00</del></span>
                                    </div>
                                    <h5 class="offer-text"><strong>Hurry up</strong>! offer ends in:</h5>
                                    <div class="product-countdown" data-countdown="2022/02/20"></div>
                                    <div class="availability">
                                        <i class="fa fa-check-circle"></i>
                                        <span>200 in stock</span>
                                    </div>
                                    <p class="pro-desc">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
                                        eirmod tempor invidunt ut labore et dolore magna.</p>
                                    <div class="quantity-cart-box d-flex align-items-center">
                                        <h6 class="option-title">qty:</h6>
                                        <div class="quantity">
                                            <div class="pro-qty"><input type="text" value="1"></div>
                                        </div>
                                        <div class="action_link">
                                            <a class="btn btn-cart2" href="#">Add to cart</a>
                                        </div>
                                    </div>
                                    <div class="useful-links">
                                        <a href="#" data-toggle="tooltip" title="Compare"><i
                                            class="pe-7s-refresh-2"></i>compare</a>
                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i
                                            class="pe-7s-like"></i>wishlist</a>
                                    </div>
                                    <div class="like-icon">
                                        <a class="facebook" href="#"><i class="fa fa-facebook"></i>like</a>
                                        <a class="twitter" href="#"><i class="fa fa-twitter"></i>tweet</a>
                                        <a class="pinterest" href="#"><i class="fa fa-pinterest"></i>save</a>
                                        <a class="google" href="#"><i class="fa fa-google-plus"></i>share</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- product details inner end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Quick view modal end -->

    <!-- offcanvas mini cart start -->
    <div class="offcanvas-minicart-wrapper">
        <div class="minicart-inner">
            <div class="offcanvas-overlay"></div>
            <div class="minicart-inner-content">
                <div class="minicart-close">
                    <i class="pe-7s-close"></i>
                </div>    
                <?php if(!empty($bladeVar['userCartProducts'])): ?>
                <?php
                    $arrayCartlist = array();
                    if(!empty($bladeVar['userCartData'])){ 
                        foreach($bladeVar['userCartData'] as $cartlist){
                            $arrayCartlist[] = $cartlist->product_id;
                        } 
                    } ?>
                    <?php if(!empty($arrayCartlist)): ?> 
                        <?php if(sizeof($bladeVar['userCartProducts']) > 0): ?>     
                        <div class="minicart-content-box">
                            <div class="minicart-item-wrapper">
                                <ul>
                                    <?php $__currentLoopData = $bladeVar['userCartProducts']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($product->deleted_at == NULL): ?>
                                            <?php if(in_array($product->_id, $arrayCartlist)): ?>
                                                <li class="minicart-item" id="product<?php echo e($product->_id); ?>">
                                                    <div class="minicart-thumb">
                                                        <a href="<?php echo e(url('/shop/'.$product->slug)); ?>">
                                                            <img src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
                                                        </a>
                                                    </div>
                                                    <div class="minicart-content">
                                                        <h3 class="product-name">
                                                            <a href="<?php echo e(url('/shop/'.$product->slug)); ?>"><?php echo e($product->name); ?></a>
                                                        </h3>
                                                        <p>
                                                            <span class="cart-quantity"><?php echo e($product->quantity); ?> <strong>&times;</strong></span>
                                                            <span class="cart-price">
                                                            <?php if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')): ?>
                                                                $<?php echo e(number_format($product->discounted_price, 2)); ?>

                                                            <?php else: ?>
                                                                $<?php echo e(number_format($product->price, 2)); ?>

                                                            <?php endif; ?>
                                                            </span>
                                                        </p>
                                                    </div>
                                                    <button class="minicart-remove removetocart" data-pid="<?php echo e($product->_id); ?>"><i class="pe-7s-close"></i></button>
                                                </li>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>

                            <div class="minicart-pricing-box">
                                <ul>
                                    <?php 
                                        $subTotal = array();
                                        $shipping = '0.00';
                                        foreach($bladeVar['userCartProducts'] as $product){
                                            if($product->deleted_at == NULL){
                                                if(in_array($product->_id, $arrayCartlist)){
                                                    if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')){
                                                        $subTotal[] = number_format($product->discounted_price * $product->quantity, 2);
                                                    }else{
                                                        $subTotal[] = number_format($product->price * $product->quantity, 2);
                                                    }
                                                }
                                            }
                                        }
                                    ?>
                                    <li>
                                        <span>sub-total</span>
                                        <span><strong>$<?php if(!empty($subTotal)){ echo number_format(array_sum($subTotal), 2); }else{ echo '0.00'; } ?></strong></span>
                                    </li>
                                    <li>
                                        <span>shipping</span>
                                        <span><strong>$<?php if(!empty($shipping)){ echo $shipping; }else{ echo '0.00'; } ?></strong></span>
                                    </li>
                                    <li class="total">
                                        <span>total</span>
                                        <span><strong>$<?php if(!empty($subTotal)){ echo number_format((array_sum($subTotal)+$shipping), 2); }else{ echo '0.00'; } ?></strong></span>
                                    </li>
                                </ul>
                            </div>

                            <div class="minicart-button">
                                <a href="<?php echo e(url('/cart')); ?>"><i class="fa fa-shopping-cart"></i> View Cart</a>
                                <a href="<?php echo e(url('/checkout')); ?>"><i class="fa fa-share"></i> Checkout</a>
                            </div>
                        </div>
                        <?php endif; ?>
                    <?php else: ?>
                        <div class="row">
                            <div class="col-lg-12" style="text-align: center;">
                                <h3>Your cart is empty!</h3>
                                <p>Add items to it now.</p>
                                <a href="<?php echo e(url('/')); ?>" class="btn btn-sqr">Shop now</a>
                            </div>
                        </div>    
                    <?php endif; ?>
                <?php else: ?>
                    <div class="row">
                        <div class="col-lg-12" style="text-align: center;">
                            <h3>Your cart is empty!</h3>
                            <p>Add items to it now.</p>
                            <a href="<?php echo e(url('/')); ?>" class="btn btn-sqr">Shop now</a>
                        </div>
                    </div>    
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!-- offcanvas mini cart end -->
    <!-- JS
============================================ -->
    <!-- Modernizer JS -->
    <script src="<?php echo e(asset('assets/js/vendor/modernizr-3.6.0.min.js')); ?>"></script>
    <!-- jQuery JS -->
    <script src="<?php echo e(asset('assets/js/vendor/jquery-3.3.1.min.js')); ?>"></script>
    <!-- Popper JS -->
    <script src="<?php echo e(asset('assets/js/vendor/popper.min.js')); ?>"></script>
    <!-- Bootstrap JS -->
    <script src="<?php echo e(asset('assets/js/vendor/bootstrap.min.js')); ?>"></script>
    <!-- slick Slider JS -->
    <script src="<?php echo e(asset('assets/js/plugins/slick.min.js')); ?>"></script>
    <!-- Countdown JS -->
    <script src="<?php echo e(asset('assets/js/plugins/countdown.min.js')); ?>"></script>
    <!-- Nice Select JS -->
    <script src="<?php echo e(asset('assets/js/plugins/nice-select.min.js')); ?>"></script>
    <!-- jquery UI JS -->
    <script src="<?php echo e(asset('assets/js/plugins/jqueryui.min.js')); ?>"></script>
    <!-- Image zoom JS -->
    <script src="<?php echo e(asset('assets/js/plugins/image-zoom.min.js')); ?>"></script>
    <!-- Imagesloaded JS -->
    <script src="<?php echo e(asset('assets/js/plugins/imagesloaded.pkgd.min.js')); ?>"></script>
    <!-- Instagram feed JS -->
    <script src="<?php echo e(asset('assets/js/plugins/instagramfeed.min.js')); ?>"></script>
    <!-- mailchimp active js -->
    <script src="<?php echo e(asset('assets/js/plugins/ajaxchimp.js')); ?>"></script>
    <!-- contact form dynamic js -->
    <script src="<?php echo e(asset('assets/js/plugins/ajax-mail.js')); ?>"></script>
    <!-- google map api -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfmCVTjRI007pC1Yk2o2d_EhgkjTsFVN8"></script>
    <!-- google map active js -->
    <script src="<?php echo e(asset('assets/js/plugins/google-map.js')); ?>"></script>
    <!-- Main JS -->
    <script src="<?php echo e(asset('assets/js/main.js')); ?>"></script>
<script type="text/javascript">
$(document).ready(function() {
    var baseUrl = '<?php echo e(url("/")); ?>';
    var token = '<?php echo e(csrf_token()); ?>';

    $(".addtowishlist").on('click', function(event) {
        event.preventDefault();
        var userID = '';
        <?php $data = Session::all(); if (Session::has('userLoggedIn')){ ?>
            userID = '<?php echo Session::get('userLoggedIn'); ?>';
        <?php } ?>
        if(userID != ''){
            var pid = $(this).attr('data-pid');
            $.ajax({
				url: baseUrl + "/addtowishlist",
				type: "POST",
				data: { 
                    '_token': token, 
					'product_id': pid,
				},
				dataType: "JSON",
				success: function(jsonStr) {
					var res_data = JSON.stringify(jsonStr);
					var response = JSON.parse(res_data);
                    var responseData = response['responseData'];
                    var responseDataCount = response['responseDataCount'];
					if ((responseData != null) && (responseData == 'add to wishlist successfully')) {
						$('#addtowishlist'+pid+' .pe-7s-like').addClass('addWishlist');
					} else {
						$('#addtowishlist'+pid+' .pe-7s-like').removeClass('addWishlist');
                    }
                    if((responseDataCount != null) && (responseDataCount != '0')){
                        $('.wishlist.notification').text(responseDataCount);
                    }else{
                        $('.wishlist.notification').text(responseDataCount);
                    }
				}
			});
        }else{
           var redirecturl = baseUrl+'/login-register';
           location.href = redirecturl;  
        }
    });

    $(".addtocart").on('click', function(event) {
        event.preventDefault();
        var userID = '';
        <?php $data = Session::all(); if (Session::has('userLoggedIn')){ ?>
            userID = '<?php echo Session::get('userLoggedIn'); ?>';
        <?php } ?>
        var getAllClass = $(this).attr('class');
        if(getAllClass == 'btn btn-cart addtocart disabled'){
            var redirecturl = baseUrl+'/cart';
            location.href = redirecturl;  
        }else{    
            if(userID != ''){
                var pid = $(this).attr('data-pid');
                var cid = $(this).attr('data-cid');
                var sid = $(this).attr('data-sid');
                $.ajax({
                    url: baseUrl + "/addtocart",
                    type: "POST",
                    data: { 
                        '_token': token, 
                        'product_id': pid,
                        'color_id': cid,
                        'size_id': sid
                    },
                    dataType: "JSON",
                    success: function(jsonStr) {
                        var res_data = JSON.stringify(jsonStr);
                        var response = JSON.parse(res_data);
                        var responseData = response['responseData'];
                        var responseDataCount = response['responseDataCount'];
                        if ((responseData != null) && (responseData == 'add to cart successfully')) {
                            $('#addtocart'+pid).addClass('disabled');
                            $('#addtocart'+pid).text('go to cart');
                        } else {
                            $('#addtocart'+pid).removeClass('disabled');
                            $('#addtocart'+pid).text('add to cart');
                        }
                        if((responseDataCount != null) && (responseDataCount != '0')){
                            $('.minicart-btn .notification').text(responseDataCount);
                        }else{
                            $('.minicart-btn .notification').text(responseDataCount);
                        }
                        viewCart();
                    }
                });
            }else{
                var redirecturl = baseUrl+'/login-register';
                location.href = redirecturl;  
            }
        }
    });

    $(document).on('click', '.removetocart', function(event){
        event.preventDefault();
        var pid = $(this).attr('data-pid');
        var userID = '';
        <?php $data = Session::all(); if (Session::has('userLoggedIn')){ ?>
            userID = '<?php echo Session::get('userLoggedIn'); ?>';
        <?php } ?>
        if((userID != '') && (pid != '')){
            $.ajax({
				url: baseUrl + "/removecartitem",
				type: "POST",
				data: { 
                    '_token': token, 
					'product_id': pid,
				},
                dataType: "JSON",
				success: function(jsonStr) {
					var res_data = JSON.stringify(jsonStr);
					var response = JSON.parse(res_data);
                    var responseData = response['responseData'];
                    var responseDataCount = response['responseDataCount'];
					if ((responseData != null) && (responseData == 'remove to cart successfully')) {
                          viewCart();
					} else {
						console.log(responseData);
                    }
                    if((responseDataCount != null) && (responseDataCount != '0')){
                        $('.minicart-btn .notification').text(responseDataCount);
                    }else{
                        $('.minicart-btn .notification').text(responseDataCount);
                    }
				}
			});    
        }else{
           var redirecturl = baseUrl+'/login-register';
           location.href = redirecturl;  
        }
    });

    $(".addToCartPro").on('click', function(event) {
        event.preventDefault();
        var userID = '';
        <?php $data = Session::all(); if (Session::has('userLoggedIn')){ ?>
            userID = '<?php echo Session::get('userLoggedIn'); ?>';
        <?php } ?>
        var getAllClass = $(this).attr('class');
        if(getAllClass == 'btn btn-cart2 addToCartPro disabled'){
            var redirecturl = baseUrl+'/cart';
            location.href = redirecturl;  
        }else{    
            if(userID != ''){
                var pid = $(this).attr('data-pid');
                //var cid = $(this).attr('data-cid');
                //var sid = $(this).attr('data-sid');
                var cid = $('.colorActive a').attr('data-pcid');
                var sid = $('#proSize').children("option:selected").val();
                var quantity = $('#proQuantity').val(); 
                $.ajax({
                    url: baseUrl + "/addtocart",
                    type: "POST",
                    data: { 
                        '_token': token, 
                        'product_id': pid,
                        'color_id': cid,
                        'size_id': sid,
                        'quantity' : quantity
                    },
                    dataType: "JSON",
                    success: function(jsonStr) {
                        var res_data = JSON.stringify(jsonStr);
                        var response = JSON.parse(res_data);
                        var responseData = response['responseData'];
                        var responseDataCount = response['responseDataCount'];
                        if ((responseData != null) && (responseData == 'add to cart successfully')) {
                            $('#addtocart'+pid).addClass('disabled');
                            $('#addtocart'+pid).text('Go To Cart');
                        } else {
                            $('#addtocart'+pid).removeClass('disabled');
                            $('#addtocart'+pid).text('Add To Cart');
                        }
                        if((responseDataCount != null) && (responseDataCount != '0')){
                            $('.minicart-btn .notification').text(responseDataCount);
                        }else{
                            $('.minicart-btn .notification').text(responseDataCount);
                        }
                        viewCart();
                    }
                });
            }else{
                var redirecturl = baseUrl+'/login-register';
                location.href = redirecturl;  
            }
        }
    });

    $(".color-categories li a").on('click', function(event) {
        event.preventDefault();
        $(".color-categories li").removeClass('colorActive');
        var cid = $(this).attr('data-pcid');
        if(cid != ''){
            $(this).parent('li').addClass('colorActive'); 
        }
        console.log(cid);
    });

    $('#couponCode').focusout(function() {
        $('.error-text').removeClass('success');
        var coupon = $(this).val();
        if (coupon == '') {
            $('#couponCode').addClass('has-error');
            $('#couponCodeError').show().text('Please enter a coupon code');
        } else if (coupon != '') {
            $('#couponCode').removeClass('has-error');
            $('#couponCodeError').hide().text('');
        }
    });

    $("#applyCoupon").on('click', function(event) {
        event.preventDefault();
        $('.error-text').removeClass('success');
        var userID = '';
        <?php $data = Session::all(); if (Session::has('userLoggedIn')){ ?>
            userID = '<?php echo Session::get('userLoggedIn'); ?>';
        <?php } ?>
        $('#couponCode').trigger("focusout");
        var couponCode = $("#couponCode").val();
        if((userID != '') && (couponCode != '')){
            var getRemAtt =  $("#applyCoupon").attr('data-rc');   
            if((getRemAtt != null) && (getRemAtt == 'remove')){
                $.ajax({
                    url: baseUrl + "/removecoupon",
                    type: "POST",
                    data: { 
                        '_token': token, 
                        'couponCode': couponCode,
                    },
                    dataType: "JSON",
                    success: function(jsonStr) {
                        var res_data = JSON.stringify(jsonStr);
                        var response = JSON.parse(res_data);
                        var responseData = response['responseData'];
                        var subTotalPrice = response['subTotalPrice'];
                        var totalPrice = response['totalPrice'];
                        var shipping = response['shipping'];
                        var percentage = response['percentage'];
                        if ((responseData != null) && (responseData == 'coupon code remove successfully')) {
                            if ((subTotalPrice != null) && (subTotalPrice != '')) {
                            $('.subTotal').text('$'+subTotalPrice);
                            $('.shipping').text('$'+shipping);
                            $('.total-amount').text('$'+totalPrice);
                            }
                            if((percentage != null) && (percentage != '')){
                            $('.couponDis').show();
                            $('.totaldis').text('$'+percentage);
                            }else{
                            $('.couponDis').hide();
                            $('.totaldis').text(''); 
                            }
                            $('.error-text').addClass('success');
                            $('.error-text').show().text('Coupon code remove successfully');
                            setTimeout(function(){ $('.error-text').hide().text(''); }, 3000);
                            $('#applyCoupon').attr('data-rc', 'add');
                            $('#applyCoupon').text('Apply Coupon');
                            $('#couponCode').val('');
                        } else {
                            if ((subTotalPrice != null) && (subTotalPrice != '')) {
                            $('.subTotal').text('$'+subTotalPrice);
                            $('.shipping').text('$'+shipping);
                            $('.total-amount').text('$'+totalPrice);
                            }
                            if((percentage != null) && (percentage != '')){
                            $('.couponDis').show();
                            $('.totaldis').text('$'+percentage);
                            }else{
                            $('.couponDis').hide(); 
                            $('.totaldis').text(''); 
                            }
                            $('.error-text').addClass('success');
                            $('.error-text').show().text('Coupon code remove successfully');
                            setTimeout(function(){ $('.error-text').hide().text(''); }, 3000);
                            $('#applyCoupon').attr('data-rc', 'add');
                            $('#applyCoupon').text('Apply Coupon');
                            $('#couponCode').val('');
                        }
                        localStorage.removeItem("coupon");
                    }
                });
            }else{
                var shippingValue = $('input[name=shipping]:checked').val(); 
                $.ajax({
                    url: baseUrl + "/applycoupon",
                    type: "POST",
                    data: { 
                        '_token': token, 
                        'couponCode': couponCode,
                        'shipping': shippingValue,
                    },
                    dataType: "JSON",
                    success: function(jsonStr) {
                        var res_data = JSON.stringify(jsonStr);
                        var response = JSON.parse(res_data);
                        var responseData = response['responseData'];
                        var subTotalPrice = response['subTotalPrice'];
                        var totalPrice = response['totalPrice'];
                        var shipping = response['shipping'];
                        var percentage = response['percentage'];
                        if ((responseData != null) && (responseData == 'coupon code apply successfully')) {
                            if ((subTotalPrice != null) && (subTotalPrice != '')) {
                            $('.subTotal').text('$'+subTotalPrice);
                            $('.shipping').text('$'+shipping);
                            $('.total-amount').text('$'+totalPrice);
                            }
                            if((percentage != null) && (percentage != '')){
                            $('.couponDis').show();
                            $('.totaldis').text('$'+percentage);
                            }else{
                            $('.couponDis').hide();
                            $('.totaldis').text(''); 
                            }
                            $('.error-text').addClass('success');
                            $('.error-text').show().text('Coupon code apply successfully');
                            setTimeout(function(){ $('.error-text').hide().text(''); }, 3000);
                            $('#applyCoupon').attr('data-rc', 'remove');
                            $('#applyCoupon').text('Remove Coupon');
                            localStorage.setItem("coupon", couponCode);
                        } else {
                            if ((subTotalPrice != null) && (subTotalPrice != '')) {
                            $('.subTotal').text('$'+subTotalPrice);
                            $('.shipping').text('$'+shipping);
                            $('.total-amount').text('$'+totalPrice);
                            }
                            if((percentage != null) && (percentage != '')){
                            $('.couponDis').show();
                            $('.totaldis').text('$'+percentage);
                            }else{
                            $('.couponDis').hide(); 
                            $('.totaldis').text(''); 
                            }
                            $('#couponCode').addClass('has-error');
                            $('#couponCodeError').show().text(responseData);
                            localStorage.removeItem("coupon");
                        }
                    }
                });
            }
        }
    });

    $(".shippingRate").change(function(){
        var shippingValue = $('input[name=shipping]:checked').val(); 
        var sTotal = '';
        var tTotal = '';
        if((shippingValue != null) && (shippingValue != '')){
            var subTotalPrice = $('.subTotal').text();
            var flatrate = $('#flatrate').val();
            var sPrice = subTotalPrice.replace("$", "");
            var shipping = flatrate.replace("$", "");
            var tPrice = parseFloat(sPrice) + parseFloat(shipping);
        }else{
            var subTotalPrice = $('.subTotal').text();
            var flatrate = $('#flatrate').val();
            var sPrice = subTotalPrice.replace("$", "");
            var shipping = flatrate.replace("$", "");
            var tPrice = parseFloat(sPrice);
        }
        $('.subTotal').text('$'+sPrice);
        $('.total-amount').text('$'+tPrice);
    });

    $('.logout').on('click', function(event) {
        event.preventDefault();
        $.ajax({
            url: baseUrl + "/userlogout",
            type: "POST",
            data: {
                '_token': token,
                'logout': 'User Logout'
            },
            dataType: "JSON",
            success: function(jsonStr) {
                var res_data = JSON.stringify(jsonStr);
                var response = JSON.parse(res_data);
                var responseData = response['responseData'];
                if (responseData == 'You have successfully logged out.') {
                    window.location.href = baseUrl+'/login-register';
                } else if(responseData == 'You are not authorized user'){
                    window.location.href = baseUrl+'/login-register';
                }else{
                    window.location.href = baseUrl;
                }
            }
        });
    });

    $(document).on('click', '.minicart-close', function(event){ 
        event.preventDefault();
        var getClass = $(this).parents('.minicart-inner').attr('class');
        if(getClass == 'minicart-inner show'){
            $('.minicart-inner').removeClass('show');
        }else{
            console.log(getClass);
        }   
    });    

    function viewCart(){
        var data = { '_token': token };
        var ajaxUrl  = baseUrl+"/viewcart";
        $.post(ajaxUrl, data, function(response) {
            $('.minicart-inner-content').html('').append(response);
        });
    }

});
</script>
</body>
</html>