<html>
<head>
   <title>Reset Your Password</title>
</head>
<body>
    <div style="background:#ebeced">
        <div style="background:#ebeced;padding:10px">
            <div style="background:#ffffff;width:80%;margin:auto">
                <center style="padding-top: 20px;">
                    <img src="http://www.nicheperfumeoils.com/images/logo.png" alt="Niche Perfume Logo" class="CToWUd" width="125" height="120" align="center">
                </center>
                <div style="font-family:Lato,sans-serif;color:#212e43;font-size:18px;font-weight:bold;text-align:center;padding:10px">Hi <?php echo e($username); ?>,</div>
                <hr style="border:1px solid #ebeced;width:500px">
                <div style="text-align:center;color:#212e43;font-family:Lato,sans-serif;font-size:16px;font-weight:400;padding:20px;line-height:1.8">
                    Follow this link to reset your password.<br>
                    <a href="<?php echo e($url); ?>" target='_blank'>Reset Password</a><br>
                </div>
                <div style="text-align:center;font-family:Lato,sans-serif;color:#212e43;font-size:16px;font-weight:400;padding:20px;line-height:1.8">
                    For any help or assistance, reach out to us anytime at <a href="mailto:demo@yourdomain.com" target="_blank">demo@yourdomain.com</a>
                </div>
                <hr style="border:1px solid #ebeced;width:600px">
                <div style="text-align:center;font-family:Lato,sans-serif;font-size:16px;font-weight:500;font-style:italic;color:#a9a9a9;line-height:1.8">Kind Regards,<br>
                    <div style="text-align:center;font-family:Lato,sans-serif;color:#212e43;font-size:16px;font-weight:bolder;padding-bottom: 15px;">Jewellery Ecommerce</div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>