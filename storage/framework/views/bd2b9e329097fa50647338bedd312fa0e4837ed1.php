<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td><?php echo e($bladeVar['result']->_id); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Name</th>
		    		<td><?php echo e($bladeVar['result']->name); ?></td>
		    	</tr>
		    </tbody>
    	</table>
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="<?php echo e($bladeVar['result']->deleted_at == null ? route('product_category.edit', $bladeVar['result']->_id) : ''); ?>" 
	    	data-destroy="<?php echo e($bladeVar['result']->deleted_at == null ? route('product_category.destroy', $bladeVar['result']->_id) : ''); ?>" 
	    	data-restore="<?php echo e($bladeVar['result']->deleted_at != null ? route('product_category.restore', $bladeVar['result']->_id) : ''); ?>"
	    	data-delete="<?php echo e(route('product_category.delete', $bladeVar['result']->_id)); ?>">
	    </span>
	</div>
</div>	    