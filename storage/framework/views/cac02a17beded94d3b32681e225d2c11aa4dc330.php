<div class="container">
    <div class="row mt-3">
      <div class="col text-center">
        <small>COPYRIGHTS &copy; <?php echo e(date('Y')); ?>, ALL RIGHTS RESERVED</small>
      </div>
    </div>
</div>
