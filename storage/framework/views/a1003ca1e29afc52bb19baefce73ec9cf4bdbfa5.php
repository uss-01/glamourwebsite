<div class="row">
	<div class="col-12">
		<form action="<?php echo e(route('product_subcategory.update', $bladeVar['result']->_id)); ?>" method="POST">
			<?php echo e(method_field('PUT')); ?>

			<div class="form-group">
				<label class="form-control-label" for="code"><strong>Category</strong></label>
				<select name="product_category" class="form-control">
					<option value=""<?php echo e(empty(old('product_category')) ? ' selected="selected"' : ''); ?>></option>
				<?php $__currentLoopData = $bladeVar['product_categories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product_category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<option value="<?php echo e($product_category->_id); ?>"<?php echo e($product_category->_id == old('product_category', $bladeVar['result']->product_category_id) ? ' selected="selected"' : ''); ?>><?php echo e($product_category->name); ?></option>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Name</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="name" value="<?php echo e(old('name', $bladeVar['result']->name)); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
            <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
      	</form>
    </div>
</div>