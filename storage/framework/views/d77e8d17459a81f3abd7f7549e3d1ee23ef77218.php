<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td><?php echo e($bladeVar['result']->_id); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Name</th>
		    		<td><?php echo e($bladeVar['result']->name); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Category</th>
		    		<td><?php echo e($bladeVar['result']->blog_category->name); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Tag</th>
		    		<td><?php echo e($bladeVar['result']->blog_tag->name); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Image</th>
		    		<td><img src="<?php echo e(asset('images/blogs').'/'.$bladeVar['result']->image); ?>" class="thumbnail" width="150" /></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Status</th>
		    		<td><?php echo e($bladeVar['result']->blog_status); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Details</th>
		    		<td><?php echo $bladeVar['result']->details; ?></td>
		    	</tr>
		    </tbody>
    	</table>
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="<?php echo e($bladeVar['result']->deleted_at == null ? route('blogs.edit', $bladeVar['result']->_id) : ''); ?>" 
	    	data-destroy="<?php echo e($bladeVar['result']->deleted_at == null ? route('blogs.destroy', $bladeVar['result']->_id) : ''); ?>" 
	    	data-restore="<?php echo e($bladeVar['result']->deleted_at != null ? route('blogs.restore', $bladeVar['result']->_id) : ''); ?>"
	    	data-delete="<?php echo e(route('blogs.delete', $bladeVar['result']->_id)); ?>">
	    </span>
	</div>
</div>	    