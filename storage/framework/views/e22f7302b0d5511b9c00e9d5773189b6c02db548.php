<div class="row">
	<div class="col-12">
		<form action="<?php echo e(route('tenant.store')); ?>" enctype="multipart/form-data" method="POST" id="tenant_form">
		  <div class="row">
			<div class="col-6">
				<div class="form-group">
					<label class="form-control-label" for="title"><strong>Name</strong> <small class="text-danger">(required)</small></label>
					<input type="text" class="form-control" name="name" value="<?php echo e(old('name')); ?>" autofocus>
					<small class="form-control-feedback"></small>
				</div>
			</div>
			<div class="col-6">
				<div class="form-group">
					<label class="form-control-label" for="title"><strong>Phone</strong>
					 <small class="text-danger">(required)</small>
					</label>
					<input type="text" class="form-control" name="phone" value="<?php echo e(old('phone')); ?>" autofocus>
					<small class="form-control-feedback"></small>
				</div>
			</div>
			
			</div>
		  <div class="row">
			<div class="col-6">
				<div class="form-group">
					<label class="form-control-label" for="title"><strong>Email</strong>
					 <small class="text-danger">(required)</small>
					</label>
					<input type="text" class="form-control" name="email" value="<?php echo e(old('email')); ?>" autofocus>
					<small class="form-control-feedback"></small>
				</div>
			</div>
			<div class="col-6">
				<div class="form-group">
					<label class="form-control-label" for="title"><strong>Address</strong>
					 <small class="text-danger"></small>
					</label>
					<input type="text" class="form-control" name="address" value="<?php echo e(old('address')); ?>" autofocus>
					<small class="form-control-feedback"></small>
				</div>
			</div>
			<div class="col-6">
				<div class="form-group">
					<label class="form-control-label" for="password"><strong>Password</strong>
					 <small class="text-danger">(required)</small>
					</label>
					<input type="password" class="form-control" name="password" value="<?php echo e(old('password')); ?>" autofocus>
					<small class="form-control-feedback"></small>
				</div>
			</div>
			<div class="col-6">
				<div class="form-group">
					<label class="form-control-label" for="password_confirmation"><strong>Confirm Password</strong>
					 <small class="text-danger">(required)</small>
					</label>
					<input type="password" class="form-control" name="password_confirmation" value="<?php echo e(old('password_confirmation')); ?>" autofocus>
					<small class="form-control-feedback"></small>
				</div>
			</div>
			</div>
			<div class="row">
			<div class="col-6">
				<div class="form-group">
					<label class="form-control-label" for="start_date"><strong>Start Date</strong></label>
					<input type="text" class="form-control bg-ffffff" name="start_date" value="<?php echo e(old('start_date')); ?>" readonly>
					<input type="hidden" name="start_date_alt">
					<small class="form-control-feedback"></small>
				</div>
			</div>
			<div class="col-6">
				<div class="form-group">
					<label class="form-control-label" for="end_date"><strong>End Date</strong></label>
					<input type="text" class="form-control bg-ffffff" name="end_date" value="<?php echo e(old('end_date')); ?>" readonly>
					<input type="hidden" name="end_date_alt">
					<small class="form-control-feedback"></small>
				</div>
			</div>
			
			<div class="col-6">
				<div class="form-group">
					<label for="type" class="form-control-label"><strong>Shops</strong></label>
					<select class="form-control" name="shop_name[]" id="shop_name" multiple="multiple">
						<option value=""<?php echo e(empty(old('shop_name')) ? ' selected="selected"' : ''); ?>></option>
						<?php $__currentLoopData = $bladeVar['shopData']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $shop): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<option value="<?php echo e($shop->_id); ?>"<?php echo e($shop->_id == old('shop_name') ? ' selected="selected"' : ''); ?>><?php echo e($shop->name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>
				</div>
			</div>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="image"><strong>Image</strong> <small class="text-danger">(required)</small></label>
				<input type="file" name="image" class="form-control">
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="details"><strong>Details</strong></label>
				<textarea class="form-control" id="htmlTextEditor" name="details"><?php echo e(old('details')); ?></textarea>
				<small class="form-control-feedback"></small>
			</div>
			 <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
			
        
		</form>
	</div>
</div>
<script type="text/javascript">
     $(document).ready(function() {
	$('#htmlTextEditor').summernote({
              height:300,
              dialogsInBody: true
            });
  });
</script>