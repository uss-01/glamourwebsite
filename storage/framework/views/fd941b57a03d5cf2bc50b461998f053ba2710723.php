<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td><?php echo e($bladeVar['result']->_id); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Page Name</th>
		    		<td><?php echo e($bladeVar['result']->name); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Page Title</th>
		    		<td><?php echo e($bladeVar['result']->title); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Slug</th>
		    		<td><?php echo e($bladeVar['result']->slug); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Image</th>
		    		<td><?php if($bladeVar['result']->image): ?>
						<img src="<?php echo e(asset('images/cms').'/'.$bladeVar['result']->image); ?>" class="thumbnail" width="150" />
						<?php else: ?>
						No Image
						<?php endif; ?>
		    		</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Content</th>
		    		<td><?php echo $bladeVar['result']->content; ?></td>
		    	</tr>
		    	
		    	
		    </tbody>
    	</table>
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="<?php echo e($bladeVar['result']->deleted_at == null ? route('cms.edit', $bladeVar['result']->_id) : ''); ?>" 
	    	data-destroy="<?php echo e($bladeVar['result']->deleted_at == null ? route('cms.destroy', $bladeVar['result']->_id) : ''); ?>" 
	    	data-restore="<?php echo e($bladeVar['result']->deleted_at != null ? route('cms.restore', $bladeVar['result']->_id) : ''); ?>"
	    	data-delete="<?php echo e(route('cms.delete', $bladeVar['result']->_id)); ?>">
	    </span>
	</div>
</div>	    