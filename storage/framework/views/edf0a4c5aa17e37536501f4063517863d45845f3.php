<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td><?php echo e($bladeVar['result']->_id); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Name</th>
		    		<td><?php echo e($bladeVar['result']->name); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Price</th>
		    		<td>$ <?php echo e($bladeVar['result']->price); ?></td>
				</tr>
				<tr>
		    		<th class="bg-faded">Discounted Price</th>
		    		<td>$ <?php echo e($bladeVar['result']->discounted_price); ?></td>
				</tr>
				<tr>
		    		<th class="bg-faded">Quantity</th>
		    		<td><?php echo e($bladeVar['result']->product_quantity); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Brand</th>
		    		<td> <?php echo e($bladeVar['result']->product_brand->name); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Category</th>
		    		<td><?php echo e($bladeVar['result']->product_category->name); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Sub Category</th>
		    		<td><?php echo e($bladeVar['result']->product_subcategory->name); ?></td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Color</th>
		    		<td> <?php echo e($bladeVar['result']->product_color->name); ?></td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Size</th>
		    		<td> <?php echo e($bladeVar['result']->product_size->name); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Image</th>
		    		<td><img src="<?php echo e(asset('images/products').'/'.$bladeVar['result']->image); ?>" class="thumbnail" width="150" /></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Status</th>
		    		<td><?php echo e($bladeVar['result']->product_status); ?></td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Sort Details</th>
		    		<td><?php echo e($bladeVar['result']->sort_details); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Details</th>
		    		<td><?php echo e($bladeVar['result']->details); ?></td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Variations</th>
		    		<td>
                    <?php $variation_data = json_decode($bladeVar['result']->variations); ?>
                    <?php $__currentLoopData = $variation_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $variation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php if($variation->color): ?>
							<?php $__currentLoopData = $bladeVar['product_colors']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<?php if($color->id == $variation->color): ?>
									Color :  <?php echo e($color->name); ?><br>
								<?php endif; ?>	
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
						<?php endif; ?>
						<?php if($variation->size): ?>
							<?php $__currentLoopData = $bladeVar['product_sizes']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $size): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<?php if($size->id == $variation->size): ?>
									Size :  <?php echo e($size->name); ?><br>
								<?php endif; ?>	
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
						<?php endif; ?>
						<?php if($variation->price): ?>
							Price :  <?php echo e($variation->price); ?><br>
						<?php endif; ?>
						<?php if($variation->discountedPrice): ?>
							Discounted Price :  <?php echo e($variation->discountedPrice); ?><br>
						<?php endif; ?>
						<?php if($variation->quantity): ?>
							Quantity :  <?php echo e($variation->quantity); ?><br>
						<?php endif; ?>
						<?php if($variation->images): ?>
							Images  :   <div class="thumbnail">
											<img src="<?php echo e(asset('images/products').'/'.$variation->images); ?>" width="100" />
										</div><br>	
						<?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		    		</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Attributes</th>
		    		<td>
                    <?php $attribute_data = json_decode($bladeVar['result']->attribute); ?>
                    <?php $__currentLoopData = $attribute_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $attribute): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($attribute->value !=null): ?>
                    <?php echo e($attribute->name); ?> :  <?php echo e($attribute->value); ?><br>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		    		</td>
		    	</tr>
		    	
		    </tbody>
    	</table>
    	
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="<?php echo e($bladeVar['result']->deleted_at == null ? route('product.edit', $bladeVar['result']->_id) : ''); ?>" 
	    	data-destroy="<?php echo e($bladeVar['result']->deleted_at == null ? route('product.destroy', $bladeVar['result']->_id) : ''); ?>" 
	    	data-restore="<?php echo e($bladeVar['result']->deleted_at != null ? route('product.restore', $bladeVar['result']->_id) : ''); ?>"
	    	data-delete="<?php echo e(route('product.delete', $bladeVar['result']->_id)); ?>">
	    </span>
	</div>
</div>	    