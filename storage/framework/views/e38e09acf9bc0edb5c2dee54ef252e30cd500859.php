<div class="row">
	<div class="col-12">
		<form action="<?php echo e(route('coupons.update', $bladeVar['result']->id)); ?>" enctype="multipart/form-data" method="POST" id="brand_form">
			<?php echo e(method_field('PUT')); ?>

			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Coupon Name</strong><small class="text-danger"> (required)</small></label>
				<input type="text" class="form-control" name="name" value="<?php echo e(old('name', $bladeVar['result']->name)); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="product_status"><strong>Coupon Type</strong></label>
				<select name="type" class="form-control">
					<option value=""<?php echo e(empty(old('type')) ? ' selected="selected"' : ''); ?>></option>
					<?php $__currentLoopData = $bladeVar['couponType']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<option value="<?php echo e($type); ?>"<?php echo e($type == old('type', $bladeVar['result']->type) ? ' selected="selected"' : ''); ?>><?php echo e($type); ?></option>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="value"><strong>Coupon Value</strong></label>
				<input type="text" class="form-control" name="value" value="<?php echo e(old('value', $bladeVar['result']->value)); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="quantity"><strong>Coupon Quantity</strong></label>
				<input type="text" class="form-control" name="quantity" value="<?php echo e(old('quantity', $bladeVar['result']->quantity)); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="expiry"><strong>Coupon Expiry</strong></label>
				<input type="text" class="form-control" name="expiry" value="<?php echo e(old('expiry', $bladeVar['result']->expiry)); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			 <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div> 
		</form>
	</div>
</div>
<script type="text/javascript">
$('#expiry').datetimepicker();
</script> 