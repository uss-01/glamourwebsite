<?php $__env->startSection('head'); ?>
<link type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"/>
<link type="text/css" href="<?php echo e(asset('thirdparty/timepicker/jquery-ui-timepicker-addon.min.css')); ?>" rel="stylesheet"/>
<link type="text/css" href="<?php echo e(asset('thirdparty/editor/summernote.css')); ?>" rel="stylesheet"/>
<style>
.complaints[data-count]:after{
	position: absolute;
	right: -10%;
	top: -10%;
	content: attr(data-count);
	font-size: 20%;
	padding: 3em;
	border-radius: 999px;
	line-height: 1.75em;
	color: white;
	background: rgba(0, 255, 85, 0.85);
	text-align: center;
	min-width: 2em;
	font-weight: bold;
	left: 50%;
}
.complaints{
	position: relative;
    display: inline-block;
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid" id="pageContainer">
    <div class="row" id="pageContent">
        <div class="col-12 col-md-8 offset-md-2">
            <div class="float-right mt-2 hidden-xs-down">
                <a href="<?php echo e(route('tenant.create')); ?>" class="btn btn-success" role="button" title="Create New category" data-toggle="modal" data-target="#createModal"><i class="fa fa-plus"></i><span class="hidden-sm-down"> Create New</span></a>
                <a href="<?php echo e(route('tenant.index')); ?>" class="btn btn-secondary btn-search" title="Search category" data-search="<?php echo e(!empty(Request::input('q')) ? 'yes' : ''); ?>"><i class="fa fa-search"></i><span<?php echo !empty(Request::input('q')) ? '' : ' class="hidden-xs-up"'; ?>> Close</span></a>
                <a href="<?php echo e(route('tenant.index')); ?>" class="btn btn-secondary" role="button" title="Refresh offer List"><i class="fa fa-refresh"></i></a>
            </div>
            <div class="float-right mt-1 hidden-sm-up">
                <a href="<?php echo e(route('tenant.create')); ?>" class="btn btn-success btn-sm" role="button" title="Create New category" data-toggle="modal" data-target="#createModal"><i class="fa fa-plus"></i></a>
                <a href="<?php echo e(route('tenant.index')); ?>" class="btn btn-secondary btn-sm btn-search" title="Search category" data-search="<?php echo e(!empty(Request::input('q')) ? 'yes' : ''); ?>"><i class="fa fa-search"></i><span<?php echo !empty(Request::input('q')) ? '' : ' class="hidden-xs-up"'; ?>> Close</span></a>
                <a href="<?php echo e(route('tenant.index')); ?>" class="btn btn-secondary btn-sm" role="button" title="Refresh offer List"><i class="fa fa-refresh"></i></a>
            </div>
            <h1 class="hidden-xs-down"><?php echo e(Request::input('q') ? 'Search ' : ''); ?>Tenants <span class="text-muted h5">(total <?php echo e(number_format($bladeVar['total_count'])); ?>)</span></h1>
            <h1 class="hidden-sm-up h3"><?php echo e(Request::input('q') ? 'Search ' : ''); ?>Tenants <br><span class="text-muted h6">(total <?php echo e(number_format($bladeVar['total_count'])); ?>)</span></h1>
            <div class="clearfix alertArea"></div>
            <div class="card mb-2 searchContainer<?php echo e(Request::input('q') ? '' : ' hidden-xs-up'); ?>">
                <div class="card-block">
                    <form method="get" action="<?php echo e(route('tenant.search')); ?>" class="m0 p0 form-search">
                        <div class="form-group mb-0">
                            <label class="form-control-label" for="q"><strong>Search by tenant name</strong></label>
                            <input type="text" class="form-control" name="q" value="<?php echo e(Request::input('q')); ?>">
                            <small class="form-text text-muted">Type offer name and hit "Enter" key to search</small>
                        </div>
                    </form>
                </div>
            </div>
            <?php if($bladeVar['total_count'] > 0): ?>
            <table class="table table-bordered table-hover table-sm">
                <thead class="bg-faded">
                    <tr>
                        <th class="text-center" style="width: 4rem;"><nobr><a href="<?php echo e(url()->current()); ?>?<?php echo e(Request::input('q') ? 'q='.Request::input('q').'&' : ''); ?>sort=id&dir=<?php echo e(Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'desc' : 'asc' : 'asc'); ?>">Id</a> <i class="fa fa-<?php echo e(Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'sort-numeric-asc' : 'sort-numeric-desc' : 'sort'); ?>"></i></nobr></th>
                        <th><nobr>
                            <a href="<?php echo e(url()->current()); ?>?<?php echo e(Request::input('q') ? 'q='.Request::input('q').'&' : ''); ?>sort=title&dir=<?php echo e(Request::input('sort') == 'title' ? Request::input('dir') == 'asc' ? 'desc' : 'asc' : 'asc'); ?>">UID</a>
                         <i class="fa fa-<?php echo e(Request::input('sort') == 'title' ? Request::input('dir') == 'asc' ? 'sort-alpha-asc' : 'sort-alpha-desc' : 'sort'); ?>">
                             
                         </i>
                     </nobr>
                     </th>
                        <th><nobr>Name</nobr></th>
                        <th><nobr>Shops</nobr></th>
                        <th><nobr>Phone No.</nobr></th>
                        <th><nobr>Email</nobr></th>
                        <th class="w-td-action">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $bladeVar['results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr<?php echo $rs->deleted_at != null ? ' class="table-danger"' : ''; ?>>
                    <td class="text-center"><span<?php echo $rs->deleted_at != null ? ' class="badge badge-danger"' : ''; ?>><?php echo e($rs->id); ?></span></td>
                    <td><a href="<?php echo e(route('tenant.show', $rs->id)); ?>" title="View" data-toggle="modal" data-target="#viewModal"><?php echo e($rs->uid); ?></a></td>
                    <td><?php echo e($rs->name); ?></td>
                    <td>
                    <?php $shopsName = json_decode($rs->shop_name, true); ?>
					<?php $__currentLoopData = $shopsName; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shopname): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					   <?php echo e($shopname['name']); ?><br />
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </td>
                    <td><?php echo e($rs->phone); ?></td>
                    <td><?php echo e($rs->email); ?></td>
                    <td class="text-center text-sm-left">
                        <div class="hidden-xs-down">
                            <a href="<?php echo e(route('tenant.show', $rs->id)); ?>" class="btn btn-sm btn-info" title="View" data-toggle="modal" data-target="#viewModal"><i class="fa fa-eye"></i></a> 
                            <a href="<?php echo e(route('tenant.edit', $rs->id)); ?>" class="btn btn-sm btn-info<?php echo e($rs->deleted_at != null ? ' disabled' : ''); ?>" title="Edit" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i></a>
                            <a href="<?php echo e(route('tenant.complaints', $rs->id)); ?>" class="btn btn-sm btn-info complaints" data-count="" title="Complaints"><i class="fa fa-envelope"></i></a>
                            <?php if($rs->deleted_at != null): ?>
                            <a href="<?php echo e(route('tenant.restore', $rs->id)); ?>" class="btn btn-sm btn-success btn-restore" title="Restore"><i class="fa fa-undo"></i></a>
                            <?php else: ?>
                            <a href="<?php echo e(route('tenant.destroy', $rs->id)); ?>" class="btn btn-sm btn-warning btn-destroy" title="Delete temporarly"><i class="fa fa-trash"></i></a>
                            <?php endif; ?>
                            <a href="<?php echo e(route('tenant.delete', $rs->id)); ?>" class="btn btn-sm btn-danger btn-delete" title="Delete permanently"><i class="fa fa-trash"></i></a>
                        </div>
                        <div class="btn-group hidden-sm-up">
                            <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="<?php echo e(route('tenant.show', $rs->id)); ?>" class="dropdown-item" data-toggle="modal" data-target="#viewModal"><i class="fa fa-eye text-info"></i> View</a>
                                <a href="<?php echo e(route('tenant.edit', $rs->id)); ?>" class="dropdown-item<?php echo e($rs->deleted_at != null ? ' disabled' : ''); ?>" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil text-<?php echo e($rs->deleted_at != null ? 'muted' : 'info'); ?>"></i> Edit</a> 
                                <a href="<?php echo e(route('tenant.complaints', $rs->id)); ?>" class="dropdown-item" data-count=""><i class="fa fa-envelope"></i> Complaints</a>
                                <?php if($rs->deleted_at != null): ?>
                                <a href="<?php echo e(route('tenant.restore', $rs->id)); ?>" class="dropdown-item btn-restore"><i class="fa fa-undo text-success"></i> Restore</a>
                                <?php else: ?>
                                <a href="<?php echo e(route('tenant.destroy', $rs->id)); ?>" class="dropdown-item btn-destroy"><i class="fa fa-trash text-warning"></i> Delete temporarly</a>
                                <?php endif; ?>
                                <a href="<?php echo e(route('tenant.delete', $rs->id)); ?>" class="dropdown-item btn-delete"><i class="fa fa-trash text-danger"></i> Delete permanently</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
        <div class="mt-4">
            <?php echo e($bladeVar['results']->appends(Request::query())->links()); ?>

            <small class="d-block text-center text-muted">(showing 20 results per page)</small>
        </div>
        <?php else: ?>
        <div class="alert alert-success">
            <strong>No records found!</strong>
            <?php if(!empty(Request::input('q'))): ?>
            No matching your search criteria. 
            <?php else: ?> 
            No Offers added yet, please click on "Create New" button to add a offer. 
            <?php endif; ?>
        </div>
        <?php endif; ?>
        <!-- Deletetion code with Form -->
        <form class="formDelete">
            <?php echo e(method_field('DELETE')); ?>

        </form>

        <!-- View modal code: START -->
        <div class="modal" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="viewModalTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="viewModalTitle">View Tenant</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                    </div>
                    <div class="modal-footer hidden-xs-up">
                        <a href="javascript:void(0);" class="btn btn-info btn-edit" title="Edit" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i> Edit</a>
                        <a href="javascript:void(0);" class="btn btn-warning btn-destroy" title="Delete temporarly"><i class="fa fa-trash"></i></a>
                        <a href="javascript:void(0);" class="btn btn-success btn-restore" title="Restore"><i class="fa fa-undo"></i></a>
                        <a href="javascript:void(0);" class="btn btn-danger btn-delete" title="Delete permanently"><i class="fa fa-trash"></i></a>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- View modal code: END -->

        <!-- Edit modal code: START -->
        <div class="modal" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editModalTitle">Edit Tenant</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                    </div>
                    <div class="modal-footer hidden-xs-up">
                     <!--  <button type="button" class="btn btn-success btn-submit">Submit</button> -->
                     <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                 </div>
             </div>
         </div>
     </div>
     <!-- Edit modal code: END -->

     <!-- Add modal code: START -->
     <div class="modal" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createModalTitle">Add Tenant</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                </div>
                <div class="modal-footer hidden-xs-up">
                  <!--   <button type="button" class="btn btn-success btn-submit">Submit</button> -->
                  <!--   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
              </div>
          </div>
      </div>
  </div>
  <!-- Add modal code: END -->
</div>
</div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('foot'); ?>
<script type="text/javascript">
    var pageUrl = '<?php echo e(route('tenant.index')); ?>';
    var ajaxLoadUrl = '<?php echo e(url()->full()); ?>';
    var pageContainer = '#pageContainer';
    var pageContent = '#pageContent';
</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src="<?php echo e(asset('thirdparty/timepicker/jquery-ui-timepicker-addon.min.js')); ?>"></script>
<script src="<?php echo e(asset('thirdparty/editor/summernote.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.form.js')); ?>"></script>
<script src="<?php echo e(asset('js/tenant.js?v=1')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>