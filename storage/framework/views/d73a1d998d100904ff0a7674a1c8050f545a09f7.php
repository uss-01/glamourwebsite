<?php $__env->startSection('content'); ?>
<div class="container">
	<?php if(session('flushcache')): ?>
	    <div class="alert alert-ajax alert-success fade show mb-2">
	    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <?php echo session('flushcache'); ?>

	    </div>
	<?php endif; ?>

	<div class="jumbotron" style="margin-top: 13%; margin-bottom: 13%;">
		<h1 class="text-center display-4">
			<span class="text-muted d-block">Signed in as <?php echo e(Auth::user()->name); ?></span>
		</h1>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>