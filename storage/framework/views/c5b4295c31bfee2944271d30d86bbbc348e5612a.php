<?php $__env->startSection('header'); ?><?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- Styles -->
<style>
    .title {
        font-size: 50px;
        text-align: center;
    }
    .m-b-md {
        margin-bottom: 30px;
    }
</style>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <img src="<?php echo e(asset('images/logo.png')); ?>" alt="" width="97px" class="mx-auto d-block img-fluid mb-4">
         <div class="title m-b-md">
            You cannot access this page! <br />This is for only '<?php echo e($role); ?>'
		  </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
<?php $__env->startComponent('auth.footer'); ?><?php echo $__env->renderComponent(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.sellersapp', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>