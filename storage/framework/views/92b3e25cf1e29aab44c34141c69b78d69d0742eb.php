<div class="row">
	<div class="col-12">
		<form action="<?php echo e(route('shop.update', $bladeVar['result']->_id)); ?>" method="POST">
			<?php echo e(method_field('PUT')); ?>

			<div class="row">
		<div class="col-12">
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>beaconId</strong></label>
				<input type="text" class="form-control" name="beacon_id" value="<?php echo e(old('name', $bladeVar['result']->beacon_id)); ?>" autofocus>
				<input type="hidden" name="category" value="1">
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Tenant UID</strong></label>
				<input type="text" class="form-control" name="tenant_uid" value="<?php echo e(old('name', $bladeVar['result']->tenant_uid)); ?>" autofocus>
				<input type="hidden" name="category" value="1">
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Shop name</strong></label>
				<input type="text" class="form-control" name="name" value="<?php echo e(old('name', $bladeVar['result']->name)); ?>" autofocus>
				<input type="hidden" name="category" value="1">
				<small class="form-control-feedback"></small>
			</div>
           </div>
           
			</div>
			<div class="row">
		<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="Phone"><strong>Store Category</strong></label>
				<select name="store_category" class="form-control">
					<option value=""<?php echo e(empty(old('store_category')) ? ' selected="selected"' : ''); ?>></option>
				<?php $__currentLoopData = $bladeVar['store_categories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $store_category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<option value="<?php echo e($store_category->_id); ?>"<?php echo e($store_category->_id == old('store_category',$bladeVar['result']->store_category_id) ? ' selected="selected"' : ''); ?>><?php echo e($store_category->name); ?></option>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>
				<div class="other_option_wrap"></div>
				<small class="form-control-feedback"></small>
			</div>
			</div>
            <div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="store_subcategory"><strong>Store Subcategory</strong></label>
				<select name="store_subcategory" class="form-control">
					<option value=""<?php echo e(empty(old('store_subcategory')) ? ' selected="selected"' : ''); ?>></option>
					<?php $__currentLoopData = $bladeVar['store_subcategories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $store_subcategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<option value="<?php echo e($store_subcategory->_id); ?>"<?php echo e($store_subcategory->_id == old('store_subcategory',$bladeVar['result']->store_subcategory_id) ? ' selected="selected"' : ''); ?>><?php echo e($store_subcategory->name); ?></option>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>
				<div class="other_option_wrap2"></div>
				<small class="form-control-feedback"></small>
			</div>
			</div>
			</div>
            <div class="row">
		<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="store"><strong>Store</strong></label>
				<select name="store" class="form-control">
					<option value=""<?php echo e(empty(old('store')) ? ' selected="selected"' : ''); ?>></option>
				<?php $__currentLoopData = $bladeVar['stores']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<option value="<?php echo e($store->store_id); ?>"<?php echo e($store->store_id == old('store', $bladeVar['result']->store_id) ? ' selected="selected"' : ''); ?>><?php echo e($store->name); ?></option>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>
				<small class="form-control-feedback"></small>
			</div>
            </div>
            <div class="col-6">

			<div class="form-group">
				<label class="form-control-label" for="email"><strong>Email</strong></label>
				<input type="text" class="form-control" name="email" value="<?php echo e(old('email', $bladeVar['result']->email)); ?>">
				<small class="form-control-feedback"></small>
			</div>
            </div>
            </div>
             <div class="row">
		<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="phone"><strong>Phone</strong></label>
				<input type="text" class="form-control" name="phone" value="<?php echo e(old('phone', $bladeVar['result']->phone)); ?>">
				<small class="form-control-feedback"></small>
			</div>
			</div>
			<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="website"><strong>Website</strong></label>
				<input type="text" class="form-control" name="website" value="<?php echo e(old('website', $bladeVar['result']->website)); ?>">
				<small class="form-control-feedback"></small>
			</div>
			</div>
			</div>

			<div class="row">
				<div class="col-6">
				<div class="form-group">
					<label class="form-control-label" for="Latitude"><strong>Latitude</strong></label>
					<input type="text" class="form-control" name="latitude" value="<?php echo e(old('latitude', $bladeVar['result']->latitude)); ?>">
					<small class="form-control-feedback"></small>
				</div>
				</div>
				<div class="col-6">
				<div class="form-group">
					<label class="form-control-label" for="Longitude"><strong>Longitude</strong></label>
					<input type="text" class="form-control" name="longitude" value="<?php echo e(old('longitude', $bladeVar['result']->longitude)); ?>">
					<small class="form-control-feedback"></small>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-6">
					<div class="form-group">
						<label class="form-control-label" for="floorlevel"><strong>Floor Level</strong></label>
						<select name="floorlevel" class="form-control">
						    <option value="<?php echo e(old('floorlevel', $bladeVar['result']->floorlevel)); ?>"><?php echo e(old('floorlevel', $bladeVar['result']->floorlevel)); ?></option>
							<option value="3">Third Level</option>
							<option value="2">Second Level</option>
							<option value="1">First Level</option>
							<option value="0">Ground Level</option>
							<option value="-1">Basement 1</option>
							<option value="-2">Basement 2</option>
						</select>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label class="form-control-label" for="lease_year"><strong>Lease Year</strong></label>
						<input type="text" class="form-control bg-ffffff" name="lease_year" value="<?php echo e(old('lease_year', $bladeVar['result']->lease_year)); ?>" readonly>
						<input type="hidden" name="lease_year_alt">
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label class="form-control-label" for="image"><strong>Shop Logo</strong> <small class="text-danger">(required)</small></label>
						<input type="file" name="image" class="form-control">
						<small class="form-control-feedback"></small>
					</div>
			    </div>
			</div>

			<div class="form-group">
				<label class="form-control-label" for="address"><strong>Address</strong></label>
				<textarea class="form-control" name="address"><?php echo e(old('address', $bladeVar['result']->address)); ?></textarea>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="details"><strong>Details</strong></label>
				<textarea class="form-control" id="htmlTextEditor" name="details"><?php echo e(old('details', $bladeVar['result']->details)); ?></textarea>
				<small class="form-control-feedback"></small>
			</div>
			<div class="row">
				<div class="col-12">
					<label class="custom-control custom-checkbox pull-right">
						<input type="checkbox" class="custom-control-input" name="selectall">
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">Select All</span>
					</label>
					<label class="form-control-label" for="city"><strong>Brands For the Shop</strong></label>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
				<?php $__currentLoopData = $bladeVar['brands']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div class="form-group col-12 col-sm-3 mb-0 pr-0">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" name="brand[]" value="<?php echo e($brand->brand_id); ?>" <?php echo in_array($brand->brand_id, $bladeVar['selectedBrands']) ? ' checked' : ''; ?>>
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><?php echo e($brand->name); ?></span>
						</label>
					</div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<div class="clearfix"></div>
				<div class="col-12">
					<small class="form-control-feedback"></small>
				</div>
			</div>
			 <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
			
      	</form>
    </div>
</div>
<script type="text/javascript">
     $(document).ready(function() {
	$('#htmlTextEditor').summernote({
              height:200,
              dialogsInBody: true
            });
  });
</script>