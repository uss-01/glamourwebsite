<?php $__env->startSection('header'); ?><?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container">
  <div class="row">
    <div class="col col-sm-4 col-md-6 col-lg-4 offset-sm-4 offset-md-3 offset-lg-4 mt-4 mt-sm-5"> <img src="<?php echo e(asset('images/logo.png')); ?>" alt="Tawar Mall" width="97px" class="mx-auto d-block img-fluid mb-4">
      <h1 class="h3"><strong>Forgot your password?</strong></h1>
      <div class="px-1"> <small>Enter your email address to reset your password.</small> </div>
      <div class="card p-4 my-4"> <?php if(session('status')): ?>
        <div class="alert alert-success" role="alert"> <?php echo e(session('status')); ?> </div>
        <?php endif; ?>
        <form action="<?php echo e(route('password.email')); ?>" method="post">
          <?php echo e(csrf_field()); ?>

          <div class="form-group<?php echo e($errors->has('email') ? ' has-danger' : ''); ?>">
            <label class="form-control-label" for="email"><strong>Email address</strong></label>
            <input type="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>" tabindex="1" required autofocus>
            <?php if($errors->has('email')): ?> <small class="form-control-feedback"><?php echo e($errors->first('email')); ?></small> <?php endif; ?> </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
            <small>or go back to <a href="<?php echo e(route('login')); ?>">login</a></small> </div>
        </form>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
<?php $__env->startComponent('auth.footer'); ?><?php echo $__env->renderComponent(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>