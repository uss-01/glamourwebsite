
<nav aria-label="Page navigation">
    <ul class="pagination justify-content-center pagination-sm mb-2">
        
        
        <li class="page-item<?php echo e(($paginator->onFirstPage() ? ' disabled' : '')); ?>"><a href="<?php echo e($paginator->previousPageUrl()); ?>" rel="prev" class="page-link" title="Previous">&larr; <span class="hidden-sm-down">Previous</span></a></li>

        
        <?php $__currentLoopData = $elements; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $element): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            
            <?php if(is_string($element)): ?>
                <li class="page-item disabled"><a href="javascript:void(0);" class="page-link"><?php echo e($element); ?></a></li>
            <?php endif; ?>

            
            <?php if(is_array($element)): ?>
                <?php $__currentLoopData = $element; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page => $url): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li class="page-item<?php echo e(($page == $paginator->currentPage() ? ' active' : '')); ?>">
                        <a href="<?php echo e($url); ?>" class="page-link">
                            <?php echo e($page); ?>

                            <?php if($page == $paginator->currentPage()): ?>
                                <span class="sr-only">(current)</span>
                            <?php endif; ?>
                        </a>
                    </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        
        <li class="page-item<?php echo e(($paginator->hasMorePages() ? '' : ' disabled')); ?>"><a href="<?php echo e($paginator->nextPageUrl()); ?>" rel="next" class="page-link" title="Next"><span class="hidden-sm-down">Next</span> &rarr;</a></li>
    </ul>
</nav>
