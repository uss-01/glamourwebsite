<?php echo $__env->make('frontend.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<style type="text/css">
.addWishlist{
    color: red;
}
</style>
<main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">search</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- page main wrapper start -->
        <div class="shop-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <!-- shop main wrapper start -->
                    <div class="col-lg-12 order-1 order-lg-2">
                        <div class="shop-product-wrapper">
                            <!-- shop product top wrap start -->
                            <div class="shop-top-bar">
                                <div class="row align-items-center">
                                    <div class="col-lg-7 col-md-6 order-2 order-md-1">
                                        <div class="top-bar-left">
                                            <div class="product-view-mode">
                                                <a class="active" href="#" data-target="grid-view" data-toggle="tooltip" title="Grid View"><i class="fa fa-th"></i></a>
                                                <a href="#" data-target="list-view" data-toggle="tooltip" title="List View"><i class="fa fa-list"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-6 order-1 order-md-2">
                                    </div>
                                </div>
                            </div>
                            <!-- shop product top wrap start -->
                            <?php if(!empty($bladeVar['product_results'])): ?>
                            <!-- product item list wrapper start -->
                            <div class="shop-product-wrap grid-view row mbn-30">
                                <!-- product single item start -->
                                <?php $__currentLoopData = $bladeVar['product_results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							        <?php if($product->deleted_at == NULL): ?> 
                                    <div class="col-md-4 col-sm-6">
                                        <!-- product grid start -->
                                        <div class="product-item">
                                            <figure class="product-thumb">
                                                <a href="<?php echo e(url('/shop/'.$product->slug)); ?>">
                                                    <img class="pri-img" src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
										            <img class="sec-img" src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
                                                </a>
                                                <div class="product-badge">
                                                    <?php if(($product->new_arrival == '1') && ($product->on_sale == '1')): ?>
                                                    <div class="product-label new">
                                                        <span>new</span>
                                                    </div>
                                                    <div class="product-label discount">
                                                        <span>sale</span>
                                                    </div>
                                                    <?php elseif($product->new_arrival == '1'): ?>
                                                    <div class="product-label new">
                                                        <span>new</span>
                                                    </div>
                                                    <?php elseif($product->on_sale == '1'): ?>
                                                    <div class="product-label new">
                                                        <span>sale</span>
                                                    </div>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="button-group">
                                                <?php 
                                                $arrayWishlist = array();
                                                if(!empty($bladeVar['userWishlistData'])){ 
                                                    foreach($bladeVar['userWishlistData'] as $wishlist){
                                                        $arrayWishlist[] = $wishlist->product_id;
                                                    } 
                                                } 
                                                if (in_array($product->_id, $arrayWishlist)) { ?>
                                                    <a href="#" class="addtowishlist" id="addtowishlist<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like addWishlist"></i></a>
                                                <?php }else{ ?>
                                                    <a href="#" class="addtowishlist" id="addtowishlist<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like"></i></a>
                                                <?php } ?>
                                                </div>
                                                <?php if(!empty($product->product_quantity) && ($product->product_quantity > 0)): ?>    
                                                    <?php if($product->add_to_cart == '1'): ?>
                                                    <div class="cart-hover">
                                                        <?php $arrayCartList = array();
                                                        if(!empty($bladeVar['userCartData'])){ 
                                                            foreach($bladeVar['userCartData'] as $cartList){
                                                                $arrayCartList[] = $cartList->product_id;
                                                            } 
                                                        } 
                                                        if (in_array($product->_id, $arrayCartList)) { ?>
                                                            <button class="btn btn-cart addtocart disabled" id="addtocart<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>">Go to cart</button>
                                                        <?php }else{ ?>
                                                            <button class="btn btn-cart addtocart" id="addtocart<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>">add to cart</button>
                                                        <?php } ?>
                                                    </div>
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <div class="cart-hover">
                                                        <button class="btn btn-cart disabled">Out of stock</button>
                                                    </div>
                                                <?php endif; ?> 
                                            </figure>
                                            <div class="product-caption text-center">
                                                <div class="product-identity">
                                                    <p class="manufacturer-name"><a href="<?php echo e(url('/shop/?category='.$product->product_category->_id)); ?>"><?php echo e($product->product_category->name); ?></a></p>
                                                </div>
                                                <?php
                                                $colorArray = array(); 
                                                $colorArray[0] = $product->product_color_id;
                                                if(!empty($product->variations)){
                                                    $variations = json_decode($product->variations); 
                                                    foreach($variations as $variation){
                                                        $colorArray[] = $variation->color;
                                                    }
                                                }
                                                $getColorArray = array_unique($colorArray);
                                                if(!empty($getColorArray)){ 
                                                ?>
                                                <ul class="color-categories">
                                                <?php foreach($getColorArray as $color){ 
                                                    $allColors = $bladeVar['color_results'];
                                                    if(!empty($allColors)){
                                                        foreach($allColors as $getColor){
                                                            if($getColor->id == $color){ ?>
                                                                <li>
                                                                    <a href="#" style="background-color: <?php echo $getColor->color_code; ?>;" title="<?php echo $getColor->name; ?>"></a>
                                                                </li>   
                                                            <?php }
                                                        }
                                                    }
                                                } ?>
                                                </ul>
                                                <?php } ?>
                                                <h6 class="product-name">
                                                    <a href="<?php echo e(url('/shop/'.$product->slug)); ?>"><?php echo e($product->name); ?></a>
                                                </h6>
                                                <div class="price-box">
                                                    <?php if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')): ?>
                                                    <span class="price-regular">$<?php echo e($product->discounted_price); ?></span>
                                                    <span class="price-old"><del>$<?php echo e($product->price); ?></del></span>
                                                    <?php else: ?>
                                                    <span class="price-regular">$<?php echo e($product->price); ?></span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- product grid end -->

                                        <!-- product list item end -->
                                        <div class="product-list-item">
                                            <figure class="product-thumb">
                                                <a href="<?php echo e(url('/shop/'.$product->slug)); ?>">
                                                    <img class="pri-img" src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
										            <img class="sec-img" src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
                                                </a>
                                                <div class="product-badge">
                                                    <?php if(($product->new_arrival == '1') && ($product->on_sale == '1')): ?>
                                                    <div class="product-label new">
                                                        <span>new</span>
                                                    </div>
                                                    <div class="product-label discount">
                                                        <span>sale</span>
                                                    </div>
                                                    <?php elseif($product->new_arrival == '1'): ?>
                                                    <div class="product-label new">
                                                        <span>new</span>
                                                    </div>
                                                    <?php elseif($product->on_sale == '1'): ?>
                                                    <div class="product-label new">
                                                        <span>sale</span>
                                                    </div>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="button-group">
                                                <?php 
                                                $arrayWishlist = array();
                                                if(!empty($bladeVar['userWishlistData'])){ 
                                                    foreach($bladeVar['userWishlistData'] as $wishlist){
                                                        $arrayWishlist[] = $wishlist->product_id;
                                                    } 
                                                } 
                                                if (in_array($product->_id, $arrayWishlist)) { ?>
                                                    <a href="#" class="addtowishlist" id="addtowishlist<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like addWishlist"></i></a>
                                                <?php }else{ ?>
                                                    <a href="#" class="addtowishlist" id="addtowishlist<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like"></i></a>
                                                <?php } ?>
                                                </div>
                                                <?php if(!empty($product->product_quantity) && ($product->product_quantity > 0)): ?>
                                                    <?php if($product->add_to_cart == '1'): ?>
                                                    <div class="cart-hover">
                                                        <?php $arrayCartList = array();
                                                        if(!empty($bladeVar['userCartData'])){ 
                                                            foreach($bladeVar['userCartData'] as $cartList){
                                                                $arrayCartList[] = $cartList->product_id;
                                                            } 
                                                        } 
                                                        if (in_array($product->_id, $arrayCartList)) { ?>
                                                            <button class="btn btn-cart addtocart disabled" id="addtocart<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>">Go to cart</button>
                                                        <?php }else{ ?>
                                                            <button class="btn btn-cart addtocart" id="addtocart<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>">add to cart</button>
                                                        <?php } ?>
                                                    </div>
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <div class="cart-hover">
                                                        <button class="btn btn-cart disabled">Out of stock</button>
                                                    </div>
                                                <?php endif; ?> 
                                            </figure>
                                            <div class="product-content-list">
                                                <div class="manufacturer-name">
                                                    <a href="<?php echo e(url('/shop/?category='.$product->product_category->_id)); ?>"><?php echo e($product->product_category->name); ?></a>
                                                </div>
                                                <?php
                                                $colorArray = array(); 
                                                $colorArray[0] = $product->product_color_id;
                                                if(!empty($product->variations)){
                                                    $variations = json_decode($product->variations); 
                                                    foreach($variations as $variation){
                                                        $colorArray[] = $variation->color;
                                                    }
                                                }
                                                $getColorArray = array_unique($colorArray);
                                                if(!empty($getColorArray)){ 
                                                ?>
                                                <ul class="color-categories">
                                                <?php foreach($getColorArray as $color){ 
                                                    $allColors = $bladeVar['color_results'];
                                                    if(!empty($allColors)){
                                                        foreach($allColors as $getColor){
                                                            if($getColor->id == $color){ ?>
                                                                <li>
                                                                    <a href="#" style="background-color: <?php echo $getColor->color_code; ?>;" title="<?php echo $getColor->name; ?>"></a>
                                                                </li>   
                                                            <?php }
                                                        }
                                                    }
                                                } ?>
                                                </ul>
                                                <?php } ?>
                                                <h5 class="product-name"><a href="<?php echo e(url('/shop/'.$product->slug)); ?>"><?php echo e($product->name); ?></a></h5>
                                                <div class="price-box">
                                                    <?php if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')): ?>
                                                    <span class="price-regular">$<?php echo e($product->discounted_price); ?></span>
                                                    <span class="price-old"><del>$<?php echo e($product->price); ?></del></span>
                                                    <?php else: ?>
                                                    <span class="price-regular">$<?php echo e($product->price); ?></span>
                                                    <?php endif; ?>
                                                </div>
                                                <p><?php echo e($product->sort_details); ?></p>
                                            </div>
                                        </div>
                                        <!-- product list item end -->
                                    </div>
                                    <?php endif; ?>
						        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <!-- product single item start -->
                            </div>
                            <!-- product item list wrapper end -->

                            <!-- start pagination area -->
                            <div class="paginatoin-area text-center">
                             <?php echo e($bladeVar['product_results']->appends(Request::query())->links()); ?> 
                            </div>
                            <!-- end pagination area -->
                            <?php else: ?>
                                <div class="paginatoin-area text-center">
                                    <strong>No records found!</strong>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- shop main wrapper end -->
                </div>
            </div>
        </div>
        <!-- page main wrapper end -->
    </main>
<?php echo $__env->make('frontend.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>