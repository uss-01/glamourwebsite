<div class="row">
	<div class="col-12">
		<form action="<?php echo e(route('store.update', $bladeVar['result']->store_id)); ?>" method="POST">
			<?php echo e(method_field('PUT')); ?>

			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Name</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="name" value="<?php echo e(old('name', $bladeVar['result']->name)); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>

			
      	</form>
    </div>
</div>