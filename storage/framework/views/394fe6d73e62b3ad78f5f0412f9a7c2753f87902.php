<div class="row">
	<div class="col-12">
		<form action="<?php echo e(route('team.store')); ?>" enctype="multipart/form-data" method="POST" id="brand_form">
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Title</strong><small class="text-danger"> (required)</small></label>
				<input type="text" class="form-control" name="name" value="<?php echo e(old('name')); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
            <div class="form-group">
				<label class="form-control-label" for="designation"><strong>Designation</strong></label>
				<input type="text" class="form-control" name="designation" value="<?php echo e(old('designation')); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="facebook_url"><strong>Facebook Url</strong></label>
				<input type="text" class="form-control" name="facebook_url" value="<?php echo e(old('facebook_url')); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="twitter_url"><strong>Twitter Url</strong></label>
				<input type="text" class="form-control" name="twitter_url" value="<?php echo e(old('twitter_url')); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="linkedin_url"><strong>Linkedin Url</strong></label>
				<input type="text" class="form-control" name="linkedin_url" value="<?php echo e(old('linkedin_url')); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="google_url"><strong>Google Url</strong></label>
				<input type="text" class="form-control" name="google_url" value="<?php echo e(old('google_url')); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="image"><strong>Image (Size 400X400 px)</strong>
				 <small class="text-danger">(required)</small></label>
				<input type="file" name="image" class="form-control">
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div> 
		</form>
	</div>
</div>