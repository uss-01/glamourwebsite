<?php echo $__env->make('frontend.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<style type="text/css">
.addWishlist{
    color: red;
}
</style>
<main>
    <?php if(!empty($bladeVar['slider_results']->count() > 0)): ?> 
	<!-- hero slider area start -->
	<section class="slider-area">
		<div class="hero-slider-active slick-arrow-style slick-arrow-style_hero slick-dot-style">
			<?php $__currentLoopData = $bladeVar['slider_results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php if($slider->deleted_at == NULL): ?>
				<!-- single slider item start -->
				<div class="hero-single-slide hero-overlay">
					<div class="hero-slider-item bg-img" data-bg="<?php echo e(asset('images/cms').'/'.$slider->image); ?>">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<div class="hero-slider-content <?php echo e($slider->slidePosition); ?>">
										<h2 class="slide-title"><?php echo $slider->title; ?></h2>
										<h4 class="slide-desc"><?php echo e($slider->shortDescription); ?></h4>
										<a href="<?php echo e($slider->redirect_url); ?>" class="btn btn-hero"><?php echo e($slider->btntext); ?></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- single slider item start -->
				<?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</div>
	</section>
	<!-- hero slider area end -->
	<?php endif; ?>
	<?php if(!empty($bladeVar['site_setting']->count() > 0)): ?>
		<?php $__currentLoopData = $bladeVar['site_setting']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		    <?php if($setting->shipping == '1'): ?>
	<!-- service policy area start -->
	<div class="service-policy section-padding">
		<div class="container">
			<div class="row mtn-30">
				
				<div class="col-sm-6 col-lg-3">
					<div class="policy-item">
						<div class="policy-icon">
							<i class="pe-7s-plane"></i>
						</div>
						<div class="policy-content">
							<h6>Free Shipping</h6>
							<p>Free shipping all order</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="policy-item">
						<div class="policy-icon">
							<i class="pe-7s-help2"></i>
						</div>
						<div class="policy-content">
							<h6>Support 24/7</h6>
							<p>Support 24 hours a day</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="policy-item">
						<div class="policy-icon">
							<i class="pe-7s-back"></i>
						</div>
						<div class="policy-content">
							<h6>Money Return</h6>
							<p>30 days for free return</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="policy-item">
						<div class="policy-icon">
							<i class="pe-7s-credit"></i>
						</div>
						<div class="policy-content">
							<h6>100% Payment Secure</h6>
							<p>We ensure secure payment</p>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<!-- service policy area end -->
	        <?php endif; ?> 
	    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>

    <?php if(!empty($bladeVar['banner_results']->count() > 0)): ?> 
	<!-- banner statistics area start -->
	<div class="banner-statistics-area">
		<div class="container">
			<div class="row row-20 mtn-20">
				<?php $__currentLoopData = $bladeVar['banner_results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php if($banner->deleted_at == NULL): ?>
					<div class="col-sm-6">
						<figure class="banner-statistics mt-20">
							<a href="<?php echo e(url('/shop?category='.$banner->product_category->_id)); ?>">
								<img src="<?php echo e(asset('images/cms').'/'.$banner->image); ?>" alt="<?php echo e($banner->name); ?>">
							</a>
							<div class="banner-content <?php echo e($banner->bannerPosition); ?>">
								<h5 class="banner-text1"><?php echo e($banner->name); ?></h5>
								<h2 class="banner-text2"><?php echo $banner->shortDescription; ?></h2>
								<a href="<?php echo e(url('/shop?category='.$banner->product_category->_id)); ?>" class="btn btn-text"><?php echo e($banner->btntext); ?></a>
							</div>
						</figure>
					</div>
					<?php endif; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
		</div>
	</div>
	<!-- banner statistics area end -->
    <?php endif; ?>
	<?php if(!empty($bladeVar['product_results']->count() > 0)): ?>
	<!-- product area start -->
	<section class="product-area section-padding">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<!-- section title start -->
					<div class="section-title text-center">
						<h2 class="title">our products</h2>
						<p class="sub-title">Add our products to weekly lineup</p>
					</div>
					<!-- section title start -->
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="product-container">
						<!-- product tab menu start -->
						<div class="product-tab-menu">
							<ul class="nav justify-content-center">
							<?php  
							$array = array();
							foreach ($bladeVar['product_results'] as $product){
						        if($product->deleted_at == NULL){		
									$array[] = $product->product_category->name;
							    } 
							}
							$categoryArray = array_unique($array);
							?> 
							    <?php ($countp=0); ?>
								<?php $__currentLoopData = $categoryArray; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $productC): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<?php ($countp++); ?>
						            <?php if($countp == '1'): ?>
									<li><a href="#tab<?php echo e($countp); ?>" class="active" data-toggle="tab"><?php echo e($productC); ?></a></li>
									<?php else: ?>
									<li><a href="#tab<?php echo e($countp); ?>" data-toggle="tab"><?php echo e($productC); ?></a></li>
									<?php endif; ?>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</ul>
						</div>
						<!-- product tab menu end -->
						<!-- product tab content start -->
						<div class="tab-content">
					    <?php ($countp1=0); ?>
						<?php $__currentLoopData = $categoryArray; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $productC): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php ($countp1++); ?>
							<?php if($countp1 == '1'): ?>
							<div class="tab-pane fade show active" id="tab<?php echo e($countp1); ?>">
								<div class="product-carousel-4 slick-row-10 slick-arrow-style">
								<?php $__currentLoopData = $bladeVar['product_results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								    <?php if($product->deleted_at == NULL): ?>
									<?php if($productC == $product->product_category->name): ?>
									<!-- product item start -->
									<div class="product-item">
										<figure class="product-thumb">
											<a href="<?php echo e(url('/shop/'.$product->slug)); ?>">
												<img class="pri-img" src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
												<img class="sec-img" src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
											</a>
											<div class="product-badge">
											    <?php if(($product->new_arrival == '1') && ($product->on_sale == '1')): ?>
												<div class="product-label new">
													<span>new</span>
												</div>
												<div class="product-label discount">
													<span>sale</span>
												</div>
												<?php elseif($product->new_arrival == '1'): ?>
                                                <div class="product-label new">
													<span>new</span>
												</div>
												<?php elseif($product->on_sale == '1'): ?>
												<div class="product-label new">
													<span>sale</span>
												</div>
												<?php endif; ?>
											</div>
											<div class="button-group">
											<?php 
											$arrayWishlist = array();
											if(!empty($bladeVar['userWishlistData'])){ 
												foreach($bladeVar['userWishlistData'] as $wishlist){
													$arrayWishlist[] = $wishlist->product_id;
												} 
											} 
											if (in_array($product->_id, $arrayWishlist)) { ?>
												<a href="#" class="addtowishlist" id="addtowishlist<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like addWishlist"></i></a>
											<?php }else{ ?>
												<a href="#" class="addtowishlist" id="addtowishlist<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like"></i></a>
											<?php } ?>
											</div>
											<?php if(!empty($product->product_quantity) && ($product->product_quantity > 0)): ?>
												<?php if($product->add_to_cart == '1'): ?>
												<div class="cart-hover">
													<?php $arrayCartList = array();
													if(!empty($bladeVar['userCartData'])){ 
														foreach($bladeVar['userCartData'] as $cartList){
															$arrayCartList[] = $cartList->product_id;
														} 
													} 
													if (in_array($product->_id, $arrayCartList)) { ?>
														<button class="btn btn-cart addtocart disabled" id="addtocart<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>">Go to cart</button>
													<?php }else{ ?>
														<button class="btn btn-cart addtocart" id="addtocart<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>">add to cart</button>
													<?php } ?>
												</div>
												<?php endif; ?>
											<?php else: ?>
												<div class="cart-hover">
													<button class="btn btn-cart disabled">Out of stock</button>
												</div>
											<?php endif; ?> 
										</figure>
										<div class="product-caption text-center">
											<div class="product-identity">
												<p class="manufacturer-name"><a href="<?php echo e(url('/shop/?category='.$product->product_category->_id)); ?>"><?php echo e($product->product_category->name); ?></a></p>
											</div>
											<?php
											$colorArray = array(); 
											$colorArray[0] = $product->product_color_id;
											if(!empty($product->variations)){
                                                $variations = json_decode($product->variations); 
												foreach($variations as $variation){
													$colorArray[] = $variation->color;
												}
											}
											$getColorArray = array_unique($colorArray);
											if(!empty($getColorArray)){ 
											?>
											<ul class="color-categories">
											<?php foreach($getColorArray as $color){ 
												$allColors = $bladeVar['color_results'];
												if(!empty($allColors)){
													foreach($allColors as $getColor){
                                                        if($getColor->id == $color){ ?>
															<li>
																<a href="#" style="background-color: <?php echo $getColor->color_code; ?>;" title="<?php echo $getColor->name; ?>"></a>
															</li>   
														<?php }
													}
												}
											} ?>
											</ul>
										    <?php } ?>	
											<h6 class="product-name">
												<a href="<?php echo e(url('/shop/'.$product->slug)); ?>"><?php echo e($product->name); ?></a>
											</h6>
											<div class="price-box">
												<?php if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')): ?>
                                                <span class="price-regular">$<?php echo e($product->discounted_price); ?></span>
                                                <span class="price-old"><del>$<?php echo e($product->price); ?></del></span>
                                                <?php else: ?>
                                                <span class="price-regular">$<?php echo e($product->price); ?></span>
                                                <?php endif; ?>
											</div>
										</div>
									</div>
									<!-- product item end -->
									<?php endif; ?>
									<?php endif; ?>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</div>
							</div>
							<?php else: ?>
							<div class="tab-pane fade" id="tab<?php echo e($countp1); ?>">
								<div class="product-carousel-4 slick-row-10 slick-arrow-style">
									<?php $__currentLoopData = $bladeVar['product_results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<?php if($product->deleted_at == NULL): ?>
										<?php if($productC == $product->product_category->name): ?>
										<!-- product item start -->
										<div class="product-item">
											<figure class="product-thumb">
												<a href="<?php echo e(url('/shop/'.$product->slug)); ?>">
													<img class="pri-img" src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
													<img class="sec-img" src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
												</a>
												<div class="product-badge">
													<?php if(($product->new_arrival == '1') && ($product->on_sale == '1')): ?>
													<div class="product-label new">
														<span>new</span>
													</div>
													<div class="product-label discount">
														<span>sale</span>
													</div>
													<?php elseif($product->new_arrival == '1'): ?>
													<div class="product-label new">
														<span>new</span>
													</div>
													<?php elseif($product->on_sale == '1'): ?>
													<div class="product-label new">
														<span>sale</span>
													</div>
													<?php endif; ?>
												</div>
												<div class="button-group">
												<?php 
                                                $arrayWishlist = array();
                                                if(!empty($bladeVar['userWishlistData'])){ 
                                                    foreach($bladeVar['userWishlistData'] as $wishlist){
                                                        $arrayWishlist[] = $wishlist->product_id;
                                                    } 
                                                } 
                                                if (in_array($product->_id, $arrayWishlist)) { ?>
                                                    <a href="#" class="addtowishlist" id="addtowishlist<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like addWishlist"></i></a>
                                                <?php }else{ ?>
                                                    <a href="#" class="addtowishlist" id="addtowishlist<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like"></i></a>
                                                <?php } ?>
												</div>
											    <?php if(!empty($product->product_quantity) && ($product->product_quantity > 0)): ?> 
													<?php if($product->add_to_cart == '1'): ?>
													<div class="cart-hover">
														<?php $arrayCartList = array();
														if(!empty($bladeVar['userCartData'])){ 
															foreach($bladeVar['userCartData'] as $cartList){
																$arrayCartList[] = $cartList->product_id;
															} 
														} 
														if (in_array($product->_id, $arrayCartList)) { ?>
															<button class="btn btn-cart addtocart disabled" id="addtocart<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>">Go to cart</button>
														<?php }else{ ?>
															<button class="btn btn-cart addtocart" id="addtocart<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>">add to cart</button>
														<?php } ?>
													</div>
													<?php endif; ?>
												<?php else: ?>
                                                    <div class="cart-hover">
                                                        <button class="btn btn-cart disabled">Out of stock</button>
                                                    </div>
                                                <?php endif; ?> 
											</figure>
											<div class="product-caption text-center">
												<div class="product-identity">
													<p class="manufacturer-name"><a href="<?php echo e(url('/shop/?category='.$product->product_category->_id)); ?>"><?php echo e($product->product_category->name); ?></a></p>
												</div>
												<?php
												$colorArray = array(); 
												$colorArray[0] = $product->product_color_id;
												if(!empty($product->variations)){
													$variations = json_decode($product->variations); 
													foreach($variations as $variation){
														$colorArray[] = $variation->color;
													}
												}
												$getColorArray = array_unique($colorArray);
												if(!empty($getColorArray)){ 
												?>
												<ul class="color-categories">
												<?php foreach($getColorArray as $color){ 
													$allColors = $bladeVar['color_results'];
													if(!empty($allColors)){
														foreach($allColors as $getColor){
															if($getColor->id == $color){ ?>
																<li>
																	<a href="#" style="background-color: <?php echo $getColor->color_code; ?>;" title="<?php echo $getColor->name; ?>"></a>
																</li>   
															<?php }
														}
													}
												} ?>
												</ul>
												<?php } ?>	
												<h6 class="product-name">
													<a href="<?php echo e(url('/shop/'.$product->slug)); ?>"><?php echo e($product->name); ?></a>
												</h6>
												<div class="price-box">
													<?php if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')): ?>
													<span class="price-regular">$<?php echo e($product->discounted_price); ?></span>
													<span class="price-old"><del>$<?php echo e($product->price); ?></del></span>
													<?php else: ?>
													<span class="price-regular">$<?php echo e($product->price); ?></span>
													<?php endif; ?>
												</div>
											</div>
										</div>
										<!-- product item end -->
										<?php endif; ?>
										<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</div>
							</div>
							<?php endif; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</div>
						<!-- product tab content end -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- product area end -->
	<?php endif; ?>
    <?php if(!empty($bladeVar['bannerslider_results']->count() > 0)): ?>
	<!-- product banner statistics area start -->
	<section class="product-banner-statistics">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="product-banner-carousel slick-row-10">
					    <?php $__currentLoopData = $bladeVar['bannerslider_results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bannerslider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<!-- banner single slide start -->
						<div class="banner-slide-item">
							<figure class="banner-statistics">
								<a href="<?php echo e($bannerslider->redirect_url); ?>">
									<img src="<?php echo e(asset('images/cms').'/'.$bannerslider->image); ?>" alt="<?php echo e($bannerslider->slug); ?>">
								</a>
								<div class="banner-content banner-content_style2">
									<h5 class="banner-text3"><a href="<?php echo e($bannerslider->redirect_url); ?>"><?php echo e($bannerslider->name); ?></a></h5>
								</div>
							</figure>
						</div>
						<!-- banner single slide start -->
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- product banner statistics area end -->
    <?php endif; ?>
	<?php if(!empty($bladeVar['product_results']->count() > 0)): ?>
	<!-- featured product area start -->
	<section class="feature-product section-padding">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<!-- section title start -->
					<div class="section-title text-center">
						<h2 class="title">featured products</h2>
						<p class="sub-title">Add featured products to weekly lineup</p>
					</div>
					<!-- section title start -->
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="product-carousel-4_2 slick-row-10 slick-arrow-style">
						<?php $__currentLoopData = $bladeVar['product_results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if(($product->deleted_at == NULL) && ($product->featured == '1')): ?>
							<!-- product item start -->
							<div class="product-item">
								<figure class="product-thumb">
									<a href="<?php echo e(url('/shop/'.$product->slug)); ?>">
										<img class="pri-img" src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
										<img class="sec-img" src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
									</a>
									<div class="product-badge">
										<?php if(($product->new_arrival == '1') && ($product->on_sale == '1')): ?>
										<div class="product-label new">
											<span>new</span>
										</div>
										<div class="product-label discount">
											<span>sale</span>
										</div>
										<?php elseif($product->new_arrival == '1'): ?>
										<div class="product-label new">
											<span>new</span>
										</div>
										<?php elseif($product->on_sale == '1'): ?>
										<div class="product-label new">
											<span>sale</span>
										</div>
										<?php endif; ?>
									</div>
									<div class="button-group">
									<?php 
									$arrayWishlist = array();
									if(!empty($bladeVar['userWishlistData'])){ 
										foreach($bladeVar['userWishlistData'] as $wishlist){
											$arrayWishlist[] = $wishlist->product_id;
										} 
									} 
									if (in_array($product->_id, $arrayWishlist)) { ?>
										<a href="#" class="addtowishlist" id="addtowishlist<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like addWishlist"></i></a>
									<?php }else{ ?>
										<a href="#" class="addtowishlist" id="addtowishlist<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like"></i></a>
									<?php } ?>
									</div>
									<?php if(!empty($product->product_quantity) && ($product->product_quantity > 0)): ?>
										<?php if($product->add_to_cart == '1'): ?>
										<div class="cart-hover">
											<?php $arrayCartList = array();
											if(!empty($bladeVar['userCartData'])){ 
												foreach($bladeVar['userCartData'] as $cartList){
													$arrayCartList[] = $cartList->product_id;
												} 
											} 
											if (in_array($product->_id, $arrayCartList)) { ?>
												<button class="btn btn-cart addtocart disabled" id="addtocart<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>">Go to cart</button>
											<?php }else{ ?>
												<button class="btn btn-cart addtocart" id="addtocart<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>">add to cart</button>
											<?php } ?>
										</div>
										<?php endif; ?>
									<?php else: ?>
										<div class="cart-hover">
											<button class="btn btn-cart disabled">Out of stock</button>
										</div>
									<?php endif; ?>
								</figure>
								<div class="product-caption text-center">
									<div class="product-identity">
										<p class="manufacturer-name"><a href="<?php echo e(url('/shop/?category='.$product->product_category->_id)); ?>"><?php echo e($product->product_category->name); ?></a></p>
									</div>

									<?php
									$colorArray = array(); 
									$colorArray[0] = $product->product_color_id;
									if(!empty($product->variations)){
										$variations = json_decode($product->variations); 
										foreach($variations as $variation){
											$colorArray[] = $variation->color;
										}
									}
									$getColorArray = array_unique($colorArray);
									if(!empty($getColorArray)){ 
									?>
									<ul class="color-categories">
									<?php foreach($getColorArray as $color){ 
										$allColors = $bladeVar['color_results'];
										if(!empty($allColors)){
											foreach($allColors as $getColor){
												if($getColor->id == $color){ ?>
													<li>
														<a href="#" style="background-color: <?php echo $getColor->color_code; ?>;" title="<?php echo $getColor->name; ?>"></a>
													</li>   
												<?php }
											}
										}
									} ?>
									</ul>
									<?php } ?>	
									<h6 class="product-name">
										<a href="<?php echo e(url('/shop/'.$product->slug)); ?>"><?php echo e($product->name); ?></a>
									</h6>
									<div class="price-box">
										<?php if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')): ?>
										<span class="price-regular">$<?php echo e($product->discounted_price); ?></span>
										<span class="price-old"><del>$<?php echo e($product->price); ?></del></span>
										<?php else: ?>
										<span class="price-regular">$<?php echo e($product->price); ?></span>
										<?php endif; ?>
									</div>
								</div>
							</div>
							<!-- product item end -->
							<?php endif; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- featured product area end -->
	<?php endif; ?>
	<?php if(!empty($bladeVar['testimonial_results']->count() > 0)): ?>
	<!-- testimonial area start -->
	<section class="testimonial-area section-padding bg-img" data-bg="assets/img/testimonial/testimonials-bg.jpg">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<!-- section title start -->
					<div class="section-title text-center">
						<h2 class="title">testimonials</h2>
						<p class="sub-title">What they say</p>
					</div>
					<!-- section title start -->
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="testimonial-thumb-wrapper">
						<div class="testimonial-thumb-carousel">
						    <?php $__currentLoopData = $bladeVar['testimonial_results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $testimonial): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="testimonial-thumb">
								<img src="<?php echo e(asset('images/cms').'/'.$testimonial->image); ?>" alt="<?php echo e($testimonial->slug); ?>">
							</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</div>
					</div>
					<div class="testimonial-content-wrapper">
						<div class="testimonial-content-carousel">
							<?php $__currentLoopData = $bladeVar['testimonial_results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $testimonial): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="testimonial-content">
								<p><?php echo e($testimonial->shortDescription); ?></p>
								<h5 class="testimonial-author"><?php echo e($testimonial->name); ?></h5>
							</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- testimonial area end -->
	<?php endif; ?>
	<?php if(!empty($bladeVar['singleBanner_results']->count() > 0)): ?>
	<!-- group product start -->
	<section class="group-product-area section-padding">
		<div class="container">
			<div class="row">
			    <?php if(!empty($bladeVar['singleBanner_results']->count() > 0)): ?>
				    <?php ($count=0); ?>
				    <?php $__currentLoopData = $bladeVar['singleBanner_results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $singleBanner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php ($count++); ?>
						<?php if(($count == '1') && ($singleBanner->deleted_at == NULL)): ?>
						<div class="col-lg-6">
							<div class="group-product-banner">
								<figure class="banner-statistics">
									<a href="<?php echo e(url('/shop/?category='.$singleBanner->product_category->_id)); ?>">
										<img src="<?php echo e(asset('images/cms').'/'.$singleBanner->image); ?>" alt="<?php echo e($singleBanner->slug); ?>">
									</a>
									<div class="banner-content banner-content_style3 text-center">
										<h6 class="banner-text1"><?php echo e($singleBanner->name); ?></h6>
										<h2 class="banner-text2"><?php echo e($singleBanner->shortDescription); ?></h2>
										<a href="<?php echo e(url('/shop/?category='.$singleBanner->product_category->_id)); ?>" class="btn btn-text"><?php echo e($singleBanner->btntext); ?></a>
									</div>
								</figure>
							</div>
						</div>
						<?php endif; ?>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>

				<?php  
				$arrayOnsale = array();
				$arrayBestseller = array();
				foreach ($bladeVar['product_results'] as $product){
					if(($product->deleted_at == NULL) && ($product->on_sale == '1')){		
						$arrayOnsale[] = $product->_id;
					} 
					if(($product->deleted_at == NULL) && ($product->best_seller == '1')){		
						$arrayBestseller[] = $product->_id;
					}
				}
				?> 
				<?php if(!empty($arrayBestseller)){ ?>
				<div class="col-lg-3">
					<div class="categories-group-wrapper">
						<!-- section title start -->
						<div class="section-title-append">
							<h4>best seller product</h4>
							<div class="slick-append"></div>
						</div>
						<!-- section title start -->

						<!-- group list carousel start -->
						<div class="group-list-item-wrapper">
							<div class="group-list-carousel">
							<?php $__currentLoopData = $bladeVar['product_results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<?php if(($product->deleted_at == NULL) && ($product->best_seller == '1')): ?>
									<!-- group list item start -->
									<div class="group-slide-item">
										<div class="group-item">
											<div class="group-item-thumb">
												<a href="<?php echo e(url('/shop/'.$product->slug)); ?>">
													<img src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
												</a>
											</div>
											<div class="group-item-desc">
												<h5 class="group-product-name">
												    <a href="<?php echo e(url('/shop/'.$product->slug)); ?>"><?php echo e($product->name); ?></a>
												</h5>
												<div class="price-box">
													<?php if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')): ?>
													<span class="price-regular">$<?php echo e($product->discounted_price); ?></span>
													<span class="price-old"><del>$<?php echo e($product->price); ?></del></span>
													<?php else: ?>
													<span class="price-regular">$<?php echo e($product->price); ?></span>
													<?php endif; ?>
												</div>
											</div>
										</div>
									</div>
									<!-- group list item end -->
									<?php endif; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</div>
						</div>
						<!-- group list carousel start -->
					</div>
				</div>
				<?php } ?>
				<?php if(!empty($arrayOnsale)){ ?>
				<div class="col-lg-3">
					<div class="categories-group-wrapper">
						<!-- section title start -->
						<div class="section-title-append">
							<h4>on-sale product</h4>
							<div class="slick-append"></div>
						</div>
						<!-- section title start -->

						<!-- group list carousel start -->
						<div class="group-list-item-wrapper">
							<div class="group-list-carousel">
								<?php $__currentLoopData = $bladeVar['product_results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<?php if(($product->deleted_at == NULL) && ($product->on_sale == '1')): ?>
										<!-- group list item start -->
										<div class="group-slide-item">
											<div class="group-item">
												<div class="group-item-thumb">
													<a href="<?php echo e(url('/shop/'.$product->slug)); ?>">
														<img src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
													</a>
												</div>
												<div class="group-item-desc">
													<h5 class="group-product-name">
														<a href="<?php echo e(url('/shop/'.$product->slug)); ?>"><?php echo e($product->name); ?></a>
													</h5>
													<div class="price-box">
														<?php if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')): ?>
														<span class="price-regular">$<?php echo e($product->discounted_price); ?></span>
														<span class="price-old"><del>$<?php echo e($product->price); ?></del></span>
														<?php else: ?>
														<span class="price-regular">$<?php echo e($product->price); ?></span>
														<?php endif; ?>
													</div>
												</div>
											</div>
										</div>
										<!-- group list item end -->
										<?php endif; ?>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</div>
						</div>
						<!-- group list carousel start -->
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</section>
	<!-- group product end -->
    <?php endif; ?>
	<?php if(!empty($bladeVar['blog_results']->count() > 0)): ?>
	<!-- latest blog area start -->
	<section class="latest-blog-area section-padding pt-0">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<!-- section title start -->
					<div class="section-title text-center">
						<h2 class="title">latest blogs</h2>
						<p class="sub-title">There are latest blog posts</p>
					</div>
					<!-- section title start -->
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="blog-carousel-active slick-row-10 slick-arrow-style">
					    <?php $__currentLoopData = $bladeVar['blog_results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if($blog->deleted_at == NULL): ?>
							<!-- blog post item start -->
							<div class="blog-post-item">
								<figure class="blog-thumb">
									<a href="<?php echo e(url('/blog/'.$blog->slug)); ?>">
										<img src="<?php echo e(asset('images/blogs').'/'.$blog->image); ?>" alt="<?php echo e($blog->slug); ?>">
									</a>
								</figure>
								<div class="blog-content">
									<div class="blog-meta">
										<p><?php echo e(date('d/m/Y', strtotime($blog->updated_at))); ?></p>
									</div>
									<h5 class="blog-title">
										<a href="<?php echo e(url('/blog/'.$blog->slug)); ?>"><?php echo e($blog->name); ?></a>
									</h5>
								</div>
							</div>
							<!-- blog post item end -->
							<?php endif; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php endif; ?>
	<!-- latest blog area end -->
    <?php if(!empty($bladeVar['brand_results']->count() > 0)): ?>
	<!-- brand logo area start -->
	<div class="brand-logo section-padding pt-0">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="brand-logo-carousel slick-row-10 slick-arrow-style">
						<?php $__currentLoopData = $bladeVar['brand_results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<!-- single brand start -->
						<div class="brand-item">
							<a href="#">
								<img src="<?php echo e(asset('images/brands').'/'.$brand->image); ?>" alt="<?php echo e($brand->name); ?>">
							</a>
						</div>
						<!-- single brand end -->
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- brand logo area end -->
	<?php endif; ?>
</main>
<?php echo $__env->make('frontend.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>