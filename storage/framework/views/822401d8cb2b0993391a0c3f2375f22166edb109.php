<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td><?php echo e($bladeVar['result']->id); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Color Name</th>
		    		<td><?php echo e($bladeVar['result']->name); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Color Code</th>
		    		<td><span style="height:20px;width:20px;background:<?php echo e($bladeVar['result']->color_code); ?>;padding:5px;margin-left:10px;margin-right: 10px;"></span><?php echo e($bladeVar['result']->color_code); ?></td>
		    	</tr>
		    </tbody>
    	</table>
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="<?php echo e($bladeVar['result']->deleted_at == null ? route('color.edit', $bladeVar['result']->id) : ''); ?>" 
	    	data-destroy="<?php echo e($bladeVar['result']->deleted_at == null ? route('color.destroy', $bladeVar['result']->id) : ''); ?>" 
	    	data-restore="<?php echo e($bladeVar['result']->deleted_at != null ? route('color.restore', $bladeVar['result']->id) : ''); ?>"
	    	data-delete="<?php echo e(route('color.delete', $bladeVar['result']->id)); ?>">
	    </span>
	</div>
</div>	    