<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td><?php echo e($bladeVar['result']->_id); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Title</th>
		    		<td><?php echo e($bladeVar['result']->title); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Sub Title</th>
		    		<td><?php echo e($bladeVar['result']->sub_title); ?></td>
		    	</tr>
                <tr>
		    		<th class="bg-faded">Event Start Time</th>
		    		<td><?php echo e($bladeVar['result']->event_time); ?></td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Event Location</th>
		    		<td><?php echo e($bladeVar['result']->event_location); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Details</th>
		    		<td><?php echo e(strip_tags($bladeVar['result']->details)); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Start Date</th>
		    		<td><?php echo e(Carbon\Carbon::parse($bladeVar['result']->start_date)->format('d/m/Y')); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">End Date</th>
		    		<td><?php echo e(Carbon\Carbon::parse($bladeVar['result']->end_date)->format('d/m/Y')); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Image</th>
		    		<td><img src="<?php echo e(asset('images/events').'/'.$bladeVar['result']->image); ?>" class="thumbnail" width="150" /></td>
		    	</tr>
		    	
		    </tbody>
    	</table>
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="<?php echo e($bladeVar['result']->deleted_at == null ? route('event.edit', $bladeVar['result']->_id) : ''); ?>" 
	    	data-destroy="<?php echo e($bladeVar['result']->deleted_at == null ? route('event.destroy', $bladeVar['result']->_id) : ''); ?>" 
	    	data-restore="<?php echo e($bladeVar['result']->deleted_at != null ? route('event.restore', $bladeVar['result']->_id) : ''); ?>"
	    	data-delete="<?php echo e(route('event.delete', $bladeVar['result']->_id)); ?>">
	    </span>
	</div>
</div>	    