<div class="row">
	<div class="col-12">
		<form action="<?php echo e(route('store.store')); ?>" method="POST">
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Store name</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="name" value="<?php echo e(old('name')); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>

		</form>
	</div>
</div>