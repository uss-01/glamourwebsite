<?php echo $__env->make('frontend.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<style type="text/css">
    .has-error {
        border: 1px solid red !important;
    }
    #couponCodeError {
        color: red;
    }
    #responseMessage, #responseMSG, #resMessage{
        margin-top: 8px;
    }
    .error-text.success{
        color: #000 !important;
    }
    .couponDis{
        display : none;
    }
</style>
    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="<?php echo e(url('/account')); ?>">my account</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Order Details</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- cart main wrapper start -->
        
        <div class="cart-main-wrapper section-padding">
            <div class="container">
                <?php if(!empty($bladeVar['ordersList'])): ?>
                        <div class="section-bg-color">  
                            <div class="row">
                                <div class="col-lg-12">
                                    <!-- Cart Table Area -->
                                    <div class="cart-table table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="pro-order">Order Id</th>
                                                    <th class="pro-thumbnail">Thumbnail</th>
                                                    <th class="pro-title">Product</th>
                                                    <th class="pro-price">Price</th>
                                                    <th class="pro-quantity">Quantity</th>
                                                    <th class="pro-subtotal">Total</th>
                                                    <th class="pro-remove">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody class="cartList">
                                                <?php $__currentLoopData = $bladeVar['ordersList']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                                    <tr id="order<?php echo e($order->_id); ?>">
                                                        <td class="pro-order"><?php echo e($order->orderUniqid); ?></td>
                                                        <td class="pro-thumbnail"><img class="img-fluid" src="<?php echo e(asset('images/products').'/'.$order->product_image); ?>" alt="<?php echo e($order->product_name); ?>" /></td>
                                                        <td class="pro-title"><?php echo e($order->product_name); ?></td>
                                                        <td class="pro-price"><span>
                                                            <?php if(!empty($order->discounted_price) && ($order->discounted_price != '0.00')): ?>
                                                                $<?php echo e($order->discounted_price); ?>

                                                            <?php else: ?>
                                                                $<?php echo e($order->price); ?>

                                                            <?php endif; ?>
                                                        </span></td>
                                                        <td class="pro-quantity">
                                                            <span><?php echo e($order->quantity); ?></span>
                                                        </td>
                                                        <td class="pro-subtotal"><span>
                                                            <?php if(!empty($order->discounted_price) && ($order->discounted_price != '0.00')): ?>
                                                                $<?php echo e(number_format($order->discounted_price * $order->quantity, 2)); ?>

                                                            <?php else: ?>
                                                                $<?php echo e(number_format($order->price * $order->quantity, 2)); ?>

                                                            <?php endif; ?>
                                                        </span></td>
                                                        <?php if(!empty($order->order_status) && ($order->order_status == 'Canceled')): ?>
                                                        <td><?php echo e($order->order_status); ?></td>
                                                        <?php else: ?>
                                                        <td>
                                                            <span style="display: block; text-transform:capitalize;"><?php echo e($order->order_status); ?></span>
                                                            <a href="#" style="text-decoration: underline;" class="cancelProduct" data-opid="<?php echo e($order->order_id); ?>" data-pid="<?php echo e($order->product_id); ?>" data-oid="<?php echo e($order->_id); ?>" data-ouid="<?php echo e($order->orderUniqid); ?>">Cancel Order</a>
                                                        </td>
                                                        <?php endif; ?> 
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 ml-auto">
                                    <!-- Cart Calculation Area -->
                                    <?php if(!empty($bladeVar['order'])): ?>
                                    <div class="cart-calculator-wrapper">
                                        <div class="cart-calculate-items">
                                            <h6>Order Total</h6>
                                            <?php $subTotal = $bladeVar['order']['0']['amount']; ?>
                                            <?php $shipping = $bladeVar['order']['0']['shippingCharge']; ?>
                                            <?php if(!empty($bladeVar['couponData'])){  ?>
                                                <?php 
                                                    $coupon = $bladeVar['couponData']['0']->name;
                                                    $type = $bladeVar['couponData']['0']->type;
                                                    $value = $bladeVar['couponData']['0']->value;
                                                    if($type == 'Flat'){
                                                        $percentage = number_format($value, 2);
                                                        if(!empty($subTotal)){ $subTotalPrice = number_format(($subTotal-$shipping), 2); }else{ $subTotalPrice = '0.00'; }
                                                        if(!empty($subTotal)){ $totalPrice = number_format(($subTotal-$percentage), 2); }else{ $totalPrice = number_format(($shipping), 2); }
                                                    }else{
                                                        $numberFour = number_format($value, 2);
                                                        $total = $subTotal-$shipping;
                                                        $percentage = number_format(($numberFour / 100) * $total, 2);
                                                        if(!empty($subTotal)){ 
                                                            $subTotalPrice = number_format(($subTotal-$percentage-$shipping), 2); }else{ $subTotalPrice = '0.00'; }
                                                        if(!empty($subTotal)){
                                                            $totalPrice = number_format(($total-$percentage), 2); 
                                                        }else{ $totalPrice = number_format(($shipping), 2); }
                                                    }
                                                ?>
                                                <table class="table">
                                                    <tr>
                                                        <td>Sub Total</td>
                                                        <td class="subTotal">$<?php if(!empty($subTotalPrice)){ echo number_format($subTotalPrice, 2); } ?></td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td>Coupon Code</td>
                                                        <td class="coupon"><?php if(!empty($coupon)){ echo $coupon; } ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Coupon Discount</td>
                                                        <td class="coupon">$<?php if(!empty($percentage)){ echo number_format($percentage, 2); } ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shipping</td>
                                                        <td class="shipping">$<?php if(!empty($shipping)){ echo $shipping; }else{ echo 'Free'; } ?></td>
                                                    </tr>
                                                    <tr class="total">
                                                        <td>Total</td>
                                                        <td class="total-amount">$<?php if(!empty($totalPrice)){ echo number_format($totalPrice, 2); }  ?></td>
                                                    </tr>
                                                </table>
                                            <?php }else{ ?>
                                                <table class="table">
                                                    <tr>
                                                        <td>Sub Total</td>
                                                        <td class="subTotal">$<?php if(!empty($subTotal)){ echo number_format($subTotal-$shipping, 2); } ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shipping</td>
                                                        <td class="shipping">$<?php if(!empty($shipping)){ echo $shipping; }else{ echo 'Free'; } ?></td>
                                                    </tr>
                                                    <tr class="total">
                                                        <td>Total</td>
                                                        <td class="total-amount">$<?php if(!empty($subTotal)){ echo number_format(($subTotal), 2); } ?></td>
                                                    </tr>
                                                </table>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div> 
                <?php endif; ?>
            </div>
        </div>
        
        <!-- cart main wrapper end -->
    </main>
<?php echo $__env->make('frontend.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script type="text/javascript">
$(document).ready(function() {
    var baseUrl = '<?php echo e(url("/")); ?>';
    var token = '<?php echo e(csrf_token()); ?>';
    
    $(".cancelProduct").on('click', function(event) {
        event.preventDefault();
        var getOID = $(this).attr('data-oid');
        var getOUID = $(this).attr('data-ouid');
        var getPID = $(this).attr('data-pid');  
        var orderid = $(this).attr('data-opid');
        var userID = '';
        <?php $data = Session::all(); if (Session::has('userLoggedIn')){ ?>
            userID = '<?php echo Session::get('userLoggedIn'); ?>';
        <?php } ?>
        if((userID != '') && (getOID != '') && (getOUID != '')){
            $.ajax({
				url: baseUrl + "/orderCancel",
				type: "POST",
				data: { 
                    '_token': token, 
					'orderId': getOID,
                    'orderUId': getOUID,
                    'productId': getPID,
				},
				dataType: "JSON",
				success: function(jsonStr) {
					var res_data = JSON.stringify(jsonStr);
					var response = JSON.parse(res_data);
                    var responseData = response['responseData'];
					if ((responseData != null) && (responseData == 'order product cancel successfully')) {
                          var redirecturl = baseUrl+'/orderDetail/'+orderid;
                          location.href = redirecturl;
					} else {
						console.log(responseData);
                    }
				}
			});    
        }else{
           var redirecturl = baseUrl+'/login-register';
           location.href = redirecturl;  
        } 
    });
 

});
</script>