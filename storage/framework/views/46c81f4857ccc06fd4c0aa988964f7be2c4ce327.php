<?php $__env->startSection('head'); ?>
<link type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"/>
<link type="text/css" href="<?php echo e(asset('thirdparty/timepicker/jquery-ui-timepicker-addon.min.css')); ?>" rel="stylesheet"/>
<link type="text/css" href="<?php echo e(asset('thirdparty/editor/summernote.css')); ?>" rel="stylesheet"/>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid" id="pageContainer">
    <div class="row" id="pageContent">
        <div class="col-12 col-md-8 offset-md-2">
            <div class="float-right mt-2 hidden-xs-down">
                <a href="<?php echo e(route('profile.create')); ?>" class="btn btn-sm btn-info" title="Add" data-toggle="modal" data-target="#createModal"><i class="fa fa-plus"></i></a> 	
            </div>
            <div class="float-right mt-1 hidden-sm-up">
                <a href="<?php echo e(route('profile.create')); ?>" class="dropdown-item" data-toggle="modal" data-target="#createModal"><i class="fa fa-plus text-info"></i> Add</a>
            </div>
            <h1 class="hidden-xs-down">Tenant Complaints</h1>
            <h1 class="hidden-sm-up h3">Tenant Complaints</h1>
            <div class="clearfix alertArea"></div>
        <?php if(!empty($bladeVar['complaintsData'])): ?>
            <table class="table table-bordered table-hover table-sm">
                <thead class="bg-faded">
                    <tr>
                        <th class="text-center" style="width: 4rem;"><nobr><a href="<?php echo e(url()->current()); ?>?<?php echo e(Request::input('q') ? 'q='.Request::input('q').'&' : ''); ?>sort=id&dir=<?php echo e(Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'desc' : 'asc' : 'asc'); ?>">Id</a> <i class="fa fa-<?php echo e(Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'sort-numeric-asc' : 'sort-numeric-desc' : 'sort'); ?>"></i></nobr></th>
                        <th><nobr>Complaint</nobr></th>
                        <th><nobr>Admin Reply</nobr></th>
                        <th><nobr>Complaint Date</nobr></th>
                        <th><nobr>Reply Date</nobr></th>
                    </tr>
                </thead>
                <tbody>
				    <?php $__currentLoopData = $bladeVar['complaintsData']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $complaint): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr<?php echo $complaint->deleted_at != null ? ' class="table-danger"' : ''; ?>>
						<td class="text-center"><span<?php echo $complaint->deleted_at != null ? ' class="badge badge-danger"' : ''; ?>><?php echo e($complaint->id); ?></span></td>
                        <td><?php echo e($complaint->complaint); ?></td>
                        <td><?php echo e($complaint->reply); ?></td>
                        <td><?php echo e(date('d F Y', strtotime($complaint->created_at))); ?></td>
                        <?php if($complaint->updated_at > $complaint->created_at): ?>
                        <td><?php echo e(date('d F Y', strtotime($complaint->updated_at))); ?></td>
                        <?php else: ?>
                        <td></td>
                        <?php endif; ?>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
			</table>
			<div class="mt-4">
				<?php echo e($bladeVar['complaintsData']->appends(Request::query())->links()); ?>

				<small class="d-block text-center text-muted">(showing 20 results per page)</small>
			</div>
		<?php else: ?>
			<div class="alert alert-success">
				<strong>No records found!</strong>
				<?php if(!empty(Request::input('q'))): ?>
				No matching your search criteria. 
				<?php else: ?> 
				No Offers added yet, please click on "Create New" button to add a leasing. 
				<?php endif; ?>
			</div>
		<?php endif; ?>
        <!-- Deletetion code with Form -->
        <form class="formDelete">
            <?php echo e(method_field('DELETE')); ?>

        </form>
		<!-- Add modal code: START -->
		<div class="modal" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalTitle" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="createModalTitle">Add Complaint</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
					</div>
					<div class="modal-footer hidden-xs-up">
					</div>
				</div>
			</div>
		</div>
		<!-- Add modal code: END -->


</div>
</div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('foot'); ?>
<script type="text/javascript">
    var pageUrl = '<?php echo e(route('profile.index')); ?>';
    var ajaxLoadUrl = '<?php echo e(url()->full()); ?>';
    var pageContainer = '#pageContainer';
    var pageContent = '#pageContent';
</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src="<?php echo e(asset('thirdparty/timepicker/jquery-ui-timepicker-addon.min.js')); ?>"></script>
<script src="<?php echo e(asset('thirdparty/editor/summernote.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.form.js')); ?>"></script>
<script src="<?php echo e(asset('js/profile.js?v=1')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>