<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td><?php echo e($bladeVar['result']->_id); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Title</th>
		    		<td><?php echo e($bladeVar['result']->title); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Email</th>
		    		<td><?php echo e($bladeVar['result']->email); ?></td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Contact No.</th>
		    		<td><?php echo e($bladeVar['result']->phone); ?></td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Facebook Url</th>
		    		<td><?php echo e($bladeVar['result']->facebook_url); ?></td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Twitter Url</th>
		    		<td><?php echo e($bladeVar['result']->twitter_url); ?></td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Instagram Url</th>
		    		<td><?php echo e($bladeVar['result']->instagram_url); ?></td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Youtube Url</th>
		    		<td><?php echo e($bladeVar['result']->youtube_url); ?></td>
				</tr>
				<tr>
		    		<th class="bg-faded">Linkedin Url</th>
		    		<td><?php echo e($bladeVar['result']->linkedin_url); ?></td>
				</tr>
				<tr>
		    		<th class="bg-faded">Pinterest Url</th>
		    		<td><?php echo e($bladeVar['result']->pinterest_url); ?></td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Address</th>
		    		<td><?php echo e($bladeVar['result']->address); ?></td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Message</th>
		    		<td><?php echo e($bladeVar['result']->message); ?></td>
		    	</tr>
				<?php if(!empty($bladeVar['result']->image)): ?>
				<tr>
		    		<th class="bg-faded">Image</th>
		    		<td><img src="<?php echo e(asset('images/cms').'/'.$bladeVar['result']->image); ?>" class="thumbnail" width="150" /></td>
		    	</tr>
				<?php endif; ?>
		    </tbody>
    	</table>
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="<?php echo e($bladeVar['result']->deleted_at == null ? route('setting.edit', $bladeVar['result']->_id) : ''); ?>" 
	    	data-delete="<?php echo e(route('setting.delete', $bladeVar['result']->_id)); ?>">
	    </span>
	</div>
</div>	    