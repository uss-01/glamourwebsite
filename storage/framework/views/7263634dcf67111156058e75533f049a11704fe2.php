<div class="row">
	<div class="col-12">
		<form action="<?php echo e(route('color.update', $bladeVar['result']->id)); ?>" method="POST">
			<?php echo e(method_field('PUT')); ?>

			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Color Name</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="name" value="<?php echo e(old('name', $bladeVar['result']->name)); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="color_code"><strong>Color Code</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="color_code" value="<?php echo e(old('color_code', $bladeVar['result']->color_code)); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
      	</form>
    </div>
</div>