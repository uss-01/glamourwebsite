<?php echo $__env->make('frontend.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<style type="text/css">
    .has-error {
        border: 1px solid red !important;
    }

    #usernameError,
    #emailError,
    #passwordError,
    #conpasswordError {
        color: red;
    }
    .forgot-password-form{
        display: none;
    }
    #responseMessage, #responseMSG, #resMessage{
        margin-top: 8px;
    }
</style>
    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">login-Register</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->
        <!-- login register wrapper start -->
        <div class="login-register-wrapper section-padding">
            <div class="container">
                <div class="member-area-from-wrap">
                    <div class="row">
                        <!-- Login Content Start -->
                        <div class="col-lg-6">
                            <div class="login-reg-form-wrap sign-in-form">
                                <h5>Sign In</h5>
                                <p class="login-box-msg" id="responseMessage"></p>
                                <form action="#" method="post" id="userLoginForm">
                                    <input type="hidden" id="_token" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <div class="single-input-item">
                                        <input type="email" id="email" name="email" placeholder="Enter your Email" />
                                        <span class="error-text" id="emailError"></span>
                                    </div>
                                    <div class="single-input-item">
                                        <input type="password" id="password" name="password" placeholder="Enter your Password" />
                                        <span class="error-text" id="passwordError"></span>
                                    </div>
                                    <div class="single-input-item">
                                        <div class="login-reg-form-meta d-flex align-items-center justify-content-between">
                                            <div class="remember-meta">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="rememberMe">
                                                    <label class="custom-control-label" for="rememberMe">Remember Me</label>
                                                </div>
                                            </div>
                                            <a href="#" class="forget-pwd">Forget Password?</a>
                                        </div>
                                    </div>
                                    <div class="single-input-item">
                                        <button class="btn btn-sqr" id="userLogin">Login</button>
                                        <img src="<?php echo e(asset('images/ajax-loader.gif')); ?>" id="ajax-loader" style="display:none; margin-left: 10px; height: 40px;">
                                    </div>
                                </form>
                            </div>
                            <div class="login-reg-form-wrap forgot-password-form">
                                <h5>Forgot Password</h5>
                                <p class="login-box-msg" id="responseMSG">Please enter your email address below. We will send an email with a secure link to change your password.</p>
                                <form action="#" method="post" id="userForgotPasswordForm">
                                    <input type="hidden" id="_token" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <div class="single-input-item">
                                        <input type="email" id="email" name="email" placeholder="Enter your Email" />
                                        <span class="error-text" id="emailError"></span>
                                    </div>
                                    <div class="single-input-item">
                                        <div class="login-reg-form-meta d-flex align-items-center justify-content-between">
                                            <div class="remember-meta"></div>
                                            <a href="#" class="login-form">Login?</a>
                                        </div>
                                    </div>
                                    <div class="single-input-item">
                                        <button class="btn btn-sqr" id="userForgotPassword">Forget Password</button>
                                        <img src="<?php echo e(asset('images/ajax-loader.gif')); ?>" id="ajax-loader" style="display:none; margin-left: 10px; height: 40px;">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- Login Content End -->
                        <!-- Register Content Start -->
                        <div class="col-lg-6">
                            <div class="login-reg-form-wrap sign-up-form">
                                <h5>Sing Up</h5>
                                <p class="login-box-msg" id="resMessage"></p>
                                <form action="#" method="post" id="userSingupForm">
                                    <input type="hidden" id="_token" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <div class="single-input-item">
                                        <input type="text" id="username" name="username" placeholder="Full Name" />
                                        <span class="error-text" id="usernameError"></span>
                                    </div>
                                    <div class="single-input-item">
                                        <input type="email" id="email" name="email" placeholder="Enter your Email" />
                                        <span class="error-text" id="emailError"></span>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="single-input-item">
                                                <input type="password" id="password" name="password" placeholder="Enter your Password" />
                                                <span class="error-text" id="passwordError"></span> 
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="single-input-item">
                                                <input type="password" id="conpassword" name="password_confirmation" placeholder="Repeat your Password" />
                                                <span class="error-text" id="conpasswordError"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single-input-item">
                                        <div class="login-reg-form-meta">
                                            <div class="remember-meta">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="subnewsletter">
                                                    <label class="custom-control-label" for="subnewsletter">Subscribe Our Newsletter</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single-input-item">
                                        <button class="btn btn-sqr" id="userRegister">Register</button>
                                        <img src="<?php echo e(asset('images/ajax-loader.gif')); ?>" id="ajax-loader" style="display:none; margin-left: 10px; height: 40px;">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- Register Content End -->
                    </div>
                </div>
            </div>
        </div>
        <!-- login register wrapper end -->
    </main>
<?php echo $__env->make('frontend.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script type="text/javascript">
$(document).ready(function() {
    var baseUrl = '<?php echo e(url("/")); ?>';
	function isEmail(emailid) {
		var pattern = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
		return pattern.test(emailid);
	}
//---------------------------------Sign Up Start-------------------------------------------//
    $('#userSingupForm #username').focusout(function() {
        var usernameValue = $(this).val();
        if (usernameValue == '') {
            $('#userSingupForm #username').addClass('has-error');
            $('#userSingupForm #usernameError').show().text('Please enter a full name');
        } else if (usernameValue != '') {
            $('#userSingupForm #username').removeClass('has-error');
            $('#userSingupForm #usernameError').hide().text('');
        }
    });

    $('#userSingupForm #email').focusout(function() {
        var mailValue = $(this).val();            
        if ((mailValue == '') || (!isEmail($('#userSingupForm #email').val()))) {
            $('#userSingupForm #email').addClass('has-error');
            $('#userSingupForm #emailError').show().text('Please enter a valid email address');
        } else if ((mailValue != '') && (isEmail($('#userSingupForm #email').val()))) {
            $('#userSingupForm #email').removeClass('has-error');
            $('#userSingupForm #emailError').hide().text('');
        }
    });

    $('#userSingupForm #password').focusout(function() {
        var passwordValue = $(this).val();
        if (passwordValue == '') {
            $('#userSingupForm #password').addClass('has-error');
            $('#userSingupForm #passwordError').show().text('Password empty or invalid');
        } else if (passwordValue.length < 6) {
            $('#userSingupForm #password').addClass('has-error');
            $('#userSingupForm #passwordError').show().text('Password must be at least 6 characters');
        } else if (passwordValue != '') {
            $('#userSingupForm #password').removeClass('has-error');
            $('#userSingupForm #passwordError').hide().text('');
        }
    });

    $('#userSingupForm #conpassword').focusout(function() {
        var passwordValue = $('#userSingupForm #password').val();  
        var conpasswordValue = $(this).val();
        if (conpasswordValue == '') {
            $('#userSingupForm #conpassword').addClass('has-error');
            $('#userSingupForm #conpasswordError').show().text('Confirm Password empty or invalid');
        } else if (conpasswordValue.length < 6) {
            $('#userSingupForm #conpassword').addClass('has-error');
            $('#userSingupForm #conpasswordError').show().text('Confirm Password must be at least 6 characters');
        } else if (conpasswordValue != passwordValue) {
            $('#userSingupForm #conpassword').addClass('has-error');
            $('#userSingupForm #conpasswordError').show().text('Your password and confirmation password do not match');
        } else if (conpasswordValue != '') {
            $('#userSingupForm #conpassword').removeClass('has-error');
            $('#userSingupForm #conpasswordError').hide().text('');
        }
    });

    $('#userRegister').on('click', function(event) {
        event.preventDefault();
        $('#responseMessage').html('');
        $('#userSingupForm #username').trigger("focusout");
        $('#userSingupForm #email').trigger("focusout");
        $('#userSingupForm #password').trigger("focusout");
        $('#userSingupForm #conpassword').trigger("focusout");
        var usernameValue = $("#userSingupForm #username").val();
        var mailValue = $("#userSingupForm #email").val();
        var passwordValue = $("#userSingupForm #password").val();
        var conpasswordValue = $("#userSingupForm #conpassword").val();
        if ((usernameValue != '') && (mailValue != '') && (isEmail($('#userSingupForm #email').val())) && (passwordValue != '') && (passwordValue.length >= 6) && (conpasswordValue != '') && (conpasswordValue.length >= 6) && (conpasswordValue == passwordValue)) {
            $('#userSingupForm #username').removeClass('has-error');
            $('#userSingupForm #email').removeClass('has-error');
            $('#userSingupForm #password').removeClass('has-error');
            $('#userSingupForm #conpassword').removeClass('has-error');
            $('#userSingupForm #usernameError').text('').hide();
            $('#userSingupForm #emailError').text('').hide();
            $('#userSingupForm #passwordError').text('').hide();
            $('#userSingupForm #conpasswordError').text('').hide();
            user_signup_account();
        }
    });

    function user_signup_account() {
        $('#resMessage').html('');
        $('#userSingupForm #ajax-loader').show();
        var token = $('#userSingupForm #_token').val();
        var username1 = $('#userSingupForm #username').val();
        var email1 = $('#userSingupForm #email').val();
        var password1 = $('#userSingupForm #password').val();
        var conpassword1 = $('#userSingupForm #conpassword').val();
        if($('#userSingupForm #subnewsletter').is(":checked")){
            var subnewsletter1 = true;
        }else{
            var subnewsletter1 = false;
        }
        $.ajax({
            url: baseUrl + "/usersignup",
            type: "POST",
            data: {
                '_token': token,
                'name': username1,
                'email': email1,
                'password': password1,
                'password_confirmation': conpassword1,
                'subnewsletter' : subnewsletter1,
                'createAccount': 'Signup customer'
            },
            dataType: "JSON",
            success: function(jsonStr) {
                var res_data = JSON.stringify(jsonStr);
                var response = JSON.parse(res_data);
                var responseData = response['responseData'];
                if ((responseData != null) && (responseData == 'user created successfully')) {
                    $('#resMessage').html('Your account has been created successfully');
                    setTimeout(function(){ window.location.href = baseUrl;  }, 1500);
                } else {
                    $('#resMessage').html(responseData);
                }
                $('#userSingupForm #ajax-loader').hide();
            }
        });
    }
//-----------------------------------Sign Up End-----------------------------------------//
    $('.login-form').on('click', function(event) {
        $('.forgot-password-form').hide();
        $('.sign-in-form').show();
    });

    $('.forget-pwd').on('click', function(event) {
        $('.sign-in-form').hide();
        $('.forgot-password-form').show();
    });
//-------------------------------Sign In Start-----------------------------------------//	
    $('#userLoginForm #email').focusout(function() {
        var mailValue = $(this).val();            
        if ((mailValue == '') || (!isEmail($('#userLoginForm #email').val()))) {
            $('#userLoginForm #email').addClass('has-error');
            $('#userLoginForm #emailError').show().text('Please enter a valid email address');
        } else if ((mailValue != '') && (isEmail($('#userLoginForm #email').val()))) {
            $('#userLoginForm #email').removeClass('has-error');
            $('#userLoginForm #emailError').hide().text('');
        }
    });

    $('#userLoginForm #password').focusout(function() {
        var passwordValue = $(this).val();
        if (passwordValue == '') {
            $('#userLoginForm #password').addClass('has-error');
            $('#userLoginForm #passwordError').show().text('Password empty or invalid');
        } else if (passwordValue.length < 4) {
            $('#userLoginForm #password').addClass('has-error');
            $('#userLoginForm #passwordError').show().text('Password must be at least 4 characters');
        } else if (passwordValue != '') {
            $('#userLoginForm #password').removeClass('has-error');
            $('#userLoginForm #passwordError').hide().text('');
        }
    });

    $('#userLogin').on('click', function(event) {
        event.preventDefault();
        $('#responseMessage').html('');
        $('#userLoginForm #email').trigger("focusout");
        $('#userLoginForm #password').trigger("focusout");
        var mailValue = $("#userLoginForm #email").val();
        var passwordValue = $("#userLoginForm #password").val();
        if ((mailValue != '') && (isEmail($('#userLoginForm #email').val())) && (passwordValue != '') && (passwordValue.length >= 4)) {
            $('#userLoginForm #email').removeClass('has-error');
            $('#userLoginForm #password').removeClass('has-error');
            $('#userLoginForm #emailError').text('').hide();
            $('#userLoginForm #passwordError').text('').hide();
            user_login_account();
        }
    });

    function user_login_account() {
        $('#responseMessage').html('');
        $('#userLoginForm #ajax-loader').show();
        var token = $('#userLoginForm #_token').val();
        var email1 = $('#userLoginForm #email').val();
        var password1 = $('#userLoginForm #password').val();
        if($('#userLoginForm #rememberMe').is(":checked")){
            var remember1 = true;
        }else{
            var remember1 = false;
        }
        $.ajax({
            url: baseUrl + "/userlogin",
            type: "POST",
            data: {
                '_token': token,
                'email': email1,
                'password': password1,
                'remember': remember1,
                'loginAccount': 'login customer'
            },
            dataType: "JSON",
            success: function(jsonStr) {
                var res_data = JSON.stringify(jsonStr);
                var response = JSON.parse(res_data);
                var responseData = response['responseData'];
                if ((responseData != null) && (responseData == 'logged in successfully')) {
                    $('#responseMessage').html('You have successfully logged in');
                    setTimeout(function(){ window.location.href = baseUrl;  }, 1500);;
                } else if (((response['emailError'] != null) && (response['emailError'] == 'Please enter a valid email address')) || ((response['passwordError'] != null) && (response['passwordError'] == 'Invalid password')) || ((response['statusError'] != null) && (response['statusError'] == 'Your account temporarily deactivated'))) {
                    if ((response['emailError'] != null) && (response['emailError'] == 'Please enter a valid email address')) {
                        $('#userLoginForm #email').addClass('has-error');
                        $('#userLoginForm #emailError').show().text('Please enter a valid email address');
                    }
                    if ((response['passwordError'] != null) && (response['passwordError'] == 'Invalid password')) {
                        $('#userLoginForm #password').addClass('has-error');
                        $('#userLoginForm #passwordError').show().text('Invalid password');
                    }
                    if ((response['statusError'] != null) && (response['statusError'] == 'Your account temporarily deactivated')) {
                        $('#responseMessage').html('Your account temporarily deactivated');
                    }
                } else {
                    $('#responseMessage').html(responseData);
                }
                $('#userLoginForm #ajax-loader').hide();
            }
        });
    }
//---------------------------------Sign In End-------------------------------------------//
//---------------------------------Sign Up Start-------------------------------------------//
    $('#userForgotPasswordForm #email').focusout(function() {
        var mailValue = $(this).val();
        if ((mailValue == '') || (!isEmail($('#userForgotPasswordForm #email').val()))) {
            $('#userForgotPasswordForm #email').addClass('has-error');
            $('#userForgotPasswordForm #emailError').show().text('Please enter a valid email address');
        } else if ((mailValue != '') && (isEmail($('#userForgotPasswordForm #email').val()))) {
            $('#userForgotPasswordForm #email').removeClass('has-error');
            $('#userForgotPasswordForm #emailError').hide().text('');
        }
    });

    $('#userForgotPassword').on('click', function(event) {
        event.preventDefault();
        $('#responseMSG').html('');
        $('#userForgotPasswordForm #email').trigger("focusout");
        var mailValue = $("#userForgotPasswordForm #email").val();
        if ((mailValue != '') && (isEmail($('#userForgotPasswordForm #email').val()))) {
            $('#userForgotPasswordForm #email').removeClass('has-error');
            $('#userForgotPasswordForm #emailError').text('').hide();
            userForgotPassword();
        }
    });

    function userForgotPassword() {
        $('#responseMSG').html('');
        $('#userForgotPasswordForm #ajax-loader').show();
        var token = $('#userForgotPasswordForm #_token').val();
        $.ajax({
            url: baseUrl + "/userforgotPassword",
            type: "POST",
            data: {
                '_token': token,
                'email': $('#userForgotPasswordForm #email').val(),
                'forgotPassword': 'Forgot Password'
            },
            dataType: "JSON",
            success: function(jsonStr) {
                var res_data = JSON.stringify(jsonStr);
                var response = JSON.parse(res_data);
                var status = response['status'];
                var responseData = response['responseData'];
                if (status == 'success') {
                    $('#responseMSG').html('We have received a request for forgot password & sent you a reset password link');
                } else {
                    $('#responseMSG').html('No matching email address was found. Check your spelling and try again.');
                }
                $('#userForgotPasswordForm #ajax-loader').hide();
            }
        });
    }
//-----------------------------------Sign Up End-------------------------------------------//
});
</script>