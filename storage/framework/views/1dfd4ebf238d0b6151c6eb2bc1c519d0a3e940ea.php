<!DOCTYPE html>
<html lang="<?php echo e(config('app.locale')); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title><?php echo e((isset($bladeVar['page']['title']) ? $bladeVar['page']['title'] : '')); ?></title>
    <!-- <link rel="shortcut icon" href="<?php echo e(asset('favicon.ico')); ?>" type="image/x-icon"> -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('assets/img/favicon.ico')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/thirdparty/fontawesome.css')); ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(asset('css/thirdparty/bootstrap.min.css')); ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(asset('css/thirdparty/bootstrap-datetimepicker.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/boot.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/resp.css')); ?>" rel="stylesheet">
    <script>
        window.webApp = <?php echo json_encode([
        	'csrfToken' => csrf_token(),
        ]); ?>;
    </script>
    <?php $__env->startSection('head'); ?><?php echo $__env->yieldSection(); ?>
</head>
<body<?php echo (isset($bladeVar['page']['bodyClass']) ? ' class="'.$bladeVar['page']['bodyClass'].'"' : ''); ?>>
    <?php $__env->startSection('header'); ?>
        <?php if (! (!Auth::check())): ?>
            <nav class="navbar navbar-toggleable-md sticky-top navbar-light bg-faded mb-3">
                <button class="navbar-toggler navbar-toggler-right rounded-0" type="button" data-toggle="collapse" data-target="#mainNav" aria-controls="mainNav" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars"></i></button>
                <a class="navbar-brand" href="<?php echo e(url('/')); ?>"><img width="97px" src="<?php echo e(asset('assets/img/logo.png')); ?>" alt="Glamour Jewellery" class="d-inline-block align-top 1hidden-xs-up"></a>
                <div class="collapse navbar-collapse" id="mainNav">
                   <?php if(Auth::user()->type == 'seller'): ?>

                   <?php else: ?>
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle<?php echo e($bladeVar['page']['activeNav']['nav'] == 'master' ? ' active' : ''); ?>" href="javascript:void(0);" id="mastersDropNav" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Masters</a>
							<div class="dropdown-menu" aria-labelledby="mastersDropNav">
                                <a class="dropdown-item<?php echo e($bladeVar['page']['activeNav']['sub'] == 'home_page_images' ? ' active' : ''); ?>" href="<?php echo e(route('homepageimage.index')); ?>">Home Slider</a>
                                <a class="dropdown-item<?php echo e($bladeVar['page']['activeNav']['sub'] == 'banner_images' ? ' active' : ''); ?>" href="<?php echo e(route('bannerimages.index')); ?>">Banner Images</a>
                                <a class="dropdown-item<?php echo e($bladeVar['page']['activeNav']['sub'] == 'banner_slider' ? ' active' : ''); ?>" href="<?php echo e(route('bannerslider.index')); ?>">Banner Slider</a>
                                <a class="dropdown-item<?php echo e($bladeVar['page']['activeNav']['sub'] == 'single_banner_images' ? ' active' : ''); ?>" href="<?php echo e(route('singlebannerimage.index')); ?>">Single Banner Slider</a>
                                <a class="dropdown-item<?php echo e($bladeVar['page']['activeNav']['sub'] == 'blog_category' ? ' active' : ''); ?>" href="<?php echo e(route('blog_category.index')); ?>">Blog Categories</a>
                                <a class="dropdown-item<?php echo e($bladeVar['page']['activeNav']['sub'] == 'tags' ? ' active' : ''); ?>" href="<?php echo e(route('tags.index')); ?>">Blog Tags</a>
                                <a class="dropdown-item<?php echo e($bladeVar['page']['activeNav']['sub'] == 'product_category' ? ' active' : ''); ?>" href="<?php echo e(route('product_category.index')); ?>">Product Categories</a>
                                <a class="dropdown-item<?php echo e($bladeVar['page']['activeNav']['sub'] == 'product_subcategory' ? ' active' : ''); ?>" href="<?php echo e(route('product_subcategory.index')); ?>">Product Subcategories</a>
                                <a class="dropdown-item<?php echo e($bladeVar['page']['activeNav']['sub'] == 'brand' ? ' active' : ''); ?>" href="<?php echo e(route('brand.index')); ?>">Product Brands</a>
                                <a class="dropdown-item<?php echo e($bladeVar['page']['activeNav']['sub'] == 'color' ? ' active' : ''); ?>" href="<?php echo e(route('color.index')); ?>">Product Color</a>
                                <a class="dropdown-item<?php echo e($bladeVar['page']['activeNav']['sub'] == 'size' ? ' active' : ''); ?>" href="<?php echo e(route('size.index')); ?>">Product Size</a>
                            </div>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo e(route('coupons.index')); ?>">Coupons</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo e(route('blogs.index')); ?>">Blogs</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo e(route('cms.index')); ?>">CMS</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo e(route('product.index')); ?>">Products</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo e(route('team.index')); ?>">Team</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo e(route('testimonial.index')); ?>">Testimonials</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo e(route('setting.index')); ?>">Site Settings</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo e(route('siteusers.index')); ?>">Users</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo e(route('orders.index')); ?>">Orders</a></li>
                        <!-- <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle<?php echo e($bladeVar['page']['activeNav']['nav'] == 'operations' ? ' active' : ''); ?>" href="javascript:void(0);" id="usersDropNav" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Users</a>
                            <div class="dropdown-menu" aria-labelledby="operationsDropNav">
                                <a class="dropdown-item" href="<?php echo e(route('siteusers.index')); ?>">Site Users</a>
                                <a class="dropdown-item" href="<?php echo e(route('tenant.index')); ?>">Tenants</a>
                                <a class="dropdown-item" href="<?php echo e(route('leasing.index')); ?>">Leasing</a>
                                <a class="dropdown-item" href="<?php echo e(route('leasingenquiry.index')); ?>">Leasing Enquiry</a>
                            </div>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo e(route('event.index')); ?>">Events</a></li>
						 <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle<?php echo e($bladeVar['page']['activeNav']['nav'] == 'operations' ? ' active' : ''); ?>" href="javascript:void(0);" id="offerDropNav" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Offers</a>
                            <div class="dropdown-menu" aria-labelledby="operationsDropNav">
                                <a class="dropdown-item" href="<?php echo e(route('offer.index')); ?>">Generic Offers</a>
                                <a class="dropdown-item" href="<?php echo e(route('privilege_customer_offer.index')); ?>">Privilege Customer Offers</a>
                            </div>
						</li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo e(route('cms.index')); ?>">CMS</a></li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle<?php echo e($bladeVar['page']['activeNav']['nav'] == 'operations' ? ' active' : ''); ?>" href="javascript:void(0);" id="operationsDropNav" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Shops</a>
                            <div class="dropdown-menu" aria-labelledby="operationsDropNav">
                                <a class="dropdown-item" href="<?php echo e(route('product.index')); ?>">Add Product</a>
                                <a class="dropdown-item" href="<?php echo e(route('orders.index')); ?>">Orders</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle<?php echo e($bladeVar['page']['activeNav']['nav'] == 'operations' ? ' active' : ''); ?>" href="javascript:void(0);" id="operationsDropNav" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">OLD SOUQ</a>
                            <div class="dropdown-menu" aria-labelledby="operationsDropNav">
                                <a class="dropdown-item" href="<?php echo e(route('oldsouq.index')); ?>">Manage Old Souq</a>
                            </div>
                        </li>
                         <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle<?php echo e($bladeVar['page']['activeNav']['nav'] == 'operations' ? ' active' : ''); ?>" href="javascript:void(0);" id="operationsDropNav" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dinning</a>
                            <div class="dropdown-menu" aria-labelledby="operationsDropNav">
                                <a class="dropdown-item" href="<?php echo e(route('dinning.index')); ?>">Add Dinning</a>
                                <a class="dropdown-item" href="<?php echo e(route('restaurent.index')); ?>">Add Dinning Shop</a>

                            </div>
                        </li>
                         <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle<?php echo e($bladeVar['page']['activeNav']['nav'] == 'operations' ? ' active' : ''); ?>" href="javascript:void(0);" id="operationsDropNav" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Entertainment</a>
                            <div class="dropdown-menu" aria-labelledby="operationsDropNav">
                                <a class="dropdown-item" href="<?php echo e(route('entertainment.index')); ?>">Add Entertainment</a>
                                <a class="dropdown-item" href="<?php echo e(route('show.index')); ?>">Add Show</a>

                            </div>
                        </li>
						<li class="nav-item dropdown"><a class="nav-link dropdown-toggle<?php echo e($bladeVar['page']['activeNav']['nav'] == 'galleries' ? ' active' : ''); ?>" href="javascript:void(0);" id="galleriesDropNav" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Gallery</a>
                        <div class="dropdown-menu" aria-labelledby="galleriesDropNav">
                                <a class="dropdown-item" href="<?php echo e(route('album.index')); ?>">Add Albums</a>
                                 <a class="dropdown-item" href="<?php echo e(route('photo.index')); ?>">Add Photos</a>
                            </div>
                        </li>
						<li class="nav-item dropdown"><a class="nav-link dropdown-toggle<?php echo e($bladeVar['page']['activeNav']['nav'] == 'support' ? ' active' : ''); ?>" href="javascript:void(0);" id="supportDropNav" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Messages</a>
                        <div class="dropdown-menu" aria-labelledby="supportDropNav">
                                <a class="dropdown-item" href="<?php echo e(route('message.index')); ?>">Push Notification</a>
                                <a class="dropdown-item" href="<?php echo e(route('message.index')); ?>">Inbox</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown"><a class="nav-link dropdown-toggle<?php echo e($bladeVar['page']['activeNav']['nav'] == 'support' ? ' active' : ''); ?>" href="javascript:void(0);" id="supportDropNav" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Support</a>
                        <div class="dropdown-menu" aria-labelledby="supportDropNav">
                                <a class="dropdown-item" href="<?php echo e(route('incident.index')); ?>">Incidents</a>
                                <a class="dropdown-item" href="<?php echo e(route('lostfound.index')); ?>">Lost Found</a>
                                <a class="dropdown-item" href="<?php echo e(route('management.index')); ?>">Management Email</a>
                            </div>
                        </li>
						<li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle<?php echo e($bladeVar['page']['activeNav']['nav'] == 'settings' ? ' active' : ''); ?>" href="javascript:void(0);" id="settingsDropNav" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Settings</a>
                            <div class="dropdown-menu" aria-labelledby="settingsDropNav">
                                 <a class="dropdown-item" href="<?php echo e(route('setting.index')); ?>">Mall Settings</a>
                                <a class="dropdown-item" href="<?php echo e(route('cache.flush')); ?>">Flush Cache</a>
                            </div>
                        </li> -->
                    </ul>
                    <span class="navbar-text hidden-md-down"><small>Signed in as</small></span>
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="http://example.com" id="signinUserNav" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo e(Auth::user()->name); ?></a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="signinUserNav">
                                <!-- <a class="dropdown-item" href="<?php echo e(route('setting.index')); ?>">Settings</a> -->
                                <a class="dropdown-item" title="Sign out" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" class="hidden-xs-up"><?php echo e(csrf_field()); ?></form>
                            </div>
                        </li>
                    </ul>
                    <?php endif; ?>
                    
                </div>
            </nav>

            <?php if(isset($bladeVar['page']['breadcrumb'])): ?>
                <?php if(count($bladeVar['page']['breadcrumb']) > 0): ?>
                    <div class="container-fluid">
                        <nav class="breadcrumb bg-faded py-2">
                            <?php $__currentLoopData = $bladeVar['page']['breadcrumb']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $breadcrumbKey => $breadcrumbVal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if(!empty($breadcrumbVal)): ?>
                                    <a class="breadcrumb-item" href="<?php echo e(url($breadcrumbVal)); ?>"><?php echo e($breadcrumbKey); ?></a>
                                <?php else: ?>
                                    <span class="breadcrumb-item active"><?php echo e($breadcrumbKey); ?></span>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </nav>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>
    <?php echo $__env->yieldSection(); ?>

    <?php echo $__env->yieldContent('content'); ?>

    <?php $__env->startSection('footer'); ?>
        <div class="footer-border pt-3 mt-5 mb-3">
            <small class="d-block text-center">&copy; <?php echo e(date('Y')); ?> All rights reserved</small>
        </div>
    <?php echo $__env->yieldSection(); ?>

    <div class="loadingScreen hidden-xs-up">
        <div class="my-5 text-center mx-auto h1" id="loadingScreenDiv"><i class="fa fa-spinner fa-spin"></i> Loading...</div>
    </div>

    <!-- Footer scripts -->
    <script src="<?php echo e(asset('js/jquery-3.2.1.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/ajax-tether-1.4.min.js')); ?>" ></script>
    <script src="<?php echo e(asset('js/bootstrap-4.0.0.min.js')); ?>" ></script>
    <script src="<?php echo e(asset('js/boot.js?v=1')); ?>"></script>
    <?php $__env->startSection('foot'); ?><?php echo $__env->yieldSection(); ?>
</body>
</html>