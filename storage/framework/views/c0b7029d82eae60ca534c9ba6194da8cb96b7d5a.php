<?php $__env->startSection('content'); ?>
    <div class="container-fluid" id="pageContainer">
        <div class="row" id="pageContent">
            <div class="col-12 col-md-8 offset-md-2">
                <div class="float-right mt-2 hidden-xs-down">
                    <a href="<?php echo e(route('orderdetail.index')); ?>" class="btn btn-secondary btn-search" title="Search Order" data-search="<?php echo e(!empty(Request::input('q')) ? 'yes' : ''); ?>"><i class="fa fa-search"></i><span<?php echo !empty(Request::input('q')) ? '' : ' class="hidden-xs-up"'; ?>> Close</span></a>
                    <a href="<?php echo e(route('orderdetail.index')); ?>" class="btn btn-secondary" role="button" title="Refresh Order List"><i class="fa fa-refresh"></i></a>
                </div>
                <div class="float-right mt-1 hidden-sm-up">
                    <a href="<?php echo e(route('orderdetail.index')); ?>" class="btn btn-secondary btn-sm btn-search" title="Search Order" data-search="<?php echo e(!empty(Request::input('q')) ? 'yes' : ''); ?>"><i class="fa fa-search"></i><span<?php echo !empty(Request::input('q')) ? '' : ' class="hidden-xs-up"'; ?>> Close</span></a>
                    <a href="<?php echo e(route('orderdetail.index')); ?>" class="btn btn-secondary btn-sm" role="button" title="Refresh Order List"><i class="fa fa-refresh"></i></a>
                </div>
                <h1 class="hidden-xs-down"><?php echo e(Request::input('q') ? 'Search ' : ''); ?>Order <span class="text-muted h5">(total <?php echo e(number_format($bladeVar['total_count'])); ?>)</span></h1>
                <h1 class="hidden-sm-up h3"><?php echo e(Request::input('q') ? 'Search ' : ''); ?>Order <br><span class="text-muted h6">(total <?php echo e(number_format($bladeVar['total_count'])); ?>)</span></h1>
                <div class="clearfix alertArea"></div>
                <div class="card mb-2 searchContainer<?php echo e(Request::input('q') ? '' : ' hidden-xs-up'); ?>">
                    <div class="card-block">
                        <form method="get" action="<?php echo e(route('orderdetail.search')); ?>" class="m0 p0 form-search">
                            <div class="form-group mb-0">
                                <label class="form-control-label" for="q"><strong>Search by Order name</strong></label>
                                <input type="text" class="form-control" name="q" value="<?php echo e(Request::input('q')); ?>">
                                <small class="form-text text-muted">Type order name and hit "Enter" key to search</small>
                            </div>
                        </form>
                    </div>
                </div>
                <?php if($bladeVar['total_count'] > 0): ?>
                    <table class="table table-bordered table-hover table-sm">
                        <thead class="bg-faded">
                             <tr>
                                <th class="text-center" style="width: 4rem;"><nobr><a href="<?php echo e(url()->current()); ?>?<?php echo e(Request::input('q') ? 'q='.Request::input('q').'&' : ''); ?>sort=id&dir=<?php echo e(Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'desc' : 'asc' : 'asc'); ?>">Id</a> <i class="fa fa-<?php echo e(Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'sort-numeric-asc' : 'sort-numeric-desc' : 'sort'); ?>"></i></nobr></th>
                                <th><nobr>Order Placed</nobr></th>
                                <th><nobr>Name</nobr></th>
                                <th><nobr>Image</nobr></th>
								<th><nobr>Price</nobr></th>
								<th><nobr>Quantity</nobr></th>
								<th><nobr>Order Status</nobr></th>
                                <th class="w-td-action">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $bladeVar['results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr<?php echo $rs->deleted_at != null ? ' class="table-danger"' : ''; ?>>
                                    <td class="text-center"><span<?php echo $rs->deleted_at != null ? ' class="badge badge-danger"' : ''; ?>><?php echo e($rs->orderUniqid); ?></span></td>
                                    <td><?php echo e(date('d F Y', strtotime($rs->created_at))); ?></td>
                                    <td><?php echo e($rs->product_name); ?></td>
									<td><img src="<?php echo e(asset('images/products/thumbnails').'/'.$rs->product_image); ?>" class="thumbnail" width="80" /></td>
									<td>QR <?php echo e($rs->price); ?></td>
                                    <td><?php echo e($rs->quantity); ?></td>
                                    <!-- <td><?php echo e($rs->order_status); ?></td> -->
                                    <td>
                                        <form method="post" class="updateStatus">
                                            <div class="form-group">
                                                <input type="hidden" class="form-control" name="id" id="id" value="<?php echo e($rs->_id); ?>">
                                                <select name="order_status" class="form-control">
                                                    <option value=""<?php echo e(empty(old('order_status')) ? ' selected="selected"' : ''); ?>></option>
                                                    <?php $__currentLoopData = $bladeVar['status']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $order_status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($order_status); ?>"<?php echo e($order_status == old('order_status', $rs->order_status) ? ' selected="selected"' : ''); ?>><?php echo e($order_status); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                                <small class="form-control-feedback"></small>
                                            </div>
                                        </form>
                                    </td>
                                    <td class="text-center text-sm-left">
                                        <div class="hidden-xs-down">
                                            <a href="<?php echo e(route('orderdetail.show', $rs->_id)); ?>" class="btn btn-sm btn-info" title="View" data-toggle="modal" data-target="#viewModal"><i class="fa fa-eye"></i></a> 
                                            <!-- <a href="<?php echo e(route('orderdetail.edit', $rs->_id)); ?>" class="btn btn-sm btn-info<?php echo e($rs->deleted_at != null ? ' disabled' : ''); ?>" title="Edit" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i></a>   -->
                                        </div>
                                        <div class="btn-group hidden-sm-up">
                                            <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Actions
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a href="<?php echo e(route('orderdetail.show', $rs->_id)); ?>" class="dropdown-item" data-toggle="modal" data-target="#viewModal"><i class="fa fa-eye text-info"></i> View</a>
                                                <!-- <a href="<?php echo e(route('orderdetail.edit', $rs->_id)); ?>" class="dropdown-item<?php echo e($rs->deleted_at != null ? ' disabled' : ''); ?>" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil text-<?php echo e($rs->deleted_at != null ? 'muted' : 'info'); ?>"></i> Edit</a>  -->
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                    <div class="mt-4">
                        <?php echo e($bladeVar['results']->appends(Request::query())->links()); ?>

                        <small class="d-block text-center text-muted">(showing 20 results per page)</small>
                    </div>
                <?php else: ?>
                    <div class="alert alert-success">
                        <strong>No records found!</strong>
                        <?php if(!empty(Request::input('q'))): ?>
                            No matching your search criteria. 
                        <?php else: ?> 
                            No order added yet 
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <!-- Deletetion code with Form -->
                <form class="formDelete">
                    <?php echo e(method_field('DELETE')); ?>

                </form>

                <!-- View modal code: START -->
                <div class="modal" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="viewModalTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="viewModalTitle">View Order</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            </div>
                            <div class="modal-footer hidden-xs-up">
                                <!-- <a href="javascript:void(0);" class="btn btn-info btn-edit" title="Edit" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i> Edit</a> -->
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- View modal code: END -->

                <!-- Edit modal code: START -->
                <div class="modal" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalTitle" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="editModalTitle">Edit Order</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            </div>
                            <div class="modal-footer hidden-xs-up">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Edit modal code: END -->
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('foot'); ?>
    <script type="text/javascript">
        var pageUrl = '<?php echo e(route('orderdetail.index')); ?>';
        var ajaxLoadUrl = '<?php echo e(url()->full()); ?>';
        var pageContainer = '#pageContainer';
        var pageContent = '#pageContent';
    </script>
     <script src="<?php echo e(asset('js/jquery.form.js')); ?>"></script>
	<script src="<?php echo e(asset('js/orderdetail.js?v=1')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>