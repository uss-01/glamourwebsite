<?php echo $__env->make('frontend.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<style type="text/css">
.addWishlist{
    color: red;
}
</style>
<main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">shop</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- page main wrapper start -->
        <div class="shop-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <!-- sidebar area start -->
                    <div class="col-lg-3 order-2 order-lg-1">
                        <aside class="sidebar-wrapper">
                            <!-- single sidebar start -->
                            <?php if(!empty($bladeVar['productsCategories'])): ?>
                            <div class="sidebar-single">
                                <h5 class="sidebar-title">categories</h5>
                                <div class="sidebar-body">
                                    <ul class="checkbox-container categories-list">
                                        <?php $__currentLoopData = $bladeVar['productsCategories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($category->deleted_at == NULL): ?>
                                            <?php if($category->categoryProducts != 0): ?>
                                            <li>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input productCategory" name="productCategory[]" value="<?php echo e($category->_id); ?>" id="productCategory<?php echo e($category->_id); ?>" <?php if(!empty($_GET['category'])){ $categoryArray = explode(",",$_GET['category']); if(in_array($category->_id, $categoryArray)){ ?>checked<?php }else{ } } ?>>
                                                    <label class="custom-control-label" for="productCategory<?php echo e($category->_id); ?>"><?php echo e($category->name); ?> (<?php echo e($category->categoryProducts); ?>)</label>
                                                </div>
                                            </li>
                                            <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            </div>
                            <?php endif; ?>
                            <!-- single sidebar end -->

                            <!-- single sidebar start -->
                            <?php if(!empty($bladeVar['productsMinMaxPrice'])): ?>
                            <div class="sidebar-single">
                                <h5 class="sidebar-title">price</h5>
                                <div class="sidebar-body">
                                    <div class="price-range-wrap">
                                        <?php $__currentLoopData = $bladeVar['productsMinMaxPrice']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $price): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="price-range" data-min="<?php echo e(str_replace('.00','',$price->minPrice)); ?>" data-max="<?php echo e(str_replace('.00','',$price->maxPrice)); ?>"></div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <div class="range-slider">
                                            <form action="#" class="d-flex align-items-center justify-content-between">
                                                <div class="price-input">
                                                    <label for="amount">Price: </label>
                                                    <input type="text" id="amount">
                                                </div>
                                                <button class="filter-btn" id="priceFilter">filter</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                            <!-- single sidebar end -->

                            <!-- single sidebar start -->
                            <?php if(!empty($bladeVar['productsBrands'])): ?>
                            <div class="sidebar-single">
                                <h5 class="sidebar-title">Brand</h5>
                                <div class="sidebar-body">
                                    <ul class="checkbox-container categories-list">
                                        <?php $__currentLoopData = $bladeVar['productsBrands']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($brand->deleted_at == NULL): ?>
                                            <?php if($brand->brandProducts != 0): ?>
                                            <li>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input productBrand" name="productBrand[]" value="<?php echo e($brand->_id); ?>" id="productBrand<?php echo e($brand->_id); ?>" <?php if(!empty($_GET['brand'])){ $brandArray = explode(",",$_GET['brand']); if(in_array($brand->_id, $brandArray)){ ?>checked<?php }else{ } } ?>>
                                                    <label class="custom-control-label" for="productBrand<?php echo e($brand->_id); ?>"><?php echo e($brand->name); ?> (<?php echo e($brand->brandProducts); ?>)</label>
                                                </div>
                                            </li>
                                            <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            </div>
                            <?php endif; ?>
                            <!-- single sidebar end -->

                            <!-- single sidebar start -->
                            <?php if(!empty($bladeVar['productsColors'])): ?>
                            <div class="sidebar-single">
                                <h5 class="sidebar-title">color</h5>
                                <div class="sidebar-body">
                                    <ul class="checkbox-container categories-list">
                                        <?php $__currentLoopData = $bladeVar['productsColors']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($color->deleted_at == NULL): ?>
                                            <?php if($color->colorProducts != 0): ?>
                                            <li>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input productColor" name="productColor[]" value="<?php echo e($color->id); ?>" id="productColor<?php echo e($color->id); ?>" <?php if(!empty($_GET['color'])){ $colorArray = explode(",",$_GET['color']); if(in_array($color->id, $colorArray)){ ?>checked<?php }else{ } } ?>>
                                                    <label class="custom-control-label" for="productColor<?php echo e($color->id); ?>"><?php echo e($color->name); ?> (<?php echo e($color->colorProducts); ?>)</label>
                                                </div>
                                            </li>
                                            <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            </div>
                            <?php endif; ?>
                            <!-- single sidebar end -->

                            <!-- single sidebar start -->
                            <?php if(!empty($bladeVar['productsSizes'])): ?>
                            <div class="sidebar-single">
                                <h5 class="sidebar-title">size</h5>
                                <div class="sidebar-body">
                                    <ul class="checkbox-container categories-list">
                                        <?php $__currentLoopData = $bladeVar['productsSizes']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $size): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($size->deleted_at == NULL): ?>
                                            <?php if($size->sizeProducts != 0): ?>
                                            <li>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input productSize" name="productSize[]" value="<?php echo e($size->id); ?>" id="productSize<?php echo e($size->id); ?>" <?php if(!empty($_GET['size'])){ $sizeArray = explode(",",$_GET['size']); if(in_array($size->id, $sizeArray)){ ?>checked<?php }else{ } } ?>>
                                                    <label class="custom-control-label" for="productSize<?php echo e($size->id); ?>"><?php echo e($size->name); ?> (<?php echo e($size->sizeProducts); ?>)</label>
                                                </div>
                                            </li>
                                            <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            </div>
                            <?php endif; ?>
                            <!-- single sidebar end -->

                            <!-- single sidebar start -->
                            <!-- <div class="sidebar-banner">
                                <div class="img-container">
                                    <a href="#">
                                        <img src="assets/img/banner/sidebar-banner.jpg" alt="">
                                    </a>
                                </div>
                            </div> -->
                            <!-- single sidebar end -->
                        </aside>
                    </div>
                    <!-- sidebar area end -->

                    <!-- shop main wrapper start -->
                    <div class="col-lg-9 order-1 order-lg-2">
                        <div class="shop-product-wrapper">
                            <!-- shop product top wrap start -->
                            <div class="shop-top-bar">
                                <div class="row align-items-center">
                                    <div class="col-lg-7 col-md-6 order-2 order-md-1">
                                        <div class="top-bar-left">
                                            <div class="product-view-mode">
                                                <a class="active" href="#" data-target="grid-view" data-toggle="tooltip" title="Grid View"><i class="fa fa-th"></i></a>
                                                <a href="#" data-target="list-view" data-toggle="tooltip" title="List View"><i class="fa fa-list"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-6 order-1 order-md-2">
                                        <div class="top-bar-right">
                                            <div class="product-short">
                                                <p>Sort By : </p>
                                                <select class="nice-select" id="sortby" name="sortby">
                                                    <option value="trending" <?php if(!empty($_GET['sortby']) && ($_GET['sortby'] == 'trending')){ ?>selected<?php }else{ } ?>>Relevance</option>
                                                    <option value="a-z" <?php if(!empty($_GET['sortby']) && ($_GET['sortby'] == 'a-z')){ ?>selected<?php }else{ } ?>>Name (A - Z)</option>
                                                    <option value="z-a" <?php if(!empty($_GET['sortby']) && ($_GET['sortby'] == 'z-a')){ ?>selected<?php }else{ } ?>>Name (Z - A)</option>
                                                    <option value="low-high" <?php if(!empty($_GET['sortby']) && ($_GET['sortby'] == 'low-high')){ ?>selected<?php }else{ } ?>>Price (Low &gt; High)</option>
                                                    <option value="high-low" <?php if(!empty($_GET['sortby']) && ($_GET['sortby'] == 'high-low')){ ?>selected<?php }else{ } ?>>Price (High &gt; Low)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- shop product top wrap start -->
                            <?php if(!empty($bladeVar['product_results']->count() > 0)): ?>
                            <!-- product item list wrapper start -->
                            <div class="shop-product-wrap grid-view row mbn-30">
                                <!-- product single item start -->
                                <?php $__currentLoopData = $bladeVar['product_results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							        <?php if($product->deleted_at == NULL): ?> 
                                    <div class="col-md-4 col-sm-6">
                                        <!-- product grid start -->
                                        <div class="product-item">
                                            <figure class="product-thumb">
                                                <a href="<?php echo e(url('/shop/'.$product->slug)); ?>">
                                                    <img class="pri-img" src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
										            <img class="sec-img" src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
                                                </a>
                                                <div class="product-badge">
                                                    <?php if(($product->new_arrival == '1') && ($product->on_sale == '1')): ?>
                                                    <div class="product-label new">
                                                        <span>new</span>
                                                    </div>
                                                    <div class="product-label discount">
                                                        <span>sale</span>
                                                    </div>
                                                    <?php elseif($product->new_arrival == '1'): ?>
                                                    <div class="product-label new">
                                                        <span>new</span>
                                                    </div>
                                                    <?php elseif($product->on_sale == '1'): ?>
                                                    <div class="product-label new">
                                                        <span>sale</span>
                                                    </div>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="button-group">
                                                <?php 
                                                $arrayWishlist = array();
                                                if(!empty($bladeVar['userWishlistData'])){ 
                                                    foreach($bladeVar['userWishlistData'] as $wishlist){
                                                        $arrayWishlist[] = $wishlist->product_id;
                                                    } 
                                                } 
                                                if (in_array($product->_id, $arrayWishlist)) { ?>
                                                    <a href="#" class="addtowishlist" id="addtowishlist<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like addWishlist"></i></a>
                                                <?php }else{ ?>
                                                    <a href="#" class="addtowishlist" id="addtowishlist<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like"></i></a>
                                                <?php } ?>
                                                </div>
                                                <?php if(!empty($product->product_quantity) && ($product->product_quantity > 0)): ?>
                                                    <?php if($product->add_to_cart == '1'): ?>
                                                    <div class="cart-hover">
                                                        <?php $arrayCartList = array();
                                                        if(!empty($bladeVar['userCartData'])){ 
                                                            foreach($bladeVar['userCartData'] as $cartList){
                                                                $arrayCartList[] = $cartList->product_id;
                                                            } 
                                                        } 
                                                        if (in_array($product->_id, $arrayCartList)) { ?>
                                                            <button class="btn btn-cart addtocart disabled" id="addtocart<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>">Go to cart</button>
                                                        <?php }else{ ?>
                                                            <button class="btn btn-cart addtocart" id="addtocart<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>" data-cid="<?php echo e($product->product_color_id); ?>" data-sid="<?php echo e($product->product_size_id); ?>">add to cart</button>
                                                        <?php } ?>
                                                    </div>
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <div class="cart-hover">
                                                        <button class="btn btn-cart disabled">Out of stock</button>
                                                    </div>
                                                <?php endif; ?> 
                                            </figure>
                                            <div class="product-caption text-center">
                                                <div class="product-identity">
                                                    <p class="manufacturer-name"><a href="<?php echo e(url('/shop/?category='.$product->product_category->_id)); ?>"><?php echo e($product->product_category->name); ?></a></p>
                                                </div>
                                                <?php
                                                $colorArray = array(); 
                                                $colorArray[0] = $product->product_color_id;
                                                if(!empty($product->variations)){
                                                    $variations = json_decode($product->variations); 
                                                    foreach($variations as $variation){
                                                        $colorArray[] = $variation->color;
                                                    }
                                                }
                                                $getColorArray = array_unique($colorArray);
                                                if(!empty($getColorArray)){ 
                                                ?>
                                                <ul class="color-categories">
                                                <?php foreach($getColorArray as $color){ 
                                                    $allColors = $bladeVar['color_results'];
                                                    if(!empty($allColors)){
                                                        foreach($allColors as $getColor){
                                                            if($getColor->id == $color){ ?>
                                                                <li>
                                                                    <a href="#" style="background-color: <?php echo $getColor->color_code; ?>;" title="<?php echo $getColor->name; ?>"></a>
                                                                </li>   
                                                            <?php }
                                                        }
                                                    }
                                                } ?>
                                                </ul>
                                                <?php } ?>	
                                                <h6 class="product-name">
                                                    <a href="<?php echo e(url('/shop/'.$product->slug)); ?>"><?php echo e($product->name); ?></a>
                                                </h6>
                                                <div class="price-box">
                                                    <?php if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')): ?>
                                                    <span class="price-regular">$<?php echo e($product->discounted_price); ?></span>
                                                    <span class="price-old"><del>$<?php echo e($product->price); ?></del></span>
                                                    <?php else: ?>
                                                    <span class="price-regular">$<?php echo e($product->price); ?></span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- product grid end -->

                                        <!-- product list item end -->
                                        <div class="product-list-item">
                                            <figure class="product-thumb">
                                                <a href="<?php echo e(url('/shop/'.$product->slug)); ?>">
                                                    <img class="pri-img" src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
										            <img class="sec-img" src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
                                                </a>
                                                <div class="product-badge">
                                                    <?php if(($product->new_arrival == '1') && ($product->on_sale == '1')): ?>
                                                    <div class="product-label new">
                                                        <span>new</span>
                                                    </div>
                                                    <div class="product-label discount">
                                                        <span>sale</span>
                                                    </div>
                                                    <?php elseif($product->new_arrival == '1'): ?>
                                                    <div class="product-label new">
                                                        <span>new</span>
                                                    </div>
                                                    <?php elseif($product->on_sale == '1'): ?>
                                                    <div class="product-label new">
                                                        <span>sale</span>
                                                    </div>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="button-group">
                                                <?php 
                                                $arrayWishlist = array();
                                                if(!empty($bladeVar['userWishlistData'])){ 
                                                    foreach($bladeVar['userWishlistData'] as $wishlist){
                                                        $arrayWishlist[] = $wishlist->product_id;
                                                    } 
                                                } 
                                                if (in_array($product->_id, $arrayWishlist)) { ?>
                                                    <a href="#" class="addtowishlist" id="addtowishlist<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like addWishlist"></i></a>
                                                <?php }else{ ?>
                                                    <a href="#" class="addtowishlist" id="addtowishlist<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like"></i></a>
                                                <?php } ?>
                                                </div>
                                                <?php if(!empty($product->product_quantity) && ($product->product_quantity > 0)): ?>
                                                    <?php if($product->add_to_cart == '1'): ?>
                                                    <div class="cart-hover">
                                                        <?php $arrayCartList = array();
                                                        if(!empty($bladeVar['userCartData'])){ 
                                                            foreach($bladeVar['userCartData'] as $cartList){
                                                                $arrayCartList[] = $cartList->product_id;
                                                            } 
                                                        } 
                                                        if (in_array($product->_id, $arrayCartList)) { ?>
                                                            <button class="btn btn-cart addtocart disabled" id="addtocart<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>">Go to cart</button>
                                                        <?php }else{ ?>
                                                            <button class="btn btn-cart addtocart" id="addtocart<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>" data-cid="<?php echo e($product->product_color_id); ?>" data-sid="<?php echo e($product->product_size_id); ?>">add to cart</button>
                                                        <?php } ?>
                                                    </div>
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <div class="cart-hover">
                                                        <button class="btn btn-cart disabled">Out of stock</button>
                                                    </div>
                                                <?php endif; ?> 
                                            </figure>
                                            <div class="product-content-list">
                                                <div class="manufacturer-name">
                                                    <a href="<?php echo e(url('/shop/?category='.$product->product_category->_id)); ?>"><?php echo e($product->product_category->name); ?></a>
                                                </div>
                                                <?php
                                                $colorArray = array(); 
                                                $colorArray[0] = $product->product_color_id;
                                                if(!empty($product->variations)){
                                                    $variations = json_decode($product->variations); 
                                                    foreach($variations as $variation){
                                                        $colorArray[] = $variation->color;
                                                    }
                                                }
                                                $getColorArray = array_unique($colorArray);
                                                if(!empty($getColorArray)){ 
                                                ?>
                                                <ul class="color-categories">
                                                <?php foreach($getColorArray as $color){ 
                                                    $allColors = $bladeVar['color_results'];
                                                    if(!empty($allColors)){
                                                        foreach($allColors as $getColor){
                                                            if($getColor->id == $color){ ?>
                                                                <li>
                                                                    <a href="#" style="background-color: <?php echo $getColor->color_code; ?>;" title="<?php echo $getColor->name; ?>"></a>
                                                                </li>   
                                                            <?php }
                                                        }
                                                    }
                                                } ?>
                                                </ul>
                                                <?php } ?>	
                                                <h5 class="product-name"><a href="<?php echo e(url('/shop/'.$product->slug)); ?>"><?php echo e($product->name); ?></a></h5>
                                                <div class="price-box">
                                                    <?php if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')): ?>
                                                    <span class="price-regular">$<?php echo e($product->discounted_price); ?></span>
                                                    <span class="price-old"><del>$<?php echo e($product->price); ?></del></span>
                                                    <?php else: ?>
                                                    <span class="price-regular">$<?php echo e($product->price); ?></span>
                                                    <?php endif; ?>
                                                </div>
                                                <p><?php echo e($product->sort_details); ?></p>
                                            </div>
                                        </div>
                                        <!-- product list item end -->
                                    </div>
                                    <?php endif; ?>
						        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <!-- product single item start -->
                            </div>
                            <!-- product item list wrapper end -->

                            <!-- start pagination area -->
                            <div class="paginatoin-area text-center">
                             <?php echo e($bladeVar['product_results']->appends(Request::query())->links()); ?> 
                            </div>
                            <!-- end pagination area -->
                            <?php else: ?>
                                <div class="paginatoin-area text-center">
                                    <strong>No records found!</strong>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- shop main wrapper end -->
                </div>
            </div>
        </div>
        <!-- page main wrapper end -->
    </main>
<?php echo $__env->make('frontend.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script type="text/javascript">
$(document).ready(function() {
    var baseUrl = '<?php echo e(url("/")); ?>';
    var seasoning = ''; 
    var category = []; 
    var brand = []; 
    var color = []; 
    var size = [];

    $('input[type=checkbox]').click(function (e) {
        $('input[name="productCategory[]"]:checked').each(function(){
            category.push($(this).val());
        })
        if(category.length !== 0){
            seasoning+='&category='+category.toString();
        }
        $('input[name="productBrand[]"]:checked').each(function(){
            brand.push($(this).val());
        })
        if(brand.length !== 0){
            seasoning+='&brand='+brand.toString();
        }
        $('input[name="productColor[]"]:checked').each(function(){
            color.push($(this).val());
        })
        if(color.length !== 0){
            seasoning+='&color='+color.toString();
        }
        $('input[name="productSize[]"]:checked').each(function(){
            size.push($(this).val());
        })
        if(size.length !== 0){
            seasoning+='&size='+size.toString();
        }
        var redirecturl1 = baseUrl+'/shop?'+ seasoning;
        location.href = redirecturl1;
    });

    $("#sortby").change(function(){
        var selectedOrder = $(this).children("option:selected").val();
        var urlParams = new URLSearchParams(window.location.search);
        var pageURL = $(location).attr("href");
        var category = '', brand = '', color = '', size = '', price = ''; 
        if(urlParams.has('category')){
            category = urlParams.has('category');
        }else if(urlParams.has('brand')){
            brand = urlParams.has('brand');
        }else if(urlParams.has('color')){
            color = urlParams.has('color');
        }else if(urlParams.has('size')){
            size = urlParams.has('size');
        }else if(urlParams.has('price')){
            price = urlParams.has('price');
        }
        if((category != '') || (brand != '') || (color != '') || (size != '') || (price != '')){
            seasoning+='&sortby='+selectedOrder;
            var redirecturl = pageURL+seasoning;
        }else{
            seasoning+='sortby='+selectedOrder;
            var redirecturl = baseUrl+'/shop?'+ seasoning;
        } 
        location.href = redirecturl;
    });
    
    $("#priceFilter").on('click', function(event) {
		event.preventDefault();
        var getPrice = $('#amount').val();
        var urlParams = new URLSearchParams(window.location.search);
        var pageURL = $(location).attr("href");
        var category = '', brand = '', color = '', size = '', price = '', sortby = ''; 
        if(urlParams.has('category')){
            category = urlParams.has('category');
        }else if(urlParams.has('brand')){
            brand = urlParams.has('brand');
        }else if(urlParams.has('color')){
            color = urlParams.has('color');
        }else if(urlParams.has('size')){
            size = urlParams.has('size');
        }else if(urlParams.has('price')){
            price = urlParams.has('price');
        }else if(urlParams.has('sortby')){
            sortby = urlParams.has('sortby');
        }
        if((category != '') || (brand != '') || (color != '') || (size != '') || (price != '') || (sortby != '')){
            seasoning+='&price='+ getPrice;
            var redirecturl = pageURL+seasoning;
        }else{
            seasoning+='price='+getPrice;
            var redirecturl = baseUrl+'/shop?'+ seasoning;
        } 
        location.href = redirecturl;
    });
});
</script>