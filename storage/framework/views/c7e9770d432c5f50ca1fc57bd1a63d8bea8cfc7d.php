<?php echo $__env->make('frontend.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="<?php echo e(url('/shop')); ?>">shop</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">wishlist</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- wishlist main wrapper start -->
        <div class="wishlist-main-wrapper section-padding">
            <div class="container">
                <!-- Wishlist Page Content Start -->
                <div class="section-bg-color">
                    <div class="row">
                        <div class="col-lg-12" id="wishlistItem">
                            <!-- Wishlist Table Area -->
                            <?php
                            $arrayWishlist = array();
                            if(!empty($bladeVar['userWishlistData'])){ 
                                foreach($bladeVar['userWishlistData'] as $wishlist){
                                    $arrayWishlist[] = $wishlist->product_id;
                                } 
                            } ?>
                            <?php if(!empty($arrayWishlist)): ?>
                                <?php if(!empty($bladeVar['product_results'])): ?>
                                <div class="cart-table table-responsive">
                                    <?php if(sizeof($bladeVar['product_results']) > 0): ?>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="pro-thumbnail">Thumbnail</th>
                                                <th class="pro-title">Product</th>
                                                <th class="pro-price">Price</th>
                                                <th class="pro-quantity">Stock Status</th>
                                                <th class="pro-subtotal">Add to Cart</th>
                                                <th class="pro-remove">Remove</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $__currentLoopData = $bladeVar['product_results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($product->deleted_at == NULL): ?>
                                                    <?php if(in_array($product->_id, $arrayWishlist)): ?>
                                                    <tr id="product<?php echo e($product->_id); ?>">
                                                        <td class="pro-thumbnail"><a href="<?php echo e(url('/shop/'.$product->slug)); ?>"><img class="img-fluid" src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>" /></a></td>
                                                        <td class="pro-title"><a href="<?php echo e(url('/shop/'.$product->slug)); ?>"><?php echo e($product->name); ?></a></td>
                                                        <td class="pro-price"><span>
                                                            <?php if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')): ?>
                                                                $<?php echo e($product->discounted_price); ?>

                                                            <?php else: ?>
                                                                $<?php echo e($product->price); ?>

                                                            <?php endif; ?> 
                                                        </span></td>
                                                        <?php if(!empty($product->product_quantity) && ($product->product_quantity > 0)): ?>
                                                            <td class="pro-quantity"><span class="text-success">In Stock</span></td>
                                                            <?php if($product->add_to_cart == '1'): ?>

                                                            <?php $arrayCartList = array();
                                                            if(!empty($bladeVar['userCartData'])){ 
                                                                foreach($bladeVar['userCartData'] as $cartList){
                                                                    $arrayCartList[] = $cartList->product_id;
                                                                } 
                                                            } ?>
                                                            <td class="pro-subtotal">
                                                            <?php if (in_array($product->_id, $arrayCartList)) { ?>
                                                                <a href="#" class="btn btn-sqr addtocart1 disabled" id="addtocart<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>">Go to cart</a>
                                                            <?php }else{ ?>
                                                                <a href="#" class="btn btn-sqr addtocart1" id="addtocart<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>">add to cart</a>
                                                            <?php } ?>      
                                                            </td>
                                                            <?php endif; ?>
                                                        <?php else: ?>
                                                            <td class="pro-quantity"><span class="text-danger">Stock Out</span></td>
                                                            <?php if($product->add_to_cart == '1'): ?>
                                                            <td class="pro-subtotal"><a href="#" class="btn btn-sqr disabled">Add to Cart</a></td>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                        <td class="pro-remove"><a href="#" class="removeProduct" data-pid="<?php echo e($product->_id); ?>"><i class="fa fa-trash-o"></i></a></td>
                                                    </tr>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>
                                    </table>
                                    <?php endif; ?> 
                                </div>
                                <?php endif; ?>
                            <?php else: ?>
                            <h3 style="text-align: center;">Empty Wishlist</h3>
                            <p style="text-align: center;">You have no items in your wishlist. Start adding!</p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <!-- Wishlist Page Content End -->
            </div>
        </div>
        <!-- wishlist main wrapper end -->
    </main>
<?php echo $__env->make('frontend.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script type="text/javascript">
$(document).ready(function() {
    var baseUrl = '<?php echo e(url("/")); ?>';
    var token = '<?php echo e(csrf_token()); ?>';

    $(".addtocart1").on('click', function(event) {
        event.preventDefault();
        var userID = '';
        <?php $data = Session::all(); if (Session::has('userLoggedIn')){ ?>
            userID = '<?php echo Session::get('userLoggedIn'); ?>';
        <?php } ?>
        var getAllClass = $(this).attr('class');
        if(getAllClass == 'btn btn-sqr addtocart1 disabled'){
            var redirecturl = baseUrl+'/cart';
            location.href = redirecturl;  
        }else{    
            if(userID != ''){
                var pid = $(this).attr('data-pid');
                $.ajax({
                    url: baseUrl + "/addtocart",
                    type: "POST",
                    data: { 
                        '_token': token, 
                        'product_id': pid,
                    },
                    dataType: "JSON",
                    success: function(jsonStr) {
                        var res_data = JSON.stringify(jsonStr);
                        var response = JSON.parse(res_data);
                        var responseData = response['responseData'];
                        var responseDataCount = response['responseDataCount'];
                        if ((responseData != null) && (responseData == 'add to cart successfully')) {
                            $('#addtocart'+pid).addClass('disabled');
                            $('#addtocart'+pid).text('go to cart');
                        } else {
                            $('#addtocart'+pid).removeClass('disabled');
                            $('#addtocart'+pid).text('add to cart');
                        }
                        if((responseDataCount != null) && (responseDataCount != '0')){
                            $('.minicart-btn .notification').text(responseDataCount);
                        }else{
                            $('.minicart-btn .notification').text(responseDataCount);
                        }
                        viewCart();
                    }
                });
            }else{
                var redirecturl = baseUrl+'/login-register';
                location.href = redirecturl;  
            }
        }
    });

    $(".removeProduct").on('click', function(event) {
        event.preventDefault();
        var pid = $(this).attr('data-pid');
        var userID = '';
        <?php $data = Session::all(); if (Session::has('userLoggedIn')){ ?>
            userID = '<?php echo Session::get('userLoggedIn'); ?>';
        <?php } ?>
        if((userID != '') && (pid != '')){
            $.ajax({
				url: baseUrl + "/addtowishlist",
				type: "POST",
				data: { 
                    '_token': token, 
					'product_id': pid,
				},
				dataType: "JSON",
				success: function(jsonStr) {
					var res_data = JSON.stringify(jsonStr);
					var response = JSON.parse(res_data);
                    var responseData = response['responseData'];
                    var responseDataCount = response['responseDataCount'];
					if ((responseData != null) && (responseData == 'add to wishlist successfully')) {
						$('#addtowishlist'+pid+' .pe-7s-like').addClass('addWishlist');
					} else {
						$('#addtowishlist'+pid+' .pe-7s-like').removeClass('addWishlist');
                    }
                    if((responseDataCount != null) && (responseDataCount != '0')){
                        $('.wishlist.notification').text(responseDataCount);
                    }else{
                        $('.wishlist.notification').text(responseDataCount);
                    }
                    $('#product'+pid).remove();
                    if(responseDataCount > 0){
                        console.log(responseDataCount);
                    }else{
                        $('.cart-table').hide();
                        $('#wishlistItem').append('<h3 style="text-align: center;">Empty Wishlist</h3>'+
                        '<p style="text-align: center;">You have no items in your wishlist. Start adding!</p>');
                    }
				}
			});    
        }else{
           var redirecturl = baseUrl+'/login-register';
           location.href = redirecturl;  
        }
    });

    function viewCart(){
        var data = { '_token': token };
        var ajaxUrl  = baseUrl+"/viewcart";
        $.post(ajaxUrl, data, function(response) {
            $('.minicart-inner-content').html('').append(response);
        });
    }
});
</script>    