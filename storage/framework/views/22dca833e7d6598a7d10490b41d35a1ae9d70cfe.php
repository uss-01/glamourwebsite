<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td><?php echo e($bladeVar['result']->store_id); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Name</th>
		    		<td><?php echo e($bladeVar['result']->name); ?></td>
		    	</tr>
		    	
		    </tbody>
    	</table>
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="<?php echo e($bladeVar['result']->deleted_at == null ? route('store.edit', $bladeVar['result']->store_id) : ''); ?>" 
	    	data-destroy="<?php echo e($bladeVar['result']->deleted_at == null ? route('store.destroy', $bladeVar['result']->store_id) : ''); ?>" 
	    	data-restore="<?php echo e($bladeVar['result']->deleted_at != null ? route('store.restore', $bladeVar['result']->store_id) : ''); ?>"
	    	data-delete="<?php echo e(route('store.delete', $bladeVar['result']->store_id)); ?>">
	    </span>
	</div>
</div>	    