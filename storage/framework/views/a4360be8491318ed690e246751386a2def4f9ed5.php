<?php echo $__env->make('frontend.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<style type="text/css">
.has-error {
    border: 1px solid red !important;
}
</style>
    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">contact us</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- google map start -->
        <div class="map-area section-padding">
            <div id="google-map"></div>
        </div>
        <!-- google map end -->

        <!-- contact area start -->
        <div class="contact-area section-padding pt-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="contact-message">
                            <h4 class="contact-title">Tell Us Your Project</h4>
                            <form id="contact-form" action="#" method="post" class="contact-form">
                                <input type="hidden" id="_token" name="_token" value="<?php echo e(csrf_token()); ?>">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <input name="first_name" id="first_name" placeholder="Name *" type="text" required>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <input name="phone" id="phone" placeholder="Phone *" type="text" required>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <input name="email_address" id="email_address" placeholder="Email *" type="text" required>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <input name="contact_subject" id="contact_subject" placeholder="Subject *" type="text">
                                    </div>
                                    <div class="col-12">
                                        <div class="contact2-textarea text-center">
                                            <textarea placeholder="Message *" name="message" id="message" class="form-control2" required=""></textarea>
                                        </div>
                                        <div class="contact-btn">
                                            <button class="btn btn-sqr" type="submit" id="sendMessage">Send Message</button>
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex justify-content-center">
                                        <p class="form-messege" id="responseMessage"></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <?php if(!empty($bladeVar['results'])): ?> 
                    <div class="col-lg-6">
                        <div class="contact-info">
                            <h4 class="contact-title"><?php echo e($bladeVar['results']->title); ?></h4>
                            <?php echo $bladeVar['results']->content; ?>

                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- contact area end -->
    </main>
<?php echo $__env->make('frontend.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script type="text/javascript">
$(document).ready(function() {
    var baseUrl = '<?php echo e(url("/")); ?>';
	function isEmail(emailid) {
		var pattern = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
		return pattern.test(emailid);
	}
	$("#sendMessage").on('click', function(event) {
		event.preventDefault();
        var token = $('#_token').val();
		var name = $('#first_name').val();
		var phone = $('#phone').val();
		var email_address = $('#email_address').val();
		var contact_subject = $('#contact_subject').val();
		var message = $('#message').val();

        $('input[name="phone"]').keyup(function(e){
            if (/\D/g.test(this.value)){
                this.value = this.value.replace(/\D/g, '');
            }
        });

		if (name == '') {
			$('#first_name').addClass('has-error');
		} else if (name != '') {
			$('#first_name').removeClass('has-error');
		}
        if (phone == '') {
			$('#phone').addClass('has-error');
		} else if (phone != '') {
			$('#phone').removeClass('has-error');
		}
        if (contact_subject == '') {
			$('#contact_subject').addClass('has-error');
		} else if (contact_subject != '') {
			$('#contact_subject').removeClass('has-error');
		}
        if (message == '') {
			$('#message').addClass('has-error');
		} else if (message != '') {
			$('#message').removeClass('has-error');
		}
		if ((email_address == '') || (!isEmail($('#email_address').val()))) {
			$('#email_address').addClass('has-error');
		} else if ((email_address != '') && (isEmail($('#email_address').val()))) {
			$('#email_address').removeClass('has-error');
		}

		if ((name != '') && (email_address != '') && (isEmail($('#email_address').val())) && (phone != '') && (contact_subject != '') && (message != '')) {
			$('#responseMessage').html('');
			$.ajax({
				url: baseUrl + "/contactform",
				type: "POST",
				data: {
                    '_token': token,
					'name': name,
					'phone': phone,
					'email_address': email_address,
					'contact_subject': contact_subject,
					'message': message
				},
				dataType: "JSON",
				success: function(jsonStr) {
					var res_data = JSON.stringify(jsonStr);
					var response = JSON.parse(res_data);
					var responseData = response['responseData'];
					if ((responseData != null) && (responseData == 'comment send successfully')) {
						$('#responseMessage').html('Your message sent successfully');
                        setTimeout(function(){ $('#contact-form')[0].reset(); location.reload();  }, 3000);
					} else {
						$('#responseMessage').html(responseData);
					}
				}
			});
		}
    });
});
</script>