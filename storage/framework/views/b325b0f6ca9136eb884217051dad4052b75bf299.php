<!DOCTYPE html>
<html lang="<?php echo e(config('app.locale')); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title>404</title>
    <link rel="shortcut icon" href="<?php echo e(asset('favicon.ico')); ?>" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo e(asset('css/thirdparty/fontawesome.css')); ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(asset('css/thirdparty/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/boot.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/resp.css')); ?>" rel="stylesheet">
    <script>
        window.webApp = <?php echo json_encode([
        	'csrfToken' => csrf_token(),
        ]); ?>;
    </script>
    <?php $__env->startSection('head'); ?><?php echo $__env->yieldSection(); ?>
</head>
<body>
    <?php $__env->startSection('header'); ?>

    <?php echo $__env->yieldSection(); ?>
    
    <?php echo $__env->yieldContent('content'); ?>

    <?php $__env->startSection('footer'); ?>
        <div class="footer-border pt-3 mt-5 mb-3">
            <small class="d-block text-center">&copy; <?php echo e(date('Y')); ?> Systems, All rights reserved</small>
        </div>
    <?php echo $__env->yieldSection(); ?>

    <div class="loadingScreen hidden-xs-up">
        <div class="my-5 text-center mx-auto h1" id="loadingScreenDiv"><i class="fa fa-spinner fa-spin"></i> Loading...</div>
    </div>

    <!-- Footer scripts -->
    <script src="<?php echo e(asset('js/jquery-3.2.1.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/ajax-tether-1.4.min.js')); ?>" ></script>
    <script src="<?php echo e(asset('js/bootstrap-4.0.0.min.js')); ?>" ></script>
    <script src="<?php echo e(asset('js/boot.js?v=1')); ?>"></script>

    <?php $__env->startSection('foot'); ?><?php echo $__env->yieldSection(); ?>
</body>
</html>