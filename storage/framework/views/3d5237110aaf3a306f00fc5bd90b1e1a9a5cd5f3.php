<div class="row">
	<div class="col-12">
		<form action="<?php echo e(route('orders.update', $bladeVar['result']->_id)); ?>" method="POST">
			<?php echo e(method_field('PUT')); ?>

            <div class="row">
			    <input type="hidden" class="form-control updateStatusid" name="updateStatusid" value="<?php echo e($bladeVar['result']->_id); ?>">
				<div class="col-6">
					<div class="form-group">
						<img src="<?php echo e(asset('images/products/thumbnails').'/'.$bladeVar['result']->product_image); ?>" class="thumbnail" width="150" />
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label class="form-control-label" for="product_name"><strong>Product name</strong></label>
						<input type="text" class="form-control" name="product_name" readonly value="<?php echo e(old('product_name', $bladeVar['result']->product_name)); ?>">
					</div>
				</div>
			    <div class="col-6">
					<div class="form-group">
						<label class="form-control-label" for="price"><strong>Price</strong></label>
						<input type="text" class="form-control" name="price" readonly value="<?php echo e(old('price', $bladeVar['result']->price)); ?>">
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label class="form-control-label" for="quantity"><strong>Quantity</strong></label>
						<input type="text" class="form-control" name="quantity" readonly value="<?php echo e(old('quantity', $bladeVar['result']->quantity)); ?>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="product_details"><strong>Details</strong></label>
				<textarea class="form-control" readonly name="product_details"><?php echo e(old('product_details', $bladeVar['result']->product_details)); ?></textarea>
			</div>
			<div class="row">
				<div class="col-6">
					<div class="form-group">
						<label class="form-control-label" for="created_at"><strong>Order Date</strong></label>
						<input type="text" class="form-control" name="created_at" readonly value="<?php echo e(old('created_at', date('d F Y', strtotime($bladeVar['result']->created_at)))); ?>">
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label class="form-control-label" for="updated_at"><strong>Order Status Updated Date</strong></label>
						<input type="text" class="form-control" name="updated_at" readonly value="<?php echo e(old('updated_at', date('d F Y', strtotime($bladeVar['result']->updated_at)))); ?>">
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label class="form-control-label" for="category"><strong>Product Status</strong></label>
						<select name="order_status" class="form-control">
							<option value=""<?php echo e(empty(old('order_status')) ? ' selected="selected"' : ''); ?>></option>
							<?php $__currentLoopData = $bladeVar['status']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $order_status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($order_status); ?>"<?php echo e($order_status == old('order_status', $bladeVar['result']->order_status) ? ' selected="selected"' : ''); ?>><?php echo e($order_status); ?></option>
						    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
						<small class="form-control-feedback"></small>
					</div>
				</div>
			</div>
			
			<table class="table table-bordered table-sm mb-0">
				<tbody>
					<tr>
						<th class="bg-faded">User Details</th>
					</tr>
					<tr>
						<th class="bg-faded">User Name</th>
						<td> <?php echo e($bladeVar['userdetail']->firstname); ?> <?php echo e($bladeVar['userdetail']->lastname); ?></td>
					</tr>
					<tr>
						<th class="bg-faded">Contact Email</th>
						<td> <?php echo e($bladeVar['userdetail']->email); ?></td>
					</tr>
					<tr>
						<th class="bg-faded">Contact Number</th>
						<td> <?php echo e($bladeVar['userdetail']->phone); ?></td>
					</tr>
					<tr>
						<th class="bg-faded">Address</th>
						<td> <?php echo e($bladeVar['userdetail']->address); ?></td>
					</tr>
					<tr>
						<th class="bg-faded">Payment Method</th>
						<td> <?php echo e($bladeVar['userdetail']->payment_method); ?></td>
					</tr>
				</tbody>
			</table>

			<div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
      	</form>
    </div>
</div>