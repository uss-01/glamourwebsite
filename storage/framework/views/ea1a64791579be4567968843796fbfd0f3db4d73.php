<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td><?php echo e($bladeVar['result']->id); ?></td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Name</th>
		    		<td><?php echo e($bladeVar['result']->name); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Category</th>
		    		<td><?php echo e($bladeVar['result']->product_category->name); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Subcategory</th>
		    		<td><?php echo e($bladeVar['result']->product_subcategory->name); ?></td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Button Text</th>
		    		<td><?php echo e($bladeVar['result']->btntext); ?></td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Short Description</th>
		    		<td><?php echo e($bladeVar['result']->shortDescription); ?></td>
				</tr>
				<tr>
		    		<th class="bg-faded">Banner Position</th>
		    		<td>
					<?php if($bladeVar['result']->bannerPosition == 'text-center'): ?>         
						Default                    
					<?php endif; ?>
					<?php if($bladeVar['result']->bannerPosition == 'text-left'): ?>         
						Left                
					<?php endif; ?>
					<?php if($bladeVar['result']->bannerPosition == 'text-right'): ?>         
						Right                
					<?php endif; ?>
					</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Image</th>
		    		<td><img src="<?php echo e(asset('images/cms').'/'.$bladeVar['result']->image); ?>" class="thumbnail" width="150" /></td>
		    	</tr>
		    </tbody>
    	</table>
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="<?php echo e($bladeVar['result']->deleted_at == null ? route('singlebannerimage.edit', $bladeVar['result']->id) : ''); ?>" 
	    	data-destroy="<?php echo e($bladeVar['result']->deleted_at == null ? route('singlebannerimage.destroy', $bladeVar['result']->id) : ''); ?>" 
	    	data-restore="<?php echo e($bladeVar['result']->deleted_at != null ? route('singlebannerimage.restore', $bladeVar['result']->id) : ''); ?>"
	    	data-delete="<?php echo e(route('singlebannerimage.delete', $bladeVar['result']->id)); ?>">
	    </span>
	</div>
</div>	    