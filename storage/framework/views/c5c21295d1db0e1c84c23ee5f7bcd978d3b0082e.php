<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td><?php echo e($bladeVar['result']->id); ?></td>
				</tr>
		    	<tr>
		    		<th class="bg-faded">Company Name</th>
		    		<td><?php echo e($bladeVar['result']->companyName); ?></td>
				</tr>
				<tr>
		    		<th class="bg-faded">Email</th>
		    		<td><?php echo e($bladeVar['result']->email); ?></td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Phone No.</th>
		    		<td><?php echo e($bladeVar['result']->mobile); ?></td>
				</tr>
				<tr>
		    		<th class="bg-faded">Address</th>
		    		<td><?php echo e($bladeVar['result']->address); ?></td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Details</th>
		    		<td><?php echo e(strip_tags($bladeVar['result']->details)); ?></td>
				</tr>
				<tr>
		    		<th class="bg-faded">Designation</th>
		    		<td><?php echo e(strip_tags($bladeVar['result']->designation)); ?></td>
				</tr>
				<tr>
		    		<th class="bg-faded">Website</th>
		    		<td><?php echo e(strip_tags($bladeVar['result']->website)); ?></td>
				</tr>
				<tr>
		    		<th class="bg-faded">Category</th>
		    		<td><?php echo e(strip_tags($bladeVar['result']->category)); ?></td>
				</tr>
				<tr>
		    		<th class="bg-faded">Concept</th>
		    		<td><?php echo e(strip_tags($bladeVar['result']->concept)); ?></td>
				</tr>
				<tr>
		    		<th class="bg-faded">Brand</th>
		    		<td><?php echo e(strip_tags($bladeVar['result']->brand)); ?></td>
				</tr>
				<tr>
		    		<th class="bg-faded">Stores</th>
		    		<td><?php echo e(strip_tags($bladeVar['result']->stores)); ?></td>
				</tr>
				<tr>
		    		<th class="bg-faded">Created Date</th>
		    		<td><?php echo e(date('d F Y', strtotime($bladeVar['result']->created_at))); ?></td>
				</tr>
		    </tbody>
    	</table>
	</div>
</div>	    