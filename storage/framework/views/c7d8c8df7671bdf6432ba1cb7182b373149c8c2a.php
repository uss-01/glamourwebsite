<?php echo $__env->make('frontend.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php if(!empty($bladeVar['blogresult'])): ?>
<style type="text/css">
.has-error {
    border: 1px solid red !important;
}
#responseMessage{
    display: none;
}
</style>
    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="<?php echo e(url('/blog')); ?>">blog</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"><?php echo e($bladeVar['blogresult']['name']); ?></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- blog main wrapper start -->
        <div class="blog-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 order-2 order-lg-1">
                        <aside class="blog-sidebar-wrapper">
                            <div class="blog-sidebar">
                                <h5 class="title">search</h5>
                                <div class="sidebar-serch-form">
                                    <form action="<?php echo e(url('/blog/')); ?>" method="get">
                                        <input type="text" class="search-field" id="search" name="search" placeholder="search here">
                                        <button type="submit" class="search-btn"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            </div> <!-- single sidebar end -->
                            <?php if(!empty($bladeVar['blogCategories'])): ?>
                            <div class="blog-sidebar">
                                <h5 class="title">categories</h5>
                                <ul class="blog-archive blog-category">
                                <?php $__currentLoopData = $bladeVar['blogCategories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($category->deleted_at == NULL): ?>
                                    <?php if($category->categoryBlogs != 0): ?>
                                    <li><a href="<?php echo e(url('/blog/?category='.$category->slug)); ?>"><?php echo e($category->name); ?> (<?php echo e($category->categoryBlogs); ?>)</a></li>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div> 
                            <?php endif; ?>
                            <!-- single sidebar end -->
                            <?php if(!empty($bladeVar['blogArchives'])): ?>

                            <div class="blog-sidebar">
                                <h5 class="title">Blog Archives</h5>
                                <ul class="blog-archive">
                                    <?php $__currentLoopData = $bladeVar['blogArchives']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $archive): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><a href="<?php echo e(url('/blog/?archive='.$archive->monthname)); ?>"><?php echo e($archive->monthname); ?> (<?php echo e($archive->count); ?>)</a></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div> 
                            <?php endif; ?>
                            <!-- single sidebar end -->
                            <?php if(!empty($bladeVar['recent_post']->count() > 0)): ?>
                            <div class="blog-sidebar">
                                <h5 class="title">recent post</h5>
                                <div class="recent-post">
                                    <?php $__currentLoopData = $bladeVar['recent_post']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($blog->deleted_at == NULL): ?>
                                    <div class="recent-post-item">
                                        <figure class="product-thumb">
                                            <a href="<?php echo e(url('/blog/'.$blog->slug)); ?>">
                                                <img src="<?php echo e(asset('images/blogs').'/'.$blog->image); ?>" alt="<?php echo e($blog->slug); ?>">
                                            </a>
                                        </figure>
                                        <div class="recent-post-description">
                                            <div class="product-name">
                                                <h6><a href="<?php echo e(url('/blog/'.$blog->slug)); ?>"><?php echo e($blog->name); ?></a></h6>
                                                <p><?php echo e(date('F j Y', strtotime($blog->updated_at))); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
                                </div>
                            </div> 
                            <?php endif; ?>
                            <!-- single sidebar end -->
                            <?php if(!empty($bladeVar['blogTags'])): ?>
                            <div class="blog-sidebar">
                                <h5 class="title">Tags</h5>
                                <ul class="blog-tags">
                                <?php $__currentLoopData = $bladeVar['blogTags']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($tag->deleted_at == NULL): ?>
                                <?php if($tag->tagBlogs != 0): ?>
                                    <li><a href="<?php echo e(url('/blog/?tag='.$tag->slug)); ?>"><?php echo e($tag->name); ?></a></li>
                                <?php endif; ?>
                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div> 
                            <?php endif; ?>
                            <!-- single sidebar end -->
                        </aside>
                    </div>
                    <div class="col-lg-9 order-1 order-lg-2">
                        <div class="blog-item-wrapper">
                            <!-- blog post item start -->
                            <div class="blog-post-item blog-details-post">
                                <?php if($bladeVar['blogresult']['blog_image_slide'] == '1'): ?>
                                <?php if(!empty($bladeVar['blogPics'])): ?>
                                <figure class="blog-thumb">
                                    <div class="blog-carousel-2 slick-row-15 slick-arrow-style slick-dot-style">
                                        <?php $__currentLoopData = $bladeVar['blogPics']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="blog-single-slide">
                                            <img src="<?php echo e(asset('images/blogs').'/'.$pic['image']); ?>" alt="<?php echo e($bladeVar['blogresult']['slug']); ?>">
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </figure>
                                <?php endif; ?>
                                <?php elseif($bladeVar['blogresult']['blog_audio'] == '1'): ?> 
                                <figure class="blog-thumb embed-responsive embed-responsive-16by9">
                                    <iframe src="<?php echo e($bladeVar['blogresult']['audio_url']); ?>"></iframe>
                                </figure>
                                <?php elseif($bladeVar['blogresult']['blog_video'] == '1'): ?>
                                <figure class="blog-thumb embed-responsive embed-responsive-16by9">
                                    <iframe src="<?php echo e($bladeVar['blogresult']['video_url']); ?>" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                </figure>
                                <?php else: ?> 
                                <figure class="blog-thumb">
                                    <img src="<?php echo e(asset('images/blogs').'/'.$bladeVar['blogresult']['image']); ?>" alt="<?php echo e($bladeVar['blogresult']['slug']); ?>">
                                </figure>
                                <?php endif; ?>
                                <div class="blog-content">
                                    <h3 class="blog-title">
                                        <?php echo e($bladeVar['blogresult']['name']); ?>

                                    </h3>
                                    <div class="blog-meta">
                                        <p><?php echo e(date('d/m/Y', strtotime($bladeVar['blogresult']['updated_at']))); ?> </p>
                                    </div>
                                    <div class="entry-summary">
                                        <?php echo $bladeVar['blogresult']['details']; ?>

                                        <div class="tag-line">
                                            <h6>Tag :</h6>
                                            <a href="#"><?php echo e($bladeVar['blogresult']['blog_tag']['name']); ?></a>
                                        </div>
                                        <div class="blog-share-link">
                                            <h6>Share :</h6>
                                            <div class="blog-social-icon">
                                                <a href="http://www.facebook.com/share.php?u=<?php echo e(url('/blog/'.$bladeVar['blogresult']['slug'])); ?>&amp;title=<?php echo e($bladeVar['blogresult']['name']); ?>" class="facebook fbclick"><i class="fa fa-facebook"></i></a>
                                                <a href="https://twitter.com/intent/tweet?text=<?php echo e($bladeVar['blogresult']['name']); ?>&amp;url=<?php echo e(url('/blog/'.$bladeVar['blogresult']['slug'])); ?>" class="twitter fbclick"><i class="fa fa-twitter"></i></a>
                                                <a href="<?php echo e(url('/blog/'.$bladeVar['blogresult']['slug'])); ?>" target="_blank" data-image="<?php echo e(asset('images/blogs').'/'.$bladeVar['blogresult']['image']); ?>" data-desc="<?php echo e($bladeVar['blogresult']['name']); ?>" class="pinterest btnPinIt"><i class="fa fa-pinterest"></i></a>
                                                <a href="mailto:?subject=Jewellery Ecommerce: <?php echo e($bladeVar['blogresult']['name']); ?>&amp;body=<?php echo e(url('/blog/'.$bladeVar['blogresult']['slug'])); ?>%0D<?php echo e(url('/')); ?>" class="envelope" title="<?php echo e($bladeVar['blogresult']['name']); ?>" style="background-color: #fe4419;"><i class="fa fa-envelope"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- blog post item end -->

                            <!-- comment area start -->
                            <div class="comment-section section-padding">
                                <?php if(!empty($bladeVar['blogComments'])): ?>
                                <h5><?php echo e($bladeVar['blogCommentsCount']['0']->commentsCount); ?> Comment</h5>
                                <ul>
                                <?php $__currentLoopData = $bladeVar['blogComments']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($comment->type == 'blog'): ?>
                                    <li>
                                        <div class="author-avatar">
                                            <img src="<?php echo e(asset('assets/img/blog/comment-icon.png')); ?>" alt="<?php echo e($comment->name); ?>">
                                        </div>
                                        <div class="comment-body">
                                            <!-- <span class="reply-btn"><a href="#commentForm">Reply</a></span> -->
                                            <h5 class="comment-author"><?php echo e($comment->name); ?></h5>
                                            <div class="comment-post-date">
                                               <?php $timestamp = strtotime($comment->updated_at); ?>
                                                <?php echo e(date('d F, Y', $timestamp).' at '.date('g:i a', $timestamp)); ?>

                                            </div>
                                            <p><?php echo e($comment->comment); ?></p>
                                        </div>
                                    </li>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                                </ul>
                                <?php endif; ?>
                            </div>
                            <!-- comment area end -->

                            <!-- start blog comment box -->
                            <div class="blog-comment-wrapper">
                                <h5>Leave a reply</h5>
                                <p>Your email address will not be published. Required fields are marked *</p>
                                <p class="form-messege" id="responseMessage"></p>
                                <form action="#" id="commentForm">
                                    <div class="comment-post-box">
                                        <div class="row">
                                            <input type="hidden" id="_token" name="_token" value="<?php echo e(csrf_token()); ?>">
                                            <input type="hidden" id="pid" class="form-control" value="<?php echo e($bladeVar['blogresult']['_id']); ?>">
											<input type="hidden" id="ptitle" class="form-control" value="<?php echo e($bladeVar['blogresult']['name']); ?>">
                                            <div class="col-lg-4 col-md-4">
                                                <label>Name *</label>
                                                <input type="text" name="name" id="name" class="coment-field" placeholder="Name">
                                            </div>
                                            <div class="col-lg-4 col-md-4">
                                                <label>Email *</label>
                                                <input type="text" name="email" id="email" class="coment-field" placeholder="Email">
                                            </div>
                                            <div class="col-lg-4 col-md-4">
                                                <label>Website</label>
                                                <input type="text" name="website" id="website" class="coment-field" placeholder="Website">
                                            </div>
                                            <div class="col-12">
                                                <label>Comment *</label>
                                                <textarea name="commnet" id="comment" placeholder="Write a comment"></textarea>
                                            </div>
                                            <div class="col-12">
                                                <div class="coment-btn">
                                                    <input class="btn btn-sqr" id="postComment" type="submit" name="submit" value="Post Comment">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- start blog comment box -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- blog main wrapper end -->
    </main>
<?php endif; ?>
<?php echo $__env->make('frontend.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script type="text/javascript">
$(document).ready(function() {
    var baseUrl = '<?php echo e(url("/")); ?>';
    function isEmail(emailid) {
		var pattern = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
		return pattern.test(emailid);
	}

    $("#postComment").on('click', function(event) {
		event.preventDefault();
        $('#responseMessage').hide().html('');
        var token = $('#_token').val();
		var getId = $('#commentForm #pid').val();
		var ptitle = $('#commentForm #ptitle').val();
		var name = $('#commentForm #name').val();
		var email = $('#commentForm #email').val();
        var website = $('#commentForm #website').val();
		var comment = $('#commentForm #comment').val();
		if (name == '') {
			$('#commentForm #name').addClass('has-error');
		} else if (name != '') {
			$('#commentForm #name').removeClass('has-error');
		}
		if ((email == '') || (!isEmail($('#commentForm #email').val()))) {
			$('#commentForm #email').addClass('has-error');
		} else if ((email != '') && (isEmail($('#commentForm #email').val()))) {
			$('#commentForm #email').removeClass('has-error');
		}
        if (comment == '') {
			$('#commentForm #comment').addClass('has-error');
		} else if (comment != '') {
			$('#commentForm #comment').removeClass('has-error');
		}
		if ((name != '') && (email != '') && (isEmail($('#commentForm #email').val())) && (comment != '')) {
			$.ajax({
				url: baseUrl + "/sendcomment",
				type: "POST",
				data: {
                    '_token': token,   
					'postId': getId,
					'ptitle': ptitle,
					'name': name,
					'email': email,
                    'website': website,
					'comment': comment,
                    'type' : 'blog'
				},
				dataType: "JSON",
				success: function(jsonStr) {
					var res_data = JSON.stringify(jsonStr);
					var response = JSON.parse(res_data);
					var responseData = response['responseData'];
					if ((responseData != null) && (responseData == 'comment send successfully')) {
						$('#responseMessage').show().html('').html('Your comment sent successfully');
                        $("#commentForm").trigger("reset");
                        setTimeout(function(){ location.reload(); }, 2000);
					} else {
						$('#responseMessage').show().html('').html(responseData);
					}
				}
			});
		}
    });

    $('.fbclick').click(function(event) {
        var width  = 575,
            height = 400,
            left   = ($(window).width()  - width)  / 2,
            top    = ($(window).height() - height) / 2,
            url    = this.href,
            opts   = 'status=1' +
                    ',width='  + width  +
                    ',height=' + height +
                    ',top='    + top    +
                    ',left='   + left;
        window.open(url, 'twitter', opts);
        return false;
    });	

    $('.btnPinIt').click(function() {
        var url = $(this).attr('href');
        var media = $(this).attr('data-image');
        var desc = $(this).attr('data-desc');
        window.open("//www.pinterest.com/pin/create/button/"+
        "?url="+url+
        "&media="+media+
        "&description="+desc,"pinIt","toolbar=no, scrollbars=no, resizable=no, top=0, right=0, width=750, height=320");
        return false;
    });
    
});
</script>   