<div class="row">
	<div class="col-12">
		<form action="<?php echo e(route('homepageimage.update', $bladeVar['result']->id)); ?>" enctype="multipart/form-data" method="POST" id="brand_form">
			<?php echo e(method_field('PUT')); ?>

			<div class="form-group">
				<label class="form-control-label" for="title"><strong>Title</strong> 
					<small class="text-danger"> (required)</small></label>
				<input type="text" class="form-control" name="title" value="<?php echo e(old('title', $bladeVar['result']->title)); ?>"autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="shortDescription"><strong>Short Description</strong></label> 
				<textarea class="form-control" name="shortDescription"><?php echo e(old('shortDescription', $bladeVar['result']->shortDescription)); ?></textarea>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="image"><strong>Image</strong>
				 <small class="text-danger"> (required)</small></label>
				<input type="file" name="image" class="form-control">
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="slidePosition"><strong>Slide Position</strong></label>
				<select name="slidePosition" class="form-control">
					<?php if($bladeVar['result']->slidePosition == 'slide-3'): ?>         
						<option value="slide-3" selected="selected">Default</option>          
					<?php else: ?>
					    <option value="slide-3">Default</option>          
					<?php endif; ?>
					<?php if($bladeVar['result']->slidePosition == 'slide-1'): ?>         
						<option value="slide-1" selected="selected">Left</option>          
					<?php else: ?>
					    <option value="slide-1">Left</option>         
					<?php endif; ?>
					<?php if($bladeVar['result']->slidePosition == 'slide-2 float-md-right float-none'): ?>         
						<option value="slide-2 float-md-right float-none" selected="selected">Right</option>          
					<?php else: ?>
					    <option value="slide-2 float-md-right float-none">Right</option>          
					<?php endif; ?>
				</select>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="redirect_url"><strong>Rdirect Url</strong></label>
				<input type="text" name="redirect_url" class="form-control" value="<?php echo e(old('redirect_url', $bladeVar['result']->redirect_url)); ?>">
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="btntext"><strong>Button Text</strong></label>
				<input type="text" name="btntext" class="form-control"  value="<?php echo e(old('btntext', $bladeVar['result']->btntext)); ?>">
				<small class="form-control-feedback"></small>
			</div>
			 <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div> 
		</form>
	</div>
</div>