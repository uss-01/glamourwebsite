<div class="row">
	<div class="col-12">
		<form action="<?php echo e(route('product.update', $bladeVar['result']->_id)); ?>" method="POST">
			<?php echo e(method_field('PUT')); ?>

			<div class="row">
			    <div class="col-12">
					<div class="form-group">
						<label class="form-control-label" for="name"><strong>Name</strong><small class="text-danger"> (required)</small></label>
						<input type="text" class="form-control" name="name" value="<?php echo e(old('name', $bladeVar['result']->name)); ?>" autofocus>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="product_brand"><strong>Brand</strong><small class="text-danger"> (required)</small></label>
						<select name="product_brand" class="form-control">
							<option value=""<?php echo e(empty(old('product_brand')) ? ' selected="selected"' : ''); ?>></option>
							<?php $__currentLoopData = $bladeVar['product_brands']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product_brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($product_brand->_id); ?>"<?php echo e($product_brand->_id == old('product_brand',$bladeVar['result']->brand_id) ? ' selected="selected"' : ''); ?>><?php echo e($product_brand->name); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="product_category"><strong>Category</strong><small class="text-danger"> (required)</small></label>
						<select name="product_category" class="form-control">
							<option value=""<?php echo e(empty(old('product_category')) ? ' selected="selected"' : ''); ?>></option>
							<?php $__currentLoopData = $bladeVar['product_categories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product_category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($product_category->_id); ?>"<?php echo e($product_category->_id == old('product_category',$bladeVar['result']->product_category_id) ? ' selected="selected"' : ''); ?>><?php echo e($product_category->name); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<option value="-1"<?php echo e(old('product_category')== -1 ? ' selected="selected"' : ''); ?>>Other</option>
						</select>
						<div class="other_option_wrap"></div>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="product_subcategory"><strong>Sub Category</strong></label>
						<select name="product_subcategory" class="form-control">
							<option value=""<?php echo e(empty(old('product_subcategory')) ? ' selected="selected"' : ''); ?>></option>
							<?php $__currentLoopData = $bladeVar['product_subcategories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product_subcategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value="<?php echo e($product_subcategory->_id); ?>"<?php echo e($product_subcategory->_id == old('product_subcategory',$bladeVar['result']->product_subcategory_id) ? ' selected="selected"' : ''); ?>><?php echo e($product_subcategory->name); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<option value="-1"<?php echo e(old('product_subcategory')== -1 ? ' selected="selected"' : ''); ?>>Other</option>
						</select>
						<div class="other_option_wrap2"></div>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="product_color"><strong>Color</strong></label>
						<select name="product_color" class="form-control">
							<option value=""<?php echo e(empty(old('color')) ? ' selected="selected"' : ''); ?>></option>
							<?php $__currentLoopData = $bladeVar['product_colors']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($color->id); ?>"<?php echo e($color->id == old('product_color', $bladeVar['result']->product_color_id) ? ' selected="selected"' : ''); ?>><?php echo e($color->name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
						
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="product_size"><strong>Size</strong></label>
						<select name="product_size" class="form-control">
							<option value=""<?php echo e(empty(old('size')) ? ' selected="selected"' : ''); ?>></option>
							<?php $__currentLoopData = $bladeVar['product_sizes']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $size): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($size->id); ?>"<?php echo e($size->id == old('product_size', $bladeVar['result']->product_size_id) ? ' selected="selected"' : ''); ?>><?php echo e($size->name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<!-- <div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="product_status"><strong>Status</strong></label>
						<select name="product_status" class="form-control">
							<option value=""<?php echo e(empty(old('product_status')) ? ' selected="selected"' : ''); ?>></option>
							<?php $__currentLoopData = $bladeVar['productStatus']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product_status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($product_status); ?>"<?php echo e($product_status == old('product_status', $bladeVar['result']->product_status) ? ' selected="selected"' : ''); ?>><?php echo e($product_status); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
						<small class="form-control-feedback"></small>
					</div>
				</div> -->
			</div>
            <div class="row">
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="price"><strong>Price</strong><small class="text-danger"> (required)</small></label>
						<input type="text" class="form-control" name="price" value="<?php echo e(old('price', $bladeVar['result']->price)); ?>" autofocus>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="discounted_price"><strong>Discounted Price</strong></label>
						<input type="text" class="form-control" name="discounted_price" value="<?php echo e(old('discounted_price', $bladeVar['result']->discounted_price)); ?>" autofocus>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="product_quantity"><strong>Quantity</strong></label>
						<input type="text" class="form-control" name="product_quantity" value="<?php echo e(old('product_quantity', $bladeVar['result']->product_quantity)); ?>" autofocus>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="image"><strong>Image (Size 600X600 px)</strong> <small class="text-danger">(required)</small></label>
						<input type="file" name="image" class="form-control">
						<small class="form-control-feedback"></small>
					</div>
				</div>
            </div>
            
			<div class="row">
		        <div class="col-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" value="1" name="featured" <?php echo e($bladeVar['result']->featured == 1 ? ' checked="checked"' : ''); ?> >
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><strong>Featured</strong></span>
						</label>
					</div>
			    </div>
				<div class="col-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" name="new_arrival" value="1" <?php echo e($bladeVar['result']->new_arrival == 1 ? ' checked="checked"' : ''); ?> >
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><strong>New Arrival</strong></span>
						</label>
					</div>
			    </div>
				<div class="col-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" name="on_sale" value="1" <?php echo e($bladeVar['result']->on_sale == 1 ? ' checked="checked"' : ''); ?> >
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><strong>On Sale</strong></span>
						</label>
					</div>
			    </div>
				<div class="col-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" name="hot_deal" value="1" <?php echo e($bladeVar['result']->hot_deal == 1 ? ' checked="checked"' : ''); ?> >
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><strong>Hot Deal</strong></span>
						</label>
					</div>
			    </div>
				<div class="col-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" name="best_seller" value="1" <?php echo e($bladeVar['result']->best_seller == 1 ? ' checked="checked"' : ''); ?>>
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><strong>Best Seller</strong></span>
						</label>
					</div>
			    </div>
			    <div class="col-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" value="1" name="add_to_cart" <?php echo e($bladeVar['result']->add_to_cart == 1 ? ' checked="checked"' : ''); ?>>
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><strong>Add To Cart</strong></span>
						</label>
					</div>
			    </div>
		    </div>
			<div class="form-group">
				<label class="form-control-label" for="sort_details"><strong>Sort Details</strong></label>
				<textarea class="form-control" name="sort_details"><?php echo e(old('sort_details', $bladeVar['result']->sort_details)); ?></textarea>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="details"><strong>Details</strong></label>
				<textarea class="form-control" name="details"><?php echo e(old('details', $bladeVar['result']->details)); ?></textarea>
				<small class="form-control-feedback"></small>
			</div>

            <div class="form-group">
				<label class="form-control-label" for="product_variation"><strong>Product Variation</strong></label>
                <p>For product multiple color & size variation</p>   
				<?php $variations_data = json_decode($bladeVar['result']->variations); ?>
                <div id="allVariation">
				    <?php $__currentLoopData = $variations_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $variation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<label class="form-control-label" for="pcolor"><strong>Color</strong></label>
								<select name="pcolor[]" class="form-control">
									<option value=""<?php echo e(empty(old('product_color')) ? ' selected="selected"' : ''); ?>></option>
									<?php $__currentLoopData = $bladeVar['product_colors']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									    <option value="<?php echo e($color->id); ?>"<?php echo e($color->id == old('pcolor[]',$variation->color) ? ' selected="selected"' : ''); ?>><?php echo e($color->name); ?></option>
								    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</select>
								<small class="form-control-feedback"></small>
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<label class="form-control-label" for="psize"><strong>Size</strong></label>
								<select name="psize[]" class="form-control">
									<option value=""<?php echo e(empty(old('product_size')) ? ' selected="selected"' : ''); ?>></option>
									<?php $__currentLoopData = $bladeVar['product_sizes']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $size): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									    <option value="<?php echo e($size->id); ?>"<?php echo e($size->id == old('psize[]',$variation->size) ? ' selected="selected"' : ''); ?>><?php echo e($size->name); ?></option>
								    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</select>
								<small class="form-control-feedback"></small>
							</div>
						</div>
						<div class="col-4">
							<div class="form-group">
								<label class="form-control-label" for="pprice"><strong>Price</strong></label>
								<input type="number" min="0" step="any" class="form-control" name="pprice[]" value="<?php echo e(old('pprice[]', $variation->price)); ?>">
								<small class="form-control-feedback"></small>
							</div>
						</div>
						<div class="col-4">
							<div class="form-group">
								<label class="form-control-label" for="pdprice"><strong>Discounted Price</strong></label>
								<input type="number" min="0" step="any" class="form-control" name="pdprice[]" value="<?php echo e(old('pdprice[]',$variation->discountedPrice)); ?>">
								<small class="form-control-feedback"></small>
							</div>
						</div>
						<div class="col-4">
							<div class="form-group">
								<label class="form-control-label" for="pquantity"><strong>Quantity</strong></label>
								<input type="number" class="form-control" name="pquantity[]" value="<?php echo e(old('pquantity[]',$variation->quantity)); ?>">
								<small class="form-control-feedback"></small>
							</div>
						</div>
						<div class="col-4">
							<div class="form-group">
							    <?php $images_data = $variation->images; ?>
								<label class="form-control-label" for="images"><strong>Images (Size 600X600 px)</strong></label>
								<input type="file" name="images[]" class="form-control">
								<input type="hidden" name="imagesurl[]" class="form-control" value="<?php echo e(old('imagesurl[]',$images_data)); ?>">
								<small class="form-control-feedback"></small>
							</div>
						</div>
					</div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
				<div class="form-group text-right">
					<button class="btn btn-success" type="button" id="addVariation">Add Variation</button>
					<button class="btn btn-secondary removeVariation" type="button">Remove Variation</button>
				</div>
			</div>

			<div class="form-group">
				<label class="form-control-label" for="product_attributes"><strong>Attributes</strong></label>
				  <p>For multiple values use semicolon as seprator. e.g value1:value2;value3</p>
                  <?php $attribute_data = json_decode($bladeVar['result']->attribute); ?>
				<div id="allAttributes">
				<?php $__currentLoopData = $attribute_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $attribute): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<div class="row">
		         <div class="col-3"><div class="form-group">
					    <input type="text" class="form-control" name="attrName[]" value="<?php echo e(old('attrName[]',$attribute->name)); ?>" autofocus placeholder="Attribute Name">
			    </div></div>
			    <div class="col-3"><div class="form-group">
					    <input type="text" class="form-control" name="attrVal[]" value="<?php echo e(old('attrVal[]',$attribute->value)); ?>" autofocus placeholder="Attribute Value">
			    </div></div>
			  </div>
			  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			  </div>  
		    <div class="form-group text-right">
						<button class="btn btn-success" type="button" id="addAttribute">Add Attribute</button>
						<button class="btn btn-secondary removeAttribute" type="button">Remove Attribute</button>
				</div>
			</div>
			 <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
      	</form>
    </div>
</div>