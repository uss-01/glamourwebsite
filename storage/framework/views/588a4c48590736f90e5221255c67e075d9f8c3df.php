<?php echo $__env->make('frontend.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php if(!empty($bladeVar['productDetail'])): ?>
<style type="text/css">
.has-error {
    border: 1px solid red !important;
}
#responseMessage{
    display: none;
}
.unavailability {
    color : #dc3545 !important;
}
.addWishlist{
    color: red;
}
.colorActive{
    border-color: #c29958 !important;
}
</style>
    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="<?php echo e(url('/shop')); ?>">shop</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"><?php echo e($bladeVar['productDetail']['name']); ?></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- page main wrapper start -->
        <div class="shop-main-wrapper section-padding pb-0">
            <div class="container">
                <div class="row">
                    <!-- product details wrapper start -->
                    <div class="col-lg-12 order-1 order-lg-2">
                        <!-- product details inner end -->
                        <div class="product-details-inner">
                            <div class="row">
                                <div class="col-lg-5">
                                <?php if(!empty($bladeVar['slideimages'])): ?>
                                    <div class="product-large-slider">
                                    <?php $__currentLoopData = $bladeVar['slideimages']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="pro-large-img img-zoom">
                                            <img src="<?php echo e($slide['image']); ?>" alt="<?php echo e($bladeVar['productDetail']['name']); ?>" />
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                                    </div>
                                    <div class="pro-nav slick-row-10 slick-arrow-style">
                                    <?php $__currentLoopData = $bladeVar['slideimages']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="pro-nav-thumb">
                                            <img src="<?php echo e($slide['image']); ?>" alt="<?php echo e($bladeVar['productDetail']['name']); ?>" />
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                                    </div>
                                <?php endif; ?>
                                </div>
                                <div class="col-lg-7">
                                    <div class="product-details-des">
                                        <div class="manufacturer-name">
                                            <a href="<?php echo e(url('/shop/?category='.$bladeVar['productDetail']->product_category->_id)); ?>"><?php echo e($bladeVar['productDetail']->product_category->name); ?></a>
                                        </div>
                                        <h3 class="product-name"><?php echo e($bladeVar['productDetail']['name']); ?></h3>
                                        <div class="ratings d-flex">
                                            <?php if(!empty($bladeVar['productRatings'])): ?>
                                            <?php 
                                                $pratings = round($bladeVar['productRatings']['0']->ratings);
                                             ?>
                                            <?php if($pratings > 0): ?>
                                                    <?php for($i = 1 ; $i <= 5 ; $i++): ?>
                                                        <?php if($i <= $pratings): ?>
                                                        <span><i class="fa fa-star"></i></span>
                                                        <?php else: ?>
                                                        <span><i class="fa fa-star-o"></i></span>
                                                        <?php endif; ?>
                                                    <?php endfor; ?> 
                                                    
                                                    <?php if(!empty($bladeVar['productCommentsCount'])): ?>
                                                        <div class="pro-review">
                                                            <span><?php echo e($bladeVar['productCommentsCount']['0']->commentsCount); ?> Reviews</span>
                                                        </div>
                                                    <?php endif; ?> 
                                            <?php endif; ?>         
                                            <?php endif; ?>
                                        </div>
                                        <div class="price-box">
                                            <?php if(!empty($bladeVar['productDetail']['discounted_price']) && ($bladeVar['productDetail']['discounted_price'] != '0.00')): ?>
                                            <span class="price-regular">$<?php echo e($bladeVar['productDetail']['discounted_price']); ?></span>
                                            <span class="price-old"><del>$<?php echo e($bladeVar['productDetail']['price']); ?></del></span>
                                            <?php else: ?>
                                            <span class="price-regular">$<?php echo e($bladeVar['productDetail']['price']); ?></span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="availability">
                                            <?php if(!empty($bladeVar['productDetail']['product_quantity']) && ($bladeVar['productDetail']['product_quantity'] > 0)): ?>
                                                <i class="fa fa-check-circle"></i>
                                                <span><?php echo e($bladeVar['productDetail']['product_quantity']); ?> in stock</span>
                                            <?php else: ?>
                                                <i class="fa fa-times-circle unavailability"></i>
                                                <span>out of stock</span>
                                            <?php endif; ?>
                                        </div>
                                        <p class="pro-desc"><?php echo e($bladeVar['productDetail']['sort_details']); ?></p>
                                        <div class="quantity-cart-box d-flex align-items-center">
                                            <h6 class="option-title">qty:</h6>
                                            <div class="quantity">
                                                <div class="pro-qty"><input type="text" id="proQuantity" value="1"></div>
                                            </div>
                                            
                                            <div class="action_link">
                                                <?php if(!empty($bladeVar['productDetail']['product_quantity']) && ($bladeVar['productDetail']['product_quantity'] > 0)): ?>
                                                    <?php if(!empty($bladeVar['productDetail']['already_in_cart']) && ($bladeVar['productDetail']['already_in_cart'] == 'No')): ?>
                                                        <a class="btn btn-cart2 addToCartPro" href="#" id="addtocart<?php echo e($bladeVar['productDetail']['_id']); ?>" data-pid="<?php echo e($bladeVar['productDetail']['_id']); ?>" data-cid="<?php echo e($bladeVar['productDetail']['product_color_id']); ?>" data-sid="<?php echo e($bladeVar['productDetail']['product_size_id']); ?>">Add To Cart</a>
                                                    <?php else: ?>
                                                        <a class="btn btn-cart2 addToCartPro disabled" href="<?php echo e(url('/cart')); ?>" id="addtocart<?php echo e($bladeVar['productDetail']['_id']); ?>" data-pid="<?php echo e($bladeVar['productDetail']['_id']); ?>">Go To Cart</a>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="pro-size">
                                            <h6 class="option-title">size :</h6>
                                            <select class="nice-select" id="proSize">
                                            <?php 
                                            $sizeArray = array(); 
                                            $sizeArray[0] = $bladeVar['productDetail']['product_size_id'];
                                            if(!empty($bladeVar['productDetail']['variations'])){
                                                $variations = json_decode($bladeVar['productDetail']['variations']); 
                                                foreach($variations as $variation){
                                                    $sizeArray[] = $variation->size;
                                                }
                                            }
                                            $getSizeArray = array_unique($sizeArray);
                                            if(!empty($getSizeArray)){ 
                                                foreach($getSizeArray as $size){ 
                                                    $allSizes = $bladeVar['size_results'];
                                                    if(!empty($allSizes)){
                                                        foreach($allSizes as $getSize){
                                                            if($getSize->id == $size){ ?>
                                                                <option value="<?php echo $getSize->id; ?>"><?php echo $getSize->name; ?></option> 
                                                      <?php }
                                                        }
                                                    }
                                                }  
                                            } ?> 
                                            </select>
                                        </div>
                                        <div class="color-option">
                                            <h6 class="option-title">color :</h6>
                                            <?php
                                            $colorArray = array(); 
                                            $colorArray[0] = $bladeVar['productDetail']['product_color_id'];
                                            if(!empty($bladeVar['productDetail']['variations'])){
                                                $variations = json_decode($bladeVar['productDetail']['variations']); 
                                                foreach($variations as $variation){
                                                    $colorArray[] = $variation->color;
                                                }
                                            }
                                            $getColorArray = array_unique($colorArray);
                                            if(!empty($getColorArray)){ ?>
                                            <ul class="color-categories">
                                                <?php 
                                                $count = 0; foreach($getColorArray as $color){ 
                                                    $allColors = $bladeVar['color_results'];
                                                    if(!empty($allColors)){ 
                                                        foreach($allColors as $getColor){ 
                                                            if($getColor->id == $color){ $count++; ?>
                                                                <li class="<?php if($count == '1'){ echo 'colorActive'; } ?>">
                                                                <?php $varData = array();
                                                                $variations = json_decode($bladeVar['productDetail']['variations']); 
                                                                foreach($variations as $variation){
                                                                    if($getColor->id == $variation->color){
                                                                        $varData[] = $variation;
                                                                    }
                                                                }
                                                                ?>
                                                                    <a href="#" data-pcid="<?php echo $getColor->id; ?>" style="background-color: <?php echo $getColor->color_code; ?>;" title="<?php echo $getColor->name; ?>"></a>
                                                                </li>   
                                                            <?php }
                                                        }
                                                    }
                                                } ?>
                                            </ul>
                                            <?php } ?> 
                                        </div>
                                        <div class="useful-links">
                                            <?php if(!empty($bladeVar['productDetail']['already_in_Wishlist']) && ($bladeVar['productDetail']['already_in_Wishlist'] == 'No')): ?>
                                                <a href="#" class="addtowishlist" id="addtowishlist<?php echo e($bladeVar['productDetail']['_id']); ?>" data-pid="<?php echo e($bladeVar['productDetail']['_id']); ?>" data-toggle="tooltip"><i class="pe-7s-like"></i>wishlist</a>
                                            <?php else: ?>
                                                <a href="#" class="addtowishlist" id="addtowishlist<?php echo e($bladeVar['productDetail']['_id']); ?>" data-pid="<?php echo e($bladeVar['productDetail']['_id']); ?>" data-toggle="tooltip"><i class="pe-7s-like addWishlist"></i>wishlist</a>
                                            <?php endif; ?>
                                        </div>
                                        <div class="like-icon">
                                            <a href="http://www.facebook.com/share.php?u=<?php echo e(url('/shop/'.$bladeVar['productDetail']['slug'])); ?>&amp;title=<?php echo e($bladeVar['productDetail']['name']); ?>" class="facebook fbclick"><i class="fa fa-facebook"></i>Share</a>
                                            <a href="https://twitter.com/intent/tweet?text=<?php echo e($bladeVar['productDetail']['name']); ?>&amp;url=<?php echo e(url('/shop/'.$bladeVar['productDetail']['slug'])); ?>" class="twitter fbclick"><i class="fa fa-twitter"></i>Share</a>
                                            <a href="<?php echo e(url('/shop/'.$bladeVar['productDetail']['slug'])); ?>" target="_blank" data-image="<?php echo e($bladeVar['productDetail']['image']); ?>" data-desc="<?php echo e($bladeVar['productDetail']['name']); ?>" class="pinterest btnPinIt"><i class="fa fa-pinterest"></i>Share</a>
                                            <a href="mailto:?subject=Jewellery Ecommerce: <?php echo e($bladeVar['productDetail']['name']); ?>&amp;body=<?php echo e(url('/shop/'.$bladeVar['productDetail']['slug'])); ?>%0D<?php echo e(url('/')); ?>" class="envelope" title="<?php echo e($bladeVar['productDetail']['name']); ?>" style="background-color: #fe4419;"><i class="fa fa-envelope"></i>Share</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- product details inner end -->

                        <!-- product details reviews start -->
                        <div class="product-details-reviews section-padding pb-0">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="product-review-info">
                                        <ul class="nav review-tab">
                                            <li>
                                                <a class="active" data-toggle="tab" href="#tab_one">description</a>
                                            </li>
                                            <li>
                                                <a data-toggle="tab" href="#tab_two">information</a>
                                            </li>
                                            <li>
                                                <a data-toggle="tab" href="#tab_three">
                                                <?php if(!empty($bladeVar['productCommentsCount'])): ?>
                                                reviews (<?php echo e($bladeVar['productCommentsCount']['0']->commentsCount); ?>)
                                                <?php endif; ?>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content reviews-tab">
                                            <div class="tab-pane fade show active" id="tab_one">
                                                <div class="tab-one">
                                                    <p><?php echo e($bladeVar['productDetail']['details']); ?></p>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab_two">
                                                <table class="table table-bordered">
                                                    <tbody>
                                                    <?php if(!empty($bladeVar['productDetail']['attribute'])): ?>
                                                        <?php 
                                                        $attributes = $bladeVar['productDetail']['attribute'];
                                                         ?>
                                                        <?php $__currentLoopData = $attributes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $attribute): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <tr>
                                                            <td><?php echo e($attribute->name); ?></td>
                                                            <td><?php echo e($attribute->value); ?></td>
                                                        </tr>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>    
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="tab-pane fade" id="tab_three">
                                                <form action="#" class="review-form" id="commentForm">
                                                    <?php if(!empty($bladeVar['productComments'])): ?>
                                                    <h5><?php echo e($bladeVar['productCommentsCount']['0']->commentsCount); ?> review for <span><?php echo e($bladeVar['productDetail']['name']); ?></span></h5>
                                                    <?php $__currentLoopData = $bladeVar['productComments']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($comment->type == 'product'): ?>
                                                    <div class="total-reviews">
                                                        <div class="rev-avatar">
                                                            <img src="<?php echo e(asset('assets/img/about/avatar.jpg')); ?>" alt="<?php echo e($comment->name); ?>">
                                                        </div>
                                                        <div class="review-box">
                                                            <div class="ratings">
                                                                <?php 
                                                                $ratings = $comment->website;
                                                                 ?>
                                                                <?php for($i = 1 ; $i <= 5 ; $i++): ?>
                                                                    <?php if($i <= $ratings): ?>
                                                                    <span><i class="fa fa-star"></i></span>
                                                                    <?php else: ?>
                                                                    <span><i class="fa fa-star-o"></i></span>
                                                                    <?php endif; ?>
                                                                <?php endfor; ?>
                                                            </div>
                                                            <div class="post-author">
                                                                <p><span><?php echo e($comment->name); ?> -</span> 
                                                                <?php $timestamp = strtotime($comment->updated_at); ?>
                                                                <?php echo e(date('d F, Y', $timestamp).' at '.date('g:i a', $timestamp)); ?></p>
                                                            </div>
                                                            <p><?php echo e($comment->comment); ?></p>
                                                        </div>
                                                    </div>
                                                    <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                    <p class="form-messege" id="responseMessage"></p>
                                                    <input type="hidden" id="_token" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                    <input type="hidden" id="pid" class="form-control" value="<?php echo e($bladeVar['productDetail']['_id']); ?>">
                                                    <input type="hidden" id="ptitle" class="form-control" value="<?php echo e($bladeVar['productDetail']['name']); ?>"> 
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label class="col-form-label"><span class="text-danger">*</span>Your Name</label>
                                                            <input type="text" name="name" id="name" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label class="col-form-label"><span class="text-danger">*</span>Your Email</label>
                                                            <input type="email" name="email" id="email" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label class="col-form-label"><span class="text-danger">*</span>Your Review</label>
                                                            <textarea class="form-control" name="commnet" id="comment"></textarea>
                                                            <div class="help-block pt-10"><span class="text-danger">Note:</span>
                                                                HTML is not translated!
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label class="col-form-label"><span class="text-danger">*</span>Rating</label>
                                                            &nbsp;&nbsp;&nbsp; Bad&nbsp;
                                                            <input type="radio" value="1" name="rating">
                                                            &nbsp;
                                                            <input type="radio" value="2" name="rating">
                                                            &nbsp;
                                                            <input type="radio" value="3" name="rating">
                                                            &nbsp;
                                                            <input type="radio" value="4" name="rating">
                                                            &nbsp;
                                                            <input type="radio" value="5" name="rating" checked>
                                                            &nbsp;Good
                                                        </div>
                                                    </div>
                                                    <div class="buttons">
                                                        <button class="btn btn-sqr" id="postComment" type="submit" name="submit">Continue</button>
                                                    </div>
                                                </form> <!-- end of review-form -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- product details reviews end -->
                    </div>
                    <!-- product details wrapper end -->
                </div>
            </div>
        </div>
        <!-- page main wrapper end -->

        <!-- related products area start -->
        <?php if(!empty($bladeVar['youMayLike'])): ?>
        <section class="related-products section-padding">
            <div class="container">
            <?php if(sizeof($bladeVar['youMayLike']) > 0): ?>
                <div class="row">
                    <div class="col-12">
                        <!-- section title start -->
                        <div class="section-title text-center">
                            <h2 class="title">Related Products</h2>
                            <p class="sub-title">Add related products to weekly lineup</p>
                        </div>
                        <!-- section title start -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="product-carousel-4 slick-row-10 slick-arrow-style">
                            <!-- product item start -->
                            <?php $__currentLoopData = $bladeVar['youMayLike']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							    <?php if($product->deleted_at == NULL): ?>
                                    <div class="product-item">
                                        <figure class="product-thumb">
                                            <a href="<?php echo e(url('/shop/'.$product->slug)); ?>">
                                                <img class="pri-img" src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
                                                <img class="sec-img" src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
                                            </a>
                                            <div class="product-badge">
                                                <?php if(($product->new_arrival == '1') && ($product->on_sale == '1')): ?>
                                                <div class="product-label new">
                                                    <span>new</span>
                                                </div>
                                                <div class="product-label discount">
                                                    <span>sale</span>
                                                </div>
                                                <?php elseif($product->new_arrival == '1'): ?>
                                                <div class="product-label new">
                                                    <span>new</span>
                                                </div>
                                                <?php elseif($product->on_sale == '1'): ?>
                                                <div class="product-label new">
                                                    <span>sale</span>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                            <div class="button-group">
                                            <?php 
                                            $arrayWishlist = array();
                                            if(!empty($bladeVar['userWishlistData'])){ 
                                                foreach($bladeVar['userWishlistData'] as $wishlist){
                                                    $arrayWishlist[] = $wishlist->product_id;
                                                } 
                                            } 
                                            if (in_array($product->_id, $arrayWishlist)) { ?>
                                                <a href="#" class="addtowishlist" id="addtowishlist<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like addWishlist"></i></a>
                                            <?php }else{ ?>
                                                <a href="#" class="addtowishlist" id="addtowishlist<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like"></i></a>
                                            <?php } ?> 
                                            </div>
                                            <?php if(!empty($product->product_quantity) && ($product->product_quantity > 0)): ?>
                                                <?php if($product->add_to_cart == '1'): ?>
                                                <div class="cart-hover">
                                                    <?php $arrayCartList = array();
                                                    if(!empty($bladeVar['userCartData'])){ 
                                                        foreach($bladeVar['userCartData'] as $cartList){
                                                            $arrayCartList[] = $cartList->product_id;
                                                        } 
                                                    } 
                                                    if (in_array($product->_id, $arrayCartList)) { ?>
                                                        <button class="btn btn-cart addtocart disabled" id="addtocart<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>">Go to cart</button>
                                                    <?php }else{ ?>
                                                        <button class="btn btn-cart addtocart" id="addtocart<?php echo e($product->_id); ?>" data-pid="<?php echo e($product->_id); ?>" data-cid="<?php echo e($product->product_color_id); ?>" data-sid="<?php echo e($product->product_size_id); ?>">add to cart</button>
                                                    <?php } ?>
                                                </div>
                                                <?php endif; ?>
                                            <?php else: ?>
                                                <div class="cart-hover">
                                                    <button class="btn btn-cart disabled">Out of stock</button>
                                                </div>
                                            <?php endif; ?>    
                                        </figure>
                                        <div class="product-caption text-center">
                                            <div class="product-identity">
                                                <p class="manufacturer-name"><a href="<?php echo e(url('/shop/?category='.$product->product_category->slug)); ?>"><?php echo e($product->product_category->name); ?></a></p>
                                            </div>
                                            <?php
                                                $colorArray = array(); 
                                                $colorArray[0] = $product->product_color_id;
                                                if(!empty($product->variations)){
                                                    $variations = json_decode($product->variations); 
                                                    foreach($variations as $variation){
                                                        $colorArray[] = $variation->color;
                                                    }
                                                }
                                                $getColorArray = array_unique($colorArray);
                                                if(!empty($getColorArray)){ 
                                                ?>
                                                <ul class="color-categories">
                                                <?php foreach($getColorArray as $color){ 
                                                    $allColors = $bladeVar['color_results'];
                                                    if(!empty($allColors)){
                                                        foreach($allColors as $getColor){
                                                            if($getColor->id == $color){ ?>
                                                                <li>
                                                                    <a href="#" style="background-color: <?php echo $getColor->color_code; ?>;" title="<?php echo $getColor->name; ?>"></a>
                                                                </li>   
                                                            <?php }
                                                        }
                                                    }
                                                } ?>
                                                </ul>
                                                <?php } ?>
                                            <h6 class="product-name">
                                                <a href="<?php echo e(url('/shop/'.$product->slug)); ?>"><?php echo e($product->name); ?></a>
                                            </h6>
                                            <div class="price-box">
                                                <?php if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')): ?>
                                                <span class="price-regular">$<?php echo e($product->discounted_price); ?></span>
                                                <span class="price-old"><del>$<?php echo e($product->price); ?></del></span>
                                                <?php else: ?>
                                                <span class="price-regular">$<?php echo e($product->price); ?></span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
						    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <!-- product item end -->
                        </div>
                    </div>
                </div>
            <?php endif; ?>    
            </div>
        </section>
        <?php endif; ?>
        <!-- related products area end -->
    </main>
<?php endif; ?>    
<?php echo $__env->make('frontend.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script type="text/javascript">
$(document).ready(function() {
    var baseUrl = '<?php echo e(url("/")); ?>';
    function isEmail(emailid) {
		var pattern = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
		return pattern.test(emailid);
    }

    $("#postComment").on('click', function(event) {
		event.preventDefault();
        $('#responseMessage').hide().html('');
        var token = $('#_token').val();
		var getId = $('#commentForm #pid').val();
		var ptitle = $('#commentForm #ptitle').val();
		var name = $('#commentForm #name').val();
		var email = $('#commentForm #email').val();
        var rating = $("input[name='rating']:checked").val(); //$('#commentForm #rating').val();
		var comment = $('#commentForm #comment').val();
		if (name == '') {
			$('#commentForm #name').addClass('has-error');
		} else if (name != '') {
			$('#commentForm #name').removeClass('has-error');
		}
		if ((email == '') || (!isEmail($('#commentForm #email').val()))) {
			$('#commentForm #email').addClass('has-error');
		} else if ((email != '') && (isEmail($('#commentForm #email').val()))) {
			$('#commentForm #email').removeClass('has-error');
		}
        if (comment == '') {
			$('#commentForm #comment').addClass('has-error');
		} else if (comment != '') {
			$('#commentForm #comment').removeClass('has-error');
		}
		if ((name != '') && (email != '') && (isEmail($('#commentForm #email').val())) && (comment != '') && (rating != '')) {
			$.ajax({
				url: baseUrl + "/sendcomment",
				type: "POST",
				data: {
                    '_token': token,   
					'postId': getId,
					'ptitle': ptitle,
					'name': name,
					'email': email,
                    'website': rating,
					'comment': comment,
                    'type' : 'product'
				},
				dataType: "JSON",
				success: function(jsonStr) {
					var res_data = JSON.stringify(jsonStr);
					var response = JSON.parse(res_data);
					var responseData = response['responseData'];
					if ((responseData != null) && (responseData == 'comment send successfully')) {
						$('#responseMessage').show().html('').html('Your comment sent successfully');
                        $("#commentForm").trigger("reset");
                        setTimeout(function(){ location.reload(); }, 2000);
					} else {
						$('#responseMessage').show().html('').html(responseData);
					}
				}
			});
		}
    });

    $('.fbclick').click(function(event) {
        var width  = 575,
            height = 400,
            left   = ($(window).width()  - width)  / 2,
            top    = ($(window).height() - height) / 2,
            url    = this.href,
            opts   = 'status=1' +
                    ',width='  + width  +
                    ',height=' + height +
                    ',top='    + top    +
                    ',left='   + left;
        window.open(url, 'twitter', opts);
        return false;
    });	

    $('.btnPinIt').click(function() {
        var url = $(this).attr('href');
        var media = $(this).attr('data-image');
        var desc = $(this).attr('data-desc');
        window.open("//www.pinterest.com/pin/create/button/"+
        "?url="+url+
        "&media="+media+
        "&description="+desc,"pinIt","toolbar=no, scrollbars=no, resizable=no, top=0, right=0, width=750, height=320");
        return false;
    });
    
});
</script>