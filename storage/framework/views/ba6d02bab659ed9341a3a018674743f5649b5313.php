<div class="row">
	<div class="col-12">
		<form action="<?php echo e(route('blogs.update', $bladeVar['result']->_id)); ?>" method="POST">
			<?php echo e(method_field('PUT')); ?>

			<div class="row">
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="blog_category"><strong>Category</strong><small class="text-danger"> (required)</small></label>
						<select name="blog_category" class="form-control">
							<option value=""<?php echo e(empty(old('blog_category')) ? ' selected="selected"' : ''); ?>></option>
							<?php $__currentLoopData = $bladeVar['blog_categories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $blog_category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($blog_category->_id); ?>"<?php echo e($blog_category->_id == old('blog_category',$bladeVar['result']->blog_category_id) ? ' selected="selected"' : ''); ?>><?php echo e($blog_category->name); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
						<div class="other_option_wrap"></div>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="blog_tag"><strong>Tags</strong></label>
						<select name="blog_tag" class="form-control">
							<option value=""<?php echo e(empty(old('blog_tag')) ? ' selected="selected"' : ''); ?>></option>
							<?php $__currentLoopData = $bladeVar['blog_tags']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $blog_tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value="<?php echo e($blog_tag->id); ?>"<?php echo e($blog_tag->id == old('blog_tag',$bladeVar['result']->blog_tag_id) ? ' selected="selected"' : ''); ?>><?php echo e($blog_tag->name); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
						<div class="other_option_wrap2"></div>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="blog_status"><strong>Status</strong></label>
						<select name="blog_status" class="form-control">
							<option value=""<?php echo e(empty(old('blog_status')) ? ' selected="selected"' : ''); ?>></option>
							<?php $__currentLoopData = $bladeVar['blogStatus']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $blog_status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($blog_status); ?>"<?php echo e($blog_status == old('blog_status', $bladeVar['result']->blog_status) ? ' selected="selected"' : ''); ?>><?php echo e($blog_status); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select>
						<small class="form-control-feedback"></small>
					</div>
				</div>
			</div>
            <div class="row">
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="name"><strong>Name</strong><small class="text-danger"> (required)</small></label>
						<input type="text" class="form-control" name="name" value="<?php echo e(old('name', $bladeVar['result']->name)); ?>" autofocus>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="audio_url"><strong>Audio Url</strong></label>
						<input type="text" class="form-control" name="audio_url" value="<?php echo e(old('audio_url', $bladeVar['result']->audio_url)); ?>" autofocus>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="video_url"><strong>Video Url</strong></label>
						<input type="text" class="form-control" name="video_url" value="<?php echo e(old('video_url', $bladeVar['result']->video_url)); ?>" autofocus>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="image"><strong>Image (Size 1110X800 px)</strong> <small class="text-danger">(required)</small></label>
						<input type="file" name="image" class="form-control">
						<small class="form-control-feedback"></small>
					</div>
				</div>
            </div>
			<div class="row">
		        <div class="col-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" value="1" name="blog_audio" <?php echo e($bladeVar['result']->blog_audio == 1 ? ' checked="checked"' : ''); ?> >
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><strong>Audio Blog</strong></span>
						</label>
					</div>
			    </div>
				<div class="col-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" name="blog_video" value="1" <?php echo e($bladeVar['result']->blog_video == 1 ? ' checked="checked"' : ''); ?> >
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><strong>Video Blog</strong></span>
						</label>
					</div>
			    </div>
				<div class="col-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" name="blog_image_slide" value="1" <?php echo e($bladeVar['result']->blog_image_slide == 1 ? ' checked="checked"' : ''); ?> >
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><strong>Image Slider Blog</strong></span>
						</label>
					</div>
			    </div>
		    </div>
			<div class="form-group">
				<label class="form-control-label" for="details"><strong>Details</strong></label>
				<textarea class="form-control" id="htmlTextEditor" name="details"><?php echo e(old('details', $bladeVar['result']->details)); ?></textarea>
				<small class="form-control-feedback"></small>
			</div>
			 <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
      	</form>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('#htmlTextEditor').summernote({
		height:300,
		dialogsInBody: true
    });
  });
</script>