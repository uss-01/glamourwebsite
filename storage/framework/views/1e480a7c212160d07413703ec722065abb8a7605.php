<div class="row">
	<div class="col-12">
		<form action="<?php echo e(route('setting.store')); ?>" method="POST" enctype="multipart/form-data">
			<div class="form-group">
				<label class="form-control-label" for="key"><strong>Key</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="key" value="<?php echo e(old('key')); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="value"><strong>Value</strong> <small class="text-danger">(required)</small></label>
				<textarea class="form-control" id="htmlTextEditor" name="value"><?php echo e(old('value')); ?></textarea>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="image"><strong>Image</strong></label>
				<input type="file" name="image" class="form-control">
				<small class="form-control-feedback"></small>
			</div>
            <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
		</form>
	</div>
</div>
<script type="text/javascript">
     $(document).ready(function() {
	$('#htmlTextEditor').summernote({
              height:200,
              dialogsInBody: true
            });
  });
</script>