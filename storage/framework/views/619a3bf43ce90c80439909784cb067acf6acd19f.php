<div class="row">
	<div class="col-12">
		<form action="<?php echo e(route('event.update', $bladeVar['result']->_id)); ?>" method="POST">
			<?php echo e(method_field('PUT')); ?>

			 <div class="row">
			<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="title"><strong>Title</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="title" value="<?php echo e(old('title', $bladeVar['result']->title)); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			</div>
			<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="sub_title"><strong>Sub Title</strong> <small class="text-danger"></small></label>
				<input type="text" class="form-control" name="sub_title" value="<?php echo e(old('sub_title', $bladeVar['result']->sub_title)); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			</div>
            
      <div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="event_time"><strong>Event Start Time</strong> <small class="text-danger"></small></label>
				<input type="text" class="form-control" name="event_time" value="<?php echo e(old('event_time', $bladeVar['result']->event_time)); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			</div>
			<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="event_location"><strong>Event Location</strong> <small class="text-danger"></small></label>
				<input type="text" class="form-control" name="event_location" value="<?php echo e(old('event_location', $bladeVar['result']->event_location)); ?>" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			</div>
            
            </div>
             <div class="form-group">
				<label class="form-control-label" for="image"><strong>Event Image</strong> <small class="text-danger">(required)</small></label>
				<input type="file" name="image" class="form-control">
				<small class="form-control-feedback"></small>
			</div>
			<div class="row">
			<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="start_date"><strong>Start Date</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control bg-ffffff"  name="start_date" value="<?php echo e(old('start_date',Carbon\Carbon::parse($bladeVar['result']->start_date)->format('d/m/Y'))); ?>" readonly>
					<input type="hidden" name="start_date_alt" value="<?php echo e(Carbon\Carbon::parse($bladeVar['result']->start_date)->format('Y-m-d')); ?>" />
				<small class="form-control-feedback"></small>
			</div>
			</div>
			<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="end_date"><strong>End Date</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control bg-ffffff"  name="end_date" value="<?php echo e(old('end_date',Carbon\Carbon::parse($bladeVar['result']->end_date)->format('d/m/Y'))); ?>" readonly>
					<input type="hidden" name="end_date_alt" value="<?php echo e(Carbon\Carbon::parse($bladeVar['result']->end_date)->format('Y-m-d')); ?>" />
				<small class="form-control-feedback"></small>
			</div>
            </div>
            </div>
			<div class="form-group">
				<label class="form-control-label" for="details"><strong>Details</strong></label>
				<textarea class="form-control" id="htmlTextEditor" name="details"><?php echo e(old('details',$bladeVar['result']->details)); ?></textarea>
				<small class="form-control-feedback"></small>
			</div>
			 <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
			
      	</form>
    </div>
</div>
<script type="text/javascript">
     $(document).ready(function() {
	$('#htmlTextEditor').summernote({
              height:300,
              dialogsInBody: true
            });
  });
</script>