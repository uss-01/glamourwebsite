<div class="minicart-close">
    <i class="pe-7s-close"></i>
</div>    
<?php if(!empty($bladeVar['userCartProducts'])): ?>
<?php
    $arrayCartlist = array();
    if(!empty($bladeVar['userCartData'])){ 
        foreach($bladeVar['userCartData'] as $cartlist){
            $arrayCartlist[] = $cartlist->product_id;
        } 
    } ?>
    <?php if(!empty($arrayCartlist)): ?> 
        <?php if(sizeof($bladeVar['userCartProducts']) > 0): ?>     
        <div class="minicart-content-box">
            <div class="minicart-item-wrapper">
                <ul>
                    <?php $__currentLoopData = $bladeVar['userCartProducts']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($product->deleted_at == NULL): ?>
                            <?php if(in_array($product->_id, $arrayCartlist)): ?>
                                <li class="minicart-item" id="product<?php echo e($product->_id); ?>">
                                    <div class="minicart-thumb">
                                        <a href="<?php echo e(url('/shop/'.$product->slug)); ?>">
                                            <img src="<?php echo e(asset('images/products').'/'.$product->image); ?>" alt="<?php echo e($product->slug); ?>">
                                        </a>
                                    </div>
                                    <div class="minicart-content">
                                        <h3 class="product-name">
                                            <a href="<?php echo e(url('/shop/'.$product->slug)); ?>"><?php echo e($product->name); ?></a>
                                        </h3>
                                        <p>
                                            <span class="cart-quantity"><?php echo e($product->quantity); ?> <strong>&times;</strong></span>
                                            <span class="cart-price">
                                            <?php if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')): ?>
                                                $<?php echo e(number_format($product->discounted_price, 2)); ?>

                                            <?php else: ?>
                                                $<?php echo e(number_format($product->price, 2)); ?>

                                            <?php endif; ?>
                                            </span>
                                        </p>
                                    </div>
                                    <button class="minicart-remove removetocart" data-pid="<?php echo e($product->_id); ?>"><i class="pe-7s-close"></i></button>
                                </li>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>

            <div class="minicart-pricing-box">
                <ul>
                    <?php 
                        $subTotal = array();
                        $shipping = '0.00';
                        foreach($bladeVar['userCartProducts'] as $product){
                            if($product->deleted_at == NULL){
                                if(in_array($product->_id, $arrayCartlist)){
                                    if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')){
                                        $subTotal[] = number_format($product->discounted_price * $product->quantity, 2);
                                    }else{
                                        $subTotal[] = number_format($product->price * $product->quantity, 2);
                                    }
                                }
                            }
                        }
                    ?>
                    <li>
                        <span>sub-total</span>
                        <span><strong>$<?php if(!empty($subTotal)){ echo number_format(array_sum($subTotal), 2); }else{ echo '0.00'; } ?></strong></span>
                    </li>
                    <li>
                        <span>shipping</span>
                        <span><strong>$<?php if(!empty($shipping)){ echo $shipping; }else{ echo '0.00'; } ?></strong></span>
                    </li>
                    <li class="total">
                        <span>total</span>
                        <span><strong>$<?php if(!empty($subTotal)){ echo number_format((array_sum($subTotal)+$shipping), 2); }else{ echo '0.00'; } ?></strong></span>
                    </li>
                </ul>
            </div>

            <div class="minicart-button">
                <a href="<?php echo e(url('/cart')); ?>"><i class="fa fa-shopping-cart"></i> View Cart</a>
                <a href="<?php echo e(url('/checkout')); ?>"><i class="fa fa-share"></i> Checkout</a>
            </div>
        </div>
        <?php endif; ?>
    <?php else: ?>
        <div class="row">
            <div class="col-lg-12" style="text-align: center;">
                <h3>Your cart is empty!</h3>
                <p>Add items to it now.</p>
                <a href="<?php echo e(url('/')); ?>" class="btn btn-sqr">Shop now</a>
            </div>
        </div>    
    <?php endif; ?>
<?php else: ?>
    <div class="row">
        <div class="col-lg-12" style="text-align: center;">
            <h3>Your cart is empty!</h3>
            <p>Add items to it now.</p>
            <a href="<?php echo e(url('/')); ?>" class="btn btn-sqr">Shop now</a>
        </div>
    </div>    
<?php endif; ?>