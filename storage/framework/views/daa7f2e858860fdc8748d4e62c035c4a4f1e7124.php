<?php echo $__env->make('frontend.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Blog List</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- blog main wrapper start -->
        <div class="blog-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 order-2 order-lg-1">
                        <aside class="blog-sidebar-wrapper">
                            <div class="blog-sidebar">
                                <h5 class="title">search</h5>
                                <div class="sidebar-serch-form">
                                    <form action="<?php echo e(url('/blog/')); ?>" method="get">
                                        <input type="text" class="search-field" id="search" name="search" placeholder="search here">
                                        <button type="submit" class="search-btn"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            </div> <!-- single sidebar end -->
                            <?php if(!empty($bladeVar['blogCategories'])): ?>
                            <div class="blog-sidebar">
                                <h5 class="title">categories</h5>
                                <ul class="blog-archive blog-category">
                                <?php $__currentLoopData = $bladeVar['blogCategories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($category->deleted_at == NULL): ?>
                                    <?php if($category->categoryBlogs != 0): ?>
                                    <li><a href="<?php echo e(url('/blog/?category='.$category->slug)); ?>"><?php echo e($category->name); ?> (<?php echo e($category->categoryBlogs); ?>)</a></li>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div> 
                            <?php endif; ?>
                            <!-- single sidebar end -->
                            <?php if(!empty($bladeVar['blogArchives'])): ?>

                            <div class="blog-sidebar">
                                <h5 class="title">Blog Archives</h5>
                                <ul class="blog-archive">
                                    <?php $__currentLoopData = $bladeVar['blogArchives']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $archive): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><a href="<?php echo e(url('/blog/?archive='.$archive->monthname)); ?>"><?php echo e($archive->monthname); ?> (<?php echo e($archive->count); ?>)</a></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div> 
                            <?php endif; ?>
                            <!-- single sidebar end -->
                            <?php if(!empty($bladeVar['recent_post']->count() > 0)): ?>
                            <div class="blog-sidebar">
                                <h5 class="title">recent post</h5>
                                <div class="recent-post">
                                    <?php $__currentLoopData = $bladeVar['recent_post']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($blog->deleted_at == NULL): ?>
                                    <div class="recent-post-item">
                                        <figure class="product-thumb">
                                            <a href="<?php echo e(url('/blog/'.$blog->slug)); ?>">
                                                <img src="<?php echo e(asset('images/blogs').'/'.$blog->image); ?>" alt="<?php echo e($blog->slug); ?>">
                                            </a>
                                        </figure>
                                        <div class="recent-post-description">
                                            <div class="product-name">
                                                <h6><a href="<?php echo e(url('/blog/'.$blog->slug)); ?>"><?php echo e($blog->name); ?></a></h6>
                                                <p><?php echo e(date('F j Y', strtotime($blog->updated_at))); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
                                </div>
                            </div> 
                            <?php endif; ?>
                            <!-- single sidebar end -->
                            <?php if(!empty($bladeVar['blogTags'])): ?>
                            <div class="blog-sidebar">
                                <h5 class="title">Tags</h5>
                                <ul class="blog-tags">
                                <?php $__currentLoopData = $bladeVar['blogTags']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($tag->deleted_at == NULL): ?>
                                <?php if($tag->tagBlogs != 0): ?>
                                    <li><a href="<?php echo e(url('/blog/?tag='.$tag->slug)); ?>"><?php echo e($tag->name); ?></a></li>
                                <?php endif; ?>
                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div> 
                            <?php endif; ?>
                            <!-- single sidebar end -->
                        </aside>
                    </div>
                    <div class="col-lg-9 order-1 order-lg-2">
                        <div class="blog-item-wrapper">
                            <!-- blog item wrapper end -->
                            <?php if(!empty($bladeVar['results']->count() > 0)): ?>
                            <div class="row mbn-30">
                                <?php $__currentLoopData = $bladeVar['results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($blog->deleted_at == NULL): ?>
                                        <?php if($blog->blog_image_slide == '1'): ?>
                                        <div class="col-md-6">
                                            <div class="blog-post-item mb-30">
                                                <?php if(!empty($bladeVar['blogPics'])): ?>
                                                <figure class="blog-thumb">
                                                    <div class="blog-carousel-2 slick-row-15 slick-arrow-style slick-dot-style">
                                                        <?php $__currentLoopData = $bladeVar['blogPics']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($pic->blog_id == $blog->_id): ?>
                                                            <div class="blog-single-slide  ">
                                                                <a href="<?php echo e(url('/blog/'.$blog->slug)); ?>">
                                                                    <img src="<?php echo e(asset('images/blogs').'/'.$pic->image); ?>" alt="<?php echo e($blog->slug); ?>">
                                                                </a>
                                                            </div>
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </div>
                                                </figure>
                                                <?php endif; ?>
                                                <div class="blog-content">
                                                    <div class="blog-meta">
                                                        <p><?php echo e(date('d/m/Y', strtotime($blog->updated_at))); ?></p>
                                                    </div>
                                                    <h4 class="blog-title">
                                                        <a href="<?php echo e(url('/blog/'.$blog->slug)); ?>"><?php echo e($blog->name); ?></a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div> 
                                        <?php elseif($blog->blog_audio == '1'): ?>
                                        <div class="col-md-6">
                                            <div class="blog-post-item mb-30">
                                                <figure class="blog-thumb embed-responsive embed-responsive-16by9">
                                                    <iframe src="<?php echo e($blog->audio_url); ?>"></iframe>
                                                </figure>
                                                <div class="blog-content">
                                                    <div class="blog-meta">
                                                        <p><?php echo e(date('d/m/Y', strtotime($blog->updated_at))); ?></p>
                                                    </div>
                                                    <h4 class="blog-title">
                                                        <a href="<?php echo e(url('/blog/'.$blog->slug)); ?>"><?php echo e($blog->name); ?></a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                        <?php elseif($blog->blog_video == '1'): ?>
                                        <div class="col-md-6">
                                            <div class="blog-post-item mb-30">
                                                <figure class="blog-thumb embed-responsive embed-responsive-16by9">
                                                    <iframe src="<?php echo e($blog->video_url); ?>" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                                </figure>
                                                <div class="blog-content">
                                                    <div class="blog-meta">
                                                        <p><?php echo e(date('d/m/Y', strtotime($blog->updated_at))); ?></p>
                                                    </div>
                                                    <h4 class="blog-title">
                                                        <a href="<?php echo e(url('/blog/'.$blog->slug)); ?>"><?php echo e($blog->name); ?></a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                        <?php else: ?>  
                                        <div class="col-md-6">
                                            <div class="blog-post-item mb-30">
                                                <figure class="blog-thumb">
                                                    <a href="<?php echo e(url('/blog/'.$blog->slug)); ?>">
                                                        <img src="<?php echo e(asset('images/blogs').'/'.$blog->image); ?>" alt="<?php echo e($blog->slug); ?>">
                                                    </a>
                                                </figure>
                                                <div class="blog-content">
                                                    <div class="blog-meta">
                                                        <p><?php echo e(date('d/m/Y', strtotime($blog->updated_at))); ?></p>
                                                    </div>
                                                    <h4 class="blog-title">
                                                        <a href="<?php echo e(url('/blog/'.$blog->slug)); ?>"><?php echo e($blog->name); ?></a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                            <!-- blog item wrapper end -->

                            <!-- start pagination area -->
                            <div class="paginatoin-area text-center">
                                <?php echo e($bladeVar['results']->appends(Request::query())->links()); ?>

                                <!-- <ul class="pagination-box">
                                    <li><a class="previous" href="#"><i class="pe-7s-angle-left"></i></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a class="next" href="#"><i class="pe-7s-angle-right"></i></a></li>
                                </ul> -->
                            </div>
                            <!-- end pagination area -->
                            <?php else: ?>
                                <div class="paginatoin-area text-center">
                                    <strong>No records found!</strong>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- blog main wrapper end -->
    </main>
<?php echo $__env->make('frontend.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
