<div class="row">
	<div class="col-12">
		<form action="<?php echo e(route('setting.update', $bladeVar['result']->_id)); ?>" method="POST" enctype="multipart/form-data">
			<?php echo e(method_field('PUT')); ?>

			<div class="row">
				<div class="col-12">
					<div class="form-group">
						<label class="form-control-label" for="title"><strong>Title</strong></label>
						<input type="text" class="form-control" name="title" value="<?php echo e(old('title',$bladeVar['result']->title)); ?>" />
						<small class="form-control-feedback"></small>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="email"><strong>Email</strong></label>
						<input type="text" class="form-control" name="email" value="<?php echo e(old('email',$bladeVar['result']->email)); ?>" />
						<small class="form-control-feedback"></small>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="phone"><strong>Contact No.</strong></label>
						<input type="text" class="form-control" name="phone" value="<?php echo e(old('phone',$bladeVar['result']->phone)); ?>" />
						<small class="form-control-feedback"></small>
					</div>
                    <div class="form-group">
						<label class="form-control-label" for="facebook_url"><strong>Facebook Url</strong></label>
						<input type="text" class="form-control" name="facebook_url" value="<?php echo e(old('facebook_url',$bladeVar['result']->facebook_url)); ?>" />
						<small class="form-control-feedback"></small>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="twitter_url"><strong>Twitter Url</strong></label>
						<input type="text" class="form-control" name="twitter_url" value="<?php echo e(old('twitter_url',$bladeVar['result']->twitter_url)); ?>" />
						<small class="form-control-feedback"></small>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="instagram_url"><strong>Instagram Url</strong></label>
						<input type="text" class="form-control" name="instagram_url" value="<?php echo e(old('instagram_url',$bladeVar['result']->instagram_url)); ?>" />
						<small class="form-control-feedback"></small>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="youtube_url"><strong>Youtube Url</strong></label>
						<input type="text" class="form-control" name="youtube_url" value="<?php echo e(old('youtube_url',$bladeVar['result']->youtube_url)); ?>" />
						<small class="form-control-feedback"></small>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="linkedin_url"><strong>Linkedin Url</strong></label>
						<input type="text" class="form-control" name="linkedin_url" value="<?php echo e(old('linkedin_url',$bladeVar['result']->linkedin_url)); ?>" />
						<small class="form-control-feedback"></small>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="pinterest_url"><strong>Pinterest Url</strong></label>
						<input type="text" class="form-control" name="pinterest_url" value="<?php echo e(old('pinterest_url',$bladeVar['result']->pinterest_url)); ?>" />
						<small class="form-control-feedback"></small>
					</div>
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" name="shipping" value="1" <?php echo e($bladeVar['result']->shipping == 1 ? ' checked="checked"' : ''); ?> >
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><strong>Service Policy</strong></span>
						</label>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="address"><strong>Address</strong></label>
						<textarea class="form-control" name="address"><?php echo e(old('address',$bladeVar['result']->address)); ?></textarea>
						<small class="form-control-feedback"></small>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="message"><strong>Message</strong></label>
						<textarea class="form-control" name="message"><?php echo e(old('message',$bladeVar['result']->message)); ?></textarea>
						<small class="form-control-feedback"></small>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="image"><strong>Image</strong></label>
						<input type="file" name="image" class="form-control">
						<small class="form-control-feedback"></small>
					</div>
				</div>
			</div>
			<div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
      	</form>
    </div>
</div>