<?php

namespace App\Leasing;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeasingEnquiry extends Model{
    use SoftDeletes;
    protected $table = 'leasing_enquiries';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id','brand','companyName','details','designation','address','mobile','email','website','category','concept','stores'
    ];
}
