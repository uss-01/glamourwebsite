<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserJoinOffer extends Model
{
    protected $table = 'user_join_offer';
    protected $primaryKey = '_id';
    protected $fillable = ['user_id','offer_id','created_at','updated_at'];
}
