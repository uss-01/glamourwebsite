<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comments extends Model
{
    use SoftDeletes;
	
    protected $table = 'comments';
    protected $primaryKey = 'id';
    protected $fillable = [
        'token','name','website','email','comment','postName','postId','reply','replyBy','type','created_by','updated_by' 
    ];
}
