<?php

namespace App\Coupons;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupons extends Model
{
	use SoftDeletes;
    protected $table = 'coupons';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name','slug','value','quantity','type','expiry','created_by','updated_by' 
    ];
}
