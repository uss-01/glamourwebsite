<?php

namespace App\Gallery;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Album extends Model
{   use SoftDeletes;
	
    protected $table = 'gallery_albums';
    protected $primaryKey = '_id';
    protected $fillable = [
        'name','image','created_by','updated_by' 
    ];
}
