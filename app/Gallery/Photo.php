<?php

namespace App\Gallery;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Photo extends Model
{
    use SoftDeletes;
	
    protected $table = 'gallery_images';
    protected $primaryKey = '_id';
    protected $fillable = [
        'title','image','album_id','created_by','updated_by' 
    ];

     public function album()
    {
    	return $this->belongsTo('App\Gallery\Album', 'album_id', '_id');	
    }
}
