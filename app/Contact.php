<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;
	
    protected $table = 'contact';
    protected $primaryKey = 'id';
    protected $fillable = [
        'token','name','phone','email','subject','message','created_by','updated_by' 
    ];
}
