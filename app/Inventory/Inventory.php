<?php

namespace App\Inventory;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventory extends Model
{
    use SoftDeletes; 

    protected $table = 'order_details';
    protected $primaryKey = '_id';
    protected $fillable = [
        'order_id','product_id','product_seller_id','product_name','product_details','product_image','quantity','attr_key','attr_val','price','order_status','created_at','updated_at','deleted_at'
    ];
}