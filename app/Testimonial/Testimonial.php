<?php

namespace App\Testimonial;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Testimonial extends Model
{
	use SoftDeletes;
    protected $table = 'testimonial';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name','slug','shortDescription','image','created_by','updated_by' 
    ];
}
