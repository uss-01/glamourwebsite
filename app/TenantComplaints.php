<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TenantComplaints extends Model
{
	use SoftDeletes;
	protected $table = 'tenant_complaints';
	protected $primaryKey = 'id';
	    protected $fillable = [
        'tenant_id','complaint','admin_id','reply','status','created_at','updated_at' 
    ];
}
