<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class SiteUsers extends Authenticatable
{
    use SoftDeletes;
	use Notifiable;
    
    protected $table = 'site_users';
    protected $primaryKey = '_id';
	protected $fillable = ['username', 'fname', 'lname', 'password', 'fbid','token','email','phone','country', 'ipAddress','dob','gender','userAddress','about','notify_offers','notify_push','current_work','profile_mode','push_token','device','lat','long','membership'];
}
