<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tenantadmin extends Model
{
	use SoftDeletes;
	protected $table = 'tenantadmins';
	protected $primaryKey = 'id';
	protected $fillable = [
        'name', 'email', 'password', 'type', 'tenant_id', 'shop_name', 'remember_token' 
    ];
}
