<?php

namespace App\Message;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';
    protected $primaryKey = '_id';
    protected $fillable = ['from_user_id','to_user_id','parent_id','type','subject','message','from_read','to_read','from_del','to_del','image','redirect_to','redirect_id'];
}
