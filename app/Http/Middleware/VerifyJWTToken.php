<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use JWTAuthException;

use App\SiteUsers;

class VerifyJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
	 public function handle($request, Closure $next)
    {
        try {
            $getRecord = SiteUsers::where(['token' => $request->token])->count();
            if(!$getRecord) {
                //return response()->json(['token_invalid']);
                $responseData = [
                    'code' => '401',
                    'content' => ['error ' => "Token Invalid"]
                ];
                return response()->json($responseData);
            }
		} catch (\Exception $e) {
			$responseData = [
				'code' => '500',
				'content' => ['error ' => "Internal Server Error"]
			];
			return response()->json($responseData);
		}
		/*try{
            $user = JWTAuth::toUser($request->input('token'));
        }catch (JWTException $e) {
            if($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return response()->json(['token_expired'], $e->getStatusCode());
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json(['token_invalid'], $e->getStatusCode());
            }else{
                return response()->json(['error'=>'Token is required']);
            }
        }*/
       return $next($request);
    }
	
    /*public function handle($request, Closure $next)
    {
        return $next($request);
    }*/
}
