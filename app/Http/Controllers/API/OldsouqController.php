<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cms;
use App\Shop\Shop;
use App\Shop\ShopPics;
use App\Product\Product;
use App\Favourite;
use App\Masters\Category;
use App\Masters\Store;
use App\Masters\ProductCategory;
use App\Masters\ProductSubcategory;
use App\SiteUsers;

class OldsouqController extends Controller
{
    /**
     * Display a listing of the resource.

     * @return \Illuminate\Http\Response
     */
    public function index(){
       $row = Cms::where(['slug' => 'old-souq'])->first();
       if($row->image != 'NULL') {
        $oldsouqInfo['image'] = url('/')."/images/cms/".$row->image;
        } else {
            $oldsouqInfo['image'] = url('/')."/images/no-image.jpg";
        }
        $oldsouqInfo['name'] = $row->title;
        $oldsouqInfo['title'] = $row->title;
        $oldsouqInfo['content'] = $row->content;
        $sendData['oldsouqInfo'] = $oldsouqInfo;

       //-------------------Slider data -----------------------------
        $SliderImages = \App\Masters\SliderImages::where(['slug' => 'oldSouq'])
                           ->get();

        
        $SliderImagesArr = [];

            $i=0;
            foreach($SliderImages as $records) {
                if($records->image != 'NULL') {
                    $SliderImagesArr[$i]['image'] = url('/')."/images/cms/".$records->image;
                } else {
                    $SliderImagesArr[$i]['image'] = url('/')."/images/no-image.jpg";
                }
                
                $SliderImagesArr[$i]['image_title'] = $records->slug;
                $SliderImagesArr[$i]['redirect_to'] = $records->redirect_to;
                $SliderImagesArr[$i]['rdirect_id'] = $records->rdirect_id;
                
                $i++;
            }
        

        
        $sendData['SliderImages'] = $SliderImagesArr;
        
         //-----------------------------------------------------------
        //---------------
        $where['category_id'] = 4;
        //$queryObj = Shop::where($where)
        //->select('logo','_id','store_id','store_category_id','store_subcategory_id','name','email','phone','address','details')
        //->orderBy('name','asc');
        //$allRecords = $queryObj->get();


        $allRecords = Shop::where($where)
			->join('master_store as ms','ms.store_id','=','shops.store_id')
			->select('shops.logo','shops._id','shops.store_id','shops.store_category_id','shops.store_subcategory_id','shops.name','shops.email','shops.phone','shops.address','shops.details', 'ms.name as unit_id','shops.floorlevel','shops.latitude','shops.longitude')
			->orderBy('ms.name')
            ->get();
        //$sendData['oldsouqShopsList'] = $shopList;

        $getRecords = [];
        $i=0;
        foreach($allRecords as $records) {
            //$store_row = Store::where(['store_id' => $records->store_id])->first(); // added on 05/09/18
			$getRecords[$i]['store_id'] = $records->store_id;
			$getRecords[$i]['shop_id'] = $records->_id;
            $getRecords[$i]['store_category_id'] = $records->store_category_id;
            $getRecords[$i]['store_subcategory_id'] = $records->store_subcategory_id;
            $getRecords[$i]['unit_id'] = $records->unit_id;
			$getRecords[$i]['shop_name'] = $records->name;
            $getRecords[$i]['email'] = $records->email;
            $getRecords[$i]['phone'] = $records->phone;
            $getRecords[$i]['address'] = $records->address;
            $getRecords[$i]['details'] = $records->details;
            $getRecords[$i]['floorlevel'] = $records->floorlevel;
            $getRecords[$i]['latitude'] = $records->latitude;
            $getRecords[$i]['longitude'] = $records->longitude;

            if($records->logo) {
                $getRecords[$i]['store_image'] = url('/')."/images/shops/thumbnails/".$records->logo;
            } else {
                $getRecords[$i]['store_image'] = url('/')."/images/no-image.jpg";
            }
            $i++;
        }
        $sendData['oldsouqShops'] = $getRecords;


        $responseData = [
            'code' => 200,
            'content' => $sendData
        ];
        return response()->json($responseData);
	}
	
	
    /**
     * Display a details of the resource.

     * @return \Illuminate\Http\Response
     */
    /*
	public function oldSouqDetails(Request $request){
       $row = Cms::where(['slug' => 'old-souq'])->first();
       if($row->image != 'NULL') {
        $oldsouqInfo['image'] = url('/')."/images/cms/".$row->image;
        } else {
            $oldsouqInfo['image'] = url('/')."/images/no-image.jpg";
        }
        $oldsouqInfo['name'] = $row->title;
        $oldsouqInfo['title'] = $row->title;
        $oldsouqInfo['content'] = $row->content;
        $sendData['oldsouqInfo'] = $oldsouqInfo;

       //-------------------Slider data -----------------------------
        $SliderImages = \App\Masters\SliderImages::where(['slug' => 'oldSouq'])
                           ->get();

        
        $SliderImagesArr = [];

            $i=0;
            foreach($SliderImages as $records) {
                if($records->image != 'NULL') {
                    $SliderImagesArr[$i]['image'] = url('/')."/images/cms/".$records->image;
                } else {
                    $SliderImagesArr[$i]['image'] = url('/')."/images/no-image.jpg";
                }
                
                $SliderImagesArr[$i]['image_title'] = $records->slug;
                $SliderImagesArr[$i]['redirect_to'] = $records->redirect_to;
                $SliderImagesArr[$i]['rdirect_id'] = $records->rdirect_id;
                
                $i++;
            }
        

        $sendData['SliderImages'] = $SliderImagesArr;
        
         //-----------------------------------------------------------
        //---------------
        $where['category_id'] = 4;
		$where['_id'] = $request->shop_id;
        $queryObj = Shop::where($where)

        ->select('logo','_id','store_id','store_category_id','store_subcategory_id','name','email','phone','address','details')
        ->orderBy('name','asc');
        $allRecords = $queryObj->get();

        $getRecords = [];
        $i=0;
        foreach($allRecords as $records) {
            $store_row = Store::where(['store_id' => $records->store_id])->first(); // added on 05/09/18
			$getRecords[$i]['store_id'] = $records->store_id;
			$getRecords[$i]['shop_id'] = $records->_id;
            $getRecords[$i]['store_category_id'] = $records->store_category_id;
            $getRecords[$i]['store_subcategory_id'] = $records->store_subcategory_id;
            // $getRecords[$i]['store_name'] = $records->name; // // commented on 05/09/18
			// $getRecords[$i]['store_name'] = $store_row->name;
			$getRecords[$i]['unit_id'] = $store_row->name;
			$getRecords[$i]['shop_name'] = $records->name;
            $getRecords[$i]['email'] = $records->email;
            $getRecords[$i]['phone'] = $records->phone;
            $getRecords[$i]['address'] = $records->address;
            $getRecords[$i]['details'] = $records->details;

            if($records->logo) {
                $getRecords[$i]['store_image'] = url('/')."/images/shops/thumbnails/".$records->logo;
            } else {
                $getRecords[$i]['store_image'] = url('/')."/images/no-image.jpg";
            }
            $i++;
        }
        $sendData['oldsouqShops'] = $getRecords;


        $responseData = [
            'code' => 200,
            'content' => $sendData
        ];
        return response()->json($responseData);
	}	 */
	

    /**
     * Display a details of the resource.

     * @return \Illuminate\Http\Response
     */

	public function oldSouqDetails(Request $request){	
		$token = $request->token;
		// $type = $request->type;
		$type = "store";
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;

		// $store_id = $request->store_id;
		$shop_id = $request->shop_id;
		$record = Shop::where(['_id' => $shop_id])
					->select('logo','phone','email','website','details','name','address','floorlevel','latitude','longitude')
					->first();

		$getRecords = [];
		if(isset($record->logo)) {
			$getRecords['store_image'] = url('/')."/images/shops/".$record->logo;
		} else {
			$getRecords['store_image'] = url('/')."/images/no-image.jpg";
		}
		$getRecords['name'] = $record->name;
		$getRecords['phone'] = $record->phone;
		$getRecords['email'] = $record->email;
		$getRecords['website'] = $record->website;
		$getRecords['address'] = $record->address;
        $getRecords['details'] = $record->details;
        $getRecords['floorlevel'] = $record->floorlevel;
        $getRecords['latitude'] = $record->latitude;
        $getRecords['longitude'] = $record->longitude;
		$sendData['storedetail'] = $getRecords;

		// now fetching slide images
		$allRecords = ShopPics::where(['shop_id' => $shop_id])
					->select('image')->get();
		$getSlideImage = [];
		$i=0;
		$getSlideImage[$i++]['image'] = $getRecords['store_image']; // first image should be default store_image;
		foreach($allRecords as $records) {
			if($records->image) {
				$getSlideImage[$i]['image'] = url('/')."/images/shops/".$records->image;
			} else{
				$getSlideImage[$i]['image'] = url('/')."/images/no-image.jpg";
			}
			$i++;
		}
        $sendData['slideimages'] = $getSlideImage;

		//fetch related products as per brand;
		$allRecords = Product::where(['shop_id' => $shop_id])
					->select('_id','name','price','image')
		 			->paginate(3);

		$getRecords = [];
		$i=0;
		foreach($allRecords as $records) {
		 	if($records->image) {
				$getRecords[$i]['image'] = url('/')."/images/products/thumbnails/".$records->image;
			} else{
				$getRecords[$i]['image'] = url('/')."/images/no-image.jpg";
			}
		 	$getRecords[$i]['id'] = $records->_id;
		 	$getRecords[$i]['name'] = $records->name;
		 	$getRecords[$i]['price'] = $records->price;
		 	$i++;
		}
		$sendData['storeproducts'] = $getRecords;

		$countFavourite = Favourite::where(['type' => $type, 'type_id' => $shop_id, 'user_id' => $user_id])->count();
		$sendData['storedetail']['favourite'] = ($countFavourite)?true:false;
		$sendData['favourite'] = ($countFavourite)?true:false;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}


}
