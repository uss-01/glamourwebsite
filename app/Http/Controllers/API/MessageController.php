<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Message\Message;
use App\SiteUsers;

class MessageController extends Controller
{
    public function getAllMessages(Request $request) {
		$token = $request->token;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;
		
		$allMessages = Message::where(['to_user_id' => $user_id, 'to_del' => 0])
					   ->select('_id','type','subject','message', 'image', 'to_read','to_del','created_at')
					   ->orderBy('created_at', 'DESC')
					   ->paginate($this->paginate_total);
		
		$getRecords = [];
        $i=0;
		foreach($allMessages as $records) {
			$getRecords[$i]['id'] = $records->_id;
			$getRecords[$i]['type'] = $records->type;
			$getRecords[$i]['subject'] = $records->subject;
			$getRecords[$i]['message'] = $records->message;//substr($records->message,0,50);
			$getRecords[$i]['image'] = $records->image;
			$getRecords[$i]['to_read'] = $records->to_read;
			$getRecords[$i]['to_del'] = $records->to_del;
			$getRecords[$i]['date'] = Carbon::parse($records->created_at)->format('Y-m-d H:i:s');
           $i++;
		}
		
		$sendData['allMessages'] = $getRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}
	
	public function getMessage(Request $request){
		$token = $request->token;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;
		$message_id = $request->message_id;
		$getMessage = Message::where(['_id' => $message_id,'to_user_id' => $user_id, 'to_del' => 0])
					   ->select('_id','type','subject','message','to_read','created_at')
					   ->first();
		$sendData['maildetail'] = $getMessage;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}
	
	
	public function deleteMessage(Request $request){
		$token = $request->token;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;
		$message_id = $request->message_id;
		$message = Message::find($message_id);
        $updateMessageFlag = $message->update([
            'to_del' => 1
        ]);
		
		if($updateMessageFlag){
			$respMsg = "Message Deleted.";
		}else{
			$respMsg = "Could not deleted";
		}

		$responseData = [
			'code' => 200,
			'message' => $respMsg
		];

		return response()->json($responseData);
	}
	
}
