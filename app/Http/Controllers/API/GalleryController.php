<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gallery\Album;
use App\Gallery\Photo;
use JWTAuth;
use JWTAuthException;

class GalleryController extends Controller
{
    public function getAllAlbum(Request $request) {
		
		$allRecords = Album::orderBy('name','asc')->select('_id as id','name','image')->paginate($this->paginate_total);
		$getRecords = [];
        $i=0;
		foreach($allRecords as $records) {
			$count = Photo::where('album_id','=',$records->id)->count();
			if($records->image != 'NULL') {
				$getRecords[$i]['image'] = url('/')."/images/galleries/albums/".$records->image;
			}
			$getRecords[$i]['id'] = $records->id;
			$getRecords[$i]['name'] = $records->name;
			$getRecords[$i]['photo_count'] = $count;
           $i++;
		}
				
		$sendData['galleryAlbums'] = $getRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

	public function getAlbumPhotos(Request $request) {
		$album_id = $request->album_id;
		$allRecords = Photo::where('album_id','=',$album_id)->select('_id as id','title','image')->get();
		$getRecords = [];
        $i=0;
		foreach($allRecords as $records) {
			if($records->image != 'NULL') {
				$getRecords[$i]['image'] = url('/')."/images/galleries/photos/".$records->image;
			}
			$getRecords[$i]['id'] = $records->id;
			$getRecords[$i]['title'] = $records->title;
           $i++;
		}
				
		$sendData['albumPhotos'] = $getRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

}
