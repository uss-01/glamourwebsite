<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    function search(Request $request){
    	$q = $request->q;
    	$sendData['searchQuery'] = $q;
		$sendData['products'] = productSearch($q);
		$sendData['brands'] = brandSearch($q);
		$sendData['movies'] = movieSearch($q);
		$sendData['shops'] = shopSearch($q);

    $responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
    }
}
