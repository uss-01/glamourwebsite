<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InstaGramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tag = 'tag'; // tag for which ou want images 
        $results_array = $this->scrape_instagram_hash($tag);
		$limit = 60; // provide the limit thats important because one page only give some images.
		$media_edges_count = count($results_array['entry_data']['TagPage'][0]['graphql']['hashtag']['edge_hashtag_to_media']['edges']);
		if($media_edges_count < $limit){
			$limit = $media_edges_count;
		}
		$image_array= array(); // array to store images.
		for ($i=0; $i < $limit; $i++) { 
			//previous code to get images from json     
			//$latest_array = $results_array['entry_data']['TagPage'][0]['tag']['media']['nodes'][$i];  
			//new code to get images from json  
			$latest_array = $results_array['entry_data']['TagPage'][0]['graphql']['hashtag']['edge_hashtag_to_media']['edges'][$i]['node']['thumbnail_resources'];
			//$image_data  = '<img src="'.$latest_array['thumbnail_src'].'">'; // thumbnail and same sizes 
			//$image_data  = '<img src="'.$latest_array['display_src'].'">'; actual image and different sizes 
			array_push($image_array, $latest_array);
			//$insta_array = json_encode($image_array);

		}
		$oneDimensionalArray = call_user_func_array('array_merge', $image_array);
		$instaImage = array();
		foreach($oneDimensionalArray as $oneDimensional){
			if($oneDimensional['config_width'] == '640'){
				$instaImage[] = $oneDimensional;
			}
		}
		$sendData['getInstaGramMedia'] = array_reverse($instaImage);
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
    }

	function scrape_instagram_hash($tag) {
			$insta_source = file_get_contents('https://www.instagram.com/explore/tags/'.$tag.'/'); // instagrame tag url
			$shards = explode('window._sharedData = ', $insta_source);
			$insta_json = explode(';</script>', $shards[1]); 
			$insta_array = json_decode($insta_json[0], TRUE);
			return $insta_array; // this return a lot things print it and see what else you need
	}
	
	public function getInstagramImages(Request $request){
		
		$token = $request->token;
		$instaData = array(); 
		$access_token = "3270781565.23b7cf7.b77e33eb66be49d49533a1fec116";
		$photo_count = 20;
		$json_link = "https://api.instagram.com/v1/users/self/media/recent/?";
		if(!empty($request->imageCount)){
            $json_link .="access_token={$access_token}&max_id={$request->imageCount}";
		}else{
		    $json_link .="access_token={$access_token}&count={$photo_count}";
		}
		$json = file_get_contents($json_link);
		$obj = json_decode(preg_replace('/("\w+"):(\d+)/', '\\1:"\\2"', $json), true);
		
		$nextImagesCount = $obj['pagination']['next_max_id'];
		foreach($obj['data'] as $key => $post){
 			if(!empty($post['videos'])){
			   $instaData[$key]['imageUrl'] = str_replace("http://", "https://", $post['images']['low_resolution']['url']);
		       $instaData[$key]['videoUrl'] = str_replace("http://", "https://", $post['videos']['standard_resolution']['url']);
			}else{
			   $instaData[$key]['imageUrl'] = str_replace("http://", "https://", $post['images']['low_resolution']['url']);
			}
		}
		
		$sendData['getInstaGramMedia'] = $instaData;
		if(!empty($nextImagesCount)){
            $sendData['nextImagesCount'] = $nextImagesCount;
		}
		$responseData = array(
			'code' => 200,
			'content' => $sendData
		);
		return response()->json($responseData);
	}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
