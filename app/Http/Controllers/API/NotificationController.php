<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{

    public function __construct(Request $request){

        $page = isset($request->page) ? $request->page : 1;
        $this->limit = isset($request->recordPerPage) ? $request->recordPerPage : 5000;
        $this->offset = ($page - 1) * $this->limit;
    }

    public function offerNearByMe(Request $request){
        $beaconId       =   trim($request->beaconId);
        $deviceToken    =   trim($request->deviceToken);
        $currentOffer   =   \App\Offer::where(['s.beacon_id' => $beaconId])
        ->join('shops as s','offers.shop_id','=','s._id')
         ->select('offers._id as offerId', 'offers.title as offerTitle', 'offers.sub_title as offerSubTitle', 'offers.image as offerImage', 'offers.start_date', 'offers.end_date', 's._id as shopId','s.name as shopName')
        ->orderBy('offers._id','desc')
        //->toSql();
        ->first();
        //->get();
        $pushData['message']    =   $currentOffer['offerTitle'];
        $pushData['image']  =   url('/')."/images/offers/".$currentOffer['offerImage'];
        $pushData['redirect_to']    =   'shop';
        $pushData['redirect_id']    =   $currentOffer['shopId'];

        $pushData['status']     =   PushNotificationIos($deviceToken, $pushData);

        $pushData['shopId']     =   $currentOffer['shopId'];
        $pushData['shopName']   =   $currentOffer['shopName'];

        
        return response()->json($pushData);

    }



    public function notifiOffer(Request $request){
        $result = array();
        //$beaconId   =   trim($request->beaconId);
        $deviceToken    =   trim($request->deviceToken);
        $pushData['image']  = url('/')."/images/offers/1522764154_buy-1-get-1-free.jpg";
        $pushData['message'] = 'Buy 1 Get 1 Free';
        $pushData['redirect_to']='shop';
        $pushData['redirect_id']='191';
        $result['status']   =   PushNotificationIos($deviceToken, $pushData);
        $result['image']   =   $pushData['image'];
        return response()->json($result);

    }

    public function androidNotifyOffer(Request $request){
        //$beaconId = trim($request->beaconId);
        $deviceToken = trim($request->deviceToken);
        $pushData['image']  = $request->imageUrl;
        $pushData['title'] = $request->title;
        $pushData['message'] = $request->message;
        $pushData['redirect_to'] = $request->redirect_to;
        $pushData['redirect_id'] = $request->redirect_id;
        //$pushData['status'] = $this->pushMessageNew('AIzaSyDqsRf-uFOBbFvuNwxPaS-jHwlBgwRQkVw',$deviceToken,$pushData);
        $pushData['status'] = $this->pushMessageNew('AIzaSyA-XHkiEROl9rGoPY3gvGRU6igyetusj3E',$deviceToken,$pushData);                                       
        $pushData['image'] = $pushData['image'];
        return response()->json($pushData);
    }

    public function androidOfferNearByMe(Request $request){
        $beaconId = trim($request->beaconId);
        $deviceToken = trim($request->deviceToken);
        $currentOffer = \App\Offer::where(['s.beacon_id' => $beaconId])
        ->join('shops as s','offers.shop_id','=','s._id')
         ->select('offers._id as offerId', 'offers.title as offerTitle', 'offers.sub_title as offerSubTitle', 'offers.details as offerDetails', 'offers.image as offerImage', 'offers.start_date', 'offers.end_date', 's._id as shopId','s.name as shopName')
        ->orderBy('offers._id','desc')
        ->first();
        $pushData['title'] = $currentOffer['offerTitle'];
        $pushData['message'] = $currentOffer['offerDetails'];
        $pushData['image'] = url('/')."/images/offers/".$currentOffer['offerImage'];
        $pushData['redirect_to'] = 'shop';
        $pushData['redirect_id'] = $currentOffer['shopId'];
        //$pushData['status'] = $this->pushMessageNew('AIzaSyDqsRf-uFOBbFvuNwxPaS-jHwlBgwRQkVw',$deviceToken,$pushData);
        $pushData['status'] = $this->pushMessageNew('AIzaSyA-XHkiEROl9rGoPY3gvGRU6igyetusj3E',$deviceToken,$pushData);
        $pushData['shopId'] = $currentOffer['shopId'];
        $pushData['shopName'] = $currentOffer['shopName'];
        return response()->json($pushData);
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pushMessageNew($api_access_key,$device_id,$pushData) {
        //$api_access_key = $api_access_key;
        $msg = array(
          'body' => $pushData['message'],
          'title' => $pushData['title'],
          'icon' => 'myicon',/*Default Icon*/
          'sound' => 'mySound'/*Default sound*/
        );
        $data=array(
          'redirect_to' => $pushData['redirect_to'],
          'redirect_id' => $pushData['redirect_id'],
          'vibrate' => 1,
        );
        $fields = array
        (
          'to' => $device_id,
          'mutable-content' => 1, 
          'notification' => $msg,
          'data' => $data,
        );
        $headers = array
        (
          'Authorization: key=' . $api_access_key,
          'Content-Type: application/json'
        );
        #Send Reponse To FireBase Server    
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        //Now close the connection
        curl_close($ch);
        //and return the result 
        return $result;
      }
}
