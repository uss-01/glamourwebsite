<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Masters\Brand;
use App\Shop\MovieDetail;
use App\Masters\ProductCategory;
use App\Masters\ProductSubcategory;
use App\Masters\Category;
use App\Masters\Store;
use App\Shop\ShopBrands;
use App\Shop\Shop;
use App\Shop\ShopPics;
use App\Product\Product;
use App\Product\ProductPics;
use App\Orders\Orders;
use App\Orderdetail\Orderdetail;
use App\Favourite;
use App\Product\Usercart;
use App\User;
use App\Tenant;
use App\Tenantadmin;
use App\TenantComplaints;
use App\Leasing;
use App\Leasing\LeasingEnquiry;
use App\SiteUsers;
use App\SiteUsersAddress;
class StoreController extends Controller
{
    public function __construct(Request $request){
		$page = isset($request->page) ? $request->page : 1;
		$this->limit = isset($request->recordPerPage) ? $request->recordPerPage : 5000;
		$this->offset = ($page - 1) * $this->limit;
	}
	
	public function getBrandDetail($brand_id){
		$brand = Brand::where(['brand_id' => $brand_id]) // category_id (1 = Shop; 2 = Dinning; 3 = Entertainment)
		->first();
		$brandDetail['brand_id']=$brand->brand_id;
		$brandDetail['name']=$brand->name;
		$brandDetail['description']=$brand->description;
		if($brand->image != 'NULL') {
			$brandDetail['image'] = url('/')."/images/brands/".$brand->image;
		} else {
			$brandDetail['image'] = url('/')."/images/no-image.jpg";
		}
		return $brandDetail;
	}

	public function getShopsByBrand(Request $request){
		if($request->has('brand_id')) {
			$brand_id = $request->brand_id;
			$shopList = Shop::where(['shops.category_id' => 1, 'sb.brand_id'=>$brand_id])
			->join('shop_brands as sb','shops._id','=','sb.shop_id')
			->join('master_store as ms','ms.store_id','=','shops.store_id')
			->select('shops._id as id','shops.name', 'shops.phone', 'shops.email', 'shops.website', 'shops.address', 'shops.details','shops.logo', 'ms.name as location')
			->orderBy('name','asc')
			->get();
		$getRecords = [];
		$i=0;
		foreach($shopList as $records) {
			$getRecords[$i]['shop_id'] = $records->id;
			$getRecords[$i]['shop_name'] = $records->name;
			$getRecords[$i]['shop_phone'] = $records->phone;
			$getRecords[$i]['shop_email'] = $records->email;
			$getRecords[$i]['shop_website'] = $records->website;
			$getRecords[$i]['location'] = $records->location;
			$getRecords[$i]['shop_address'] = $records->address;
			$getRecords[$i]['shop_detail'] = $records->details;
			if($records->logo) {
				$getRecords[$i]['shop_image'] = url('/')."/images/shops/thumbnails/".$records->logo;
			} else {
				$getRecords[$i]['shop_image'] = url('/')."/images/no-image.jpg";
			}
			$i++;
		}
		$sendData['brandDetail'] = $this->getBrandDetail($brand_id);
		$sendData['shops'] = $getRecords;
		$responseData = [
			'code' => 200,
			'content'=>$sendData
		];
		return response()->json($responseData);

		}
	}

    public function getAllStore(Request $request) {
		$where['shops.category_id'] = 1;
		$orwhere['shops.category_id'] = 2;
		if($request->has('store_category_id')) {
			$where['shops.store_category_id'] = $request->store_category_id;
			$orwhere['shops.category_id'] = 0;
		}
		if($request->has('store_subcategory_id')) {
			$where['shops.store_subcategory_id'] = $request->store_subcategory_id;
			$orwhere['shops.category_id'] = 0;
		}
		$queryObj = Shop::where($where)
					->orwhere($orwhere)
					->join('master_category as c', 'c.category_id', '=', 'shops.category_id')
					->join('master_store as s', 's.store_id', '=', 'shops.store_id')
					->join('shop_brands as sb', 'sb.shop_id', '=', 'shops._id')
					->join('master_brand as mb', 'mb.brand_id', '=', 'sb.brand_id')
					->select('shops.logo','shops._id as store_id', 'shops.store_id as location_id', 'shops.store_category_id','shops.store_subcategory_id','shops.name','shops.email','shops.phone','shops.website','shops.address')
					->with('category')
					->with('store')
					->orderBy('shops.created_at','desc')
					->groupBy('shops._id');
		$allRecords = $queryObj->skip($this->offset)->take($this->limit)->get();
		$getRecords = [];
		$i=0;
		foreach($allRecords as $records) {
			$getRecords[$i]['store_id'] = $records->store_id;
			$getRecords[$i]['location_id'] = $records->location_id;
			$getRecords[$i]['store_category_id'] = $records->store_category_id;
			$getRecords[$i]['store_subcategory_id'] = $records->store_subcategory_id;
			$getRecords[$i]['store_name'] = $records->name;
			$getRecords[$i]['email'] = $records->email;
			$getRecords[$i]['phone'] = $records->phone;
			$getRecords[$i]['website'] = $records->website;
			$getRecords[$i]['address'] = $records->address;
			if($records->logo) {
				$getRecords[$i]['store_image'] = url('/')."/images/shops/thumbnails/".$records->logo;
			} else {
				$getRecords[$i]['store_image'] = url('/')."/images/no-image.jpg";
			}
			$i++;
		}
		$sendData['allStores'] = $getRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

	public function getStoreDetail(Request $request){
		$token = $request->token;
		$type = $request->type;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;

		$store_id = $request->store_id;
		$record = Shop::where(['_id' => $store_id])
					->select('logo','phone','email','website','details','name','address','floorlevel','latitude','longitude')
					->first();

		$getRecords = [];
		if(isset($record->logo)) {
			$getRecords['store_image'] = url('/')."/images/shops/".$record->logo;
		} else {
			$getRecords['store_image'] = url('/')."/images/no-image.jpg";
		}
		$getRecords['name'] = $record->name;
		$getRecords['phone'] = $record->phone;
		$getRecords['email'] = $record->email;
		$getRecords['website'] = $record->website;
		$getRecords['address'] = $record->address;
		$getRecords['details'] = $record->details;
		$getRecords['floorlevel'] = $record->floorlevel;
		$getRecords['latitude'] = $record->latitude;
		$getRecords['longitude'] = $record->longitude;
		// get product count
		$totProds = Product::where(['shop_id' => $store_id])
					->select('_id')
		 			->count();
		$getRecords['totalProducts'] = $totProds;			
		$sendData['storedetail'] = $getRecords;
		// now fetching slide images
		$allRecords = ShopPics::where(['shop_id' => $store_id])
					->select('image')->get();
		$getSlideImage = [];
		$i=0;
		foreach($allRecords as $records) {
			if($records->image) {
				$getSlideImage[$i]['image'] = url('/')."/images/shops/".$records->image;
			} else{
				$getSlideImage[$i]['image'] = url('/')."/images/no-image.jpg";
			}
			$i++;
		}
        $sendData['slideimages'] = $getSlideImage;

		//fetch related products as per brand;
		$allRecords = Product::where(['shop_id' => $store_id])
					->select('_id','name','price','image','discounted_price')
                    ->get();   
		$getRecords = [];
		$i=0;
		foreach($allRecords as $records) {
		 	if($records->image) {
				$getRecords[$i]['image'] = url('/')."/images/products/thumbnails/".$records->image;
			} else{
				$getRecords[$i]['image'] = url('/')."/images/no-image.jpg";
			}
		 	$getRecords[$i]['id'] = $records->_id;
		 	$getRecords[$i]['name'] = $records->name;
			$getRecords[$i]['price'] = $records->price;
			$getRecords[$i]['discountedPrice'] = $records->discounted_price; 
		 	$i++;
		}
		$sendData['storeproducts'] = $getRecords;
		$countFavourite = Favourite::where(['type' => $type, 'type_id' => $store_id, 'user_id' => $user_id])->count();
		$sendData['storedetail']['favourite'] = ($countFavourite)?true:false;
		$sendData['favourite'] = ($countFavourite)?true:false;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

	public function getAllProductCategory(){
		$allRecords = ProductCategory::where(['type' => 'Product'])->orderBy('name','asc')->select('_id as id','name')->get();
		$sendData['allProductCategories'] = $allRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

	public function getAllProductSubCategory(Request $request){
		$product_category_id = $request->product_category_id;
		$allRecords = ProductSubcategory::where(['product_category_id' => $product_category_id])
						->orderBy('name','asc')
						->select('_id as id','name')
						->get();
		$sendData['allProductSubCategories'] = $allRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

    public function getAllStoreCategory(){

		$allRecords = ProductCategory::where(['type' => 'Store'])->orderBy('name','asc')->select('_id as id','name')->get();
		$sendData['allStoreCategories'] = $allRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

	public function getStoreSubCategory(Request $request){
		$store_category_id = $request->store_category_id;
		$allRecords = ProductSubcategory::where(['product_category_id' => $store_category_id,'type' => 'Store'])
						->orderBy('name','asc')
						->select('_id as id','name')
						->get();
		$sendData['storeSubCategories'] = $allRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

	public function getmallDirectory(Request $request){
		$store_category_id = $request->has('store_category_id');
		$store_subcategory_id = $request->has('store_subcategory_id');

		$allCategories = ProductCategory::where(['type' => 'Store'])->orderBy('name','asc')->select('_id as id','name')->get();
		$getCategoryRecords['0']['id'] = ' ';
		$getCategoryRecords['0']['name'] = 'Main Category';
		$i=1;
		foreach($allCategories as $records) {
			$getCategoryRecords[$i]['id'] = $records->id;
			$getCategoryRecords[$i]['name'] = $records->name;
			$i++;
		}
		$sendData['allStoreCategories'] = $getCategoryRecords;
		
		if(!empty($store_category_id)){
			$whereSub = array("product_subcategories.product_category_id" => $request->store_category_id, "product_subcategories.type" => "Store");
		}else{
			$whereSub = array("product_subcategories.type" => "Store");
		}
		$allSubCategories = ProductSubcategory::where($whereSub)->orderBy('name','asc')->select('_id as id','name')->get();
		$getSubcategoryRecords['0']['id'] = ' ';
		$getSubcategoryRecords['0']['name'] = 'Sub Category';
		$j=1;
		foreach($allSubCategories as $records) {
			$getSubcategoryRecords[$j]['id'] = $records->id;
			$getSubcategoryRecords[$j]['name'] = $records->name;
			$j++;
		}
		$sendData['storeSubCategories'] = $getSubcategoryRecords;						
		
		
		if(!empty($store_category_id) && empty($store_subcategory_id)) {
			$where['shops.store_category_id'] = $request->store_category_id;
			$orwhere['shops.category_id'] = 0;
		}else if(empty($store_category_id) && !empty($store_subcategory_id)) {
			$where['shops.store_subcategory_id'] = $request->store_subcategory_id;
			$orwhere['shops.category_id'] = 0;
		}else if(!empty($store_category_id) && !empty($store_subcategory_id)){
            $where = array("shops.store_category_id" => $request->store_category_id, "shops.store_subcategory_id" => $request->store_subcategory_id);   
		    $orwhere['shops.category_id'] = 0;
		}else{
			$where['shops.category_id'] = 1;
		    $orwhere['shops.category_id'] = 2;
		}
		$queryObj = Shop::where($where)
							->orwhere($orwhere)
							->join('master_category as c', 'c.category_id', '=', 'shops.category_id')
							->join('master_store as s', 's.store_id', '=', 'shops.store_id')
							->join('shop_brands as sb', 'sb.shop_id', '=', 'shops._id')
							->join('master_brand as mb', 'mb.brand_id', '=', 'sb.brand_id')
							->select('shops.logo','shops._id as store_id', 'shops.store_id as location_id', 'shops.store_category_id','shops.store_subcategory_id','shops.name','shops.email','shops.phone','shops.website','shops.address')
							->with('category')
							->with('store')
							->orderBy('shops.name','asc')
							->groupBy('shops._id');
		$allRecords = $queryObj->skip($this->offset)->take($this->limit)->get();
	
		$getRecords = []; $getfirst_char = [];
		$i=0;
		foreach($allRecords as $records) {
			$getRecords[$i]['store_id'] = $records->store_id;
			$getRecords[$i]['location_id'] = $records->location_id;
			$getRecords[$i]['store_category_id'] = $records->store_category_id;
			$getRecords[$i]['store_subcategory_id'] = $records->store_subcategory_id;
			$getRecords[$i]['store_name'] = $records->name;
			$getRecords[$i]['email'] = $records->email;
			$getRecords[$i]['phone'] = $records->phone;
			$getRecords[$i]['website'] = $records->website;
			$getRecords[$i]['address'] = $records->address;
			if($records->logo) {
				$getRecords[$i]['store_image'] = url('/')."/images/shops/thumbnails/".$records->logo;
			} else {
				$getRecords[$i]['store_image'] = url('/')."/images/no-image.jpg";
			}
			$i++;
		}
		$sendData['allStores'] = $getRecords;
		
		foreach($allRecords as $record) {
			$getfirst_char[] = strtolower(substr($record->name, 0, 1));
		}
		$sendData['allFirstChar'] = array_unique($getfirst_char);

		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

	public function getAllProduct(Request $request) {

		$where['products.add_to_cart'] = 1;
		if($request->has('product_page')) {
				$where['products.add_to_cart'] = 0;
		}
		if($request->has('category_id')) {
			$category_id = $request->category_id;
			$where['products.product_category_id'] = $category_id;
		}
		if($request->has('store_id')) {
			$store_id = $request->store_id;
			$where['products.shop_id'] = $store_id;
		}

		$allRecords = Product::where($where)
		            ->join('shops as s', 's._id', '=', 'products.shop_id')
					->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
					->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
					->select('products._id','.products.name','products.image','products.price','products.discounted_price')
					->with('shop')
					->with('product_category')
					->with('product_subcategory')
					//->inRandomOrder()
					->orderBy('products.name','asc')
					->groupBy('products._id')
					->skip($this->offset)->take($this->limit)->get();

		$getRecords = [];
		$i=0;
		foreach($allRecords as $records) {
			if($records->image) {
				$getRecords[$i]['image'] = url('/')."/images/products/thumbnails/".$records->image;
			} else {
				$getRecords[$i]['image'] = url('/')."/images/no-image.jpg";
			}
			$getRecords[$i]['id'] = $records->_id;
			$getRecords[$i]['name'] = $records->name;
			$getRecords[$i]['price'] = $records->price;
			$getRecords[$i]['discountedPrice'] = $records->discounted_price;
			$i++;
		}
		$sendData['allProduct'] = $getRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);

	}

	public function getProductDetail(Request $request) {
		$product_id = $request->product_id;
		$getRecord = Product::where(['products._id' => $product_id])
					->join('shops','shops._id','=','products.shop_id')
					->join('master_store as ms','ms.store_id','=','shops.store_id')
					->select('products._id as id','products.name','products.shop_id','products.details','products.image','products.price','products.discounted_price','products.product_quantity','products.attribute','products.add_to_cart','ms.name as location')
					->first();

		if(isset($getRecord->image)) {
			$getRecord['image'] = url('/')."/images/products/".$getRecord->image;
		} else {
			$getRecord['image'] = url('/')."/images/no-image.jpg";
		}
		$attributeArray = [];
		if(isset($getRecord->attribute)) {
			$attributeArray = json_decode($getRecord->attribute);
		}
		$actualAttribue = [];
		foreach($attributeArray as $item){
			if($item->name != null && $item->value != null) {
				$actualAttribue[] = $item;
			}
		}

		$getRecord['attribute'] = $actualAttribue;

		$token = $request->token;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;
		$usercartData = Usercart::where('user_id', '=', $user_id)
		            ->where('product_id', '=', $product_id)
					->select('users_cart._id')->first();
        if(!empty($usercartData->_id)){
			$getRecord['already_in_cart'] = 'Yes';
		}else{
			$getRecord['already_in_cart'] = 'No';
		}
		$sendData['productDetail'] = $getRecord;
		$getSlideImage = [];
		$i=0;
		$getSlideImage[$i++]['image'] = $getRecord['image']; // first image should be default store_image;

		// fetching related multiple image(s);
		$getRecord = ProductPics::where(['product_id' => $product_id])->select('image')->get();

		foreach($getRecord as $records) {
			if($records->image) {
				$getSlideImage[$i]['image'] = url('/')."/images/products/".$records->image;
			} else {
				$getSlideImage[$i]['image'] = url('/')."/images/no-image.jpg";
			}
			$i++;
		}

		$sendData['slideimages'] = $getSlideImage;
		$sendData['youMayLike'] = productsYouMayLike($count=6, $curr_product_id=$product_id);
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

	public function getAllBrand(Request $request) {

		$allRecords = Brand::where(['master_brand.category_id' => 1])
					->inRandomOrder()
					->skip($this->offset)->take($this->limit)->get();

		$getRecords = [];
		$i=0;
		$brandName=array();
		foreach($allRecords as $records) {
			

			if($records->brand_id == '263'){
			
			}else{
			if(in_array($records->name, $brandName)){ continue;}
			if($records->image != 'NULL') {
				$getRecords[$i]['image'] = url('/')."/images/brands/".$records->image;
			} else {
				$getRecords[$i]['image'] = url('/')."/images/no-image.jpg";
			}
			$getRecords[$i]['id'] = $records->brand_id;
			$getRecords[$i]['name'] = $records->name;
			$brandName[]=$records->name;
			$i++;
			}
			
		}
		$sendData['allBrands'] = $getRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

	public function getAllDinning(Request $request) {

		$allRecords = Brand::where(['category_id' => 2])
					->select('brand_id as id','name','description','image as brand_image')
					->orderBy('name','asc')
					->paginate($this->paginate_total);

		$getRecords = [];
		$i=0;
		foreach($allRecords as $records) {
			if($records->brand_image) {
				$getRecords[$i]['dinning_image'] = url('/')."/images/brands/".$records->brand_image;
			} else {
				$getRecords[$i]['dinning_image'] = url('/')."/images/no-image.jpg";
			}
			$getRecords[$i]['id'] = $records->id;
			$getRecords[$i]['name'] = $records->name;
			$i++;
		}
		$sendData['allDinning'] = $getRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

	public function getAllDinningStore(Request $request) {
		$brand_id = $request->dinning_id;

		$sendData['dinningStoredetail'] = Brand::where(['brand_id' => $brand_id])
					->select('brand_id as id','name','description','image as brand_image')
					->first();
		$sendData['dinningStoredetail']['brand_image'] = url('/')."/images/brands/".$sendData['dinningStoredetail']->brand_image;
		$allRecords = Shop::where(['shops.category_id' => 2])
		            ->where('sb.brand_id','=',$brand_id)
		            ->join('shop_brands as sb','sb.shop_id','=','shops._id')
		            ->join('master_store as ms','ms.store_id','=','shops.store_id')
					->select('shops._id as shop_id','shops.name','ms.name as store_location','shops.logo as shop_image')
					->orderBy('shops.name','asc')
					->paginate($this->paginate_total);

		$getRecords = [];
		$i=0;
		foreach($allRecords as $records) {
			if($records->shop_image) {
				$getRecords[$i]['dinning_image'] = url('/')."/images/shops/".$records->shop_image;
			} else {
				$getRecords[$i]['dinning_image'] = url('/')."/images/no-image.jpg";
			}
			$getRecords[$i]['shop_id'] = $records->shop_id;
			$getRecords[$i]['store_location'] = $records->store_location;
			$getRecords[$i]['name'] = $records->name;
			$i++;
		}
		$sendData['allDinningStore'] = $getRecords;

		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

	public function getAllEntertainment(Request $request) {

		$allRecords = Brand::where(['category_id' => 3])
					->join('shop_brands as sb','sb.brand_id','=','master_brand.brand_id')
					->select('master_brand.brand_id as id','master_brand.name','master_brand.image as brand_image','sb.shop_id')
					->orderBy('name','asc')
					->groupBy('master_brand.brand_id')
					->get();
        $getRecords = [];
		$i=0;
		foreach($allRecords as $records) {
			$count = Shop::where(['shops.category_id' => 3])
					->where('sb.brand_id','=',$records->id)
					->join('shop_brands as sb','sb.shop_id','=','shops._id')->count();
			if($records->brand_image) {
				$getRecords[$i]['entertainment_image'] = url('/')."/images/brands/".$records->brand_image;
			} else {
				$getRecords[$i]['entertainment_image'] = url('/')."/images/no-image.jpg";
			}
            $getRecords[$i]['id'] = $records->id;
			$getRecords[$i]['name'] = $records->name;
			$getRecords[$i]['shop_id'] = $records->shop_id;
			$getRecords[$i]['no_of_shows'] = $count;
			$i++;
		}
		$sendData['allEntertainment'] = $getRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

	public function getCinemaDetail(Request $request) {
		$ent_id = $request->entertainment_id;

		$records = Brand::where(['brand_id' => $ent_id])
					->select('name','description','image as brand_image')
					->first();
		$getRecords = [];
		$i=0;

		if($records->brand_image) {
			$getRecords['entertainment_image'] = url('/')."/images/brands/".$records->brand_image;
		} else {
			$getRecords['entertainment_image'] = url('/')."/images/no-image.jpg";
		}
		$getRecords['cinema_name'] = $records->name;
		$getRecords['description'] = $records->description;

		$getMovies = $this->getAllMovies($ent_id);

		$sendData['cinemadetail'] = $getRecords;
		$sendData['allmovies'] = $getMovies;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

	public function getAllMovies($ent_id) {
		$brand_id = $ent_id;
		$allRecords = Shop::where(['shops.category_id' => 3])
		            ->where('sb.brand_id','=',$brand_id)
		            ->join('shop_brands as sb','sb.shop_id','=','shops._id')
		            ->join('master_brand as mb','mb.brand_id','=','sb.brand_id')
		            ->join('master_store as ms','ms.store_id','=','shops.store_id')
					->join('movie_detail as md','md.shop_id','=','shops._id')
					->select('shops._id as shop_id','shops.name','ms.name as store_location','shops.logo as shop_image','shops.upcoming','mb.name as entertainment_type','md.subtitle','md.duration','md.description as details','md.2d_showtime as showtime_2d','md.3d_showtime','md.cast')
					->orderBy('shops.name','asc')
					->paginate($this->paginate_total);

		$getRecords = [];
		$i=0;
		foreach($allRecords as $records) {
			if($records->shop_image) {
				$getRecords[$i]['show_image'] = url('/')."/images/shops/".$records->shop_image;
			} else {
				$getRecords[$i]['show_image'] = url('/')."/images/no-image.jpg";
			}
			$getRecords[$i]['shop_id'] = $records->shop_id;
			$getRecords[$i]['store_location'] = $records->store_location;
			$getRecords[$i]['name'] = $records->name;
			$getRecords[$i]['upcoming'] = $records->upcoming;

			$i++;
		}
		return $getRecords;

	}

	public function getMovieDetail(Request $request) {
		$shop_id = $request->shop_id;

		$allRecords = Shop::where(['shops._id' => $shop_id])
					->join('movie_detail as md','md.shop_id','=','shops._id')
					->select('shops._id as shop_id','shops.name','shops.logo as shop_image','md.subtitle','md.duration','md.description as details','md.2d_showtime as showtime_2d','md.3d_showtime as showtime_3d','md.cast')
					->first();
		$allRecords['shop_image'] = url('/')."/images/shops/".$allRecords->shop_image;
		$sendData['moviedetail'] = $allRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}
	
	public function getEntertainmentStoreDetail(Request $request){
       
		$token = $request->token;
		$type = $request->type;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;
        $brand_id = $request->brand_id;
		
		$shopBrand = ShopBrands::where(['shop_brands.brand_id' => $brand_id])
		             ->select('shop_id')
					 ->orderBy('_id', 'desc')
					 ->first(); 
					 
		if(!empty($shopBrand->shop_id)){
		$store_id = $shopBrand->shop_id;
		
		$recordData = Shop::where(['shops._id' => $store_id, 'shop_brands.brand_id' => $brand_id])
		 			->join('shop_brands','shops._id','=','shop_brands.shop_id')
		 			->join('master_brand','master_brand.brand_id','=','shop_brands.brand_id')
		 			->select('image as brand_image','shops.logo','shops._id as shop_id','shops.phone','shops.email','shops.website','shops.details','shops.name','shops.address')
		 			->get();

		$getRecords = [];
		foreach($recordData as $record){
		if(isset($record->logo)){
			$getRecords['store_image'] = url('/')."/images/shops/".$record->logo;
		}else{
			$getRecords['store_image'] = url('/')."/images/no-image.jpg";
		}
		$getRecords['name'] = $record->name;
		$getRecords['phone'] = $record->phone;
		$getRecords['email'] = $record->email;
		$getRecords['website'] = $record->website;
		$getRecords['address'] = $record->address;
		$getRecords['details'] = $record->details;
		// get product count
		$totProds = Product::where(['shop_id' => $store_id])->select('_id')->count();
		$getRecords['totalProducts'] = $totProds;	
		}
		$sendData['storedetail'] = $getRecords;

		// now fetching slide images
		$allRecords = ShopPics::where(['shop_id' => $store_id])->select('image')->get();
		$getSlideImage = [];
		$i=0;
		foreach($allRecords as $records){ 
			if($records->image) {
				$getSlideImage[$i]['image'] = url('/')."/images/shops/".$records->image;
			} else{
				$getSlideImage[$i]['image'] = url('/')."/images/no-image.jpg";
			}
			$i++;
		}
        $sendData['slideimages'] = $getSlideImage;

		//fetch related products as per brand;
		$allRecords = Product::where(['shop_id' => $store_id])->select('_id','name','price','image')->get();   
		$getRecords = [];
		$i=0;
		foreach($allRecords as $records){
		 	if($records->image) {
				$getRecords[$i]['image'] = url('/')."/images/products/thumbnails/".$records->image;
			} else{
				$getRecords[$i]['image'] = url('/')."/images/no-image.jpg";
			}
		 	$getRecords[$i]['id'] = $records->_id;
		 	$getRecords[$i]['name'] = $records->name;
		 	$getRecords[$i]['price'] = $records->price;
		 	$i++;
		}
		$sendData['storeproducts'] = $getRecords;
		
		$countFavourite = Favourite::where(['type' => $type, 'type_id' => $store_id, 'user_id' => $user_id])->count();
		$sendData['storedetail']['favourite'] = ($countFavourite)?true:false;
		$sendData['favourite'] = ($countFavourite)?true:false;
		}else{
			$sendData['storedetail'] = 'no store found.';
		}
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	
	}

	public function getWhatsHappening(Request $request) {
		$where['featured'] = 1;
		$size ="";
		if($request->has('no_record')) {
			$this->paginate_total = $request->no_record;
			$size ="320x320_";
		}	
		$productCategories = ProductCategory::where(['type' => 'Product'])->orderBy('name','asc')->select('_id as id','name')->get();
		$sendData['allProductCategories'] = $productCategories;
		$product_category_id = $request->categoryId;
		if(!empty($product_category_id)){
		    $allRecords = Product::where(['products.featured' => 1, 'products.product_category_id'=> $product_category_id])
			        ->join('shops as s', 's._id', '=', 'products.shop_id')
					->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
					->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
					->select('products._id','products.name','products.image','products.price','products.discounted_price','products.product_quantity')
					->with('shop')
					->with('product_category')
					->with('product_subcategory')
				    ->inRandomOrder()
					->groupBy('products._id')
					->get();
		}else{
			$allRecords = Product::where($where)
			        ->join('shops as s', 's._id', '=', 'products.shop_id')
					->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
					->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
					->select('products._id','products.name','products.image','products.price','products.discounted_price','products.product_quantity')
					->with('shop')
					->with('product_category')
					->with('product_subcategory')
				    ->inRandomOrder()
					->groupBy('products._id')
					->get();
		}
				
		$getRecords = [];
		$i=0;
		foreach($allRecords as $records) {
			if($records->image) {
				$getRecords[$i]['image'] = url('/')."/images/products/thumbnails/".$size."".$records->image;
			} else {
				$getRecords[$i]['image'] = url('/')."/images/no-image.jpg";
			}

			$getRecords[$i]['id'] = $records->_id;
			$getRecords[$i]['name'] = $records->name;
			$getRecords[$i]['price'] = $records->price;
			$getRecords[$i]['discountedPrice'] = $records->discounted_price;
			$getRecords[$i]['quantity'] = $records->product_quantity;
			$getRecords[$i]['type'] = 'product';

			$token = $request->token;
			$user = SiteUsers::where(['token' => $token])->select('_id')->first();
			$user_id = $user->_id;
			$usercartData = Usercart::where('user_id', '=', $user_id)->where('product_id', '=', $records->_id)->select('users_cart._id')->first();
			if(!empty($usercartData->_id)){
				$getRecords[$i]['already_in_cart'] = 'Yes';
			}else{
				$getRecords[$i]['already_in_cart'] = 'No';
			}

			$i++;
		}
		$sendData['allWHitems'] = $getRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

	public function addToCart(Request $request) {
		$token = $request->token;
		$product_id = $request->product_id;
		$attr_key = ($request->attr_key) ? $request->attr_key : '';
		$attr_val = ($request->attr_val) ? $request->attr_val : '';
		$quantity = $request->quantity;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$data = [
			'user_id' => $user->_id,
			'product_id' => $product_id,
			'quantity' => $quantity,
			'attr_key' => $attr_key,
			'attr_val' => $attr_val,
		];
		Usercart::updateOrCreate(
		['user_id' => $user->_id, 'product_id' => $product_id],
		$data
		);
		$responseData = [
			'code' => 200,
			'content' => ['message' => 'success']
		];
		return response()->json($responseData);
	}

	public function getViewcartItems(Request $request) {
		$token = $request->token;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;

		$allRecords = Usercart::where(['user_id' => $user_id])
					->join('products as prod','prod._id','=','users_cart.product_id')
					->select('prod._id','prod.name','prod.image','prod.product_quantity as maxQuantity','prod.price','prod.discounted_price','users_cart.quantity','users_cart.attr_key','users_cart.attr_val')
					->orderBy('name','asc')
					->get();
		$getRecords = [];
		$i=0;
		foreach($allRecords as $records) {
			if($records->image) {
				$getRecords[$i]['image'] = url('/')."/images/products/thumbnails/".$records->image;
			} else {
				$getRecords[$i]['image'] = url('/')."/images/no-image.jpg";
			}
			$getRecords[$i]['id'] = $records->_id;
			$getRecords[$i]['name'] = $records->name;
			$getRecords[$i]['price'] = $records->price;
			$getRecords[$i]['discountedPrice'] = $records->discounted_price;
			$getRecords[$i]['maxQuantity'] = $records->maxQuantity;
			$getRecords[$i]['quantity'] = $records->quantity;
			$getRecords[$i]['attr_key'] = $records->attr_key;
			$getRecords[$i]['attr_val'] = $records->attr_val;
			$i++;
		}
		$sendData['allProduct'] = $getRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

	public function placeOrder(Request $request){
		$token = $request->token;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;
		$name = $request->name;
		$nameData = explode(" ", $name);
		$firstname = $nameData[0];
		
		if(!empty($nameData[1])){
		   $lastname = str_replace($firstname,"",$name);
		}else{
		   $lastname = ' ';    
		}
		$address = $request->address;
		$phone = $request->phone;
		$payment_method = $request->payment_method;
		$totalPrice = $request->totalPrice;
		$shippingCharge = $request->shippingCharge;
		$order_id = $this->addNewOrder($user_id,$firstname,$lastname,$address,$phone,$payment_method,$totalPrice,$shippingCharge);
		if(!empty($order_id)){
			$getOrderDetails = $this->addOrderDetails($user_id,$order_id);
			if(!empty($getOrderDetails)){
				$emptyShoppingBag = $this->emptyShoppingBag($user_id);
			}
			$responseData = [
				'code' => 200,
				'content' => ['message' => 'success']
			];
		}else{
			$responseData = [
				'code' => 401,
				'content' => ['message' => 'error']
			];
		}	
		return response()->json($responseData);
	}

	public function addNewOrder($user_id,$firstname,$lastname,$address,$phone,$payment_method,$totalPrice,$shippingCharge){
		$user_id = $user_id;
		$emptyArray = '';
		$allRecords = Usercart::where(['user_id' => $user_id])
					->join('products as prod','prod._id','=','users_cart.product_id')
					->select('prod._id','prod.name','prod.details','prod.image','prod.seller_id','prod.price','prod.discounted_price','users_cart.quantity','users_cart.attr_key','users_cart.attr_val')
					->orderBy('name','asc')
					->get();
		if(!empty($allRecords)){			
			$new_order = Orders::create([
				'user_id' => $user_id,
				'amount' => $totalPrice,
				'shippingCharge' => $shippingCharge,
				'payment_method' => $payment_method,
				'firstname' => $firstname,
				'lastname' => $lastname,
				'address' => $address,
				'phone' => $phone
			]);
			return $new_order->_id;
		}else{
			return $emptyArray; 
		}
	}

	public function addOrderDetails($user_id,$order_id){
		$user_id = $user_id;
		$allRecords = Usercart::where(['user_id' => $user_id])
					->join('products as prod','prod._id','=','users_cart.product_id')
					->select('prod._id','prod.name','prod.details','prod.image','prod.seller_id','prod.price','prod.discounted_price','users_cart.quantity','users_cart.attr_key','users_cart.attr_val')
					->orderBy('name','asc')
					->get();
		$orderData = array();			
		foreach($allRecords as $k => $records){
            if(!empty($records->discounted_price) && ($records->discounted_price > 0)){
			  $price = $records->discounted_price;
			}else{
			  $price = $records->price;
			}
			$productId = $records->_id;
			$quantity = $records->quantity;
			$updateProductQuantity = $this->updateProductQuantity($productId,$quantity);
			$orderUniqid = 'ORD-'.date('dmy').rand();
			$orderData[$k] = OrderDetail::create([
				'order_id' => $order_id,
				'orderUniqid' => $orderUniqid,
				'product_name' => $records->name,
				'product_details' => $records->details,
				'product_image' => $records->image,
                'order_status' => 'pending', 
				'product_seller_id' => $records->seller_id,
				'product_id' => $records->_id,
				'quantity' => $records->quantity,
				'price' => $price,
				'discounted_price' => $records->discounted_price,
				'attr_key' => $records->attr_key,
				'attr_val' => $records->attr_val
			]);
		}
		return $orderData;
	}

	public function updateProductQuantity($productId,$quantity) {
		$updatedQuantity = '';
		if(!empty($productId)){
			$allRecords = Product::where(['products._id' => $productId])
								   ->select('products.product_quantity')
								   ->get();
            if($allRecords[0]->product_quantity > 0){ 
                $quantityValue = $allRecords[0]->product_quantity - $quantity;
				$updatedData['product_quantity'] = $quantityValue;
				$updatedQuantity = Product::where(['_id' => $productId])->update($updatedData);
			}
	   }
	   return $updatedQuantity;
	}

	public function emptyShoppingBag($user_id) {
		$removeCart = Usercart::where(['user_id' => $user_id])->forceDelete();
		return $removeCart;
	}

	public function removeCartItem(Request $request) {
		$token = $request->token;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;
		$product_id = explode(",",$request->product_id);
		foreach($product_id as $row_id){
			Usercart::where(['user_id' => $user_id, 'product_id' => $row_id])->forceDelete();
		}
		$responseData = [
			'code' => 200,
			'content' => ['message' => 'success']
		];
		return response()->json($responseData);
	}
 
	public function addressBook(Request $request) {
		$token = $request->token;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;
        if(!empty($user_id)){
            if(!empty($request->addressDelete)){
				SiteUsersAddress::where(['site_users_address.user_id' => $user_id])->delete();
			}
			if(!empty($request->addressDic)){
				SiteUsersAddress::where(['site_users_address.user_id' => $user_id])->delete();
				foreach($request->addressDic as $key => $addressData){ 	
				    $addData = SiteUsersAddress::create([
						'user_id' => $user_id,
						'name' => $addressData['name'],
						'address' => $addressData['address'],
						'address_type' => $addressData['address_type'],
						'phone' => $addressData['phone']
					]);
				}	
			}	
			$allRecords = SiteUsersAddress::where(['site_users_address.user_id' => $user_id])
					->select('site_users_address._id','site_users_address.name','site_users_address.address','site_users_address.address_type','site_users_address.phone')
					->get();
			$sendData['AddressDetail'] = $allRecords;
			$responseData = [
				'code' => 200,
				'content' => $sendData
			];     
		}else{
			$responseData = [
				'code' => 200,
				'content' => ['message' => 'error']
			];
		}
		return response()->json($responseData);
	}

	public function myOrders(Request $request) {
		$token = $request->token;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;
		$orderRecords = array();
        if(!empty($user_id)){
			$orders = Orders::where(['user_id' => $user_id])
							  ->select('_id','amount','payment_method','firstname','lastname','address','phone','created_at')
							  ->get();
			foreach($orders as $key => $order){
				$orderDetails = OrderDetail::where(['order_id' => $order->_id])
										    ->select('orderUniqid','product_id','product_name','product_details','product_image','quantity','price','discounted_price','order_status','attr_key','attr_val','created_at','updated_at')
											->get();
				
				foreach($orderDetails as $k => $orderDetail){
					$orderRecords[$k]['id'] = $order->_id;
					$orderRecords[$k]['firstname'] = $order->firstname;
					$orderRecords[$k]['lastname'] = $order->lastname;
					$orderRecords[$k]['phone'] = $order->phone;
					$orderRecords[$k]['address'] = $order->address;
					$orderRecords[$k]['payment_method'] = $order->payment_method;
					$orderRecords[$k]['amount'] = $order->amount;
					$orderRecords[$k]['orderDate'] = $this->dateFormate($order->created_at);

					$orderRecords[$k]['orderUniqid'] = $orderDetail->orderUniqid;
					$orderRecords[$k]['product_id'] = $orderDetail->product_id;
					$orderRecords[$k]['product_name'] = $orderDetail->product_name;
					$orderRecords[$k]['product_details'] = $orderDetail->product_details;
					if(isset($orderDetail->product_image)) {
						$orderRecords[$k]['product_image'] = url('/')."/images/products/".$orderDetail->product_image;
					} else {
						$orderRecords[$k]['product_image'] = url('/')."/images/no-image.jpg";
					}
                    $orderRecords[$k]['quantity'] = $orderDetail->quantity;
					$orderRecords[$k]['quantity'] = $orderDetail->quantity;
					$orderRecords[$k]['price'] = $orderDetail->price;
					$orderRecords[$k]['discounted_price'] = $orderDetail->discounted_price;
					$orderRecords[$k]['order_status'] = $orderDetail->order_status;
					$orderRecords[$k]['attr_key'] = $orderDetail->attr_key;
					$orderRecords[$k]['attr_val'] = $orderDetail->attr_val;
					$orderRecords[$k]['createdDate'] = $this->dateFormate($orderDetail->created_at);
					$orderRecords[$k]['updatedDate'] = $this->dateFormate($orderDetail->updated_at);
				}
				$allRecords[$key] = $orderRecords;							
			}	
			$oneDimensionalArray = call_user_func_array('array_merge', $allRecords);			  
			$sendData['orders'] = $oneDimensionalArray;
			$responseData = [
				'code' => 200,
				'content' => $sendData
			];     
		}else{
			$responseData = [
				'code' => 401,
				'content' => ['message' => 'error']
			];
		}
		return response()->json($responseData);
	}

	public function orderCancel(Request $request) {
		$token = $request->token;
		$orderId = $request->orderId;
		$productId = $request->productId;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$userId = $user->_id;
		$updateOrderDetail = '';
        if(!empty($userId)){
			$orders = Orders::where(['user_id' => $userId])->select('_id')->get();
			foreach($orders as $key1 => $order){
				if($order->_id == $orderId){
					$orderDetails = OrderDetail::where(['order_id' => $order->_id])->select('product_id','order_status')->get();
					foreach($orderDetails as $k1 => $orderDetail){
						if($orderDetail->product_id == $productId){
							$updatedData['order_status'] = 'canceled';
							$updateOrderDetail = OrderDetail::where(['order_id' => $orderId,'product_id' => $productId])->update($updatedData);  
						}
					}
				}
			}
			if(!empty($updateOrderDetail)){
				$responseData = [
					'code' => 200,
					'content' => ['message' => 'success']
				];
			}else{
				$responseData = [
					'code' => 401,
					'content' => ['message' => 'error']
				];
			} 
		}else{
			$responseData = [
				'code' => 401,
				'content' => ['message' => 'error']
			];
		}
		return response()->json($responseData);
	}

	public function tenantLogin(Request $request) {
		$email = $request->email;
		$password = md5($request->password);
		$getRecord = Tenantadmin::where([ 'email' => $email, 'password' => $password ])->get();
		if(!empty($getRecord[0]->tenant_id)){
			$tenantId = $getRecord[0]->tenant_id;
			if(!empty($tenantId)){
				$userData = array();
				$userData['tenantId'] = $getRecord[0]->tenant_id;
				$userData['name'] = $getRecord[0]->name;
				$userData['email'] = $getRecord[0]->email;
				$newtoken = Str::random(60);
				$tokenUpdate = hash('sha256', $newtoken);
				Tenantadmin::where([ 'email' => $email ])->update([ 'remember_token' => $tokenUpdate ]);
				$responseData = [
					'code' => 200,
					'content' => array('tenantDetail' => $userData)
				];
			}else{
				$responseData = [
					'code' => 401,
					'content' => ['error ' => "Log in"]
				];
			}
		}else{
			$responseData = [
				'code' => 401,
				'content' => ['error ' => "Log in"]
			];
		}
		return response()->json($responseData);
	}

	public function tenantComplaints(Request $request) {
		$token = $request->token;
		$tenantId = $request->tenantId;
		$complaint = $request->complaint;
		if(!empty($tenantId)){
			TenantComplaints::create([
				'tenant_id' => $tenantId,
				'complaint' => $complaint,
				'status' => 'unread',
			]);
			$responseData = [
				'code' => 200,
				'content' => ['message' => 'success']
			];
		}else{
			$responseData = [
				'code' => 401,
				'content' => ['message' => 'error']
			];
		}
		return response()->json($responseData);
	}

	public function leasingEnquiry(Request $request) {
		$token = $request->token;
		//$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		//$user_id = $user->_id;
		$brand = $request->brand;
		$companyName = $request->name;
		$details = $request->details;
		$designation = $request->designation;
		$address = $request->address;
		$mobile = $request->mobile;
		$email = $request->email;
		$website = $request->website;
		$category = $request->category;
		$concept = $request->concept;
		$stores = $request->stores;
        if(!empty($email)){
			LeasingEnquiry::create([
				//'user_id' => $user_id,
				'brand' => $brand,
				'companyName' => $companyName,
				'details' => $details,
				'designation' => $designation,
				'address' => $address,
				'mobile' => $mobile,
				'email' => $email,
				'website' => $website,
				'category' => $category,
				'concept' => $concept,
				'stores' => $stores
			]);
			$responseData = [
				'code' => 200,
				'content' => ['message' => 'success']
			];
		}else{
			$responseData = [
				'code' => 401,
				'content' => ['message' => 'error']
			];
		}
		return response()->json($responseData);
	}

	public function leasingDetails(Request $request) {
		$token = $request->token;
		$email = $request->email;
		$user = User::where(['email' => $email])->select('id','tenant_id')->first();
		$userId = $user->id;
		$tenantID = $user->tenant_id;
		$allRecords = array();
		if(!empty($userId) && !empty($tenantID)){
			$tenantData = Tenant::where(['id' => $tenantID])->select('id','name','email','phone','photo_doc','shop_name')->get();
				foreach($tenantData as $tKey => $tenant){
					$tenantRecord[$tKey]['id'] = $tenant->id;
					$tenantRecord[$tKey]['name'] = $tenant->name;
					$tenantRecord[$tKey]['email'] = $tenant->email;
					$tenantRecord[$tKey]['phone'] = $tenant->phone;
					if(!empty($tenant->photo_doc)) {
						$tenantRecord[$tKey]['image'] = url('/')."/images/cms/".$tenant->photo_doc;
					} else {
						$tenantRecord[$tKey]['image'] = url('/')."/images/no-image.jpg";
					}
					$shopsName = json_decode($tenant->shop_name, true);
					$shopRecords = array();
					foreach($shopsName as $key2 => $shops){
						$shopRecord = Shop::where(['shops._id' => $shops['_id']])->select('shops._id','shops.name','shops.logo','shops.phone','shops.email','shops.address','shops.details','shops.floorlevel')->get();
						foreach($shopRecord as $dKey => $record){
							$shopsDetail[$dKey]['_id'] = $record->_id;
							$shopsDetail[$dKey]['name'] = $record->name;
							if(!empty($record->logo)) {
								$shopsDetail[$dKey]['logo'] = url('/')."/images/shops/".$record->logo;
							} else {
								$shopsDetail[$dKey]['logo'] = url('/')."/images/no-image.jpg";
							}
							$shopsDetail[$dKey]['phone'] = $record->phone;
							$shopsDetail[$dKey]['email'] = $record->email;
							$shopsDetail[$dKey]['address'] = $record->address;
							$shopsDetail[$dKey]['details'] = $record->details;
							$shopsDetail[$dKey]['floorlevel'] = $record->floorlevel;
							$shopsDetail[$dKey]['leaseyear'] = $record->lease_year;
						}
						$shopRecords[] = $shopsDetail;
					}
					$oneDimensionalArray = call_user_func_array('array_merge', $shopRecords);
					$tenantRecord[$tKey]['shopsDetail'] = $oneDimensionalArray;

					$accountDetails = Leasing::where(['tenant_id' => $tenantID])->select('shops','rent_amount','payment_status','payment_method','pending_amount','created_at','updated_at')->get();
					foreach($accountDetails as $aKey => $account){
						$shopName = json_decode( $account->shops, true);
						$accountData[$aKey]['shopName'] = $shopName[0]['name'];
						$accountData[$aKey]['rent_amount'] = $account->rent_amount;
						$accountData[$aKey]['payment_status'] = $account->payment_status;
						$accountData[$aKey]['payment_method'] = $account->payment_method;
						$accountData[$aKey]['pending_amount'] = $account->pending_amount;
						$accountData[$aKey]['createdDate'] = $this->dateFormate($account->created_at);
						$accountData[$aKey]['updatedDate'] = $this->dateFormate($account->updated_at);
					}
					$tenantRecord[$tKey]['accountDetail']  = $accountData;
				}
			$sendData['ownerDetails'] = $tenantRecord;
			$responseData = [
				'code' => 200,
				'content' => $sendData
			];
		}else{
			$responseData = [
				'code' => 401,
				'content' => ['message' => 'error']
			];
		}
		return response()->json($responseData);
	}

	public function dateFormate($date){
		$getDate = date('d F Y', strtotime($date));
		return $getDate;
	}

}