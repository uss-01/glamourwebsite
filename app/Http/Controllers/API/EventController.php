<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use App\EventPics;
use App\UserJoinEvent;
use JWTAuth;
use JWTAuthException;
use Carbon\Carbon;
use App\SiteUsers;
class EventController extends Controller
{
	    public function __construct(Request $request){

		$page = isset($request->page) ? $request->page : 1;
		$this->limit = isset($request->recordPerPage) ? $request->recordPerPage : 5000;
		$this->offset = ($page - 1) * $this->limit;
    }
     public function getAllEvent(Request $request) {
		$token = $request->token;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;
		
		if($request->has('no_record')) {
			$this->paginate_total = $request->no_record;
		}
		$allRecords = Event::orderBy('title','asc')
							->select('events._id as id','events.title','events.sub_title','events.event_time', 'events.event_location','events.details','events.image','events.start_date','events.end_date')
							->skip($this->offset)->take($this->limit)->get();
		$getRecords = [];
		$getTodayRecords = [];
		$getUpcomingRecords = [];
        $i=0; $j=0; $k=0; 
		foreach($allRecords as $records) {
			$start = strtotime($records->start_date); 
			$startDate = date('Y-m-d', $start);
			$end = strtotime($records->end_date); 
			$endDate = date('Y-m-d', $end);
			$currentDate = date('Y-m-d', time());
            if($currentDate > $endDate){

			}else{ 
				if($records->image) {
					$getRecords[$i]['image'] = url('/')."/images/events/".$records->image;
				} else {
					$getRecords[$i]['image'] = url('/')."/images/no-image.jpg";
				}
				$getRecords[$i]['id'] = $records->id;
				$getRecords[$i]['title'] = $records->title;
				$getRecords[$i]['sub_title'] = $records->sub_title;
				$getRecords[$i]['event_time'] = $records->event_time;
				$getRecords[$i]['event_location'] = $records->event_location;
				$getRecords[$i]['details'] = $records->details;
				$getRecords[$i]['start_date'] = $records->start_date;
				$getRecords[$i]['end_date'] = $records->end_date;
				$getRecords[$i]['join_status'] = ($this->checkJoinStatus($user_id,$records->id))? 'yes': 'no';
					// now fetching slide images
					$allSlideImage = EventPics::where(['event_id' => $records->id])->select('image')->get();
					$getSlideImage = [];
					$ii=0;
					$getSlideImage[$ii++]['image'] = $getRecords[$i]['image'];
					foreach($allSlideImage as $slideImage) {
						if($slideImage->image){
							$getSlideImage[$ii]['image'] = url('/')."/images/events/".$slideImage->image;
						}else{
							$getSlideImage[$ii]['image'] = url('/')."/images/no-image.jpg";
						}
						$ii++;
					}
					$getRecords[$i]['slideimages'] = $getSlideImage;                  
				$i++;
			}
			if(($currentDate == $startDate) || (($currentDate >= $startDate) && ($currentDate <= $endDate))){
                if($records->image) {
					$getTodayRecords[$j]['image'] = url('/')."/images/events/".$records->image;
				} else {
					$getTodayRecords[$j]['image'] = url('/')."/images/no-image.jpg";
				}
				$getTodayRecords[$j]['id'] = $records->id;
				$getTodayRecords[$j]['title'] = $records->title;
				$getTodayRecords[$j]['sub_title'] = $records->sub_title;
				$getTodayRecords[$j]['event_time'] = $records->event_time;
				$getTodayRecords[$j]['event_location'] = $records->event_location;
				$getTodayRecords[$j]['details'] = $records->details;
				$getTodayRecords[$j]['start_date'] = $records->start_date;
				$getTodayRecords[$j]['end_date'] = $records->end_date;
				$getTodayRecords[$j]['join_status'] = ($this->checkJoinStatus($user_id,$records->id))? 'yes': 'no';
					// now fetching slide images
					$allSlideImage = EventPics::where(['event_id' => $records->id])->select('image')->get();
					$getSlideImage = [];
					$jj=0;
					$getSlideImage[$jj++]['image'] = $getTodayRecords[$j]['image'];
					foreach($allSlideImage as $slideImage) {
						if($slideImage->image){
							$getSlideImage[$jj]['image'] = url('/')."/images/events/".$slideImage->image;
						}else{
							$getSlideImage[$jj]['image'] = url('/')."/images/no-image.jpg";
						}
						$jj++;
					}
					$getTodayRecords[$j]['slideimages'] = $getSlideImage;                  
				$j++;
			} 
			if($currentDate < $startDate){
                if($records->image) {
					$getUpcomingRecords[$k]['image'] = url('/')."/images/events/".$records->image;
				} else {
					$getUpcomingRecords[$k]['image'] = url('/')."/images/no-image.jpg";
				}
				$getUpcomingRecords[$k]['id'] = $records->id;
				$getUpcomingRecords[$k]['title'] = $records->title;
				$getUpcomingRecords[$k]['sub_title'] = $records->sub_title;
				$getUpcomingRecords[$k]['event_time'] = $records->event_time;
				$getUpcomingRecords[$k]['event_location'] = $records->event_location;
				$getUpcomingRecords[$k]['details'] = $records->details;
				$getUpcomingRecords[$k]['start_date'] = $records->start_date;
				$getUpcomingRecords[$k]['end_date'] = $records->end_date;
				$getUpcomingRecords[$k]['join_status'] = ($this->checkJoinStatus($user_id,$records->id))? 'yes': 'no';
					// now fetching slide images
					$allSlideImage = EventPics::where(['event_id' => $records->id])->select('image')->get();
					$getSlideImage = [];
					$kk=0;
					$getSlideImage[$kk++]['image'] = $getUpcomingRecords[$k]['image'];
					foreach($allSlideImage as $slideImage) {
						if($slideImage->image){
							$getSlideImage[$kk]['image'] = url('/')."/images/events/".$slideImage->image;
						}else{
							$getSlideImage[$kk]['image'] = url('/')."/images/no-image.jpg";
						}
						$kk++;
					}
					$getUpcomingRecords[$k]['slideimages'] = $getSlideImage;                  
				$k++;
			}
		}
		$sendData['today'] = $getTodayRecords;
		$sendData['upcoming'] = $getUpcomingRecords;   
		$sendData['events'] = $getRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}
	
	public function checkJoinStatus($user_id,$event_id) {
		return UserJoinEvent::where(['user_id' => $user_id, 'event_id' => $event_id])->count();
	}
	
	public function userJoinEvent(Request $request) {
		$token = $request->token;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;
		$event_id = $request->event_id;
		$flag = $request->join_status; // yes/no 
		if($flag == "yes") {
			UserJoinEvent::updateOrCreate(['user_id' => $user_id,'event_id' => $event_id],['user_id' => $user_id,'event_id' => $event_id]);
		} else if($flag == "no") {
			UserJoinEvent::where(['user_id' => $user_id,'event_id' => $event_id])->delete();
		}
		$responseData = [
			'code' => 200,
			'content' => ['message' => 'success']
		];
		return response()->json($responseData);
	}
	
	public function getEventDetail(Request $request) {
		$event_id = $request->event_id;
		$token = $request->token;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;
		$getRecords = Event::where(['_id' => $event_id])
							->select('events._id as id','events.title','events.sub_title','events.event_time', 'events.event_location','events.details','events.image','events.start_date','events.end_date')
							->first();
		if($getRecords['image']) {
			$getRecords['image'] = url('/')."/images/events/".$getRecords['image'];
		} else {
			$getRecords['image'] = url('/')."/images/no-image.jpg";
		}
		$getRecords['join_status'] = ($this->checkJoinStatus($user_id,$event_id))? 'yes': 'no';

		// now fetching slide images
		$allSlideImage = EventPics::where(['event_id' => $event_id])->select('image')->get();
		$getSlideImage = [];
		$j=0;
		$getSlideImage[$j++]['image'] = $getRecords['image'];
		foreach($allSlideImage as $slideImage) {
			if($slideImage->image){
				$getSlideImage[$j]['image'] = url('/')."/images/events/".$slideImage->image;
			}else{
				$getSlideImage[$j]['image'] = url('/')."/images/no-image.jpg";
			}
			$j++;
		}
		$getRecords['slideimages'] = $getSlideImage; 

		$sendData['event_detail'] = $getRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}
}
