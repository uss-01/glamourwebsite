<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\SiteUsers;
use App\SiteUsersEntryExitTime;
use App\Favourite;
use App\Masters\Experience;
use App\SiteUserExperience;
use App\Support\Incident;
use App\Support\LostFound;
use JWTAuth;
use JWTAuthException;
use Image;
use Validator;
use Mail;

class SiteUsersController extends Controller
{
	
	public function loginFBUser(Request $request) {
		if($request->personalInfo === 'personalInfo'){
			$token = $request->token;
			$country = $request->country;
			$callingCode = $request->callingCode;
			$phone = $request->phone;
			$dob = $request->dob;
			$age = $request->age;
			$userAddress = $request->userAddress;
			$gender = $request->gender;
			$countryCode = $request->countryCode;
			$phoneCode = $request->phoneCode;
			$ipAddress = $request->ipAddress;
		}else{
			$deviceArr = array(1=>'ios', 2=>'android');
			$fbid = $request->fbid;
			$token = $request->token;
			$push_token = $request->push_token;
			$fname = $request->fname;
			$ipAddress = $request->ipAddress;
			$lname = $request->lname;
			$email = $request->email;
			$country = $request->country;
			$phone = $request->phone;
			$dob = \Carbon\Carbon::parse($request->dob)->format('Y-m-d');
			$gender = $request->gender;
			$device = $deviceArr[$request->device];
			$about = $request->about;
		}
		try {
			if($request->personalInfo === 'personalInfo'){
				SiteUsers::where(['token' => $token])->update([ 'country' => $country, 'callingCode' => $callingCode,'ipAddress' => $ipAddress, 'phone' => $phone, 'dob' => $dob, 'age' => $age, 'gender' => $gender, 'countryCode' => $countryCode, 'phoneCode' => $phoneCode, 'userAddress' => $userAddress ]);
			}else{
				SiteUsers::updateOrCreate(
					['fbid' => $fbid],
					[ 'fbid'  => $fbid,
					'token' => $token,
					'device' => $device,
					'push_token' => $push_token,
					'fname' => $fname,
					'lname' => $lname,
					'email' => $email,
					'ipAddress' => $ipAddress,
				    ]
			    );
			}

			$getRecord = SiteUsers::where(['token' => $request->token])->get();	
			$getRecord['token'] = $getRecord[0]->token;
			$responseData = [
				'code' => 200,
				'content' => array('userdetail' => $getRecord, 'token' => $token)
			];
			return response()->json($responseData);
		} catch (\Exception $e) {
			$responseData = [
				'code' => 401,
				'content' => ['error ' => "Log in"]
			];
			return response()->json($responseData);
		}
		
	}
	
	public function getAuthUser(Request $request) {
		$getRecord = SiteUsers::where(['token' => $request->token])->get();	
		$getRecord['membership'] = getMemberShip($getRecord[0]->points);	
		$favourites = $this->userFavorites($getRecord[0]->_id);
		$experience = $this->userExperience($getRecord[0]->_id);

		$getRecord[0]['favourite_stores'] = $favourites;
		$getRecord[0]['user_experience'] = $experience;
		$responseData = [
			'code' => 200,
			'content' => ['userdetail' => $getRecord]
		];
		return response()->json($responseData);	
	}
	
	/*public function getUserEnterExit(Request $request){
		$getRecord = SiteUsers::where(['token' => $request->token])->get();
		$getOldCount = $getRecord[0]->visit_count;
		$totalTime = $getRecord[0]->totalTime;

		$visitCount = $request->visitCount;
		$enterTime = $request->enterTime;
		$exitTime = $request->exitTime;
		$timeDiff = $request->minutes;

		if(!empty($timeDiff)){
			$minutes = explode('.',$totalTime+$timeDiff);
			$updatePoints = explode('.',$minutes[0]/60);
			$entryExitTime = SiteUsersEntryExitTime::create(['site_user_id'=>$getRecord[0]->_id,'entry'=> $enterTime,'exit'=> $exitTime]);
			if($visitCount <= $getOldCount){
				$updateSiteUsers = SiteUsers::where('token', $request->token)->update(['enterTime' => $enterTime, 'exitTime' => $exitTime, 'totalTime' => $minutes[0], 'points' => $updatePoints]);
			}else{
				$updateSiteUsers = SiteUsers::where('token', $request->token)->update(['visit_count' => $visitCount, 'enterTime' => $enterTime, 'exitTime' => $exitTime, 'totalTime' => $minutes[0], 'points' => $updatePoints]);
			}
		}
		$getAllRecord = SiteUsers::where(['token' => $request->token])->get();
		$getAllRecords['visitCount'] = $getAllRecord[0]->visit_count;
		$getAllRecords['membership'] = getMemberShip($getAllRecord[0]->points);
		$getAllRecords['points'] = $getAllRecord[0]->points;
		$responseData = [
			'code' => 200,
			'content' => ['userdetail' => $getAllRecords]
		];
		return response()->json($responseData);	
	}*/

	public function getUserEnterExit(Request $request){
		$getRecord = SiteUsers::where(['token' => $request->token])->get();
		$getOldCount = $getRecord[0]->visit_count;
		
		$visitCount = $request->visitCount;
		$enterTime = $request->enterTime;
		$exitTime = $request->exitTime;
		$memberShip = $request->memberShip;
		$points = $request->points;
		$action = $request->action;
		if($action == 'edit'){
		    if($visitCount <= $getOldCount){
				$updateSiteUsers = SiteUsers::where('token', $request->token)->update(['enterTime' => $enterTime, 'exitTime' => $exitTime, 'points' => $points, 'membership' => $memberShip]);
			}else{
				$updateSiteUsers = SiteUsers::where('token', $request->token)->update(['visit_count' => $visitCount, 'enterTime' => $enterTime, 'exitTime' => $exitTime, 'points' => $points, 'membership' => $memberShip]);
			}
		}
		$getAllRecord = SiteUsers::where(['token' => $request->token])->get();
		$getAllRecords['visitCount'] = $getAllRecord[0]->visit_count;
		$getAllRecords['membership'] = $getAllRecord[0]->membership;
		$getAllRecords['points'] = $getAllRecord[0]->points;
		$responseData = [
			'code' => 200,
			'content' => ['userdetail' => $getAllRecords]
		];
		return response()->json($responseData);	
	}
	
	public function getUserMembership(Request $request){
		$getRecord = SiteUsers::where(['token' => $request->token])->get();
		$getRecords['visitCount'] = $getRecord[0]->visit_count;
		$getRecords['membership'] = getMemberShip($getRecord[0]->points);
		$getRecords['points'] = $getRecord[0]->points;
		$responseData = [
			'code' => 200,
			'content' => ['userdetail' => $getRecords]
		];
		return response()->json($responseData);	
	}

	// $push_notify : enable or disable;
	public function updatePushNotification(Request $request) {	
		$push_notify = $request->push_notify;
		if($push_notify === 'enable') {
			SiteUsers::where(['token' => $request->token])->update(['notify_push' => 1]);	
		} else if($push_notify === 'disable') {
			SiteUsers::where(['token' => $request->token])->update(['notify_push' => 0]);	
		}
		$responseData = [
			'code' => 200,
			'content' => ['message' => 'success']
		];
		return response()->json($responseData);			
	}
	
	// $notify_offers : enable or disable;
	public function updateOffersNotification(Request $request) {	
		$notify_offers = $request->notify_offers;
		if($notify_offers === 'enable') {
			SiteUsers::where(['token' => $request->token])->update(['notify_offers' => 1]);	
		} else if($notify_offers === 'disable') {
			SiteUsers::where(['token' => $request->token])->update(['notify_offers' => 0]);	
		}
		$responseData = [
			'code' => 200,
			'content' => ['message' => 'success']
		];
		return response()->json($responseData);			
	}
	
	public function editProfile(Request $request) {
		$token = $request->token;
		$updatedData = [];
		if(isset($request->about)){
			$updatedData['about'] = $request->about;
		}
		if(isset($request->current_work)){
			$updatedData['current_work'] = $request->current_work;
		}
		if(isset($request->instagram)){
			$updatedData['instagram'] = $request->instagram;
		}
		if(isset($request->gender)){
			$updatedData['gender'] = $request->gender;
		}
		if(isset($request->profile_mode)){
			$updatedData['profile_mode'] = $request->profile_mode;
		}
		if( isset($request->lat) && isset($request->long)){
			$updatedData['lat'] = $request->lat;
			$updatedData['long'] = $request->long;
		}

		SiteUsers::where(['token' => $token])->update($updatedData);
		$responseData = [
			'code' => 200,
			'content' => ['message' => 'success']
		];
		return response()->json($responseData);	
	}

	public function getAllExperience(Request $request) {
		$experience = Experience::withTrashed()
		->whereNull('deleted_at')
		->select('_id as id','name')
		->get();				
		$sendData['experiences'] = $experience;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);	
	}

	public function userExperience($user_id){
		$experience = SiteUserExperience::where(['user_id' => $user_id ])
		->join('experiences as ex','ex._id', '=', 'site_user_experiences.experience_id')
		->select('ex._id as id','ex.name')
		->get();
		return $experience;
	}

	public function addSiteUserExperience(Request $request) {
		$token = $request->token;
		$experience = explode(",",$request->experience);
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();	
		SiteUserExperience::where(['user_id' => $user->_id ])->delete();
		foreach($experience as $row){
			$experience = SiteUserExperience::create([
				'user_id' => $user->_id,
				'experience_id' => $row,
			]);
		}
		
		$responseData = [
			'code' => 200,
			'content' => ['message' => 'success']
		];
		return response()->json($responseData);	
	}

	public function addReportIncident(Request $request) {
		$token = $request->token;
		$name = $request->name;
		$email = $request->email;
		$phone = $request->phone;
		$message = $request->message;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();	
		$validator = Validator::make($request->all(),[
        'name' => 'required'
		]);
		if ($validator->passes()) {
			if(isset($request->image1)){
				$image_name1 = time().'_incident.png';
				$ImagePath = 'images/support/incidents/'.$image_name1;
				file_put_contents($ImagePath,base64_decode($request->image1));
			}else{
				$image_name1 = NULL;
			}
			if(isset($request->image2)){
				$image_name2 = time().'_incident1.png';
				$ImagePath = 'images/support/incidents/'.$image_name2;
				file_put_contents($ImagePath,base64_decode($request->image2));
			}else{
				$image_name2 = NULL;
			}
			if(isset($request->image3)){
			  $image_name3 = time().'_incident2.png';
				$ImagePath = 'images/support/incidents/'.$image_name3;
				file_put_contents($ImagePath,base64_decode($request->image3));
			}else{
				$image_name3 = NULL;
			}
			$incident = Incident::create([
				'name'=>$name,
				'image1'=> $image_name1,
				'image2'=> $image_name2,
				'image3'=> $image_name3,
				'email'=>$email,
				'phone'=>$phone,
				'message'=>$message,
				'created_by'=>$user->_id
			]);			
			$responseData = [
				'code' => 200,
				'content' => ['message' => 'success']
			];
		}else{
			$responseData = [
				'code' => 422,
				'content' => ['error' => "Somthing Went Wrong."]
			];
		}
		return response()->json($responseData);	
	}

	public function addReportLostFound(Request $request) {
		$token = $request->token;
		$name = $request->name;
		$email = $request->email;
		$phone = $request->phone;
		$message = $request->message;
		$type = $request->type;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$validator = Validator::make($request->all(),[
        'name' => 'required'
       ]);
		if ($validator->passes()) {
			if(isset($request->image1)){
				$image_name1 = time().'_lostfound.png';
				$ImagePath = 'images/support/lostfound/'.$image_name1;
				file_put_contents($ImagePath,base64_decode($request->image1));
			}else{
				$image_name1 = NULL;
			}
			if(isset($request->image2)){
				$image_name2 = time().'_lostfound1.png';
				$ImagePath = 'images/support/lostfound/'.$image_name2;
				file_put_contents($ImagePath,base64_decode($request->image2));
			}else{
				$image_name2 = NULL;
			}
			if(isset($request->image3)){
				$image_name3 = time().'_lostfound2.png';
				$ImagePath = 'images/support/lostfound/'.$image_name3;
				file_put_contents($ImagePath,base64_decode($request->image3));
			}else{
				$image_name3 = NULL;
			}
			$incident = LostFound::create([
				'name'=>$name,
				'image1'=>$image_name1,
				'image2'=>$image_name2,
				'image3'=>$image_name3,
				'email'=>$email,
				'phone'=>$phone,
				'message'=>$message,
				'type'=>$type,
				'created_by'=>$user->_id
			]);
			
			$responseData = [
				'code' => 200,
				'content' => ['message' => 'success']
			];
		}else{
			$responseData = [
				'code' => 422,
				'content' => ['error' => "Something Went Wrong"]
			];
		}
		return response()->json($responseData);	
	}

	public function addFavourite(Request $request) {
		$token = $request->token;
		$type = strtolower($request->type); // store,product,brand
		$action = 'favourite';
		if( strtolower($request->action) === 'remove') {
			$action = 'unfavourite';
		}else if( strtolower($request->action) === 'multipleremove') {
			$action = 'multipleUnfavourite';
		}
		
		if($type=='store' || $type =='product'|| $type == 'brand'){
			$favourite = $request->type_id; //store,brand_id,product_id
			$user = SiteUsers::where(['token' => $request->token])->select('_id')->first();	
			if($action == 'favourite') {
				Favourite::updateOrCreate([
					'user_id' => $user->_id,
					'type' => $type,
					'type_id' => $favourite,			   
				]);
				$msg = ucfirst($type) . " added to your favourite list.";
			} else if($action == 'unfavourite') {
				Favourite::where([
					'user_id' => $user->_id,
					'type' => $type,
					'type_id' => $favourite,			   
				])->delete();
				$msg = ucfirst($type) . " removed from your favourite list.";
			} else if($action == 'multipleUnfavourite') {
				  $multipleUnfavourite = explode(",",$request->type_id);
				  foreach($multipleUnfavourite as $unfavourite){
					Favourite::where([
						'user_id' => $user->_id,
						'type' => $type,
						'type_id' => $unfavourite,			   
					])->delete();
				  }
    			  $msg = ucfirst($type) . " removed from your favourite list.";
			}	
			$responseData = [
				'code' => 200,
				'content' => ['message' => $msg]
			];
		}else{
			$responseData = [
				'code' => 422,
				'content' => ['error' => 'Invalid Type']
			];
		}
		return response()->json($responseData);	
	}
	
	public function userFavorites($user_id){
		$allRecords = Favourite::where(['favourites.user_id' => $user_id,'favourites.type' => 'store'])
		//$allRecords = Favourite::where(['favourites.user_id' => $user_id])
		->join('shops','shops._id','=','favourites.type_id')			  
		->select('shops.logo','shops._id')
		->get();
		$getRecords = [];
		$i=0;
		foreach($allRecords as $records) {
			$getRecords[$i]['store_id'] = $records->_id;
			if($records->logo) {
				$getRecords[$i]['store_logo'] = url('/')."/images/shops/thumbnails/".$records->logo;
			} else {
				$getRecords[$i]['store_logo'] = url('/')."/images/no-image.jpg";
			}
			$i++;
		}
		return $getRecords;
	}

	public function addVisitCount(Request $request){
		$token = $request->token;
		if( $request->visit_count==0 || !$request->has('visit_count') ){
			$responseData = [
				'code' => 500,
				'content' => ['message' => 'visit_count can not zero']
			];	
		}else{

			$updatedData['visit_count'] = $request->visit_count;
			$updatedData['points'] = $request->visit_count*7;
			SiteUsers::where(['token' => $token])->update($updatedData);
			$responseData = [
				'code' => 200,
				'content' => ['message' => 'success']
			];
		}

		return response()->json($responseData);	
	}
}
