<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use JWTAuth;
use JWTAuthException;
use App\Offer;
use App\UserJoinOffer;
use App\SiteUsers;

class OffersController extends Controller
{
	public function getOffer(Request $request){
		if($request->has('no_record')) {
			$this->paginate_total = $request->no_record;
		}
		$token = $request->token;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;
		$allRecords	= Offer::withTrashed()
		->join('shops as s', 's._id', '=', 'offers.shop_id')
		->select('offers.*')
		->with('shop')
		->get();
		//->paginate($this->paginate_total);
		$getRecords = [];
		$i=0;
		foreach($allRecords as $records){
			$end = strtotime($records->end_date); 
			$endDate = date('Y-m-d', $end);
			$currentDate = date('Y-m-d', time());
            if($currentDate > $endDate){

			}else{ 
				if($records->image) {
					$getRecords[$i]['image'] = url('/')."/images/offers/".$records->image;
				} else {
					$getRecords[$i]['image'] = url('/')."/images/no-image.jpg";
				}
				$getRecords[$i]['id'] = $records->_id;
				$getRecords[$i]['title'] = $records->title;
				$getRecords[$i]['sub_title'] = $records->sub_title;
				$getRecords[$i]['details'] = $records->details;
				$getRecords[$i]['shop_id'] = $records->shop_id;
				$getRecords[$i]['start_date'] = $records->start_date;
				$getRecords[$i]['end_date'] = $records->end_date;
				$getRecords[$i]['join_status'] = ($this->checkJoinStatus($user_id,$records->_id))? 'yes': 'no';
				$i++;
		    }
		}
		$sendData['allOffers'] = $getRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

	public function getOfferDetail(Request $request) {
		$offer_id = $request->offer_id;
		$token = $request->token;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;
		$getRecord = Offer::where(['_id' => $offer_id])
							->select('offers._id as id','offers.title','offers.shop_id','offers.sub_title','offers.image','offers.details', 'offers.start_date','offers.end_date')
							->first();
		$getRecords = [];					
		if($getRecord['image']) {
			$getRecords['image'] = url('/')."/images/offers/".$getRecord['image'];
		} else {
			$getRecords['image'] = url('/')."/images/no-image.jpg";
		}
		$getRecords['id'] = $getRecord['id'];
		$getRecords['title'] = $getRecord['title'];
		$getRecords['sub_title'] = $getRecord['sub_title'];
		$getRecords['details'] = $getRecord['details'];
		$getRecords['shop_id'] = $getRecord['shop_id'];
		$getRecords['start_date'] = $getRecord['start_date'];
		$getRecords['end_date'] = $getRecord['end_date'];
		$getRecords['join_status'] = ($this->checkJoinStatus($user_id,$getRecord['id']))? 'yes': 'no';
		$sendData['offer_detail'] = $getRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

	public function checkJoinStatus($user_id,$offer_id) {
		return UserJoinOffer::where(['user_id' => $user_id, 'offer_id' => $offer_id])->count();
	}
	
	public function userJoinOffer(Request $request) {
		$token = $request->token;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;
		$offer_id = $request->offer_id;
		$flag = $request->join_status; // yes/no 
		if($flag == "yes") {
			UserJoinOffer::updateOrCreate(['user_id' => $user_id,'offer_id' => $offer_id],['user_id' => $user_id,'offer_id' => $offer_id]);
		} else if($flag == "no") {
			UserJoinOffer::where(['user_id' => $user_id,'offer_id' => $offer_id])->delete();
		}
		$responseData = [
			'code' => 200,
			'content' => ['message' => 'success']
		];
		return response()->json($responseData);
	}
}
