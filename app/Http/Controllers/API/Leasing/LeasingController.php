<?php

namespace App\Http\Controllers\API\Leasing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Leasing\LeasingEnquiry;
use App\SiteUsers;

class LeasingController extends Controller
{
    public function enquiryform(Request $request) {
		$token = $request->token;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;

		$brand = $request->brand;
		$company = $request->company;
		$contact_person_name = $request->contact_person_name;
		$contact_person_designation = $request->contact_person_designation;
		$contact_person_address = $request->contact_person_address;
		$contact_person_mobile = $request->contact_person_mobile;
		$contact_person_email = $request->contact_person_email;
		$contact_person_website = $request->contact_person_website;
		$category = $request->category;
		$concept = $request->concept;
		$existing_number_stores = $request->existing_number_stores;

		LeasingEnquiry::create([
			'user_id' => $user_id,
			'brand' => $brand,
			'company' => $company,
			'contact_person_name' => $contact_person_name,
			'contact_person_designation' => $contact_person_designation,
			'contact_person_address' => $contact_person_address,
			'contact_person_mobile' => $contact_person_mobile,
			'contact_person_email' => $contact_person_email,
			'contact_person_website' => $contact_person_website,
			'category' => $category,
			'concept' => $concept,
			'existing_number_stores' => $existing_number_stores
		]);
		$responseData = [
			'code' => 200,
			'content' => ['message' => 'success']
		];
		return response()->json($responseData);
	}

	function getFloorPlan(){
		$DataRows = \App\FloorPlans::all();
		$i=0;
		$floorPlansArr=array();
		foreach ($DataRows as $row) {
    		$floorPlansArr[$i]['id']=$row->id;
    		$floorPlansArr[$i]['title']=$row->title;
    		if($row->document) {
				$floorPlansArr[$i]['document'] = url('/')."/images/floorplan/".$row->document;
			} else {
				$floorPlansArr[$i]['document'] = '';
			}
    		$i++;
		}
		$sendData['FloorPlans']=$floorPlansArr;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}
}
