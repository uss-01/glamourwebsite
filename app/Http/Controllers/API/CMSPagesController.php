<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;
use App\Cms;

class CMSPagesController extends Controller
{
    public function getMallInfo() {
		 $allRecords = Cms::select('slug','name','title','image','content')
					->get();
		$getRecords = [];
		$i=0;
		foreach($allRecords as $records) {
			$getRecords[$i]['slug'] = $records->slug;
			$getRecords[$i]['name'] = $records->name;
			$getRecords[$i]['title'] = $records->title;
			//$getRecords[$i]['content'] = $records->content;
			if($records->image != 'NULL') {
				$getRecords[$i]['banner_image'] = url('/')."/images/cms/".$records->image;
			}
			$i++;
		}
		$settings = Setting::select('slug','value','image')->get();
		$getMallRecords = [];
		$j=0;
		foreach($settings as $setting) { 
			$slug = $setting->slug;
			$content = $setting->value;
			$image = $setting->image;
			$getMallRecords[$j]['slug'] = $slug;
			$getMallRecords[$j]['banner_image'] = url('/')."/images/cms/".$image;
			$getMallRecords[$j]['content'] = $content;
			$j++;
		}
		$sendData['mallInfo'] = $getMallRecords;
		$sendData['pages'] = $getRecords;
		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}

	public function homePageImages() {

		$allRecords = \App\Masters\SliderImages::where(['slug' => 'homePage'])
		                   ->get();

		
		$getRecords = [];

			$i=0;
			foreach($allRecords as $records) {
				if($records->image != 'NULL') {
					$getRecords[$i]['image'] = url('/')."/images/cms/".$records->image;
				} else {
					$getRecords[$i]['image'] = url('/')."/images/no-image.jpg";
				}
				
				$getRecords[$i]['image_title'] = $records->slug;
				$getRecords[$i]['redirect_to'] = $records->redirect_to;
				$getRecords[$i]['rdirect_id'] = $records->rdirect_id;
				
				$i++;
			}
		

		$sendData['homePageImages'] = $getRecords;

		$responseData = [
			'code' => 200,
			'content' => $sendData
		];
		return response()->json($responseData);
	}
}
