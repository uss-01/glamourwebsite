<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\User;
use App\Tenant;
use App\Tenantadmin;
use App\Leasing;
use App\TenantComplaints;
use App\Shop\Shop;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;

class TenantController extends Controller
{
    protected $breadcrumb_master = 'Tenant';
    protected $view_master = 'tenant.';
    protected $route_master = 'tenant.';
    protected $pageTitle = 'Tenant';
    protected $activeNav = [
        'nav' => 'tenant',
        'sub' => 'tenant'

    ];
    protected $sortable = [
        'id' => 'id',
        'title' => 'name',
        'start_date' => 'start_date',
        'end_date' => 'end_date'
    ];

    public function __construct(){
       $this->bladeVar['page']['title'] = $this->pageTitle;
       $this->bladeVar['page']['activeNav'] = $this->activeNav;
       $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
       $this->cache();
    }

    protected function cache(){
        $time = Carbon::now()->addHours(1);
        $this->bladeVar['shops'] = Cache::remember('shops', $time, function () {
            return Shop::orderBy('name', 'asc')->get();
        });
    }

    public function index(Request $request){
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = Tenant::withTrashed()
                                            ->select('tenants.*')
                                            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
                                            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Tenant::withTrashed()->count();
        $this->bladeVar['complaints_count'] = TenantComplaints::select('tenant_complaints.*')->get();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    public function search(Request $request){
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Tenant::withTrashed()
            ->where('name', 'like', '%'.$request->q.'%')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Tenant::withTrashed()
            ->where('name', 'like', '%'.$request->q.'%')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    public function create(Request $request){
        if ($request->ajax()) {
            $this->bladeVar['shopData'] = Shop::withTrashed()
                    ->join('master_category as c', 'c.category_id', '=', 'shops.category_id')
                    ->join('master_store as s', 's.store_id', '=', 'shops.store_id')
                    ->where('shops.category_id','=','1')
                    ->select('shops.*')
                    ->with('category')
                    ->with('store')
                    ->orderBy('name', 'asc')
                    ->groupBy('shops._id')
                    ->get();
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);        
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    public function store(Request $request){
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|min:6|string|required_with:password|same:password',
        ]);
        if ($validator->passes()) {
            $image_title = str_slug($request->phone, '-');
            $input['image'] = time().'_'.$image_title.'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('images/cms'), $input['image']);

            $shop_name = array();
            for($i=0; $i < count($request->shop_name); $i++){
            $shop_name[$i]['id'] = $request->shop_name[$i];
            }
            $shopName = json_encode($shop_name);

            $shopArray = array();
            for($i=0; $i < count($request->shop_name); $i++){
                $shop = Shop::find($request->shop_name[$i]);  
                $shopArray[$i]['_id'] = $request->shop_name[$i];   
                $shopArray[$i]['name'] = $shop->name;
            }
            $shopNameArray = json_encode($shopArray);

            $tenant = Tenant::create([
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'address' => $request->address,
                'start_date' => $request->start_date_alt,
                'end_date' => $request->end_date_alt,
                'photo_doc'=> $input['image'],
                'details' => $request->details,
                'shop_name' => $shopNameArray,
                'created_by'=>Auth::id(),
                'created_by'=>NULL,
            ]);
            $tenantUID = getTenantUID($tenant->id);
            $tenantObj = Tenant::find($tenant->id);
            $tenantObj->update([
                'uid' => $tenantUID,
            ]);

            $userData = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'type' => 'seller',
                'tenant_id' => $tenant->id,
                'shop_name' => $shopName,
            ]);

            $tenantData = Tenantadmin::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => md5($request->password),
                'type' => 'seller',
                'tenant_id' => $tenant->id,
                'shop_name' => $shopName,
            ]);

            $name = $request->name;
            $email = $request->email;
            $title = 'You have successfully Created in Admin';
            $content = 'admin sent an email to '.$request->name.' successfully!';
            //$sendMail = $this->sendMail($name,$email,$title,$content);

            return response()->json([
                'success' => '<strong>Success!</strong> data saved for offer <strong>'.$request->name.'</strong><a href="'.route($this->route_master.'show', $tenant->id).'" class="btn btn-sm btn-info" title="View this offer" data-toggle="modal" data-target="#viewModal"> Click here to view it</a>'
            ]);
        }
        return response()->json(['error'=>$validator->errors()->getMessages()]);
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id){
        if ($request->ajax()) {
            $this->bladeVar['result'] = Tenant::withTrashed()->find($id);
            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id){
        if ($request->ajax()) {
            $this->bladeVar['result'] = Tenant::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $input = $request->all();
        $validator = Validator::make($request->all(), [
           'name' => 'required',
           'phone' => 'required',
           'email' => 'required',
           'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $shopArray = array();
        for($i=0; $i < count($request->shop_name); $i++){
            $shop = Shop::find($request->shop_name[$i]);  
            $shopArray[$i]['_id'] = $request->shop_name[$i];   
            $shopArray[$i]['name'] = $shop->name;
        }
        $shopNameArray = json_encode($shopArray);

        if(!empty($request->password)){
            $userDataObj = User::where('tenant_id',$id)->first();
            $userDataObj->update([
                'password' => bcrypt($request->password),
            ]);
        }

        if(isset($request->image)) {
            if($validator->passes()){
                $image_title = str_slug($request->phone, '-');
                $input['image'] = time().'_'.$image_title.'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('images/cms'), $input['image']);
                $tenant = Tenant::find($id);
                File::delete(public_path().'/images/cms/'.$tenant->image);
                $tenant->update([
                    'name' => $request->name,
                    'phone' => $request->phone,
                    'email' => $request->email,
                    'address' => $request->address,
                    'details' => $request->details,
                    'photo_doc'=> $input['image'],
                    'start_date' => $request->start_date_alt,
                    'end_date' => $request->end_date_alt,
                    'shop_name' => $shopNameArray,   
                    'updated_by'=>Auth::id(),
                ]);
                return response()->json([
                    'success' => '<strong>Success!</strong> Data updated for Tenant <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $tenant->id).'" class="btn btn-sm btn-info" title="View Tenant" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
                ]);
            } 
            return response()->json(['error'=>$validator->errors()->getMessages()]);
        }else{
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'phone' => 'required',
            ]);
            if($validator->passes()){
                $tenant = Tenant::find($id);
                $tenant->update([
                    'name' => $request->name,
                    'phone' => $request->phone,
                    'email' => $request->email,
                    'address' => $request->address,
                    'details' => $request->details,
                    'start_date' => $request->start_date_alt,
                    'end_date' => $request->end_date_alt,
                    'shop_name' => $shopNameArray,   
                    'updated_by'=>Auth::id(),
                ]);
                return response()->json([
                    'success' => '<strong>Success!</strong> Data updated for Tenant <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $tenant->id).'" class="btn btn-sm btn-info" title="View Tenant" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
                ]);
            }
            return response()->json(['error'=>$validator->errors()->getMessages()]);
        }
    }

    public function complaints(Request $request, $id){
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '/tenant';
        $this->bladeVar['complaintsData'] = TenantComplaints::where(['tenant_id' => $id])
                                            ->select('tenant_complaints.*')
                                            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
                                            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = count($this->bladeVar['complaintsData']);
        return view($this->view_master.'complaints', ['bladeVar' => $this->bladeVar]);
    }

    public function editComplaint(Request $request, $id){
        if ($request->ajax()) {
            $this->bladeVar['result'] = TenantComplaints::find($id);
            return view($this->view_master.'editComplaint', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    public function updateComplaint(Request $request, $id){
        $tenantComplaints = TenantComplaints::find($id);
        $tenantComplaints->update([
            'reply' => $request->reply,
            'status' => 'read',
            'admin_id' => Auth::id(),
        ]);
        return response()->json([
            'success' => '<strong>Message sent successfully</strong>'
        ]);
    }

    public function delete($id){
        DB::delete('delete from users where tenant_id = ?',[$id]);
        Tenant::withTrashed()->find($id)->forceDelete();
        return response()->json([
            'success' => '<strong>Success!</strong> Tenant has been deleted permanently!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        Tenant::find($id)->delete();
        return response()->json([
            'success' => '<strong>Success!</strong> Tenant has been deleted temporarily!'
        ]);
    }

    public function restore($id){
        Tenant::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> Tenant has been restored!'
        ]);
    }

    public function sendMail($name,$email,$title,$content){
        $sendMailData = Mail::send('emails.tenant_email', ['name' => $name, 'email' => $email, 'title' => $title, 'content' => $content], function ($message) {
            $message->to('nikunj@rapidarts.com')->subject('send mail');
        });
        return $sendMailData;
    }
}
