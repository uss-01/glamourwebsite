<?php

namespace App\Http\Controllers\Message;

use App\Message\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SiteUsers;
use Illuminate\Support\Facades\File;
use Validator;

class MessageController extends Controller
{

	protected $sortable = [
	   'id' => '_id',
	   'message' => 'message',
	   'created_at' => 'created_at'
	 ];

 protected $validation_rules = [
  'subject' => 'required',
  'message' => 'required',
];

protected $pageTitle = 'Push Notification';
protected $activeNav = [
  'nav' => 'message',
  'sub' => 'push notificaton'
];
protected $breadcrumb_master = 'Push Notification';
protected $view_master = 'message.pushnotification.';
protected $route_master = 'message.';

public function __construct()
{

 $this->bladeVar['page']['title'] = $this->pageTitle;
 $this->bladeVar['page']['activeNav'] = $this->activeNav;
 $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
 $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function index(Request $request)
  {
    $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
    $this->bladeVar['results'] = Message::where(['type' => 'offers'])
    ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'DESC'))
    ->groupBy('message')
    ->paginate($this->paginate_total);
    $this->bladeVar['total_count'] = Message::all()->count();
    return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
  }
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Message::where(['type' => 'offers'])
            ->where('message', 'LIKE', '%'.$request->q.'%')
			->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'DESC'))
			->paginate($this->paginate_total);
			$this->bladeVar['total_count'] = Message::all()->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      if ($request->ajax()) {
        return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
      } else {
        return redirect(route($this->route_master.'index'));
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = $request->all();
      $validator = Validator::make($request->all(), [
        'subject' => 'required',
        'message' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);
      if ($validator->passes()) {
        $image_name = str_replace(' ', '-',time());
        $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/cms'), $input['image']);
        $pushData['subject'] = $request->subject;
        $pushData['message'] = $request->message;

        if($request->age == 'all'){ }else{ $arr = explode("-", $request->age); $from = $arr[0]; $to = $arr[1]; }
        if($request->member == 'all'){ }else{ $wheremembership['site_users.membership'] = $request->member; }
        if($request->gender == 'all'){ }else{ $wheregender['site_users.gender'] = $request->gender; }        
       
        if(!empty($from) && !empty($to) && !empty($wheremembership) && !empty($wheregender) && ($request->age != 'all') && ($request->member != 'all') && ($request->gender != 'all')){
          $fetchAllUser = SiteUsers::where($wheremembership)
                                    ->where($wheregender)
                                    ->whereBetween('site_users.age', [$from, $to])
                                    ->select('site_users._id','site_users.push_token','site_users.fname','site_users.device')
                                    ->get();
        }else if(($request->age == 'all') && ($request->member != 'all') && ($request->gender == 'all')){
          $fetchAllUser = SiteUsers::where($wheremembership)
                                    ->select('site_users._id','site_users.push_token','site_users.fname','site_users.device')
                                    ->get();
        }else if(($request->age == 'all') && ($request->member == 'all') && ($request->gender != 'all')){
          $fetchAllUser = SiteUsers::where($wheregender)
                                    ->select('site_users._id','site_users.push_token','site_users.fname','site_users.device')
                                    ->get();
        }else if(($request->age != 'all') && ($request->member == 'all') && ($request->gender == 'all')){
          $fetchAllUser = SiteUsers::whereBetween('site_users.age', [$from, $to])
                                    ->select('site_users._id','site_users.push_token','site_users.fname','site_users.device')
                                    ->get();
        }else if(($request->age == 'all') && ($request->member != 'all') && ($request->gender != 'all')){
          $fetchAllUser = SiteUsers::where($wheremembership)
                                    ->where($wheregender)
                                    ->select('site_users._id','site_users.push_token','site_users.fname','site_users.device')
                                    ->get();
        }else if(($request->age != 'all') && ($request->member != 'all') && ($request->gender == 'all')){
          $fetchAllUser = SiteUsers::where($wheremembership)
                                    ->whereBetween('site_users.age', [$from, $to])
                                    ->select('site_users._id','site_users.push_token','site_users.fname','site_users.device')
                                    ->get();
        }else if(($request->age != 'all') && ($request->member == 'all') && ($request->gender != 'all')){
          $fetchAllUser = SiteUsers::where($wheregender)
                                    ->whereBetween('site_users.age', [$from, $to])
                                    ->select('site_users._id','site_users.push_token','site_users.fname','site_users.device')
                                    ->get();
        }else if(($request->age == 'all') && ($request->member == 'all') && ($request->gender == 'all')){
          $fetchAllUser = SiteUsers::select('site_users._id','site_users.push_token','site_users.fname','site_users.device')
                                    ->get();
        }else{
          $fetchAllUser = SiteUsers::all(['_id','push_token','fname','device']);
        }
        
        $pushData['image']=url('/')."/images/cms/".$input['image'];
        if( $request->has('redirect_to') && $request->has('redirect_id') ){
          $pushData['redirect_to']=($request->redirect_to == 'other' ? $request->redirect_to_name : $request->redirect_to);
          $pushData['redirect_to_name']=($request->redirect_to == 'other' ? $request->redirect_to : '');
          $pushData['redirect_id']=$request->redirect_id;
        }else{
          $pushData['redirect_to']='';
          $pushData['redirect_id']=0;
        }
          if(!empty($fetchAllUser)){ 
            foreach($fetchAllUser as $users) { 
              /*if(!empty($users->push_token) && ($users->_id == '78')){
                PushNotificationIos($users->push_token, $pushData);
              }*/
              if(!empty($users->push_token) && ($users->push_token != 'Fake_Push_Notification_Token')) {
                  if('ios'==$users->device){
                    PushNotificationIos($users->push_token, $pushData);
                  }else{
                    //$this->pushMessageNew('AIzaSyDqsRf-uFOBbFvuNwxPaS-jHwlBgwRQkVw',$users->push_token,$pushData);
                    $this->pushMessageNew('AIzaSyA-XHkiEROl9rGoPY3gvGRU6igyetusj3E',$users->push_token,$pushData);
                  }
                  Message::create([
                    'from_user_id' => \Auth::id(),
                    'to_user_id' => $users->_id,
                    'subject' => $pushData['subject'],
                    'message' => $pushData['message'],
                    'image' => $pushData['image'],
                    'redirect_to' => $pushData['redirect_to'],
                    'redirect_id' => $pushData['redirect_id']
                  ]);
              }
            }
            return response()->json([
              'success' => '<strong>Success!</strong> data saved for category <strong>'.$request->subject.'</strong> '
            ]);
          }
         }
         return response()->json(['error'=>$validator->errors()->getMessages()]);
       }

       public function pushMessageNew($api_access_key,$device_id,$pushData) {
        //$api_access_key = $api_access_key;
        $msg = array
        (
          'body' => $pushData['message'],
          'title' => $pushData['subject'],
          'icon' => 'myicon',/*Default Icon*/
          'sound' => 'mySound'/*Default sound*/
        );
        $data=array(
          'redirect_to' => $pushData['redirect_to'],
          'redirect_id' => $pushData['redirect_id'],
          'vibrate' => 1,
        );
        $fields = array
        (
          'to' => $device_id,
          'mutable-content' => 1, 
          'notification' => $msg,
          'data' => $data,
        );
        $headers = array
        (
          'Authorization: key=' . $api_access_key,
          'Content-Type: application/json'
        );
        #Send Reponse To FireBase Server    
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
      }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Message\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }

    public function pushMessage($api_access_key,$device_id,$subject,$message) {
      $api_access_key = $api_access_key;
      $msg = array
      ( 
        'body'    => $message,
        'title'   => $subject,
        'icon'	=> 'myicon',/*Default Icon*/
        'sound' => 'mySound'/*Default sound*/
      );
      $fields = array
      (
        'to'		=> $device_id,
        'notification'	=> $msg
      );
      $headers = array
      (
        'Authorization: key=' . $api_access_key,
        'Content-Type: application/json'
      );
      #Send Reponse To FireBase Server	
      $ch = curl_init();
      curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
      curl_setopt( $ch,CURLOPT_POST, true );
      curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
      curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
      curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
      curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
      $result = curl_exec($ch );
      curl_close( $ch );
    }
  }
