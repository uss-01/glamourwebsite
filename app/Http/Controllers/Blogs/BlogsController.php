<?php

namespace App\Http\Controllers\Blogs;

use Auth;
use DB;
use App\Blogs\Blogs;
use App\Blogs\BlogsPics;
use App\Masters\BlogCategory;
use App\Masters\Tags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use Validator;
use Illuminate\Support\Facades\File;
use Image;

class BlogsController extends Controller
{
    protected $sortable = [
        'id' => '_id',
        'name' => 'name',
        'blog_category' => 'pc.name',
    ];
    protected $validation_rules = [
        'name' => 'required|max:255|unique:blogs',
        'blog_category' => 'required|integer',  
    ];
    protected $pageTitle = 'blogs';
    protected $activeNav = [
        'nav' => 'shop',
        'sub' => 'blogs'
    ];
    protected $breadcrumb_master = 'blogs';
    protected $view_master = 'blogs.';
    protected $route_master = 'blogs.';

    public function __construct()
    {
         $this->bladeVar['page']['title'] = $this->pageTitle;
         $this->bladeVar['page']['activeNav'] = $this->activeNav;
         $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
         $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
         $this->cache();
    }

    /**
     * Refresh cache and returns puts it in array
     */
    protected function cache() 
    {
        $time = Carbon::now()->addHours(1);
        $this->bladeVar['blog_categories'] = Cache::remember('blog_categories', $time, function () {
            return BlogCategory::orderBy('name', 'asc')->get();
        });

        $this->bladeVar['blog_tags'] = Cache::remember('blog_tags', $time, function () {
            return Tags::orderBy('name', 'asc')->get();
        });
         
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = Blogs::withTrashed()
            ->join('blog_categories as bc', 'bc._id', '=', 'blogs.blog_category_id')
            ->leftJoin('blog_tags as bt', 'bt.id', '=', 'blogs.blog_tag_id')
            ->select('blogs.*')
            ->with('blog_category')
            ->with('blog_tag')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
			->groupBy('blogs._id')
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Blogs::withTrashed()
            ->join('blog_categories as bc', 'bc._id', '=', 'blogs.blog_category_id')
            ->leftJoin('blog_tags as bt', 'bt.id', '=', 'blogs.blog_tag_id')
            ->select('blogs.*')
            ->with('blog_category')
            ->with('blog_tag')->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    { 
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }

        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Blogs::withTrashed()
             ->where('blogs.name', 'LIKE', '%'.$request->q.'%')
            ->join('blog_categories as bc', 'bc._id', '=', 'blogs.blog_category_id')
            ->leftJoin('blog_tags as bt', 'bt.id', '=', 'blogs.blog_tag_id')
            ->select('blogs.*')
            ->with('blog_category')
            ->with('blog_tag')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
			->groupBy('blogs._id')
            ->paginate($this->paginate_total);
			
        $this->bladeVar['total_count'] = Blogs::withTrashed()
            ->where('blogs.name', 'LIKE', '%'.$request->q.'%')
            ->join('blog_categories as bc', 'bc._id', '=', 'blogs.blog_category_id')
            ->leftJoin('blog_tags as bt', 'bt.id', '=', 'blogs.blog_tag_id')
            ->with('blog_category')
            ->with('blog_tag')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            $this->bladeVar['blogStatus'] = array('1' => 'Draft','2' => 'Published');
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
        $input = $request->all();
        $this->validation_rules['name'] = 'required|max:255|unique:blogs'; 
        $this->validation_rules['image'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'; 
        $validator = Validator::make($request->all(),$this->validation_rules);
        if ($validator->passes()) {
            if($request->image){
                list($width, $height) = getimagesize($request->image);
                if($width != 1110 && $height != 800){
                    $error_msg=[
                    'image' => 'Size Should be 1110X800',
                    ];
                    return response()->json(['error'=>$error_msg]);
                }
            }
            $image_name = str_replace(' ', '-',strtolower($request->name));
            $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('images/blogs'), $input['image']);
            
            $img_thumb = Image::make(public_path('images/blogs').'/'.$input['image']);
            $img_thumb->resize(470, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('images/blogs/thumbnails').'/'.$input['image']);

            $img_thumb->resize(460, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('images/blogs/thumbnails').'/460x460_'.$input['image']);

            $img_thumb->resize(320, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('images/blogs/thumbnails').'/320x320_'.$input['image']);

            if($request->blog_category == NULL){
               $request->blog_category = 0; 
            }
            if($request->blog_tag == NULL){
               $request->blog_tag = 0; 
            }
            
            if($request->blog_audio != 1){
                $request->blog_audio = 0;
            }

            if($request->blog_video != 1){
                $request->blog_video = 0;
            }

            if($request->blog_image_slide != 1){
                $request->blog_image_slide = 0;
            }

            if(!empty($request->audio_url)){
                $audio_url = $request->audio_url;
            }else{
                $audio_url = '';
            }
            if(!empty($request->video_url)){
                $video_url = $request->video_url;
            }else{
                $video_url = '';
            }
            if(!empty($request->blog_status)){
                $blog_status = $request->blog_status;
            }else{
                $blog_status = 'Draft';
            }
            if(!empty($request->details)){
                $details = $request->details;
            }else{
                $details = '';
            }

            $blog = Blogs::create([
                'name' => $request->name,
                'slug' => $this->removeSpecialChapr($request->name),
                'blog_category_id' => $request->blog_category,
                'blog_tag_id' => $request->blog_tag,
                'blog_audio' => $request->blog_audio,
                'blog_video' => $request->blog_video,
                'blog_image_slide' => $request->blog_image_slide,
                'video_url' => $video_url,
                'audio_url' => $audio_url,
                'blog_status' => $blog_status,
                'image'=> $input['image'],
                'details'=> $details,
                'created_by'=>Auth::id(),
            ]);
            return response()->json([
                'success' => '<strong>Success!</strong> data saved for blog <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $blog->_id).'" class="btn btn-sm btn-info" title="View this blog" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
            ]);
        }
        return response()->json(['error'=>$validator->errors()->getMessages()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Blogs::withTrashed()->find($id);
            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $product = Blogs::find($id);
            $this->bladeVar['result'] = $product;
            $this->bladeVar['blogStatus'] = array('1' => 'Draft','2' => 'Published');
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $this->validation_rules['name'] = 'required|max:255';  
        if (isset($request->image)) {
            $this->validation_rules['image'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'; 
            $validator = Validator::make($request->all(),$this->validation_rules);
            if($validator->passes()){       
                if($request->image){
                    list($width, $height) = getimagesize($request->image);
                    if($width != 1110 && $height != 800){
                        $error_msg=[
                        'image' => 'Size Should be 1110X800',
                        ];
                        return response()->json(['error'=>$error_msg]);
                    }
                }
                $image_name = str_replace(' ', '-',strtolower($request->name));
                $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('images/blogs'), $input['image']);
                $img_thumb = Image::make(public_path('images/blogs').'/'.$input['image']);
                $img_thumb->resize(470, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path('images/blogs/thumbnails').'/'.$input['image']);

                $img_thumb->resize(460, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path('images/blogs/thumbnails').'/460x460_'.$input['image']);

                $img_thumb->resize(320, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path('images/blogs/thumbnails').'/320x320_'.$input['image']);

                $product = Blogs::find($id);
                File::delete(public_path().'/images/blogs/'.$product->image);
                File::delete(public_path().'/images/blogs/thumbnails/'.$product->image);
                File::delete(public_path().'/images/blogs/thumbnails/460x460_'.$product->image);
                File::delete(public_path().'/images/blogs/thumbnails/320x320_'.$product->image);

                if($request->blog_category == NULL){
                $request->blog_category = 0; 
                }
                if($request->blog_tag == NULL){
                $request->blog_tag = 0; 
                }
                
                if($request->blog_audio != 1){
                    $request->blog_audio = 0;
                }

                if($request->blog_video != 1){
                    $request->blog_video = 0;
                }

                if($request->blog_image_slide != 1){
                    $request->blog_image_slide = 0;
                }

                if(!empty($request->audio_url)){
                    $audio_url = $request->audio_url;
                }else{
                    $audio_url = '';
                }
                if(!empty($request->video_url)){
                    $video_url = $request->video_url;
                }else{
                    $video_url = '';
                }
                if(!empty($request->blog_status)){
                    $blog_status = $request->blog_status;
                }else{
                    $blog_status = 'Draft';
                }
                if(!empty($request->details)){
                    $details = $request->details;
                }else{
                    $details = '';
                }

                $product->update([
                    'name' => $request->name,
                    'slug' => $this->removeSpecialChapr($request->name),
                    'blog_category_id' => $request->blog_category,
                    'blog_tag_id' => $request->blog_tag,
                    'blog_audio' => $request->blog_audio,
                    'blog_video' => $request->blog_video,
                    'blog_image_slide' => $request->blog_image_slide,
                    'video_url' => $video_url,
                    'audio_url' => $audio_url,
                    'blog_status' => $blog_status,
                    'image'=> $input['image'],
                    'details'=> $details,
                    'updated_by'=>Auth::id(),
                ]);
                return response()->json([
                    'success' => '<strong>Success!</strong> Data updated for blog <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $product->_id).'" class="btn btn-sm btn-info" title="View this blog" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
                ]);
            } 
            return response()->json(['error'=>$validator->errors()->getMessages()]);
        }else{
            $validator = Validator::make($request->all(),$this->validation_rules);
            if($validator->passes()){

                if($request->blog_category == NULL){
                    $request->blog_category = 0; 
                }
                if($request->blog_tag == NULL){
                    $request->blog_tag = 0; 
                }
                
                if($request->blog_audio != 1){
                    $request->blog_audio = 0;
                }

                if($request->blog_video != 1){
                    $request->blog_video = 0;
                }

                if($request->blog_image_slide != 1){
                    $request->blog_image_slide = 0;
                }

                if(!empty($request->audio_url)){
                    $audio_url = $request->audio_url;
                }else{
                    $audio_url = '';
                }
                if(!empty($request->video_url)){
                    $video_url = $request->video_url;
                }else{
                    $video_url = '';
                }
                if(!empty($request->blog_status)){
                    $blog_status = $request->blog_status;
                }else{
                    $blog_status = 'Draft';
                }
                if(!empty($request->details)){
                    $details = $request->details;
                }else{
                    $details = '';
                }

                $product = Blogs::find($id);
                $product->update([
                    'name' => $request->name,
                    'slug' => $this->removeSpecialChapr($request->name),
                    'blog_category_id' => $request->blog_category,
                    'blog_tag_id' => $request->blog_tag,
                    'blog_audio' => $request->blog_audio,
                    'blog_video' => $request->blog_video,
                    'blog_image_slide' => $request->blog_image_slide,
                    'video_url' => $video_url,
                    'audio_url' => $audio_url,
                    'blog_status' => $blog_status,
                    'details'=> $details,
                    'updated_by'=>Auth::id(),
                ]);

                return response()->json([
                    'success' => '<strong>Success!</strong> Data updated for blog <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $product->_id).'" class="btn btn-sm btn-info" title="View this blog" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
                ]);
            }
            return response()->json(['error'=>$validator->errors()->getMessages()]);
        }  
    }

    // For Blog Pics
    public function pics(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Blogs::withTrashed()->find($id);
            $this->bladeVar['blogPics'] = BlogsPics::where(['blog_pics.blog_id' => $id])
                ->select('blog_pics._id','blog_pics.image')
                ->get();
            return view($this->view_master.'pics', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    public function savePics(Request $request)
    {
        
        $input = $request->all();
        $rules = [];
        $photo_cnt = count($request->image);
        foreach(range(0, ($photo_cnt-1)) as $index) {
            $rules['image.' . $index] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048';
        }
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            $cnt = 1;
        foreach ($request->image as $photo) {
         list($width, $height) = getimagesize($photo);
            $image_fullname = time().'_'.$cnt.'.'.$photo->getClientOriginalExtension();
            $photo->move(public_path('images/blogs'), $image_fullname);
            $photo = BlogsPics::create([
                'blog_id' => $request->blog,
                'image'=> $image_fullname,
                'created_by'=>Auth::id(),
            ]);
            $cnt++;
        }
        return response()->json([
                'success' => '<strong>Success!</strong> data saved for photo <strong>'.$request->title.'</strong>'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Blogs::find($id)->delete();
        return response()->json([
            'success' => '<strong>Success!</strong> Blog has been deleted temporarily!'
        ]);
    }

    public function deletePic($id)
    {
        $pics=BlogsPics::find($id);
        File::delete(public_path().'/images/blogs/'.$pics->image);
        $pics->delete();
       
        return response()->json([
            'success' => '<strong>Success!</strong> Blog Pic has been deleted permanently!'
        ]);
    }

    /**
     * Mark as deleted the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $product = Blogs::withTrashed()->find($id);
        File::delete(public_path().'/images/blogs/'.$product->image);
        $product->forceDelete();
        return response()->json([
            'success' => '<strong>Success!</strong> Blog has been deleted permanently!'
        ]);
    }

    /**
     * Restores deleted marked resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        Blogs::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> Blog has been restored!'
        ]);
    }

    public function removeSpecialChapr($value){
		$title = str_replace( array( '\'', '"', ',' , ';', '<', '>','!', '@', '#' , '$', '%', '^', '&', '*' , '(', ')', '_', '-', '=' , '+', ':', '?', '.', '`', '~', '[', ']', '{', '}', '|' , '/' , '\\' , '‘' , '’' , '“', '”' , '…', '‰' ), '', $value);
		$post_title1 = str_replace( array("  "), array(" "), $title);	
		$post_title = str_replace( array(" ","'"), array("-",""), $post_title1);
		$postTitle = strtolower($post_title);
		return $postTitle;
	}
}
