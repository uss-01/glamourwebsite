<?php

namespace App\Http\Controllers\Coupons;

use Auth;
use Validator;
use DB;
use App\Coupons\Coupons;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class CouponsController extends Controller
{

    protected $sortable = [
        'id' => 'id',
        'name' => 'name',
    ];
    protected $validation_rules = [
        'name' => 'required|max:255|unique:coupons',
        'slug' => 'required|max:255|unique:coupons',
    ];
    protected $pageTitle = 'Coupons';
    protected $activeNav = [
        'nav' => 'coupons',
        'sub' => 'coupons'
    ];
    protected $breadcrumb_master = 'Coupons';
    protected $view_master = 'coupons.';
    protected $route_master = 'coupons.';

    public function __construct()
    {
       $this->bladeVar['page']['title'] = $this->pageTitle;
       $this->bladeVar['page']['activeNav'] = $this->activeNav;
       $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
       $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = Coupons::withTrashed()
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->select('coupons.*')
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Coupons::withTrashed()->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Coupons::withTrashed()
            ->where('name', 'like', '%'.$request->q.'%')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Coupons::withTrashed()
            ->where('name', 'like', '%'.$request->q.'%')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            $this->bladeVar['couponType'] = array('flat' => 'Flat','percent' => 'Percentage (%)');
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:coupons',
        ]);
        if ($validator->passes()) {
            if(!empty($request->value)){
                $value = $request->value;
            }else{
                $value = '';  
            }
            if(!empty($request->quantity)){
                $qty = $request->quantity;
            }else{
                $qty = '';  
            }
            if(!empty($request->type)){
                $type = $request->type;
            }else{
                $type = '';  
            }
            if(!empty($request->expiry)){
                $expiry = $request->expiry;
            }else{
                $expiry = '';  
            }
            $coupon = Coupons::create([
                'name' => $request->name,
                'slug' => $this->removeSpecialChapr($request->name),
                'value' => $value,
                'quantity' => $qty,
                'type' => $type,
                'expiry' => $expiry,
                'created_by'=>Auth::id(),
            ]);
            return response()->json([
                'success' => '<strong>Success!</strong> data saved for coupon <strong>'.$request->name.'</strong><a href="'.route($this->route_master.'show', $coupon->id).'" class="btn btn-sm btn-info" title="View this coupon" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
            ]);
        }
        return response()->json(['error'=>$validator->errors()->getMessages()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Coupons::withTrashed()->find($id);
            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['couponType'] = array('flat' => 'Flat','percent' => 'Percentage (%)');
            $this->bladeVar['result'] = Coupons::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);
        if($validator->passes()){
            $coupon = Coupons::find($id);
            if(!empty($request->value)){
                $value = $request->value;
            }else{
                $value = '';  
            }
            if(!empty($request->quantity)){
                $qty = $request->quantity;
            }else{
                $qty = '';  
            }
            if(!empty($request->type)){
                $type = $request->type;
            }else{
                $type = '';  
            }
            if(!empty($request->expiry)){
                $expiry = $request->expiry;
            }else{
                $expiry = '';  
            }
            $coupon->update([
                'name' => $request->name,
                'slug' => $this->removeSpecialChapr($request->name),
                'value' => $value,
                'quantity' => $qty,
                'type' => $type,
                'expiry' => $expiry,
                'updated_by'=>Auth::id(),
            ]);
            return response()->json([
                'success' => '<strong>Success!</strong> Data updated for coupon <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $coupon->id).'" class="btn btn-sm btn-info" title="View this coupon" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
            ]);
        }
        return response()->json(['error'=>$validator->errors()->getMessages()]);
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Coupons::find($id)->delete();
        return response()->json([
            'success' => '<strong>Success!</strong> coupon has been deleted temporarily!'
        ]);
    }

    /**
     * Restores deleted marked resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        Coupons::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> coupon has been restored!'
        ]);
    }

    public function delete($id){
        $offer = Coupons::withTrashed()->find($id);
        $offer->forceDelete();
        return response()->json([
            'success' => '<strong>Success!</strong> coupon has been deleted permanently!'
        ]);
    }

    public function removeSpecialChapr($value){
		$title = str_replace( array( '\'', '"', ',' , ';', '<', '>','!', '@', '#' , '$', '%', '^', '&', '*' , '(', ')', '_', '-', '=' , '+', ':', '?', '.', '`', '~', '[', ']', '{', '}', '|' , '/' , '\\' , '‘' , '’' , '“', '”' , '…', '‰' ), '', $value);
		$post_title1 = str_replace( array("  "), array(" "), $title);	
		$post_title = str_replace( array(" ","'"), array("-",""), $post_title1);
		$postTitle = strtolower($post_title);
		return $postTitle;
	}
}