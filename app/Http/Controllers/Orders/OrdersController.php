<?php

namespace App\Http\Controllers\Orders;

use Auth;
use DB;
use App\Orders\Orders;
use App\Orderdetail\Orderdetail;
use App\Shop\Shop;
use App\Product\Product;
use App\User;
use App\Tenant;
use App\Product\ProductPics;
use App\Masters\ProductCategory;
use App\Masters\ProductSubcategory;
use App\Masters\Brand;
use App\SiteUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
     protected $sortable = [
        'id' => '_id',
        'name' => 'name',
    ];
    protected $pageTitle = 'Orders';
    protected $activeNav = [
        'nav' => 'orders',
        'sub' => 'orderdetails'
    ];
    protected $breadcrumb_master = 'Orders';
    protected $view_master = 'orders.';
    protected $route_master = 'orders.';

    public function __construct()
    {
         $this->bladeVar['page']['title'] = $this->pageTitle;
         $this->bladeVar['page']['activeNav'] = $this->activeNav;
         $this->bladeVar['page']['breadcrumb'] = ['Home' => '/', 'Orders' => '/orders'];
         $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = Orders::withTrashed()
            ->select('orders.*')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'DESC'))
            ->groupBy('orders._id')
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Orders::withTrashed()
            ->select('orders.*')
            ->select('orders.*')->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request){ 
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Orders::withTrashed()
            ->where('orders.firstname', 'LIKE', '%'.$request->q.'%')
            ->select('orders.*')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'DESC'))
            ->groupBy('orders._id')
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Orders::withTrashed()
           ->where('orders.firstname', 'LIKE', '%'.$request->q.'%')
           ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id){   
        $this->bladeVar['page']['breadcrumb'] = ['Home' => '/', 'Orders' => '/orders'];
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = ''; 
        $this->bladeVar['results'] = Orderdetail::withTrashed()
                                    ->where('order_details.order_id', $id)
                                    ->select('order_details.*')
                                    ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'DESC'))
                                    ->groupBy('order_details._id')
                                    ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Orderdetail::where(['order_id' => $id])
                                         ->count(); 
        $userdetail = Orders::withTrashed()->find($id);
        $this->bladeVar['userdetail'] = $userdetail;                                 
        $this->bladeVar['status'] = array('1' => 'Pending','2' => 'Accepted','3' => 'Processing','4' => 'Complete','5' => 'Canceled');
		return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id){
        if($request->ajax()){
            $this->bladeVar['status'] = array('1' => 'Pending','2' => 'Accepted','3' => 'Processing','4' => 'Complete','5' => 'Canceled');
            $orderdetail = Orderdetail::find($id);
            $orderId = $orderdetail->order_id;
            $productId = $orderdetail->product_id;
            $sellerId = $orderdetail->product_seller_id;

            $productdetail = Product::find($productId);
            //$shopdetail = Shop::find($productdetail->shop_id);
            $userdetail = User::find($sellerId);
            //$tenantdetail = Tenant::find($userdetail->tenant_id);
            
            $userdetail = Orders::withTrashed()->find($orderId);
            $this->bladeVar['userdetail'] = $userdetail;
            $this->bladeVar['result'] = $orderdetail;
            //$this->bladeVar['sellerdetail'] = $tenantdetail;
            //$this->bladeVar['shopdetail'] = $shopdetail;
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $orderdetail = Orderdetail::find($id);
        $orderdetail->update([
            'order_status' => $request->order_status,
        ]);
         return response()->json([
            'success' => '<strong>Success!</strong> Data updated for order <strong>'.$request->product_name.'</strong> <a href="'.route($this->route_master.'show', $orderdetail->_id).'" class="btn btn-sm btn-info" title="View this orderdetail" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);
    }  
    
    public function updateOrderStatus(Request $request)
    {
        $orderdetail = Orderdetail::find($request->id);
        $orderdetail->update([
            'order_status' => $request->order_status,
        ]);
        return response()->json([
            'success' => 'Success!'
        ]);
    }

    /**
     * Mark as deleted the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $product = Orders::withTrashed()->find($id);
        $product->forceDelete();
        return response()->json([
            'success' => '<strong>Success!</strong> Order has been deleted permanently!'
        ]);
    }

}
