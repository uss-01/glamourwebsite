<?php

namespace App\Http\Controllers;

use Auth;
use App\Offer;
use App\Shop\Shop;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class OfferController extends Controller
{
    protected $sortable = [
        'id' => 'offers._id',
        'title' => 'title',
        'start_date' => 'start_date',
        'end_date' => 'end_date'
    ];

    protected $validation_rules = [
        'title' => 'required|max:100',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ];

    protected $pageTitle = 'offer';
    protected $activeNav = [
        'nav' => 'offer',
        'sub' => 'offer'
       
    ];
    protected $breadcrumb_master = 'Generic Offer';
    protected $view_master = 'offer.';
    protected $route_master = 'offer.';

   public function __construct()
    {
         
     $this->bladeVar['page']['title'] = $this->pageTitle;
     $this->bladeVar['page']['activeNav'] = $this->activeNav;
     $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
     $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
     $this->cache();
    }
   

   protected function cache() 
    {
        $time = Carbon::now()->addHours(1);
         $this->bladeVar['shops'] = Cache::remember('shops', $time, function () {
            return Shop::orderBy('name', 'asc')->get();
        });
         
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = Offer::withTrashed()
            ->join('shops as s', 's._id', '=', 'offers.shop_id')
            ->select('offers.*')
            ->with('shop')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Offer::withTrashed()
            ->join('shops as s', 's._id', '=', 'offers.shop_id')->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Offer::withTrashed()
            ->join('shops as s', 's._id', '=', 'offers.shop_id')
            ->where('title', 'like', '%'.$request->q.'%')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Offer::withTrashed()
            ->join('shops as s', 's._id', '=', 'offers.shop_id')
            ->where('title', 'like', '%'.$request->q.'%')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   


        $input = $request->all();
        $validator = Validator::make($request->all(), [
        'title' => 'required',
        'shop' => 'required',
        'start_date' => 'required',
        'end_date' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);
          if ($validator->passes()) {
        $image_title = str_slug($request->title, '-');
        $input['image'] = time().'_'.$image_title.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/offers'), $input['image']);

        $offer = Offer::create([
                'title' => $request->title,
                'sub_title' => $request->sub_title,
                'shop_id' =>$request->shop,
                'details' => $request->details,
                'image'=> $input['image'],
                'start_date' => $request->start_date_alt,
                'end_date' => $request->end_date_alt,
                'created_by'=>Auth::id(),

                ]);

        return response()->json([
                'success' => '<strong>Success!</strong> data saved for offer <strong>'.$request->title.'</strong><a href="'.route($this->route_master.'show', $offer->_id).'" class="btn btn-sm btn-info" title="View this offer" data-toggle="modal" data-target="#viewModal"> Click here to view it</a>'
        
            ]);
     }
      

      return response()->json(['error'=>$validator->errors()->getMessages()]);

    }

   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Offer::withTrashed()->find($id);
            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Offer::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
        'title' => 'required',
        'shop' => 'required',
        'start_date' => 'required',
        'end_date' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);

          if (isset($request->image)) {
            if($validator->passes()){

        $image_title = str_slug($request->title, '-');
        $input['image'] = time().'_'.$image_title.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/offers'), $input['image']);
        $offer = Offer::find($id);
         File::delete(public_path().'/images/offers/'.$offer->image);
        $offer->update([
                'title' => $request->title,
                'sub_title' => $request->sub_title,
                'shop_id' =>$request->shop,
                'details' => $request->details,
                'image'=> $input['image'],
                'start_date' => $request->start_date_alt,
                'end_date' => $request->end_date_alt,
                'updated_by'=>Auth::id(),
                ]);
      
         return response()->json([
            'success' => '<strong>Success!</strong> Data updated for offer <strong>'.$request->title.'</strong> <a href="'.route($this->route_master.'show', $offer->_id).'" class="btn btn-sm btn-info" title="View this offer" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);
     } 
       return response()->json(['error'=>$validator->errors()->getMessages()]);
     }
     else{
        $validator = Validator::make($request->all(), [
        'title' => 'required',
        'shop' => 'required',
        'start_date' => 'required',
        'end_date' => 'required',
      ]);
         if($validator->passes()){

        $offer = offer::find($id);
        $offer->update([
                'title' => $request->title,
                'sub_title' => $request->sub_title,
                'shop_id' =>$request->shop,
                'details' => $request->details,
                'start_date' => $request->start_date_alt,
                'end_date' => $request->end_date_alt,
                'updated_by'=>Auth::id(),
                ]);

         return response()->json([
            'success' => '<strong>Success!</strong> Data updated for offer <strong>'.$request->title.'</strong> <a href="'.route($this->route_master.'show', $offer->_id).'" class="btn btn-sm btn-info" title="View this offer" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);
    }
     return response()->json(['error'=>$validator->errors()->getMessages()]);
     }
        
       
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  $offer = offer::find($id); 
       $offer->delete();
        
        return response()->json([
            'success' => '<strong>Success!</strong> offer has been deleted temporarily!'
        ]);
    }

    /**
     * Mark as deleted the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $offer = offer::withTrashed()->find($id);
        $offer->forceDelete();
        File::delete(public_path().'/images/offers/'.$offer->image);
        return response()->json([
            'success' => '<strong>Success!</strong> offer has been deleted permanently!'
        ]);
    }

    /**
     * Restores deleted marked resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        offer::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> offer has been restored!'
        ]);
    }
}
