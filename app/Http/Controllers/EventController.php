<?php

namespace App\Http\Controllers;

use Auth;
use App\Event;
use App\EventPics;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    protected $sortable = [
        'id' => '_id',
        'title' => 'title',
        'start_date' => 'start_date',
        'end_date' => 'end_date'
    ];

    protected $validation_rules = [
        'title' => 'required|max:100',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ];

    protected $pageTitle = 'Event';
    protected $activeNav = [
        'nav' => 'event',
        'sub' => 'event'
       
    ];
    protected $breadcrumb_master = 'Event';
    protected $view_master = 'event.';
    protected $route_master = 'event.';

   public function __construct()
    {
         
     $this->bladeVar['page']['title'] = $this->pageTitle;
     $this->bladeVar['page']['activeNav'] = $this->activeNav;
     $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
     $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = Event::withTrashed()
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Event::withTrashed()->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Event::withTrashed()
            ->where('title', 'like', '%'.$request->q.'%')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Event::withTrashed()
            ->where('title', 'like', '%'.$request->q.'%')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   


        $input = $request->all();
        $validator = Validator::make($request->all(), [
        'title' => 'required',
        'start_date' => 'required',
        'end_date' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);
          if ($validator->passes()) {
        $image_title = str_replace(' ', '-',strtolower($request->title));
        $input['image'] = time().'_'.$image_title.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/events'), $input['image']);

        $event = Event::create([
                'title' => $request->title,
                'sub_title' => $request->sub_title,
                'event_time' => $request->event_time,
                'event_location' => $request->event_location,
                'details' => $request->details,
                'image'=> $input['image'],
                'start_date' => $request->start_date_alt,
                'end_date' => $request->end_date_alt,
                'created_by'=>Auth::id(),
                ]);

        return response()->json([
                'success' => '<strong>Success!</strong> data saved for event <strong>'.$request->title.'</strong><a href="'.route($this->route_master.'show', $event->_id).'" class="btn btn-sm btn-info" title="View this event" data-toggle="modal" data-target="#viewModal"> Click here to view it</a>'
        
            ]);
     }
      

      return response()->json(['error'=>$validator->errors()->getMessages()]);

    }

   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Event::withTrashed()->find($id);
            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Event::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
        'title' => 'required',
        'start_date' => 'required',
        'end_date' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);

          if (isset($request->image)) {
            if($validator->passes()){

        $image_title = str_replace(' ', '-',strtolower($request->title));
        $input['image'] = time().'_'.$image_title.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/events'), $input['image']);
        $event = Event::find($id);
         File::delete(public_path().'/images/events/'.$event->image);
        $event->update([
                'title' => $request->title,
                'sub_title' => $request->sub_title,
                'event_time' => $request->event_time,
                'event_location' => $request->event_location,
                'details' => $request->details,
                'image'=> $input['image'],
                'start_date' => $request->start_date_alt,
                'end_date' => $request->end_date_alt,
                'updated_by'=>Auth::id(),
                 ]);
      
         return response()->json([
            'success' => '<strong>Success!</strong> Data updated for event <strong>'.$request->title.'</strong> <a href="'.route($this->route_master.'show', $event->_id).'" class="btn btn-sm btn-info" title="View this event" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);
     } 
       return response()->json(['error'=>$validator->errors()->getMessages()]);
     }
     else{
        $validator = Validator::make($request->all(), [
        'title' => 'required',
        'start_date' => 'required',
        'end_date' => 'required',
      ]);
         if($validator->passes()){

        $event = Event::find($id);
        $event->update([
                'title' => $request->title,
                'sub_title' => $request->sub_title,
                'event_time' => $request->event_time,
                'event_location' => $request->event_location,
                'details' => $request->details,
                'start_date' => $request->start_date_alt,
                'end_date' => $request->end_date_alt,
                'updated_by'=>Auth::id(),
                ]);

         return response()->json([
            'success' => '<strong>Success!</strong> Data updated for event <strong>'.$request->title.'</strong> <a href="'.route($this->route_master.'show', $event->_id).'" class="btn btn-sm btn-info" title="View this event" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);
    }
     return response()->json(['error'=>$validator->errors()->getMessages()]);
     }
        
       
    }

    // For SHop Pics
    public function pics(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Event::withTrashed()->find($id);
            $this->bladeVar['eventPics'] = EventPics::where(['event_pics.event_id' => $id])
                ->select('event_pics._id','event_pics.image')
                ->get();
            
            return view($this->view_master.'pics', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    public function savePics(Request $request)
    {
        
        $input = $request->all();
        $rules = [];
        $photo_cnt = count($request->image);
        foreach(range(0, ($photo_cnt-1)) as $index) {
            $rules['image.' . $index] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->passes()) {
            $cnt = 1;
        
        foreach ($request->image as $photo) {
           
            $image_fullname = time().'_'.$cnt.'.'.$photo->getClientOriginalExtension();
            
            $photo->move(public_path('images/events'), $image_fullname);
            
            $photo = EventPics::create([
              
                'event_id' => $request->event,
                'image'=> $image_fullname,
                'created_by'=>Auth::id(),
               
            ]);
            $cnt++;
        }

        return response()->json([
                'success' => '<strong>Success!</strong> data saved for photo <strong>'.$request->title.'</strong><a href="'.route($this->route_master.'show', $photo->_id).'" class="btn btn-sm btn-info" title="View this photo" data-toggle="modal" data-target="#viewModal"> Click here to view it</a>'
        
            ]);
        }
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  $event = Event::find($id); 
       $event->delete();
        
        return response()->json([
            'success' => '<strong>Success!</strong> event has been deleted temporarily!'
        ]);
    }

    /**
     * Mark as deleted the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $event = Event::withTrashed()->find($id);
        File::delete(public_path().'/images/events/'.$event->image);
        $event->forceDelete();
        
        return response()->json([
            'success' => '<strong>Success!</strong> event has been deleted permanently!'
        ]);
    }

    public function deletePic($id)
    {
        $event = EventPics::find($id);
        File::delete(public_path().'/images/events/'.$event->image);
        $event->delete();
        return response()->json([
            'success' => '<strong>Success!</strong> Shop Pic has been deleted permanently!'
        ]);
    }

    /**
     * Restores deleted marked resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        event::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> event has been restored!'
        ]);
    }
}
