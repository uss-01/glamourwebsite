<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SiteUsers;
use App\SiteUsersEntryExitTime;

class SiteUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

   
   protected $sortable = [
        'id' => '_id',
        'fname' => 'fname',
        'lname' => 'lname',
        'email' => 'email'		
    ];

    protected $pageTitle = 'SiteUsers';
    protected $activeNav = [
        'nav' => 'site_users',
        
    ];
    protected $breadcrumb_master = 'Users';
    protected $view_master = 'siteusers.';
    protected $route_master = 'siteusers.';

   protected $validation_rules = [
        'fname' => 'required|max:255',
        'lname' => 'required|max:255',        
    ];
    
    protected $validation_messages = [
        'fname.required' => 'Please Enter First Name'
    ];

    public function index(Request $request) 
    {

        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = SiteUsers::withTrashed()
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = SiteUsers::withTrashed()->count();

        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);

    }

    public function search(Request $request)
    {
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = SiteUsers::withTrashed()
            ->where('fname', 'like', '%'.$request->q.'%')
            ->orwhere('lname', 'like', '%'.$request->q.'%')
            ->orwhere('email', 'like', '%'.$request->q.'%')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = SiteUsers::withTrashed()
            ->where('fname', 'like', '%'.$request->q.'%')
			->orwhere('lname', 'like', '%'.$request->q.'%')
			->orwhere('email', 'like', '%'.$request->q.'%')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = SiteUsers::withTrashed()->find($id);
            $this->bladeVar['entryExitTime'] = SiteUsersEntryExitTime::where(['site_user_id' => $id])
            ->select('*')
            ->get();

            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = SiteUsers::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function update(Request $request, $id)
    {
       $this->validate($request, $this->validation_rules, $this->validation_messages);
        $siteuser = SiteUsers::find($id);
         $siteuser->update([
            'fname' => $request->fname,
           'lname' => $request->lname,
           'dob' => $request->dob,
        ]);
        
        return response()->json([
            'success' => '<strong>Success!</strong> Data updated for User <strong>'.$request->fname.'</strong> <a href="'.route($this->route_master.'show', $siteuser->_id).'" class="btn btn-sm btn-info" title="View this User" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);
    }


     public function delete($id)
    {
        SiteUsers::withTrashed()->find($id)->forceDelete();
        return response()->json([
            'success' => '<strong>Success!</strong> User has been deleted permanently!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SiteUsers::find($id)->delete();
        return response()->json([
            'success' => '<strong>Success!</strong> Users has been deleted temporarily!'
        ]);
    }

    public function restore($id)
    {
        SiteUsers::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> User has been restored!'
        ]);
    }
}
