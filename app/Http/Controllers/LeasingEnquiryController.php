<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\User;
use App\Tenant;
use App\Leasing\LeasingEnquiry;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

class LeasingEnquiryController extends Controller
{
    protected $breadcrumb_master = 'Leasing Enquiry';
    protected $view_master = 'leasingenquiry.';
    protected $route_master = 'leasingenquiry.';
    protected $pageTitle = 'Leasing Enquiry';
    protected $activeNav = [
        'nav' => 'leasingenquiry',
        'sub' => 'leasingenquiry'
    ];
    protected $sortable = [
        'id' => 'id',
        'title' => 'name'
    ];
    public function __construct()
    {
       $this->bladeVar['page']['title'] = $this->pageTitle;
       $this->bladeVar['page']['activeNav'] = $this->activeNav;
       $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
   }

    public function index(Request $request){
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = LeasingEnquiry::withTrashed()
                                    ->select('leasing_enquiries.*')
                                    ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
                                    ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = LeasingEnquiry::withTrashed()->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    public function search(Request $request){
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = LeasingEnquiry::withTrashed()
            ->where('companyName', 'like', '%'.$request->q.'%')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = LeasingEnquiry::withTrashed()
            ->where('companyName', 'like', '%'.$request->q.'%')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id){
        if ($request->ajax()) {
            $this->bladeVar['result'] = LeasingEnquiry::withTrashed()->find($id);
            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    public function delete($id){
        DB::delete('delete from leasing_enquiries where id = ?',[$id]);
        LeasingEnquiry::withTrashed()->find($id)->forceDelete();
        return response()->json([
            'success' => '<strong>Success!</strong> Leasing Enquiry has been deleted permanently!'
        ]);
    }
    
}
