<?php

namespace App\Http\Controllers\Shop;
use Auth;
use DB;
use App\Shop\Shop;
use App\Shop\ShopBrands;
use App\Shop\ShopPics;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use App\Masters\Category;
use App\Masters\Store;
use App\Masters\ProductCategory;
use App\Masters\ProductSubcategory;
use App\Masters\Brand;
use Validator;
use Image;

class ShopController extends Controller
{
    protected $sortable = [
        'id' => '_id',
        'name' => 'name',
        'category' => 'c.name',
        'store' => 's.name'
    ];

    protected $validation_rules = [
        'name' => 'required|max:100',
        'category' => 'required|integer',
        'store' => 'required|integer'
    ];

    protected $pageTitle = 'Shop';
    protected $activeNav = [
        'nav' => 'shop',
        'sub' => 'store'
    ];
    protected $breadcrumb_master = 'Shop';
    protected $view_master = 'shop.';
    protected $route_master = 'shop.';

    public function __construct()
    {
         $this->bladeVar['page']['title'] = $this->pageTitle;
         $this->bladeVar['page']['activeNav'] = $this->activeNav;
         $this->bladeVar['page']['breadcrumb'] = ['Home' => '/', 'Shop' => '/shop'];
         $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
         $this->cache();
    }

    /**
     * Refresh cache and returns puts it in array
     */
    protected function cache()
    {
        $time = Carbon::now()->addHours(1);
        $this->bladeVar['categories'] = Cache::remember('categories', $time, function () {
            return Category::orderBy('name', 'asc')->get();
        });
         $this->bladeVar['stores'] = Cache::remember('stores', $time, function () {
            return Store::orderBy('name', 'asc')->get();
        });
         $this->bladeVar['store_categories'] = Cache::remember('store_categories', $time, function () {
            return ProductCategory::orderBy('name', 'asc')->where('type','=','Store')->get();
        });
        $this->bladeVar['store_subcategories'] = Cache::remember('store_subcategories', $time, function () {
            return ProductSubcategory::orderBy('name', 'asc')->where('type','=','Store')->get();
        });

        $this->bladeVar['brands'] = Cache::remember('brands', $time, function () {
            return Brand::where('category_id','=','1')->orderBy('name', 'asc')->get();
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = Shop::withTrashed()
            ->join('master_category as c', 'c.category_id', '=', 'shops.category_id')
            ->join('master_store as s', 's.store_id', '=', 'shops.store_id')
            ->where('shops.category_id','=','1')
            ->select('shops.*')
            ->with('category')
            ->with('store')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
			->groupBy('shops._id')
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Shop::withTrashed()
            ->join('master_category as c', 'c.category_id', '=', 'shops.category_id')
            ->join('master_store as s', 's.store_id', '=', 'shops.store_id')
			->where('shops.category_id','=','1')			
            ->with('category')
            ->with('store')->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb']['Shop'] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Shop::withTrashed()
            ->where('shops.name', 'LIKE', '%'.$request->q.'%')
            ->join('master_category as c', 'c.category_id', '=', 'shops.category_id')
            ->join('master_store as s', 's.store_id', '=', 'shops.store_id')
            ->where('shops.category_id','=','1')
            ->select('shops.*')
            ->with('category')
            ->with('store')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
			->groupBy('shops._id')
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Shop::withTrashed()
            ->where('shops.name', 'LIKE', '%'.$request->q.'%')
            ->join('master_category as c', 'c.category_id', '=', 'shops.category_id')
            ->join('master_store as s', 's.store_id', '=', 'shops.store_id')
            ->where('shops.category_id','=','1')
            ->with('category')
            ->with('store')			
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $logo = NULL;
        if($request->store_category==-1){
          $this->validation_rules['other_category'] = 'required';
        }
        if($request->store_subcategory==-1){
          $this->validation_rules['other_subcategory'] = 'required';
        }

        if(isset($request->image)){
        $this->validation_rules['image'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048';

        $validator = Validator::make($request->all(),$this->validation_rules);
          if ($validator->passes()) {
        $image_name = str_replace(' ', '-',strtolower($request->name));
        $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/shops'), $input['image']);

        $img_thumb = Image::make(public_path('images/shops').'/'.$input['image']);
        $img_thumb->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('images/shops/thumbnails').'/'.$input['image']);
        $logo = $input['image'];
      }else{
         return response()->json(['error'=>$validator->errors()->getMessages()]);
      }

      }else{
       $validator = Validator::make($request->all(),$this->validation_rules);
       if (!$validator->passes()) {
        return response()->json(['error'=>$validator->errors()->getMessages()]);
         }
       }
        if($request->store_subcategory == NULL){
           $request->store_subcategory = 0;
        }
        // Other Product Category Insert
        if($request->store_category==-1){
         $store_category = ProductCategory::updateOrCreate(
            ['name' => $request->other_category,
             'type'=>'Store'
            ],
            ['created_by'=>Auth::id()]
        );
         $request->store_category = $store_category->_id;
        }
        // Other Product Subcategory Insert
         if($request->store_subcategory==-1){
        $store_subcategory = ProductSubcategory::updateOrCreate(
            ['name' => $request->other_subcategory,
            'product_category_id' => $request->store_category,
            'type'=>'Store'],
            ['created_by'=>Auth::id()]
        );
         $request->store_subcategory = $store_subcategory->_id;
        }
        $shop = Shop::create([
            'name' => $request->name,
            'category_id' => $request->category,
            'store_id' => $request->store,
            'logo' =>  $logo,
             'store_category_id' => $request->store_category,
            'store_subcategory_id' => $request->store_subcategory,
            'email' => $request->email,
            'phone' => $request->phone,
            'website' => $request->website,
            'address' => $request->address,
            'details' => $request->details,
            'floorlevel' => $request->floorlevel,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'lease_year' => $request->lease_year,
            'created_by'=>Auth::id(),
        ]);
		if($request->brand && count($request->brand) > 0){
         foreach ($request->brand as $brandid) {
            ShopBrands::create([
                'shop_id' => $shop->_id,
                'brand_id' => $brandid,
                'created_by'=>Auth::id(),
            ]);
         }
		}

        return response()->json([
            'success' => '<strong>Success!</strong> data saved for shop <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $shop->_id).'" class="btn btn-sm btn-info" title="View this city" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Shop::withTrashed()->find($id);
            $this->bladeVar['selectedBrands'] = ShopBrands::where(['shop_brands.shop_id' => $id])
                ->join('master_brand', 'master_brand.brand_id', '=', 'shop_brands.brand_id')
                ->select('master_brand.name')
                ->get();

            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $shop = Shop::find($id);
            $this->bladeVar['result'] = $shop;
            $shopBrands = ShopBrands::where(['shop_brands.shop_id' => $id])
                ->join('master_brand', 'master_brand.brand_id', '=', 'shop_brands.brand_id')
                ->select('shop_brands.brand_id')
                ->get();
            $this->bladeVar['selectedBrands'] = array();
			foreach ($shopBrands as $brand) {
                $this->bladeVar['selectedBrands'][] = $brand->brand_id;
            }

            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $logo = NULL;
        if(isset($request->image)){
        $this->validation_rules['image'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048';

        $validator = Validator::make($request->all(),$this->validation_rules);
          if ($validator->passes()) {
        $image_name = str_replace(' ', '-',strtolower($request->name));
        $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/shops'), $input['image']);

        $img_thumb = Image::make(public_path('images/shops').'/'.$input['image']);
        $img_thumb->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('images/shops/thumbnails').'/'.$input['image']);
        $shop = Shop::find($id);
         File::delete(public_path().'/images/shops/'.$shop->logo);
         File::delete(public_path().'/images/shops/thumbnails/'.$shop->logo);
        $logo = $input['image'];
      }else{
         return response()->json(['error'=>$validator->errors()->getMessages()]);
      }

      } else{
        $validator = Validator::make($request->all(),$this->validation_rules);
       if (!$validator->passes()) {
        return response()->json(['error'=>$validator->errors()->getMessages()]);
         }
       }
        $shop = Shop::find($id);
        if($logo == NULL){
        $shop->update([
            'beacon_id' => trim($request->beacon_id),
            'tenant_uid' => trim($request->tenant_uid),
            'name' => $request->name,
            'category_id' => $request->category,
            'store_id' => $request->store,
            'store_category_id' => $request->store_category,
            'store_subcategory_id' => $request->store_subcategory,
            'email' => $request->email,
            'phone' => $request->phone,
            'website' => $request->website,
            'address' => $request->address,
            'details' => $request->details,
            'floorlevel' => $request->floorlevel,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'lease_year' => $request->lease_year,
            'updated_by'=>Auth::id(),
        ]);
      }else{
       $shop->update([
            'name' => $request->name,
            'category_id' => $request->category,
            'store_id' => $request->store,
            'logo' => $logo,
            'store_category_id' => $request->store_category,
            'store_subcategory_id' => $request->store_subcategory,
            'email' => $request->email,
            'phone' => $request->phone,
            'website' => $request->website,
            'address' => $request->address,
            'details' => $request->details,
            'floorlevel' => $request->floorlevel,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'lease_year' => $request->lease_year,
            'updated_by'=>Auth::id(),
        ]);

      }

         ShopBrands::where(['shop_id' => $shop->_id])->delete();

        //Add selected tour types for agent
		if($request->brand && count($request->brand) > 0){
			foreach ($request->brand as $brandid) {
				ShopBrands::create([
					'shop_id' => $shop->_id,
					'brand_id' => $brandid,
					'created_by'=>Auth::id(),
				]);
			}
		}

        return response()->json([
            'success' => '<strong>Success!</strong> data updated for shop <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $shop->_id).'" class="btn btn-sm btn-info" title="View this shop" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);
    }

    // Store Subcategory
    public function store_subcategory(Request $request)
    {
        $this->validate($request, [
            'product_category' => 'required|integer'
        ]);
         $product_subcategories = ProductSubcategory::where('product_subcategories.product_category_id', '=', $request->product_category)
            ->select('_id', 'name')
            ->orderBy($this->sortable[$request->input('sort', 'name')], $request->input('dir', 'ASC'))->get();
        return response()->json($product_subcategories);
    }

    // For SHop Pics
    public function pics(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Shop::withTrashed()->find($id);
            $this->bladeVar['shopPics'] = ShopPics::where(['shop_pics.shop_id' => $id])
                ->select('shop_pics._id','shop_pics.image')
                ->get();

            return view($this->view_master.'pics', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    public function savePics(Request $request)
    {

        $input = $request->all();
        $rules = [];
        $photo_cnt = count($request->image);
        foreach(range(0, ($photo_cnt-1)) as $index) {
            $rules['image.' . $index] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->passes()) {
            $cnt = 1;

        foreach ($request->image as $photo) {

            $image_fullname = time().'_'.$cnt.'.'.$photo->getClientOriginalExtension();

            $photo->move(public_path('images/shops'), $image_fullname);

            $photo = ShopPics::create([

                'shop_id' => $request->shop,
                'image'=> $image_fullname,
                'created_by'=>Auth::id(),

            ]);
            $cnt++;
        }

        return response()->json([
                'success' => '<strong>Success!</strong> data saved for photo <strong>'.$request->title.'</strong><a href="'.route($this->route_master.'show', $photo->_id).'" class="btn btn-sm btn-info" title="View this photo" data-toggle="modal" data-target="#viewModal"> Click here to view it</a>'

            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Shop::find($id)->delete();
        return response()->json([
            'success' => '<strong>Success!</strong> Shop has been deleted temporarily!'
        ]);
    }

    /**
     * Mark as deleted the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $shop=Shop::withTrashed()->find($id);
        File::delete(public_path().'/images/shops/'.$shop->image);
        File::delete(public_path().'/images/shops/thumbnails/'.$shop->image);
        $shop->forceDelete();
        ShopBrands::where(['shop_id' => $id])->delete();
        return response()->json([
            'success' => '<strong>Success!</strong> Shop has been deleted permanently!'
        ]);
    }

     public function deletePic($id)
    {
        $shop = ShopPics::find($id);
        File::delete(public_path().'/images/shops/'.$shop->image);
        $shop->delete();
        return response()->json([
            'success' => '<strong>Success!</strong> Shop Pic has been deleted permanently!'
        ]);
    }

    /**
     * Restores deleted marked resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        Shop::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> Shop has been restored!'
        ]);
    }
}
