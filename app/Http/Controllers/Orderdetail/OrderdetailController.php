<?php

namespace App\Http\Controllers\Orderdetail;

use Auth;
use DB;
use App\Orders\Orders;
use App\Orderdetail\Orderdetail;
use App\SiteUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderdetailController extends Controller
{
     protected $sortable = [
        'id' => '_id',
        'name' => 'name',
    ];
    protected $pageTitle = 'Orders';
    protected $activeNav = [
        'nav' => 'orders',
        'sub' => 'orderdetail'
    ];
    protected $breadcrumb_master = 'Orders';
    protected $view_master = 'orderdetail.';
    protected $route_master = 'orderdetail.';

    public function __construct()
    {
         $this->bladeVar['page']['title'] = $this->pageTitle;
         $this->bladeVar['page']['activeNav'] = $this->activeNav;
         $this->bladeVar['page']['breadcrumb'] = ['Home' => '/', 'Orders' => '/orders'];
         $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = Orderdetail::withTrashed()
            ->where('order_details.product_seller_id', Auth::id())
            ->select('order_details.*')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'DESC'))
            ->groupBy('order_details._id')
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Orderdetail::withTrashed()
            ->where('order_details.product_seller_id', Auth::id())
            ->select('order_details.*')
            ->count();
        $this->bladeVar['status'] = array('1' => 'Pending','2' => 'Accepted','3' => 'Processing','4' => 'Complete','5' => 'Canceled');    
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request){ 
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Orderdetail::withTrashed()
            ->where('order_details.product_name', 'LIKE', '%'.$request->q.'%')
            ->where('order_details.product_seller_id', Auth::id())
            ->select('order_details.*')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'DESC'))
            ->groupBy('order_details._id')
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Orderdetail::withTrashed()
           ->where('order_details.product_name', 'LIKE', '%'.$request->q.'%')
           ->where('order_details.product_seller_id', Auth::id())
           ->select('order_details.*')
           ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id){   
        if ($request->ajax()) {
            $orderdetail = Orderdetail::withTrashed()->find($id);
            $orderId = $orderdetail->order_id;
            $userdetail = Orders::withTrashed()->find($orderId);
            $this->bladeVar['userdetail'] = $userdetail;
            $this->bladeVar['result'] = $orderdetail;
            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if($request->ajax()){
            $this->bladeVar['status'] = array('1' => 'Pending','2' => 'Accepted','3' => 'Processing','4' => 'Complete','5' => 'Canceled');
            $this->bladeVar['result'] = Orderdetail::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $orderdetail = Orderdetail::find($id);
        $orderdetail->update([
            'order_status' => $request->order_status,
        ]);
         return response()->json([
            'success' => '<strong>Success!</strong> Data updated for order <strong>'.$request->product_name.'</strong> <a href="'.route($this->route_master.'show', $orderdetail->_id).'" class="btn btn-sm btn-info" title="View this orderdetail" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);
    }

    public function updateOrderdetail(Request $request)
    {
        $orderdetail = Orderdetail::find($request->id);
        $orderdetail->update([
            'order_status' => $request->order_status,
        ]);
        return response()->json([
            'success' => 'Success!'
        ]);
    }

    /**
     * Mark as deleted the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $product = Orderdetail::withTrashed()->find($id);
        $product->forceDelete();
        return response()->json([
            'success' => '<strong>Success!</strong> Order has been deleted permanently!'
        ]);
    }

}
