<?php

namespace App\Http\Controllers\Inventory;

use Auth;
use DB;
use App\Orders\Orders;
use App\Orderdetail\Orderdetail;
use App\Inventory\Inventory;
use App\SiteUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InventoryController extends Controller
{
     protected $sortable = [
        'id' => '_id',
        'name' => 'name',
    ];
    protected $pageTitle = 'Inventory';
    protected $activeNav = [
        'nav' => 'inventory',
        'sub' => 'inventory'
    ];
    protected $breadcrumb_master = 'Inventory';
    protected $view_master = 'inventory.';
    protected $route_master = 'inventory.';

    public function __construct()
    {
         $this->bladeVar['page']['title'] = $this->pageTitle;
         $this->bladeVar['page']['activeNav'] = $this->activeNav;
         $this->bladeVar['page']['breadcrumb'] = ['Home' => '/', 'Inventory' => '/inventory'];
         $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $orderdetail = Orderdetail::where('order_details.product_seller_id', Auth::id())
            ->select('order_details.*')
            ->get();
        $orderId = array();   
        foreach($orderdetail as $order){    
           $orderId[] = $order->order_id;
        }
        $userdetail = Orders::withTrashed()->find($orderId);
        $this->bladeVar['results'] = $userdetail;

        $this->bladeVar['total_count'] = Orderdetail::withTrashed()
            ->where('order_details.product_seller_id', Auth::id())
            ->select('order_details.*')
            ->count();    
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request){ 
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Orders::withTrashed()   
            ->where('orders.firstname', 'LIKE', '%'.$request->q.'%')
            ->orWhere('orders.lastname', 'LIKE', '%'.$request->q.'%')
            ->select('orders.*')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'DESC'))
            ->groupBy('orders._id')
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Orders::withTrashed()
           ->where('orders.firstname', 'LIKE', '%'.$request->q.'%')
           ->orWhere('orders.lastname', 'LIKE', '%'.$request->q.'%')
           ->select('orders.*')
           ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchOrders(Request $request){ 
        if (!$request->q) {
            return redirect(route($this->route_master.'show'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Orderdetail::withTrashed()
            ->where('order_details.product_seller_id', Auth::id())    
            ->where('order_details.product_name', 'LIKE', '%'.$request->q.'%')
            ->select('order_details.*')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'DESC'))
            ->groupBy('order_details._id')
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Orderdetail::withTrashed()
           ->where('order_details.product_seller_id', Auth::id())
           ->where('order_details.product_name', 'LIKE', '%'.$request->q.'%')
           ->select('order_details.*')
           ->count();
        return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id){   
        $this->bladeVar['page']['breadcrumb'] = ['Home' => '/', 'Inventory' => '/inventory'];
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = ''; 
        $this->bladeVar['results'] = Orderdetail::withTrashed()
            ->where('order_details.product_seller_id', Auth::id())
            ->where('order_details.order_id', $id)
            ->select('order_details.*')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'DESC'))
            ->groupBy('order_details._id')
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Orderdetail::where(['order_id' => $id])
            ->where('order_details.product_seller_id', Auth::id()) 
            ->count(); 
        $this->bladeVar['orderID'] = $id;  
		return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
    }


}
