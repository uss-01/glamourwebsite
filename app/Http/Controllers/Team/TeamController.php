<?php

namespace App\Http\Controllers\Team;

use Auth;
use Validator;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class TeamController extends Controller
{

    protected $sortable = [
        'id' => 'id',
        'name' => 'name',
    ];
    protected $validation_rules = [
        'name' => 'required|max:255|unique:team',
        'slug' => 'required|max:255|unique:team',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ];
    protected $pageTitle = 'Team';
    protected $activeNav = [
        'nav' => 'team',
        'sub' => 'team'
    ];
    protected $breadcrumb_master = 'Team';
    protected $view_master = 'team.';
    protected $route_master = 'team.';

    public function __construct()
    {
       $this->bladeVar['page']['title'] = $this->pageTitle;
       $this->bladeVar['page']['activeNav'] = $this->activeNav;
       $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
       $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = \App\Team\Team::withTrashed()
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->select('team.*')
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = \App\Team\Team::withTrashed()->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = \App\Team\Team::withTrashed()
            ->where('name', 'like', '%'.$request->q.'%')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = \App\Team\Team::withTrashed()
            ->where('name', 'like', '%'.$request->q.'%')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:team',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($validator->passes()) {
            if($request->image){
                list($width, $height) = getimagesize($request->image);
                if($width != 400 && $height != 400){
                    $error_msg=[
                    'image' => 'Size Should be 400X400',
                    ];
                    return response()->json(['error'=>$error_msg]);
                }
            }
            $image_name = str_replace(' ', '-',strtolower($request->name));
            $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('images/cms'), $input['image']);
            if(!empty($request->designation)){
                $designation = $request->designation;
            }else{
                $designation = '';  
            }
            if(!empty($request->facebook_url)){
                $facebook_url = $request->facebook_url;
            }else{
                $facebook_url = '';  
            }
            if(!empty($request->twitter_url)){
                $twitter_url = $request->twitter_url;
            }else{
                $twitter_url = '';  
            }
            if(!empty($request->linkedin_url)){
                $linkedin_url = $request->linkedin_url;
            }else{
                $linkedin_url = '';  
            }
            if(!empty($request->google_url)){
                $google_url = $request->google_url;
            }else{
                $google_url = '';  
            }
            $brand = \App\Team\Team::create([
                'name' => $request->name,
                'slug' => $this->removeSpecialChapr($request->name),
                'designation' => $designation,
                'facebook_url' => $facebook_url,
                'twitter_url' => $twitter_url,
                'linkedin_url' => $linkedin_url,
                'google_url' => $google_url,
                'image'=> $input['image'],
                'created_by'=>Auth::id(),
            ]);
            return response()->json([
                'success' => '<strong>Success!</strong> data saved for team <strong>'.$request->name.'</strong><a href="'.route($this->route_master.'show', $brand->id).'" class="btn btn-sm btn-info" title="View this team" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
            ]);
        }
        return response()->json(['error'=>$validator->errors()->getMessages()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = \App\Team\Team::withTrashed()->find($id);
            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = \App\Team\Team::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);
        if (isset($request->image)) {
            if($validator->passes()){
                if($request->image){
                    list($width, $height) = getimagesize($request->image);
                    if($width != 400 && $height != 400){
                        $error_msg=[
                        'image' => 'Size Should be 400X400',
                        ];
                        return response()->json(['error'=>$error_msg]);
                    }
                }
                $image_name = str_replace(' ', '-',strtolower($request->name));
                $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('images/cms'), $input['image']);
                $slider = \App\Team\Team::find($id);
                File::delete(public_path().'/images/cms/'.$slider->image);
                if(!empty($request->designation)){
                    $designation = $request->designation;
                }else{
                    $designation = '';  
                }
                if(!empty($request->facebook_url)){
                    $facebook_url = $request->facebook_url;
                }else{
                    $facebook_url = '';  
                }
                if(!empty($request->twitter_url)){
                    $twitter_url = $request->twitter_url;
                }else{
                    $twitter_url = '';  
                }
                if(!empty($request->linkedin_url)){
                    $linkedin_url = $request->linkedin_url;
                }else{
                    $linkedin_url = '';  
                }
                if(!empty($request->google_url)){
                    $google_url = $request->google_url;
                }else{
                    $google_url = '';  
                }
                $slider->update([
                    'name' => $request->name,
                    'slug' => $this->removeSpecialChapr($request->name),
                    'designation' => $designation,
                    'facebook_url' => $facebook_url,
                    'twitter_url' => $twitter_url,
                    'linkedin_url' => $linkedin_url,
                    'google_url' => $google_url,
                    'image'=> $input['image'],
                    'updated_by'=>Auth::id(),
                ]);
                return response()->json([
                    'success' => '<strong>Success!</strong> Data updated for team <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $slider->id).'" class="btn btn-sm btn-info" title="View this team" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
                ]);
            } 
            return response()->json(['error'=>$validator->errors()->getMessages()]);
        }
        else{
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
            ]);
            if($validator->passes()){
                $slider = \App\Team\Team::find($id);
                if(!empty($request->designation)){
                    $designation = $request->designation;
                }else{
                    $designation = '';  
                }
                if(!empty($request->facebook_url)){
                    $facebook_url = $request->facebook_url;
                }else{
                    $facebook_url = '';  
                }
                if(!empty($request->twitter_url)){
                    $twitter_url = $request->twitter_url;
                }else{
                    $twitter_url = '';  
                }
                if(!empty($request->linkedin_url)){
                    $linkedin_url = $request->linkedin_url;
                }else{
                    $linkedin_url = '';  
                }
                if(!empty($request->google_url)){
                    $google_url = $request->google_url;
                }else{
                    $google_url = '';  
                }
                $slider->update([
                    'name' => $request->name,
                    'slug' => $this->removeSpecialChapr($request->name),
                    'designation' => $designation,
                    'facebook_url' => $facebook_url,
                    'twitter_url' => $twitter_url,
                    'linkedin_url' => $linkedin_url,
                    'google_url' => $google_url,
                    'updated_by'=>Auth::id(),
                ]);
                return response()->json([
                    'success' => '<strong>Success!</strong> Data updated for team <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $slider->id).'" class="btn btn-sm btn-info" title="View this team" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
                ]);
            }
            return response()->json(['error'=>$validator->errors()->getMessages()]);
        }
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Team\Team::find($id)->delete();
        return response()->json([
            'success' => '<strong>Success!</strong> team has been deleted temporarily!'
        ]);
    }

    /**
     * Restores deleted marked resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        \App\Team\Team::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> team has been restored!'
        ]);
    }

    public function delete($id){
        $offer = \App\Team\Team::withTrashed()->find($id);
        $offer->forceDelete();
        File::delete(public_path().'/images/cms/'.$offer->image);
        return response()->json([
            'success' => '<strong>Success!</strong> team has been deleted permanently!'
        ]);
    }

    public function removeSpecialChapr($value){
		$title = str_replace( array( '\'', '"', ',' , ';', '<', '>','!', '@', '#' , '$', '%', '^', '&', '*' , '(', ')', '_', '-', '=' , '+', ':', '?', '.', '`', '~', '[', ']', '{', '}', '|' , '/' , '\\' , '‘' , '’' , '“', '”' , '…', '‰' ), '', $value);
		$post_title1 = str_replace( array("  "), array(" "), $title);	
		$post_title = str_replace( array(" ","'"), array("-",""), $post_title1);
		$postTitle = strtolower($post_title);
		return $postTitle;
	}
}