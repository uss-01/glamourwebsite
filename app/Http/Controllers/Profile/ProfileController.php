<?php
namespace App\Http\Controllers\Profile;

use Auth;
use DB;
use App\User;
use App\Tenant;
use App\Leasing;
use App\Shop\Shop;
use App\TenantComplaints;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    protected $sortable = [
        'id' => 'id',
    ];

    protected $breadcrumb_master = 'Profile';
    protected $view_master = 'profile.';
    protected $route_master = 'profile.';
    protected $pageTitle = 'Profile';
    protected $activeNav = [
        'nav' => 'profile',
        'sub' => 'profile'
    ];

    public function __construct(){
       $this->bladeVar['page']['title'] = $this->pageTitle;
       $this->bladeVar['page']['activeNav'] = $this->activeNav;
       $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
       $this->cache();
    }
    protected function cache(){
        $time = Carbon::now()->addHours(1);
        $this->bladeVar['shops'] = Cache::remember('shops', $time, function () {
            return Shop::orderBy('name', 'asc')->get();
        });
    }

    public function index(Request $request){
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $user = Auth::id();
        $userData = User::where(['id' => $user])->select('tenant_id')->first();
        $id = $userData->tenant_id;

        $this->bladeVar['leasingData'] = Leasing::where(['tenant_id' => $id])
                                    ->select('leasing.*')
                                    ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
                                    ->paginate($this->paginate_total);
        $this->bladeVar['result'] = Tenant::withTrashed()->find($id);
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    public function create(Request $request){
        if ($request->ajax()) {
            $user = Auth::id();
            $userData = User::where(['id' => $user])->select('tenant_id')->first();
            $this->bladeVar['result'] = $userData->tenant_id;
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    public function store(Request $request){
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'complaint' => 'required',
        ]);
        if($validator->passes()) {
            $TenantComplaints = TenantComplaints::create([
                'tenant_id' => $request->id,
				'complaint' => $request->complaint,
                'status' => 'unread',
            ]);
            return response()->json([
                'success' => '<strong>Success!</strong> Complaint sent to Admin'
            ]);
        }
        return response()->json(['error' => $validator->errors()->getMessages()]);
    }

    public function edit(Request $request, $id){
        if ($request->ajax()) {
            $this->bladeVar['result'] = Tenant::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    public function update(Request $request, $id){
        $input = $request->all();
        $validator = Validator::make($request->all(), [
           'name' => 'required',
           'phone' => 'required',
           'email' => 'required',
        ]);
        $shopArray = array();
        for($i=0; $i < count($request->shop_name); $i++){
            $shop = Shop::find($request->shop_name[$i]);  
            $shopArray[$i]['_id'] = $request->shop_name[$i];   
            $shopArray[$i]['name'] = $shop->name;
        }
        $shopNameArray = json_encode($shopArray);
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
        ]);

        if(!empty($request->password)){
            $userDataObj = User::where('tenant_id',$id)->first();
            $userDataObj->update([
                'password' => bcrypt($request->password),
            ]);
        }

        if($validator->passes()){
            $tenant = Tenant::find($id);
            $tenant->update([
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'address' => $request->address,
                'details' => $request->details,
                'shop_name' => $shopNameArray,   
                'updated_by'=>Auth::id(),
            ]);
            return response()->json([
                'success' => '<strong>Success!</strong> Data updated for Tenant <strong>'.$request->name.'</strong>'
            ]);
        }
        return response()->json(['error'=>$validator->errors()->getMessages()]);
    }

    public function tenantComplaints(Request $request, $id){
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['complaintsData'] = TenantComplaints::where(['tenant_id' => $id])
                                            ->select('tenant_complaints.*')
                                            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
                                            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = count($this->bladeVar['complaintsData']);
        return view($this->view_master.'complaints', ['bladeVar' => $this->bladeVar]);
    }

}
