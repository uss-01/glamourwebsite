<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
     protected $sortable = [
        'id' => '_id',
    ];

    protected $validation_rules = [
        'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ];

    protected $pageTitle = 'Settings';
    protected $activeNav = [
        'nav' => 'setting',
        'sub' => 'setting'
    ];
   
    protected $breadcrumb_master = 'Settings';
    protected $view_master = 'setting.';
    protected $route_master = 'setting.';

    public function __construct()
    {
     $this->bladeVar['page']['title'] = $this->pageTitle;
     $this->bladeVar['page']['activeNav'] = $this->activeNav;
     $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
     $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = Setting::withTrashed()->
            orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Setting::count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){   
        $input = $request->all();
        $validator = Validator::make($request->all(),[
        'key' => 'required',
        ]);
        if($validator->passes()){
            $slug = str_slug($request->key);
            $image_title = $slug;
            if(!empty($input['image'])){
                $input['image'] = time().'_'.$image_title.'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('images/cms'), $input['image']);
                $setting = Setting::create([
                    'title' => $request->title,
                    'message'=> $request->message,
                    'address' => $request->address,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'facebook_url' => $request->facebook_url,
                    'twitter_url' => $request->twitter_url,
                    'instagram_url' => $request->instagram_url,
                    'youtube_url' => $request->youtube_url,
                    'linkedin_url' => $request->linkedin_url,
                    'pinterest_url' => $request->pinterest_url,
                    'shipping' => $request->shipping,
                    'image'=> $input['image'],
                    'updated_by'=>Auth::id(),
                ]);
            }else{
                $setting = Setting::create([
                    'title' => $request->title,
                    'message'=> $request->message,
                    'address' => $request->address,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'facebook_url' => $request->facebook_url,
                    'twitter_url' => $request->twitter_url,
                    'instagram_url' => $request->instagram_url,
                    'youtube_url' => $request->youtube_url,
                    'linkedin_url' => $request->linkedin_url,
                    'pinterest_url' => $request->pinterest_url,
                    'shipping' => $request->shipping,
                    'updated_by'=>Auth::id(),
                ]);
            }
        
        return response()->json([
            'success' => '<strong>Success!</strong> data saved for Settings <strong>'.$request->key.'</strong><a href="'.route($this->route_master.'show', $setting->_id).'" class="btn btn-sm btn-info" title="View this Setting" data-toggle="modal" data-target="#viewModal"> Click here to view it</a>'
        ]);
     }
      return response()->json(['error'=>$validator->errors()->getMessages()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Setting::withTrashed()->find($id);
            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Setting::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(),[
          
        ]);
        if($validator->passes()){
            $setting = Setting::find($id);
            $slug = str_slug($request->key);

            $image_title = $slug;
            if(!empty($input['image'])){
                $input['image'] = time().'_'.$image_title.'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('images/cms'), $input['image']);
                $setting->update([
                    'title' => $request->title,
                    'message'=> $request->message,
                    'address' => $request->address,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'facebook_url' => $request->facebook_url,
                    'twitter_url' => $request->twitter_url,
                    'instagram_url' => $request->instagram_url,
                    'youtube_url' => $request->youtube_url,
                    'linkedin_url' => $request->linkedin_url,
                    'pinterest_url' => $request->pinterest_url,
                    'shipping' => $request->shipping,
                    'image'=> $input['image'],
                    'updated_by'=>Auth::id(),
                ]);
            }else{
                $setting->update([
                'title' => $request->title,
                'message'=> $request->message,
                'address' => $request->address,
                'email' => $request->email,
                'phone' => $request->phone,
                'facebook_url' => $request->facebook_url,
                'twitter_url' => $request->twitter_url,
                'instagram_url' => $request->instagram_url,
                'youtube_url' => $request->youtube_url,
                'linkedin_url' => $request->linkedin_url,
                'pinterest_url' => $request->pinterest_url,
                'shipping' => $request->shipping,
                'updated_by'=>Auth::id(),
                ]);
            }

            return response()->json([
            'success' => '<strong>Success!</strong> Data updated for Settings  <a href="'.route($this->route_master.'show', $setting->_id).'" class="btn btn-sm btn-info" title="View this Setting" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
            ]);
        } 
        return response()->json(['error'=>$validator->errors()->getMessages()]);
    }
     
     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  $incident = Setting::find($id); 
       $incident->delete();
        
        return response()->json([
            'success' => '<strong>Success!</strong> setting has been deleted temporarily!'
        ]);
    }

    /**
     * Mark as deleted the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $incident = Setting::withTrashed()->find($id);
        $incident->forceDelete();
        return response()->json([
            'success' => '<strong>Success!</strong> setting has been deleted permanently!'
        ]);
    }

    /**
     * Restores deleted marked resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        Setting::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> setting has been restored!'
        ]);
    }
}
