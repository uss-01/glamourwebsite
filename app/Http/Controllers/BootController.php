<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BootController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {    	
        $this->bladeVar['page']['title'] = 'Dashboard';
        unset($this->bladeVar['page']['breadcrumb']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('boot', ['bladeVar' => $this->bladeVar]);
    }
}
