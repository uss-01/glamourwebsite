<?php

namespace App\Http\Controllers\Support;

use Auth;
use Validator;
use App\Support\LostFound;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class LostFoundController extends Controller
{
   protected $sortable = [
        'id' => '_id',
        'name' => 'name',
        'email' => 'email',
        'phone' => 'phone'
    ];

    protected $validation_rules = [
        'name' => 'required|max:100',
        'phone' => 'required|max:50',
    ];

    protected $pageTitle = 'Lost Found';
    protected $activeNav = [
        'nav' => 'lostfound',
        'sub' => 'lostfound'

    ];
    protected $upload_path = 'images/support/lostfound';
    protected $breadcrumb_master = 'Lost Found';
    protected $view_master = 'support.lostfound.';
    protected $route_master = 'lostfound.';

   public function __construct()
    {

     $this->bladeVar['page']['title'] = $this->pageTitle;
     $this->bladeVar['page']['activeNav'] = $this->activeNav;
     $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
     $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = LostFound::withTrashed()
           ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'DESC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = LostFound::withTrashed()->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = LostFound::withTrashed()
            ->where('title', 'like', '%'.$request->q.'%')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = LostFound::withTrashed()
            ->where('title', 'like', '%'.$request->q.'%')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $input = $request->all();
        $this->$validation_rules['image'] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048';
       $validator = Validator::make($request->all(),$this->validation_rules);
          if ($validator->passes()) {
        $image_title = str_replace(' ', '-',strtolower($request->name));
        $input['image'] = time().'_'.$image_title.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path($upload_path), $input['image']);

        $lostfound = LostFound::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'message' => $request->message,
            'image1'=> $input['image'],
             'created_by'=>Auth::id(),

        ]);

        return response()->json([
                'success' => '<strong>Success!</strong> data saved for lost found <strong>'.$request->title.'</strong><a href="'.route($this->route_master.'show', $lostfound->_id).'" class="btn btn-sm btn-info" title="View this lost found" data-toggle="modal" data-target="#viewModal"> Click here to view it</a>'

            ]);
     }


      return response()->json(['error'=>$validator->errors()->getMessages()]);

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = LostFound::withTrashed()->find($id);

            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = LostFound::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

          if (isset($request->image)) {
            $validator = Validator::make($request->all(),[
       'name' => 'required|max:100',
       'type' => 'required',
        'phone' => 'required|max:50',
        'image'=> 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);
            if($validator->passes()){

                $image_title = str_replace(' ', '-',strtolower($request->name));
                $input['image'] = time().'_'.$image_title.'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('images/support/lostfound'), $input['image']);
                $lostfound = LostFound::find($id);
                File::delete(public_path().'/'.'images/support/lostfound'.'/'.$lostfound->image);
                $lostfound->update([
                'name' => $request->name,
                'type'=> $request->type,
                'email' => $request->email,
                'phone' => $request->phone,
                'message' => $request->message,
                'image1'=> $input['image'],
                'updated_by'=>Auth::id(),
                ]);

                return response()->json([
                'success' => '<strong>Success!</strong> Data updated for lost found <strong>'.$request->title.'</strong> <a href="'.route($this->route_master.'show', $lostfound->_id).'" class="btn btn-sm btn-info" title="View this lost found" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
                ]);
            }
            return response()->json(['error'=>$validator->errors()->getMessages()]);
     }
     else{
        $validator = Validator::make($request->all(),[
       'name' => 'required|max:100',
       'name' => 'required|max:100',
       'type' => 'required',
       'phone' => 'required|max:50',
      ]);
         if($validator->passes()){

            $lostfound = LostFound::find($id);
            $lostfound->update([
            'name' => $request->name,
            'type'=> $request->type,
            'email' => $request->email,
            'phone' => $request->phone,
            'message' => $request->message,
            'updated_by'=>Auth::id(),
            ]);

            return response()->json([
            'success' => '<strong>Success!</strong> Data updated for lost found <strong>'.$request->title.'</strong> <a href="'.route($this->route_master.'show', $lostfound->_id).'" class="btn btn-sm btn-info" title="View this lost found" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
            ]);
    }
     return response()->json(['error'=>$validator->errors()->getMessages()]);
     }


    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  $lostfound = LostFound::find($id);
       $lostfound->delete();

        return response()->json([
            'success' => '<strong>Success!</strong> lost found has been deleted temporarily!'
        ]);
    }

    /**
     * Mark as deleted the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $lostfound = LostFound::withTrashed()->find($id);
        $lostfound->forceDelete();
        File::delete(public_path().'/'.'images/support/lostfound'.'/'.$lostfound->image1);
        return response()->json([
            'success' => '<strong>Success!</strong> lost found has been deleted permanently!'
        ]);
    }

    /**
     * Restores deleted marked resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        LostFound::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> lost found has been restored!'
        ]);
    }
}
