<?php

namespace App\Http\Controllers\Support;
use Auth;
use App\Support\Management;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManagementController extends Controller
{
   protected $sortable = [
        'id' => '_id',
        'email' => 'email'
    ];

    protected $pageTitle = 'Management Email';
    protected $activeNav = [
        'nav' => 'management',
        'sub' => 'management'

    ];
    protected $breadcrumb_master = 'Management Email';
    protected $view_master = 'support.management.';
    protected $route_master = 'management.';

   public function __construct()
    {

     $this->bladeVar['page']['title'] = $this->pageTitle;
     $this->bladeVar['page']['activeNav'] = $this->activeNav;
     $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
     $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){   
	    $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = Management::withTrashed()
           ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'DESC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Management::withTrashed()->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }
	
	    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Management::withTrashed()
            ->where('email', 'like', '%'.$request->q.'%')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Management::withTrashed()
            ->where('email', 'like', '%'.$request->q.'%')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }
	
	    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $management = Management::create([
             'email' => $request->email,
             'created_by'=> Auth::id(),
        ]);
        return response()->json(['success' => '<strong>Success!</strong> data saved for management email <strong>'.$request->email.'</strong><a href="'.route($this->route_master.'show', $management->_id).'" class="btn btn-sm btn-info" title="View this management email" data-toggle="modal" data-target="#viewModal"> Click here to view it</a>']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Management::withTrashed()->find($id);

            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Management::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
		$management = Management::find($id);
		$management->update([
		'email' => $request->email,
		'updated_by'=>Auth::id(),
		]);
		return response()->json([
		'success' => '<strong>Success!</strong> Data updated for management email <strong>'.$request->email.'</strong> <a href="'.route($this->route_master.'show', $management->_id).'" class="btn btn-sm btn-info" title="View this management email" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
		]);
    
     
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){ 
	   $management = Management::find($id);
       $management->delete();

        return response()->json([
            'success' => '<strong>Success!</strong> management email has been deleted temporarily!'
        ]);
    }

    /**
     * Mark as deleted the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id){
        $management = Management::withTrashed()->find($id);
        $management->forceDelete();
        return response()->json([
            'success' => '<strong>Success!</strong> management email has been deleted permanently!'
        ]);
    }

    /**
     * Restores deleted marked resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id){
        Management::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> management has been restored!'
        ]);
    }	

}
