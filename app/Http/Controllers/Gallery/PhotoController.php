<?php

namespace App\Http\Controllers\Gallery;

use Auth;
use App\Gallery\Photo;
use App\Gallery\Album;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Validator;

class PhotoController extends Controller
{
     protected $sortable = [
        'id' => '_id',
        'title' => 'title',
        'album'=>'album_id'
    ];


    protected $pageTitle = 'Photo';
    protected $activeNav = [
        'nav' => 'gallery',
        'sub' => 'photo'
    ];
    protected $breadcrumb_master = 'Photo';
    protected $view_master = 'gallery.photo.';
    protected $route_master = 'photo.';

   public function __construct()
    {
         
     $this->bladeVar['page']['title'] = $this->pageTitle;
     $this->bladeVar['page']['activeNav'] = $this->activeNav;
     $this->bladeVar['page']['breadcrumb'] = ['Home' => '/', 'Gallery' => '/gallery'];
     $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
     $this->cache();
    }

     protected function cache() 
    {
        $time = Carbon::now()->addHours(1);
        $this->bladeVar['albums'] = Cache::remember('albums', $time, function () {
            return Album::orderBy('name', 'asc')->get();
        });
        
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = Photo::withTrashed()
            ->join('gallery_albums as a', 'a._id', '=', 'gallery_images.album_id')
            ->select('gallery_images.*')
            ->with('album')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Photo::withTrashed()->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Photo::withTrashed()
            ->where('title', 'like', '%'.$request->q.'%')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Photo::withTrashed()
            ->where('title', 'like', '%'.$request->q.'%')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   


        $input = $request->all();
        $rules = [
            //'title' => 'required'
        ];
        $photo_cnt = count($request->image);
        foreach(range(0, ($photo_cnt-1)) as $index) {
            $rules['image.' . $index] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->passes()) {
            $cnt = 1;
			$request->title = ($request->title) ? $request->title : 'photo';
            $album_image = $request->image[0];  // getting first image for album cover
            $album_image_name = str_replace(' ', '-',strtolower($request->title));
            $album_image_name = time().'_'.$album_image_name.'.'.$album_image->getClientOriginalExtension();
            $album_image->move(public_path('images/galleries/albums'), $album_image_name);
        
         if(!isset($request->album)){
            $album = Album::create([
            'name' => $request->title,
            'image'=> $album_image_name,
             'created_by'=>Auth::id(),
           
        ]);
          $request->album = $album->_id;  
        }
        foreach ($request->image as $photo) {
            $image_name = str_replace(' ', '-',strtolower($request->title));
            $image_name = $image_name.'-'.$cnt;
            $image_fullname = time().'_'.$image_name.'.'.$photo->getClientOriginalExtension();
            if($cnt == 1){
            File::copy(public_path('images/galleries/albums').'/'.$album_image_name, public_path('images/galleries/photos').'/'.$image_fullname);
            }else{
            $photo->move(public_path('images/galleries/photos'), $image_fullname);
            }
            $photo = Photo::create([
                'title' => $request->title,
                'album_id' => $request->album,
                'image'=> $image_fullname,
                'created_by'=>Auth::id(),
               
            ]);
            $cnt++;
        }

        return response()->json([
                'success' => '<strong>Success!</strong> data saved for photo <strong>'.$request->title.'</strong><a href="'.route($this->route_master.'show', $photo->_id).'" class="btn btn-sm btn-info" title="View this photo" data-toggle="modal" data-target="#viewModal"> Click here to view it</a>'
        
            ]);
     }
      

      return response()->json(['error'=>$validator->errors()->getMessages()]);

    }

   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Photo::withTrashed()->find($id);
            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Photo::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
        'title' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);

          if (isset($request->image)) {
            if($validator->passes()){

        $image_name = str_replace(' ', '-',strtolower($request->title));
        $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/galleries/photos'), $input['image']);
        $photo = Photo::find($id);
         File::delete(public_path().'/images/galleries/photos/'.$photo->image);
        $photo->update([
            'title' => $request->title,
            'album_id' => $request->album,
            'image'=> $input['image'],
             'updated_by'=>Auth::id(),
        ]);
      
         return response()->json([
            'success' => '<strong>Success!</strong> Data updated for photo <strong>'.$request->title.'</strong> <a href="'.route($this->route_master.'show', $photo->_id).'" class="btn btn-sm btn-info" title="View this album" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);
     } 
       return response()->json(['error'=>$validator->errors()->getMessages()]);
     }
     else{
        $validator = Validator::make($request->all(), [
        'title' => 'required',
      ]);
         if($validator->passes()){

        $photo = Photo::find($id);
        $photo->update([
            'title' => $request->title,
            'album_id' => $request->album,
             'updated_by'=>Auth::id(),
        ]);

         return response()->json([
            'success' => '<strong>Success!</strong> Data updated for photo <strong>'.$request->title.'</strong> <a href="'.route($this->route_master.'show', $photo->_id).'" class="btn btn-sm btn-info" title="View this album" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);
    }
     return response()->json(['error'=>$validator->errors()->getMessages()]);
     }
        
       
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  $photo = Photo::find($id); 
       $photo->delete();
        
        return response()->json([
            'success' => '<strong>Success!</strong> Photo has been deleted temporarily!'
        ]);
    }

    /**
     * Mark as deleted the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $photo = Photo::withTrashed()->find($id);
        File::delete(public_path().'/images/galleries/photos/'.$photo->image);
        $photo->forceDelete();
        
        return response()->json([
            'success' => '<strong>Success!</strong> Photo has been deleted permanently!'
        ]);
    }

    /**
     * Restores deleted marked resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        Photo::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> Photo has been restored!'
        ]);
    }
}
