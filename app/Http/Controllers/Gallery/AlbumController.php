<?php

namespace App\Http\Controllers\Gallery;
use Auth;
use App\Gallery\Album;
use App\Gallery\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use Validator;
class AlbumController extends Controller
{
     protected $sortable = [
        'id' => '_id',
        'name' => 'name'
    ];

    protected $validation_rules = [
        'name' => 'required|max:100',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ];

    protected $pageTitle = 'Album';
    protected $activeNav = [
        'nav' => 'gallery',
        'sub' => 'album'
    ];
    protected $breadcrumb_master = 'Album';
    protected $view_master = 'gallery.album.';
    protected $route_master = 'album.';

   public function __construct()
    {
         
     $this->bladeVar['page']['title'] = $this->pageTitle;
     $this->bladeVar['page']['activeNav'] = $this->activeNav;
     $this->bladeVar['page']['breadcrumb'] = ['Home' => '/', 'Gallery' => '/gallery'];
     $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = Album::withTrashed()
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Album::withTrashed()->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Album::withTrashed()
            ->where('name', 'like', '%'.$request->q.'%')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Album::withTrashed()
            ->where('name', 'like', '%'.$request->q.'%')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   


        $input = $request->all();
        $validator = Validator::make($request->all(), [
        'name' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);
          if ($validator->passes()) {
        $image_name = str_replace(' ', '-',strtolower($request->name));
        $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/galleries/albums'), $input['image']);

        $album = Album::create([
            'name' => $request->name,
            'image'=> $input['image'],
             'created_by'=>Auth::id(),
           
        ]);

        return response()->json([
                'success' => '<strong>Success!</strong> data saved for album <strong>'.$request->name.'</strong><a href="'.route($this->route_master.'show', $album->_id).'" class="btn btn-sm btn-info" title="View this album" data-toggle="modal" data-target="#viewModal"> Click here to view it</a>'
        
            ]);
     }
      

      return response()->json(['error'=>$validator->errors()->getMessages()]);

    }

   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Album::withTrashed()->find($id);
            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Album::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
        'name' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);

          if (isset($request->image)) {
            if($validator->passes()){

        $image_name = str_replace(' ', '-',strtolower($request->name));
        $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/galleries/albums'), $input['image']);
        $album = Album::find($id);
         File::delete(public_path().'/images/galleries/albums/'.$album->image);
        $album->update([
            'name' => $request->name,
            'image'=> $input['image'],
             'updated_by'=>Auth::id(),
        ]);
      
         return response()->json([
            'success' => '<strong>Success!</strong> Data updated for album <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $album->_id).'" class="btn btn-sm btn-info" title="View this album" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);
     } 
       return response()->json(['error'=>$validator->errors()->getMessages()]);
     }
     else{
        $validator = Validator::make($request->all(), [
        'name' => 'required',
      ]);
         if($validator->passes()){

        $album = Album::find($id);
        $album->update([
            'name' => $request->name,
             'updated_by'=>Auth::id(),
        ]);

         return response()->json([
            'success' => '<strong>Success!</strong> Data updated for album <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $album->_id).'" class="btn btn-sm btn-info" title="View this album" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);
    }
     return response()->json(['error'=>$validator->errors()->getMessages()]);
     }
        
       
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  $album = Album::find($id); 
       $album->delete();
       $photo = Photo::where('album_id','=',$id)->delete(); 
        return response()->json([
            'success' => '<strong>Success!</strong> Album has been deleted temporarily!'
        ]);
    }

    /**
     * Mark as deleted the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $album = Album::withTrashed()->find($id);
        File::delete(public_path().'/images/galleries/albums/'.$album->image);
        $album->forceDelete();
       $photo = Photo::where('album_id','=',$id); 
       foreach ($photo as $row) {
           File::delete(public_path().'/images/galleries/photos/'.$row->image);

       }
       $photo->forceDelete();
        return response()->json([
            'success' => '<strong>Success!</strong> Album has been deleted permanently!'
        ]);
    }

    /**
     * Restores deleted marked resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        Album::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> Album has been restored!'
        ]);
    }
}
