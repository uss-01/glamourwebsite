<?php

namespace App\Http\Controllers\Entertainment;
use Auth;
use DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use App\Masters\Category;
use App\Masters\Brand;
use Validator;
use Image;

class EntertainmentController extends Controller
{
     protected $sortable = [
        'id' => 'brand_id',
        'name' => 'name'
    ];

    protected $validation_rules = [
        'name' => 'required|max:100',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ];

    protected $pageTitle = 'Entertainment';
    protected $activeNav = [
        'nav' => 'entertainment',
        'sub' => 'entertainment'
    ];
    protected $breadcrumb_master = 'Entertainment';
    protected $view_master = 'entertainment.';
    protected $route_master = 'entertainment.';

   public function __construct()
    {
         
     $this->bladeVar['page']['title'] = $this->pageTitle;
     $this->bladeVar['page']['activeNav'] = $this->activeNav;
     $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
     $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = Brand::withTrashed()
            ->where('category_id','=','3')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Brand::withTrashed()->where('category_id','=','3')->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Brand::withTrashed()
            ->where('name', 'like', '%'.$request->q.'%')
            ->where('category_id','=','3')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Brand::withTrashed()
            ->where('name', 'like', '%'.$request->q.'%')
            ->where('category_id','=','3')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   


        $input = $request->all();
        $validator = Validator::make($request->all(), [
        'name' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);
          if ($validator->passes()) {
        $image_name = str_replace(' ', '-',strtolower($request->name));
        $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/brands'), $input['image']);

        $brand = Brand::create([
            'name' => $request->name,
            'category_id' =>'3',
            'description'=>$request->description,
            'image'=> $input['image'],
             'created_by'=>Auth::id(),
           
        ]);

        return response()->json([
                'success' => '<strong>Success!</strong> data saved for brand <strong>'.$request->name.'</strong><a href="'.route($this->route_master.'show', $brand->brand_id).'" class="btn btn-sm btn-info" title="View this brand" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        
            ]);
     }
      

      return response()->json(['error'=>$validator->errors()->getMessages()]);

    }

   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Brand::withTrashed()->find($id);
            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Brand::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
        'name' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);

          if (isset($request->image)) {
            if($validator->passes()){

        $image_name = str_replace(' ', '-',strtolower($request->name));
        $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/brands'), $input['image']);
        $brand = Brand::find($id);
         File::delete(public_path().'/images/brands/'.$brand->image);
        $brand->update([
            'name' => $request->name,
            'category_id' =>'3',
            'description'=>$request->description,
            'image'=> $input['image'],
             'updated_by'=>Auth::id(),
        ]);
      
         return response()->json([
            'success' => '<strong>Success!</strong> Data updated for brand <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $brand->brand_id).'" class="btn btn-sm btn-info" title="View this brand" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);
     } 
       return response()->json(['error'=>$validator->errors()->getMessages()]);
     }
     else{
        $validator = Validator::make($request->all(), [
        'name' => 'required',
      ]);
         if($validator->passes()){

        $brand = Brand::find($id);
        $brand->update([
            'name' => $request->name,
             'description'=>$request->description,
             'updated_by'=>Auth::id(),
        ]);

         return response()->json([
            'success' => '<strong>Success!</strong> Data updated for brand <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $brand->brand_id).'" class="btn btn-sm btn-info" title="View this brand" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);
    }
     return response()->json(['error'=>$validator->errors()->getMessages()]);
     }
        
       
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  $brand = Brand::find($id); 
       $brand->delete();
        
        return response()->json([
            'success' => '<strong>Success!</strong> Brand has been deleted temporarily!'
        ]);
    }

    /**
     * Mark as deleted the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $brand = Brand::withTrashed()->find($id);
        $brand->forceDelete();
        File::delete(public_path().'/images/brands/'.$brand->image);
        return response()->json([
            'success' => '<strong>Success!</strong> Brand has been deleted permanently!'
        ]);
    }

    /**
     * Restores deleted marked resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        Brand::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> Brand has been restored!'
        ]);
    }
}
