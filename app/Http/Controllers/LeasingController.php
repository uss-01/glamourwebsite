<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\User;
use App\Tenant;
use App\Leasing;
use App\Shop\Shop;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;

class LeasingController extends Controller
{
    protected $breadcrumb_master = 'Leasing';
    protected $view_master = 'leasing.';
    protected $route_master = 'leasing.';
    protected $pageTitle = 'Leasing';
    protected $activeNav = [
        'nav' => 'leasing',
        'sub' => 'leasing'
    ];
    protected $sortable = [
        'id' => 'id',
        'title' => 'name'
    ];
    public function __construct()
    {
       $this->bladeVar['page']['title'] = $this->pageTitle;
       $this->bladeVar['page']['activeNav'] = $this->activeNav;
       $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
       $this->cache();
   }

   protected function cache(){
        $time = Carbon::now()->addHours(1);
        $this->bladeVar['shops'] = Cache::remember('shops', $time, function () {
            return Shop::orderBy('name', 'asc')->get();
        });
    }

    public function index(Request $request){
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = Tenant::withTrashed()
                                    ->select('tenants.*')
                                    ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
                                    ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Tenant::withTrashed()->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    public function search(Request $request){
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Tenant::withTrashed()
            ->where('name', 'like', '%'.$request->q.'%')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Tenant::withTrashed()
            ->where('name', 'like', '%'.$request->q.'%')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request){
        if ($request->ajax()) {
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id){
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '/leasing';
        $this->bladeVar['leasingData'] = Leasing::where(['tenant_id' => $id])
                                    ->select('leasing.*')
                                    ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
                                    ->paginate($this->paginate_total);
        $this->bladeVar['result'] = Tenant::withTrashed()->find($id);
        $this->bladeVar['total_count'] = count($this->bladeVar['leasingData']);
        return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id){
        if ($request->ajax()) {
            $this->bladeVar['result'] = Tenant::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }
    
    public function editLeasing(Request $request, $id){
        if ($request->ajax()) {
            $leasingData = Leasing::find($id);
            $tenantData = Tenant::find($leasingData->tenant_id);
            $this->bladeVar['resultLeasing'] = $leasingData;
            $this->bladeVar['resultTenant'] = $tenantData;
            return view($this->view_master.'editLeasing', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }
    

    public function store(Request $request){
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'rent_amount' => 'required',
            'payment_status' => 'required',
            'payment_method' => 'required',
        ]);
        if ($validator->passes()) {
            $shopArray = array();
            for($i=0; $i < count($request->shop_name); $i++){
                $shop = Shop::find($request->shop_name[$i]);  
                $shopArray[$i]['_id'] = $request->shop_name[$i];   
                $shopArray[$i]['name'] = $shop->name;
            }
            $shopNameArray = json_encode($shopArray);
            if(!empty($request->pending_amount)){
                $pendingAmount = $request->pending_amount;
            }else{
                $pendingAmount = '0.00';
            }
            $leasing = Leasing::create([
                'tenant_id' => $request->id,
                'rent_amount' => $request->rent_amount,
                'payment_status' => $request->payment_status,
                'payment_method' => $request->payment_method,
                'pending_amount' => $pendingAmount,
                'shops' => $shopNameArray,
            ]);
            return response()->json([
                'success' => '<strong>Success!</strong> data saved for leasing'
            ]);
        }
        return response()->json(['error'=>$validator->errors()->getMessages()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'rent_amount' => 'required',
            'payment_status' => 'required',
            'payment_method' => 'required',
        ]);
        $shopArray = array();
        for($i=0; $i < count($request->shop_name); $i++){
            $shop = Shop::find($request->shop_name[$i]);  
            $shopArray[$i]['_id'] = $request->shop_name[$i];   
            $shopArray[$i]['name'] = $shop->name;
        }
        $shopNameArray = json_encode($shopArray);
        if(!empty($request->pending_amount)){
            $pendingAmount = $request->pending_amount;
        }else{
            $pendingAmount = '0.00';
        }
        if($validator->passes()){
            $leasing = Leasing::find($id);
            $leasing->update([
                'shops' => $shopNameArray,
                'rent_amount' => $request->rent_amount,
                'payment_status' => $request->payment_status,
                'payment_method' => $request->payment_method,
                'pending_amount' => $pendingAmount
            ]);
            return response()->json([
                'success' => '<strong>Success!</strong> Data updated for Leasing'
            ]);
        }
        return response()->json(['error'=>$validator->errors()->getMessages()]);
    }

    public function delete($id){
        Leasing::withTrashed()->find($id)->forceDelete();
        return response()->json([
            'success' => '<strong>Success!</strong> Leasing has been deleted permanently!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        Leasing::find($id)->delete();
        return response()->json([
            'success' => '<strong>Success!</strong> Leasing has been deleted temporarily!'
        ]);
    }

    public function restore($id){
        Leasing::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> Leasing has been restored!'
        ]);
    }
}
