<?php

namespace App\Http\Controllers;
use Auth;
use Validator;
use App\Cms;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CmsController extends Controller
{
    protected $sortable = [
        'id' => '_id',
        'name' => 'name',
        'title' => 'title',
       
    ];

    protected $validation_rules = [
        'name' => 'required|max:100',
        'title' => 'required|max:200',
       
    ];

    protected $validation_messages = [
        'name.required' => 'Please Enter Page Name',
        'title.required' => 'Please Enter Page Title'
    ];

    protected $pageTitle = 'CMS';
    protected $activeNav = [
        'nav' => 'cms',
        'sub' => 'cms'
       
    ];
    protected $breadcrumb_master = 'CMS';
    protected $view_master = 'cms.';
    protected $route_master = 'cms.';

   public function __construct()
    {
         
     $this->bladeVar['page']['title'] = $this->pageTitle;
     $this->bladeVar['page']['activeNav'] = $this->activeNav;
     $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
     $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = Cms::withTrashed()
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Cms::withTrashed()->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Cms::withTrashed()
            ->where('title', 'like', '%'.$request->q.'%')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Cms::withTrashed()
            ->where('title', 'like', '%'.$request->q.'%')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   if(isset($request->image)){
        $this->validation_rules['image'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'; 
       }
        $validator = Validator::make($request->all(), $this->validation_rules);
       if ($validator->passes()) {
        $slug = str_slug($request->name,'-');
        if(isset($request->image)){
        $input['image'] = time().'_'.$slug.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/cms'), $input['image']);
        }
        else{
            $input['image']  = NULL;
        }
        $cms = Cms::create([
            'name' => $request->name,
            'slug' => $slug,
            'title' => $request->title,
            'image'=>$input['image'],
            'content' => $request->content,
             'created_by'=>Auth::id(),
           
        ]);

        return response()->json([
                'success' => '<strong>Success!</strong> data saved for Cms <strong>'.$request->name.'</strong><a href="'.route($this->route_master.'show', $cms->_id).'" class="btn btn-sm btn-info" title="View this Cms" data-toggle="modal" data-target="#viewModal"> Click here to view it</a>'
        
            ]);
    }
    return response()->json(['error'=>$validator->errors()->getMessages()]);

    }

   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Cms::withTrashed()->find($id);
            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Cms::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   if(isset($request->image)){
        $this->validation_rules['image'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'; 
       }

        $validator = Validator::make($request->all(), $this->validation_rules);
       if ($validator->passes()) {
       
        $slug = str_slug($request->name,'-');
         $cms = Cms::find($id);
         if(isset($request->image)){
        $input['image'] = time().'_'.$slug.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/cms'), $input['image']);

         $cms->update([
            'name' => $request->name,
            'slug' => $slug,
            'title' => $request->title,
             'image'=>$input['image'],
            'content' => $request->content,
            'updated_by'=>Auth::id(),
        ]);
        }
        else{
             $cms->update([
            'name' => $request->name,
            'slug' => $slug,
            'title' => $request->title,
            'content' => $request->content,
            'updated_by'=>Auth::id(),
        ]);
        }

       
       
      
         return response()->json([
            'success' => '<strong>Success!</strong> Data updated for Cms <strong>'.$request->title.'</strong> <a href="'.route($this->route_master.'show', $cms->_id).'" class="btn btn-sm btn-info" title="View this Cms" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);

    } 
         return response()->json(['error'=>$validator->errors()->getMessages()]);
       
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  $Cms = Cms::find($id); 
       $Cms->delete();
        
        return response()->json([
            'success' => '<strong>Success!</strong> Cms has been deleted temporarily!'
        ]);
    }

    /**
     * Mark as deleted the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $Cms = Cms::withTrashed()->find($id);
        $Cms->forceDelete();
        return response()->json([
            'success' => '<strong>Success!</strong> Cms has been deleted permanently!'
        ]);
    }

    /**
     * Restores deleted marked resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        Cms::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> Cms has been restored!'
        ]);
    }
}
