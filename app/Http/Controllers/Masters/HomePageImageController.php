<?php

namespace App\Http\Controllers\Masters;

use Auth;
use Validator;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
class HomePageImageController extends Controller
{

    protected $sortable = [
        'id' => 'id',
        'name' => 'title'
    ];
    protected $validation_rules = [
        'title' => 'required|max:255|unique:slider_images',
        'slug' => 'required|max:255|unique:slider_images',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ];

    protected $pageTitle = 'Home Slider';
    protected $activeNav = [
        'nav' => 'home page',
        'sub' => 'images'
    ];
    protected $breadcrumb_master = 'Home Slider';
    protected $view_master = 'master.homepageimage.';
    protected $route_master = 'homepageimage.';

    public function __construct()
    {
       $this->bladeVar['page']['title'] = $this->pageTitle;
       $this->bladeVar['page']['activeNav'] = $this->activeNav;
       $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
       $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
   }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = \App\Masters\SliderImages::withTrashed()
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = \App\Masters\SliderImages::withTrashed()->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = \App\Masters\SliderImages::withTrashed()
            ->where('title', 'like', '%'.$request->q.'%')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = \App\Masters\SliderImages::withTrashed()
            ->where('title', 'like', '%'.$request->q.'%')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255|unique:slider_images',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($validator->passes()) {
            $image_name = str_replace(' ', '-',strtolower($request->title));
            $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('images/cms'), $input['image']);
            if(!empty($request->shortDescription)){
                $shortDescription = $request->shortDescription; 
            }else{
                $shortDescription = ' '; 
            }
            if(!empty($request->redirect_url)){
                $redirect_url = $request->redirect_url; 
            }else{
                $redirect_url = ' '; 
            }
            if(!empty($request->btntext)){
                $btntext = $request->btntext; 
            }else{
                $btntext = ' '; 
            } 
            $slug = strip_tags($request->title);
            $brand = \App\Masters\SliderImages::create([
                'title' => $request->title,
                'slug' => $this->removeSpecialChapr($slug),
                'shortDescription' => $shortDescription,
                'image'=> $input['image'],
                'redirect_url'=> $redirect_url,
                'slidePosition'=>$request->slidePosition,
                'btntext' => $btntext,
                'created_by'=>Auth::id(),
            ]);
            return response()->json([
                'success' => '<strong>Success!</strong> data saved for slider image <strong>'.$request->title.'</strong><a href="'.route($this->route_master.'show', $brand->id).'" class="btn btn-sm btn-info" title="View this slider image" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'

            ]);
        }
        return response()->json(['error'=>$validator->errors()->getMessages()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = \App\Masters\SliderImages::withTrashed()->find($id);
            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = \App\Masters\SliderImages::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
        ]);

        if (isset($request->image)) {
            if($validator->passes()){
                $image_name = str_replace(' ', '-',strtolower($request->title));
                $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('images/cms'), $input['image']);
                $slider = \App\Masters\SliderImages::find($id);
                File::delete(public_path().'/images/cms/'.$slider->image);
                if(!empty($request->shortDescription)){
                    $shortDescription = $request->shortDescription; 
                }else{
                    $shortDescription = ' '; 
                }
                if(!empty($request->redirect_url)){
                    $redirect_url = $request->redirect_url; 
                }else{
                    $redirect_url = ' '; 
                }
                if(!empty($request->btntext)){
                    $btntext = $request->btntext; 
                }else{
                    $btntext = ' '; 
                } 
                $slug = strip_tags($request->title);
                $slider->update([
                    'title' => $request->title,
                    'slug' => $this->removeSpecialChapr($slug),
                    'image'=> $input['image'],
                    'shortDescription' => $shortDescription,
                    'redirect_url'=>$redirect_url,
                    'slidePosition'=>$request->slidePosition,
                    'btntext' => $btntext,
                    'updated_by'=>Auth::id(),
                ]);

                return response()->json([
                    'success' => '<strong>Success!</strong> Data updated for slider image <strong>'.$request->title.'</strong> <a href="'.route($this->route_master.'show', $slider->id).'" class="btn btn-sm btn-info" title="View this slider image" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
                ]);
            } 
            return response()->json(['error'=>$validator->errors()->getMessages()]);
        }
        else{
            $validator = Validator::make($request->all(), [
                'title' => 'required|max:255',
            ]);
            if($validator->passes()){

                $slider = \App\Masters\SliderImages::find($id);
                if(!empty($request->shortDescription)){
                    $shortDescription = $request->shortDescription; 
                }else{
                    $shortDescription = ' '; 
                }
                if(!empty($request->redirect_url)){
                    $redirect_url = $request->redirect_url; 
                }else{
                    $redirect_url = ' '; 
                }
                if(!empty($request->btntext)){
                    $btntext = $request->btntext; 
                }else{
                    $btntext = ' '; 
                } 
                $slug = strip_tags($request->title);
                $slider->update([
                    'title' => $request->title,
                    'slug' => $this->removeSpecialChapr($slug),
                    'shortDescription' => $shortDescription,
                    'redirect_url'=>$redirect_url,
                    'slidePosition'=>$request->slidePosition,
                    'btntext' => $btntext,
                    'updated_by'=>Auth::id(),
                ]);

                return response()->json([
                    'success' => '<strong>Success!</strong> Data updated for slider image <strong>'.$request->title.'</strong> <a href="'.route($this->route_master.'show', $slider->id).'" class="btn btn-sm btn-info" title="View this slider image" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
                ]);
            }
            return response()->json(['error'=>$validator->errors()->getMessages()]);
        }
        
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Masters\SliderImages::find($id)->delete();
        return response()->json([
            'success' => '<strong>Success!</strong> slider has been deleted temporarily!'
        ]);
    }

    /**
     * Restores deleted marked resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        \App\Masters\SliderImages::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> slider has been restored!'
        ]);
    }

    public function delete($id){
        $offer = \App\Masters\SliderImages::withTrashed()->find($id);
        $offer->forceDelete();
        File::delete(public_path().'/images/cms/'.$offer->image);
        return response()->json([
            'success' => '<strong>Success!</strong> slider has been deleted permanently!'
        ]);
    }

    public function removeSpecialChapr($value){
		$title = str_replace( array( '\'', '"', ',' , ';', '<', '>','!', '@', '#' , '$', '%', '^', '&', '*' , '(', ')', '_', '-', '=' , '+', ':', '?', '.', '`', '~', '[', ']', '{', '}', '|' , '/' , '\\' , '‘' , '’' , '“', '”' , '…', '‰' ), '', $value);
		$post_title1 = str_replace( array("  "), array(" "), $title);	
		$post_title = str_replace( array(" ","'"), array("-",""), $post_title1);
		$postTitle = strtolower($post_title);
		return $postTitle;
	}	
}
