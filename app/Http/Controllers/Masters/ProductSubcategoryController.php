<?php

namespace App\Http\Controllers\Masters;
use Auth;
use Validator;
use DB;
use App\Masters\ProductSubcategory;
use App\Masters\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class ProductSubcategoryController extends Controller
{
     protected $sortable = [
        'id' => '_id',
        'name' => 'name',
        'product_category' => 'pc.name',
    ];

    protected $validation_rules = [
        'name' => 'required|max:255|unique:product_subcategories',
        'product_category' => 'required|integer',
    ];

    protected $pageTitle = 'Subcategory';
    protected $activeNav = [
        'nav' => 'master',
        'sub' => 'product_subcategory'
    ];
    protected $breadcrumb_master = 'Subcategory';
    protected $view_master = 'master.product_subcategory.';
    protected $route_master = 'product_subcategory.';

    public function __construct()
    {
         
     $this->bladeVar['page']['title'] = $this->pageTitle;
     $this->bladeVar['page']['activeNav'] = $this->activeNav;
     //$this->bladeVar['page']['breadcrumb'] = ['Home' => '/', 'Masters' => '/master'];
     $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
     $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
     $this->cache();
    }


    protected function cache() 
    {
        $time = Carbon::now()->addHours(1);
        $this->bladeVar['product_categories'] = Cache::remember('product_categories', $time, function () {
            return ProductCategory::where('type','=','Product')->orderBy('name', 'asc')->get();
        });
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = ProductSubcategory::withTrashed()
            ->where('product_subcategories.type','=','Product')
            ->join('product_categories as pc', 'pc._id', '=', 'product_subcategories.product_category_id')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->select('product_subcategories.*')
            ->with('product_category')
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = ProductSubcategory::withTrashed()
           ->where('product_subcategories.type','=','Product')->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = ProductSubcategory::withTrashed()
             ->where('product_subcategories.type','=','Product')
            ->where('name', 'like', '%'.$request->q.'%')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = ProductSubcategory::withTrashed()
            ->where('name', 'like', '%'.$request->q.'%')
             ->where('product_subcategories.type','=','Product')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:product_subcategories',
        ]);
        if ($validator->passes()) {
            $product_subcategory = ProductSubcategory::create([
                'name' => $request->name,
                'slug' => $this->removeSpecialChapr($request->name),
                'product_category_id' => $request->product_category,
                'type'=>'Product',
                'created_by'=>Auth::id(),
            ]);
            
            return response()->json([
                'success' => '<strong>Success!</strong> data saved for subcategory <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $product_subcategory->_id).'" class="btn btn-sm btn-info" title="View this subcategory" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
            ]);
        } 
        return response()->json(['error'=>$validator->errors()->getMessages()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = ProductSubcategory::withTrashed()->find($id);
            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = ProductSubcategory::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);
        if ($validator->passes()) {

            $product_subcategory = ProductSubcategory::find($id);
            $product_subcategory->update([
                'name' => $request->name,
                'slug' => $this->removeSpecialChapr($request->name),
                'product_category_id' => $request->product_category,
                'updated_by'=>Auth::id(),
            
            ]);
            
            return response()->json([
                'success' => '<strong>Success!</strong> Data updated for Subcategory <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $product_subcategory->_id).'" class="btn btn-sm btn-info" title="View this subcategory" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
            ]);
        } 
        return response()->json(['error'=>$validator->errors()->getMessages()]);
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductSubcategory::find($id)->delete();
        return response()->json([
            'success' => '<strong>Success!</strong> Subcategory has been deleted temporarily!'
        ]);
    }

    /**
     * Mark as deleted the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        ProductSubcategory::withTrashed()->find($id)->forceDelete();
        return response()->json([
            'success' => '<strong>Success!</strong> Subcategory has been deleted permanently!'
        ]);
    }

    /**
     * Restores deleted marked resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        ProductSubcategory::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> Subcategory has been restored!'
        ]);
    }

    public function removeSpecialChapr($value){
		$title = str_replace( array( '\'', '"', ',' , ';', '<', '>','!', '@', '#' , '$', '%', '^', '&', '*' , '(', ')', '_', '-', '=' , '+', ':', '?', '.', '`', '~', '[', ']', '{', '}', '|' , '/' , '\\' , '‘' , '’' , '“', '”' , '…', '‰' ), '', $value);
		$post_title1 = str_replace( array("  "), array(" "), $title);	
		$post_title = str_replace( array(" ","'"), array("-",""), $post_title1);
		$postTitle = strtolower($post_title);
		return $postTitle;
	}	
}
