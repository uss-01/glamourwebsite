<?php

namespace App\Http\Controllers\Masters;

use Auth;
use Validator;
use DB;
use App\Masters\ProductSubcategory;
use App\Masters\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class SingleBannerImageController extends Controller
{

    protected $sortable = [
        'id' => 'id',
        'name' => 'name',
        'product_category' => 'pc.name',
        'product_subcategory' => 'pcs.name',
    ];
    protected $validation_rules = [
        'name' => 'required|max:255|unique:single_banner_images',
        'slug' => 'required|max:255|unique:single_banner_images',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'product_category' => 'required|integer',
    ];
    protected $pageTitle = 'Single Banner Image';
    protected $activeNav = [
        'nav' => 'banner image',
        'sub' => 'images'
    ];
    protected $breadcrumb_master = 'Single Banner Image';
    protected $view_master = 'master.singlebannerimage.';
    protected $route_master = 'singlebannerimage.';

    public function __construct()
    {
       $this->bladeVar['page']['title'] = $this->pageTitle;
       $this->bladeVar['page']['activeNav'] = $this->activeNav;
       $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
       $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
    }

    protected function cache(){
        $time = Carbon::now()->addHours(1);
        $this->bladeVar['product_categories'] = Cache::remember('product_categories', $time, function () {
            return ProductCategory::where('type','=','Product')->orderBy('name', 'asc')->get();
        });
        $this->bladeVar['product_subcategories'] = Cache::remember('product_subcategories', $time, function () {
            return ProductSubcategory::where('type','=','Product')->orderBy('name', 'asc')->get();
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = \App\Masters\SingleBannerImage::withTrashed()
            ->join('product_categories as pc', 'pc._id', '=', 'single_banner_images.category')
            ->join('product_subcategories as pcs', 'pcs._id', '=', 'single_banner_images.subcategory')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->select('single_banner_images.*')
            ->with('product_category')
            ->with('product_subcategory')
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = \App\Masters\SingleBannerImage::withTrashed()->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = \App\Masters\SingleBannerImage::withTrashed()
            ->where('name', 'like', '%'.$request->q.'%')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = \App\Masters\SingleBannerImage::withTrashed()
            ->where('name', 'like', '%'.$request->q.'%')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            $this->bladeVar['product_categories'] = ProductCategory::where('type','=','Product')->orderBy('name', 'asc')->get();
            $this->bladeVar['product_subcategories'] = ProductSubcategory::where('type','=','Product')->orderBy('name', 'asc')->get();
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:single_banner_images',
            'product_category' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if($request->product_category == -1){
          $this->validation_rules['other_category'] = 'required';   
        }
        if($request->product_subcategory == -1){
          $this->validation_rules['other_subcategory'] = 'required';   
        }
        if ($validator->passes()) {
            $image_name = str_replace(' ', '-',strtolower($request->name));
            $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('images/cms'), $input['image']);
            
            // Other Product Category Insert
            if($request->product_category==-1){
            $product_category = ProductCategory::updateOrCreate(
                ['name' => $request->other_category,
                'slug' => $this->removeSpecialChapr($request->other_category),
                'type' => 'Product',
                'created_by'=>Auth::id()]
            );
            $request->product_category = $product_category->_id;
            }
            // Other Product Subcategory Insert
            if($request->product_subcategory==-1){
            $product_subcategory = ProductSubcategory::create(
                ['name' => $request->other_subcategory,
                'product_category_id' => $request->product_category,
                'slug' => $this->removeSpecialChapr($request->other_subcategory),
                'type' => 'Product',
                'created_by'=>Auth::id()]
            );
            $request->product_subcategory = $product_subcategory->_id;
            }
            if($request->product_subcategory == NULL){
                $request->product_subcategory = 0; 
            }
            if(!empty($request->shortDescription)){
                $shortDescription = $request->shortDescription; 
            }else{
                $shortDescription = ' '; 
            }
            if(!empty($request->btntext)){
                $btntext = $request->btntext; 
            }else{
                $btntext = ' '; 
            } 
            $brand = \App\Masters\SingleBannerImage::create([
                'name' => $request->name,
                'slug' => $this->removeSpecialChapr($request->name),
                'shortDescription' => $shortDescription,
                'category' => $request->product_category,
                'subcategory'=> $request->product_subcategory,
                'bannerPosition'=>$request->bannerPosition,
                'image'=> $input['image'],
                'btntext'=>$btntext,
                'created_by'=>Auth::id(),
            ]);
            return response()->json([
                'success' => '<strong>Success!</strong> data saved for single banner image <strong>'.$request->name.'</strong><a href="'.route($this->route_master.'show', $brand->id).'" class="btn btn-sm btn-info" title="View this single banner image" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
            ]);
        }
        return response()->json(['error'=>$validator->errors()->getMessages()]);
    }

    public function product_subcategory(Request $request){
        $this->validate($request, [
            'product_category' => 'required|integer'
        ]);
         $product_subcategories = ProductSubcategory::where('product_subcategories.product_category_id', '=', $request->product_category)
            ->select('_id', 'name')
            ->orderBy($this->sortable[$request->input('sort', 'name')], $request->input('dir', 'ASC'))->get();
        return response()->json($product_subcategories);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = \App\Masters\SingleBannerImage::withTrashed()->find($id);
            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['product_categories'] = ProductCategory::where('type','=','Product')->orderBy('name', 'asc')->get();
            $this->bladeVar['product_subcategories'] = ProductSubcategory::where('type','=','Product')->orderBy('name', 'asc')->get();
            $this->bladeVar['result'] = \App\Masters\SingleBannerImage::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'product_category' => 'required',
        ]);
        if($request->product_category==-1){
          $this->validation_rules['other_category'] = 'required';   
        }
        if($request->product_subcategory==-1){
          $this->validation_rules['other_subcategory'] = 'required';   
        }
        if (isset($request->image)) {
            if($validator->passes()){
                $image_name = str_replace(' ', '-',strtolower($request->name));
                $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('images/cms'), $input['image']);
                $slider = \App\Masters\SingleBannerImage::find($id);
                File::delete(public_path().'/images/cms/'.$slider->image);
                 // Other Product Category Insert
                if($request->product_category==-1){
                $product_category = ProductCategory::updateOrCreate(
                    ['name' => $request->other_category,
                     'slug' => $this->removeSpecialChapr($request->other_category),
                    'type' => 'Product',
                    'created_by'=>Auth::id()]
                );
                $request->product_category = $product_category->_id;
                }
                // Other Product Subcategory Insert
                if($request->product_subcategory==-1){
                $product_subcategory = ProductSubcategory::create(
                    ['name' => $request->other_subcategory,
                    'product_category_id' => $request->product_category,
                    'slug' => $this->removeSpecialChapr($request->other_subcategory),
                    'type' => 'Product',
                    'created_by'=>Auth::id()]
                );
                $request->product_subcategory = $product_subcategory->_id;
                }
                if($request->product_subcategory == NULL){
                    $request->product_subcategory = 0; 
                }
                if(!empty($request->shortDescription)){
                    $shortDescription = $request->shortDescription; 
                }else{
                    $shortDescription = ' '; 
                }
                if(!empty($request->btntext)){
                    $btntext = $request->btntext; 
                }else{
                    $btntext = ' '; 
                }
                $slider->update([
                    'name' => $request->name,
                    'slug' => $this->removeSpecialChapr($request->name),
                    'shortDescription' => $shortDescription,
                    'category' => $request->product_category,
                    'subcategory'=> $request->product_subcategory,
                    'bannerPosition'=>$request->bannerPosition,
                    'image'=> $input['image'],
                    'btntext'=>$btntext,
                    'updated_by'=>Auth::id(),
                ]);
                return response()->json([
                    'success' => '<strong>Success!</strong> Data updated for single banner image <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $slider->id).'" class="btn btn-sm btn-info" title="View this single banner image" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
                ]);
            } 
            return response()->json(['error'=>$validator->errors()->getMessages()]);
        }
        else{
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'product_category' => 'required',
            ]);
            if($validator->passes()){
                $slider = \App\Masters\SingleBannerImage::find($id);
                 // Other Product Category Insert
                if($request->product_category==-1){
                $product_category = ProductCategory::updateOrCreate(
                    ['name' => $request->other_category,
                     'slug' => $this->removeSpecialChapr($request->other_category),
                    'type' => 'Product',
                    'created_by'=>Auth::id()]
                );
                $request->product_category = $product_category->_id;
                }
                // Other Product Subcategory Insert
                if($request->product_subcategory==-1){
                $product_subcategory = ProductSubcategory::create(
                    ['name' => $request->other_subcategory,
                    'product_category_id' => $request->product_category,
                    'slug' => $this->removeSpecialChapr($request->other_subcategory),
                    'type' => 'Product',
                    'created_by'=>Auth::id()]
                );
                $request->product_subcategory = $product_subcategory->_id;
                }
                if($request->product_subcategory == NULL){
                    $request->product_subcategory = 0; 
                }
                if(!empty($request->shortDescription)){
                    $shortDescription = $request->shortDescription; 
                }else{
                    $shortDescription = ' '; 
                }
                if(!empty($request->btntext)){
                    $btntext = $request->btntext; 
                }else{
                    $btntext = ' '; 
                }
                $slider->update([
                    'name' => $request->name,
                    'slug' => $this->removeSpecialChapr($request->name),
                    'shortDescription' => $shortDescription,
                    'category' => $request->product_category,
                    'subcategory'=> $request->product_subcategory,
                    'bannerPosition'=>$request->bannerPosition,
                    'btntext'=>$btntext,
                    'updated_by'=>Auth::id(),
                ]);
                return response()->json([
                    'success' => '<strong>Success!</strong> Data updated for single banner image <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $slider->id).'" class="btn btn-sm btn-info" title="View this single banner image" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
                ]);
            }
            return response()->json(['error'=>$validator->errors()->getMessages()]);
        }
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Masters\SingleBannerImage::find($id)->delete();
        return response()->json([
            'success' => '<strong>Success!</strong> single banner image has been deleted temporarily!'
        ]);
    }

    /**
     * Restores deleted marked resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        \App\Masters\SingleBannerImage::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> single banner image has been restored!'
        ]);
    }

    public function delete($id){
        $offer = \App\Masters\SingleBannerImage::withTrashed()->find($id);
        $offer->forceDelete();
        File::delete(public_path().'/images/cms/'.$offer->image);
        return response()->json([
            'success' => '<strong>Success!</strong> single banner image has been deleted permanently!'
        ]);
    }

    public function removeSpecialChapr($value){
		$title = str_replace( array( '\'', '"', ',' , ';', '<', '>','!', '@', '#' , '$', '%', '^', '&', '*' , '(', ')', '_', '-', '=' , '+', ':', '?', '.', '`', '~', '[', ']', '{', '}', '|' , '/' , '\\' , '‘' , '’' , '“', '”' , '…', '‰' ), '', $value);
		$post_title1 = str_replace( array("  "), array(" "), $title);	
		$post_title = str_replace( array(" ","'"), array("-",""), $post_title1);
		$postTitle = strtolower($post_title);
		return $postTitle;
	}
}
