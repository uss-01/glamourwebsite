<?php

namespace App\Http\Controllers\Masters;
use Auth;
use Validator;
use DB;
use App\Masters\Size;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SizeController extends Controller
{
     protected $sortable = [
        'id' => 'id',
        'name' => 'name'
    ];

    protected $validation_rules = [
        'name' => 'required|max:255|unique:product_size',
    ];

    protected $pageTitle = 'Size';
    protected $activeNav = [
        'nav' => 'master',
        'sub' => 'size'
    ];
    protected $breadcrumb_master = 'Size';
    protected $view_master = 'master.size.';
    protected $route_master = 'size.';

    public function __construct()
    {
         
     $this->bladeVar['page']['title'] = $this->pageTitle;
     $this->bladeVar['page']['activeNav'] = $this->activeNav;
     $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
     $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = Size::withTrashed()
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Size::withTrashed()->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Size::withTrashed()
            ->where('name', 'like', '%'.$request->q.'%')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Size::withTrashed()
            ->where('name', 'like', '%'.$request->q.'%')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:product_size',
        ]);
        if ($validator->passes()) {

            $category = Size::create([
                'name' => $request->name,
                'slug' => $this->removeSpecialChapr($request->name),
                'created_by'=>Auth::id(),
            ]);
            return response()->json([
                'success' => '<strong>Success!</strong> data saved for size <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $category->id).'" class="btn btn-sm btn-info" title="View this size" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
            ]);
        } 
        return response()->json(['error'=>$validator->errors()->getMessages()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Size::withTrashed()->find($id);
            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Size::find($id);
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);
        if ($validator->passes()) {

            $category = Size::find($id);
            $category->update([
                'name' => $request->name,
                'slug' => $this->removeSpecialChapr($request->name),
                'updated_by'=>Auth::id(),
            ]);
            
            return response()->json([
                'success' => '<strong>Success!</strong> Data updated for size <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $category->id).'" class="btn btn-sm btn-info" title="View this size" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
            ]);
        } 
        return response()->json(['error'=>$validator->errors()->getMessages()]);
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Size::find($id)->delete();
        return response()->json([
            'success' => '<strong>Success!</strong> Size has been deleted temporarily!'
        ]);
    }

    /**
     * Mark as deleted the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        Size::withTrashed()->find($id)->forceDelete();
        return response()->json([
            'success' => '<strong>Success!</strong> Size has been deleted permanently!'
        ]);
    }

    /**
     * Restores deleted marked resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        Size::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> Size has been restored!'
        ]);
    }

    public function removeSpecialChapr($value){
		$title = str_replace( array( '\'', '"', ',' , ';', '<', '>','!', '@', '#' , '$', '%', '^', '&', '*' , '(', ')', '_', '-', '=' , '+', ':', '?', '.', '`', '~', '[', ']', '{', '}', '|' , '/' , '\\' , '‘' , '’' , '“', '”' , '…', '‰' ), '', $value);
		$post_title1 = str_replace( array("  "), array(" "), $title);	
		$post_title = str_replace( array(" ","'"), array("-",""), $post_title1);
		$postTitle = strtolower($post_title);
		return $postTitle;
	}	
}
