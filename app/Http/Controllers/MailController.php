<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Reminder;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MailController extends Controller {

   public function basic_email() {
      $data = array('name'=>"Admin");
   
      Mail::send('emails.mail', $data, function($message) {
         $message->from('admin@gmail.com','Admin');
         $message->to('test@gmail.com', 'User')->subject('Laravel Basic Testing Mail');
      });
      echo "Basic Email Sent. Check your inbox.";
   }

}