<?php

namespace App\Http\Controllers\Product;

use Auth;
use DB;
use App\Product\Product;
use App\Product\ProductPics;
use App\Masters\ProductCategory;
use App\Masters\ProductSubcategory;
use App\Masters\Brand;
use App\Masters\Color;
use App\Masters\Size;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use Validator;
use Illuminate\Support\Facades\File;
use Image;

class ProductController extends Controller
{
     protected $sortable = [
        'id' => '_id',
        'name' => 'name',
        'product_category' => 'pc.name',
        'product_subcategory' => 'psc.name',
    ];
    protected $validation_rules = [
        'name' => 'required|max:255|unique:products',
        'product_brand' => 'required|integer',
        'product_category' => 'required|integer',  
    ];
    protected $pageTitle = 'Prooducts';
    protected $activeNav = [
        'nav' => 'shop',
        'sub' => 'product'
    ];
    protected $breadcrumb_master = 'Products';
    protected $view_master = 'product.';
    protected $route_master = 'product.';

    public function __construct()
    {
         $this->bladeVar['page']['title'] = $this->pageTitle;
         $this->bladeVar['page']['activeNav'] = $this->activeNav;
         $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
         $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
         $this->cache();
    }

    /**
     * Refresh cache and returns puts it in array
     */
    protected function cache() 
    {
        $time = Carbon::now()->addHours(1);
        $this->bladeVar['product_categories'] = Cache::remember('product_categories', $time, function () {
            return ProductCategory::where(['type' => 'Product'])->orderBy('name', 'asc')->get();
        });

        $this->bladeVar['product_subcategories'] = Cache::remember('product_subcategories', $time, function () {
            return ProductSubcategory::where('type','=','Product')->orderBy('name', 'asc')->get();
        });
        
        $this->bladeVar['product_brands'] = Cache::remember('brands', $time, function () {
            return Brand::orderBy('name', 'asc')->get();
        });
        
        $this->bladeVar['product_colors'] = Cache::remember('product_color', $time, function () {
            return Color::orderBy('name', 'asc')->get();
        }); 

        $this->bladeVar['product_sizes'] = Cache::remember('product_size', $time, function () {
            return Size::orderBy('name', 'asc')->get();
        }); 
       
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = Product::withTrashed()
            ->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
            ->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
            ->select('products.*')
            ->with('product_category')
            ->with('product_subcategory')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
			->groupBy('products._id')
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Product::withTrashed()
            ->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
            ->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
            ->select('products.*')
            ->with('product_category')
            ->with('product_subcategory')->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    { 
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }

        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Product::withTrashed()
             ->where('products.name', 'LIKE', '%'.$request->q.'%')
            ->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
            ->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
            ->select('products.*')
            ->with('product_category')
            ->with('product_subcategory')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
			->groupBy('products._id')
            ->paginate($this->paginate_total);
			
        $this->bladeVar['total_count'] = Product::withTrashed()
            ->where('products.name', 'LIKE', '%'.$request->q.'%')
            ->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
            ->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
            ->with('product_category')
            ->with('product_subcategory')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            $this->bladeVar['productStatus'] = array('1' => 'Draft','2' => 'Published');
            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
        $input = $request->all();
        $this->validation_rules['name'] = 'required|max:255|unique:products'; 
        $this->validation_rules['image'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'; 
        if($request->product_category == -1){
          $this->validation_rules['other_category'] = 'required';   
        }
        if($request->product_subcategory == -1){
          $this->validation_rules['other_subcategory'] = 'required';   
        }
        $validator = Validator::make($request->all(),$this->validation_rules);
        if ($validator->passes()) {
            $attrNum = count($request->attrName);
            $attributes = [];
            for($i=0;$i<$attrNum;$i++){
                $attributes[$i]['name'] = $request->attrName[$i];
                $attributes[$i]['value'] = $request->attrVal[$i];
            }
            $attributes = json_encode($attributes);  

            if($request->image){
                list($width, $height) = getimagesize($request->image);
                if($width != 600 && $height != 600){
                    $error_msg=[
                    'image' => 'Size Should be 600X600',
                    ];
                    return response()->json(['error'=>$error_msg]);
                }
            }

            $image_name = str_replace(' ', '-',strtolower($request->name));
            $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('images/products'), $input['image']);
            
            $img_thumb = Image::make(public_path('images/products').'/'.$input['image']);
            $img_thumb->resize(470, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('images/products/thumbnails').'/'.$input['image']);

            $img_thumb->resize(460, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('images/products/thumbnails').'/460x460_'.$input['image']);

            $img_thumb->resize(320, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('images/products/thumbnails').'/320x320_'.$input['image']);

            if($request->product_subcategory == NULL){
            $request->product_subcategory = 0; 
            }
            // Other Product Category Insert
            if($request->product_category==-1){
                $product_category = ProductCategory::updateOrCreate(
                    ['name' => $request->other_category,
                    'slug' => $this->removeSpecialChapr($request->other_category),
                    'type' => 'Product',
                    'created_by'=>Auth::id()]
                );
                $request->product_category = $product_category->_id;
            }
            // Other Product Subcategory Insert
            if($request->product_subcategory==-1){
                $product_subcategory = ProductSubcategory::create(
                    ['name' => $request->other_subcategory,
                    'product_category_id' => $request->product_category,
                    'slug' => $this->removeSpecialChapr($request->other_subcategory),
                    'type' => 'Product',
                    'created_by'=>Auth::id()]
                );
                $request->product_subcategory = $product_subcategory->_id;
            }
            if($request->featured != 1){
                $request->featured = 0;
            }
            if($request->new_arrival != 1){
                $request->new_arrival = 0;
            }
            if($request->on_sale != 1){
                $request->on_sale = 0;
            }
            if($request->hot_deal != 1){
                $request->hot_deal = 0;
            }
            if($request->add_to_cart != 1){
                $request->add_to_cart = 0;
            }
            if($request->best_seller != 1){
                $request->best_seller = 0;
            }

            if(!empty($request->product_color)){
                $product_color = $request->product_color;
            }else{
                $product_color = '0';
            }
            if(!empty($request->product_size)){
                $product_size = $request->product_size;
            }else{
                $product_size = '0';
            }

            if(!empty($request->discounted_price)){
                $discounted_price = $request->discounted_price;
            }else{
                $discounted_price = '0';
            }
            if(!empty($request->product_quantity)){
                $product_quantity = $request->product_quantity;
            }else{
                $product_quantity = '0';
            }
            if(!empty($request->product_status)){
                $product_status = $request->product_status;
            }else{
                $product_status = 'Draft';
            }

            $variations = [];
            if(!empty($request->pcolor)){
                $variationNum = count($request->pcolor);
                for($j=0; $j<$variationNum; $j++){
                    $variations[$j]['color'] = $request->pcolor[$j];
                    $variations[$j]['size'] = $request->psize[$j];
                    $variations[$j]['price'] = number_format((float)$request->pprice[$j], 2, '.', '');
                    $variations[$j]['discountedPrice'] = number_format((float)$request->pdprice[$j], 2, '.', '');
                    $variations[$j]['quantity'] = $request->pquantity[$j];
                    if(!empty($request->images[$j])){
                        if($request->images[$j]){
                            list($width, $height) = getimagesize($request->images[$j]);
                            if($width != 600 && $height != 600){
                                $error_msg=[
                                'image' => 'Size Should be 600X600',
                                ];
                                return response()->json(['error'=>$error_msg]);
                            }
                        }
                        $image_name1 = str_replace(' ', '-',strtolower($request->name));
                        $arrayImages = time().'_'.$image_name1.'_'.$j.'.'.$request->images[$j]->getClientOriginalExtension();
                        $request->images[$j]->move(public_path('images/products'), $arrayImages);
                        $variations[$j]['images'] = $arrayImages;
                    }else{
                        $variations[$j]['images'] = $request->imagesurl[$j];
                    } 
                }
            }
            if(!empty($variations)){
                $allvariations = json_encode($variations);
            }else{
                $allvariations = json_encode($variations);
            }
             
            $product = Product::create([
                'name' => $request->name,
                'slug' => $this->removeSpecialChapr($request->name),
                'price' => $request->price,
                'brand_id' => $request->product_brand,
                'product_category_id' => $request->product_category,
                'product_subcategory_id' => $request->product_subcategory,
                'product_color_id' => $product_color,
                'product_size_id' => $product_size,
                'featured' => $request->featured,
                'new_arrival' => $request->new_arrival,
                'on_sale' => $request->on_sale,
                'hot_deal' => $request->hot_deal,
                'best_seller' => $request->best_seller,
                'add_to_cart' => $request->add_to_cart,
                'product_quantity' => $product_quantity,
                'discounted_price' => $discounted_price,
                'product_status' => $product_status,
                'attribute'=>$attributes,
                'variations'=>$allvariations,
                'image'=> $input['image'],
                'sort_details'=>$request->sort_details,
                'details'=>$request->details,
                'created_by'=>Auth::id(),
            ]);
            return response()->json([
                'success' => '<strong>Success!</strong> data saved for product <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $product->_id).'" class="btn btn-sm btn-info" title="View this product" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
            ]);
        }
        return response()->json(['error'=>$validator->errors()->getMessages()]);
    }

     public function product_subcategory(Request $request)
    {
        $this->validate($request, [
            'product_category' => 'required|integer'
        ]);
         $product_subcategories = ProductSubcategory::where('product_subcategories.product_category_id', '=', $request->product_category)
            ->select('_id', 'name')
            ->orderBy($this->sortable[$request->input('sort', 'name')], $request->input('dir', 'ASC'))->get();
        return response()->json($product_subcategories);
    }

    public function shop_brands(Request $request)
    {
        $this->validate($request, [
            'shop_id' => 'required|integer'
        ]);
        $shop_brands = Brand::withTrashed()
            ->orderBy($this->sortable[$request->input('sort', 'name')], $request->input('dir', 'ASC'))->get();
        return response()->json($shop_brands);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Product::withTrashed()->find($id);
            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            $product = Product::find($id);
            $this->bladeVar['result'] = $product;
            $this->bladeVar['productStatus'] = array('1' => 'Draft','2' => 'Published');
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $this->validation_rules['name'] = 'required|max:255';  
        if($request->product_category==-1){
          $this->validation_rules['other_category'] = 'required';   
        }
        if($request->product_subcategory==-1){
          $this->validation_rules['other_subcategory'] = 'required';   
        }
        // Getting Attributes
        $attrNum = count($request->attrName);
          $attributes = [];
          for($i=0;$i<$attrNum;$i++){
           $attributes[$i]['name'] = $request->attrName[$i];
           $attributes[$i]['value'] = $request->attrVal[$i];
          }
           $attributes = json_encode($attributes);
          if (isset($request->image)) {
        $this->validation_rules['image'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'; 
        $validator = Validator::make($request->all(),$this->validation_rules);
            if($validator->passes()){       
              if($request->image){
         list($width, $height) = getimagesize($request->image);
          if($width != 600 && $height != 600){
            $error_msg=[
             'image' => 'Size Should be 600X600',
            ];
            return response()->json(['error'=>$error_msg]);
          }
        }
        $image_name = str_replace(' ', '-',strtolower($request->name));
        $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/products'), $input['image']);
        $img_thumb = Image::make(public_path('images/products').'/'.$input['image']);
         $img_thumb->resize(470, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('images/products/thumbnails').'/'.$input['image']);

        $img_thumb->resize(460, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('images/products/thumbnails').'/460x460_'.$input['image']);

        $img_thumb->resize(320, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('images/products/thumbnails').'/320x320_'.$input['image']);

         $product = Product::find($id);
         File::delete(public_path().'/images/products/'.$product->image);
         File::delete(public_path().'/images/products/thumbnails/'.$product->image);
         File::delete(public_path().'/images/products/thumbnails/460x460_'.$product->image);
         File::delete(public_path().'/images/products/thumbnails/320x320_'.$product->image);

        // Other Product Category Insert
            if($request->product_category==-1){
            $product_category = ProductCategory::updateOrCreate(
                ['name' => $request->other_category,
                'slug' => $this->removeSpecialChapr($request->other_category),
                'type' => 'Product',
                'created_by'=>Auth::id()]
            );
            $request->product_category = $product_category->_id;
            }
            // Other Product Subcategory Insert
            if($request->product_subcategory==-1){
            $product_subcategory = ProductSubcategory::create(
                ['name' => $request->other_subcategory,
                'product_category_id' => $request->product_category,
                'slug' => $this->removeSpecialChapr($request->other_subcategory),
                'type' => 'Product',
                'created_by'=>Auth::id()]
            );
            $request->product_subcategory = $product_subcategory->_id;
            }
        if($request->product_subcategory == NULL){
           $request->product_subcategory = 0; 
        }
        if($request->featured !=1){
            $request->featured = 0;
        }
        if($request->new_arrival != 1){
            $request->new_arrival = 0;
        }
        if($request->on_sale != 1){
            $request->on_sale = 0;
        }
        if($request->hot_deal != 1){
            $request->hot_deal = 0;
        }
         if($request->add_to_cart !=1){
            $request->add_to_cart = 0;
        }
        if($request->best_seller != 1){
            $request->best_seller = 0;
        }
        if(!empty($request->product_color)){
            $product_color = $request->product_color;
        }else{
            $product_color = '0';
        }
        if(!empty($request->product_size)){
            $product_size = $request->product_size;
        }else{
            $product_size = '0';
        }

        if(!empty($request->discounted_price)){
            $discounted_price = $request->discounted_price;
        }else{
            $discounted_price = '0';
        }
        if(!empty($request->product_quantity)){
            $product_quantity = $request->product_quantity;
        }else{
            $product_quantity = '0';
        }
        if(!empty($request->product_status)){
            $product_status = $request->product_status;
        }else{
            $product_status = 'Draft';
        }
        
        $variations = [];
        if(!empty($request->pcolor)){
            $variationNum = count($request->pcolor);
            for($j=0; $j<$variationNum; $j++){
                $variations[$j]['color'] = $request->pcolor[$j];
                $variations[$j]['size'] = $request->psize[$j];
                $variations[$j]['price'] = number_format((float)$request->pprice[$j], 2, '.', '');
                $variations[$j]['discountedPrice'] = number_format((float)$request->pdprice[$j], 2, '.', '');
                $variations[$j]['quantity'] = $request->pquantity[$j];
                if(!empty($request->images[$j])){
                    if($request->images[$j]){
                        list($width, $height) = getimagesize($request->images[$j]);
                        if($width != 600 && $height != 600){
                            $error_msg=[
                            'image' => 'Size Should be 600X600',
                            ];
                            return response()->json(['error'=>$error_msg]);
                        }
                    }
                    $image_name1 = str_replace(' ', '-',strtolower($request->name));
                    $arrayImages = time().'_'.$image_name1.'_'.$j.'.'.$request->images[$j]->getClientOriginalExtension();
                    $request->images[$j]->move(public_path('images/products'), $arrayImages);
                    $variations[$j]['images'] = $arrayImages;
                }else{
                    $variations[$j]['images'] = $request->imagesurl[$j];
                }
            }
        }
        if(!empty($variations)){
            $allvariations = json_encode($variations);
        }else{
            $allvariations = json_encode($variations);
        } 

        $product->update([
            'name' => $request->name,
            'slug' => $this->removeSpecialChapr($request->name),
            'price' => $request->price,
            'brand_id' => $request->product_brand,
            'product_category_id' => $request->product_category,
            'product_subcategory_id' => $request->product_subcategory,
            'product_color_id' => $product_color,
            'product_size_id' => $product_size,
            'featured' => $request->featured,
            'new_arrival' => $request->new_arrival,
            'on_sale' => $request->on_sale,
            'hot_deal' => $request->hot_deal,
            'best_seller' => $request->best_seller,
            'add_to_cart' => $request->add_to_cart,
            'product_quantity' => $product_quantity,
            'discounted_price' => $discounted_price,
            'product_status' => $product_status,
            'attribute'=>$attributes,
            'variations'=>$allvariations,
            'image'=> $input['image'],
            'sort_details'=>$request->sort_details,
            'details'=>$request->details,
            'updated_by'=>Auth::id(),
        ]);
        return response()->json([
            'success' => '<strong>Success!</strong> Data updated for product <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $product->_id).'" class="btn btn-sm btn-info" title="View this product" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);
     } 
       return response()->json(['error'=>$validator->errors()->getMessages()]);
     }
     else{
        $validator = Validator::make($request->all(),$this->validation_rules);
        if($validator->passes()){
        // Other Product Category Insert
        if($request->product_category==-1){
            $product_category = ProductCategory::updateOrCreate(
                ['name' => $request->other_category,
                'slug' => $this->removeSpecialChapr($request->other_category),
                'type' => 'Product',
                'created_by'=>Auth::id()]
            );
            $request->product_category = $product_category->_id;
        }
            // Other Product Subcategory Insert
        if($request->product_subcategory==-1){
            $product_subcategory = ProductSubcategory::create(
                ['name' => $request->other_subcategory,
                'product_category_id' => $request->product_category,
                'slug' => $this->removeSpecialChapr($request->other_subcategory),
                'type' => 'Product',
                'created_by'=>Auth::id()]
            );
            $request->product_subcategory = $product_subcategory->_id;
        }
        if($request->product_subcategory == NULL){
           $request->product_subcategory = 0; 
        } 
        if($request->featured !=1){
            $request->featured = 0;
        }
        if($request->new_arrival != 1){
            $request->new_arrival = 0;
        }
        if($request->on_sale != 1){
            $request->on_sale = 0;
        }
        if($request->hot_deal != 1){
            $request->hot_deal = 0;
        }
        if($request->add_to_cart !=1){
            $request->add_to_cart = 0;
        }
        if($request->best_seller != 1){
            $request->best_seller = 0;
        }
        if(!empty($request->product_color)){
            $product_color = $request->product_color;
        }else{
            $product_color = '0';
        }
        if(!empty($request->product_size)){
            $product_size = $request->product_size;
        }else{
            $product_size = '0';
        }

        if(!empty($request->discounted_price)){
            $discounted_price = $request->discounted_price;
        }else{
            $discounted_price = '0';
        }
        if(!empty($request->product_quantity)){
            $product_quantity = $request->product_quantity;
        }else{
            $product_quantity = '0';
        }
        if(!empty($request->product_status)){
            $product_status = $request->product_status;
        }else{
            $product_status = 'Draft';
        }

        $variations = [];
        if(!empty($request->pcolor)){
            $variationNum = count($request->pcolor);        
            for($j=0; $j<$variationNum; $j++){
                $variations[$j]['color'] = $request->pcolor[$j];
                $variations[$j]['size'] = $request->psize[$j];
                $variations[$j]['price'] = number_format((float)$request->pprice[$j], 2, '.', '');
                $variations[$j]['discountedPrice'] = number_format((float)$request->pdprice[$j], 2, '.', '');
                $variations[$j]['quantity'] = $request->pquantity[$j];
                if(!empty($request->images[$j])){
                    if($request->images[$j]){
                        list($width, $height) = getimagesize($request->images[$j]);
                        if($width != 600 && $height != 600){
                            $error_msg=[
                            'image' => 'Size Should be 600X600',
                            ];
                            return response()->json(['error'=>$error_msg]);
                        }
                    }
                    $image_name1 = str_replace(' ', '-',strtolower($request->name));
                    $arrayImages = time().'_'.$image_name1.'_'.$j.'.'.$request->images[$j]->getClientOriginalExtension();
                    $request->images[$j]->move(public_path('images/products'), $arrayImages);
                    $variations[$j]['images'] = $arrayImages;
                }else{
                    $variations[$j]['images'] = $request->imagesurl[$j];
                }
            }
        }
        if(!empty($variations)){
            $allvariations = json_encode($variations);
        }else{
            $allvariations = json_encode($variations);
        }

        $product = Product::find($id);
        $product->update([
            'name' => $request->name,
            'slug' => $this->removeSpecialChapr($request->name),
            'price' => $request->price,
            'brand_id' => $request->product_brand,
            'product_category_id' => $request->product_category,
            'product_subcategory_id' => $request->product_subcategory,
            'product_color_id' => $product_color,
            'product_size_id' => $product_size,
            'featured' => $request->featured,
            'new_arrival' => $request->new_arrival,
            'on_sale' => $request->on_sale,
            'hot_deal' => $request->hot_deal,
            'best_seller' => $request->best_seller,
            'add_to_cart' => $request->add_to_cart,
            'product_quantity' => $product_quantity,
            'discounted_price' => $discounted_price,
            'product_status' => $product_status,
            'attribute'=>$attributes,
            'variations'=>$allvariations,
            'sort_details'=>$request->sort_details,
            'details'=>$request->details,
            'updated_by'=>Auth::id(),
        ]);

         return response()->json([
            'success' => '<strong>Success!</strong> Data updated for product <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $product->_id).'" class="btn btn-sm btn-info" title="View this product" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);
    }
     return response()->json(['error'=>$validator->errors()->getMessages()]);
     }
        
    }

    // For Product Pics
    public function pics(Request $request, $id)
    {
        if ($request->ajax()) {
            $this->bladeVar['result'] = Product::withTrashed()->find($id);
            $this->bladeVar['productPics'] = ProductPics::where(['product_pics.product_id' => $id])
                ->select('product_pics._id','product_pics.image')
                ->get();
            return view($this->view_master.'pics', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    public function savePics(Request $request)
    {
        
        $input = $request->all();
        $rules = [];
        $photo_cnt = count($request->image);
        foreach(range(0, ($photo_cnt-1)) as $index) {
            $rules['image.' . $index] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048';
        }
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            $cnt = 1;
        foreach ($request->image as $photo) {
         list($width, $height) = getimagesize($photo);
            $image_fullname = time().'_'.$cnt.'.'.$photo->getClientOriginalExtension();
            $photo->move(public_path('images/products'), $image_fullname);
            $photo = ProductPics::create([
                'product_id' => $request->product,
                'image'=> $image_fullname,
                'created_by'=>Auth::id(),
            ]);
            $cnt++;
        }
        return response()->json([
                'success' => '<strong>Success!</strong> data saved for photo <strong>'.$request->title.'</strong>'
        
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::find($id)->delete();
        return response()->json([
            'success' => '<strong>Success!</strong> Product has been deleted temporarily!'
        ]);
    }

    public function deletePic($id)
    {
        $pics=ProductPics::find($id);
        File::delete(public_path().'/images/products/'.$pics->image);
        $pics->delete();
       
        return response()->json([
            'success' => '<strong>Success!</strong> Product Pic has been deleted permanently!'
        ]);
    }

    /**
     * Mark as deleted the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $product = Product::withTrashed()->find($id);
        File::delete(public_path().'/images/products/'.$product->image);
        File::delete(public_path().'/images/shops/thumbnails'.$product->image);
        $product->forceDelete();
        return response()->json([
            'success' => '<strong>Success!</strong> Product has been deleted permanently!'
        ]);
    }

    /**
     * Restores deleted marked resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        Product::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> Product has been restored!'
        ]);
    }

    public function removeSpecialChapr($value){
		$title = str_replace( array( '\'', '"', ',' , ';', '<', '>','!', '@', '#' , '$', '%', '^', '&', '*' , '(', ')', '_', '-', '=' , '+', ':', '?', '.', '`', '~', '[', ']', '{', '}', '|' , '/' , '\\' , '‘' , '’' , '“', '”' , '…', '‰' ), '', $value);
		$post_title1 = str_replace( array("  "), array(" "), $title);	
		$post_title = str_replace( array(" ","'"), array("-",""), $post_title1);
		$postTitle = strtolower($post_title);
		return $postTitle;
	}
}
