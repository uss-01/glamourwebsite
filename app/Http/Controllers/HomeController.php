<?php namespace App\Http\Controllers;

use Auth;
use DB;
use Validator;
use App\User;
use App\Cms;
use App\Contact;
use App\Coupons\Coupons;
use App\Comments;
use App\Setting;
use App\SiteUsers;
use App\Blogs\Blogs;
use App\Blogs\BlogsPics;
use App\Masters\Brand;
use App\Masters\Color;
use App\Masters\Size;
use App\Masters\BannerImages;
use App\Masters\Bannerslider;
use App\Masters\SliderImages;
use App\Masters\SingleBannerImage;
use App\Team\Team;
use App\Testimonial\Testimonial;
use App\Product\Usercart;
use App\Product\Userwishlist;
use App\Product\Product;
use App\Product\ProductPics;
use App\Masters\ProductCategory;
use App\Masters\ProductSubcategory;
use App\Orders\Orders;
use App\Orderdetail\Orderdetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Input;

/** All Paypal Details class **/
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

    private $_api_context;

	public function __construct()
	{
		/** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
		$this->_api_context->setConfig($paypal_conf['settings']);
		
		$this->bladeVar['cms_pages'] = Cms::withTrashed()->orderBy('_id','DESC')->get();
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index(){
		$this->bladeVar['page']['title'] = "Home - Glamour Jewellery"; 

		$this->bladeVar['site_setting'] = Setting::withTrashed()->orderBy('_id','ASC')->get();
		 
		$this->bladeVar['slider_results'] = SliderImages::withTrashed()->whereNull('deleted_at')->orderBy('id', 'DESC')->get();

		$this->bladeVar['bannerslider_results'] = Bannerslider::withTrashed()->whereNull('deleted_at')->orderBy('id', 'DESC')->get();

		$this->bladeVar['banner_results'] = BannerImages::withTrashed()
            ->join('product_categories as pc', 'pc._id', '=', 'banner_images.category')
            ->join('product_subcategories as pcs', 'pcs._id', '=', 'banner_images.subcategory')
			->orderBy('id', 'DESC')
            ->select('banner_images.*')
            ->with('product_category')
            ->with('product_subcategory')
			->get();

		$this->bladeVar['singleBanner_results'] = SingleBannerImage::withTrashed()
            ->join('product_categories as pc', 'pc._id', '=', 'single_banner_images.category')
            ->join('product_subcategories as pcs', 'pcs._id', '=', 'single_banner_images.subcategory')
            ->orderBy('id', 'DESC')
            ->select('single_banner_images.*')
            ->with('product_category')
            ->with('product_subcategory')
			->get();
			
		$this->bladeVar['blog_results'] = Blogs::withTrashed()
            ->join('blog_categories as bc', 'bc._id', '=', 'blogs.blog_category_id')
            ->leftJoin('blog_tags as bt', 'bt.id', '=', 'blogs.blog_tag_id')
            ->select('blogs.*')
            ->with('blog_category')
            ->with('blog_tag')
            ->orderBy('id','DESC')
			->groupBy('blogs._id')
			->get();
			
		$this->bladeVar['product_results'] = Product::withTrashed() 
            ->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
            ->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
            ->select('products.*')
            ->with('product_category')
            ->with('product_subcategory')
            ->orderBy('_id', 'DESC')
			->groupBy('products._id')
			->get();  
			
		$this->bladeVar['brand_results'] = Brand::withTrashed()->whereNull('deleted_at')->orderBy('_id','DESC')->get();

		$this->bladeVar['color_results'] = Color::withTrashed()->whereNull('deleted_at')->orderBy('id','DESC')->get();

		$this->bladeVar['size_results'] = Size::withTrashed()->whereNull('deleted_at')->orderBy('id','DESC')->get();

		$this->bladeVar['testimonial_results'] = Testimonial::withTrashed()->whereNull('deleted_at')->orderBy('id','DESC')->get();

		$userCartDetails = $this->userCartDetails();
		$this->bladeVar['userCartProducts'] = $userCartDetails['userCartProducts'];
		$this->bladeVar['userCartData'] = $userCartDetails['userCartData'];
		$this->bladeVar['userCart'] = $userCartDetails['userCart'];
		$this->bladeVar['userWishlistData'] = $userCartDetails['userWishlistData'];
		$this->bladeVar['userWishlist'] = $userCartDetails['userWishlist'];
		
		return view('frontend/index', ['bladeVar' => $this->bladeVar]);
	}

	public function aboutus(){
		$this->bladeVar['page']['title'] = "About-us - Glamour Jewellery";
		$this->bladeVar['site_setting'] = Setting::withTrashed()->orderBy('_id','ASC')->get();
		$this->bladeVar['results'] = Cms::where('_id', '1')->whereNull('deleted_at')->first();
		$this->bladeVar['testimonial_results'] = Testimonial::withTrashed()->whereNull('deleted_at')->orderBy('id','DESC')->get();
		$this->bladeVar['team_results'] = Team::withTrashed()->whereNull('deleted_at')->orderBy('id','DESC')->get();

        $userCartDetails = $this->userCartDetails();
		$this->bladeVar['userCartProducts'] = $userCartDetails['userCartProducts'];
		$this->bladeVar['userCartData'] = $userCartDetails['userCartData'];
		$this->bladeVar['userCart'] = $userCartDetails['userCart'];
		$this->bladeVar['userWishlistData'] = $userCartDetails['userWishlistData'];
		$this->bladeVar['userWishlist'] = $userCartDetails['userWishlist'];

        return view('frontend/pages/about-us', ['bladeVar' => $this->bladeVar]);
	}
	
	public function contactus(){
		$this->bladeVar['page']['title'] = "Contact-us - Glamour Jewellery";
		$this->bladeVar['site_setting'] = Setting::withTrashed()->orderBy('_id','ASC')->get();
		$this->bladeVar['results'] = Cms::where('_id', '3')->whereNull('deleted_at')->first();

        $userCartDetails = $this->userCartDetails();
		$this->bladeVar['userCartProducts'] = $userCartDetails['userCartProducts'];
		$this->bladeVar['userCartData'] = $userCartDetails['userCartData'];
		$this->bladeVar['userCart'] = $userCartDetails['userCart'];
		$this->bladeVar['userWishlistData'] = $userCartDetails['userWishlistData'];
		$this->bladeVar['userWishlist'] = $userCartDetails['userWishlist'];

        return view('frontend/pages/contact-us', ['bladeVar' => $this->bladeVar]);
	}

	public function shopProducts(Request $request){
		$price = array(); $brand = array(); $size = array(); $color = array();
		$this->bladeVar['page']['title'] = "Shop - Glamour Jewellery";
		$this->bladeVar['site_setting'] = Setting::withTrashed()->orderBy('_id','ASC')->get();
		$where['products.deleted_at'] = NULL;
        if($request->has('category')) {
			$category = explode(",",$request->category);
		}else{
			$category = array();
		}
        if($request->has('price')) {
			$price1 = str_replace(array(' ','$','-'),array('','',','),$request->price);
			$price = explode(",",$price1);
		}else{
			$price = array();
		}
		if($request->has('brand')) {
			$brand = explode(",",$request->brand);
		}else{
			$brand = array();
		}
		if($request->has('size')) {
			$size = explode(",",$request->size);
		}else{
			$size = array();
		}
		if($request->has('color')) {
			$color = explode(",",$request->color);
		}else{
			$color = array();
		}
		if($request->has('sortby')) {
			$sortby = $request->sortby;
			if($sortby == 'a-z'){
				$name = 'products.name';
				$value = 'ASC';
			}else if($sortby == 'z-a'){
				$name = 'products.name';
				$value = 'DESC';
			}else if($sortby == 'low-high'){
				$name = 'products.price';
				$value = 'ASC';
			}else if($sortby == 'high-low'){
				$name = 'products.price';
				$value = 'DESC';
			}else{
                $name = 'products.best_seller';
			    $value = 'DESC'; 
			}
		}else{
			$name = 'products._id';
			$value = 'DESC'; 
		}


        if(!empty($category) && !empty($price) && !empty($brand) && !empty($size) && !empty($color)){
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.product_category_id', $category)->whereBetween('products.price', $price)->whereIn('products.brand_id', $brand)->whereIn('products.product_size_id', $size)->whereIn('products.product_color_id', $color)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else if(empty($category) && !empty($price) && !empty($brand) && !empty($size) && !empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereBetween('products.price', $price)->whereIn('products.brand_id', $brand)->whereIn('products.product_size_id', $size)->whereIn('products.product_color_id', $color)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else if(!empty($category) && empty($price) && !empty($brand) && !empty($size) && !empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.product_category_id', $category)->whereIn('products.brand_id', $brand)->whereIn('products.product_size_id', $size)->whereIn('products.product_color_id', $color)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else if(!empty($category) && !empty($price) && empty($brand) && !empty($size) && !empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.product_category_id', $category)->whereBetween('products.price', $price)->whereIn('products.product_size_id', $size)->whereIn('products.product_color_id', $color)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else if(!empty($category) && !empty($price) && !empty($brand) && empty($size) && !empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.product_category_id', $category)->whereBetween('products.price', $price)->whereIn('products.brand_id', $brand)->whereIn('products.product_color_id', $color)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else if(!empty($category) && !empty($price) && !empty($brand) && !empty($size) && empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.product_category_id', $category)->whereBetween('products.price', $price)->whereIn('products.brand_id', $brand)->whereIn('products.product_size_id', $size)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}
		
		else if(empty($category) && empty($price) && !empty($brand) && !empty($size) && !empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.brand_id', $brand)->whereIn('products.product_size_id', $size)->whereIn('products.product_color_id', $color)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else if(empty($category) && !empty($price) && empty($brand) && !empty($size) && !empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereBetween('products.price', $price)->whereIn('products.product_size_id', $size)->whereIn('products.product_color_id', $color)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else if(empty($category) && !empty($price) && !empty($brand) && empty($size) && !empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereBetween('products.price', $price)->whereIn('products.brand_id', $brand)->whereIn('products.product_color_id', $color)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else if(empty($category) && !empty($price) && !empty($brand) && !empty($size) && empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereBetween('products.price', $price)->whereIn('products.brand_id', $brand)->whereIn('products.product_size_id', $size)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}
		
		else if(!empty($category) && empty($price) && empty($brand) && !empty($size) && !empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.product_category_id', $category)->whereIn('products.product_size_id', $size)->whereIn('products.product_color_id', $color)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else if(!empty($category) && empty($price) && !empty($brand) && empty($size) && !empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.product_category_id', $category)->whereIn('products.brand_id', $brand)->whereIn('products.product_color_id', $color)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else if(!empty($category) && empty($price) && !empty($brand) && !empty($size) && empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.product_category_id', $category)->whereIn('products.brand_id', $brand)->whereIn('products.product_size_id', $size)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}
		
		else if(!empty($category) && !empty($price) && empty($brand) && empty($size) && !empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.product_category_id', $category)->whereBetween('products.price', $price)->whereIn('products.product_color_id', $color)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else if(!empty($category) && !empty($price) && empty($brand) && !empty($size) && empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.product_category_id', $category)->whereBetween('products.price', $price)->whereIn('products.product_size_id', $size)->whereIn('products.product_color_id', $color)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}

		else if(!empty($category) && !empty($price) && !empty($brand) && empty($size) && empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.product_category_id', $category)->whereBetween('products.price', $price)->whereIn('products.brand_id', $brand)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}

		else if(!empty($category) && empty($price) && !empty($brand) && empty($size) && empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.product_category_id', $category)->whereIn('products.brand_id', $brand)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else if(!empty($category) && empty($price) && empty($brand) && !empty($size) && empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.product_category_id', $category)->whereIn('products.product_size_id', $size)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}

		else if(empty($category) && empty($price) && empty($brand) && !empty($size) && !empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.product_size_id', $size)->whereIn('products.product_color_id', $color)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else if(empty($category) && empty($price) && !empty($brand) && empty($size) && !empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.brand_id', $brand)->whereIn('products.product_color_id', $color)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else if(empty($category) && !empty($price) && empty($brand) && empty($size) && !empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereBetween('products.price', $price)->whereIn('products.product_color_id', $color)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else if(!empty($category) && empty($price) && empty($brand) && empty($size) && !empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.product_category_id', $category)->whereIn('products.product_color_id', $color)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}

		else if(empty($category) && empty($price) && empty($brand) && empty($size) && !empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.product_color_id', $color)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else if(empty($category) && empty($price) && empty($brand) && !empty($size) && empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.product_size_id', $size)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else if(empty($category) && empty($price) && !empty($brand) && empty($size) && empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.brand_id', $brand)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else if(empty($category) && !empty($price) && empty($brand) && empty($size) && empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereBetween('products.price', $price)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else if(!empty($category) && empty($price) && empty($brand) && empty($size) && empty($color))  {
            $this->bladeVar['product_results'] = Product::where($where)->whereIn('products.product_category_id', $category)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');  
		}else{
			$this->bladeVar['product_results'] = Product::where($where)->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')->select('products.*')->with('product_category')->with('product_subcategory')->orderBy($name, $value)->groupBy('products._id')->paginate('10');
		}


		$this->bladeVar['productsMinMaxPrice'] = DB::select('SELECT min(products.price) AS minPrice, max(products.price) AS maxPrice FROM products');			
			
		$this->bladeVar['productsCategories'] = DB::select('SELECT product_categories._id, product_categories.name, product_categories.slug, product_categories.deleted_at, COUNT(products.product_category_id) AS categoryProducts FROM product_categories INNER JOIN products ON product_categories._id = products.product_category_id GROUP BY product_categories._id');
		
		$this->bladeVar['productsBrands'] = DB::select('SELECT product_brand._id, product_brand.name, product_brand.slug, product_brand.deleted_at, COUNT(products.brand_id) AS brandProducts FROM product_brand INNER JOIN products ON product_brand._id = products.brand_id GROUP BY product_brand._id');
		
		$this->bladeVar['productsColors'] = DB::select('SELECT product_color.id, product_color.name, product_color.slug, product_color.deleted_at, COUNT(products.product_color_id) AS colorProducts FROM product_color INNER JOIN products ON product_color.id = products.product_color_id GROUP BY product_color.id');

		$this->bladeVar['productsSizes'] = DB::select('SELECT product_size.id, product_size.name, product_size.slug, product_size.deleted_at, COUNT(products.product_size_id) AS sizeProducts FROM product_size INNER JOIN products ON product_size.id = products.product_size_id GROUP BY product_size.id');	

        $this->bladeVar['color_results'] = Color::withTrashed()->whereNull('deleted_at')->orderBy('id','DESC')->get();

		$this->bladeVar['size_results'] = Size::withTrashed()->whereNull('deleted_at')->orderBy('id','DESC')->get();

		$userCartDetails = $this->userCartDetails();
		$this->bladeVar['userCartProducts'] = $userCartDetails['userCartProducts'];
		$this->bladeVar['userCartData'] = $userCartDetails['userCartData'];
		$this->bladeVar['userCart'] = $userCartDetails['userCart'];
		$this->bladeVar['userWishlistData'] = $userCartDetails['userWishlistData'];
		$this->bladeVar['userWishlist'] = $userCartDetails['userWishlist'];

        return view('frontend/shop', ['bladeVar' => $this->bladeVar]);
	}

	public function product_detail($slug){
		$this->bladeVar['page']['title'] = "Shop - Glamour Jewellery";
		$this->bladeVar['site_setting'] = Setting::withTrashed()->orderBy('_id','ASC')->get();
		
		$this->bladeVar['color_results'] = Color::withTrashed()->whereNull('deleted_at')->orderBy('id','DESC')->get();

		$this->bladeVar['size_results'] = Size::withTrashed()->whereNull('deleted_at')->orderBy('id','DESC')->get();

		$getRecord = Product::where('slug', $slug)->whereNull('deleted_at')->first();
        if(!empty($getRecord->_id)){
			$product_id = $getRecord->_id;
			$category_id = $getRecord->product_category->_id; 
			
			if(isset($getRecord->image)) {
				$getRecord['image'] = url('/')."/images/products/".$getRecord->image;
			} else {
				$getRecord['image'] = url('/')."/images/no-image.jpg";
			}
			$attributeArray = [];
			if(isset($getRecord->attribute)) {
				$attributeArray = json_decode($getRecord->attribute);
			}
			$actualAttribue = [];
			foreach($attributeArray as $item){
				if($item->name != null && $item->value != null) {
					$actualAttribue[] = $item;
				}
			}

			$getRecord['attribute'] = $actualAttribue;

            $data = Session::all();
			if (Session::has('userLoggedIn')){ 
				$token = Session::get('userLoggedIn');
				$user = SiteUsers::where(['_id' => $token])->select('_id')->first();
				$user_id = $user->_id;
				$usercartData = Usercart::where('user_id', '=', $user_id)->where('product_id', '=', $product_id)->select('users_cart._id')->first();
				if(!empty($usercartData->_id)){
					$getRecord['already_in_cart'] = 'Yes';
				}else{
					$getRecord['already_in_cart'] = 'No';
				}

                $userWData = Userwishlist::where(['user_id' => $user->_id, 'product_id' => $product_id,'status' => '1'])->select('*')->first();
				if(!empty($userWData)){
                    $getRecord['already_in_Wishlist'] = 'Yes';
				}else{
                    $getRecord['already_in_Wishlist'] = 'No';
				} 
            }else{
			   $getRecord['already_in_cart'] = 'No';
			   $getRecord['already_in_Wishlist'] = 'No';
			}

			$this->bladeVar['productDetail'] = $getRecord;
			$getSlideImage = [];
			$i=0;
			$getSlideImage[$i++]['image'] = $getRecord['image'];

			$getRecord = ProductPics::where(['product_id' => $product_id])->select('image')->get();
			foreach($getRecord as $records) {
				if($records->image) {
					$getSlideImage[$i]['image'] = url('/')."/images/products/".$records->image;
				} else {
					$getSlideImage[$i]['image'] = url('/')."/images/no-image.jpg";
				}
				$i++;
			}

			$this->bladeVar['slideimages'] = $getSlideImage;

			$this->bladeVar['youMayLike'] = $this->productsYouMayLike($count=6, $curr_product_id=$product_id, $curr_category_id=$category_id);

			$this->bladeVar['productCommentsCount'] = DB::select("SELECT COUNT(id) as commentsCount FROM comments WHERE type = 'product' AND postId = ".$product_id);

			$this->bladeVar['productComments'] = DB::select("SELECT * FROM comments WHERE type = 'product' AND postId = ".$product_id);

			$this->bladeVar['productRatings'] = DB::select("SELECT avg(website) as ratings FROM comments WHERE type = 'product' AND postId = ".$product_id);
		}else{
            $this->bladeVar['productDetail'] = array();
		}

        $userCartDetails = $this->userCartDetails();
		$this->bladeVar['userCartProducts'] = $userCartDetails['userCartProducts'];
		$this->bladeVar['userCartData'] = $userCartDetails['userCartData'];
		$this->bladeVar['userCart'] = $userCartDetails['userCart'];
		$this->bladeVar['userWishlistData'] = $userCartDetails['userWishlistData'];
		$this->bladeVar['userWishlist'] = $userCartDetails['userWishlist'];

        return view('frontend/product-detail', ['bladeVar' => $this->bladeVar]);
	}

	public function productsYouMayLike($count=9, $curr_product_id='', $curr_category_id='', $user_id='') {
		$where ['products.deleted_at']= null;
		$size ="";
		$allRecords = Product::where($where)
		->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
		->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
		->select('products.*')
		->whereIn('products.product_category_id',array($curr_category_id))
		->whereNotIn( 'products._id', [$curr_product_id])
		->with('product_category')
        ->with('product_subcategory')
		->inRandomOrder()
		->groupBy('products._id')
		->take($count)->skip(0)
		->get();
		return $allRecords;
	}

	public function productSearch(Request $request){	
        $this->bladeVar['page']['title'] = "Search - Glamour Jewellery";
		$this->bladeVar['site_setting'] = Setting::withTrashed()->orderBy('_id','ASC')->get(); 

		$this->bladeVar['color_results'] = Color::withTrashed()->whereNull('deleted_at')->orderBy('id','DESC')->get();

		$this->bladeVar['size_results'] = Size::withTrashed()->whereNull('deleted_at')->orderBy('id','DESC')->get();
		
		if($request->has('search')) {
			$q = $request->search;
			$this->bladeVar['product_results'] = Product::where('products.name', 'like', '%'.$q.'%')
				->orWhere('products.details', 'like', '%'.$q.'%')
				->orWhere('pb.name', 'like', '%'.$q.'%')
				->orWhere('pc.name', 'like', '%'.$q.'%')
				->orWhere('psc.name', 'like', '%'.$q.'%')
				->join('product_brand as pb', 'pb._id', '=', 'products.brand_id')
				->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
				->join('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
				->select('products.*')
				->with('product_brand')
				->with('product_category')
				->with('product_subcategory')
				->orderBy('products._id', 'DESC')
				->groupBy('products._id')
				->paginate('10');
		}else{
			$this->bladeVar['product_results'] = array();
		}

        $userCartDetails = $this->userCartDetails();
		$this->bladeVar['userCartProducts'] = $userCartDetails['userCartProducts'];
		$this->bladeVar['userCartData'] = $userCartDetails['userCartData'];
		$this->bladeVar['userCart'] = $userCartDetails['userCart'];
		$this->bladeVar['userWishlistData'] = $userCartDetails['userWishlistData'];
		$this->bladeVar['userWishlist'] = $userCartDetails['userWishlist'];

		return view('frontend/search', ['bladeVar' => $this->bladeVar]);
	}

    public function allblogs(Request $request){
		$category = $request->input('category');
		$archive = $request->input('archive');
		$tag = $request->input('tag');
		$search = $request->input('search'); 
		
		$this->bladeVar['page']['title'] = "Blogs - Glamour Jewellery";
		$this->bladeVar['site_setting'] = Setting::withTrashed()->orderBy('_id','ASC')->get();

        if(!empty($search)){
			$this->bladeVar['results'] = Blogs::withTrashed()
			    ->where('blogs.name', 'LIKE', '%'.$search.'%')
				->join('blog_pics','blog_pics.blog_id','=','blogs._id')
				->join('blog_categories as bc', 'bc._id', '=', 'blogs.blog_category_id')
				->leftJoin('blog_tags as bt', 'bt.id', '=', 'blogs.blog_tag_id')
				->select('blogs.*')
				->with('blog_pics')
				->with('blog_category')
				->with('blog_tag')
				->orderBy('_id','DESC')
				->groupBy('blogs._id')
				->paginate('10');
		}else if(!empty($category)){
            $this->bladeVar['results'] = Blogs::withTrashed()
			    ->join('blog_pics','blog_pics.blog_id','=','blogs._id')
				->join('blog_categories as bc', 'bc._id', '=', 'blogs.blog_category_id')
				->leftJoin('blog_tags as bt', 'bt.id', '=', 'blogs.blog_tag_id')
				->where('bc.name', 'LIKE', '%'.$category.'%')
				->select('blogs.*')
				->with('blog_pics')
				->with('blog_category')
				->with('blog_tag')
				->orderBy('_id','DESC')
				->groupBy('blogs._id')
				->paginate('10');
		}else if(!empty($tag)){
            $this->bladeVar['results'] = Blogs::withTrashed()
			    ->join('blog_pics','blog_pics.blog_id','=','blogs._id')
				->join('blog_categories as bc', 'bc._id', '=', 'blogs.blog_category_id')
				->leftJoin('blog_tags as bt', 'bt.id', '=', 'blogs.blog_tag_id')
				->where('bt.name', 'LIKE', '%'.$tag.'%')
				->select('blogs.*')
				->with('blog_pics')
				->with('blog_category')
				->with('blog_tag')
				->orderBy('_id','DESC')
				->groupBy('blogs._id')
				->paginate('10');
		}else if(!empty($archive)){
			$currentTimestamp = date("n",strtotime($archive));
			$this->bladeVar['results'] = Blogs::withTrashed()
				->whereMonth('blogs.created_at', 'LIKE', '%'.$currentTimestamp.'%')
			    ->join('blog_pics','blog_pics.blog_id','=','blogs._id')
				->join('blog_categories as bc', 'bc._id', '=', 'blogs.blog_category_id')
				->leftJoin('blog_tags as bt', 'bt.id', '=', 'blogs.blog_tag_id')
				->select('blogs.*')
				->with('blog_pics')
				->with('blog_category')
				->with('blog_tag')
				->orderBy('_id','DESC')
				->groupBy('blogs._id')
				->paginate('10');
		}else{
			$this->bladeVar['results'] = Blogs::withTrashed()
				->join('blog_pics','blog_pics.blog_id','=','blogs._id')
				->join('blog_categories as bc', 'bc._id', '=', 'blogs.blog_category_id')
				->leftJoin('blog_tags as bt', 'bt.id', '=', 'blogs.blog_tag_id')
				->select('blogs.*')
				->with('blog_pics')
				->with('blog_category')
				->with('blog_tag')
				->orderBy('_id','DESC')
				->groupBy('blogs._id')
				//->get();
				->paginate('10');
		}

		$this->bladeVar['blogPics'] = DB::select('select * from blog_pics');

		$this->bladeVar['blogArchives'] = Blogs::select(DB::raw("(COUNT(*)) as count"),DB::raw("MONTHNAME(created_at) as monthname"))
		    ->whereNull('deleted_at')
			->whereYear('created_at', date('Y'))
            ->groupBy('monthname')
            ->get();

		$this->bladeVar['blogTags'] = DB::select('SELECT blog_tags.name, blog_tags.slug, blog_tags.deleted_at, COUNT(blogs.blog_tag_id) AS tagBlogs FROM blog_tags LEFT JOIN blogs ON blog_tags.id = blogs.blog_tag_id GROUP BY blog_tags.id');
		
		$this->bladeVar['blogCategories'] = DB::select('SELECT blog_categories.name, blog_categories.slug, blog_categories.deleted_at, COUNT(blogs.blog_category_id) AS categoryBlogs FROM blog_categories LEFT JOIN blogs ON blog_categories._id = blogs.blog_category_id GROUP BY blog_categories._id');

		$this->bladeVar['recent_post'] = Blogs::withTrashed()
		    ->join('blog_pics','blog_pics.blog_id','=','blogs._id')
            ->join('blog_categories as bc', 'bc._id', '=', 'blogs.blog_category_id')
            ->leftJoin('blog_tags as bt', 'bt.id', '=', 'blogs.blog_tag_id')
			->select('blogs.*')
			->with('blog_pics')
            ->with('blog_category')
			->with('blog_tag')
            ->orderBy('_id','DESC')
			->groupBy('blogs._id')
			->limit(3)
			->get(); 

		$userCartDetails = $this->userCartDetails();
		$this->bladeVar['userCartProducts'] = $userCartDetails['userCartProducts'];
		$this->bladeVar['userCartData'] = $userCartDetails['userCartData'];
		$this->bladeVar['userCart'] = $userCartDetails['userCart'];
		$this->bladeVar['userWishlistData'] = $userCartDetails['userWishlistData'];
		$this->bladeVar['userWishlist'] = $userCartDetails['userWishlist'];	

        return view('frontend/blog-list', ['bladeVar' => $this->bladeVar]);
	}

	public function blog_detail($slug){
		$this->bladeVar['page']['title'] = "Blogs - Glamour Jewellery";
		$this->bladeVar['site_setting'] = Setting::withTrashed()->orderBy('_id','ASC')->get();

		$this->bladeVar['blogArchives'] = Blogs::select(DB::raw("(COUNT(*)) as count"),DB::raw("MONTHNAME(created_at) as monthname"))
		    ->whereNull('deleted_at')
			->whereYear('created_at', date('Y'))
            ->groupBy('monthname')
            ->get();

		$this->bladeVar['blogTags'] = DB::select('SELECT blog_tags.name, blog_tags.slug, blog_tags.deleted_at, COUNT(blogs.blog_tag_id) AS tagBlogs FROM blog_tags LEFT JOIN blogs ON blog_tags.id = blogs.blog_tag_id GROUP BY blog_tags.id');
		
		$this->bladeVar['blogCategories'] = DB::select('SELECT blog_categories.name, blog_categories.slug, blog_categories.deleted_at, COUNT(blogs.blog_category_id) AS categoryBlogs FROM blog_categories LEFT JOIN blogs ON blog_categories._id = blogs.blog_category_id GROUP BY blog_categories._id');

		$this->bladeVar['blogresult'] = Blogs::where('slug', $slug)->whereNull('deleted_at')->first();
		$blog_id = $this->bladeVar['blogresult']['_id'];
		if(!empty($blog_id)){
			$getRecord = BlogsPics::where(['blog_id' => $blog_id])->select('*')->get();
			if(!empty($getRecord)){
				$getSlideImage = [];
				$i=0;
				foreach($getRecord as $records) {
					if($records->image) {
						$getSlideImage[$i]['image'] = $records->image;
					} else {
						$getSlideImage[$i]['image'] = '';
					}
					$i++;
				}  
				$this->bladeVar['blogPics'] = $getSlideImage;
			}
		}

		$this->bladeVar['blogCommentsCount'] = DB::select("SELECT COUNT(id) as commentsCount FROM comments WHERE type = 'blog' AND postId = ".$blog_id);
        $this->bladeVar['blogComments'] = DB::select("SELECT * FROM comments WHERE type = 'blog' AND postId = ".$blog_id);

		$this->bladeVar['recent_post'] = Blogs::withTrashed()
		    ->whereNotIn('blogs._id', [$blog_id])
		    ->join('blog_pics','blog_pics.blog_id','=','blogs._id')
            ->join('blog_categories as bc', 'bc._id', '=', 'blogs.blog_category_id')
            ->leftJoin('blog_tags as bt', 'bt.id', '=', 'blogs.blog_tag_id')
			->select('blogs.*')
			->with('blog_pics')
            ->with('blog_category')
			->with('blog_tag')
            ->orderBy('_id','DESC')
			->groupBy('blogs._id')
			->limit(3)
			->get(); 

        $userCartDetails = $this->userCartDetails();
		$this->bladeVar['userCartProducts'] = $userCartDetails['userCartProducts'];
		$this->bladeVar['userCartData'] = $userCartDetails['userCartData'];
		$this->bladeVar['userCart'] = $userCartDetails['userCart'];
		$this->bladeVar['userWishlistData'] = $userCartDetails['userWishlistData'];
		$this->bladeVar['userWishlist'] = $userCartDetails['userWishlist'];

        return view('frontend/blog-detail', ['bladeVar' => $this->bladeVar]);
	}   

	public function login_register(){
		$this->bladeVar['page']['title'] = "Login-Register - Glamour Jewellery";
		$this->bladeVar['site_setting'] = Setting::withTrashed()->orderBy('_id','ASC')->get();

        $userCartDetails = $this->userCartDetails();
		$this->bladeVar['userCartProducts'] = $userCartDetails['userCartProducts'];
		$this->bladeVar['userCartData'] = $userCartDetails['userCartData'];
		$this->bladeVar['userCart'] = $userCartDetails['userCart'];
		$this->bladeVar['userWishlistData'] = $userCartDetails['userWishlistData'];
		$this->bladeVar['userWishlist'] = $userCartDetails['userWishlist'];

        return view('frontend/login-register', ['bladeVar' => $this->bladeVar]);
	}

	public function myAccount(){
		$this->bladeVar['page']['title'] = "My Account - Glamour Jewellery";
		$this->bladeVar['site_setting'] = Setting::withTrashed()->orderBy('_id','ASC')->get();
        $data = Session::all();
		if (Session::has('userLoggedIn')){
			$userId = Session::get('userLoggedIn');
			$result = DB::select('SELECT * FROM site_users WHERE _id = "'.$userId.'"');
			$this->bladeVar['userDetails'] = $result[0];

			
			if(!empty($userId)){
				$orderRecords = array();
				$orders = Orders::where(['user_id' => $userId])->select('*')->get();
				foreach($orders as $key => $order){
					/*$orderDetails = OrderDetail::where(['order_id' => $order->_id])
						->select('orderUniqid','product_id','product_name','product_details','product_image','quantity','price','discounted_price','order_status','created_at','updated_at')
						->get();
					foreach($orderDetails as $k => $orderDetail){
						$orderRecords[$k]['id'] = $order->_id;
						$orderRecords[$k]['firstname'] = $order->firstname;
						$orderRecords[$k]['lastname'] = $order->lastname;
						$orderRecords[$k]['phone'] = $order->phone;
						$orderRecords[$k]['address'] = $order->address;
						$orderRecords[$k]['payment_method'] = $order->payment_method;
						$orderRecords[$k]['amount'] = $order->amount;
						$orderRecords[$k]['orderDate'] = $this->dateFormate($order->created_at);

						$orderRecords[$k]['orderUniqid'] = $orderDetail->orderUniqid;
						$orderRecords[$k]['product_id'] = $orderDetail->product_id;
						$orderRecords[$k]['product_name'] = $orderDetail->product_name;
						$orderRecords[$k]['product_details'] = $orderDetail->product_details;
						if(isset($orderDetail->product_image)) {
							$orderRecords[$k]['product_image'] = url('/')."/images/products/".$orderDetail->product_image;
						} else {
							$orderRecords[$k]['product_image'] = url('/')."/images/no-image.jpg";
						}
						$orderRecords[$k]['quantity'] = $orderDetail->quantity;
						$orderRecords[$k]['price'] = $orderDetail->price;
						$orderRecords[$k]['discounted_price'] = $orderDetail->discounted_price;
						$orderRecords[$k]['order_status'] = $orderDetail->order_status;
						$orderRecords[$k]['createdDate'] = $this->dateFormate($orderDetail->created_at);
						$orderRecords[$k]['updatedDate'] = $this->dateFormate($orderDetail->updated_at);
					}*/
					$orderRecords[$key]['id'] = $order->_id;
					$orderRecords[$key]['firstname'] = $order->firstname;
					$orderRecords[$key]['lastname'] = $order->lastname;
					$orderRecords[$key]['phone'] = $order->phone;
					$orderRecords[$key]['address'] = $order->address;
					$orderRecords[$key]['payment_method'] = $order->payment_method;
					$orderRecords[$key]['amount'] = $order->amount;
					$orderRecords[$key]['orderDate'] = $this->dateFormate($order->created_at);							
				}				  
				$this->bladeVar['orders'] = $orderRecords;   
			}
		}

        $userCartDetails = $this->userCartDetails();
		$this->bladeVar['userCartProducts'] = $userCartDetails['userCartProducts'];
		$this->bladeVar['userCartData'] = $userCartDetails['userCartData'];
		$this->bladeVar['userCart'] = $userCartDetails['userCart'];
		$this->bladeVar['userWishlistData'] = $userCartDetails['userWishlistData'];
		$this->bladeVar['userWishlist'] = $userCartDetails['userWishlist'];

        return view('frontend/my-account', ['bladeVar' => $this->bladeVar]);
	}

	public function userWishlist(){
		$this->bladeVar['page']['title'] = "Wishlist - Glamour Jewellery";
		$this->bladeVar['site_setting'] = Setting::withTrashed()->orderBy('_id','ASC')->get();

		$this->bladeVar['product_results'] = Product::withTrashed() 
            ->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
            ->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
            ->select('products.*')
            ->with('product_category')
            ->with('product_subcategory')
            ->orderBy('_id', 'DESC')
			->groupBy('products._id')
			->get(); 	

        $userCartDetails = $this->userCartDetails();
		$this->bladeVar['userCartProducts'] = $userCartDetails['userCartProducts'];
		$this->bladeVar['userCartData'] = $userCartDetails['userCartData'];
		$this->bladeVar['userCart'] = $userCartDetails['userCart'];
		$this->bladeVar['userWishlistData'] = $userCartDetails['userWishlistData'];
		$this->bladeVar['userWishlist'] = $userCartDetails['userWishlist'];

        return view('frontend/wishlist', ['bladeVar' => $this->bladeVar]);
	}

	public function userCart(){
		$this->bladeVar['page']['title'] = "Cart - Glamour Jewellery";
		$this->bladeVar['site_setting'] = Setting::withTrashed()->orderBy('_id','ASC')->get();

		$userCartDetails = $this->userCartDetails();
		$this->bladeVar['userCartProducts'] = $userCartDetails['userCartProducts'];
		$this->bladeVar['userCartData'] = $userCartDetails['userCartData'];
		$this->bladeVar['userCart'] = $userCartDetails['userCart'];
		$this->bladeVar['userWishlistData'] = $userCartDetails['userWishlistData'];
		$this->bladeVar['userWishlist'] = $userCartDetails['userWishlist'];

        return view('frontend/cart', ['bladeVar' => $this->bladeVar]);
	}

	public function userCheckout(){
		$this->bladeVar['page']['title'] = "Checkout - Glamour Jewellery";
		$this->bladeVar['site_setting'] = Setting::withTrashed()->orderBy('_id','ASC')->get();
        $data = Session::all();
		if (Session::has('userLoggedIn')){
			$userId = Session::get('userLoggedIn');
			$result = DB::select('SELECT * FROM site_users WHERE _id = "'.$userId.'"');
			$this->bladeVar['userDetails'] = $result[0];
		} 
		$userCartDetails = $this->userCartDetails();
		$this->bladeVar['userCartProducts'] = $userCartDetails['userCartProducts'];
		$this->bladeVar['userCartData'] = $userCartDetails['userCartData'];
		$this->bladeVar['userCart'] = $userCartDetails['userCart'];
		$this->bladeVar['userWishlistData'] = $userCartDetails['userWishlistData'];
		$this->bladeVar['userWishlist'] = $userCartDetails['userWishlist'];

        return view('frontend/checkout', ['bladeVar' => $this->bladeVar]);
	}

	public function userCartDetails(){
		$data = Session::all();
		if (Session::has('userLoggedIn')){ 
			$token = Session::get('userLoggedIn');
			$user = SiteUsers::where(['_id' => $token])->select('_id')->first();
			if(!empty($user->_id)){
                $usercartData = Usercart::where('user_id', '=', $user->_id)->select('*')->orderBy('_id','ASC')->get();
				if(!empty($usercartData)){
					$userCart['userCartProducts'] = Usercart::where(['user_id' => $user->_id])->join('products as prod','prod._id','=','users_cart.product_id')->select('prod.*','users_cart.quantity')->orderBy('_id','asc')->get();
					$userCart['userCartData'] = $usercartData;
                    $userCart['userCart'] = count($usercartData);
				}else{
					$userCart['userCartProducts'] = array();
					$userCart['userCartData'] = $usercartData;
                    $userCart['userCart'] = count($usercartData);
				}
				
				$userWishlistData = Userwishlist::where(['user_id' => $user->_id, 'status' => '1'])->select('*')->orderBy('_id','ASC')->get();
				if(!empty($userWishlistData)){
					$userCart['userWishlistData'] = $userWishlistData;
                    $userCart['userWishlist'] = count($userWishlistData);
				}else{
					$userCart['userWishlistData'] = array();
                    $userCart['userWishlist'] = '0';
				}
			}
		}else{
			$userCart['userCartProducts'] = array();
            $userCart['userCartData'] = array();
            $userCart['userCart'] = '0';

			$userCart['userWishlistData'] = array();
			$userCart['userWishlist'] = '0';
		}
		return $userCart;
	}
	
	public function contactform(Request $request){
		$token = $request->_token;
		$name = $request->name;
		$phone = $request->phone;
		$email_address = $request->email_address;
		$contact_subject = $request->contact_subject;
		$message = $request->message;   
		$new_contact = Contact::updateOrCreate([
			'token' => $token,
			'name' => $name,
			'phone' => $phone,
			'email' => $email_address,
			'subject' => $contact_subject,
			'message' => $message
		]);
		return response()->json(['responseData'=>'comment send successfully']);
	}
	
	public function sendcomment(Request $request){
        $token = $request->_token;
		$name = $request->name;
		$website = $request->website;
		$email = $request->email;
		$comment = $request->comment;
		$postId = $request->postId;   
		$ptitle = $request->ptitle;
		$type = $request->type;
		$new_comment = Comments::updateOrCreate([
			'token' => $token,
			'name' => $name,
			'website' => $website,
			'email' => $email,
			'comment' => $comment,
			'postId' => $postId,
			'postName' => $ptitle,
			'type' => $type
		]);
		return response()->json(['responseData'=>'comment send successfully']);
	}
	//-------------------------User Account Functionality--------------------------------------- 
	public function usersignup(Request $request){
		$site_setting = Setting::withTrashed()->orderBy('_id','ASC')->get();
        $title = $site_setting[0]->title;
		$image = asset('images/cms').'/'.$site_setting[0]->image;
		$email = $site_setting[0]->email;

		$input = $request->all();
		$arrResult = array();
	    if(!empty($request->email) && !empty($request->name) && !empty($request->password)){
            $result = DB::select('SELECT * FROM site_users WHERE email = "'.$request->email.'"');
            if (!empty($result)) {
				$arrResult['id'] = $result[0]->_id;
                $arrResult['emailId'] = $result[0]->email;
            } else {
                $userCreate = SiteUsers::create([
					'username' => $request->name,
					'email' => $request->email,
					'password' => md5($request->password),
				]);
				if (!empty($userCreate->_id)) {
					$arrResult['userId'] = $userCreate->_id;
					$arrResult['emailId'] = $request->name;   
				}	
			}
			$responseData = array();
            if ((!empty($arrResult['userId'])) && (!empty($arrResult['emailId']))) {
                $responseData['responseData'] = 'user created successfully';
				Session::put('userLoggedIn', $arrResult['userId']);
				
				$userEmaildata = array( 'email' => $request->email, 'username' => $request->name );
				$data = array('username' => $request->name,'title' => $title,'image' => $image,'email' => $email);
				/*Mail::send('frontend.emails.signup', $data, function($message) use ($userEmaildata) {
					$message->to($userEmaildata['email'], $userEmaildata['username'])->subject('User Signup');
				});
                                if (Mail::failures()) {  
					//$responseData['responseData'] = 'Mail not send successfully ';
				}else{
					//$responseData['responseData'] = 'Mail send successfully';
				}*/
            } else {
				$baseurl = url('/login-register');
                $responseData['responseData'] = 'account already exists. <a href="' . $baseurl . '">Log in</a> to continue.';
            }
            return response()->json($responseData);  
		}else{
			return response()->json(['responseData' => 'Somthing Went Wrong']);
		}
	}

	public function userlogin(Request $request){
		$input = $request->all();
		$arrResult = array();
		if(!empty($request->email) && !empty($request->password)){
			$responseData = array();
            $result1 = DB::select('SELECT * FROM site_users WHERE email = "'.$request->email.'"');
			if (!empty($result1)) { 
				$result = DB::select('SELECT * FROM site_users WHERE email = "'.$request->email.'" AND password = "'.md5($request->password).'"');
				if (!empty($result)) { 
					if(($result[0]->deleted_at == '') && ($result[0]->deleted_at == NULL)){
						$arrResult['userId'] = $result[0]->_id;
						$arrResult['emailId'] = $result[0]->email;

						Session::put('userLoggedIn', $arrResult['userId']);
						$responseData['responseData'] = 'logged in successfully';
					}else{
						$responseData['statusError'] = 'Your account temporarily deactivated';
					}
				} else {
					if ($request->email != $result1[0]->email) {
						$responseData['emailError'] = 'Please enter a valid email address';
					}
					if ($request->password != $result1[0]->password) {
						$responseData['passwordError'] = 'Invalid password';
					}
				}
            } else {
                $responseData['responseData'] = "We can't find an account with this email address.";	
			}
            return response()->json($responseData);  
		}else{
			return response()->json(['responseData' => 'Somthing Went Wrong']);
		}
	}
 
	public function userforgotPassword(Request $request){
		$site_setting = Setting::withTrashed()->orderBy('_id','ASC')->get();
        $title = $site_setting[0]->title;
		$image = asset('images/cms').'/'.$site_setting[0]->image;
		$email = $site_setting[0]->email;

		$input = $request->all();
		$arrResult = array();
		$responseData = array();
		if(!empty($request->email)){
			$userData = array();
            $userData['email'] = $request->email;
			$checkEmail = $this->checkUserEmail($userData);
			if (!empty($checkEmail['emailId'])) {
				$baseurl = url('/');
				$userEmaildata = array( 'email' => $checkEmail['emailId'], 'username' => $checkEmail['username'] );

				$str = $checkEmail['emailId'];
				$str1 = base64_encode($str);  
				$url = $baseurl.'/recover-password?hash='.$str1;
				
				$data = array('url' => $url,'username' => $checkEmail['username'],'title' => $title,'image' => $image,'email' => $email);
				/*Mail::send('frontend.emails.forgotmail', $data, function($message) use ($userEmaildata) {
					$message->to($userEmaildata['email'], $userEmaildata['username'])->subject('Reset Your Password');
				});
                                if (Mail::failures()) {  
					//$responseData['responseData'] = 'Mail not send successfully ';
				}else{
					//$responseData['responseData'] = 'Mail send successfully';
				}*/
				$responseData['status'] = 'success';
			} else {
				$baseurl = url('/login-register');
				$responseData['responseData'] = "Sorry, We can't find an account with this email address. Please try again or <a href='" . $baseurl . "' class='links'>Create a new account</a>.";
			}
            return response()->json($responseData); 
		}else{
			return response()->json(['responseData' => 'Somthing Went Wrong']);
		}
	}

	public function userResetPassword(){
		$this->bladeVar['page']['title'] = "Reset Password - Glamour Jewellery";
		$this->bladeVar['site_setting'] = Setting::withTrashed()->orderBy('_id','ASC')->get();

		$data = Session::all();
		if (Session::has('userLoggedIn')){ 
			$token = Session::get('userLoggedIn');
			$user = SiteUsers::where(['_id' => $token])->select('_id')->first();
			if(!empty($user->_id)){
				$usercartData = Usercart::where('user_id', '=', $user->_id)->select('*')->orderBy('_id','ASC')->get();
				if(!empty($usercartData)){
					$this->bladeVar['userCartData'] = $usercartData;
                    $this->bladeVar['userCart'] = count($usercartData);
				}else{
					$this->bladeVar['userCartData'] = $usercartData;
                    $this->bladeVar['userCart'] = count($usercartData);
				}
				
				$userWishlistData = Userwishlist::where(['user_id' => $user->_id, 'status' => '1'])->select('*')->orderBy('_id','ASC')->get();
				if(!empty($userWishlistData)){
					$this->bladeVar['userWishlistData'] = $userWishlistData;
                    $this->bladeVar['userWishlist'] = count($userWishlistData);
				}else{
					$this->bladeVar['userWishlistData'] = array();
                    $this->bladeVar['userWishlist'] = '0';
				}
			}
		}else{
            $this->bladeVar['userCartData'] = array();
            $this->bladeVar['userCart'] = '0';

			$this->bladeVar['userWishlistData'] = array();
			$this->bladeVar['userWishlist'] = '0';
		}

        return view('frontend/reset-password', ['bladeVar' => $this->bladeVar]);
	}

	public function userRecoverPassword(Request $request){
		$input = $request->all();
		$arrResult = array();
		$responseData = array();
		if(!empty($request->email)){
			$userData = array();
            $userData['email'] = $request->email;
			$checkEmail = $this->checkUserEmail($userData);
			if (!empty($checkEmail['emailId'])) {
				$userEmail = $checkEmail['emailId'];
				$userData = SiteUsers::where('email', $userEmail)->update(['password' => md5($request->password)]);
                if (!empty($userData)) {
					$responseData['responseData'] = 'password update successfully';
                    $responseData['status'] = 'success'; 
				}else{
					$responseData['responseData'] = 'Somthing went wrong';
                    $responseData['status'] = 'error';
				}
			} else {
				$responseData['responseData'] = 'Somthing went wrong';
				$responseData['status'] = 'error';
			}
            return response()->json($responseData); 
		}else{
			return response()->json(['responseData' => 'Somthing Went Wrong']);
		}
	}

	public function userDetails(Request $request){
        $input = $request->all();
		$arrResult = array();
		$responseData = array();
		if(!empty($request->email)){
			$userData = array();
            $userData['email'] = $request->email;
			$checkEmail = $this->checkUserEmail($userData);
			if (!empty($checkEmail['emailId'])) {
				$userEmail = $checkEmail['emailId'];
				$userData = SiteUsers::where('email', $userEmail)->update(['username'=> $request->username,'email'=> $request->email,'fname'=> $request->fname,'lname'=> $request->lname]);
                if (!empty($userData)) {
					$responseData['responseData'] = 'user detail update successfully'; 
				}else{
					$responseData['responseData'] = 'Somthing went wrong';
				}
			} else {
				$responseData['responseData'] = 'Somthing went wrong';
			}
            return response()->json($responseData); 
		}else{
			return response()->json(['responseData' => 'Somthing Went Wrong']);
		}
	}

	public function updatePassword(Request $request){
        $input = $request->all();
		$arrResult = array();
		$responseData = array();
		if(!empty($request->email)){
			$userData = array();
			$userData['email'] = $request->email;
			$userData['password'] = $request->currentpassword;
			$checkEmail = $this->checkUserPassword($userData);
			if (!empty($checkEmail['emailId'])) {
				$userEmail = $checkEmail['emailId'];
				if($request->currentpassword != $request->newpassword){
					$userData = SiteUsers::where('email', $userEmail)->update(['password'=> md5($request->newpassword)]);
					if (!empty($userData)) {
						$responseData['responseData'] = 'Your account password update successfully'; 
					}else{
						$responseData['responseData'] = 'Somthing went wrong';
					}
				}else{
					$responseData['responseData'] = 'request to change password already exist';
				}
			} else {
				$responseData['responseData'] = 'Invalid password';
			}
            return response()->json($responseData); 
		}else{
			return response()->json(['responseData' => 'You are not authorized user']);
		}
	}

	public function checkUserEmail($arrPostedData = array()){
        $arrResult = array();
        if ($arrPostedData['email']) {
            $result = DB::select('SELECT * FROM site_users WHERE email = "'.$arrPostedData['email'].'"');
            if (!empty($result)) {
				$arrResult['id'] = $result[0]->_id;
				$arrResult['emailId'] = $result[0]->email;
				$arrResult['username'] = $result[0]->username;
            } else {
                $arrResult['emailId'] = '';
            }
        }
        return $arrResult;
	}

	public function checkUserPassword($arrPostedData = array()){
        $arrResult = array();
        if (($arrPostedData['email']) && ($arrPostedData['password'])) {
            $result = DB::select('SELECT * FROM site_users WHERE email = "'.$arrPostedData['email'].'" AND password = "'.md5($arrPostedData['password']).'"');
            if (!empty($result)) {
				$arrResult['id'] = $result[0]->_id;
				$arrResult['emailId'] = $result[0]->email;
				$arrResult['username'] = $result[0]->username;
            } else {
                $arrResult['emailId'] = '';
            }
        }
        return $arrResult;
	}
	
	public function userlogout(Request $request){
		$input = $request->all();
		$data = Session::all();
		if (Session::has('userLoggedIn')){
			Session::flush();
			return response()->json(['responseData' => 'You have successfully logged out.']);
		}else{
			return response()->json(['responseData' => 'You are not authorized user']);
		}
	}
	
    //-------------------------User Cart Functionality--------------------------------------- 
	public function addToCart(Request $request) {
		$data = Session::all();
		if (Session::has('userLoggedIn')){ 
			$token = Session::get('userLoggedIn');
			$user = SiteUsers::where(['_id' => $token])->select('_id')->first();
			$user_id = $user->_id;
			$product_id = $request->product_id;
			$color = $request->color_id;
			$size = $request->size_id;
			if(!empty($request->quantity)){
               $quantity = $request->quantity;
			}else{
               $quantity = '1';
			}
			
			$cartStatus = Usercart::where(['user_id' => $user->_id, 'product_id' => $product_id])->select('*')->first();
			if(!empty($cartStatus)){
                $data = ['user_id' => $user->_id,'product_id' => $product_id,'quantity' => $quantity];
			    Usercart::updateOrCreate(['user_id' => $user->_id, 'product_id' => $product_id],$data);
                $responseData = 'remove to cart successfully';  
			}else{
                $data = ['user_id' => $user->_id,'product_id' => $product_id,'color' => $color,'size' => $size,'quantity' => $quantity];
			    Usercart::updateOrCreate(['user_id' => $user->_id, 'product_id' => $product_id],$data);
				$responseData = 'add to cart successfully';
			}
			$total_count = Usercart::where(['user_id' => $user->_id])->count();
			return response()->json(['responseData'=> $responseData,'responseDataCount' => $total_count]);
		}else{
			return response()->json(['responseData'=>'error in add to cart']);
		}
	}

	public function getViewcartItems() {
		$userCartDetails = $this->userCartDetails();
		$this->bladeVar['userCartProducts'] = $userCartDetails['userCartProducts'];
		$this->bladeVar['userCartData'] = $userCartDetails['userCartData'];
		$this->bladeVar['userCart'] = $userCartDetails['userCart'];
		$this->bladeVar['userWishlistData'] = $userCartDetails['userWishlistData'];
		$this->bladeVar['userWishlist'] = $userCartDetails['userWishlist'];
        return view('frontend/viewcart', ['bladeVar' => $this->bladeVar]);
	}

	public function removeCartItem(Request $request) {
		$data = Session::all();
		if (Session::has('userLoggedIn')){ 
			$token = Session::get('userLoggedIn');
			$user = SiteUsers::where(['_id' => $token])->select('_id')->first();
			$user_id = $user->_id;
			$product_id = $request->product_id;
			Usercart::where(['user_id' => $user->_id, 'product_id' => $product_id])->forceDelete();	
			$total_count = Usercart::where(['user_id' => $user->_id])->count();
			return response()->json(['responseData'=> 'remove to cart successfully','responseDataCount' => $total_count]);
		}else{
			return response()->json(['responseData'=>'error in remove to cart']);
		}
	}

	public function updateCart(Request $request) {
		$data = Session::all();
		if (Session::has('userLoggedIn')){ 
			$token = Session::get('userLoggedIn');
			$user = SiteUsers::where(['_id' => $token])->select('_id')->first();
			$user_id = $user->_id;
			$productArray = $request->productIds;
			if(!empty($productArray)){
                foreach($productArray as $product){
					$productId = $product['productId'];  
					$productQ = $product['productQ']; 
					if(!empty($productId)){
						/*$allRecords = Product::where(['products._id' => $productId])->select('products.product_quantity')->get();
						if($allRecords[0]->product_quantity > 0){ 
							$quantityValue = $allRecords[0]->product_quantity - $quantity;
							$updatedData['product_quantity'] = $quantityValue;
							$updatedQuantity = Product::where(['_id' => $productId])->update($updatedData);
						}*/
						$updatedData['quantity'] = $productQ;
						Usercart::where(['user_id' => $user->_id, 'product_id' => $productId])->update($updatedData);
					}
				}
			}	
			return response()->json(['responseData'=> 'update cart successfully']);
		}else{
			return response()->json(['responseData'=>'error in update to cart']);
		}
	}

	public function addToWishlist(Request $request) {
        $data = Session::all();
		if (Session::has('userLoggedIn')){ 
			$token = Session::get('userLoggedIn');
			$user = SiteUsers::where(['_id' => $token])->select('_id')->first();
			$user_id = $user->_id;
			$product_id = $request->product_id;
			$wishlistStatus = Userwishlist::where(['user_id' => $user->_id, 'product_id' => $product_id])->select('*')->first();
			if(!empty($wishlistStatus)){
				if($wishlistStatus->status == '1'){
					Userwishlist::where(['user_id' => $user->_id, 'product_id' => $product_id])->forceDelete(); 
					$responseData = 'remove to wishlist successfully';
				}else{
                    $data = ['user_id' => $user->_id,'product_id' => $product_id,'status' => '1'];
					$wishlistData = Userwishlist::updateOrCreate(['user_id' => $user->_id, 'product_id' => $product_id],$data);
					$responseData = 'add to wishlist successfully';
				}
			}else{
                $data = ['user_id' => $user->_id,'product_id' => $product_id,'status' => '1'];
				$wishlistData = Userwishlist::updateOrCreate(['user_id' => $user->_id, 'product_id' => $product_id],$data);
				$responseData = 'add to wishlist successfully';
			}
			$total_count = Userwishlist::where(['user_id' => $user->_id, 'status' => '1'])->count();
			return response()->json(['responseData'=> $responseData,'responseDataCount' => $total_count]);
		}else{
			return response()->json(['responseData'=>'error in add to wishlist']);
		}
	}

	public function applyCoupon(Request $request) {
		$userCartDetails = $this->userCartDetails();
		$userCartProducts = $userCartDetails['userCartProducts'];
		$userCartData = $userCartDetails['userCartData'];
		$userCart = $userCartDetails['userCart'];
		$userWishlistData = $userCartDetails['userWishlistData'];
		$userWishlist = $userCartDetails['userWishlist'];

		$arrayCartlist = array();
		if(!empty($userCartData)){ 
			foreach($userCartData as $cartlist){
				$arrayCartlist[] = $cartlist->product_id;
			} 
		} 

		$subTotal = array();
		
        if(!empty($request->shipping)){
            $shipping = $request->shipping;
		}else{
			$shipping = '0.00';
		}

		foreach($userCartProducts as $product){
			if($product->deleted_at == NULL){
				if(in_array($product->_id, $arrayCartlist)){
					if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')){
						$subTotal[] = number_format($product->discounted_price * $product->quantity, 2);
					}else{
						$subTotal[] = number_format($product->price * $product->quantity, 2);
					}
				}
			}
		}

		$data = Session::all();
		if (Session::has('userLoggedIn')){ 
			$token = Session::get('userLoggedIn');
			$user = SiteUsers::where(['_id' => $token])->select('_id')->first();
			$user_id = $user->_id;
			$couponCode = $request->couponCode;
            $responseData = ''; $subTotalPrice = '';  $totalPrice = ''; $percentage = '';
			$couponStatus = DB::select("SELECT * FROM site_users_coupon WHERE userId = '".$user->_id."' AND coupon_name = '".$couponCode."'");
			if(!empty($couponStatus)){
                $responseData = 'Coupon already used';  
			}else{
				$currentDate = strtotime(date('m/d/Y h:i A'));
				$couponData = DB::select("SELECT * FROM coupons WHERE name = '".$couponCode."'");
				if(!empty($couponData)){
					$expiry = strtotime($couponData['0']->expiry);
					$deleted_at = $couponData['0']->deleted_at;
					$type = $couponData['0']->type;
					$value = $couponData['0']->value;
                    if($deleted_at == ''){
						if($expiry >= $currentDate){
                            if($type == 'Flat'){
								$percentage = number_format($value, 2);
								if(!empty($subTotal)){ $subTotalPrice = number_format((array_sum($subTotal)), 2); }else{ $subTotalPrice = '0.00'; }
								if(!empty($subTotal)){ $totalPrice = number_format(((array_sum($subTotal)+$shipping)-$value), 2); }else{ $totalPrice = number_format(($shipping), 2); }
							}else{
								$numberFour = number_format($value, 2);
								$total = array_sum($subTotal);
								$percentage = number_format(($numberFour / 100) * $total, 2);
								if(!empty($subTotal)){ 
									$subTotalPrice = number_format((array_sum($subTotal)), 2); }else{ $subTotalPrice = '0.00'; }
								if(!empty($subTotal)){
									$totalPrice = number_format((($total - $percentage)+$shipping), 2); 
								}else{ $totalPrice = number_format(($shipping), 2); }
							}
							$responseData = 'coupon code apply successfully';
						}else{
							$responseData = 'This coupon code has expired.'; 
							if(!empty($subTotal)){ $subTotalPrice = number_format(array_sum($subTotal), 2); }else{ $subTotalPrice = '0.00'; }
							if(!empty($subTotal)){ $totalPrice = number_format((array_sum($subTotal)+$shipping), 2); }else{ $totalPrice = number_format(($shipping), 2); } 
						}
					}else{
						$responseData = 'This coupon code is invalid.';
						if(!empty($subTotal)){ $subTotalPrice = number_format(array_sum($subTotal), 2); }else{ $subTotalPrice = '0.00'; }
						if(!empty($subTotal)){ $totalPrice = number_format((array_sum($subTotal)+$shipping), 2); }else{ $totalPrice = number_format(($shipping), 2); }
					}	 
				}else{
					$responseData = 'This coupon code is invalid.';
					if(!empty($subTotal)){ $subTotalPrice = number_format(array_sum($subTotal), 2); }else{ $subTotalPrice = '0.00'; }
					if(!empty($subTotal)){ $totalPrice = number_format((array_sum($subTotal)+$shipping), 2); }else{ $totalPrice = number_format(($shipping), 2); }
				}
			}
			$total_count = Usercart::where(['user_id' => $user->_id])->count();
			return response()->json(['responseData'=> $responseData,'responseDataCount' => $total_count, 'subTotalPrice' => $subTotalPrice, 'shipping' => $shipping, 'totalPrice' => $totalPrice, 'percentage' => $percentage ]);
		}else{
			return response()->json(['responseData'=>'This coupon code is invalid.']);
		}
	}

	public function removeCoupon(Request $request) {
		$userCartDetails = $this->userCartDetails();
		$userCartProducts = $userCartDetails['userCartProducts'];
		$userCartData = $userCartDetails['userCartData'];
		$userCart = $userCartDetails['userCart'];
		$userWishlistData = $userCartDetails['userWishlistData'];
		$userWishlist = $userCartDetails['userWishlist'];

		$arrayCartlist = array();
		if(!empty($userCartData)){ 
			foreach($userCartData as $cartlist){
				$arrayCartlist[] = $cartlist->product_id;
			} 
		} 

		$subTotal = array();
		
        if(!empty($request->shipping)){
            $shipping = $request->shipping;
		}else{
			$shipping = '0.00';
		}   

		foreach($userCartProducts as $product){
			if($product->deleted_at == NULL){
				if(in_array($product->_id, $arrayCartlist)){
					if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')){
						$subTotal[] = number_format($product->discounted_price * $product->quantity, 2);
					}else{
						$subTotal[] = number_format($product->price * $product->quantity, 2);
					}
				}
			}
		}
        $responseData = ''; $subTotalPrice = '';  $totalPrice = ''; $percentage = '';
		$responseData = 'coupon code remove successfully';
		if(!empty($subTotal)){ $subTotalPrice = number_format(array_sum($subTotal), 2); }else{ $subTotalPrice = '0.00'; }
		if(!empty($subTotal)){ $totalPrice = number_format((array_sum($subTotal)+$shipping), 2); }else{ $totalPrice = number_format(($shipping), 2); }
		return response()->json(['responseData'=> $responseData, 'subTotalPrice' => $subTotalPrice, 'shipping' => $shipping, 'totalPrice' => $totalPrice, 'percentage' => $percentage ]);
	}

	public function placeOrder(Request $request){
		$billing_fname = $request->billing_fname;
		$billing_lname = $request->billing_lname;
		$billing_email = $request->billing_email;
		$billing_country = $request->billing_country;
		$billing_street = $request->billing_street;
		$billing_street1 = $request->billing_street1;
		$billing_city = $request->billing_city;
		$billing_state = $request->billing_state;
		$billing_postcode = $request->billing_postcode;
		$billing_phone = $request->billing_phone;
		$shipingDetails = $request->shipingDetails;
		$shiping_fname = $request->shiping_fname;
		$shiping_lname = $request->shiping_lname;
		$shiping_email = $request->shiping_email;
		$shiping_country = $request->shiping_country;
		$shiping_street = $request->shiping_street;
		$shiping_street1 = $request->shiping_street1;
		$shiping_city = $request->shiping_city;
		$shiping_state = $request->shiping_state;
		$shiping_postcode = $request->shiping_postcode;
		$billing_ordernote = $request->billing_ordernote;
		$couponCode = $request->couponCode;
		$paymentmethod = $request->paymentmethod;
		$shipping = $request->shipping;
        $data = Session::all();
		if (Session::has('userLoggedIn')){ 
			$token = Session::get('userLoggedIn');
			$user = SiteUsers::where(['_id' => $token])->select('_id')->first();
			$user_id = $user->_id;

			$updatedUserData = array();
			$updatedUserData['country'] = $billing_country;
			$updatedUserData['city'] = $billing_city;
			$updatedUserData['state'] = $billing_state;
			$updatedUserData['postcode'] = $billing_postcode;
			$updatedUserData['streetAddress'] = $billing_street;
			$updatedUserData['streetAddress1'] = $billing_street1;   
			SiteUsers::where(['_id' => $user_id])->update($updatedUserData);

			$userCartDetails = $this->userCartDetails();
			$userCartProducts = $userCartDetails['userCartProducts'];
			$userCartData = $userCartDetails['userCartData'];
			$userCart = $userCartDetails['userCart'];
			$userWishlistData = $userCartDetails['userWishlistData'];
			$userWishlist = $userCartDetails['userWishlist'];

			$arrayCartlist = array();
			if(!empty($userCartData)){ 
				foreach($userCartData as $cartlist){
					$arrayCartlist[] = $cartlist->product_id;
				} 
			} 

			$subTotal = array();
			if(!empty($request->shipping)){
				$shipping = $request->shipping;
			}else{
				$shipping = '0.00';
			}

			foreach($userCartProducts as $product){
				if($product->deleted_at == NULL){
					if(in_array($product->_id, $arrayCartlist)){
						if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')){
							$subTotal[] = number_format($product->discounted_price * $product->quantity, 2);
						}else{
							$subTotal[] = number_format($product->price * $product->quantity, 2);
						}
					}
				}
			}

            if(!empty($couponCode)){
				$couponData = DB::select('SELECT * FROM coupons WHERE name = "'.$couponCode.'"');
				if(!empty($couponData)){
					$expiry = strtotime($couponData['0']->expiry);
					$deleted_at = $couponData['0']->deleted_at;
					$type = $couponData['0']->type;
					$value = $couponData['0']->value;
					if($deleted_at == ''){
						if($expiry >= $currentDate){
							if($type == 'Flat'){
								$percentage = number_format($value, 2);
								if(!empty($subTotal)){ $subTotalPrice = number_format((array_sum($subTotal)), 2); }else{ $subTotalPrice = '0.00'; }
								if(!empty($subTotal)){ $totalPrice = number_format(((array_sum($subTotal)+$shipping)-$value), 2); }else{ $totalPrice = number_format(($shipping), 2); }
							}else{
								$numberFour = number_format($value, 2);
								$total = array_sum($subTotal);
								$percentage = number_format(($numberFour / 100) * $total, 2);
								if(!empty($subTotal)){ 
									$subTotalPrice = number_format((array_sum($subTotal)), 2); }else{ $subTotalPrice = '0.00'; }
								if(!empty($subTotal)){
									$totalPrice = number_format((($total - $percentage)+$shipping), 2); 
								}else{ $totalPrice = number_format(($shipping), 2); }
							}
						}else{
							if(!empty($subTotal)){ $subTotalPrice = number_format(array_sum($subTotal), 2); }else{ $subTotalPrice = '0.00'; }
							if(!empty($subTotal)){ $totalPrice = number_format((array_sum($subTotal)+$shipping), 2); }else{ $totalPrice = number_format(($shipping), 2); } 
						}
					}else{
						if(!empty($subTotal)){ $subTotalPrice = number_format(array_sum($subTotal), 2); }else{ $subTotalPrice = '0.00'; }
						if(!empty($subTotal)){ $totalPrice = number_format((array_sum($subTotal)+$shipping), 2); }else{ $totalPrice = number_format(($shipping), 2); }
					}	 
				}else{
					if(!empty($subTotal)){ $subTotalPrice = number_format(array_sum($subTotal), 2); }else{ $subTotalPrice = '0.00'; }
					if(!empty($subTotal)){ $totalPrice = number_format((array_sum($subTotal)+$shipping), 2); }else{ $totalPrice = number_format(($shipping), 2); }
				} 
			}else{
				$couponCode = ' ';
				if(!empty($subTotal)){ $subTotalPrice = number_format(array_sum($subTotal), 2); }else{ $subTotalPrice = '0.00'; }
				if(!empty($subTotal)){ $totalPrice = number_format((array_sum($subTotal)+$shipping), 2); }else{ $totalPrice = number_format(($shipping), 2); }
			}

            $address =  $billing_street.' '.$billing_street1.', '.$billing_city.', '.$billing_state.' - '.$billing_postcode.', '.$billing_country;
			if(!empty($shipingDetails)){
                $shippingAddress = $shiping_street.' '.$shiping_street1.', '.$shiping_city.', '.$shiping_state.' - '.$shiping_postcode.', '.$shiping_country;
			}else{
				$shippingAddress = ' ';
			}

			if($paymentmethod == 'Paypal'){    
				$array = array();
				$array['user_id'] = $user_id;
                $array['amount'] = $totalPrice;
				$getpaypal = $this->payWithpaypal($array);

				if(!empty($getpaypal)){
					$order_id = $this->addNewOrder($user_id,$billing_fname,$billing_lname,$billing_email,$address,$billing_phone,$shiping_fname,$shiping_lname,$shiping_email,$shippingAddress,$paymentmethod,$totalPrice,$shipping,$billing_ordernote,$couponCode);
					if(!empty($order_id)){
						$getOrderDetails = $this->addOrderDetails($user_id,$order_id);
						if(!empty($getOrderDetails)){
							//$emptyShoppingBag = $this->emptyShoppingBag($user_id);
						}
						//return response()->json(['responseData'=> 'Your order has been placed successfully']);
					}else{
						//return response()->json(['responseData'=>'error in order']);
					}
					Session::put('order_id', $order_id);
					Session::put('order_userid', $user_id);
					return response()->json(['responseData'=>'paypalurl','url' => $getpaypal]);
				}else{
					return response()->json(['responseData'=>'error in order']);
				}
			}else{
				$order_id = $this->addNewOrder($user_id,$billing_fname,$billing_lname,$billing_email,$address,$billing_phone,$shiping_fname,$shiping_lname,$shiping_email,$shippingAddress,$paymentmethod,$totalPrice,$shipping,$billing_ordernote,$couponCode);
				if(!empty($order_id)){
					$getOrderDetails = $this->addOrderDetails($user_id,$order_id);
					if(!empty($getOrderDetails)){
						$emptyShoppingBag = $this->emptyShoppingBag($user_id);
					}
					return response()->json(['responseData'=> 'Your order has been placed successfully']);
				}else{
					return response()->json(['responseData'=>'error in order']);
				}
			}
		}else{
			return response()->json(['responseData'=>'error in order']);
		}
	}

	public function addNewOrder($user_id,$billing_fname,$billing_lname,$billing_email,$address,$billing_phone,$shiping_fname,$shiping_lname,$shiping_email,$shippingAddress,$paymentmethod,$totalPrice,$shipping,$billing_ordernote,$couponCode){
		$user_id = $user_id;
		$emptyArray = '';
		$allRecords = Usercart::where(['user_id' => $user_id])
					->join('products as prod','prod._id','=','users_cart.product_id')
					->select('prod._id','prod.name','prod.details','prod.image','prod.seller_id','prod.price','prod.discounted_price','users_cart.quantity')
					->orderBy('name','asc')
					->get();
		if(!empty($allRecords)){	
			if(!empty($billing_fname)){
                $billingFname = $billing_fname; 
			}else{
                $billingFname = ''; 
			}
			if(!empty($billing_lname)){
                $billingLname = $billing_lname; 
			}else{
                $billingLname = ''; 
			}
			if(!empty($billing_email)){
                $billingEname = $billing_email; 
			}else{
                $billingEname = ''; 
			}
			if(!empty($address)){
                $billingAddress = $address; 
			}else{
                $billingAddress = ''; 
			}
			if(!empty($billing_phone)){
                $billingPhone = $billing_phone; 
			}else{
                $billingPhone = ''; 
			}
			if(!empty($shiping_fname)){
                $shipingFname = $shiping_fname; 
			}else{
                $shipingFname = ''; 
			}
			if(!empty($shiping_lname)){
                $shipingLname = $shiping_lname; 
			}else{
                $shipingLname = ''; 
			}
			if(!empty($shiping_email)){
                $shipingEmail = $shiping_email; 
			}else{
                $shipingEmail = ''; 
			}
            if(!empty($shippingAddress)){
                $sAddress = $shippingAddress; 
			}else{
                $sAddress = ''; 
			}
			if(!empty($billing_ordernote)){
                $billingOrdernote = $billing_ordernote; 
			}else{
                $billingOrdernote = ''; 
			}
			if(!empty($couponCode)){
                $coupon = $couponCode; 
			}else{
                $coupon = ''; 
			}
			if(!empty($totalPrice)){
                $price = $totalPrice; 
			}else{
                $price = ''; 
			}
            if(!empty($shipping)){
                $shippingCharge = $shipping; 
			}else{
                $shippingCharge = ''; 
			}
            if(!empty($paymentmethod)){
                $payment_method = $paymentmethod; 
			}else{
                $payment_method = ''; 
			}

			$new_order = Orders::create([
				'user_id' => $user_id,
				'firstname' => $billingFname,
				'lastname' => $billingLname,
				'email' => $billingEname,
				'address' => $billingAddress,
				'phone' => $billingPhone,
				'shiping_fname' => $shipingFname,
				'shiping_lname' => $shipingLname,
				'shiping_email' => $shipingEmail,
				'shipping_address' => $sAddress,
				'ordernote' => $billingOrdernote,
				'coupon' => $coupon,
				'amount' => $price,
				'shippingCharge' => $shippingCharge,
				'payment_method' => $payment_method,
			]);
			return $new_order->_id;
		}else{
			return $emptyArray; 
		}
	}

	public function addOrderDetails($user_id,$order_id){
		$user_id = $user_id;
		$allRecords = Usercart::where(['user_id' => $user_id])
					->join('products as prod','prod._id','=','users_cart.product_id')
					->select('prod._id','prod.name','prod.details','prod.image','prod.seller_id','prod.price','prod.discounted_price','users_cart.quantity','users_cart.color','users_cart.size')
					->orderBy('name','asc')
					->get();
		$orderData = array();			
		foreach($allRecords as $k => $records){
            if(!empty($records->discounted_price) && ($records->discounted_price > 0)){
			  $price = $records->discounted_price;
			}else{
			  $price = $records->price;
			}
			$productId = $records->_id;
			$quantity = $records->quantity;
			$updateProductQuantity = $this->updateProductQuantity($productId,$quantity);
			$orderUniqid = 'ORD-'.date('dmy').rand();
			$orderData[$k] = OrderDetail::create([
				'order_id' => $order_id,
				'orderUniqid' => $orderUniqid,
				'product_name' => $records->name,
				'product_details' => $records->details,
				'product_image' => $records->image,
                'order_status' => 'pending', 
				//'product_seller_id' => $records->seller_id,
				'product_id' => $records->_id,
				'quantity' => $records->quantity,
				'color' => $records->color,
				'size' => $records->size,
				'price' => $price,
				'discounted_price' => $records->discounted_price,
			]);
		}
		return $orderData;
	}

	public function updateProductQuantity($productId,$quantity) {
		$updatedQuantity = '';
		if(!empty($productId)){
			$allRecords = Product::where(['products._id' => $productId])->select('products.product_quantity')->get();
            if($allRecords[0]->product_quantity > 0){ 
                $quantityValue = $allRecords[0]->product_quantity - $quantity;
				$updatedData['product_quantity'] = $quantityValue;
				$updatedQuantity = Product::where(['_id' => $productId])->update($updatedData);
			}
	   }
	   return $updatedQuantity;
	}

	public function emptyShoppingBag($user_id) {
		$removeCart = Usercart::where(['user_id' => $user_id])->forceDelete();
		return $removeCart;
	}
 
	public function addressBook(Request $request) {
		$city = $request->city;
		$state = $request->state;
		$postcode = $request->postcode;
		$country = $request->country;
		$streetAddress = $request->streetAddress;
		$streetAddress1 = $request->streetAddress1;
        $data = Session::all();
		if (Session::has('userLoggedIn')){
            $token = Session::get('userLoggedIn');
			$user = SiteUsers::where(['_id' => $token])->select('_id')->first();
			$user_id = $user->_id;
			$updatedUserData = array();
			$updatedUserData['country'] = $country;
			$updatedUserData['city'] = $city;
			$updatedUserData['state'] = $state;
			$updatedUserData['postcode'] = $postcode;
			$updatedUserData['streetAddress'] = $streetAddress;
			$updatedUserData['streetAddress1'] = $streetAddress1;   
			SiteUsers::where(['_id' => $user_id])->update($updatedUserData);
			return response()->json(['responseData'=> 'user address detail update successfully']);
		}else{
			return response()->json(['responseData'=>'error in address']);
		}
	}

	public function myOrders(Request $request) {
		$token = $request->token;
		$user = SiteUsers::where(['token' => $token])->select('_id')->first();
		$user_id = $user->_id;
		$orderRecords = array();
        if(!empty($user_id)){
			$orders = Orders::where(['user_id' => $user_id])
				->select('_id','user_id', 'firstname', 'lastname', 'email', 'address', 'phone', 'amount', 'shiping_fname', 'shiping_lname', 'shiping_email', 'shipping_address', 'ordernote', 'coupon', 'shippingCharge', 'payment_method', 'created_at', 'updated_at')
				->get();
			foreach($orders as $key => $order){
				$orderDetails = OrderDetail::where(['order_id' => $order->_id])
					->select('orderUniqid','product_id','product_name','product_details','product_image','quantity','color','size','price','discounted_price','order_status','created_at','updated_at')
					->get();
				foreach($orderDetails as $k => $orderDetail){
					$orderRecords[$k]['id'] = $order->_id;
					$orderRecords[$k]['firstname'] = $order->firstname;
					$orderRecords[$k]['lastname'] = $order->lastname;
					$orderRecords[$k]['phone'] = $order->phone;
					$orderRecords[$k]['address'] = $order->address;
					$orderRecords[$k]['payment_method'] = $order->payment_method;
					$orderRecords[$k]['amount'] = $order->amount;
					$orderRecords[$k]['orderDate'] = $this->dateFormate($order->created_at);
					$orderRecords[$k]['orderUniqid'] = $orderDetail->orderUniqid;
					$orderRecords[$k]['product_id'] = $orderDetail->product_id;
					$orderRecords[$k]['product_name'] = $orderDetail->product_name;
					$orderRecords[$k]['product_details'] = $orderDetail->product_details;
					if(isset($orderDetail->product_image)) {
						$orderRecords[$k]['product_image'] = url('/')."/images/products/".$orderDetail->product_image;
					} else {
						$orderRecords[$k]['product_image'] = url('/')."/images/no-image.jpg";
					}
					$orderRecords[$k]['color'] = $orderDetail->color;
					$orderRecords[$k]['size'] = $orderDetail->size;
                    $orderRecords[$k]['quantity'] = $orderDetail->quantity;
					$orderRecords[$k]['price'] = $orderDetail->price;
					$orderRecords[$k]['discounted_price'] = $orderDetail->discounted_price;
					$orderRecords[$k]['order_status'] = $orderDetail->order_status;
					$orderRecords[$k]['createdDate'] = $this->dateFormate($orderDetail->created_at);
					$orderRecords[$k]['updatedDate'] = $this->dateFormate($orderDetail->updated_at);
				}
				$allRecords[$key] = $orderRecords;							
			}	
			$oneDimensionalArray = call_user_func_array('array_merge', $allRecords);			  
			$sendData['orders'] = $oneDimensionalArray;
			$responseData = ['code' => 200,'content' => $sendData];     
		}else{
			$responseData = [
				'code' => 401,
				'content' => ['message' => 'error']
			];
		}
		return response()->json($responseData);
	}

	public function orderDetail($slug) {
        $this->bladeVar['page']['title'] = "Order Detail - Glamour Jewellery";
		$this->bladeVar['site_setting'] = Setting::withTrashed()->orderBy('_id','ASC')->get();

        $data = Session::all();
		if (Session::has('userLoggedIn')){
			$userId = Session::get('userLoggedIn');
			if(!empty($userId)){
				$orderDetails = OrderDetail::where(['order_id' => $slug])->select('*')->get();
				if(!empty($orderDetails)){
					$this->bladeVar['ordersList'] = $orderDetails;

					$order = Orders::where(['_id' => $slug, 'user_id' => $userId])->select('*')->get();
					if(!empty($order['0']->coupon)){
						$coupon = $order['0']->coupon;
						$couponData = DB::select('SELECT * FROM coupons WHERE name = "'.$coupon.'"');
						$this->bladeVar['couponData'] = $couponData;
					}
					$this->bladeVar['order'] = $order;
				}				  			    
			}
		}

        $userCartDetails = $this->userCartDetails();
		$this->bladeVar['userCartProducts'] = $userCartDetails['userCartProducts'];
		$this->bladeVar['userCartData'] = $userCartDetails['userCartData'];
		$this->bladeVar['userCart'] = $userCartDetails['userCart'];
		$this->bladeVar['userWishlistData'] = $userCartDetails['userWishlistData'];
		$this->bladeVar['userWishlist'] = $userCartDetails['userWishlist'];
        return view('frontend/order-detail', ['bladeVar' => $this->bladeVar]);
	}

	public function orderCancel(Request $request) {
		$token = $request->token;
		$orderId = $request->orderId;
		$orderUId = $request->orderUId;
		$productId = $request->productId;
        $data = Session::all();
		if (Session::has('userLoggedIn')){ 
			$token = Session::get('userLoggedIn');
			$user = SiteUsers::where(['_id' => $token])->select('_id')->first();
			$user_id = $user->_id;
			$updateOrderDetail = '';
            if(!empty($orderId) && !empty($orderUId) && !empty($productId)){
				$orderDetail = OrderDetail::where(['_id' => $orderId, 'orderUniqid' => $orderUId, 'product_id' => $productId])->select('product_id','order_status')->get();
				if($orderDetail['0']->product_id == $productId){
					$updatedData['order_status'] = 'Canceled';
					$updateOrderDetail = OrderDetail::where(['_id' => $orderId, 'orderUniqid' => $orderUId, 'product_id' => $productId])->update($updatedData);  
				}
			}
			if(!empty($updateOrderDetail)){
                return response()->json(['responseData'=>'order product cancel successfully']);
			}else{
                return response()->json(['responseData'=>'error in order product cancel']);
			}
        }else{
			return response()->json(['responseData'=>'error in order product cancel']);
		}
		return response()->json($responseData);
	}

	public function userOrdersuccess(){
		$this->bladeVar['page']['title'] = "Order Success - Glamour Jewellery";
		$this->bladeVar['site_setting'] = Setting::withTrashed()->orderBy('_id','ASC')->get();
        
		if(!empty(Session::get('order_id')) && !empty(Session::get('order_userid'))){
			$order_id = Session::get('order_id');
			$order_userid = Session::get('order_userid');

			$payment_id = Session::get('paypal_payment_id');
			$payerID = Session::get('payerID');
			
			$updatedOrderData = array();
			$updatedOrderData['paypal_payment_id'] = $payment_id; 
			$updatedOrderData['paypal_payer_id'] = $payerID; 
			Orders::where(['_id' => $order_id, 'user_id' => $order_userid])->update($updatedOrderData);

			$this->emptyShoppingBag($order_userid);
			Session::forget('order_id');
			Session::forget('order_userid');
			Session::forget('paypal_payment_id');
			Session::forget('payerID');
		}
		$userCartDetails = $this->userCartDetails();
		$this->bladeVar['userCartProducts'] = $userCartDetails['userCartProducts'];
		$this->bladeVar['userCartData'] = $userCartDetails['userCartData'];
		$this->bladeVar['userCart'] = $userCartDetails['userCart'];
		$this->bladeVar['userWishlistData'] = $userCartDetails['userWishlistData'];
		$this->bladeVar['userWishlist'] = $userCartDetails['userWishlist'];
		
        return view('frontend/ordersuccess', ['bladeVar' => $this->bladeVar]);
	}

	public function userOrdercancel(){
		$this->bladeVar['page']['title'] = "Order Cancel - Glamour Jewellery";
		$this->bladeVar['site_setting'] = Setting::withTrashed()->orderBy('_id','ASC')->get();
        $userCartDetails = $this->userCartDetails();
		$this->bladeVar['userCartProducts'] = $userCartDetails['userCartProducts'];
		$this->bladeVar['userCartData'] = $userCartDetails['userCartData'];
		$this->bladeVar['userCart'] = $userCartDetails['userCart'];
		$this->bladeVar['userWishlistData'] = $userCartDetails['userWishlistData'];
		$this->bladeVar['userWishlist'] = $userCartDetails['userWishlist'];

		if(!empty(Session::get('order_id')) && !empty(Session::get('order_userid'))){
			$order_id = Session::get('order_id');
			$order_userid = Session::get('order_userid');
			Orders::where(['_id' => $order_id,'user_id' => $order_userid])->forceDelete();
			OrderDetail::where(['order_id' => $order_id])->forceDelete();
			Session::forget('order_id');
			Session::forget('order_userid');
		}
        return view('frontend/ordercancel', ['bladeVar' => $this->bladeVar]);
	}

	public function dateFormate($date){
		$getDate = date('M d, Y', strtotime($date));
		return $getDate;
	}

	public function paypalPayment(){
        return view('frontend/paywithpaypal');
	}
	
    public function payWithpaypal($orderInfo){
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

		$item_1 = new Item();
		$item_1->setName('Glamour Jewellery') 
			->setCurrency('USD')
			->setQuantity(1)
			->setPrice($orderInfo['amount']);
		
		$item_list = new ItemList();
		$item_list->setItems(array($item_1));
        
        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal($orderInfo['amount']);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your Glamour Jewellery Transaction');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::to('status')) /** Specify return URL **/
            ->setCancelUrl(URL::to('status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/
        try {  
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error', 'Connection timeout');
                return Redirect::to('/ordercancel');
            } else {
                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::to('/ordercancel');
            }
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            /** redirect to paypal **/
			//return Redirect::away($redirect_url);
			return $redirect_url;
        }
        \Session::put('error', 'Unknown error occurred');
        return Redirect::to('/ordercancel');
    }

    public function getPaymentStatus(){
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID **/
        //Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error', 'Payment failed');
            return Redirect::to('/ordercancel');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        if ($result->getState() == 'approved') {
			\Session::put('payerID', Input::get('PayerID'));
            \Session::put('success', 'Payment Success');
            return Redirect::to('/ordersuccess');
        }
        \Session::put('error', 'Payment failed');
        return Redirect::to('/ordercancel');
    }

}