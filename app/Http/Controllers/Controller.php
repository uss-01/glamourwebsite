<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	protected $bladeVar = array(
    	'page' => array(
            'breadcrumb' => array(
                'Home' => '/'
            ),
            'title' => 'Dashboard',
            'bodyClass' => '',
            'activeNav' => array(
                'nav' => '',
                'sub' => ''
            ),
        ),
    );

    protected $paginate_total = 20;
	
}
