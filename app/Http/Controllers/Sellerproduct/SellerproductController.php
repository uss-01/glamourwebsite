<?php

namespace App\Http\Controllers\Sellerproduct;

use Auth;
use DB;
use App\User;
use App\Sellerproduct\Sellerproduct;
use App\Shop\Shop;
use App\Sellerproduct\ProductPics;
use App\Masters\ProductCategory;
use App\Masters\ProductSubcategory;
use App\Masters\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use Validator;
use Illuminate\Support\Facades\File;
use Image;

class SellerproductController extends Controller
{
     protected $sortable = [
        'id' => '_id',
        'name' => 'name',
        'shop'=>'s.name',
        'product_category' => 'pc.name',
        'product_subcategory' => 'psc.name',
    ];
    protected $validation_rules = [
        'name' => 'required|max:100',
        'brand' => 'required|integer',
        'shop' => 'required|integer',
        'product_category' => 'required|integer',  
    ];
    protected $pageTitle = 'Products';
    protected $activeNav = [
        'nav' => 'product',
        'sub' => 'product'
    ];
    protected $breadcrumb_master = 'Product';
    protected $view_master = 'products.';
    protected $route_master = 'products.';

    public function __construct()
    {
         $this->bladeVar['page']['title'] = $this->pageTitle;
         $this->bladeVar['page']['activeNav'] = $this->activeNav;
         $this->bladeVar['page']['breadcrumb'] = ['Home' => '/', 'Products' => '/seller/products'];
         $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = route($this->route_master.'index');
         $this->cache();
    }

    /**
     * Refresh cache and returns puts it in array
     */
    protected function cache() 
    {
        $time = Carbon::now()->addHours(1);
         $this->bladeVar['shops'] = Cache::remember('shops', $time, function () {
            return Shop::orderBy('name', 'asc')->get();
        });
         $this->bladeVar['product_categories'] = Cache::remember('product_categories', $time, function () {
            return ProductCategory::where(['type' => 'Product'])->orderBy('name', 'asc')->get();
        });
        
        $this->bladeVar['brands'] = Cache::remember('brands', $time, function () {
            return Brand::orderBy('name', 'asc')->get();
        }); 
       
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
        $this->bladeVar['page']['breadcrumb'][$this->breadcrumb_master] = '';
        $this->bladeVar['results'] = Sellerproduct::withTrashed()
            ->where('products.seller_id', Auth::id())
            ->join('shops as s', 's._id', '=', 'products.shop_id')
            ->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
            ->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
            ->select('products.*')
            ->with('shop')
            ->with('product_category')
            ->with('product_subcategory')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
			->groupBy('products._id')
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Sellerproduct::withTrashed()
            ->where('products.seller_id', Auth::id())
            ->join('shops as s', 's._id', '=', 'products.shop_id')
            ->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
            ->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
            ->select('products.*')
            ->with('shop')
            ->with('product_category')
            ->with('product_subcategory')->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    public function allShops(Request $request){
        $this->bladeVar['page']['breadcrumb'] = ['Home' => '/', 'Shops' => '/seller/shops'];
        $sellerId = Auth::id();
        $getSellerRecord = User::where(['id' => $sellerId])->get();	
        $userShops = json_decode($getSellerRecord[0]->shop_name); 
        $shopIDArray = array();
        $allData = array();
        foreach($userShops as $key => $shop){
            $shopIDArray[] = $shop->id;
            $shopResults = Shop::withTrashed()
                ->where('shops._id', $shop->id)
                ->select('shops.*')
                ->get();
            $allData[$key]['shopId'] = $shopResults[0]->_id;
            $allData[$key]['shopName'] = $shopResults[0]->name;
        }
        $this->bladeVar['shopResults'] = $allData;
        $this->bladeVar['results'] = Sellerproduct::withTrashed()
            ->where('products.seller_id', Auth::id())
            ->whereIn('products.shop_id', $shopIDArray)
            ->join('shops as s', 's._id', '=', 'products.shop_id')
            ->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
            ->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
            ->select('products.*')
            ->with('shop')
            ->with('product_category')
            ->with('product_subcategory')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
			->groupBy('products._id')
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Sellerproduct::withTrashed()
            ->where('products.seller_id', Auth::id())
            ->whereIn('products.shop_id', $shopIDArray)
            ->join('shops as s', 's._id', '=', 'products.shop_id')
            ->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
            ->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
            ->select('products.*')
            ->with('shop')
            ->with('product_category')
            ->with('product_subcategory')->count();
        return view($this->view_master.'shops', ['bladeVar' => $this->bladeVar]);
    }

    public function shop(Request $request){
        if (!$request->id) {
            return redirect(route($this->route_master.'shops'));
        }
        $this->bladeVar['page']['breadcrumb'] = ['Home' => '/', 'Shops' => '/seller/shops'];
        $sellerId = Auth::id();
        $getSellerRecord = User::where(['id' => $sellerId])->get();	
        $userShops = json_decode($getSellerRecord[0]->shop_name); 
        $shopIDArray = array();
        $allData = array();
        foreach($userShops as $key => $shop){
            $shopIDArray[] = $shop->id;
            $shopResults = Shop::withTrashed()
                ->where('shops._id', $shop->id)
                ->select('shops.*')
                ->get();
            $allData[$key]['shopId'] = $shopResults[0]->_id;
            $allData[$key]['shopName'] = $shopResults[0]->name;
        }
        $this->bladeVar['shopResults'] = $allData;
        if(!empty($request->id)){
            $getShopId = $request->id;
        }else{
            $getShopId = $shopIDArray;
        } 
        $this->bladeVar['getid'] = $getShopId;
        $this->bladeVar['results'] = Sellerproduct::withTrashed()
            ->where('products.seller_id', Auth::id())
            ->where('products.shop_id',$getShopId)
            ->join('shops as s', 's._id', '=', 'products.shop_id')
            ->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
            ->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
            ->select('products.*')
            ->with('shop')
            ->with('product_category')
            ->with('product_subcategory')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
			->groupBy('products._id')
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Sellerproduct::withTrashed()
            ->where('products.seller_id', Auth::id())
            ->where('products.shop_id', $getShopId)
            ->join('shops as s', 's._id', '=', 'products.shop_id')
            ->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
            ->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
            ->select('products.*')
            ->with('shop')
            ->with('product_category')
            ->with('product_subcategory')->count();
        return view($this->view_master.'shops', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request){
        if (!$request->q) {
            return redirect(route($this->route_master.'index'));
        }
        $this->bladeVar['page']['breadcrumb'] = ['Home' => '/'];
        $this->bladeVar['page']['breadcrumb']['Search'] = '';
        $this->bladeVar['results'] = Sellerproduct::withTrashed()
            ->where('products.name', 'LIKE', '%'.$request->q.'%')
            ->where('products.seller_id', Auth::id())
            ->join('shops as s', 's._id', '=', 'products.shop_id')
            ->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
            ->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
            ->select('products.*')
            ->with('shop')
            ->with('product_category')
            ->with('product_subcategory')
            ->orderBy($this->sortable[$request->input('sort', 'id')], $request->input('dir', 'ASC'))
			->groupBy('products._id')
            ->paginate($this->paginate_total);
        $this->bladeVar['total_count'] = Sellerproduct::withTrashed()
            ->where('products.name', 'LIKE', '%'.$request->q.'%')
            ->where('products.seller_id', Auth::id())
			->join('shops as s', 's._id', '=', 'products.shop_id')
            ->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
            ->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
			->with('shop')
            ->with('product_category')
            ->with('product_subcategory')
            ->count();
        return view($this->view_master.'index', ['bladeVar' => $this->bladeVar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request){
        if ($request->ajax()) {
            $sellerId = Auth::id();
            $getSellerRecord = User::where(['id' => $sellerId])->get();	
            $userShops = json_decode($getSellerRecord[0]->shop_name); 
            $allData = array();
            foreach($userShops as $key => $shop){
                $shopResults = Shop::withTrashed()
                    ->where('shops._id', $shop->id)
                    ->select('shops.*')
                    ->get();
                $allData[$key]['shopId'] = $shopResults[0]->_id;
                $allData[$key]['shopName'] = $shopResults[0]->name;
            }
            $this->bladeVar['shopResults'] = $allData;

            return view($this->view_master.'create', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){    
        $input = $request->all();
        $this->validation_rules['image'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'; 
        if($request->product_category==-1){
          $this->validation_rules['other_category'] = 'required';   
        }
        if($request->product_subcategory==-1){
          $this->validation_rules['other_subcategory'] = 'required';   
        }
        $validator = Validator::make($request->all(),$this->validation_rules);
          if ($validator->passes()) {
            // Getting Attributes for Products
          $attrNum = count($request->attrName);
          $attributes = [];
          for($i=0;$i<$attrNum;$i++){
           $attributes[$i]['name'] = $request->attrName[$i];
           $attributes[$i]['value'] = $request->attrVal[$i];
          }
        $attributes = json_encode($attributes);
        if($request->image){
         list($width, $height) = getimagesize($request->image);
          if($width != 990 && $height != 990){
            $error_msg=[
             'image' => 'Size Should be 990X990',
            ];
            return response()->json(['error'=>$error_msg]);
          }
        }

        $image_name = str_replace(' ', '-',strtolower($request->name));
        $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/products'), $input['image']);
        
        $img_thumb = Image::make(public_path('images/products').'/'.$input['image']);
        $img_thumb->resize(470, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('images/products/thumbnails').'/'.$input['image']);

        $img_thumb->resize(460, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('images/products/thumbnails').'/460x460_'.$input['image']);

        $img_thumb->resize(320, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('images/products/thumbnails').'/320x320_'.$input['image']);
        if($request->product_subcategory == NULL){
           $request->product_subcategory = 0; 
        }
        // Other Product Category Insert
        if($request->product_category==-1){
         $product_category = ProductCategory::updateOrCreate(
            ['name' => $request->other_category],
            ['created_by'=>Auth::id()]
        );
         $request->product_category = $product_category->_id;
        }
        // Other Product Subcategory Insert
        if($request->product_subcategory==-1){
        $product_subcategory = ProductSubcategory::updateOrCreate(
            ['name' => $request->other_subcategory,
            'product_category_id' => $request->product_category],
            ['created_by'=>Auth::id()]
        );
         $request->product_subcategory = $product_subcategory->_id;
        }
        if($request->featured !=1){
            $request->featured = 0;
        }
        if($request->add_to_cart !=1){
            $request->add_to_cart = 0;
        }
        if(!empty($request->discounted_price)){
            $disPrice = $request->discounted_price;
        }else{
            $disPrice = '0.00';
        }
        if(!empty($request->product_quantity)){
            $proQuantity = $request->product_quantity;
        }else{
            $proQuantity = '0';
        }
        $product = Sellerproduct::create([
            'name' => $request->name,
            'price' => $request->price,
            'shop_id' => $request->shop,
            'brand_id' => $request->brand,
            'product_category_id' => $request->product_category,
            'product_subcategory_id' => $request->product_subcategory,
            'featured' => $request->featured,
            'add_to_cart' => $request->add_to_cart,
            'product_quantity' => $proQuantity,
            'discounted_price' => $disPrice,
            'product_status' => 'Draft',
            'attribute'=>$attributes,
            'image'=> $input['image'],
            'details'=>$request->details,
            'seller_id'=>Auth::id(),
        ]);
        return response()->json([
                'success' => '<strong>Success!</strong> data saved for product <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $product->_id).'" class="btn btn-sm btn-info" title="View this product" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
            ]);
     }
      return response()->json(['error'=>$validator->errors()->getMessages()]);
    }

     public function product_subcategory(Request $request){
        $this->validate($request, [
            'product_category' => 'required|integer'
        ]);
         $product_subcategories = ProductSubcategory::where('product_subcategories.product_category_id', '=', $request->product_category)
            ->select('_id', 'name')
            ->orderBy($this->sortable[$request->input('sort', 'name')], $request->input('dir', 'ASC'))->get();
        return response()->json($product_subcategories);
    }

    // Display Brands
     public function shop_brands(Request $request){
        $this->validate($request, [
            'shop_id' => 'required|integer'
        ]);
         $shop_brands = Brand::withTrashed()
            ->join('shop_brands as sb', 'sb.brand_id', '=', 'master_brand.brand_id')
            ->where('sb.shop_id', '=', $request->shop_id)
            ->select('master_brand.brand_id as _id', 'master_brand.name')
            ->orderBy($this->sortable[$request->input('sort', 'name')], $request->input('dir', 'ASC'))->get();
        return response()->json($shop_brands);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id){
        if ($request->ajax()) {
            $this->bladeVar['result'] = Sellerproduct::withTrashed()->find($id);
            return view($this->view_master.'show', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id){
        if ($request->ajax()) {
            $sellerId = Auth::id();
            $getSellerRecord = User::where(['id' => $sellerId])->get();	
            $userShops = json_decode($getSellerRecord[0]->shop_name); 
            $allData = array();
            foreach($userShops as $key => $shop){
                $shopResults = Shop::withTrashed()
                    ->where('shops._id', $shop->id)
                    ->select('shops.*')
                    ->get();
                $allData[$key]['shopId'] = $shopResults[0]->_id;
                $allData[$key]['shopName'] = $shopResults[0]->name;
            }
            $this->bladeVar['shopResults'] = $allData;

            $product = Sellerproduct::find($id);
            $this->bladeVar['result'] = $product;
            return view($this->view_master.'edit', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $input = $request->all();
        if($request->product_category==-1){
          $this->validation_rules['other_category'] = 'required';   
        }
        if($request->product_subcategory==-1){
          $this->validation_rules['other_subcategory'] = 'required';   
        }
        // Getting Attributes
        $attrNum = count($request->attrName);
          $attributes = [];
          for($i=0;$i<$attrNum;$i++){
           $attributes[$i]['name'] = $request->attrName[$i];
           $attributes[$i]['value'] = $request->attrVal[$i];
          }
           $attributes = json_encode($attributes);
        if (isset($request->image)) {
        $this->validation_rules['image'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'; 
        $validator = Validator::make($request->all(),$this->validation_rules);
        if($validator->passes()){       
        if($request->image){
         list($width, $height) = getimagesize($request->image);
          if($width != 990 && $height != 990){
            $error_msg=[
             'image' => 'Size Should be 990X990',
            ];
            return response()->json(['error'=>$error_msg]);
          }
        }
        $image_name = str_replace(' ', '-',strtolower($request->name));
        $input['image'] = time().'_'.$image_name.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/products'), $input['image']);
        $img_thumb = Image::make(public_path('images/products').'/'.$input['image']);
         $img_thumb->resize(470, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('images/products/thumbnails').'/'.$input['image']);

        $img_thumb->resize(460, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('images/products/thumbnails').'/460x460_'.$input['image']);

        $img_thumb->resize(320, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('images/products/thumbnails').'/320x320_'.$input['image']);

         $product = Sellerproduct::find($id);
         File::delete(public_path().'/images/products/'.$product->image);
         File::delete(public_path().'/images/products/thumbnails/'.$product->image);
         File::delete(public_path().'/images/products/thumbnails/460x460_'.$product->image);
         File::delete(public_path().'/images/products/thumbnails/320x320_'.$product->image);

        // Other Product Category Insert
        if($request->product_category==-1){
         $product_category = ProductCategory::updateOrCreate(
            ['name' => $request->other_category],
            ['created_by'=>Auth::id()]
        );
         $request->product_category = $product_category->_id;
        }
       // Other Product Subcategory Insert
         if($request->product_subcategory==-1){
        $product_subcategory = ProductSubcategory::create(
            ['name' => $request->other_subcategory,
            'product_category_id' => $request->product_category],
            ['created_by'=>Auth::id()]
        );
         $request->product_subcategory = $product_subcategory->_id;
        }
         if($request->product_subcategory == NULL){
           $request->product_subcategory = 0; 
        }
         if($request->featured !=1){
            $request->featured = 0;
        }
         if($request->add_to_cart !=1){
            $request->add_to_cart = 0;
        }

        if(!empty($request->discounted_price)){
            $disPrice = $request->discounted_price;
        }else{
            $disPrice = '0.00';
        }
        if(!empty($request->product_quantity)){
            $proQuantity = $request->product_quantity;
        }else{
            $proQuantity = '0';
        }
        $product->update([
            'name' => $request->name,
            'price' => $request->price,
            'shop_id' => $request->shop,
            'brand_id' => $request->brand,
            'product_category_id' => $request->product_category,
            'product_subcategory_id' => $request->product_subcategory,
            'featured' => $request->featured,
            'add_to_cart' => $request->add_to_cart,
            'product_quantity' => $proQuantity,
            'discounted_price' => $disPrice,
            'attribute'=>$attributes,
            'image'=> $input['image'],
            'details'=>$request->details,
            'seller_id'=>Auth::id(),
        ]);
         return response()->json([
            'success' => '<strong>Success!</strong> Data updated for product <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $product->_id).'" class="btn btn-sm btn-info" title="View this product" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);
     } 
       return response()->json(['error'=>$validator->errors()->getMessages()]);
     }else{
         $validator = Validator::make($request->all(),$this->validation_rules);
         if($validator->passes()){
           // Other Product Category Insert
        if($request->product_category==-1){
         $product_category = ProductCategory::updateOrCreate([
            ['name' => $request->other_category],
             'created_by'=>Auth::id(),
        ]);
         $request->product_category = $product_category->_id;
        }
          // Other Product Subcategory Insert
         if($request->product_subcategory==-1){
        $product_subcategory = ProductSubcategory::updateOrCreate([
            ['name' => $request->other_subcategory,
            'product_category_id' => $request->product_category],
            'created_by'=>Auth::id(),
        ]);
         $request->product_subcategory = $product_subcategory->_id;
        }
        if($request->product_subcategory == NULL){
           $request->product_subcategory = 0; 
        } 
        if($request->featured !=1){
            $request->featured = 0;
        }
         if($request->add_to_cart !=1){
            $request->add_to_cart = 0;
        }
       
        if(!empty($request->discounted_price)){
            $disPrice = $request->discounted_price;
        }else{
            $disPrice = '0.00';
        }
        if(!empty($request->product_quantity)){
            $proQuantity = $request->product_quantity;
        }else{
            $proQuantity = '0';
        }
        $product = Sellerproduct::find($id);
        $product->update([
            'name' => $request->name,
            'price' => $request->price,
            'shop_id' => $request->shop,
            'brand_id' => $request->brand,
            'product_category_id' => $request->product_category,
            'product_subcategory_id' => $request->product_subcategory,
            'featured' => $request->featured,
            'add_to_cart' => $request->add_to_cart,
            'product_quantity' => $proQuantity,
            'discounted_price' => $disPrice,
            'attribute'=>$attributes,
            'details'=>$request->details,
            'seller_id'=>Auth::id(),
        ]);
         return response()->json([
            'success' => '<strong>Success!</strong> Data updated for product <strong>'.$request->name.'</strong> <a href="'.route($this->route_master.'show', $product->_id).'" class="btn btn-sm btn-info" title="View this product" data-toggle="modal" data-target="#viewModal">Click here to view it</a>'
        ]);
    }
     return response()->json(['error'=>$validator->errors()->getMessages()]);
     }
        
    }

    // For Product Pics
    public function pics(Request $request, $id){
        if ($request->ajax()) {
            $this->bladeVar['result'] = Sellerproduct::withTrashed()->find($id);
            $this->bladeVar['productPics'] = ProductPics::where(['product_pics.product_id' => $id])
                ->select('product_pics._id','product_pics.image')
                ->get();
            return view($this->view_master.'pics', ['bladeVar' => $this->bladeVar]);
        } else {
            return redirect(route($this->route_master.'index'));
        }
    }

    public function savePics(Request $request){
        $input = $request->all();
        $rules = [];
        $photo_cnt = count($request->image);
        foreach(range(0, ($photo_cnt-1)) as $index) {
            $rules['image.' . $index] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048';
        }
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            $cnt = 1;
        foreach ($request->image as $photo) {
         list($width, $height) = getimagesize($photo);
            $image_fullname = time().'_'.$cnt.'.'.$photo->getClientOriginalExtension();
            $photo->move(public_path('images/products'), $image_fullname);
            $photo = ProductPics::create([
                'product_id' => $request->product,
                'image'=> $image_fullname,
                'created_by'=>Auth::id(),
            ]);
            $cnt++;
        }
        return response()->json([
                'success' => '<strong>Success!</strong> data saved for photo <strong>'.$request->title.'</strong><a href="'.route($this->route_master.'show', $photo->_id).'" class="btn btn-sm btn-info" title="View this photo" data-toggle="modal" data-target="#viewModal"> Click here to view it</a>'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        Sellerproduct::find($id)->delete();
        return response()->json([
            'success' => '<strong>Success!</strong> Product has been deleted temporarily!'
        ]);
    }

    public function deletePic($id){
        $pics=ProductPics::find($id);
        File::delete(public_path().'/images/products/'.$pics->image);
        $pics->delete();
        return response()->json([
            'success' => '<strong>Success!</strong> Product Pic has been deleted permanently!'
        ]);
    }

    /**
     * Mark as deleted the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id){
        $product = Sellerproduct::withTrashed()->find($id);
        File::delete(public_path().'/images/products/'.$product->image);
        File::delete(public_path().'/images/shops/thumbnails'.$product->image);
        $product->forceDelete();
        return response()->json([
            'success' => '<strong>Success!</strong> Product has been deleted permanently!'
        ]);
    }

    /**
     * Restores deleted marked resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id){
        Sellerproduct::withTrashed()->find($id)->restore();
        return response()->json([
            'success' => '<strong>Success!</strong> Product has been restored!'
        ]);
    }
}
