<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
	
    protected $table = 'events';
    protected $primaryKey = '_id';
   use SoftDeletes;
    protected $fillable = [
        'title','sub_title','event_time','event_location','details','image','start_date','end_date','created_by','updated_by' 
    ];
	
	public function eventCount()
    {
        return $this->hasMany('App\UserJoinEvent', 'event_id', '_id');  
    }
}
