<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Leasing extends Model{
	use SoftDeletes;
	protected $table = 'leasing';
	protected $primaryKey = 'id';
	protected $fillable = [
        'tenant_id','shops','rent_amount','payment_status','payment_method','pending_amount','created_by','updated_by' 
    ];
}
