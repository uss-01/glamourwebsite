<?php

namespace App\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Color extends Model
{
    use SoftDeletes;
    
    protected $table = 'product_color';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name','slug','color_code','created_by','updated_by'
    ];
}
