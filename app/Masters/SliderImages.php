<?php

namespace App\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SliderImages extends Model
{
	use SoftDeletes;
    protected $table = 'slider_images';
    protected $primaryKey = 'id';
    protected $fillable = [
        'title','slug','shortDescription','slidePosition','btntext','redirect_url','created_by','updated_by','image' 
    ];
}
