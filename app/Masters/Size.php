<?php

namespace App\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Size extends Model
{
    use SoftDeletes;
    
    protected $table = 'product_size';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name','slug','created_by','updated_by'
    ];
}
