<?php

namespace App\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends Model
{   
	use SoftDeletes;
    
    protected $table = 'master_store';
    protected $primaryKey = 'store_id';
    protected $fillable = [
        'name','created_by','updated_by'
    ];
}
