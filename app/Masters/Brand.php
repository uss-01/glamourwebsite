<?php

namespace App\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    use SoftDeletes;
    
    protected $table = 'product_brand';
    protected $primaryKey = '_id';
    protected $fillable = [
        'name','slug','image','created_by','updated_by' 
    ];
}
