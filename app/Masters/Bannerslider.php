<?php

namespace App\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bannerslider extends Model
{
	use SoftDeletes;
    protected $table = 'banner_slider';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name','slug','redirect_url','image','created_by','updated_by' 
    ];
}
