<?php

namespace App\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSubCategory extends Model
{
    use SoftDeletes;
    
    protected $table = 'product_subcategories';
    protected $primaryKey = '_id';
    protected $fillable = [
        'product_category_id','slug','type','name','created_by','updated_by'
    ];

    public function product_category()
    {
    	return $this->belongsTo('App\Masters\ProductCategory', 'product_category_id', '_id');	
    }
}
