<?php

namespace App\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Experience extends Model
{
   use SoftDeletes;
    
    protected $table = 'experiences';
    protected $primaryKey = '_id';
    protected $fillable = [
        'name','created_by','updated_by'
    ];
}
