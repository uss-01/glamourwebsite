<?php

namespace App\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BannerImages extends Model
{
	use SoftDeletes;
    protected $table = 'banner_images';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name','slug','bannerPosition','shortDescription','category','subcategory','image','btntext','created_by','updated_by' 
    ];

    public function product_category()
    {
    	return $this->belongsTo('App\Masters\ProductCategory', 'category', '_id');	
    }
    
    public function product_subcategory()
    {
    	return $this->belongsTo('App\Masters\ProductSubcategory', 'subcategory', '_id');	
    }
}
