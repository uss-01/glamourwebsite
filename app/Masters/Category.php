<?php

namespace App\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    
    protected $table = 'master_category';
    protected $primaryKey = 'category_id';
    protected $fillable = [
        'name','created_by','updated_by'
    ];
}
