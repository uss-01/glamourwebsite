<?php

namespace App\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogCategory extends Model
{
    use SoftDeletes;
    
    protected $table = 'blog_categories';
    protected $primaryKey = '_id';
    protected $fillable = [
        'name','slug','created_by','updated_by'
    ];
}
