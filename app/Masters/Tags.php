<?php

namespace App\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tags extends Model
{
    use SoftDeletes;
    
    protected $table = 'blog_tags';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name','slug','created_by','updated_by'
    ];
}
