<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SiteUsersEntryExitTime extends Model
{
    use SoftDeletes;
    
    protected $table = 'entry_exit_time';
    protected $primaryKey = 'sno';
	protected $fillable = ['site_user_id', 'entry', 'exit', 'created_at', 'updated_at'];
}
