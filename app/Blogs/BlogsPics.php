<?php

namespace App\Blogs;

use Illuminate\Database\Eloquent\Model;

class BlogsPics extends Model
{  
    
    protected $table = 'blog_pics';
    protected $primaryKey = '_id';
    protected $fillable = [
        'blog_id','image','created_by','updated_by' 
    ];
}
