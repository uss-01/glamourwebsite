<?php

namespace App\Blogs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blogs extends Model
{
    use SoftDeletes;

    protected $table = 'blogs';
    protected $primaryKey = '_id';
    protected $fillable = [
        'name','slug','details','image','blog_category_id','blog_tag_id','blog_status','blog_audio','blog_video','blog_image_slide','audio_url','video_url','created_by','updated_by'
    ];
    public function blog_category()
    {
        return $this->belongsTo('App\Masters\BlogCategory', 'blog_category_id', '_id');  
    }

    public function blog_tag()
    {
        return $this->belongsTo('App\Masters\Tags', 'blog_tag_id', 'id');  
    }

    public function blog_pics()
    {
        return $this->belongsTo('App\Blogs\BlogsPics', '_id', 'blog_id');  
    }
}
