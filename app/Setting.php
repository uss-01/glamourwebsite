<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    use SoftDeletes;  
	
    protected $table = 'settings';
    protected $primaryKey = '_id';
    protected $fillable = [
        'title','message','address','email','phone','facebook_url','twitter_url','instagram_url','youtube_url','linkedin_url','pinterest_url','shipping','image','created_by','updated_by' 
    ];
}
