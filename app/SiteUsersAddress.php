<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SiteUsersAddress extends Model
{
	use SoftDeletes;
    
    protected $table = 'site_users_address';
    protected $primaryKey = '_id';
	protected $fillable = ['user_id', 'name','address','phone','address_type','flag','created_at','updated_at','deleted_at'];
}
