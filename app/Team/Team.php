<?php

namespace App\Team;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
	use SoftDeletes;
    protected $table = 'team';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name','slug','designation','facebook_url','twitter_url','linkedin_url','google_url','image','created_by','updated_by' 
    ];
}
