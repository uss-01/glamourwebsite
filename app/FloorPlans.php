<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FloorPlans extends Model
{
	use SoftDeletes;
     protected $table = 'floor_plans';
         protected $fillable = [
        'id','title','document','created_by','updated_by'
    ];

}
