<?php

namespace App\Orders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model
{
    use SoftDeletes;
    
    protected $table = 'orders';
    protected $primaryKey = '_id';
    protected $fillable = [
        'user_id', 'firstname', 'lastname', 'email', 'address', 'phone', 'amount', 'shiping_fname', 'shiping_lname', 'shiping_email', 'shipping_address', 'ordernote', 'coupon', 'shippingCharge', 'payment_method', 'created_at', 'updated_at'
    ];
}
