<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  PrivilegeCustomerOffer extends Model
{
     use SoftDeletes;
	
    protected $table = 'privilege_customer_offers';
    protected $primaryKey = '_id';
    protected $fillable = [
        'title','sub_title','shop_id','membership_type','number_of_visits','number_of_hours','details','image','start_date','end_date','created_by','updated_by' 
    ];

     public function shop()
    {
        return $this->belongsTo('App\Shop\Shop', 'shop_id', '_id');  
    }
}
