<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cms extends Model
{
    use SoftDeletes;
	
    protected $table = 'cms';
    protected $primaryKey = '_id';
    protected $fillable = [
        'name','slug','title','image','content','created_by','updated_by' 
    ];
}
