<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tenant extends Model
{
	use SoftDeletes;
	protected $table = 'tenants';
	protected $primaryKey = 'id';
	    protected $fillable = [
        'uid','name','email','phone','address','details','shop_name','photo_doc','start_date','end_date','created_by','updated_by' 
    ];
}
