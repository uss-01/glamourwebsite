<?php
use App\Product\Product;
use App\Masters\Brand;
use App\Shop\Shop;

function getTenantUID($tenant_id){
	return 'TM-'.date('dmy').''.$tenant_id;
}

function PushNotificationIos($deviceToken, $pushData){
	//print_r($pushData);
	//$tHost = 'gateway.sandbox.push.apple.com';
	$tHost = 'gateway.push.apple.com';
	$tPort = 2195;
	//$tCert = config_path().'/pushcert.pem';
	$tCert = config_path().'/pushNotification.pem';
	// $tPassphrase = 'apple';
	$tPassphrase = 'password';

	$tToken = $deviceToken;
	if(!empty($pushData['subject']) && !empty($pushData['message'])){
		$sendMSG = array('title' => $pushData['subject'], 'body' => $pushData['message']);
	}else{
		$sendMSG = array('body' => $pushData['message']);
	}
	
	$tPayload = 'iOS Message Handled by LiveCode';
	$tBody['aps'] = array (
		'alert' => $sendMSG,
		'badge' => 1,
		'sound' => 'default',
		'mutable-content'=>1,
		'redirect_to'=>$pushData['redirect_to'],
		'redirect_id'=>$pushData['redirect_id'],
	);
	$tBody ['attachment-url'] = $pushData['image'];
	$tBody ['payload'] = $tPayload;
	$tBody = json_encode ($tBody);
	$tContext = stream_context_create ();
	stream_context_set_option ($tContext, 'ssl', 'local_cert', $tCert);
	stream_context_set_option ($tContext, 'ssl', 'passphrase', $tPassphrase);
	$tSocket = stream_socket_client ('ssl://'.$tHost.':'.$tPort, $error, $errstr, 30, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $tContext);
	if (!$tSocket){
		exit ("APNS Connection Failed: $error $errstr" . PHP_EOL);
	}
	// Build the Binary Notification.
	$tMsg = chr (0) . chr (0) . chr (32) . pack ('H*', trim($tToken)) . pack ('n', strlen ($tBody)) . $tBody;
	// Send the Notification to the Server.
	$tResult = fwrite ($tSocket, $tMsg, strlen ($tMsg));
	if ($tResult){
		//echo 'Delivered Message to iOs' . PHP_EOL;
		fclose ($tSocket);
		sleep(1);
		return 'success';
	}
	else{
		//echo 'Could not Deliver Message to APNS' . PHP_EOL;
		fclose ($tSocket);
		sleep(1);
		return 'failed';
	}


}

/* function getMemberShip($points){
	//return $points;
	if($points>=0 && $points<100){
		return 'Normal';
	}
	if($points>=100 && $points<200){
		return 'Broune';
	}elseif ($points>=200 && $points<300) {
		return 'Silver';
	}else{
		return 'Gold';
	}
} */


 function getMemberShip($points){
	if($points > 0 && $points <= 500){
		return 'Bronze';
	}else if($points > 500 && $points <= 1500){
		return 'Silver';
	}else if($points > 1500 && $points <= 2000){
		return 'Gold';
	}else if($points > 2000){
		return 'Platinum';
	}else{
		return 'Bronze';
	}
}


function productsYouMayLike($count=9, $curr_product_id='', $user_id='') {
		//echo $count;echo ' == '; echo $curr_product_id; echo ' == '; echo $user_id;die(' www ');
	$where ['products.deleted_at']= null;
	$size ="";

	/*$allRecords = Product::where($where)
	->select('_id','name','image','price')
	->whereNotIn( '_id', [$curr_product_id])
	->inRandomOrder()
	->take($count)->skip(0)
	->get();*/
	
	
	$allRecords = Product::where($where)
	->join('shops as s', 's._id', '=', 'products.shop_id')
	->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
	->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
	->select('products._id','products.name','products.image','products.price','products.discounted_price')
	->whereNotIn( 'products._id', [$curr_product_id])
	->inRandomOrder()
	->groupBy('products._id')
	->take($count)->skip(0)
	->get();
	
	//->toSql();
	//->paginate($count);

	$getRecords = [];
	$i=0;
	foreach($allRecords as $records) {
		if($records->image) {
			$getRecords[$i]['image'] = url('/')."/images/products/thumbnails/".$size."".$records->image;
		} else {
			$getRecords[$i]['image'] = url('/')."/images/no-image.jpg";
		}
		$getRecords[$i]['id'] = $records->_id;
		$getRecords[$i]['name'] = $records->name;
		$getRecords[$i]['price'] = $records->price;
		$getRecords[$i]['discountedPrice'] = $records->discounted_price;
		$getRecords[$i]['type'] = 'product';
		$i++;
	}
	return $getRecords;

}

function productSearch($q){

	/*$sql = DB::table('')
	->where('name', 'like', '%'.$q.'%')
	->orWhere('details', 'like', '%'.$q.'%');
	$productObj = $sql->get();
        echo  getSQL($sql);die();*/
		
	$sql = DB::table('products')
	->join('shops as s', 's._id', '=', 'products.shop_id')
	->join('product_categories as pc', 'pc._id', '=', 'products.product_category_id')
	->leftJoin('product_subcategories as psc', 'psc._id', '=', 'products.product_subcategory_id')
	->select('products._id','products.name','products.image','products.price','products.discounted_price')
	->where(function ($query) use ($q){
		$query->where('products.name', 'like', '%'.$q.'%')
			->orWhere('products.details', 'like', '%'.$q.'%');
	})
	->groupBy('products._id');
	$productObj = $sql->get();
		// echo getSQL($sql);die();		
		
	$productArr = [];
	$i=0;
	foreach($productObj as $records) {
		if($records->image) {
			$productArr[$i]['image'] = url('/')."/images/products/thumbnails/".$records->image;
		} else{
			$productArr[$i]['image'] = url('/')."/images/no-image.jpg";
		}
		$productArr[$i]['product_id'] = $records->_id;
		$productArr[$i]['name'] = $records->name;
		$productArr[$i]['price'] = $records->price;
		$productArr[$i]['discountedPrice'] = $records->discounted_price;
		$i++;
	}
	return $productArr;
}
// category_id (1 = Shop; 2 = Dinning; 3 = Entertainment)
function brandSearch($q){
		// $brandSql = Brand::whereIn( 'shops.category_id',array(1,2,3)) // category_id (1 = Shop; 2 = Dinning; 3 = Entertainment)
		$brandSql = Brand::whereIn( 'shops.category_id',array(1))
		->join('shop_brands as sb','sb.brand_id','=','master_brand.brand_id')
		->join('shops','shops._id','=','sb.shop_id')
		->join('master_store as ms','ms.store_id','=','shops.store_id')
		->select('master_brand.brand_id as id','master_brand.name','master_brand.image','ms.name as location')
		->where(function ($query) use ($q){
			$query->where('master_brand.name', 'like', '%'.$q.'%')
				->orWhere('master_brand.description', 'like', '%'.$q.'%');
		})
		->orderBy('name','asc');
		$brandResults = $brandSql->get();
			//echo getSQL($brandSql);die('jmd');

		$brands = [];
		$i=0;
		$brandName=array();
		foreach($brandResults as $records) {
			if(in_array($records->name, $brandName)){ continue;}
			$brands[$i]['id'] = $records->id;
			$brands[$i]['name'] = $records->name;
			$brands[$i]['location'] = $records->location;
			if($records->image != 'NULL') {
				$brands[$i]['image'] = url('/')."/images/brands/".$records->image;
			} else {
				$brands[$i]['image'] = url('/')."/images/no-image.jpg";
			}


			$brandName[]=$records->name;
			$i++;
		}
		return $brands;
	}
// category_id (1 = Shop; 2 = Dinning; 3 = Entertainment)
//-------movies are also shops with category 3 and brand_id 25 -----
	function movieSearch($q){
		$brand_id=25;
		$movieSql = Shop::where(['shops.category_id' => 3])

		->select('shops._id as shop_id','shops.name','ms.name as store_location',
			'shops.logo as shop_image','shops.upcoming','mb.name as entertainment_type',
			'md.subtitle','md.duration','md.description as details',
			'md.2d_showtime as showtime_2d','md.3d_showtime','md.cast','md._id as movie_id'
		)

		->join('shop_brands as sb','sb.shop_id','=','shops._id')
		->join('master_brand as mb','mb.brand_id','=','sb.brand_id')
		->join('master_store as ms','ms.store_id','=','shops.store_id')
		->join('movie_detail as md','md.shop_id','=','shops._id')

		->where('sb.brand_id','=',$brand_id)
		->where('md.subtitle', 'like', '%'.$q.'%')
		->orWhere('md.description', 'like', '%'.$q.'%')
		->orWhere('shops.name', 'like', '%'.$q.'%')
		->orderBy('shops.name','asc');
		$movieResults = $movieSql->get();
	//echo getSQL($movieSql);die();
		$moviesArr = [];
		$i=0;
		foreach($movieResults as $records) {

			$moviesArr[$i]['shop_id'] = $records->shop_id;
			$moviesArr[$i]['movie_id'] = $records->movie_id;
			$moviesArr[$i]['store_location'] = $records->store_location;
			$moviesArr[$i]['name'] = $records->name;
			if($records->shop_image) {
				$moviesArr[$i]['show_image'] = url('/')."/images/shops/".$records->shop_image;
			} else {
				$moviesArr[$i]['show_image'] = url('/')."/images/no-image.jpg";
			}
			//$getRecords[$i]['description'] = $records->details;
			//$getRecords[$i]['entertainment_type'] = $records->entertainment_type;
			//$getRecords[$i]['subtitle'] = $records->subtitle;
			//$getRecords[$i]['duration'] = $records->duration;
			//$getRecords[$i]['2d_showtime'] = $records->showtime_2d;
			//$getRecords[$i]['3d_showtime'] = $records->showtime_3d;
			$moviesArr[$i]['cast'] = $records->cast;
			$moviesArr[$i]['upcoming'] = $records->upcoming;

			$i++;
		}
		return $moviesArr;

	}
// category_id (1 = Shop; 2 = Dinning; 3 = Entertainment)
	function shopSearch($q){
		$shopSql = Shop::whereIn( 'shops.category_id',array(1,2))
		//$shopSql = Shop::whereIn( 'shops.category_id',array(1))
		//$shopSql = Shop::where(['shops.category_id' => 1])
		->join('shop_brands as sb','shops._id','=','sb.shop_id')
		->join('master_store as ms','ms.store_id','=','shops.store_id')
		->select('shops._id as id','shops.name', 'shops.phone', 'shops.email', 'shops.website', 'shops.address', 'shops.details','shops.logo', 'ms.name as location')
		->where(function ($query) use ($q){
			$query->where('shops.name', 'like', '%'.$q.'%')
				->orWhere('shops.details', 'like', '%'.$q.'%');
		})
		->orderBy('name','asc')
		->groupBy('id');
		$shopResults = $shopSql->get();
	//echo getSQL($shopSql);die();
		$shopArr = [];
		$i=0;
		foreach($shopResults as $records) {
			$shopArr[$i]['shop_id'] = $records->id;
			$shopArr[$i]['shop_name'] = $records->name;
			$shopArr[$i]['shop_phone'] = $records->phone;
			$shopArr[$i]['shop_email'] = $records->email;
			$shopArr[$i]['shop_website'] = $records->website;
			$shopArr[$i]['location'] = $records->location;
			$shopArr[$i]['shop_address'] = $records->address;
			$shopArr[$i]['shop_detail'] = $records->details;

			if($records->logo) {
				$shopArr[$i]['shop_image'] = url('/')."/images/shops/thumbnails/".$records->logo;
			} else {
				$shopArr[$i]['shop_image'] = url('/')."/images/no-image.jpg";
			}


			$i++;
		}
		return $shopArr;
	}


	function getSQL($queryObj) {
		$sql = $queryObj->toSql();
		foreach ( $queryObj->getBindings() as $binding ) {
			$value = is_numeric($binding) ? $binding : "'".$binding."'";
			$sql = preg_replace('/\?/', $value, $sql, 1);
		}
		return $sql;
	}
