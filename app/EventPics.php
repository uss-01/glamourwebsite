<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventPics extends Model
{  
    
    protected $table = 'event_pics';
    protected $primaryKey = '_id';
    protected $fillable = [
        'event_id','image','created_by','updated_by' 
    ];

     public function event()
    {
    	return $this->belongsTo('App\Event', 'event_id', '_id');	
    }
}
