<?php

namespace App\Support;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LostFound extends Model
{
    use SoftDeletes;
	
    protected $table = 'lost_found_items';
    protected $primaryKey = '_id';
    protected $fillable = [
        'name','email','type','phone','message','image1','image2','image3','created_by','updated_by' 
    ];
}
