<?php
namespace App\Support;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Management extends Model{

    use SoftDeletes;
	
    protected $table = 'management_email';
    protected $primaryKey = '_id';
    protected $fillable = [
        'email','created_by','updated_by' 
    ];
}
