<?php

namespace App\Support;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Incident extends Model
{
    use SoftDeletes;
	
    protected $table = 'incidents';
    protected $primaryKey = '_id';
    protected $fillable = [
        'name','email','phone','message','image1','image2','image3','created_by','updated_by' 
    ];
}
