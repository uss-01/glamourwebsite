<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    protected $table = 'favourites';
    protected $primaryKey = '_id';
    protected $fillable = [
        'user_id','type','type_id' 
    ];

     public function shop()
    {
        return $this->belongsTo('App\Shop\Shop', 'type_id', '_id');  
    }
}
