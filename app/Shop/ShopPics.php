<?php

namespace App\Shop;

use Illuminate\Database\Eloquent\Model;

class ShopPics extends Model
{  
    
    protected $table = 'shop_pics';
    protected $primaryKey = '_id';
    protected $fillable = [
        'shop_id','image','created_by','updated_by' 
    ];

     public function shop()
    {
    	return $this->belongsTo('App\Shop\Shop', 'shop_id', '_id');	
    }
}
