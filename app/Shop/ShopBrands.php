<?php

namespace App\Shop;

use Illuminate\Database\Eloquent\Model;


class ShopBrands extends Model
{  
    protected $table = 'shop_brands';
    protected $primaryKey = '_id';
    protected $fillable = [
         'shop_id', 'brand_id','created_by','updated_by'
    ];
}
