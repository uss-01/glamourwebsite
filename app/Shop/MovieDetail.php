<?php

namespace App\Shop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MovieDetail extends Model
{
    use SoftDeletes;

    protected $table = 'movie_detail';
    protected $primaryKey = '_id';
    protected $fillable = [
        'shop_id', 'subtitle', 'duration','description','2d_showtime','3d_showtime', 'cast','created_by','updated_by'
    ];
}
