<?php

namespace App\Shop;
// use App\Traits\ActivityLogger;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shop extends Model
{
    use SoftDeletes;

    protected $table = 'shops';
    protected $primaryKey = '_id';
    protected $fillable = [
        'name', 'category_id', 'store_id','logo','store_category_id','store_subcategory_id', 'email', 'phone','website','address','details','floorlevel','latitude','longitude','lease_year','upcoming','created_by','updated_by','tenant_uid','beacon_id'
    ];

    public function category()
    {
    	return $this->belongsTo('App\Masters\Category', 'category_id', 'category_id');	
    }
	
	public function ShopCategoryName()
    {
    	return $this->hasOne('App\Masters\ProductCategory', '_id', 'store_category_id');	
    }
	
	public function ShopSubCategoryName()
    {
    	return $this->hasOne('App\Masters\ProductSubCategory', '_id', 'store_subcategory_id');	
    }
	
    public function store()
    {
        return $this->belongsTo('App\Masters\Store', 'store_id', 'store_id');  
    }

   
}
