<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteUserExperience extends Model
{
   
	
    protected $table = 'site_user_experiences';
    protected $primaryKey = '_id';
    protected $fillable = [
        'user_id','experience_id','created_by','updated_by' 
    ];
}
?>
