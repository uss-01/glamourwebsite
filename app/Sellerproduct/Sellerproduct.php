<?php

namespace App\Sellerproduct;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sellerproduct extends Model
{
    use SoftDeletes;

    protected $table = 'products';
    protected $primaryKey = '_id';
    protected $fillable = [
        'name','price', 'details','image','shop_id','brand_id','product_category_id','product_subcategory_id','discounted_price','product_quantity','product_status','featured','add_to_cart','attribute','seller_id', 'created_by','updated_by'
    ];

    public function shop()
    {
    	return $this->belongsTo('App\Shop\Shop', 'shop_id', '_id');	
    }

    public function brand()
    {
        return $this->belongsTo('App\Masters\Brand', 'brand_id', 'brand_id'); 
    }

    public function category()
    {
        return $this->belongsTo('App\Masters\Category', 'category_id', 'category_id');  
    }

     public function product_category()
    {
        return $this->belongsTo('App\Masters\ProductCategory', 'product_category_id', '_id');  
    }

     public function product_subcategory()
    {
        return $this->belongsTo('App\Masters\ProductSubcategory', 'product_subcategory_id', '_id');  
    }
}
