<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserJoinEvent extends Model
{
    protected $table = 'user_join_event';
    protected $primaryKey = '_id';
    protected $fillable = ['user_id','event_id','created_at','updated_at'];
}
