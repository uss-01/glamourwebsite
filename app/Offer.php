<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Offer extends Model
{
     use SoftDeletes;
	
    protected $table = 'offers';
    protected $primaryKey = '_id';
    protected $fillable = [
        'title','sub_title','shop_id','details','image','start_date','end_date','created_by','updated_by' 
    ];

     public function shop()
    {
        return $this->belongsTo('App\Shop\Shop', 'shop_id', '_id');  
    }
}
