<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';
    protected $primaryKey = '_id';
    protected $fillable = [
        'name','slug','price', 'details','sort_details','image','brand_id','product_category_id','product_subcategory_id','product_color_id','product_size_id','discounted_price','product_quantity','product_status','featured','new_arrival','on_sale','hot_deal','add_to_cart','best_seller','attribute','variations', 'created_by','updated_by'
    ];

    public function product_brand()
    {
        return $this->belongsTo('App\Masters\Brand', 'brand_id', '_id'); 
    }

    public function product_category()
    {
        return $this->belongsTo('App\Masters\ProductCategory', 'product_category_id', '_id');  
    }

    public function product_subcategory()
    {
        return $this->belongsTo('App\Masters\ProductSubcategory', 'product_subcategory_id', '_id');  
    }

    public function product_color()
    {
        return $this->belongsTo('App\Masters\Color', 'product_color_id', 'id');  
    }

    public function product_size()
    {
        return $this->belongsTo('App\Masters\Size', 'product_size_id', 'id');  
    }
}
