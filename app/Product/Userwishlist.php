<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Userwishlist extends Model
{
    use SoftDeletes;
    
    protected $table = 'users_wishlist';
    protected $primaryKey = '_id';
    protected $fillable = [
        'user_id','product_id','status','created_by','updated_by'
    ];
}
