<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = 'orders';
    protected $primaryKey = '_id';
    protected $fillable = [
        'user_id','amount', 'payment_method','firstname','lastname','address','phone','created_by','updated_by'
    ];
}
