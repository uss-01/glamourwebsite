<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'order_details';
    protected $primaryKey = '_id';
    protected $fillable = [
        'order_id','product_id', 'quantity','color','size','price','created_by','updated_by'
    ];
}
