<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Usercart extends Model
{
    use SoftDeletes;
    
    protected $table = 'users_cart';
    protected $primaryKey = '_id';
    protected $fillable = [
        'user_id','product_id','color','size','quantity','discounted_price','price','created_by','updated_by'
    ];
}
