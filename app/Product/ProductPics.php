<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

class ProductPics extends Model
{  
    
    protected $table = 'product_pics';
    protected $primaryKey = '_id';
    protected $fillable = [
        'product_id','image','created_by','updated_by' 
    ];

     public function shop()
    {
    	return $this->belongsTo('App\Product\Product', 'product_id', '_id');	
    }
}
