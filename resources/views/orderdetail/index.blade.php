@extends('layouts.app')
@section('content')
    <div class="container-fluid" id="pageContainer">
        <div class="row" id="pageContent">
            <div class="col-12 col-md-8 offset-md-2">
                <div class="float-right mt-2 hidden-xs-down">
                    <a href="{{ route('orderdetail.index') }}" class="btn btn-secondary btn-search" title="Search Order" data-search="{{ !empty(Request::input('q')) ? 'yes' : ''}}"><i class="fa fa-search"></i><span{!! !empty(Request::input('q')) ? '' : ' class="hidden-xs-up"' !!}> Close</span></a>
                    <a href="{{ route('orderdetail.index') }}" class="btn btn-secondary" role="button" title="Refresh Order List"><i class="fa fa-refresh"></i></a>
                </div>
                <div class="float-right mt-1 hidden-sm-up">
                    <a href="{{ route('orderdetail.index') }}" class="btn btn-secondary btn-sm btn-search" title="Search Order" data-search="{{ !empty(Request::input('q')) ? 'yes' : ''}}"><i class="fa fa-search"></i><span{!! !empty(Request::input('q')) ? '' : ' class="hidden-xs-up"' !!}> Close</span></a>
                    <a href="{{ route('orderdetail.index') }}" class="btn btn-secondary btn-sm" role="button" title="Refresh Order List"><i class="fa fa-refresh"></i></a>
                </div>
                <h1 class="hidden-xs-down">{{ Request::input('q') ? 'Search ' : '' }}Order <span class="text-muted h5">(total {{ number_format($bladeVar['total_count']) }})</span></h1>
                <h1 class="hidden-sm-up h3">{{ Request::input('q') ? 'Search ' : '' }}Order <br><span class="text-muted h6">(total {{ number_format($bladeVar['total_count']) }})</span></h1>
                <div class="clearfix alertArea"></div>
                <div class="card mb-2 searchContainer{{ Request::input('q') ? '' : ' hidden-xs-up'}}">
                    <div class="card-block">
                        <form method="get" action="{{ route('orderdetail.search') }}" class="m0 p0 form-search">
                            <div class="form-group mb-0">
                                <label class="form-control-label" for="q"><strong>Search by Order name</strong></label>
                                <input type="text" class="form-control" name="q" value="{{ Request::input('q') }}">
                                <small class="form-text text-muted">Type order name and hit "Enter" key to search</small>
                            </div>
                        </form>
                    </div>
                </div>
                @if ($bladeVar['total_count'] > 0)
                    <table class="table table-bordered table-hover table-sm">
                        <thead class="bg-faded">
                             <tr>
                                <th class="text-center" style="width: 4rem;"><nobr><a href="{{ url()->current() }}?{{ Request::input('q') ? 'q='.Request::input('q').'&' : '' }}sort=id&dir={{ Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'desc' : 'asc' : 'asc' }}">Id</a> <i class="fa fa-{{ Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'sort-numeric-asc' : 'sort-numeric-desc' : 'sort' }}"></i></nobr></th>
                                <th><nobr>Order Placed</nobr></th>
                                <th><nobr>Name</nobr></th>
                                <th><nobr>Image</nobr></th>
								<th><nobr>Price</nobr></th>
								<th><nobr>Quantity</nobr></th>
								<th><nobr>Order Status</nobr></th>
                                <th class="w-td-action">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($bladeVar['results'] as $rs)
                            <tr{!! $rs->deleted_at != null ? ' class="table-danger"' : '' !!}>
                                    <td class="text-center"><span{!! $rs->deleted_at != null ? ' class="badge badge-danger"' : '' !!}>{{ $rs->orderUniqid }}</span></td>
                                    <td>{{ date('d F Y', strtotime($rs->created_at)) }}</td>
                                    <td>{{ $rs->product_name }}</td>
									<td><img src="{{ asset('images/products/thumbnails').'/'.$rs->product_image }}" class="thumbnail" width="80" /></td>
									<td>QR {{ $rs->price }}</td>
                                    <td>{{ $rs->quantity }}</td>
                                    <!-- <td>{{ $rs->order_status }}</td> -->
                                    <td>
                                        <form method="post" class="updateStatus">
                                            <div class="form-group">
                                                <input type="hidden" class="form-control" name="id" id="id" value="{{ $rs->_id }}">
                                                <select name="order_status" class="form-control">
                                                    <option value=""{{ empty(old('order_status')) ? ' selected="selected"' : '' }}></option>
                                                    @foreach ($bladeVar['status'] as $key => $order_status)
                                                    <option value="{{ $order_status }}"{{ $order_status == old('order_status', $rs->order_status) ? ' selected="selected"' : '' }}>{{ $order_status }}</option>
                                                    @endforeach
                                                </select>
                                                <small class="form-control-feedback"></small>
                                            </div>
                                        </form>
                                    </td>
                                    <td class="text-center text-sm-left">
                                        <div class="hidden-xs-down">
                                            <a href="{{ route('orderdetail.show', $rs->_id) }}" class="btn btn-sm btn-info" title="View" data-toggle="modal" data-target="#viewModal"><i class="fa fa-eye"></i></a> 
                                            <!-- <a href="{{ route('orderdetail.edit', $rs->_id) }}" class="btn btn-sm btn-info{{ $rs->deleted_at != null ? ' disabled' : '' }}" title="Edit" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i></a>   -->
                                        </div>
                                        <div class="btn-group hidden-sm-up">
                                            <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Actions
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a href="{{ route('orderdetail.show', $rs->_id) }}" class="dropdown-item" data-toggle="modal" data-target="#viewModal"><i class="fa fa-eye text-info"></i> View</a>
                                                <!-- <a href="{{ route('orderdetail.edit', $rs->_id) }}" class="dropdown-item{{ $rs->deleted_at != null ? ' disabled' : '' }}" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil text-{{ $rs->deleted_at != null ? 'muted' : 'info' }}"></i> Edit</a>  -->
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-4">
                        {{ $bladeVar['results']->appends(Request::query())->links() }}
                        <small class="d-block text-center text-muted">(showing 20 results per page)</small>
                    </div>
                @else
                    <div class="alert alert-success">
                        <strong>No records found!</strong>
                        @if (!empty(Request::input('q')))
                            No matching your search criteria. 
                        @else 
                            No order added yet 
                        @endif
                    </div>
                @endif
                <!-- Deletetion code with Form -->
                <form class="formDelete">
                    {{ method_field('DELETE') }}
                </form>

                <!-- View modal code: START -->
                <div class="modal" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="viewModalTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="viewModalTitle">View Order</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            </div>
                            <div class="modal-footer hidden-xs-up">
                                <!-- <a href="javascript:void(0);" class="btn btn-info btn-edit" title="Edit" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i> Edit</a> -->
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- View modal code: END -->

                <!-- Edit modal code: START -->
                <div class="modal" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalTitle" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="editModalTitle">Edit Order</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            </div>
                            <div class="modal-footer hidden-xs-up">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Edit modal code: END -->
            </div>
        </div>
    </div>
@endsection

@section('foot')
    <script type="text/javascript">
        var pageUrl = '{{ route('orderdetail.index') }}';
        var ajaxLoadUrl = '{{ url()->full() }}';
        var pageContainer = '#pageContainer';
        var pageContent = '#pageContent';
    </script>
     <script src="{{ asset('js/jquery.form.js') }}"></script>
	<script src="{{ asset('js/orderdetail.js?v=1') }}"></script>
@endsection