<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Order Id</th>
		    		<td>{{ $bladeVar['result']->orderUniqid }}</td>
                </tr>
                <tr>
		    		<th class="bg-faded" style="width: 8rem;">Order Placed</th>
		    		<td>{{ date('d F Y', strtotime($bladeVar['result']->created_at)) }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Image</th>
		    		<td><img src="{{ asset('images/products').'/'.$bladeVar['result']->product_image }}" class="thumbnail" width="150" /></td>
                </tr>
                <tr>
		    		<th class="bg-faded">Name</th>
		    		<td>{{ $bladeVar['result']->product_name }}</td>
                </tr>
                <tr>
		    		<th class="bg-faded">Details</th>
		    		<td>{{ $bladeVar['result']->product_details }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Attributes</th>
		    		<td>{{ $bladeVar['result']->attr_key }} :  {{ $bladeVar['result']->attr_val }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Price</th>
		    		<td>QR {{ $bladeVar['result']->price }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Quantity</th>
		    		<td> {{ $bladeVar['result']->quantity }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Status</th>
		    		<td> {{ $bladeVar['result']->order_status }}</td>
				</tr>
				<tr>
				    <th class="bg-faded">User Details</th>
				</tr>
				<tr>
					<th class="bg-faded">User Name</th>
					<td> {{ $bladeVar['userdetail']->firstname }} {{ $bladeVar['userdetail']->lastname }}</td>
				</tr>
				<tr>
					<th class="bg-faded">Contact Number</th>
					<td> {{ $bladeVar['userdetail']->phone }}</td>
				</tr>
				<tr>
					<th class="bg-faded">Address</th>
					<td> {{ $bladeVar['userdetail']->address }}</td>
				</tr>
				<tr>
					<th class="bg-faded">Payment Method</th>
					<td> {{ $bladeVar['userdetail']->payment_method }}</td>
				</tr>
				<tr>
					<th class="bg-faded">Order Date</th>
					<td> {{ date('d F Y', strtotime($bladeVar['userdetail']->created_at)) }}</td>
				</tr>
		    </tbody>
		</table>
    	<!-- <span class="modalUrls hidden-xs-up" data-edit="{{ $bladeVar['result']->deleted_at == null ? route('orderdetail.edit', $bladeVar['result']->_id) : '' }}">
	    </span> -->
	</div>
</div>	    