@include('frontend.header')
<style type="text/css">
.addWishlist{
    color: red;
}
</style>
<main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">shop</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- page main wrapper start -->
        <div class="shop-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <!-- sidebar area start -->
                    <div class="col-lg-3 order-2 order-lg-1">
                        <aside class="sidebar-wrapper">
                            <!-- single sidebar start -->
                            @if (!empty($bladeVar['productsCategories']))
                            <div class="sidebar-single">
                                <h5 class="sidebar-title">categories</h5>
                                <div class="sidebar-body">
                                    <ul class="checkbox-container categories-list">
                                        @foreach($bladeVar['productsCategories'] as $category)
                                            @if($category->deleted_at == NULL)
                                            @if($category->categoryProducts != 0)
                                            <li>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input productCategory" name="productCategory[]" value="{{ $category->_id }}" id="productCategory{{ $category->_id }}" <?php if(!empty($_GET['category'])){ $categoryArray = explode(",",$_GET['category']); if(in_array($category->_id, $categoryArray)){ ?>checked<?php }else{ } } ?>>
                                                    <label class="custom-control-label" for="productCategory{{ $category->_id }}">{{ $category->name }} ({{ $category->categoryProducts }})</label>
                                                </div>
                                            </li>
                                            @endif
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            @endif
                            <!-- single sidebar end -->

                            <!-- single sidebar start -->
                            @if (!empty($bladeVar['productsMinMaxPrice']))
                            <div class="sidebar-single">
                                <h5 class="sidebar-title">price</h5>
                                <div class="sidebar-body">
                                    <div class="price-range-wrap">
                                        @foreach($bladeVar['productsMinMaxPrice'] as $price)
                                        <div class="price-range" data-min="{{ str_replace('.00','',$price->minPrice) }}" data-max="{{ str_replace('.00','',$price->maxPrice) }}"></div>
                                        @endforeach
                                        <div class="range-slider">
                                            <form action="#" class="d-flex align-items-center justify-content-between">
                                                <div class="price-input">
                                                    <label for="amount">Price: </label>
                                                    <input type="text" id="amount">
                                                </div>
                                                <button class="filter-btn" id="priceFilter">filter</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <!-- single sidebar end -->

                            <!-- single sidebar start -->
                            @if (!empty($bladeVar['productsBrands']))
                            <div class="sidebar-single">
                                <h5 class="sidebar-title">Brand</h5>
                                <div class="sidebar-body">
                                    <ul class="checkbox-container categories-list">
                                        @foreach($bladeVar['productsBrands'] as $brand)
                                            @if($brand->deleted_at == NULL)
                                            @if($brand->brandProducts != 0)
                                            <li>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input productBrand" name="productBrand[]" value="{{ $brand->_id }}" id="productBrand{{ $brand->_id }}" <?php if(!empty($_GET['brand'])){ $brandArray = explode(",",$_GET['brand']); if(in_array($brand->_id, $brandArray)){ ?>checked<?php }else{ } } ?>>
                                                    <label class="custom-control-label" for="productBrand{{ $brand->_id }}">{{ $brand->name }} ({{ $brand->brandProducts }})</label>
                                                </div>
                                            </li>
                                            @endif
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            @endif
                            <!-- single sidebar end -->

                            <!-- single sidebar start -->
                            @if (!empty($bladeVar['productsColors']))
                            <div class="sidebar-single">
                                <h5 class="sidebar-title">color</h5>
                                <div class="sidebar-body">
                                    <ul class="checkbox-container categories-list">
                                        @foreach($bladeVar['productsColors'] as $color)
                                            @if($color->deleted_at == NULL)
                                            @if($color->colorProducts != 0)
                                            <li>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input productColor" name="productColor[]" value="{{ $color->id }}" id="productColor{{ $color->id }}" <?php if(!empty($_GET['color'])){ $colorArray = explode(",",$_GET['color']); if(in_array($color->id, $colorArray)){ ?>checked<?php }else{ } } ?>>
                                                    <label class="custom-control-label" for="productColor{{ $color->id }}">{{ $color->name }} ({{ $color->colorProducts }})</label>
                                                </div>
                                            </li>
                                            @endif
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            @endif
                            <!-- single sidebar end -->

                            <!-- single sidebar start -->
                            @if (!empty($bladeVar['productsSizes']))
                            <div class="sidebar-single">
                                <h5 class="sidebar-title">size</h5>
                                <div class="sidebar-body">
                                    <ul class="checkbox-container categories-list">
                                        @foreach($bladeVar['productsSizes'] as $size)
                                            @if($size->deleted_at == NULL)
                                            @if($size->sizeProducts != 0)
                                            <li>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input productSize" name="productSize[]" value="{{ $size->id }}" id="productSize{{ $size->id }}" <?php if(!empty($_GET['size'])){ $sizeArray = explode(",",$_GET['size']); if(in_array($size->id, $sizeArray)){ ?>checked<?php }else{ } } ?>>
                                                    <label class="custom-control-label" for="productSize{{ $size->id }}">{{ $size->name }} ({{ $size->sizeProducts }})</label>
                                                </div>
                                            </li>
                                            @endif
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            @endif
                            <!-- single sidebar end -->

                            <!-- single sidebar start -->
                            <!-- <div class="sidebar-banner">
                                <div class="img-container">
                                    <a href="#">
                                        <img src="assets/img/banner/sidebar-banner.jpg" alt="">
                                    </a>
                                </div>
                            </div> -->
                            <!-- single sidebar end -->
                        </aside>
                    </div>
                    <!-- sidebar area end -->

                    <!-- shop main wrapper start -->
                    <div class="col-lg-9 order-1 order-lg-2">
                        <div class="shop-product-wrapper">
                            <!-- shop product top wrap start -->
                            <div class="shop-top-bar">
                                <div class="row align-items-center">
                                    <div class="col-lg-7 col-md-6 order-2 order-md-1">
                                        <div class="top-bar-left">
                                            <div class="product-view-mode">
                                                <a class="active" href="#" data-target="grid-view" data-toggle="tooltip" title="Grid View"><i class="fa fa-th"></i></a>
                                                <a href="#" data-target="list-view" data-toggle="tooltip" title="List View"><i class="fa fa-list"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-6 order-1 order-md-2">
                                        <div class="top-bar-right">
                                            <div class="product-short">
                                                <p>Sort By : </p>
                                                <select class="nice-select" id="sortby" name="sortby">
                                                    <option value="trending" <?php if(!empty($_GET['sortby']) && ($_GET['sortby'] == 'trending')){ ?>selected<?php }else{ } ?>>Relevance</option>
                                                    <option value="a-z" <?php if(!empty($_GET['sortby']) && ($_GET['sortby'] == 'a-z')){ ?>selected<?php }else{ } ?>>Name (A - Z)</option>
                                                    <option value="z-a" <?php if(!empty($_GET['sortby']) && ($_GET['sortby'] == 'z-a')){ ?>selected<?php }else{ } ?>>Name (Z - A)</option>
                                                    <option value="low-high" <?php if(!empty($_GET['sortby']) && ($_GET['sortby'] == 'low-high')){ ?>selected<?php }else{ } ?>>Price (Low &gt; High)</option>
                                                    <option value="high-low" <?php if(!empty($_GET['sortby']) && ($_GET['sortby'] == 'high-low')){ ?>selected<?php }else{ } ?>>Price (High &gt; Low)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- shop product top wrap start -->
                            @if (!empty($bladeVar['product_results']->count() > 0))
                            <!-- product item list wrapper start -->
                            <div class="shop-product-wrap grid-view row mbn-30">
                                <!-- product single item start -->
                                @foreach($bladeVar['product_results'] as $product)
							        @if($product->deleted_at == NULL) 
                                    <div class="col-md-4 col-sm-6">
                                        <!-- product grid start -->
                                        <div class="product-item">
                                            <figure class="product-thumb">
                                                <a href="{{ url('/shop/'.$product->slug) }}">
                                                    <img class="pri-img" src="{{ asset('images/products').'/'.$product->image}}" alt="{{ $product->slug }}">
										            <img class="sec-img" src="{{ asset('images/products').'/'.$product->image}}" alt="{{ $product->slug }}">
                                                </a>
                                                <div class="product-badge">
                                                    @if(($product->new_arrival == '1') && ($product->on_sale == '1'))
                                                    <div class="product-label new">
                                                        <span>new</span>
                                                    </div>
                                                    <div class="product-label discount">
                                                        <span>sale</span>
                                                    </div>
                                                    @elseif($product->new_arrival == '1')
                                                    <div class="product-label new">
                                                        <span>new</span>
                                                    </div>
                                                    @elseif($product->on_sale == '1')
                                                    <div class="product-label new">
                                                        <span>sale</span>
                                                    </div>
                                                    @endif
                                                </div>
                                                <div class="button-group">
                                                <?php 
                                                $arrayWishlist = array();
                                                if(!empty($bladeVar['userWishlistData'])){ 
                                                    foreach($bladeVar['userWishlistData'] as $wishlist){
                                                        $arrayWishlist[] = $wishlist->product_id;
                                                    } 
                                                } 
                                                if (in_array($product->_id, $arrayWishlist)) { ?>
                                                    <a href="#" class="addtowishlist" id="addtowishlist{{ $product->_id }}" data-pid="{{ $product->_id }}" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like addWishlist"></i></a>
                                                <?php }else{ ?>
                                                    <a href="#" class="addtowishlist" id="addtowishlist{{ $product->_id }}" data-pid="{{ $product->_id }}" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like"></i></a>
                                                <?php } ?>
                                                </div>
                                                @if(!empty($product->product_quantity) && ($product->product_quantity > 0))
                                                    @if($product->add_to_cart == '1')
                                                    <div class="cart-hover">
                                                        <?php $arrayCartList = array();
                                                        if(!empty($bladeVar['userCartData'])){ 
                                                            foreach($bladeVar['userCartData'] as $cartList){
                                                                $arrayCartList[] = $cartList->product_id;
                                                            } 
                                                        } 
                                                        if (in_array($product->_id, $arrayCartList)) { ?>
                                                            <button class="btn btn-cart addtocart disabled" id="addtocart{{ $product->_id }}" data-pid="{{ $product->_id }}">Go to cart</button>
                                                        <?php }else{ ?>
                                                            <button class="btn btn-cart addtocart" id="addtocart{{ $product->_id }}" data-pid="{{ $product->_id }}" data-cid="{{ $product->product_color_id }}" data-sid="{{ $product->product_size_id }}">add to cart</button>
                                                        <?php } ?>
                                                    </div>
                                                    @endif
                                                @else
                                                    <div class="cart-hover">
                                                        <button class="btn btn-cart disabled">Out of stock</button>
                                                    </div>
                                                @endif 
                                            </figure>
                                            <div class="product-caption text-center">
                                                <div class="product-identity">
                                                    <p class="manufacturer-name"><a href="{{ url('/shop/?category='.$product->product_category->_id) }}">{{ $product->product_category->name }}</a></p>
                                                </div>
                                                <?php
                                                $colorArray = array(); 
                                                $colorArray[0] = $product->product_color_id;
                                                if(!empty($product->variations)){
                                                    $variations = json_decode($product->variations); 
                                                    foreach($variations as $variation){
                                                        $colorArray[] = $variation->color;
                                                    }
                                                }
                                                $getColorArray = array_unique($colorArray);
                                                if(!empty($getColorArray)){ 
                                                ?>
                                                <ul class="color-categories">
                                                <?php foreach($getColorArray as $color){ 
                                                    $allColors = $bladeVar['color_results'];
                                                    if(!empty($allColors)){
                                                        foreach($allColors as $getColor){
                                                            if($getColor->id == $color){ ?>
                                                                <li>
                                                                    <a href="#" style="background-color: <?php echo $getColor->color_code; ?>;" title="<?php echo $getColor->name; ?>"></a>
                                                                </li>   
                                                            <?php }
                                                        }
                                                    }
                                                } ?>
                                                </ul>
                                                <?php } ?>	
                                                <h6 class="product-name">
                                                    <a href="{{ url('/shop/'.$product->slug) }}">{{ $product->name }}</a>
                                                </h6>
                                                <div class="price-box">
                                                    @if(!empty($product->discounted_price) && ($product->discounted_price != '0.00'))
                                                    <span class="price-regular">${{ $product->discounted_price }}</span>
                                                    <span class="price-old"><del>${{ $product->price }}</del></span>
                                                    @else
                                                    <span class="price-regular">${{ $product->price }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!-- product grid end -->

                                        <!-- product list item end -->
                                        <div class="product-list-item">
                                            <figure class="product-thumb">
                                                <a href="{{ url('/shop/'.$product->slug) }}">
                                                    <img class="pri-img" src="{{ asset('images/products').'/'.$product->image}}" alt="{{ $product->slug }}">
										            <img class="sec-img" src="{{ asset('images/products').'/'.$product->image}}" alt="{{ $product->slug }}">
                                                </a>
                                                <div class="product-badge">
                                                    @if(($product->new_arrival == '1') && ($product->on_sale == '1'))
                                                    <div class="product-label new">
                                                        <span>new</span>
                                                    </div>
                                                    <div class="product-label discount">
                                                        <span>sale</span>
                                                    </div>
                                                    @elseif($product->new_arrival == '1')
                                                    <div class="product-label new">
                                                        <span>new</span>
                                                    </div>
                                                    @elseif($product->on_sale == '1')
                                                    <div class="product-label new">
                                                        <span>sale</span>
                                                    </div>
                                                    @endif
                                                </div>
                                                <div class="button-group">
                                                <?php 
                                                $arrayWishlist = array();
                                                if(!empty($bladeVar['userWishlistData'])){ 
                                                    foreach($bladeVar['userWishlistData'] as $wishlist){
                                                        $arrayWishlist[] = $wishlist->product_id;
                                                    } 
                                                } 
                                                if (in_array($product->_id, $arrayWishlist)) { ?>
                                                    <a href="#" class="addtowishlist" id="addtowishlist{{ $product->_id }}" data-pid="{{ $product->_id }}" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like addWishlist"></i></a>
                                                <?php }else{ ?>
                                                    <a href="#" class="addtowishlist" id="addtowishlist{{ $product->_id }}" data-pid="{{ $product->_id }}" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like"></i></a>
                                                <?php } ?>
                                                </div>
                                                @if(!empty($product->product_quantity) && ($product->product_quantity > 0))
                                                    @if($product->add_to_cart == '1')
                                                    <div class="cart-hover">
                                                        <?php $arrayCartList = array();
                                                        if(!empty($bladeVar['userCartData'])){ 
                                                            foreach($bladeVar['userCartData'] as $cartList){
                                                                $arrayCartList[] = $cartList->product_id;
                                                            } 
                                                        } 
                                                        if (in_array($product->_id, $arrayCartList)) { ?>
                                                            <button class="btn btn-cart addtocart disabled" id="addtocart{{ $product->_id }}" data-pid="{{ $product->_id }}">Go to cart</button>
                                                        <?php }else{ ?>
                                                            <button class="btn btn-cart addtocart" id="addtocart{{ $product->_id }}" data-pid="{{ $product->_id }}" data-cid="{{ $product->product_color_id }}" data-sid="{{ $product->product_size_id }}">add to cart</button>
                                                        <?php } ?>
                                                    </div>
                                                    @endif
                                                @else
                                                    <div class="cart-hover">
                                                        <button class="btn btn-cart disabled">Out of stock</button>
                                                    </div>
                                                @endif 
                                            </figure>
                                            <div class="product-content-list">
                                                <div class="manufacturer-name">
                                                    <a href="{{ url('/shop/?category='.$product->product_category->_id) }}">{{ $product->product_category->name }}</a>
                                                </div>
                                                <?php
                                                $colorArray = array(); 
                                                $colorArray[0] = $product->product_color_id;
                                                if(!empty($product->variations)){
                                                    $variations = json_decode($product->variations); 
                                                    foreach($variations as $variation){
                                                        $colorArray[] = $variation->color;
                                                    }
                                                }
                                                $getColorArray = array_unique($colorArray);
                                                if(!empty($getColorArray)){ 
                                                ?>
                                                <ul class="color-categories">
                                                <?php foreach($getColorArray as $color){ 
                                                    $allColors = $bladeVar['color_results'];
                                                    if(!empty($allColors)){
                                                        foreach($allColors as $getColor){
                                                            if($getColor->id == $color){ ?>
                                                                <li>
                                                                    <a href="#" style="background-color: <?php echo $getColor->color_code; ?>;" title="<?php echo $getColor->name; ?>"></a>
                                                                </li>   
                                                            <?php }
                                                        }
                                                    }
                                                } ?>
                                                </ul>
                                                <?php } ?>	
                                                <h5 class="product-name"><a href="{{ url('/shop/'.$product->slug) }}">{{ $product->name }}</a></h5>
                                                <div class="price-box">
                                                    @if(!empty($product->discounted_price) && ($product->discounted_price != '0.00'))
                                                    <span class="price-regular">${{ $product->discounted_price }}</span>
                                                    <span class="price-old"><del>${{ $product->price }}</del></span>
                                                    @else
                                                    <span class="price-regular">${{ $product->price }}</span>
                                                    @endif
                                                </div>
                                                <p>{{ $product->sort_details }}</p>
                                            </div>
                                        </div>
                                        <!-- product list item end -->
                                    </div>
                                    @endif
						        @endforeach
                                <!-- product single item start -->
                            </div>
                            <!-- product item list wrapper end -->

                            <!-- start pagination area -->
                            <div class="paginatoin-area text-center">
                             {{ $bladeVar['product_results']->appends(Request::query())->links() }} 
                            </div>
                            <!-- end pagination area -->
                            @else
                                <div class="paginatoin-area text-center">
                                    <strong>No records found!</strong>
                                </div>
                            @endif
                        </div>
                    </div>
                    <!-- shop main wrapper end -->
                </div>
            </div>
        </div>
        <!-- page main wrapper end -->
    </main>
@include('frontend.footer')
<script type="text/javascript">
$(document).ready(function() {
    var baseUrl = '{{ url("/") }}';
    var seasoning = ''; 
    var category = []; 
    var brand = []; 
    var color = []; 
    var size = [];

    $('input[type=checkbox]').click(function (e) {
        $('input[name="productCategory[]"]:checked').each(function(){
            category.push($(this).val());
        })
        if(category.length !== 0){
            seasoning+='&category='+category.toString();
        }
        $('input[name="productBrand[]"]:checked').each(function(){
            brand.push($(this).val());
        })
        if(brand.length !== 0){
            seasoning+='&brand='+brand.toString();
        }
        $('input[name="productColor[]"]:checked').each(function(){
            color.push($(this).val());
        })
        if(color.length !== 0){
            seasoning+='&color='+color.toString();
        }
        $('input[name="productSize[]"]:checked').each(function(){
            size.push($(this).val());
        })
        if(size.length !== 0){
            seasoning+='&size='+size.toString();
        }
        var redirecturl1 = baseUrl+'/shop?'+ seasoning;
        location.href = redirecturl1;
    });

    $("#sortby").change(function(){
        var selectedOrder = $(this).children("option:selected").val();
        var urlParams = new URLSearchParams(window.location.search);
        var pageURL = $(location).attr("href");
        var category = '', brand = '', color = '', size = '', price = ''; 
        if(urlParams.has('category')){
            category = urlParams.has('category');
        }else if(urlParams.has('brand')){
            brand = urlParams.has('brand');
        }else if(urlParams.has('color')){
            color = urlParams.has('color');
        }else if(urlParams.has('size')){
            size = urlParams.has('size');
        }else if(urlParams.has('price')){
            price = urlParams.has('price');
        }
        if((category != '') || (brand != '') || (color != '') || (size != '') || (price != '')){
            seasoning+='&sortby='+selectedOrder;
            var redirecturl = pageURL+seasoning;
        }else{
            seasoning+='sortby='+selectedOrder;
            var redirecturl = baseUrl+'/shop?'+ seasoning;
        } 
        location.href = redirecturl;
    });
    
    $("#priceFilter").on('click', function(event) {
		event.preventDefault();
        var getPrice = $('#amount').val();
        var urlParams = new URLSearchParams(window.location.search);
        var pageURL = $(location).attr("href");
        var category = '', brand = '', color = '', size = '', price = '', sortby = ''; 
        if(urlParams.has('category')){
            category = urlParams.has('category');
        }else if(urlParams.has('brand')){
            brand = urlParams.has('brand');
        }else if(urlParams.has('color')){
            color = urlParams.has('color');
        }else if(urlParams.has('size')){
            size = urlParams.has('size');
        }else if(urlParams.has('price')){
            price = urlParams.has('price');
        }else if(urlParams.has('sortby')){
            sortby = urlParams.has('sortby');
        }
        if((category != '') || (brand != '') || (color != '') || (size != '') || (price != '') || (sortby != '')){
            seasoning+='&price='+ getPrice;
            var redirecturl = pageURL+seasoning;
        }else{
            seasoning+='price='+getPrice;
            var redirecturl = baseUrl+'/shop?'+ seasoning;
        } 
        location.href = redirecturl;
    });
});
</script>