<div class="minicart-close">
    <i class="pe-7s-close"></i>
</div>    
@if (!empty($bladeVar['userCartProducts']))
<?php
    $arrayCartlist = array();
    if(!empty($bladeVar['userCartData'])){ 
        foreach($bladeVar['userCartData'] as $cartlist){
            $arrayCartlist[] = $cartlist->product_id;
        } 
    } ?>
    @if (!empty($arrayCartlist)) 
        @if (sizeof($bladeVar['userCartProducts']) > 0)     
        <div class="minicart-content-box">
            <div class="minicart-item-wrapper">
                <ul>
                    @foreach($bladeVar['userCartProducts'] as $product)
                        @if($product->deleted_at == NULL)
                            @if(in_array($product->_id, $arrayCartlist))
                                <li class="minicart-item" id="product{{ $product->_id }}">
                                    <div class="minicart-thumb">
                                        <a href="{{ url('/shop/'.$product->slug) }}">
                                            <img src="{{ asset('images/products').'/'.$product->image}}" alt="{{ $product->slug }}">
                                        </a>
                                    </div>
                                    <div class="minicart-content">
                                        <h3 class="product-name">
                                            <a href="{{ url('/shop/'.$product->slug) }}">{{ $product->name }}</a>
                                        </h3>
                                        <p>
                                            <span class="cart-quantity">{{ $product->quantity }} <strong>&times;</strong></span>
                                            <span class="cart-price">
                                            @if(!empty($product->discounted_price) && ($product->discounted_price != '0.00'))
                                                ${{ number_format($product->discounted_price, 2) }}
                                            @else
                                                ${{ number_format($product->price, 2) }}
                                            @endif
                                            </span>
                                        </p>
                                    </div>
                                    <button class="minicart-remove removetocart" data-pid="{{ $product->_id }}"><i class="pe-7s-close"></i></button>
                                </li>
                            @endif
                        @endif
                    @endforeach
                </ul>
            </div>

            <div class="minicart-pricing-box">
                <ul>
                    <?php 
                        $subTotal = array();
                        $shipping = '0.00';
                        foreach($bladeVar['userCartProducts'] as $product){
                            if($product->deleted_at == NULL){
                                if(in_array($product->_id, $arrayCartlist)){
                                    if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')){
                                        $subTotal[] = number_format($product->discounted_price * $product->quantity, 2);
                                    }else{
                                        $subTotal[] = number_format($product->price * $product->quantity, 2);
                                    }
                                }
                            }
                        }
                    ?>
                    <li>
                        <span>sub-total</span>
                        <span><strong>$<?php if(!empty($subTotal)){ echo number_format(array_sum($subTotal), 2); }else{ echo '0.00'; } ?></strong></span>
                    </li>
                    <li>
                        <span>shipping</span>
                        <span><strong>$<?php if(!empty($shipping)){ echo $shipping; }else{ echo '0.00'; } ?></strong></span>
                    </li>
                    <li class="total">
                        <span>total</span>
                        <span><strong>$<?php if(!empty($subTotal)){ echo number_format((array_sum($subTotal)+$shipping), 2); }else{ echo '0.00'; } ?></strong></span>
                    </li>
                </ul>
            </div>

            <div class="minicart-button">
                <a href="{{ url('/cart') }}"><i class="fa fa-shopping-cart"></i> View Cart</a>
                <a href="{{ url('/checkout') }}"><i class="fa fa-share"></i> Checkout</a>
            </div>
        </div>
        @endif
    @else
        <div class="row">
            <div class="col-lg-12" style="text-align: center;">
                <h3>Your cart is empty!</h3>
                <p>Add items to it now.</p>
                <a href="{{ url('/') }}" class="btn btn-sqr">Shop now</a>
            </div>
        </div>    
    @endif
@else
    <div class="row">
        <div class="col-lg-12" style="text-align: center;">
            <h3>Your cart is empty!</h3>
            <p>Add items to it now.</p>
            <a href="{{ url('/') }}" class="btn btn-sqr">Shop now</a>
        </div>
    </div>    
@endif