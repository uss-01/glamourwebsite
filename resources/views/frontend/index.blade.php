@include('frontend.header')
<style type="text/css">
.addWishlist{
    color: red;
}
</style>
<main>
    @if (!empty($bladeVar['slider_results']->count() > 0)) 
	<!-- hero slider area start -->
	<section class="slider-area">
		<div class="hero-slider-active slick-arrow-style slick-arrow-style_hero slick-dot-style">
			@foreach ($bladeVar['slider_results'] as $slider)
				@if($slider->deleted_at == NULL)
				<!-- single slider item start -->
				<div class="hero-single-slide hero-overlay">
					<div class="hero-slider-item bg-img" data-bg="{{ asset('images/cms').'/'.$slider->image}}">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<div class="hero-slider-content {{ $slider->slidePosition }}">
										<h2 class="slide-title"><?php echo $slider->title; ?></h2>
										<h4 class="slide-desc">{{ $slider->shortDescription }}</h4>
										<a href="{{ $slider->redirect_url }}" class="btn btn-hero">{{ $slider->btntext }}</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- single slider item start -->
				@endif
            @endforeach
		</div>
	</section>
	<!-- hero slider area end -->
	@endif
	@if (!empty($bladeVar['site_setting']->count() > 0))
		@foreach($bladeVar['site_setting']  as $setting)
		    @if($setting->shipping == '1')
	<!-- service policy area start -->
	<div class="service-policy section-padding">
		<div class="container">
			<div class="row mtn-30">
				
				<div class="col-sm-6 col-lg-3">
					<div class="policy-item">
						<div class="policy-icon">
							<i class="pe-7s-plane"></i>
						</div>
						<div class="policy-content">
							<h6>Free Shipping</h6>
							<p>Free shipping all order</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="policy-item">
						<div class="policy-icon">
							<i class="pe-7s-help2"></i>
						</div>
						<div class="policy-content">
							<h6>Support 24/7</h6>
							<p>Support 24 hours a day</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="policy-item">
						<div class="policy-icon">
							<i class="pe-7s-back"></i>
						</div>
						<div class="policy-content">
							<h6>Money Return</h6>
							<p>30 days for free return</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="policy-item">
						<div class="policy-icon">
							<i class="pe-7s-credit"></i>
						</div>
						<div class="policy-content">
							<h6>100% Payment Secure</h6>
							<p>We ensure secure payment</p>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<!-- service policy area end -->
	        @endif 
	    @endforeach
    @endif

    @if (!empty($bladeVar['banner_results']->count() > 0)) 
	<!-- banner statistics area start -->
	<div class="banner-statistics-area">
		<div class="container">
			<div class="row row-20 mtn-20">
				@foreach ($bladeVar['banner_results'] as $banner)
					@if($banner->deleted_at == NULL)
					<div class="col-sm-6">
						<figure class="banner-statistics mt-20">
							<a href="{{ url('/shop?category='.$banner->product_category->_id) }}">
								<img src="{{ asset('images/cms').'/'.$banner->image}}" alt="{{ $banner->name }}">
							</a>
							<div class="banner-content {{ $banner->bannerPosition }}">
								<h5 class="banner-text1">{{ $banner->name }}</h5>
								<h2 class="banner-text2"><?php echo $banner->shortDescription; ?></h2>
								<a href="{{ url('/shop?category='.$banner->product_category->_id) }}" class="btn btn-text">{{ $banner->btntext }}</a>
							</div>
						</figure>
					</div>
					@endif
				@endforeach
			</div>
		</div>
	</div>
	<!-- banner statistics area end -->
    @endif
	@if (!empty($bladeVar['product_results']->count() > 0))
	<!-- product area start -->
	<section class="product-area section-padding">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<!-- section title start -->
					<div class="section-title text-center">
						<h2 class="title">our products</h2>
						<p class="sub-title">Add our products to weekly lineup</p>
					</div>
					<!-- section title start -->
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="product-container">
						<!-- product tab menu start -->
						<div class="product-tab-menu">
							<ul class="nav justify-content-center">
							<?php  
							$array = array();
							foreach ($bladeVar['product_results'] as $product){
						        if($product->deleted_at == NULL){		
									$array[] = $product->product_category->name;
							    } 
							}
							$categoryArray = array_unique($array);
							?> 
							    @php($countp=0)
								@foreach ($categoryArray as $productC)
									@php($countp++)
						            @if($countp == '1')
									<li><a href="#tab{{ $countp }}" class="active" data-toggle="tab">{{ $productC }}</a></li>
									@else
									<li><a href="#tab{{ $countp }}" data-toggle="tab">{{ $productC }}</a></li>
									@endif
								@endforeach
							</ul>
						</div>
						<!-- product tab menu end -->
						<!-- product tab content start -->
						<div class="tab-content">
					    @php($countp1=0)
						@foreach($categoryArray as $productC)
							@php($countp1++)
							@if($countp1 == '1')
							<div class="tab-pane fade show active" id="tab{{ $countp1 }}">
								<div class="product-carousel-4 slick-row-10 slick-arrow-style">
								@foreach($bladeVar['product_results']  as $product)
								    @if($product->deleted_at == NULL)
									@if($productC == $product->product_category->name)
									<!-- product item start -->
									<div class="product-item">
										<figure class="product-thumb">
											<a href="{{ url('/shop/'.$product->slug) }}">
												<img class="pri-img" src="{{ asset('images/products').'/'.$product->image}}" alt="{{ $product->slug }}">
												<img class="sec-img" src="{{ asset('images/products').'/'.$product->image}}" alt="{{ $product->slug }}">
											</a>
											<div class="product-badge">
											    @if(($product->new_arrival == '1') && ($product->on_sale == '1'))
												<div class="product-label new">
													<span>new</span>
												</div>
												<div class="product-label discount">
													<span>sale</span>
												</div>
												@elseif($product->new_arrival == '1')
                                                <div class="product-label new">
													<span>new</span>
												</div>
												@elseif($product->on_sale == '1')
												<div class="product-label new">
													<span>sale</span>
												</div>
												@endif
											</div>
											<div class="button-group">
											<?php 
											$arrayWishlist = array();
											if(!empty($bladeVar['userWishlistData'])){ 
												foreach($bladeVar['userWishlistData'] as $wishlist){
													$arrayWishlist[] = $wishlist->product_id;
												} 
											} 
											if (in_array($product->_id, $arrayWishlist)) { ?>
												<a href="#" class="addtowishlist" id="addtowishlist{{ $product->_id }}" data-pid="{{ $product->_id }}" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like addWishlist"></i></a>
											<?php }else{ ?>
												<a href="#" class="addtowishlist" id="addtowishlist{{ $product->_id }}" data-pid="{{ $product->_id }}" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like"></i></a>
											<?php } ?>
											</div>
											@if(!empty($product->product_quantity) && ($product->product_quantity > 0))
												@if($product->add_to_cart == '1')
												<div class="cart-hover">
													<?php $arrayCartList = array();
													if(!empty($bladeVar['userCartData'])){ 
														foreach($bladeVar['userCartData'] as $cartList){
															$arrayCartList[] = $cartList->product_id;
														} 
													} 
													if (in_array($product->_id, $arrayCartList)) { ?>
														<button class="btn btn-cart addtocart disabled" id="addtocart{{ $product->_id }}" data-pid="{{ $product->_id }}">Go to cart</button>
													<?php }else{ ?>
														<button class="btn btn-cart addtocart" id="addtocart{{ $product->_id }}" data-pid="{{ $product->_id }}" data-cid="{{ $product->product_color_id }}" data-sid="{{ $product->product_size_id }}">add to cart</button>
													<?php } ?>
												</div>
												@endif
											@else
												<div class="cart-hover">
													<button class="btn btn-cart disabled">Out of stock</button>
												</div>
											@endif 
										</figure>
										<div class="product-caption text-center">
											<div class="product-identity">
												<p class="manufacturer-name"><a href="{{ url('/shop/?category='.$product->product_category->_id) }}">{{ $product->product_category->name }}</a></p>
											</div>
											<?php
											$colorArray = array(); 
											$colorArray[0] = $product->product_color_id;
											if(!empty($product->variations)){
                                                $variations = json_decode($product->variations); 
												foreach($variations as $variation){
													$colorArray[] = $variation->color;
												}
											}
											$getColorArray = array_unique($colorArray);
											if(!empty($getColorArray)){ 
											?>
											<ul class="color-categories">
											<?php foreach($getColorArray as $color){ 
												$allColors = $bladeVar['color_results'];
												if(!empty($allColors)){
													foreach($allColors as $getColor){
                                                        if($getColor->id == $color){ ?>
															<li>
																<a href="#" style="background-color: <?php echo $getColor->color_code; ?>;" title="<?php echo $getColor->name; ?>"></a>
															</li>   
														<?php }
													}
												}
											} ?>
											</ul>
										    <?php } ?>	
											<h6 class="product-name">
												<a href="{{ url('/shop/'.$product->slug) }}">{{ $product->name }}</a>
											</h6>
											<div class="price-box">
												@if(!empty($product->discounted_price) && ($product->discounted_price != '0.00'))
                                                <span class="price-regular">${{ $product->discounted_price }}</span>
                                                <span class="price-old"><del>${{ $product->price }}</del></span>
                                                @else
                                                <span class="price-regular">${{ $product->price }}</span>
                                                @endif
											</div>
										</div>
									</div>
									<!-- product item end -->
									@endif
									@endif
								@endforeach
								</div>
							</div>
							@else
							<div class="tab-pane fade" id="tab{{ $countp1 }}">
								<div class="product-carousel-4 slick-row-10 slick-arrow-style">
									@foreach($bladeVar['product_results']  as $product)
										@if($product->deleted_at == NULL)
										@if($productC == $product->product_category->name)
										<!-- product item start -->
										<div class="product-item">
											<figure class="product-thumb">
												<a href="{{ url('/shop/'.$product->slug) }}">
													<img class="pri-img" src="{{ asset('images/products').'/'.$product->image}}" alt="{{ $product->slug }}">
													<img class="sec-img" src="{{ asset('images/products').'/'.$product->image}}" alt="{{ $product->slug }}">
												</a>
												<div class="product-badge">
													@if(($product->new_arrival == '1') && ($product->on_sale == '1'))
													<div class="product-label new">
														<span>new</span>
													</div>
													<div class="product-label discount">
														<span>sale</span>
													</div>
													@elseif($product->new_arrival == '1')
													<div class="product-label new">
														<span>new</span>
													</div>
													@elseif($product->on_sale == '1')
													<div class="product-label new">
														<span>sale</span>
													</div>
													@endif
												</div>
												<div class="button-group">
												<?php 
                                                $arrayWishlist = array();
                                                if(!empty($bladeVar['userWishlistData'])){ 
                                                    foreach($bladeVar['userWishlistData'] as $wishlist){
                                                        $arrayWishlist[] = $wishlist->product_id;
                                                    } 
                                                } 
                                                if (in_array($product->_id, $arrayWishlist)) { ?>
                                                    <a href="#" class="addtowishlist" id="addtowishlist{{ $product->_id }}" data-pid="{{ $product->_id }}" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like addWishlist"></i></a>
                                                <?php }else{ ?>
                                                    <a href="#" class="addtowishlist" id="addtowishlist{{ $product->_id }}" data-pid="{{ $product->_id }}" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like"></i></a>
                                                <?php } ?>
												</div>
											    @if(!empty($product->product_quantity) && ($product->product_quantity > 0)) 
													@if($product->add_to_cart == '1')
													<div class="cart-hover">
														<?php $arrayCartList = array();
														if(!empty($bladeVar['userCartData'])){ 
															foreach($bladeVar['userCartData'] as $cartList){
																$arrayCartList[] = $cartList->product_id;
															} 
														} 
														if (in_array($product->_id, $arrayCartList)) { ?>
															<button class="btn btn-cart addtocart disabled" id="addtocart{{ $product->_id }}" data-pid="{{ $product->_id }}">Go to cart</button>
														<?php }else{ ?>
															<button class="btn btn-cart addtocart" id="addtocart{{ $product->_id }}" data-pid="{{ $product->_id }}" data-cid="{{ $product->product_color_id }}" data-sid="{{ $product->product_size_id }}">add to cart</button>
														<?php } ?>
													</div>
													@endif
												@else
                                                    <div class="cart-hover">
                                                        <button class="btn btn-cart disabled">Out of stock</button>
                                                    </div>
                                                @endif 
											</figure>
											<div class="product-caption text-center">
												<div class="product-identity">
													<p class="manufacturer-name"><a href="{{ url('/shop/?category='.$product->product_category->_id) }}">{{ $product->product_category->name }}</a></p>
												</div>
												<?php
												$colorArray = array(); 
												$colorArray[0] = $product->product_color_id;
												if(!empty($product->variations)){
													$variations = json_decode($product->variations); 
													foreach($variations as $variation){
														$colorArray[] = $variation->color;
													}
												}
												$getColorArray = array_unique($colorArray);
												if(!empty($getColorArray)){ 
												?>
												<ul class="color-categories">
												<?php foreach($getColorArray as $color){ 
													$allColors = $bladeVar['color_results'];
													if(!empty($allColors)){
														foreach($allColors as $getColor){
															if($getColor->id == $color){ ?>
																<li>
																	<a href="#" style="background-color: <?php echo $getColor->color_code; ?>;" title="<?php echo $getColor->name; ?>"></a>
																</li>   
															<?php }
														}
													}
												} ?>
												</ul>
												<?php } ?>	
												<h6 class="product-name">
													<a href="{{ url('/shop/'.$product->slug) }}">{{ $product->name }}</a>
												</h6>
												<div class="price-box">
													@if(!empty($product->discounted_price) && ($product->discounted_price != '0.00'))
													<span class="price-regular">${{ $product->discounted_price }}</span>
													<span class="price-old"><del>${{ $product->price }}</del></span>
													@else
													<span class="price-regular">${{ $product->price }}</span>
													@endif
												</div>
											</div>
										</div>
										<!-- product item end -->
										@endif
										@endif
									@endforeach
								</div>
							</div>
							@endif
						@endforeach
						</div>
						<!-- product tab content end -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- product area end -->
	@endif
    @if (!empty($bladeVar['bannerslider_results']->count() > 0))
	<!-- product banner statistics area start -->
	<section class="product-banner-statistics">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="product-banner-carousel slick-row-10">
					    @foreach ($bladeVar['bannerslider_results'] as $bannerslider)
						<!-- banner single slide start -->
						<div class="banner-slide-item">
							<figure class="banner-statistics">
								<a href="{{ $bannerslider->redirect_url }}">
									<img src="{{ asset('images/cms').'/'.$bannerslider->image}}" alt="{{ $bannerslider->slug }}">
								</a>
								<div class="banner-content banner-content_style2">
									<h5 class="banner-text3"><a href="{{ $bannerslider->redirect_url }}">{{ $bannerslider->name }}</a></h5>
								</div>
							</figure>
						</div>
						<!-- banner single slide start -->
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- product banner statistics area end -->
    @endif
	@if (!empty($bladeVar['product_results']->count() > 0))
	<!-- featured product area start -->
	<section class="feature-product section-padding">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<!-- section title start -->
					<div class="section-title text-center">
						<h2 class="title">featured products</h2>
						<p class="sub-title">Add featured products to weekly lineup</p>
					</div>
					<!-- section title start -->
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="product-carousel-4_2 slick-row-10 slick-arrow-style">
						@foreach($bladeVar['product_results']  as $product)
							@if(($product->deleted_at == NULL) && ($product->featured == '1'))
							<!-- product item start -->
							<div class="product-item">
								<figure class="product-thumb">
									<a href="{{ url('/shop/'.$product->slug) }}">
										<img class="pri-img" src="{{ asset('images/products').'/'.$product->image}}" alt="{{ $product->slug }}">
										<img class="sec-img" src="{{ asset('images/products').'/'.$product->image}}" alt="{{ $product->slug }}">
									</a>
									<div class="product-badge">
										@if(($product->new_arrival == '1') && ($product->on_sale == '1'))
										<div class="product-label new">
											<span>new</span>
										</div>
										<div class="product-label discount">
											<span>sale</span>
										</div>
										@elseif($product->new_arrival == '1')
										<div class="product-label new">
											<span>new</span>
										</div>
										@elseif($product->on_sale == '1')
										<div class="product-label new">
											<span>sale</span>
										</div>
										@endif
									</div>
									<div class="button-group">
									<?php 
									$arrayWishlist = array();
									if(!empty($bladeVar['userWishlistData'])){ 
										foreach($bladeVar['userWishlistData'] as $wishlist){
											$arrayWishlist[] = $wishlist->product_id;
										} 
									} 
									if (in_array($product->_id, $arrayWishlist)) { ?>
										<a href="#" class="addtowishlist" id="addtowishlist{{ $product->_id }}" data-pid="{{ $product->_id }}" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like addWishlist"></i></a>
									<?php }else{ ?>
										<a href="#" class="addtowishlist" id="addtowishlist{{ $product->_id }}" data-pid="{{ $product->_id }}" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like"></i></a>
									<?php } ?>
									</div>
									@if(!empty($product->product_quantity) && ($product->product_quantity > 0))
										@if($product->add_to_cart == '1')
										<div class="cart-hover">
											<?php $arrayCartList = array();
											if(!empty($bladeVar['userCartData'])){ 
												foreach($bladeVar['userCartData'] as $cartList){
													$arrayCartList[] = $cartList->product_id;
												} 
											} 
											if (in_array($product->_id, $arrayCartList)) { ?>
												<button class="btn btn-cart addtocart disabled" id="addtocart{{ $product->_id }}" data-pid="{{ $product->_id }}">Go to cart</button>
											<?php }else{ ?>
												<button class="btn btn-cart addtocart" id="addtocart{{ $product->_id }}" data-pid="{{ $product->_id }}" data-cid="{{ $product->product_color_id }}" data-sid="{{ $product->product_size_id }}">add to cart</button>
											<?php } ?>
										</div>
										@endif
									@else
										<div class="cart-hover">
											<button class="btn btn-cart disabled">Out of stock</button>
										</div>
									@endif
								</figure>
								<div class="product-caption text-center">
									<div class="product-identity">
										<p class="manufacturer-name"><a href="{{ url('/shop/?category='.$product->product_category->_id) }}">{{ $product->product_category->name }}</a></p>
									</div>

									<?php
									$colorArray = array(); 
									$colorArray[0] = $product->product_color_id;
									if(!empty($product->variations)){
										$variations = json_decode($product->variations); 
										foreach($variations as $variation){
											$colorArray[] = $variation->color;
										}
									}
									$getColorArray = array_unique($colorArray);
									if(!empty($getColorArray)){ 
									?>
									<ul class="color-categories">
									<?php foreach($getColorArray as $color){ 
										$allColors = $bladeVar['color_results'];
										if(!empty($allColors)){
											foreach($allColors as $getColor){
												if($getColor->id == $color){ ?>
													<li>
														<a href="#" style="background-color: <?php echo $getColor->color_code; ?>;" title="<?php echo $getColor->name; ?>"></a>
													</li>   
												<?php }
											}
										}
									} ?>
									</ul>
									<?php } ?>	
									<h6 class="product-name">
										<a href="{{ url('/shop/'.$product->slug) }}">{{ $product->name }}</a>
									</h6>
									<div class="price-box">
										@if(!empty($product->discounted_price) && ($product->discounted_price != '0.00'))
										<span class="price-regular">${{ $product->discounted_price }}</span>
										<span class="price-old"><del>${{ $product->price }}</del></span>
										@else
										<span class="price-regular">${{ $product->price }}</span>
										@endif
									</div>
								</div>
							</div>
							<!-- product item end -->
							@endif
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- featured product area end -->
	@endif
	@if (!empty($bladeVar['testimonial_results']->count() > 0))
	<!-- testimonial area start -->
	<section class="testimonial-area section-padding bg-img" data-bg="assets/img/testimonial/testimonials-bg.jpg">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<!-- section title start -->
					<div class="section-title text-center">
						<h2 class="title">testimonials</h2>
						<p class="sub-title">What they say</p>
					</div>
					<!-- section title start -->
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="testimonial-thumb-wrapper">
						<div class="testimonial-thumb-carousel">
						    @foreach ($bladeVar['testimonial_results'] as $testimonial)
							<div class="testimonial-thumb">
								<img src="{{ asset('images/cms').'/'.$testimonial->image}}" alt="{{ $testimonial->slug }}">
							</div>
							@endforeach
						</div>
					</div>
					<div class="testimonial-content-wrapper">
						<div class="testimonial-content-carousel">
							@foreach ($bladeVar['testimonial_results'] as $testimonial)
							<div class="testimonial-content">
								<p>{{ $testimonial->shortDescription }}</p>
								<h5 class="testimonial-author">{{ $testimonial->name }}</h5>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- testimonial area end -->
	@endif
	@if (!empty($bladeVar['singleBanner_results']->count() > 0))
	<!-- group product start -->
	<section class="group-product-area section-padding">
		<div class="container">
			<div class="row">
			    @if (!empty($bladeVar['singleBanner_results']->count() > 0))
				    @php($count=0)
				    @foreach ($bladeVar['singleBanner_results'] as $singleBanner)
						@php($count++)
						@if(($count == '1') && ($singleBanner->deleted_at == NULL))
						<div class="col-lg-6">
							<div class="group-product-banner">
								<figure class="banner-statistics">
									<a href="{{ url('/shop/?category='.$singleBanner->product_category->_id) }}">
										<img src="{{ asset('images/cms').'/'.$singleBanner->image}}" alt="{{ $singleBanner->slug }}">
									</a>
									<div class="banner-content banner-content_style3 text-center">
										<h6 class="banner-text1">{{ $singleBanner->name }}</h6>
										<h2 class="banner-text2">{{ $singleBanner->shortDescription }}</h2>
										<a href="{{ url('/shop/?category='.$singleBanner->product_category->_id) }}" class="btn btn-text">{{ $singleBanner->btntext }}</a>
									</div>
								</figure>
							</div>
						</div>
						@endif
					@endforeach
                @endif

				<?php  
				$arrayOnsale = array();
				$arrayBestseller = array();
				foreach ($bladeVar['product_results'] as $product){
					if(($product->deleted_at == NULL) && ($product->on_sale == '1')){		
						$arrayOnsale[] = $product->_id;
					} 
					if(($product->deleted_at == NULL) && ($product->best_seller == '1')){		
						$arrayBestseller[] = $product->_id;
					}
				}
				?> 
				<?php if(!empty($arrayBestseller)){ ?>
				<div class="col-lg-3">
					<div class="categories-group-wrapper">
						<!-- section title start -->
						<div class="section-title-append">
							<h4>best seller product</h4>
							<div class="slick-append"></div>
						</div>
						<!-- section title start -->

						<!-- group list carousel start -->
						<div class="group-list-item-wrapper">
							<div class="group-list-carousel">
							@foreach($bladeVar['product_results']  as $product)
								@if(($product->deleted_at == NULL) && ($product->best_seller == '1'))
									<!-- group list item start -->
									<div class="group-slide-item">
										<div class="group-item">
											<div class="group-item-thumb">
												<a href="{{ url('/shop/'.$product->slug) }}">
													<img src="{{ asset('images/products').'/'.$product->image}}" alt="{{ $product->slug }}">
												</a>
											</div>
											<div class="group-item-desc">
												<h5 class="group-product-name">
												    <a href="{{ url('/shop/'.$product->slug) }}">{{ $product->name }}</a>
												</h5>
												<div class="price-box">
													@if(!empty($product->discounted_price) && ($product->discounted_price != '0.00'))
													<span class="price-regular">${{ $product->discounted_price }}</span>
													<span class="price-old"><del>${{ $product->price }}</del></span>
													@else
													<span class="price-regular">${{ $product->price }}</span>
													@endif
												</div>
											</div>
										</div>
									</div>
									<!-- group list item end -->
									@endif
							@endforeach
							</div>
						</div>
						<!-- group list carousel start -->
					</div>
				</div>
				<?php } ?>
				<?php if(!empty($arrayOnsale)){ ?>
				<div class="col-lg-3">
					<div class="categories-group-wrapper">
						<!-- section title start -->
						<div class="section-title-append">
							<h4>on-sale product</h4>
							<div class="slick-append"></div>
						</div>
						<!-- section title start -->

						<!-- group list carousel start -->
						<div class="group-list-item-wrapper">
							<div class="group-list-carousel">
								@foreach($bladeVar['product_results']  as $product)
									@if(($product->deleted_at == NULL) && ($product->on_sale == '1'))
										<!-- group list item start -->
										<div class="group-slide-item">
											<div class="group-item">
												<div class="group-item-thumb">
													<a href="{{ url('/shop/'.$product->slug) }}">
														<img src="{{ asset('images/products').'/'.$product->image}}" alt="{{ $product->slug }}">
													</a>
												</div>
												<div class="group-item-desc">
													<h5 class="group-product-name">
														<a href="{{ url('/shop/'.$product->slug) }}">{{ $product->name }}</a>
													</h5>
													<div class="price-box">
														@if(!empty($product->discounted_price) && ($product->discounted_price != '0.00'))
														<span class="price-regular">${{ $product->discounted_price }}</span>
														<span class="price-old"><del>${{ $product->price }}</del></span>
														@else
														<span class="price-regular">${{ $product->price }}</span>
														@endif
													</div>
												</div>
											</div>
										</div>
										<!-- group list item end -->
										@endif
								@endforeach
							</div>
						</div>
						<!-- group list carousel start -->
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</section>
	<!-- group product end -->
    @endif
	@if (!empty($bladeVar['blog_results']->count() > 0))
	<!-- latest blog area start -->
	<section class="latest-blog-area section-padding pt-0">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<!-- section title start -->
					<div class="section-title text-center">
						<h2 class="title">latest blogs</h2>
						<p class="sub-title">There are latest blog posts</p>
					</div>
					<!-- section title start -->
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="blog-carousel-active slick-row-10 slick-arrow-style">
					    @foreach ($bladeVar['blog_results'] as $blog)
							@if($blog->deleted_at == NULL)
							<!-- blog post item start -->
							<div class="blog-post-item">
								<figure class="blog-thumb">
									<a href="{{ url('/blog/'.$blog->slug) }}">
										<img src="{{ asset('images/blogs').'/'.$blog->image}}" alt="{{ $blog->slug }}">
									</a>
								</figure>
								<div class="blog-content">
									<div class="blog-meta">
										<p>{{ date('d/m/Y', strtotime($blog->updated_at)) }}</p>
									</div>
									<h5 class="blog-title">
										<a href="{{ url('/blog/'.$blog->slug) }}">{{ $blog->name }}</a>
									</h5>
								</div>
							</div>
							<!-- blog post item end -->
							@endif
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</section>
	@endif
	<!-- latest blog area end -->
    @if (!empty($bladeVar['brand_results']->count() > 0))
	<!-- brand logo area start -->
	<div class="brand-logo section-padding pt-0">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="brand-logo-carousel slick-row-10 slick-arrow-style">
						@foreach ($bladeVar['brand_results'] as $brand)
						<!-- single brand start -->
						<div class="brand-item">
							<a href="#">
								<img src="{{ asset('images/brands').'/'.$brand->image}}" alt="{{ $brand->name }}">
							</a>
						</div>
						<!-- single brand end -->
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- brand logo area end -->
	@endif
</main>
@include('frontend.footer')