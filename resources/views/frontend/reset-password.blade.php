@include('frontend.header')
<style type="text/css">
    .has-error {
        border: 1px solid red !important;
    }
    #usernameError,
    #emailError,
    #passwordError,
    #con_passwordError {
        color: red;
    }
    #responseMessage, #responseMSG, #resMessage{
        margin-top: 8px;
    }
</style>
    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Reset Password</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->
        <!-- login register wrapper start -->
        <div class="login-register-wrapper section-padding">
            <div class="container">
                <div class="member-area-from-wrap">
                    <div class="row">
                        <!-- Reset Content Start -->
                        <div class="col-lg-3"></div>
                        <div class="col-lg-6">
                            <div class="login-reg-form-wrap sign-in-form">
                                <h5>Recover Password</h5>
                                <p class="login-box-msg" id="responseMessage"></p>
                                <form action="#" method="post" id="userforgotForm">
                                    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" id="email" name="email" value="<?php echo base64_decode($_GET['hash']); ?>" />
                                    <div class="single-input-item">
                                        <input type="password" id="password" name="password" placeholder="Enter New Password" />
                                        <span class="error-text" id="passwordError"></span>
                                    </div>
                                    <div class="single-input-item">
                                        <input type="password" id="con_password" name="con_password" placeholder="Enter Confirm Password" />
                                        <span class="error-text" id="con_passwordError"></span>
                                    </div>
                                    <div class="single-input-item">
                                        <button class="btn btn-sqr" id="resetpasswrod">Change password</button>
                                        <img src="{{ asset('images/ajax-loader.gif') }}" id="ajax-loader" style="display:none; margin-left: 10px; height: 40px;">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-3"></div>
                        <!-- Reset Content End -->
                    </div>
                </div>
            </div>
        </div>
        <!-- login register wrapper end -->
    </main>
@include('frontend.footer')
<script type="text/javascript">
$(document).ready(function() {
    var baseUrl = '{{ url("/") }}';
	function isEmail(emailid) {
		var pattern = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
		return pattern.test(emailid);
	}

    $('#userforgotForm #password').focusout(function() {
        var passwordValue = $(this).val();
        if (passwordValue == '') {
            $('#userforgotForm #password').addClass('has-error');
            $('#userforgotForm #passwordError').show().text('Password empty or invalid');
        } else if (passwordValue.length < 6) {
            $('#userforgotForm #password').addClass('has-error');
            $('#userforgotForm #passwordError').show().text('Password must be at least 6 characters');
        } else if (passwordValue != '') {
            $('#userforgotForm #password').removeClass('has-error');
            $('#userforgotForm #passwordError').hide().text('');
        }
    });

    $('#userforgotForm #con_password').focusout(function() {
        var passwordValue = $('#userforgotForm #password').val();  
        var conpasswordValue = $(this).val();
        if (conpasswordValue == '') {
            $('#userforgotForm #con_password').addClass('has-error');
            $('#userforgotForm #con_passwordError').show().text('Confirm Password empty or invalid');
        } else if (conpasswordValue.length < 6) {
            $('#userforgotForm #con_password').addClass('has-error');
            $('#userforgotForm #con_passwordError').show().text('Confirm Password must be at least 6 characters');
        }else if (conpasswordValue != passwordValue) {
            $('#userforgotForm #con_password').addClass('has-error');
            $('#userforgotForm #con_passwordError').show().text('Your password and confirmation password do not match');
        } else if (conpasswordValue != '') {
            $('#userforgotForm #con_password').removeClass('has-error');
            $('#userforgotForm #con_passwordError').hide().text('');
        }
    });

    $('#resetpasswrod').on('click', function(event) {
        event.preventDefault();
        $('#responseMessage').html('');
        $('#userforgotForm #password').trigger("focusout");
        $('#userforgotForm #con_password').trigger("focusout");
        var passwordValue = $("#userforgotForm #password").val();
        var con_password = $("#userforgotForm #con_password").val();
        if (passwordValue == con_password)  {
            if ((passwordValue != '') && (con_password != '') && (passwordValue.length >= 6)) {
                $('#userforgotForm #password').removeClass('has-error');
                $('#userforgotForm #passwordError').text('').hide();
                update_password();
            }
        }else{
            $('#userforgotForm #con_password').addClass('has-error');
            $('#userforgotForm #con_passwordError').show().text('Confirm Password not match');
        }
    });

    function update_password() {
        var token = $('#userforgotForm #_token').val();
        $('#userforgotForm #ajax-loader').show();
        $.ajax({
            url: baseUrl + "/userRecoverPassword",
            type: "POST",
            data: {
                '_token': token,
                'email': $('#userforgotForm #email').val(),
                'password': $('#userforgotForm #password').val(),
                'updatepassword': 'Update Password', 
            },
            dataType: "JSON",
            success: function(jsonStr) {
                var res_data = JSON.stringify(jsonStr);
                var response = JSON.parse(res_data);
                var responseData = response['responseData'];
                var status = response['status'];
                if (status == 'success') {
                     $('#responseMessage').html('Your password update successfully');
                    setTimeout(function(){ window.location.href = baseUrl+'/login-register';  }, 1500);
                }else {
                    $('#responseMessage').html('Somthing went wrong');
                }
                $('#userforgotForm #ajax-loader').hide();
            }
        });
    } 
});
</script>