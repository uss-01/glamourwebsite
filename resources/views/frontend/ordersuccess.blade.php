@include('frontend.header')
    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/account') }}">my account</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- wishlist main wrapper start -->
        <div class="wishlist-main-wrapper section-padding">
            <div class="container">
                <!-- Wishlist Page Content Start -->
                <div class="section-bg-color">
                    <div class="row">
                        <div class="col-lg-12" id="wishlistItem">
                            <!-- Wishlist Table Area -->
                            @if ($message = Session::get('success'))
                                <h3 style="text-align: center;">{!! $message !!}</h3>
                                <p style="text-align: center;">Your order has been placed successfully</p>
                            <?php Session::forget('success');?>
                            @else
                                <h3 style="text-align: center;">Order Placed</h3>
                                <p style="text-align: center;">Your order has been placed successfully</p>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- Wishlist Page Content End -->
            </div>
        </div>
        <!-- wishlist main wrapper end -->
    </main>
@include('frontend.footer')  