@include('frontend.header')
<style type="text/css">
    .has-error {
        border: 1px solid red !important;
    }
    #couponCodeError {
        color: red;
    }
    #responseMessage{
        margin-top: 8px;
    }
    .error-text.success{
        color: #000 !important;
    }
    .error-text {
        color: red;
    }
    .couponDis{
        display : none;
    }
    .terms::before{
        border: 1px solid red !important;
    } 

</style>
    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/shop') }}">shop</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">checkout</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- checkout main wrapper start -->
        <div class="checkout-page-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <!-- Checkout Login Coupon Accordion Start -->
                        <div class="checkoutaccordion" id="checkOutAccordion">
                            <?php if (Session::has('userLoggedIn')){ ?>
                            <?php }else{ ?>
                                <div class="card">
                                    <h6>Returning Customer? <span data-toggle="collapse" data-target="#logInaccordion">Click Here To Login</span></h6>
                                    <div id="logInaccordion" class="collapse" data-parent="#checkOutAccordion">
                                        <div class="card-body">
                                            <div class="login-reg-form-wrap mt-20">
                                                <div class="row">
                                                    <div class="col-lg-7 m-auto">
                                                       <p class="login-box-msg" id="responseMessage"></p>
                                                        <form action="#" method="post" id="userLoginForm">
                                                            <div class="row">
                                                                <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                                                                <div class="col-md-12">
                                                                    <div class="single-input-item">
                                                                        <input type="email" id="email" name="email" placeholder="Enter your Email" />
                                                                        <span class="error-text" id="emailError"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="single-input-item">
                                                                        <input type="password" id="password" name="password" placeholder="Enter your Password" />
                                                                        <span class="error-text" id="passwordError"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="single-input-item">
                                                                <div class="login-reg-form-meta d-flex align-items-center justify-content-between">
                                                                    <div class="remember-meta">
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="rememberMe">
                                                                            <label class="custom-control-label" for="rememberMe">Remember Me</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="single-input-item">
                                                                <button class="btn btn-sqr" id="userLogin">Login</button>
                                                                <img src="{{ asset('images/ajax-loader.gif') }}" id="ajax-loader" style="display:none; margin-left: 10px; height: 40px;">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>   
                            <?php } ?>
                            <div class="card">
                                <h6>Have A Coupon? <span data-toggle="collapse" data-target="#couponaccordion">Click Here To Enter Your Code</span></h6>
                                <div id="couponaccordion" class="collapse" data-parent="#checkOutAccordion">
                                    <div class="card-body">
                                        <div class="cart-update-option">
                                            <div class="apply-coupon-wrapper">
                                                <form id="couponForm" action="#" method="post" class=" d-block d-md-flex">
                                                    <input type="text" id="couponCode" name="couponCode" placeholder="Enter Your Coupon Code" />
                                                    <button class="btn btn-sqr" id="applyCoupon">Apply Coupon</button>
                                                </form>
                                                <span class="error-text" id="couponCodeError"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Checkout Login Coupon Accordion End -->
                    </div>
                </div>
                <div class="row">
                    <!-- Checkout Billing Details -->
                    <div class="col-lg-6">
                        <div class="checkout-billing-details-wrap">
                            <h5 class="checkout-title">Billing Details</h5>
                            <div class="billing-form-wrap">
                                <form action="#" method="post" id="orderForm">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="single-input-item">
                                                <label for="fname" class="required">First Name</label>
                                                <input type="text" id="fname" name="fname" <?php if(!empty($bladeVar['userDetails']->fname)){ ?>value="{{ $bladeVar['userDetails']->fname }}"<?php } ?> placeholder="First Name" />
                                                <span class="error-text" id="fnameError"></span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="single-input-item">
                                                <label for="lname" class="required">Last Name</label>
                                                <input type="text" id="lname" name="lname" <?php if(!empty($bladeVar['userDetails']->lname)){ ?>value="{{ $bladeVar['userDetails']->lname }}"<?php } ?> placeholder="Last Name" />
                                                <span class="error-text" id="lnameError"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="single-input-item">
                                        <label for="email" class="required">Email Address</label>
                                        <input type="email" id="email" name="email" <?php if(!empty($bladeVar['userDetails']->email)){ ?>value="{{ $bladeVar['userDetails']->email }}"<?php } ?> placeholder="Email Address" />
                                        <span class="error-text" id="emailError"></span>
                                    </div>

                                    <div class="single-input-item">
                                        <label for="country" class="required">Country</label>
                                        <select name="country nice-select" id="country">
                                            <option value="">Select Country</option>
                                            <?php if(!empty($bladeVar['userDetails']->country)){ ?><option value="{{ $bladeVar['userDetails']->country }}" selected>{{ $bladeVar['userDetails']->country }}</option><?php } ?>
                                            <option value="Afganistan">Afghanistan</option>
                                            <option value="Albania">Albania</option>
                                            <option value="Algeria">Algeria</option>
                                            <option value="American Samoa">American Samoa</option>
                                            <option value="Andorra">Andorra</option>
                                            <option value="Angola">Angola</option>
                                            <option value="Anguilla">Anguilla</option>
                                            <option value="Antigua & Barbuda">Antigua & Barbuda</option>
                                            <option value="Argentina">Argentina</option>
                                            <option value="Armenia">Armenia</option>
                                            <option value="Aruba">Aruba</option>
                                            <option value="Australia">Australia</option>
                                            <option value="Austria">Austria</option>
                                            <option value="Azerbaijan">Azerbaijan</option>
                                            <option value="Bahamas">Bahamas</option>
                                            <option value="Bahrain">Bahrain</option>
                                            <option value="Bangladesh">Bangladesh</option>
                                            <option value="Barbados">Barbados</option>
                                            <option value="Belarus">Belarus</option>
                                            <option value="Belgium">Belgium</option>
                                            <option value="Belize">Belize</option>
                                            <option value="Benin">Benin</option>
                                            <option value="Bermuda">Bermuda</option>
                                            <option value="Bhutan">Bhutan</option>
                                            <option value="Bolivia">Bolivia</option>
                                            <option value="Bonaire">Bonaire</option>
                                            <option value="Bosnia & Herzegovina">Bosnia & Herzegovina</option>
                                            <option value="Botswana">Botswana</option>
                                            <option value="Brazil">Brazil</option>
                                            <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                                            <option value="Brunei">Brunei</option>
                                            <option value="Bulgaria">Bulgaria</option>
                                            <option value="Burkina Faso">Burkina Faso</option>
                                            <option value="Burundi">Burundi</option>
                                            <option value="Cambodia">Cambodia</option>
                                            <option value="Cameroon">Cameroon</option>
                                            <option value="Canada">Canada</option>
                                            <option value="Canary Islands">Canary Islands</option>
                                            <option value="Cape Verde">Cape Verde</option>
                                            <option value="Cayman Islands">Cayman Islands</option>
                                            <option value="Central African Republic">Central African Republic</option>
                                            <option value="Chad">Chad</option>
                                            <option value="Channel Islands">Channel Islands</option>
                                            <option value="Chile">Chile</option>
                                            <option value="China">China</option>
                                            <option value="Christmas Island">Christmas Island</option>
                                            <option value="Cocos Island">Cocos Island</option>
                                            <option value="Colombia">Colombia</option>
                                            <option value="Comoros">Comoros</option>
                                            <option value="Congo">Congo</option>
                                            <option value="Cook Islands">Cook Islands</option>
                                            <option value="Costa Rica">Costa Rica</option>
                                            <option value="Cote DIvoire">Cote DIvoire</option>
                                            <option value="Croatia">Croatia</option>
                                            <option value="Cuba">Cuba</option>
                                            <option value="Curaco">Curacao</option>
                                            <option value="Cyprus">Cyprus</option>
                                            <option value="Czech Republic">Czech Republic</option>
                                            <option value="Denmark">Denmark</option>
                                            <option value="Djibouti">Djibouti</option>
                                            <option value="Dominica">Dominica</option>
                                            <option value="Dominican Republic">Dominican Republic</option>
                                            <option value="East Timor">East Timor</option>
                                            <option value="Ecuador">Ecuador</option>
                                            <option value="Egypt">Egypt</option>
                                            <option value="El Salvador">El Salvador</option>
                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                            <option value="Eritrea">Eritrea</option>
                                            <option value="Estonia">Estonia</option>
                                            <option value="Ethiopia">Ethiopia</option>
                                            <option value="Falkland Islands">Falkland Islands</option>
                                            <option value="Faroe Islands">Faroe Islands</option>
                                            <option value="Fiji">Fiji</option>
                                            <option value="Finland">Finland</option>
                                            <option value="France">France</option>
                                            <option value="French Guiana">French Guiana</option>
                                            <option value="French Polynesia">French Polynesia</option>
                                            <option value="French Southern Ter">French Southern Ter</option>
                                            <option value="Gabon">Gabon</option>
                                            <option value="Gambia">Gambia</option>
                                            <option value="Georgia">Georgia</option>
                                            <option value="Germany">Germany</option>
                                            <option value="Ghana">Ghana</option>
                                            <option value="Gibraltar">Gibraltar</option>
                                            <option value="Great Britain">Great Britain</option>
                                            <option value="Greece">Greece</option>
                                            <option value="Greenland">Greenland</option>
                                            <option value="Grenada">Grenada</option>
                                            <option value="Guadeloupe">Guadeloupe</option>
                                            <option value="Guam">Guam</option>
                                            <option value="Guatemala">Guatemala</option>
                                            <option value="Guinea">Guinea</option>
                                            <option value="Guyana">Guyana</option>
                                            <option value="Haiti">Haiti</option>
                                            <option value="Hawaii">Hawaii</option>
                                            <option value="Honduras">Honduras</option>
                                            <option value="Hong Kong">Hong Kong</option>
                                            <option value="Hungary">Hungary</option>
                                            <option value="Iceland">Iceland</option>
                                            <option value="Indonesia">Indonesia</option>
                                            <option value="India">India</option>
                                            <option value="Iran">Iran</option>
                                            <option value="Iraq">Iraq</option>
                                            <option value="Ireland">Ireland</option>
                                            <option value="Isle of Man">Isle of Man</option>
                                            <option value="Israel">Israel</option>
                                            <option value="Italy">Italy</option>
                                            <option value="Jamaica">Jamaica</option>
                                            <option value="Japan">Japan</option>
                                            <option value="Jordan">Jordan</option>
                                            <option value="Kazakhstan">Kazakhstan</option>
                                            <option value="Kenya">Kenya</option>
                                            <option value="Kiribati">Kiribati</option>
                                            <option value="Korea North">Korea North</option>
                                            <option value="Korea Sout">Korea South</option>
                                            <option value="Kuwait">Kuwait</option>
                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                            <option value="Laos">Laos</option>
                                            <option value="Latvia">Latvia</option>
                                            <option value="Lebanon">Lebanon</option>
                                            <option value="Lesotho">Lesotho</option>
                                            <option value="Liberia">Liberia</option>
                                            <option value="Libya">Libya</option>
                                            <option value="Liechtenstein">Liechtenstein</option>
                                            <option value="Lithuania">Lithuania</option>
                                            <option value="Luxembourg">Luxembourg</option>
                                            <option value="Macau">Macau</option>
                                            <option value="Macedonia">Macedonia</option>
                                            <option value="Madagascar">Madagascar</option>
                                            <option value="Malaysia">Malaysia</option>
                                            <option value="Malawi">Malawi</option>
                                            <option value="Maldives">Maldives</option>
                                            <option value="Mali">Mali</option>
                                            <option value="Malta">Malta</option>
                                            <option value="Marshall Islands">Marshall Islands</option>
                                            <option value="Martinique">Martinique</option>
                                            <option value="Mauritania">Mauritania</option>
                                            <option value="Mauritius">Mauritius</option>
                                            <option value="Mayotte">Mayotte</option>
                                            <option value="Mexico">Mexico</option>
                                            <option value="Midway Islands">Midway Islands</option>
                                            <option value="Moldova">Moldova</option>
                                            <option value="Monaco">Monaco</option>
                                            <option value="Mongolia">Mongolia</option>
                                            <option value="Montserrat">Montserrat</option>
                                            <option value="Morocco">Morocco</option>
                                            <option value="Mozambique">Mozambique</option>
                                            <option value="Myanmar">Myanmar</option>
                                            <option value="Nambia">Nambia</option>
                                            <option value="Nauru">Nauru</option>
                                            <option value="Nepal">Nepal</option>
                                            <option value="Netherland Antilles">Netherland Antilles</option>
                                            <option value="Netherlands">Netherlands (Holland, Europe)</option>
                                            <option value="Nevis">Nevis</option>
                                            <option value="New Caledonia">New Caledonia</option>
                                            <option value="New Zealand">New Zealand</option>
                                            <option value="Nicaragua">Nicaragua</option>
                                            <option value="Niger">Niger</option>
                                            <option value="Nigeria">Nigeria</option>
                                            <option value="Niue">Niue</option>
                                            <option value="Norfolk Island">Norfolk Island</option>
                                            <option value="Norway">Norway</option>
                                            <option value="Oman">Oman</option>
                                            <option value="Pakistan">Pakistan</option>
                                            <option value="Palau Island">Palau Island</option>
                                            <option value="Palestine">Palestine</option>
                                            <option value="Panama">Panama</option>
                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                            <option value="Paraguay">Paraguay</option>
                                            <option value="Peru">Peru</option>
                                            <option value="Phillipines">Philippines</option>
                                            <option value="Pitcairn Island">Pitcairn Island</option>
                                            <option value="Poland">Poland</option>
                                            <option value="Portugal">Portugal</option>
                                            <option value="Puerto Rico">Puerto Rico</option>
                                            <option value="Qatar">Qatar</option>
                                            <option value="Republic of Montenegro">Republic of Montenegro</option>
                                            <option value="Republic of Serbia">Republic of Serbia</option>
                                            <option value="Reunion">Reunion</option>
                                            <option value="Romania">Romania</option>
                                            <option value="Russia">Russia</option>
                                            <option value="Rwanda">Rwanda</option>
                                            <option value="St Barthelemy">St Barthelemy</option>
                                            <option value="St Eustatius">St Eustatius</option>
                                            <option value="St Helena">St Helena</option>
                                            <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                                            <option value="St Lucia">St Lucia</option>
                                            <option value="St Maarten">St Maarten</option>
                                            <option value="St Pierre & Miquelon">St Pierre & Miquelon</option>
                                            <option value="St Vincent & Grenadines">St Vincent & Grenadines</option>
                                            <option value="Saipan">Saipan</option>
                                            <option value="Samoa">Samoa</option>
                                            <option value="Samoa American">Samoa American</option>
                                            <option value="San Marino">San Marino</option>
                                            <option value="Sao Tome & Principe">Sao Tome & Principe</option>
                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                            <option value="Senegal">Senegal</option>
                                            <option value="Seychelles">Seychelles</option>
                                            <option value="Sierra Leone">Sierra Leone</option>
                                            <option value="Singapore">Singapore</option>
                                            <option value="Slovakia">Slovakia</option>
                                            <option value="Slovenia">Slovenia</option>
                                            <option value="Solomon Islands">Solomon Islands</option>
                                            <option value="Somalia">Somalia</option>
                                            <option value="South Africa">South Africa</option>
                                            <option value="Spain">Spain</option>
                                            <option value="Sri Lanka">Sri Lanka</option>
                                            <option value="Sudan">Sudan</option>
                                            <option value="Suriname">Suriname</option>
                                            <option value="Swaziland">Swaziland</option>
                                            <option value="Sweden">Sweden</option>
                                            <option value="Switzerland">Switzerland</option>
                                            <option value="Syria">Syria</option>
                                            <option value="Tahiti">Tahiti</option>
                                            <option value="Taiwan">Taiwan</option>
                                            <option value="Tajikistan">Tajikistan</option>
                                            <option value="Tanzania">Tanzania</option>
                                            <option value="Thailand">Thailand</option>
                                            <option value="Togo">Togo</option>
                                            <option value="Tokelau">Tokelau</option>
                                            <option value="Tonga">Tonga</option>
                                            <option value="Trinidad & Tobago">Trinidad & Tobago</option>
                                            <option value="Tunisia">Tunisia</option>
                                            <option value="Turkey">Turkey</option>
                                            <option value="Turkmenistan">Turkmenistan</option>
                                            <option value="Turks & Caicos Is">Turks & Caicos Is</option>
                                            <option value="Tuvalu">Tuvalu</option>
                                            <option value="Uganda">Uganda</option>
                                            <option value="United Kingdom">United Kingdom</option>
                                            <option value="Ukraine">Ukraine</option>
                                            <option value="United Arab Erimates">United Arab Emirates</option>
                                            <option value="United States of America">United States of America</option>
                                            <option value="Uraguay">Uruguay</option>
                                            <option value="Uzbekistan">Uzbekistan</option>
                                            <option value="Vanuatu">Vanuatu</option>
                                            <option value="Vatican City State">Vatican City State</option>
                                            <option value="Venezuela">Venezuela</option>
                                            <option value="Vietnam">Vietnam</option>
                                            <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                                            <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                                            <option value="Wake Island">Wake Island</option>
                                            <option value="Wallis & Futana Is">Wallis & Futana Is</option>
                                            <option value="Yemen">Yemen</option>
                                            <option value="Zaire">Zaire</option>
                                            <option value="Zambia">Zambia</option>
                                            <option value="Zimbabwe">Zimbabwe</option>  
                                        </select>
                                        <span class="error-text" id="countryError" style="display: block;"></span>
                                    </div>

                                    <div class="single-input-item">
                                        <label for="streetAddress" class="required mt-20">Street address</label>
                                        <input type="text" id="streetAddress" name="streetAddress" <?php if(!empty($bladeVar['userDetails']->streetAddress)){ ?>value="{{ $bladeVar['userDetails']->streetAddress }}"<?php } ?> placeholder="Street address Line 1" />
                                        <span class="error-text" id="streetAddressError"></span>
                                    </div>

                                    <div class="single-input-item">
                                        <input type="text" id="streetAddress1" name="streetAddress1" <?php if(!empty($bladeVar['userDetails']->streetAddress1)){ ?>value="{{ $bladeVar['userDetails']->streetAddress1 }}"<?php } ?> placeholder="Street address Line 2 (Optional)">
                                    </div>

                                    <div class="single-input-item">
                                        <label for="city" class="required">Town / City</label>
                                        <input type="text" id="city" name="city" <?php if(!empty($bladeVar['userDetails']->city)){ ?>value="{{ $bladeVar['userDetails']->city }}"<?php } ?> placeholder="Town / City" />
                                        <span class="error-text" id="cityError"></span>
                                    </div>

                                    <div class="single-input-item">
                                        <label for="state">State / Divition</label>
                                        <input type="text" id="state" name="state" <?php if(!empty($bladeVar['userDetails']->state)){ ?>value="{{ $bladeVar['userDetails']->state }}"<?php } ?> placeholder="State / Divition" />
                                    </div>

                                    <div class="single-input-item">
                                        <label for="postcode" class="required">Postcode / ZIP</label>
                                        <input type="text" id="postcode" name="postcode" <?php if(!empty($bladeVar['userDetails']->postcode)){ ?>value="{{ $bladeVar['userDetails']->postcode }}"<?php } ?> placeholder="Postcode / ZIP" />
                                        <span class="error-text" id="postcodeError"></span>  
                                    </div>

                                    <div class="single-input-item">
                                        <label for="phone">Phone</label>
                                        <input type="text" id="phone" name="phone" <?php if(!empty($bladeVar['userDetails']->phone)){ ?>value="{{ $bladeVar['userDetails']->phone }}"<?php } ?> placeholder="Phone" />
                                    </div>
                                    
                                    <?php if (Session::has('userLoggedIn')){ ?>
                                    <?php }else{ ?>
                                    <?php } ?>
                                    <div class="checkout-box-wrap">
                                        <div class="single-input-item">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="shipToDifferent" name="shipToDifferent">
                                                <label class="custom-control-label" for="shipToDifferent">Ship to a different address?</label>
                                            </div>
                                        </div>
                                        <div class="shipToDifferent single-form-row">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="single-input-item">
                                                        <label for="f_name_2" class="required">First Name</label>
                                                        <input type="text" id="f_name_2" name="f_name_2" placeholder="First Name" />
                                                        <span class="error-text" id="f_name_2Error"></span>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="single-input-item">
                                                        <label for="l_name_2" class="required">Last Name</label>
                                                        <input type="text" id="l_name_2" name="l_name_2" placeholder="Last Name" />
                                                        <span class="error-text" id="l_name_2Error"></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="single-input-item">
                                                <label for="email_2" class="required">Email Address</label>
                                                <input type="email" id="email_2" name="email_2" placeholder="Email Address" />
                                                <span class="error-text" id="email_2Error"></span>
                                            </div>

                                            <div class="single-input-item">
                                                <label for="country_2" class="required">Country</label>
                                                <select name="country_2" id="country_2">
                                                    <option value="">Select Country</option>
                                                    <option value="Afganistan">Afghanistan</option>
                                                    <option value="Albania">Albania</option>
                                                    <option value="Algeria">Algeria</option>
                                                    <option value="American Samoa">American Samoa</option>
                                                    <option value="Andorra">Andorra</option>
                                                    <option value="Angola">Angola</option>
                                                    <option value="Anguilla">Anguilla</option>
                                                    <option value="Antigua & Barbuda">Antigua & Barbuda</option>
                                                    <option value="Argentina">Argentina</option>
                                                    <option value="Armenia">Armenia</option>
                                                    <option value="Aruba">Aruba</option>
                                                    <option value="Australia">Australia</option>
                                                    <option value="Austria">Austria</option>
                                                    <option value="Azerbaijan">Azerbaijan</option>
                                                    <option value="Bahamas">Bahamas</option>
                                                    <option value="Bahrain">Bahrain</option>
                                                    <option value="Bangladesh">Bangladesh</option>
                                                    <option value="Barbados">Barbados</option>
                                                    <option value="Belarus">Belarus</option>
                                                    <option value="Belgium">Belgium</option>
                                                    <option value="Belize">Belize</option>
                                                    <option value="Benin">Benin</option>
                                                    <option value="Bermuda">Bermuda</option>
                                                    <option value="Bhutan">Bhutan</option>
                                                    <option value="Bolivia">Bolivia</option>
                                                    <option value="Bonaire">Bonaire</option>
                                                    <option value="Bosnia & Herzegovina">Bosnia & Herzegovina</option>
                                                    <option value="Botswana">Botswana</option>
                                                    <option value="Brazil">Brazil</option>
                                                    <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                                                    <option value="Brunei">Brunei</option>
                                                    <option value="Bulgaria">Bulgaria</option>
                                                    <option value="Burkina Faso">Burkina Faso</option>
                                                    <option value="Burundi">Burundi</option>
                                                    <option value="Cambodia">Cambodia</option>
                                                    <option value="Cameroon">Cameroon</option>
                                                    <option value="Canada">Canada</option>
                                                    <option value="Canary Islands">Canary Islands</option>
                                                    <option value="Cape Verde">Cape Verde</option>
                                                    <option value="Cayman Islands">Cayman Islands</option>
                                                    <option value="Central African Republic">Central African Republic</option>
                                                    <option value="Chad">Chad</option>
                                                    <option value="Channel Islands">Channel Islands</option>
                                                    <option value="Chile">Chile</option>
                                                    <option value="China">China</option>
                                                    <option value="Christmas Island">Christmas Island</option>
                                                    <option value="Cocos Island">Cocos Island</option>
                                                    <option value="Colombia">Colombia</option>
                                                    <option value="Comoros">Comoros</option>
                                                    <option value="Congo">Congo</option>
                                                    <option value="Cook Islands">Cook Islands</option>
                                                    <option value="Costa Rica">Costa Rica</option>
                                                    <option value="Cote DIvoire">Cote DIvoire</option>
                                                    <option value="Croatia">Croatia</option>
                                                    <option value="Cuba">Cuba</option>
                                                    <option value="Curaco">Curacao</option>
                                                    <option value="Cyprus">Cyprus</option>
                                                    <option value="Czech Republic">Czech Republic</option>
                                                    <option value="Denmark">Denmark</option>
                                                    <option value="Djibouti">Djibouti</option>
                                                    <option value="Dominica">Dominica</option>
                                                    <option value="Dominican Republic">Dominican Republic</option>
                                                    <option value="East Timor">East Timor</option>
                                                    <option value="Ecuador">Ecuador</option>
                                                    <option value="Egypt">Egypt</option>
                                                    <option value="El Salvador">El Salvador</option>
                                                    <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                    <option value="Eritrea">Eritrea</option>
                                                    <option value="Estonia">Estonia</option>
                                                    <option value="Ethiopia">Ethiopia</option>
                                                    <option value="Falkland Islands">Falkland Islands</option>
                                                    <option value="Faroe Islands">Faroe Islands</option>
                                                    <option value="Fiji">Fiji</option>
                                                    <option value="Finland">Finland</option>
                                                    <option value="France">France</option>
                                                    <option value="French Guiana">French Guiana</option>
                                                    <option value="French Polynesia">French Polynesia</option>
                                                    <option value="French Southern Ter">French Southern Ter</option>
                                                    <option value="Gabon">Gabon</option>
                                                    <option value="Gambia">Gambia</option>
                                                    <option value="Georgia">Georgia</option>
                                                    <option value="Germany">Germany</option>
                                                    <option value="Ghana">Ghana</option>
                                                    <option value="Gibraltar">Gibraltar</option>
                                                    <option value="Great Britain">Great Britain</option>
                                                    <option value="Greece">Greece</option>
                                                    <option value="Greenland">Greenland</option>
                                                    <option value="Grenada">Grenada</option>
                                                    <option value="Guadeloupe">Guadeloupe</option>
                                                    <option value="Guam">Guam</option>
                                                    <option value="Guatemala">Guatemala</option>
                                                    <option value="Guinea">Guinea</option>
                                                    <option value="Guyana">Guyana</option>
                                                    <option value="Haiti">Haiti</option>
                                                    <option value="Hawaii">Hawaii</option>
                                                    <option value="Honduras">Honduras</option>
                                                    <option value="Hong Kong">Hong Kong</option>
                                                    <option value="Hungary">Hungary</option>
                                                    <option value="Iceland">Iceland</option>
                                                    <option value="Indonesia">Indonesia</option>
                                                    <option value="India">India</option>
                                                    <option value="Iran">Iran</option>
                                                    <option value="Iraq">Iraq</option>
                                                    <option value="Ireland">Ireland</option>
                                                    <option value="Isle of Man">Isle of Man</option>
                                                    <option value="Israel">Israel</option>
                                                    <option value="Italy">Italy</option>
                                                    <option value="Jamaica">Jamaica</option>
                                                    <option value="Japan">Japan</option>
                                                    <option value="Jordan">Jordan</option>
                                                    <option value="Kazakhstan">Kazakhstan</option>
                                                    <option value="Kenya">Kenya</option>
                                                    <option value="Kiribati">Kiribati</option>
                                                    <option value="Korea North">Korea North</option>
                                                    <option value="Korea Sout">Korea South</option>
                                                    <option value="Kuwait">Kuwait</option>
                                                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                    <option value="Laos">Laos</option>
                                                    <option value="Latvia">Latvia</option>
                                                    <option value="Lebanon">Lebanon</option>
                                                    <option value="Lesotho">Lesotho</option>
                                                    <option value="Liberia">Liberia</option>
                                                    <option value="Libya">Libya</option>
                                                    <option value="Liechtenstein">Liechtenstein</option>
                                                    <option value="Lithuania">Lithuania</option>
                                                    <option value="Luxembourg">Luxembourg</option>
                                                    <option value="Macau">Macau</option>
                                                    <option value="Macedonia">Macedonia</option>
                                                    <option value="Madagascar">Madagascar</option>
                                                    <option value="Malaysia">Malaysia</option>
                                                    <option value="Malawi">Malawi</option>
                                                    <option value="Maldives">Maldives</option>
                                                    <option value="Mali">Mali</option>
                                                    <option value="Malta">Malta</option>
                                                    <option value="Marshall Islands">Marshall Islands</option>
                                                    <option value="Martinique">Martinique</option>
                                                    <option value="Mauritania">Mauritania</option>
                                                    <option value="Mauritius">Mauritius</option>
                                                    <option value="Mayotte">Mayotte</option>
                                                    <option value="Mexico">Mexico</option>
                                                    <option value="Midway Islands">Midway Islands</option>
                                                    <option value="Moldova">Moldova</option>
                                                    <option value="Monaco">Monaco</option>
                                                    <option value="Mongolia">Mongolia</option>
                                                    <option value="Montserrat">Montserrat</option>
                                                    <option value="Morocco">Morocco</option>
                                                    <option value="Mozambique">Mozambique</option>
                                                    <option value="Myanmar">Myanmar</option>
                                                    <option value="Nambia">Nambia</option>
                                                    <option value="Nauru">Nauru</option>
                                                    <option value="Nepal">Nepal</option>
                                                    <option value="Netherland Antilles">Netherland Antilles</option>
                                                    <option value="Netherlands">Netherlands (Holland, Europe)</option>
                                                    <option value="Nevis">Nevis</option>
                                                    <option value="New Caledonia">New Caledonia</option>
                                                    <option value="New Zealand">New Zealand</option>
                                                    <option value="Nicaragua">Nicaragua</option>
                                                    <option value="Niger">Niger</option>
                                                    <option value="Nigeria">Nigeria</option>
                                                    <option value="Niue">Niue</option>
                                                    <option value="Norfolk Island">Norfolk Island</option>
                                                    <option value="Norway">Norway</option>
                                                    <option value="Oman">Oman</option>
                                                    <option value="Pakistan">Pakistan</option>
                                                    <option value="Palau Island">Palau Island</option>
                                                    <option value="Palestine">Palestine</option>
                                                    <option value="Panama">Panama</option>
                                                    <option value="Papua New Guinea">Papua New Guinea</option>
                                                    <option value="Paraguay">Paraguay</option>
                                                    <option value="Peru">Peru</option>
                                                    <option value="Phillipines">Philippines</option>
                                                    <option value="Pitcairn Island">Pitcairn Island</option>
                                                    <option value="Poland">Poland</option>
                                                    <option value="Portugal">Portugal</option>
                                                    <option value="Puerto Rico">Puerto Rico</option>
                                                    <option value="Qatar">Qatar</option>
                                                    <option value="Republic of Montenegro">Republic of Montenegro</option>
                                                    <option value="Republic of Serbia">Republic of Serbia</option>
                                                    <option value="Reunion">Reunion</option>
                                                    <option value="Romania">Romania</option>
                                                    <option value="Russia">Russia</option>
                                                    <option value="Rwanda">Rwanda</option>
                                                    <option value="St Barthelemy">St Barthelemy</option>
                                                    <option value="St Eustatius">St Eustatius</option>
                                                    <option value="St Helena">St Helena</option>
                                                    <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                                                    <option value="St Lucia">St Lucia</option>
                                                    <option value="St Maarten">St Maarten</option>
                                                    <option value="St Pierre & Miquelon">St Pierre & Miquelon</option>
                                                    <option value="St Vincent & Grenadines">St Vincent & Grenadines</option>
                                                    <option value="Saipan">Saipan</option>
                                                    <option value="Samoa">Samoa</option>
                                                    <option value="Samoa American">Samoa American</option>
                                                    <option value="San Marino">San Marino</option>
                                                    <option value="Sao Tome & Principe">Sao Tome & Principe</option>
                                                    <option value="Saudi Arabia">Saudi Arabia</option>
                                                    <option value="Senegal">Senegal</option>
                                                    <option value="Seychelles">Seychelles</option>
                                                    <option value="Sierra Leone">Sierra Leone</option>
                                                    <option value="Singapore">Singapore</option>
                                                    <option value="Slovakia">Slovakia</option>
                                                    <option value="Slovenia">Slovenia</option>
                                                    <option value="Solomon Islands">Solomon Islands</option>
                                                    <option value="Somalia">Somalia</option>
                                                    <option value="South Africa">South Africa</option>
                                                    <option value="Spain">Spain</option>
                                                    <option value="Sri Lanka">Sri Lanka</option>
                                                    <option value="Sudan">Sudan</option>
                                                    <option value="Suriname">Suriname</option>
                                                    <option value="Swaziland">Swaziland</option>
                                                    <option value="Sweden">Sweden</option>
                                                    <option value="Switzerland">Switzerland</option>
                                                    <option value="Syria">Syria</option>
                                                    <option value="Tahiti">Tahiti</option>
                                                    <option value="Taiwan">Taiwan</option>
                                                    <option value="Tajikistan">Tajikistan</option>
                                                    <option value="Tanzania">Tanzania</option>
                                                    <option value="Thailand">Thailand</option>
                                                    <option value="Togo">Togo</option>
                                                    <option value="Tokelau">Tokelau</option>
                                                    <option value="Tonga">Tonga</option>
                                                    <option value="Trinidad & Tobago">Trinidad & Tobago</option>
                                                    <option value="Tunisia">Tunisia</option>
                                                    <option value="Turkey">Turkey</option>
                                                    <option value="Turkmenistan">Turkmenistan</option>
                                                    <option value="Turks & Caicos Is">Turks & Caicos Is</option>
                                                    <option value="Tuvalu">Tuvalu</option>
                                                    <option value="Uganda">Uganda</option>
                                                    <option value="United Kingdom">United Kingdom</option>
                                                    <option value="Ukraine">Ukraine</option>
                                                    <option value="United Arab Erimates">United Arab Emirates</option>
                                                    <option value="United States of America">United States of America</option>
                                                    <option value="Uraguay">Uruguay</option>
                                                    <option value="Uzbekistan">Uzbekistan</option>
                                                    <option value="Vanuatu">Vanuatu</option>
                                                    <option value="Vatican City State">Vatican City State</option>
                                                    <option value="Venezuela">Venezuela</option>
                                                    <option value="Vietnam">Vietnam</option>
                                                    <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                                                    <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                                                    <option value="Wake Island">Wake Island</option>
                                                    <option value="Wallis & Futana Is">Wallis & Futana Is</option>
                                                    <option value="Yemen">Yemen</option>
                                                    <option value="Zaire">Zaire</option>
                                                    <option value="Zambia">Zambia</option>
                                                    <option value="Zimbabwe">Zimbabwe</option>    
                                                </select>
                                                <span class="error-text" id="country_2Error" style="display: block;"></span>
                                            </div>

                                            <div class="single-input-item">
                                                <label for="street-address_2" class="required mt-20">Street address</label>
                                                <input type="text" id="street_2" name="street_2" placeholder="Street address Line 1" />
                                                <span class="error-text" id="street_2Error"></span>
                                            </div>

                                            <div class="single-input-item">
                                                <input type="text" id="street_21" name="street_21" placeholder="Street address Line 2 (Optional)" />
                                            </div>

                                            <div class="single-input-item">
                                                <label for="town_2" class="required">Town / City</label>
                                                <input type="text" id="town_2" name="town_2" placeholder="Town / City" />
                                                <span class="error-text" id="town_2Error"></span>
                                            </div>

                                            <div class="single-input-item">
                                                <label for="state_2">State / Divition</label>
                                                <input type="text" id="state_2" name="state_2" placeholder="State / Divition" />
                                            </div>

                                            <div class="single-input-item">
                                                <label for="postcode_2" class="required">Postcode / ZIP</label>
                                                <input type="text" id="postcode_2" name="postcode_2" placeholder="Postcode / ZIP" />
                                                <span class="error-text" id="postcode_2Error"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="single-input-item">
                                        <label for="ordernote">Order Note</label>
                                        <textarea name="ordernote" id="ordernote" cols="30" rows="3" placeholder="Notes about your order, e.g. special notes for delivery."></textarea>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- Order Summary Details -->
                    @if (!empty($bladeVar['userCartProducts']))
                    <?php
                    $arrayCartlist = array();
                    if(!empty($bladeVar['userCartData'])){ 
                        foreach($bladeVar['userCartData'] as $cartlist){
                            $arrayCartlist[] = $cartlist->product_id;
                        } 
                    } ?> 
                    @if (!empty($arrayCartlist)) 
                    @if (sizeof($bladeVar['userCartProducts']) > 0)
                    <div class="col-lg-6">
                        <div class="order-summary-details">
                            <h5 class="checkout-title">Your Order Summary</h5>
                            <div class="order-summary-content">
                                <!-- Order Summary Table -->
                                <div class="order-summary-table table-responsive text-center">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Products</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                         @foreach($bladeVar['userCartProducts'] as $product)
                                            @if($product->deleted_at == NULL)
                                                @if(in_array($product->_id, $arrayCartlist)) 
                                                    <tr>
                                                        <td><a href="{{ url('/shop/'.$product->slug) }}">{{ $product->name }} <strong> × {{ $product->quantity }}</strong></a>
                                                        </td>
                                                        <td>
                                                        @if(!empty($product->discounted_price) && ($product->discounted_price != '0.00'))
                                                            ${{ $product->discounted_price }}
                                                        @else
                                                            ${{ $product->price }}
                                                        @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endif
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                            <?php 
                                                $subTotal = array();
                                                $shipping = '0.00';
                                                foreach($bladeVar['userCartProducts'] as $product){
                                                    if($product->deleted_at == NULL){
                                                        if(in_array($product->_id, $arrayCartlist)){
                                                            if(!empty($product->discounted_price) && ($product->discounted_price != '0.00')){
                                                                $subTotal[] = number_format($product->discounted_price * $product->quantity, 2);
                                                            }else{
                                                                $subTotal[] = number_format($product->price * $product->quantity, 2);
                                                            }
                                                        }
                                                    }
                                                }
                                            ?>
                                            <tr>
                                                <td>Sub Total</td>
                                                <td class="subTotal">$<?php if(!empty($subTotal)){ echo number_format(array_sum($subTotal), 2); }else{ echo '0.00'; } ?></td>
                                            </tr>
                                            <tr>
                                                <td>Shipping</td>
                                                <td class="shipping" data-sp="<?php if(!empty($shipping)){ echo $shipping; }else{ echo '0.00'; } ?>">$<?php if(!empty($shipping)){ echo $shipping; }else{ echo '0.00'; } ?></td>
                                            </tr>
                                            <tr class="couponDis">
                                                <td>Coupon Discount</td>
                                                <td class="totaldis"></td>
                                            </tr>
                                            <tr>
                                                <td>Total Amount</td>
                                                <td class="total-amount">$<?php if(!empty($subTotal)){ echo number_format((array_sum($subTotal)+$shipping), 2); }else{ echo '0.00'; } ?></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <!-- Order Payment Method -->
                                <div class="order-payment-method">
                                    <div class="single-payment-method show">
                                        <div class="payment-method-name">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="cashon" name="paymentmethod" value="Cash On Delivery" class="custom-control-input" checked />
                                                <label class="custom-control-label" for="cashon">Cash On Delivery</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single-payment-method">
                                        <div class="payment-method-name">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="paypalpayment" name="paymentmethod" value="Paypal" class="custom-control-input" />
                                                <label class="custom-control-label" for="paypalpayment">Paypal <img src="assets/img/paypal-card.jpg" class="img-fluid paypal-card" alt="Paypal" /></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="summary-footer-area">
                                        <div class="custom-control custom-checkbox mb-20">
                                            <input type="checkbox" class="custom-control-input" id="terms" name="terms" />
                                            <label class="custom-control-label" for="terms">I have read and agree to
                                                the website <a href="#">terms and conditions.</a></label>
                                        </div>
                                        <button type="submit" id="placeOrder" class="btn btn-sqr">Place Order</button>
                                        <img src="{{ asset('images/ajax-loader.gif') }}" id="ajax-loader" style="display:none; margin-left: 10px; height: 40px;">
                                        <p id="responseMessage"></p> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endif
                    @endif
                </div>
            </div>
        </div>
        <!-- checkout main wrapper end -->
    </main>
@include('frontend.footer')
<script type="text/javascript">
$(document).ready(function() {
    var baseUrl = '{{ url("/") }}';
    var token = '{{ csrf_token() }}';
	function isEmail(emailid) {
		var pattern = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
		return pattern.test(emailid);
    }

    //-------------------------------Sign In Start-----------------------------------------//	
    $('#userLoginForm #email').focusout(function() {
        var mailValue = $(this).val();            
        if ((mailValue == '') || (!isEmail($('#userLoginForm #email').val()))) {
            $('#userLoginForm #email').addClass('has-error');
            $('#userLoginForm #emailError').show().text('Please enter a valid email address');
        } else if ((mailValue != '') && (isEmail($('#userLoginForm #email').val()))) {
            $('#userLoginForm #email').removeClass('has-error');
            $('#userLoginForm #emailError').hide().text('');
        }
    });

    $('#userLoginForm #password').focusout(function() {
        var passwordValue = $(this).val();
        if (passwordValue == '') {
            $('#userLoginForm #password').addClass('has-error');
            $('#userLoginForm #passwordError').show().text('Password empty or invalid');
        } else if (passwordValue.length < 4) {
            $('#userLoginForm #password').addClass('has-error');
            $('#userLoginForm #passwordError').show().text('Password must be at least 4 characters');
        } else if (passwordValue != '') {
            $('#userLoginForm #password').removeClass('has-error');
            $('#userLoginForm #passwordError').hide().text('');
        }
    });

    $('#userLogin').on('click', function(event) {
        event.preventDefault();
        $('#responseMessage').html('');
        $('#userLoginForm #email').trigger("focusout");
        $('#userLoginForm #password').trigger("focusout");
        var mailValue = $("#userLoginForm #email").val();
        var passwordValue = $("#userLoginForm #password").val();
        if ((mailValue != '') && (isEmail($('#userLoginForm #email').val())) && (passwordValue != '') && (passwordValue.length >= 4)) {
            $('#userLoginForm #email').removeClass('has-error');
            $('#userLoginForm #password').removeClass('has-error');
            $('#userLoginForm #emailError').text('').hide();
            $('#userLoginForm #passwordError').text('').hide();
            user_login_account();
        }
    });

    function user_login_account() {
        $('#responseMessage').html('');
        $('#userLoginForm #ajax-loader').show();
        var token = $('#userLoginForm #_token').val();
        var email1 = $('#userLoginForm #email').val();
        var password1 = $('#userLoginForm #password').val();
        if($('#userLoginForm #rememberMe').is(":checked")){
            var remember1 = true;
        }else{
            var remember1 = false;
        }
        $.ajax({
            url: baseUrl + "/userlogin",
            type: "POST",
            data: {
                '_token': token,
                'email': email1,
                'password': password1,
                'remember': remember1,
                'loginAccount': 'login customer'
            },
            dataType: "JSON",
            success: function(jsonStr) {
                var res_data = JSON.stringify(jsonStr);
                var response = JSON.parse(res_data);
                var responseData = response['responseData'];
                if ((responseData != null) && (responseData == 'logged in successfully')) {
                    $('#responseMessage').html('You have successfully logged in');
                    setTimeout(function(){ window.location.href = baseUrl;  }, 1500);;
                } else if (((response['emailError'] != null) && (response['emailError'] == 'Please enter a valid email address')) || ((response['passwordError'] != null) && (response['passwordError'] == 'Invalid password')) || ((response['statusError'] != null) && (response['statusError'] == 'Your account temporarily deactivated'))) {
                    if ((response['emailError'] != null) && (response['emailError'] == 'Please enter a valid email address')) {
                        $('#userLoginForm #email').addClass('has-error');
                        $('#userLoginForm #emailError').show().text('Please enter a valid email address');
                    }
                    if ((response['passwordError'] != null) && (response['passwordError'] == 'Invalid password')) {
                        $('#userLoginForm #password').addClass('has-error');
                        $('#userLoginForm #passwordError').show().text('Invalid password');
                    }
                    if ((response['statusError'] != null) && (response['statusError'] == 'Your account temporarily deactivated')) {
                        $('#responseMessage').html('Your account temporarily deactivated');
                    }
                } else {
                    $('#responseMessage').html(responseData);
                }
                $('#userLoginForm #ajax-loader').hide();
            }
        });
    }
    //---------------------------------Sign In End-------------------------------------------//

    $('#shipToDifferent').on('click', function(event) {
        if($(this).is(":checked")){
            $('.shipToDifferent').show();
        }else if($(this).is(":not(:checked)")){
            $('.shipToDifferent').hide(); 
        }
    });

    $('#create_pwd').on('click', function(event) {
        if($(this).is(":checked")){
            $('.account-create').show();
        }else if($(this).is(":not(:checked)")){
            $('.account-create').hide();
        }
    });

    $('#terms').on('click', function(event) {
        if($(this).is(":checked")){
            $('#terms').parent().find('.custom-control-label').removeClass('terms');
        }else if($(this).is(":not(:checked)")){
            $('#terms').parent().find('.custom-control-label').addClass('terms');
        }
    });

    $('#orderForm #fname').focusout(function() {
        var fnameValue = $(this).val();
        if (fnameValue == '') {
            $('#orderForm #fname').addClass('has-error');
            $('#orderForm #fnameError').show().text('Please enter a first name');
        } else if (fnameValue != '') {
            $('#orderForm #fname').removeClass('has-error');
            $('#orderForm #fnameError').hide().text('');
        }
    });
    $('#orderForm #lname').focusout(function() {
        var fnameValue = $(this).val();
        if (fnameValue == '') {
            $('#orderForm #lname').addClass('has-error');
            $('#orderForm #lnameError').show().text('Please enter a last name');
        } else if (fnameValue != '') {
            $('#orderForm #lname').removeClass('has-error');
            $('#orderForm #lnameError').hide().text('');
        }
    });
    $('#orderForm #email').focusout(function() {
        var mailValue = $(this).val();            
        if ((mailValue == '') || (!isEmail($('#orderForm #email').val()))) {
            $('#orderForm #email').addClass('has-error');
            $('#orderForm #emailError').show().text('Please enter a valid email address');
        } else if ((mailValue != '') && (isEmail($('#orderForm #email').val()))) {
            $('#orderForm #email').removeClass('has-error');
            $('#orderForm #emailError').hide().text('');
        }
    });
    $('#orderForm #streetAddress').focusout(function() {
        var streetAddress = $(this).val();
        if (streetAddress == '') {
            $('#orderForm #streetAddress').addClass('has-error');
            $('#orderForm #streetAddressError').show().text('Please enter a street address');
        } else if (streetAddress != '') {
            $('#orderForm #streetAddress').removeClass('has-error');
            $('#orderForm #streetAddressError').hide().text('');
        }
    });
    $('#orderForm #city').focusout(function() {
        var city = $(this).val();
        if (city == '') {
            $('#orderForm #city').addClass('has-error');
            $('#orderForm #cityError').show().text('Please enter a city');
        } else if (city != '') {
            $('#orderForm #city').removeClass('has-error');
            $('#orderForm #cityError').hide().text('');
        }
    });
    $('#orderForm #postcode').focusout(function() {
        var postcode = $(this).val();
        if (postcode == '') {
            $('#orderForm #postcode').addClass('has-error');
            $('#orderForm #postcodeError').show().text('Please enter a postcode / zip');
        } else if (postcode != '') {
            $('#orderForm #postcode').removeClass('has-error');
            $('#orderForm #postcodeError').hide().text('');
        }
    });
    //--------------------------------------------------------------
    $('#orderForm #f_name_2').focusout(function() {
        var fnameValue = $(this).val();
        if (fnameValue == '') {
            $('#orderForm #f_name_2').addClass('has-error');
            $('#orderForm #f_name_2Error').show().text('Please enter a first name');
        } else if (fnameValue != '') {
            $('#orderForm #f_name_2').removeClass('has-error');
            $('#orderForm #f_name_2Error').hide().text('');
        }
    });
    $('#orderForm #l_name_2').focusout(function() {
        var fnameValue = $(this).val();
        if (fnameValue == '') {
            $('#orderForm #l_name_2').addClass('has-error');
            $('#orderForm #l_name_2Error').show().text('Please enter a last name');
        } else if (fnameValue != '') {
            $('#orderForm #l_name_2').removeClass('has-error');
            $('#orderForm #l_name_2Error').hide().text('');
        }
    });
    $('#orderForm #email_2').focusout(function() {
        var mailValue = $(this).val();            
        if ((mailValue == '') || (!isEmail($('#orderForm #email_2').val()))) {
            $('#orderForm #email_2').addClass('has-error');
            $('#orderForm #email_2Error').show().text('Please enter a valid email address');
        } else if ((mailValue != '') && (isEmail($('#orderForm #email_2').val()))) {
            $('#orderForm #email_2').removeClass('has-error');
            $('#orderForm #email_2Error').hide().text('');
        }
    });
    $('#orderForm #street_2').focusout(function() {
        var streetAddress = $(this).val();
        if (streetAddress == '') {
            $('#orderForm #street_2').addClass('has-error');
            $('#orderForm #street_2Error').show().text('Please enter a street address');
        } else if (streetAddress != '') {
            $('#orderForm #street_2').removeClass('has-error');
            $('#orderForm #street_2Error').hide().text('');
        }
    });
    $('#orderForm #town_2').focusout(function() {
        var city = $(this).val();
        if (city == '') {
            $('#orderForm #town_2').addClass('has-error');
            $('#orderForm #town_2Error').show().text('Please enter a city');
        } else if (city != '') {
            $('#orderForm #town_2').removeClass('has-error');
            $('#orderForm #town_2Error').hide().text('');
        }
    });
    $('#orderForm #postcode_2').focusout(function() {
        var postcode = $(this).val();
        if (postcode == '') {
            $('#orderForm #postcode_2').addClass('has-error');
            $('#orderForm #postcode_2Error').show().text('Please enter a postcode / zip');
        } else if (postcode != '') {
            $('#orderForm #postcode_2').removeClass('has-error');
            $('#orderForm #postcode_2Error').hide().text('');
        }
    });

    $("#placeOrder").on('click', function(event) {
        event.preventDefault();
        $('#responseMessage').html('');
        $('#orderForm #fname').trigger("focusout");
        $('#orderForm #lname').trigger("focusout");
        $('#orderForm #email').trigger("focusout");
        $('#orderForm #streetAddress').trigger("focusout");  
        $('#orderForm #city').trigger("focusout");
        $('#orderForm #postcode').trigger("focusout"); 
        var country = $('#country').val();
        if (country == '') {
            $('#orderForm #country').parent().find('.nice-select').addClass('has-error');
            $('#orderForm #countryError').show().text('Please enter a country');
        } else if (country != '') {
            $('#orderForm #country').parent().find('.nice-select').removeClass('has-error');
            $('#orderForm #countryError').hide().text('');
        }
        
        if($('#shipToDifferent').is(":checked")){
            $('.shipToDifferent').show();
            $('#orderForm #f_name_2').trigger("focusout");
            $('#orderForm #l_name_2').trigger("focusout");
            $('#orderForm #email_2').trigger("focusout");
            $('#orderForm #street_2').trigger("focusout");  
            $('#orderForm #town_2').trigger("focusout");
            $('#orderForm #postcode_2').trigger("focusout"); 

            var country_2 = $('#country_2').val();
            if (country_2 == '') {
                $('#orderForm #country_2').parent().find('.nice-select').addClass('has-error');
                $('#orderForm #country_2Error').show().text('Please enter a country');
            } else if (country_2 != '') {
                $('#orderForm #country_2').parent().find('.nice-select').removeClass('has-error');
                $('#orderForm #country_2Error').hide().text('');
            }
        }

        var fnameValue = $("#orderForm #fname").val();
        var lnameValue = $("#orderForm #lname").val();
        var mailValue = $("#orderForm #email").val();
        var city = $("#orderForm #city").val();
        var postcode = $("#orderForm #postcode").val();
        var streetAddress = $("#orderForm #streetAddress").val();

        var fnameValue_2 = $("#orderForm #f_name_2").val();
        var lnameValue_2 = $("#orderForm #l_name_2").val();
        var mailValue_2 = $("#orderForm #email_2").val();
        var city_2 = $("#orderForm #town_2").val();
        var postcode_2 = $("#orderForm #postcode_2").val();
        var street_2 = $("#orderForm #street_2").val();

        var couponCode = $("#couponCode").val();
        var paymentmethod = $('input[name=paymentmethod]:checked').val(); 

        if($('#terms').is(":checked")){
            var terms = '1';
            $('#terms').parent().find('.custom-control-label').removeClass('terms');
        }else if($('#terms').is(":not(:checked)")){
            var terms = '0';
            $('#terms').parent().find('.custom-control-label').addClass('terms');
        } 
        if ((mailValue != '') && (isEmail($('#orderForm #email').val())) && (fnameValue != '') && (lnameValue != '') && (country != '') && (city != '') && (postcode != '') && (streetAddress != '') && (terms != '0')) {
            if($('#shipToDifferent').is(":checked")){
                if ((mailValue != '') && (isEmail($('#orderForm #email').val())) && (fnameValue != '') && (lnameValue != '') && (country != '') && (city != '') && (postcode != '') && (streetAddress != '') && (mailValue_2 != '') && (isEmail($('#orderForm #email_2').val())) && (fnameValue_2 != '') && (lnameValue_2 != '') && (country_2 != '') && (city_2 != '') && (postcode_2 != '') && (street_2 != '') && (terms != '0')) {
                    orderPlace();
                }
            }else if($('#shipToDifferent').is(":not(:checked)")){
                if ((mailValue != '') && (isEmail($('#orderForm #email').val())) && (fnameValue != '') && (lnameValue != '') && (country != '') && (city != '') && (postcode != '') && (streetAddress != '') && (terms != '0')) {
                    orderPlace();
                }
            }    
        }
    });

    function orderPlace() {
        $('#responseMessage').html('');
        $('.summary-footer-area #ajax-loader').show();

        var fnameValue = $("#orderForm #fname").val();
        var lnameValue = $("#orderForm #lname").val();
        var mailValue = $("#orderForm #email").val();
        var country = $('#orderForm #country').val();
        var streetAddress = $("#orderForm #streetAddress").val();
        var streetAddress1 = $("#orderForm #streetAddress1").val();
        var city = $("#orderForm #city").val();
        var state = $("#orderForm #state").val();
        var postcode = $("#orderForm #postcode").val();
        var phone = $("#orderForm #phone").val();

        if($('#shipToDifferent').is(":checked")){
            var shipingDetails = true;
        }else if($('#shipToDifferent').is(":not(:checked)")){
            var shipingDetails = false;
        } 

        var fnameValue_2 = $("#orderForm #f_name_2").val();
        var lnameValue_2 = $("#orderForm #l_name_2").val();
        var mailValue_2 = $("#orderForm #email_2").val();
        var country_2 = $('#orderForm #country_2').val();
        var street_2 = $("#orderForm #street_2").val();
        var street_21 = $("#orderForm #street_21").val();
        var city_2 = $("#orderForm #town_2").val();
        var state_2 = $("#orderForm #state_2").val();
        var postcode_2 = $("#orderForm #postcode_2").val();

        var ordernote = $("#orderForm #ordernote").val();
        var couponCode = $("#couponCode").val();
        var paymentmethod = $('input[name=paymentmethod]:checked').val(); 
        var shipping = $('.shipping').attr('data-sp');//$('input[name=shipping]:checked').val();   
        if($('#terms').is(":checked")){
            var terms = true;
        }else if($('#terms').is(":not(:checked)")){
            var terms = false;
        } 
        $.ajax({
            url: baseUrl + "/placeorder",
            type: "POST",
            data: {
                '_token': token,
                'billing_fname': fnameValue,
                'billing_lname': lnameValue,
                'billing_email': mailValue,
                'billing_country': country,
                'billing_street': streetAddress,
                'billing_street1' : streetAddress1,
                'billing_city': city,
                'billing_state': state,
                'billing_postcode': postcode,
                'billing_phone': phone,
                'shipingDetails' : shipingDetails,
                'shiping_fname': fnameValue_2,
                'shiping_lname': lnameValue_2,
                'shiping_email': mailValue_2,
                'shiping_country': country_2,
                'shiping_street': street_2,
                'shiping_street1' : street_21,
                'shiping_city': city_2,
                'shiping_state': state_2,
                'shiping_postcode': postcode_2,
                'billing_ordernote': ordernote,
                'couponCode' : couponCode,
                'paymentmethod' : paymentmethod,
                'shipping' : shipping
            },
            dataType: "JSON",
            success: function(jsonStr) {
                var res_data = JSON.stringify(jsonStr);
                var response = JSON.parse(res_data);
                var responseData = response['responseData'];
                if ((responseData != null) && (responseData == 'Your order has been placed successfully')) {
                    //$('#responseMessage').html('Your order has been placed successfully');
                    window.location.href = baseUrl+'/ordersuccess';
                } else if((responseData != null) && (responseData == 'paypalurl')) {
                    var paypalurl = response['url'];
                    window.location.href = paypalurl;
                } else {
                    //$('#responseMessage').html(responseData);
                    window.location.href = baseUrl+'/ordercancel';
                }
                $('.summary-footer-area #ajax-loader').hide();
            }
        });
    } 

});
</script>