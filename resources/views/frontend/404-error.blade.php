@extends('layouts.sellersapp')
@section('header')@endsection
@section('content')
<!-- Styles -->
<style>
    .title {
        font-size: 50px;
        text-align: center;
    }
    .m-b-md {
        margin-bottom: 30px;
    }
</style>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <img src="{{ asset('images/logo.png') }}" alt="" width="97px" class="mx-auto d-block img-fluid mb-4">
         <div class="title m-b-md">
            You cannot access this page! <br />This is for only '{{$role}}'
		  </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
@endsection
@section('footer')
@component('auth.footer')@endcomponent
@endsection
