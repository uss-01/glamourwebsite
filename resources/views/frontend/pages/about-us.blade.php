@include('frontend.header')
    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">About us</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->
        @if (!empty($bladeVar['results'])) 
        <!-- about us area start -->
        <section class="about-us section-padding">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        @if($bladeVar['results']->image)
                        <div class="about-thumb">
                            <img src="{{ asset('images/cms').'/'.$bladeVar['results']->image}}" alt="{{ $bladeVar['results']->slug }}">
                        </div>
                        @endif
                    </div>
                    <div class="col-lg-7">
                        <div class="about-content">
                            <h2 class="about-title">{{ $bladeVar['results']->title }}</h2>
                            {!! $bladeVar['results']->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- about us area end -->
        @endif
        <!-- choosing area start -->
        <div class="choosing-area section-padding pt-0">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title text-center">
                            <h2 class="title">Why Choose Us</h2>
                            <p>Accumsan vitae pede lacus ut ullamcorper sollicitudin quisque libero</p>
                        </div>
                    </div>
                </div>
                <div class="row mbn-30">
                    <div class="col-lg-4 col-md-4">
                        <div class="single-choose-item text-center mb-30">
                            <i class="fa fa-globe"></i>
                            <h4>free shipping</h4>
                            <p>Amadea Shop is a very slick and clean e-commerce template with endless possibilities.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="single-choose-item text-center mb-30">
                            <i class="fa fa-plane"></i>
                            <h4>fast delivery</h4>
                            <p>Amadea Shop is a very slick and clean e-commerce template with endless possibilities.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="single-choose-item text-center mb-30">
                            <i class="fa fa-comments"></i>
                            <h4>customers support</h4>
                            <p>Amadea Shop is a very slick and clean e-commerce template with endless possibilities.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- choosing area end -->
        @if (!empty($bladeVar['testimonial_results']->count() > 0))
        <!-- testimonial area start -->
        <section class="testimonial-area section-padding bg-img" data-bg="assets/img/testimonial/testimonials-bg.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <!-- section title start -->
                        <div class="section-title text-center">
                            <h2 class="title">testimonials</h2>
                            <p class="sub-title">What they say</p>
                        </div>
                        <!-- section title start -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="testimonial-thumb-wrapper">
                            <div class="testimonial-thumb-carousel">
                                @foreach ($bladeVar['testimonial_results'] as $testimonial)
                                <div class="testimonial-thumb">
                                    <img src="{{ asset('images/cms').'/'.$testimonial->image}}" alt="{{ $testimonial->slug }}">
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="testimonial-content-wrapper">
                            <div class="testimonial-content-carousel">
                                @foreach ($bladeVar['testimonial_results'] as $testimonial)
                                <div class="testimonial-content">
                                    <p>{{ $testimonial->shortDescription }}</p>
                                    <!-- <div class="ratings">
                                        <span><i class="fa fa-star-o"></i></span>
                                        <span><i class="fa fa-star-o"></i></span>
                                        <span><i class="fa fa-star-o"></i></span>
                                        <span><i class="fa fa-star-o"></i></span>
                                        <span><i class="fa fa-star-o"></i></span>
                                    </div> -->
                                    <h5 class="testimonial-author">{{ $testimonial->name }}</h5>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    </section>
        <!-- testimonial area end -->
        @endif
        @if (!empty($bladeVar['team_results']->count() > 0))
        <!-- team area start -->
        <div class="team-area section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title text-center">
                            <h2 class="title">Our Team</h2>
                            <p>Accumsan vitae pede lacus ut ullamcorper sollicitudin quisque libero</p>
                        </div>
                    </div>
                </div>
                <div class="row mbn-30">
                    @foreach ($bladeVar['team_results'] as $team)
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="team-member mb-30"> 
                            <div class="team-thumb">
                                <img src="{{ asset('images/cms').'/'.$team->image}}" alt="{{ $team->slug }}">
                                <div class="team-social">
                                @if($team->facebook_url != '')<a href="{{ $team->facebook_url }}"><i class="fa fa-facebook"></i></a>@endif
                                @if($team->twitter_url != '')<a href="{{ $team->twitter_url }}"><i class="fa fa-twitter"></i></a>@endif
                                @if($team->linkedin_url != '')<a href="{{ $team->linkedin_url }}"><i class="fa fa-linkedin"></i></a>@endif
                                @if($team->google_url != '')<a href="{{ $team->google_url }}"><i class="fa fa-google-plus"></i></a>@endif
                                </div>
                            </div>
                            <div class="team-content text-center">
                                <h6 class="team-member-name">{{ $team->name }}</h6>
                                <p>{{ $team->designation }}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- team area end -->
        @endif
    </main>
@include('frontend.footer')