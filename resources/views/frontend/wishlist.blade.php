@include('frontend.header')
    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/shop') }}">shop</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">wishlist</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- wishlist main wrapper start -->
        <div class="wishlist-main-wrapper section-padding">
            <div class="container">
                <!-- Wishlist Page Content Start -->
                <div class="section-bg-color">
                    <div class="row">
                        <div class="col-lg-12" id="wishlistItem">
                            <!-- Wishlist Table Area -->
                            <?php
                            $arrayWishlist = array();
                            if(!empty($bladeVar['userWishlistData'])){ 
                                foreach($bladeVar['userWishlistData'] as $wishlist){
                                    $arrayWishlist[] = $wishlist->product_id;
                                } 
                            } ?>
                            @if (!empty($arrayWishlist))
                                @if (!empty($bladeVar['product_results']))
                                <div class="cart-table table-responsive">
                                    @if (sizeof($bladeVar['product_results']) > 0)
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="pro-thumbnail">Thumbnail</th>
                                                <th class="pro-title">Product</th>
                                                <th class="pro-price">Price</th>
                                                <th class="pro-quantity">Stock Status</th>
                                                <th class="pro-subtotal">Add to Cart</th>
                                                <th class="pro-remove">Remove</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($bladeVar['product_results'] as $product)
                                                @if($product->deleted_at == NULL)
                                                    @if(in_array($product->_id, $arrayWishlist))
                                                    <tr id="product{{ $product->_id }}">
                                                        <td class="pro-thumbnail"><a href="{{ url('/shop/'.$product->slug) }}"><img class="img-fluid" src="{{ asset('images/products').'/'.$product->image}}" alt="{{ $product->slug }}" /></a></td>
                                                        <td class="pro-title"><a href="{{ url('/shop/'.$product->slug) }}">{{ $product->name }}</a></td>
                                                        <td class="pro-price"><span>
                                                            @if(!empty($product->discounted_price) && ($product->discounted_price != '0.00'))
                                                                ${{ $product->discounted_price }}
                                                            @else
                                                                ${{ $product->price }}
                                                            @endif 
                                                        </span></td>
                                                        @if(!empty($product->product_quantity) && ($product->product_quantity > 0))
                                                            <td class="pro-quantity"><span class="text-success">In Stock</span></td>
                                                            @if($product->add_to_cart == '1')

                                                            <?php $arrayCartList = array();
                                                            if(!empty($bladeVar['userCartData'])){ 
                                                                foreach($bladeVar['userCartData'] as $cartList){
                                                                    $arrayCartList[] = $cartList->product_id;
                                                                } 
                                                            } ?>
                                                            <td class="pro-subtotal">
                                                            <?php if (in_array($product->_id, $arrayCartList)) { ?>
                                                                <a href="#" class="btn btn-sqr addtocart1 disabled" id="addtocart{{ $product->_id }}" data-pid="{{ $product->_id }}">Go to cart</a>
                                                            <?php }else{ ?>
                                                                <a href="#" class="btn btn-sqr addtocart1" id="addtocart{{ $product->_id }}" data-pid="{{ $product->_id }}" data-cid="{{ $product->product_color_id }}" data-sid="{{ $product->product_size_id }}">add to cart</a>
                                                            <?php } ?>      
                                                            </td>
                                                            @endif
                                                        @else
                                                            <td class="pro-quantity"><span class="text-danger">Stock Out</span></td>
                                                            @if($product->add_to_cart == '1')
                                                            <td class="pro-subtotal"><a href="#" class="btn btn-sqr disabled">Add to Cart</a></td>
                                                            @endif
                                                        @endif
                                                        <td class="pro-remove"><a href="#" class="removeProduct" data-pid="{{ $product->_id }}"><i class="fa fa-trash-o"></i></a></td>
                                                    </tr>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @endif 
                                </div>
                                @endif
                            @else
                            <h3 style="text-align: center;">Empty Wishlist</h3>
                            <p style="text-align: center;">You have no items in your wishlist. Start adding!</p>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- Wishlist Page Content End -->
            </div>
        </div>
        <!-- wishlist main wrapper end -->
    </main>
@include('frontend.footer')
<script type="text/javascript">
$(document).ready(function() {
    var baseUrl = '{{ url("/") }}';
    var token = '{{ csrf_token() }}';

    $(".addtocart1").on('click', function(event) {
        event.preventDefault();
        var userID = '';
        <?php $data = Session::all(); if (Session::has('userLoggedIn')){ ?>
            userID = '<?php echo Session::get('userLoggedIn'); ?>';
        <?php } ?>
        var getAllClass = $(this).attr('class');
        if(getAllClass == 'btn btn-sqr addtocart1 disabled'){
            var redirecturl = baseUrl+'/cart';
            location.href = redirecturl;  
        }else{    
            if(userID != ''){
                var pid = $(this).attr('data-pid');
                var cid = $(this).attr('data-cid');
                var sid = $(this).attr('data-sid');
                $.ajax({
                    url: baseUrl + "/addtocart",
                    type: "POST",
                    data: { 
                        '_token': token, 
                        'product_id': pid,
                        'color_id': cid,
                        'size_id': sid
                    },
                    dataType: "JSON",
                    success: function(jsonStr) {
                        var res_data = JSON.stringify(jsonStr);
                        var response = JSON.parse(res_data);
                        var responseData = response['responseData'];
                        var responseDataCount = response['responseDataCount'];
                        if ((responseData != null) && (responseData == 'add to cart successfully')) {
                            $('#addtocart'+pid).addClass('disabled');
                            $('#addtocart'+pid).text('go to cart');
                        } else {
                            $('#addtocart'+pid).removeClass('disabled');
                            $('#addtocart'+pid).text('add to cart');
                        }
                        if((responseDataCount != null) && (responseDataCount != '0')){
                            $('.minicart-btn .notification').text(responseDataCount);
                        }else{
                            $('.minicart-btn .notification').text(responseDataCount);
                        }
                        viewCart();
                    }
                });
            }else{
                var redirecturl = baseUrl+'/login-register';
                location.href = redirecturl;  
            }
        }
    });

    $(".removeProduct").on('click', function(event) {
        event.preventDefault();
        var pid = $(this).attr('data-pid');
        var userID = '';
        <?php $data = Session::all(); if (Session::has('userLoggedIn')){ ?>
            userID = '<?php echo Session::get('userLoggedIn'); ?>';
        <?php } ?>
        if((userID != '') && (pid != '')){
            $.ajax({
				url: baseUrl + "/addtowishlist",
				type: "POST",
				data: { 
                    '_token': token, 
					'product_id': pid,
				},
				dataType: "JSON",
				success: function(jsonStr) {
					var res_data = JSON.stringify(jsonStr);
					var response = JSON.parse(res_data);
                    var responseData = response['responseData'];
                    var responseDataCount = response['responseDataCount'];
					if ((responseData != null) && (responseData == 'add to wishlist successfully')) {
						$('#addtowishlist'+pid+' .pe-7s-like').addClass('addWishlist');
					} else {
						$('#addtowishlist'+pid+' .pe-7s-like').removeClass('addWishlist');
                    }
                    if((responseDataCount != null) && (responseDataCount != '0')){
                        $('.wishlist.notification').text(responseDataCount);
                    }else{
                        $('.wishlist.notification').text(responseDataCount);
                    }
                    $('#product'+pid).remove();
                    if(responseDataCount > 0){
                        console.log(responseDataCount);
                    }else{
                        $('.cart-table').hide();
                        $('#wishlistItem').append('<h3 style="text-align: center;">Empty Wishlist</h3>'+
                        '<p style="text-align: center;">You have no items in your wishlist. Start adding!</p>');
                    }
				}
			});    
        }else{
           var redirecturl = baseUrl+'/login-register';
           location.href = redirecturl;  
        }
    });

    function viewCart(){
        var data = { '_token': token };
        var ajaxUrl  = baseUrl+"/viewcart";
        $.post(ajaxUrl, data, function(response) {
            $('.minicart-inner-content').html('').append(response);
        });
    }
});
</script>    