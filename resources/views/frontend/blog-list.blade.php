@include('frontend.header')
    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Blog List</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- blog main wrapper start -->
        <div class="blog-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 order-2 order-lg-1">
                        <aside class="blog-sidebar-wrapper">
                            <div class="blog-sidebar">
                                <h5 class="title">search</h5>
                                <div class="sidebar-serch-form">
                                    <form action="{{ url('/blog/') }}" method="get">
                                        <input type="text" class="search-field" id="search" name="search" placeholder="search here">
                                        <button type="submit" class="search-btn"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            </div> <!-- single sidebar end -->
                            @if (!empty($bladeVar['blogCategories']))
                            <div class="blog-sidebar">
                                <h5 class="title">categories</h5>
                                <ul class="blog-archive blog-category">
                                @foreach($bladeVar['blogCategories'] as $category)
                                    @if($category->deleted_at == NULL)
                                    @if($category->categoryBlogs != 0)
                                    <li><a href="{{ url('/blog/?category='.$category->slug) }}">{{ $category->name }} ({{ $category->categoryBlogs }})</a></li>
                                    @endif
                                    @endif
                                @endforeach
                                </ul>
                            </div> 
                            @endif
                            <!-- single sidebar end -->
                            @if (!empty($bladeVar['blogArchives']))

                            <div class="blog-sidebar">
                                <h5 class="title">Blog Archives</h5>
                                <ul class="blog-archive">
                                    @foreach($bladeVar['blogArchives'] as $archive)
                                    <li><a href="{{ url('/blog/?archive='.$archive->monthname) }}">{{ $archive->monthname }} ({{ $archive->count }})</a></li>
                                    @endforeach
                                </ul>
                            </div> 
                            @endif
                            <!-- single sidebar end -->
                            @if (!empty($bladeVar['recent_post']->count() > 0))
                            <div class="blog-sidebar">
                                <h5 class="title">recent post</h5>
                                <div class="recent-post">
                                    @foreach($bladeVar['recent_post']  as $blog)
                                    @if($blog->deleted_at == NULL)
                                    <div class="recent-post-item">
                                        <figure class="product-thumb">
                                            <a href="{{ url('/blog/'.$blog->slug) }}">
                                                <img src="{{ asset('images/blogs').'/'.$blog->image}}" alt="{{ $blog->slug }}">
                                            </a>
                                        </figure>
                                        <div class="recent-post-description">
                                            <div class="product-name">
                                                <h6><a href="{{ url('/blog/'.$blog->slug) }}">{{ $blog->name }}</a></h6>
                                                <p>{{ date('F j Y', strtotime($blog->updated_at)) }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach  
                                </div>
                            </div> 
                            @endif
                            <!-- single sidebar end -->
                            @if (!empty($bladeVar['blogTags']))
                            <div class="blog-sidebar">
                                <h5 class="title">Tags</h5>
                                <ul class="blog-tags">
                                @foreach($bladeVar['blogTags'] as $tag)
                                @if($tag->deleted_at == NULL)
                                @if($tag->tagBlogs != 0)
                                    <li><a href="{{ url('/blog/?tag='.$tag->slug) }}">{{ $tag->name }}</a></li>
                                @endif
                                @endif
                                @endforeach
                                </ul>
                            </div> 
                            @endif
                            <!-- single sidebar end -->
                        </aside>
                    </div>
                    <div class="col-lg-9 order-1 order-lg-2">
                        <div class="blog-item-wrapper">
                            <!-- blog item wrapper end -->
                            @if (!empty($bladeVar['results']->count() > 0))
                            <div class="row mbn-30">
                                @foreach($bladeVar['results']  as $blog)
                                    @if($blog->deleted_at == NULL)
                                        @if($blog->blog_image_slide == '1')
                                        <div class="col-md-6">
                                            <div class="blog-post-item mb-30">
                                                @if(!empty($bladeVar['blogPics']))
                                                <figure class="blog-thumb">
                                                    <div class="blog-carousel-2 slick-row-15 slick-arrow-style slick-dot-style">
                                                        @foreach($bladeVar['blogPics'] as $pic)
                                                            @if($pic->blog_id == $blog->_id)
                                                            <div class="blog-single-slide  ">
                                                                <a href="{{ url('/blog/'.$blog->slug) }}">
                                                                    <img src="{{ asset('images/blogs').'/'.$pic->image }}" alt="{{ $blog->slug }}">
                                                                </a>
                                                            </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </figure>
                                                @endif
                                                <div class="blog-content">
                                                    <div class="blog-meta">
                                                        <p>{{ date('d/m/Y', strtotime($blog->updated_at)) }}</p>
                                                    </div>
                                                    <h4 class="blog-title">
                                                        <a href="{{ url('/blog/'.$blog->slug) }}">{{ $blog->name }}</a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div> 
                                        @elseif($blog->blog_audio == '1')
                                        <div class="col-md-6">
                                            <div class="blog-post-item mb-30">
                                                <figure class="blog-thumb embed-responsive embed-responsive-16by9">
                                                    <iframe src="{{ $blog->audio_url }}"></iframe>
                                                </figure>
                                                <div class="blog-content">
                                                    <div class="blog-meta">
                                                        <p>{{ date('d/m/Y', strtotime($blog->updated_at)) }}</p>
                                                    </div>
                                                    <h4 class="blog-title">
                                                        <a href="{{ url('/blog/'.$blog->slug) }}">{{ $blog->name }}</a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                        @elseif($blog->blog_video == '1')
                                        <div class="col-md-6">
                                            <div class="blog-post-item mb-30">
                                                <figure class="blog-thumb embed-responsive embed-responsive-16by9">
                                                    <iframe src="{{ $blog->video_url }}" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                                </figure>
                                                <div class="blog-content">
                                                    <div class="blog-meta">
                                                        <p>{{ date('d/m/Y', strtotime($blog->updated_at)) }}</p>
                                                    </div>
                                                    <h4 class="blog-title">
                                                        <a href="{{ url('/blog/'.$blog->slug) }}">{{ $blog->name }}</a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                        @else  
                                        <div class="col-md-6">
                                            <div class="blog-post-item mb-30">
                                                <figure class="blog-thumb">
                                                    <a href="{{ url('/blog/'.$blog->slug) }}">
                                                        <img src="{{ asset('images/blogs').'/'.$blog->image}}" alt="{{ $blog->slug }}">
                                                    </a>
                                                </figure>
                                                <div class="blog-content">
                                                    <div class="blog-meta">
                                                        <p>{{ date('d/m/Y', strtotime($blog->updated_at)) }}</p>
                                                    </div>
                                                    <h4 class="blog-title">
                                                        <a href="{{ url('/blog/'.$blog->slug) }}">{{ $blog->name }}</a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                            <!-- blog item wrapper end -->

                            <!-- start pagination area -->
                            <div class="paginatoin-area text-center">
                                {{ $bladeVar['results']->appends(Request::query())->links() }}
                                <!-- <ul class="pagination-box">
                                    <li><a class="previous" href="#"><i class="pe-7s-angle-left"></i></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a class="next" href="#"><i class="pe-7s-angle-right"></i></a></li>
                                </ul> -->
                            </div>
                            <!-- end pagination area -->
                            @else
                                <div class="paginatoin-area text-center">
                                    <strong>No records found!</strong>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- blog main wrapper end -->
    </main>
@include('frontend.footer')
