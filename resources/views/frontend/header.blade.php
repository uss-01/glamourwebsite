<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ (isset($bladeVar['page']['title']) ? $bladeVar['page']['title'] : '') }}</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @if (!empty($bladeVar['blogresult']))
        <meta property="og:title" content="{{ $bladeVar['blogresult']['name'] }}" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="{{ url('/blog/'.$bladeVar['blogresult']['slug']) }}" />
        <meta property="og:description" content="{{ $bladeVar['blogresult']['name'] }}" />
        <meta property="og:image" content="{{ asset('images/blogs').'/'.$bladeVar['blogresult']['image']}}" />

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@JewelleryEcommerce">
        <meta name="twitter:creator" content="@JewelleryEcommerce">
        <meta name="twitter:title" content="{{ $bladeVar['blogresult']['name'] }}">
        <meta name="twitter:description" content="{{ $bladeVar['blogresult']['name'] }}">
        <meta name="twitter:image" content="{{ asset('images/blogs').'/'.$bladeVar['blogresult']['image']}}">
    @endif
    @if (!empty($bladeVar['productDetail']))
        <meta property="og:title" content="{{ $bladeVar['productDetail']['name'] }}" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="{{ url('/blog/'.$bladeVar['productDetail']['slug']) }}" />
        <meta property="og:description" content="{{ $bladeVar['productDetail']['name'] }}" />
        <meta property="og:image" content="{{ $bladeVar['productDetail']['image']}}" />

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@JewelleryEcommerce">
        <meta name="twitter:creator" content="@JewelleryEcommerce">
        <meta name="twitter:title" content="{{ $bladeVar['productDetail']['name'] }}">
        <meta name="twitter:description" content="{{ $bladeVar['productDetail']['name'] }}">
        <meta name="twitter:image" content="{{ $bladeVar['productDetail']['image']}}">
    @endif
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/img/favicon.ico') }}">

    <!-- CSS
	============================================ -->
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,900" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/vendor/bootstrap.min.css') }}">
    <!-- Pe-icon-7-stroke CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/vendor/pe-icon-7-stroke.css') }}">
    <!-- Font-awesome CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/vendor/font-awesome.min.css') }}">
    <!-- Slick slider css -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/slick.min.css') }}">
    <!-- animate css -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/animate.css') }}">
    <!-- Nice Select css -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/nice-select.css') }}">
    <!-- jquery UI css -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/jqueryui.min.css') }}">
    <!-- main style css -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <style>
        .page-link{
            color: #c29958;
        }
        .page-item.active .page-link{
            background-color: #c29958;
            border-color: #c29958;
        }
    </style>
</head>

<body>
    <!-- Start Header Area -->
    <header class="header-area header-wide">
        <!-- main header start -->
        <div class="main-header d-none d-lg-block">
            <!-- header top start -->
            <div class="header-top bdr-bottom">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6">
                            <div class="welcome-message">
                                @if (!empty($bladeVar['site_setting']->count() > 0))
                                    @foreach($bladeVar['site_setting']  as $setting)
                                        @if($setting->title != '')
                                        <p>{{ $setting->title }}</p>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6 text-right">
                            <div class="header-top-settings">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- header top end -->

            <!-- header middle area start -->
            <div class="header-main-area sticky">
                <div class="container">
                    <div class="row align-items-center position-relative">

                        <!-- start logo area -->
                        <div class="col-lg-2">
                            @if (!empty($bladeVar['site_setting']->count() > 0))
                                @foreach($bladeVar['site_setting']  as $setting)
                                @if($setting->image != '')
                                <div class="logo">
                                    <a href="{{ url('/') }}">
                                        <img src="{{ asset('images/cms').'/'.$setting->image}}" alt="{{ $setting->title }}">
                                    </a>
                                </div>
                                @endif
                                @endforeach
                            @endif
                        </div>
                        <!-- start logo area -->

                        <!-- main menu area start -->
                        <div class="col-lg-6 position-static">
                            <div class="main-menu-area">
                                <div class="main-menu">
                                    <!-- main menu navbar start -->
                                    <nav class="desktop-menu">
                                        <ul> 
                                            <?php 
                                                $segments = request()->segments();
                                                $last  = end($segments);
                                                $first = reset($segments); 
                                            ?>
                                            <li <?php if(empty($first)){ ?>class="active"<?php } ?>><a href="{{ url('/') }}">Home</a></li>
                                            <li <?php if(!empty($first) && ($first == 'shop')){ ?>class="active"<?php } ?>><a href="{{ url('/shop') }}">shop</a></li>
                                            <li <?php if(!empty($first) && ($first == 'blog')){ ?>class="active"<?php } ?>><a href="{{ url('/blog') }}">Blog</a></li>
                                            <li <?php if(!empty($first) && ($first == 'about-us')){ ?>class="active"<?php } ?>><a href="{{ url('/about-us') }}">About us</a></li>
                                            <li <?php if(!empty($first) && ($first == 'contact-us')){ ?>class="active"<?php } ?>><a href="{{ url('/contact-us') }}">Contact us</a></li>
                                        </ul>
                                    </nav>
                                    <!-- main menu navbar end -->
                                </div>
                            </div>
                        </div>
                        <!-- main menu area end -->

                        <!-- mini cart area start -->
                        <div class="col-lg-4">
                            <div class="header-right d-flex align-items-center justify-content-xl-between justify-content-lg-end">
                                <div class="header-search-container">
                                    <button class="search-trigger d-xl-none d-lg-block"><i class="pe-7s-search"></i></button>
                                    <form class="header-search-box d-lg-none d-xl-block animated jackInTheBox" action="{{ url('/search') }}" method="get">
                                        <input type="text" name="search" id="search" placeholder="Search entire store hire" class="header-search-field">
                                        <button class="header-search-btn"><i class="pe-7s-search"></i></button>
                                    </form>
                                </div>
                                <div class="header-configure-area">
                                    <ul class="nav justify-content-end">
                                        <li class="user-hover">
                                            <a href="#">
                                                <i class="pe-7s-user"></i>
                                            </a>
                                            <ul class="dropdown-list">
                                            <?php  $data = Session::all();
                                                if (Session::has('userLoggedIn')){ ?>
                                                    <li><a href="{{ url('/account') }}">my account</a></li>
                                                    <li><a href="#" class="logout">log out</a></li>
                                            <?php }else{ ?>
                                                    <li><a href="{{ url('/login-register') }}">login</a></li>
                                                    <li><a href="{{ url('/login-register') }}">register</a></li> 
                                            <?php } ?>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="{{ url('/wishlist') }}">
                                                <i class="pe-7s-like"></i>
                                                <div class="wishlist notification">
                                                @if (!empty($bladeVar['userWishlist']))
                                                   {{ $bladeVar['userWishlist'] }} 
                                                @else
                                                   {{ $bladeVar['userWishlist'] }} 
                                                @endif
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="minicart-btn">
                                                <i class="pe-7s-shopbag"></i>
                                                <div class="notification">
                                                @if (!empty($bladeVar['userCart']))
                                                   {{ $bladeVar['userCart'] }} 
                                                @else
                                                   {{ $bladeVar['userCart'] }} 
                                                @endif
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- mini cart area end -->

                    </div>
                </div>
            </div>
            <!-- header middle area end -->
        </div>
        <!-- main header start -->

        <!-- mobile header start -->
        <!-- mobile header start -->
        <div class="mobile-header d-lg-none d-md-block sticky">
            <!--mobile header top start -->
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="mobile-main-header">

                            @if (!empty($bladeVar['site_setting']->count() > 0))
                                @foreach($bladeVar['site_setting']  as $setting)
                                @if($setting->image != '')
                                <div class="mobile-logo">
                                    <a href="{{ url('/') }}">
                                        <img src="{{ asset('images/cms').'/'.$setting->image}}" alt="{{ $setting->title }}">
                                    </a>
                                </div>
                                @endif
                                @endforeach
                            @endif
                            
                            <div class="mobile-menu-toggler">
                                <div class="mini-cart-wrap">
                                    <a href="#">
                                        <i class="pe-7s-shopbag"></i>
                                        <div class="notification">
                                        @if (!empty($bladeVar['userCart']))
                                            {{ $bladeVar['userCart'] }} 
                                        @else
                                            {{ $bladeVar['userCart'] }} 
                                        @endif
                                        </div>
                                    </a>
                                </div>
                                <button class="mobile-menu-btn">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- mobile header top start -->
        </div>
        <!-- mobile header end -->
        <!-- mobile header end -->

        <!-- offcanvas mobile menu start -->
        <!-- off-canvas menu start -->
        <aside class="off-canvas-wrapper">
            <div class="off-canvas-overlay"></div>
            <div class="off-canvas-inner-content">
                <div class="btn-close-off-canvas">
                    <i class="pe-7s-close"></i>
                </div>

                <div class="off-canvas-inner">
                    <!-- search box start -->
                    <div class="search-box-offcanvas">
                        <form action="{{ url('/search') }}" method="get">
                            <input type="text" name="search" id="search" placeholder="Search Here...">
                            <button class="search-btn"><i class="pe-7s-search"></i></button>
                        </form>
                    </div>
                    <!-- search box end -->

                    <!-- mobile menu start -->
                    <div class="mobile-navigation">

                        <!-- mobile menu navigation start -->
                        <nav>
                            <ul class="mobile-menu">
                                <li class="active"><a href="{{ url('/home') }}">Home</a></li>
                                <li><a href="{{ url('/shop') }}">shop</a></li>
                                <li><a href="{{ url('/blog') }}">Blog</a></li>
                                <li><a href="{{ url('/about-us') }}">About us</a></li>
                                <li><a href="{{ url('/contact-us') }}">Contact us</a></li>
                            </ul>
                        </nav>
                        <!-- mobile menu navigation end -->
                    </div>
                    <!-- mobile menu end -->

                    <div class="mobile-settings">
                        <ul class="nav">
                            <li>
                                <div class="dropdown mobile-top-dropdown">
                                    <a href="#" class="dropdown-toggle" id="myaccount" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        My Account
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="myaccount">
                                        <?php  $data = Session::all();
                                            if (Session::has('userLoggedIn')){
                                                //echo Session::get('userLoggedIn'); ?>
                                                <a class="dropdown-item" href="{{ url('/account') }}">my account</a>
                                                <a href="#" class="logout dropdown-item">log out</a>
                                        <?php }else{ ?>
                                                <a class="dropdown-item" href="{{ url('/login-register') }}">login</a>
                                                <a class="dropdown-item" href="{{ url('/login-register') }}">register</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <!-- offcanvas widget area start -->
                    <div class="offcanvas-widget-area">
                        <div class="off-canvas-contact-widget">
                            <ul>
                                @if (!empty($bladeVar['site_setting']->count() > 0))
                                    @foreach($bladeVar['site_setting']  as $setting)
                                    @if($setting->phone != '')<li><i class="fa fa-mobile"></i> <a href="tel:{{ $setting->phone }}">{{ $setting->phone }}</a></li>@endif
                                    @if($setting->email != '')<li><i class="fa fa-envelope-o"></i> <a href="mailto:{{ $setting->email }}">{{ $setting->email }} </a></li>@endif
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                        @if (!empty($bladeVar['site_setting']->count() > 0))
                            @foreach($bladeVar['site_setting']  as $setting)
                            <div class="off-canvas-social-widget">
                                @if($setting->facebook_url != '')<a href="{{ $setting->facebook_url }}"><i class="fa fa-facebook"></i></a>@endif
                                @if($setting->twitter_url != '')<a href="{{ $setting->twitter_url }}"><i class="fa fa-twitter"></i></a>@endif
                                @if($setting->linkedin_url != '')<a href="{{ $setting->linkedin_url }}"><i class="fa fa-linkedin"></i></a>@endif
                                @if($setting->pinterest_url != '')<a href="{{ $setting->pinterest_url }}"><i class="fa fa-pinterest-p"></i></a>@endif
                                @if($setting->instagram_url != '')<a href="{{ $setting->instagram_url }}"><i class="fa fa-instagram"></i></a>@endif
                                @if($setting->youtube_url != '')<a href="{{ $setting->youtube_url }}"><i class="fa fa-youtube"></i></a>@endif
                            </div>
                            @endforeach
                        @endif
                    </div>
                    <!-- offcanvas widget area end -->
                </div>
            </div>
        </aside>
        <!-- off-canvas menu end -->
        <!-- offcanvas mobile menu end -->
    </header>
    <!-- end Header Area -->