@include('frontend.header')
@if (!empty($bladeVar['productDetail']))
<style type="text/css">
.has-error {
    border: 1px solid red !important;
}
#responseMessage{
    display: none;
}
.unavailability {
    color : #dc3545 !important;
}
.addWishlist{
    color: red;
}
.colorActive{
    border-color: #c29958 !important;
}
</style>
    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/shop') }}">shop</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">{{ $bladeVar['productDetail']['name'] }}</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- page main wrapper start -->
        <div class="shop-main-wrapper section-padding pb-0">
            <div class="container">
                <div class="row">
                    <!-- product details wrapper start -->
                    <div class="col-lg-12 order-1 order-lg-2">
                        <!-- product details inner end -->
                        <div class="product-details-inner">
                            <div class="row">
                                <div class="col-lg-5">
                                @if (!empty($bladeVar['slideimages']))
                                    <div class="product-large-slider">
                                    @foreach($bladeVar['slideimages'] as $slide)
                                        <div class="pro-large-img img-zoom">
                                            <img src="{{ $slide['image'] }}" alt="{{ $bladeVar['productDetail']['name'] }}" />
                                        </div>
                                    @endforeach    
                                    </div>
                                    <div class="pro-nav slick-row-10 slick-arrow-style">
                                    @foreach($bladeVar['slideimages'] as $slide)
                                        <div class="pro-nav-thumb">
                                            <img src="{{ $slide['image'] }}" alt="{{ $bladeVar['productDetail']['name'] }}" />
                                        </div>
                                    @endforeach    
                                    </div>
                                @endif
                                </div>
                                <div class="col-lg-7">
                                    <div class="product-details-des">
                                        <div class="manufacturer-name">
                                            <a href="{{ url('/shop/?category='.$bladeVar['productDetail']->product_category->_id) }}">{{ $bladeVar['productDetail']->product_category->name }}</a>
                                        </div>
                                        <h3 class="product-name">{{ $bladeVar['productDetail']['name'] }}</h3>
                                        <div class="ratings d-flex">
                                            @if (!empty($bladeVar['productRatings']))
                                            @php
                                                $pratings = round($bladeVar['productRatings']['0']->ratings);
                                            @endphp
                                            @if($pratings > 0)
                                                    @for ($i = 1 ; $i <= 5 ; $i++)
                                                        @if ($i <= $pratings)
                                                        <span><i class="fa fa-star"></i></span>
                                                        @else
                                                        <span><i class="fa fa-star-o"></i></span>
                                                        @endif
                                                    @endfor 
                                                    
                                                    @if (!empty($bladeVar['productCommentsCount']))
                                                        <div class="pro-review">
                                                            <span>{{ $bladeVar['productCommentsCount']['0']->commentsCount }} Reviews</span>
                                                        </div>
                                                    @endif 
                                            @endif         
                                            @endif
                                        </div>
                                        <div class="price-box">
                                            @if(!empty($bladeVar['productDetail']['discounted_price']) && ($bladeVar['productDetail']['discounted_price'] != '0.00'))
                                            <span class="price-regular">${{ $bladeVar['productDetail']['discounted_price'] }}</span>
                                            <span class="price-old"><del>${{ $bladeVar['productDetail']['price'] }}</del></span>
                                            @else
                                            <span class="price-regular">${{ $bladeVar['productDetail']['price'] }}</span>
                                            @endif
                                        </div>
                                        <div class="availability">
                                            @if(!empty($bladeVar['productDetail']['product_quantity']) && ($bladeVar['productDetail']['product_quantity'] > 0))
                                                <i class="fa fa-check-circle"></i>
                                                <span>{{ $bladeVar['productDetail']['product_quantity'] }} in stock</span>
                                            @else
                                                <i class="fa fa-times-circle unavailability"></i>
                                                <span>out of stock</span>
                                            @endif
                                        </div>
                                        <p class="pro-desc">{{ $bladeVar['productDetail']['sort_details'] }}</p>
                                        <div class="quantity-cart-box d-flex align-items-center">
                                            <h6 class="option-title">qty:</h6>
                                            <div class="quantity">
                                                <div class="pro-qty"><input type="text" id="proQuantity" value="1"></div>
                                            </div>
                                            
                                            <div class="action_link">
                                                @if(!empty($bladeVar['productDetail']['product_quantity']) && ($bladeVar['productDetail']['product_quantity'] > 0))
                                                    @if(!empty($bladeVar['productDetail']['already_in_cart']) && ($bladeVar['productDetail']['already_in_cart'] == 'No'))
                                                        <a class="btn btn-cart2 addToCartPro" href="#" id="addtocart{{ $bladeVar['productDetail']['_id'] }}" data-pid="{{ $bladeVar['productDetail']['_id'] }}" data-cid="{{ $bladeVar['productDetail']['product_color_id'] }}" data-sid="{{ $bladeVar['productDetail']['product_size_id'] }}">Add To Cart</a>
                                                    @else
                                                        <a class="btn btn-cart2 addToCartPro disabled" href="{{ url('/cart') }}" id="addtocart{{ $bladeVar['productDetail']['_id'] }}" data-pid="{{ $bladeVar['productDetail']['_id'] }}">Go To Cart</a>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                        <div class="pro-size">
                                            <h6 class="option-title">size :</h6>
                                            <select class="nice-select" id="proSize">
                                            <?php 
                                            $sizeArray = array(); 
                                            $sizeArray[0] = $bladeVar['productDetail']['product_size_id'];
                                            if(!empty($bladeVar['productDetail']['variations'])){
                                                $variations = json_decode($bladeVar['productDetail']['variations']); 
                                                foreach($variations as $variation){
                                                    $sizeArray[] = $variation->size;
                                                }
                                            }
                                            $getSizeArray = array_unique($sizeArray);
                                            if(!empty($getSizeArray)){ 
                                                foreach($getSizeArray as $size){ 
                                                    $allSizes = $bladeVar['size_results'];
                                                    if(!empty($allSizes)){
                                                        foreach($allSizes as $getSize){
                                                            if($getSize->id == $size){ ?>
                                                                <option value="<?php echo $getSize->id; ?>"><?php echo $getSize->name; ?></option> 
                                                      <?php }
                                                        }
                                                    }
                                                }  
                                            } ?> 
                                            </select>
                                        </div>
                                        <div class="color-option">
                                            <h6 class="option-title">color :</h6>
                                            <?php
                                            $colorArray = array(); 
                                            $colorArray[0] = $bladeVar['productDetail']['product_color_id'];
                                            if(!empty($bladeVar['productDetail']['variations'])){
                                                $variations = json_decode($bladeVar['productDetail']['variations']); 
                                                foreach($variations as $variation){
                                                    $colorArray[] = $variation->color;
                                                }
                                            }
                                            $getColorArray = array_unique($colorArray);
                                            if(!empty($getColorArray)){ ?>
                                            <ul class="color-categories">
                                                <?php 
                                                $count = 0; foreach($getColorArray as $color){ 
                                                    $allColors = $bladeVar['color_results'];
                                                    if(!empty($allColors)){ 
                                                        foreach($allColors as $getColor){ 
                                                            if($getColor->id == $color){ $count++; ?>
                                                                <li class="<?php if($count == '1'){ echo 'colorActive'; } ?>">
                                                                <?php $varData = array();
                                                                $variations = json_decode($bladeVar['productDetail']['variations']); 
                                                                foreach($variations as $variation){
                                                                    if($getColor->id == $variation->color){
                                                                        $varData[] = $variation;
                                                                    }
                                                                }
                                                                ?>
                                                                    <a href="#" data-pcid="<?php echo $getColor->id; ?>" style="background-color: <?php echo $getColor->color_code; ?>;" title="<?php echo $getColor->name; ?>"></a>
                                                                </li>   
                                                            <?php }
                                                        }
                                                    }
                                                } ?>
                                            </ul>
                                            <?php } ?> 
                                        </div>
                                        <div class="useful-links">
                                            @if(!empty($bladeVar['productDetail']['already_in_Wishlist']) && ($bladeVar['productDetail']['already_in_Wishlist'] == 'No'))
                                                <a href="#" class="addtowishlist" id="addtowishlist{{ $bladeVar['productDetail']['_id'] }}" data-pid="{{ $bladeVar['productDetail']['_id'] }}" data-toggle="tooltip"><i class="pe-7s-like"></i>wishlist</a>
                                            @else
                                                <a href="#" class="addtowishlist" id="addtowishlist{{ $bladeVar['productDetail']['_id'] }}" data-pid="{{ $bladeVar['productDetail']['_id'] }}" data-toggle="tooltip"><i class="pe-7s-like addWishlist"></i>wishlist</a>
                                            @endif
                                        </div>
                                        <div class="like-icon">
                                            <a href="http://www.facebook.com/share.php?u={{ url('/shop/'.$bladeVar['productDetail']['slug']) }}&amp;title={{ $bladeVar['productDetail']['name'] }}" class="facebook fbclick"><i class="fa fa-facebook"></i>Share</a>
                                            <a href="https://twitter.com/intent/tweet?text={{ $bladeVar['productDetail']['name'] }}&amp;url={{ url('/shop/'.$bladeVar['productDetail']['slug']) }}" class="twitter fbclick"><i class="fa fa-twitter"></i>Share</a>
                                            <a href="{{ url('/shop/'.$bladeVar['productDetail']['slug']) }}" target="_blank" data-image="{{ $bladeVar['productDetail']['image'] }}" data-desc="{{ $bladeVar['productDetail']['name'] }}" class="pinterest btnPinIt"><i class="fa fa-pinterest"></i>Share</a>
                                            <a href="mailto:?subject=Jewellery Ecommerce: {{ $bladeVar['productDetail']['name'] }}&amp;body={{ url('/shop/'.$bladeVar['productDetail']['slug']) }}%0D{{ url('/') }}" class="envelope" title="{{ $bladeVar['productDetail']['name'] }}" style="background-color: #fe4419;"><i class="fa fa-envelope"></i>Share</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- product details inner end -->

                        <!-- product details reviews start -->
                        <div class="product-details-reviews section-padding pb-0">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="product-review-info">
                                        <ul class="nav review-tab">
                                            <li>
                                                <a class="active" data-toggle="tab" href="#tab_one">description</a>
                                            </li>
                                            <li>
                                                <a data-toggle="tab" href="#tab_two">information</a>
                                            </li>
                                            <li>
                                                <a data-toggle="tab" href="#tab_three">
                                                @if (!empty($bladeVar['productCommentsCount']))
                                                reviews ({{ $bladeVar['productCommentsCount']['0']->commentsCount }})
                                                @endif
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content reviews-tab">
                                            <div class="tab-pane fade show active" id="tab_one">
                                                <div class="tab-one">
                                                    <p>{{ $bladeVar['productDetail']['details'] }}</p>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab_two">
                                                <table class="table table-bordered">
                                                    <tbody>
                                                    @if (!empty($bladeVar['productDetail']['attribute']))
                                                        @php
                                                        $attributes = $bladeVar['productDetail']['attribute'];
                                                        @endphp
                                                        @foreach($attributes  as $attribute)
                                                        <tr>
                                                            <td>{{ $attribute->name }}</td>
                                                            <td>{{ $attribute->value }}</td>
                                                        </tr>
                                                        @endforeach
                                                    @endif    
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="tab-pane fade" id="tab_three">
                                                <form action="#" class="review-form" id="commentForm">
                                                    @if (!empty($bladeVar['productComments']))
                                                    <h5>{{ $bladeVar['productCommentsCount']['0']->commentsCount }} review for <span>{{ $bladeVar['productDetail']['name'] }}</span></h5>
                                                    @foreach($bladeVar['productComments']  as $comment)
                                                    @if ($comment->type == 'product')
                                                    <div class="total-reviews">
                                                        <div class="rev-avatar">
                                                            <img src="{{ asset('assets/img/about/avatar.jpg') }}" alt="{{ $comment->name }}">
                                                        </div>
                                                        <div class="review-box">
                                                            <div class="ratings">
                                                                @php
                                                                $ratings = $comment->website;
                                                                @endphp
                                                                @for ($i = 1 ; $i <= 5 ; $i++)
                                                                    @if ($i <= $ratings)
                                                                    <span><i class="fa fa-star"></i></span>
                                                                    @else
                                                                    <span><i class="fa fa-star-o"></i></span>
                                                                    @endif
                                                                @endfor
                                                            </div>
                                                            <div class="post-author">
                                                                <p><span>{{ $comment->name }} -</span> 
                                                                <?php $timestamp = strtotime($comment->updated_at); ?>
                                                                {{ date('d F, Y', $timestamp).' at '.date('g:i a', $timestamp) }}</p>
                                                            </div>
                                                            <p>{{ $comment->comment }}</p>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                    <p class="form-messege" id="responseMessage"></p>
                                                    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" id="pid" class="form-control" value="{{ $bladeVar['productDetail']['_id'] }}">
                                                    <input type="hidden" id="ptitle" class="form-control" value="{{ $bladeVar['productDetail']['name'] }}"> 
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label class="col-form-label"><span class="text-danger">*</span>Your Name</label>
                                                            <input type="text" name="name" id="name" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label class="col-form-label"><span class="text-danger">*</span>Your Email</label>
                                                            <input type="email" name="email" id="email" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label class="col-form-label"><span class="text-danger">*</span>Your Review</label>
                                                            <textarea class="form-control" name="commnet" id="comment"></textarea>
                                                            <div class="help-block pt-10"><span class="text-danger">Note:</span>
                                                                HTML is not translated!
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label class="col-form-label"><span class="text-danger">*</span>Rating</label>
                                                            &nbsp;&nbsp;&nbsp; Bad&nbsp;
                                                            <input type="radio" value="1" name="rating">
                                                            &nbsp;
                                                            <input type="radio" value="2" name="rating">
                                                            &nbsp;
                                                            <input type="radio" value="3" name="rating">
                                                            &nbsp;
                                                            <input type="radio" value="4" name="rating">
                                                            &nbsp;
                                                            <input type="radio" value="5" name="rating" checked>
                                                            &nbsp;Good
                                                        </div>
                                                    </div>
                                                    <div class="buttons">
                                                        <button class="btn btn-sqr" id="postComment" type="submit" name="submit">Continue</button>
                                                    </div>
                                                </form> <!-- end of review-form -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- product details reviews end -->
                    </div>
                    <!-- product details wrapper end -->
                </div>
            </div>
        </div>
        <!-- page main wrapper end -->

        <!-- related products area start -->
        @if (!empty($bladeVar['youMayLike']))
        <section class="related-products section-padding">
            <div class="container">
            @if (sizeof($bladeVar['youMayLike']) > 0)
                <div class="row">
                    <div class="col-12">
                        <!-- section title start -->
                        <div class="section-title text-center">
                            <h2 class="title">Related Products</h2>
                            <p class="sub-title">Add related products to weekly lineup</p>
                        </div>
                        <!-- section title start -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="product-carousel-4 slick-row-10 slick-arrow-style">
                            <!-- product item start -->
                            @foreach($bladeVar['youMayLike'] as $product)
							    @if($product->deleted_at == NULL)
                                    <div class="product-item">
                                        <figure class="product-thumb">
                                            <a href="{{ url('/shop/'.$product->slug) }}">
                                                <img class="pri-img" src="{{ asset('images/products').'/'.$product->image}}" alt="{{ $product->slug }}">
                                                <img class="sec-img" src="{{ asset('images/products').'/'.$product->image}}" alt="{{ $product->slug }}">
                                            </a>
                                            <div class="product-badge">
                                                @if(($product->new_arrival == '1') && ($product->on_sale == '1'))
                                                <div class="product-label new">
                                                    <span>new</span>
                                                </div>
                                                <div class="product-label discount">
                                                    <span>sale</span>
                                                </div>
                                                @elseif($product->new_arrival == '1')
                                                <div class="product-label new">
                                                    <span>new</span>
                                                </div>
                                                @elseif($product->on_sale == '1')
                                                <div class="product-label new">
                                                    <span>sale</span>
                                                </div>
                                                @endif
                                            </div>
                                            <div class="button-group">
                                            <?php 
                                            $arrayWishlist = array();
                                            if(!empty($bladeVar['userWishlistData'])){ 
                                                foreach($bladeVar['userWishlistData'] as $wishlist){
                                                    $arrayWishlist[] = $wishlist->product_id;
                                                } 
                                            } 
                                            if (in_array($product->_id, $arrayWishlist)) { ?>
                                                <a href="#" class="addtowishlist" id="addtowishlist{{ $product->_id }}" data-pid="{{ $product->_id }}" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like addWishlist"></i></a>
                                            <?php }else{ ?>
                                                <a href="#" class="addtowishlist" id="addtowishlist{{ $product->_id }}" data-pid="{{ $product->_id }}" data-toggle="tooltip" data-placement="left"><i class="pe-7s-like"></i></a>
                                            <?php } ?> 
                                            </div>
                                            @if(!empty($product->product_quantity) && ($product->product_quantity > 0))
                                                @if($product->add_to_cart == '1')
                                                <div class="cart-hover">
                                                    <?php $arrayCartList = array();
                                                    if(!empty($bladeVar['userCartData'])){ 
                                                        foreach($bladeVar['userCartData'] as $cartList){
                                                            $arrayCartList[] = $cartList->product_id;
                                                        } 
                                                    } 
                                                    if (in_array($product->_id, $arrayCartList)) { ?>
                                                        <button class="btn btn-cart addtocart disabled" id="addtocart{{ $product->_id }}" data-pid="{{ $product->_id }}">Go to cart</button>
                                                    <?php }else{ ?>
                                                        <button class="btn btn-cart addtocart" id="addtocart{{ $product->_id }}" data-pid="{{ $product->_id }}" data-cid="{{ $product->product_color_id }}" data-sid="{{ $product->product_size_id }}">add to cart</button>
                                                    <?php } ?>
                                                </div>
                                                @endif
                                            @else
                                                <div class="cart-hover">
                                                    <button class="btn btn-cart disabled">Out of stock</button>
                                                </div>
                                            @endif    
                                        </figure>
                                        <div class="product-caption text-center">
                                            <div class="product-identity">
                                                <p class="manufacturer-name"><a href="{{ url('/shop/?category='.$product->product_category->slug) }}">{{ $product->product_category->name }}</a></p>
                                            </div>
                                            <?php
                                                $colorArray = array(); 
                                                $colorArray[0] = $product->product_color_id;
                                                if(!empty($product->variations)){
                                                    $variations = json_decode($product->variations); 
                                                    foreach($variations as $variation){
                                                        $colorArray[] = $variation->color;
                                                    }
                                                }
                                                $getColorArray = array_unique($colorArray);
                                                if(!empty($getColorArray)){ 
                                                ?>
                                                <ul class="color-categories">
                                                <?php foreach($getColorArray as $color){ 
                                                    $allColors = $bladeVar['color_results'];
                                                    if(!empty($allColors)){
                                                        foreach($allColors as $getColor){
                                                            if($getColor->id == $color){ ?>
                                                                <li>
                                                                    <a href="#" style="background-color: <?php echo $getColor->color_code; ?>;" title="<?php echo $getColor->name; ?>"></a>
                                                                </li>   
                                                            <?php }
                                                        }
                                                    }
                                                } ?>
                                                </ul>
                                                <?php } ?>
                                            <h6 class="product-name">
                                                <a href="{{ url('/shop/'.$product->slug) }}">{{ $product->name }}</a>
                                            </h6>
                                            <div class="price-box">
                                                @if(!empty($product->discounted_price) && ($product->discounted_price != '0.00'))
                                                <span class="price-regular">${{ $product->discounted_price }}</span>
                                                <span class="price-old"><del>${{ $product->price }}</del></span>
                                                @else
                                                <span class="price-regular">${{ $product->price }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endif
						    @endforeach
                            <!-- product item end -->
                        </div>
                    </div>
                </div>
            @endif    
            </div>
        </section>
        @endif
        <!-- related products area end -->
    </main>
@endif    
@include('frontend.footer')
<script type="text/javascript">
$(document).ready(function() {
    var baseUrl = '{{ url("/") }}';
    function isEmail(emailid) {
		var pattern = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
		return pattern.test(emailid);
    }

    $("#postComment").on('click', function(event) {
		event.preventDefault();
        $('#responseMessage').hide().html('');
        var token = $('#_token').val();
		var getId = $('#commentForm #pid').val();
		var ptitle = $('#commentForm #ptitle').val();
		var name = $('#commentForm #name').val();
		var email = $('#commentForm #email').val();
        var rating = $("input[name='rating']:checked").val(); //$('#commentForm #rating').val();
		var comment = $('#commentForm #comment').val();
		if (name == '') {
			$('#commentForm #name').addClass('has-error');
		} else if (name != '') {
			$('#commentForm #name').removeClass('has-error');
		}
		if ((email == '') || (!isEmail($('#commentForm #email').val()))) {
			$('#commentForm #email').addClass('has-error');
		} else if ((email != '') && (isEmail($('#commentForm #email').val()))) {
			$('#commentForm #email').removeClass('has-error');
		}
        if (comment == '') {
			$('#commentForm #comment').addClass('has-error');
		} else if (comment != '') {
			$('#commentForm #comment').removeClass('has-error');
		}
		if ((name != '') && (email != '') && (isEmail($('#commentForm #email').val())) && (comment != '') && (rating != '')) {
			$.ajax({
				url: baseUrl + "/sendcomment",
				type: "POST",
				data: {
                    '_token': token,   
					'postId': getId,
					'ptitle': ptitle,
					'name': name,
					'email': email,
                    'website': rating,
					'comment': comment,
                    'type' : 'product'
				},
				dataType: "JSON",
				success: function(jsonStr) {
					var res_data = JSON.stringify(jsonStr);
					var response = JSON.parse(res_data);
					var responseData = response['responseData'];
					if ((responseData != null) && (responseData == 'comment send successfully')) {
						$('#responseMessage').show().html('').html('Your comment sent successfully');
                        $("#commentForm").trigger("reset");
                        setTimeout(function(){ location.reload(); }, 2000);
					} else {
						$('#responseMessage').show().html('').html(responseData);
					}
				}
			});
		}
    });

    $('.fbclick').click(function(event) {
        var width  = 575,
            height = 400,
            left   = ($(window).width()  - width)  / 2,
            top    = ($(window).height() - height) / 2,
            url    = this.href,
            opts   = 'status=1' +
                    ',width='  + width  +
                    ',height=' + height +
                    ',top='    + top    +
                    ',left='   + left;
        window.open(url, 'twitter', opts);
        return false;
    });	

    $('.btnPinIt').click(function() {
        var url = $(this).attr('href');
        var media = $(this).attr('data-image');
        var desc = $(this).attr('data-desc');
        window.open("//www.pinterest.com/pin/create/button/"+
        "?url="+url+
        "&media="+media+
        "&description="+desc,"pinIt","toolbar=no, scrollbars=no, resizable=no, top=0, right=0, width=750, height=320");
        return false;
    });
    
});
</script>