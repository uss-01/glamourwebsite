<div class="row">
	<div class="col-12">
		<form action="{{ route('color.update', $bladeVar['result']->id) }}" method="POST">
			{{ method_field('PUT') }}
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Color Name</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="name" value="{{ old('name', $bladeVar['result']->name) }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="color_code"><strong>Color Code</strong> <small class="text-danger">(required)</small></label>
				<input type="color" class="form-control" name="color_code" value="{{ old('color_code', $bladeVar['result']->color_code) }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
      	</form>
    </div>
</div>