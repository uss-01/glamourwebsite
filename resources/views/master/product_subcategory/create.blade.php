<div class="row">
	<div class="col-12">
		<form action="{{ route('product_subcategory.store') }}" method="POST">
		    <div class="form-group">
				<label class="form-control-label" for="code"><strong>Category</strong></label>
				<select name="product_category" class="form-control">
					<option value=""{{ empty(old('product_category')) ? ' selected="selected"' : '' }}></option>
				@foreach ($bladeVar['product_categories'] as $key => $product_category)
					<option value="{{ $product_category->_id }}"{{ $product_category->_id == old('product_category') ? ' selected="selected"' : '' }}>{{ $product_category->name }}</option>
				@endforeach
				</select>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Subcategory Name</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="name" value="{{ old('name') }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
		</form>
	</div>
</div>