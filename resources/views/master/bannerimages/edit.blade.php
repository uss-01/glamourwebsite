<div class="row">
	<div class="col-12">
		<form action="{{ route('bannerimages.update', $bladeVar['result']->id) }}" enctype="multipart/form-data" method="POST" id="brand_form">
			{{ method_field('PUT') }}
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Title</strong><small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="name" value="{{ old('name', $bladeVar['result']->name) }}"autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="product_category"><strong>Category</strong><small class="text-danger"> (required)</small></label>
				<select name="product_category" class="form-control">
					<option value=""{{ empty(old('product_category')) ? ' selected="selected"' : '' }}></option>
					@foreach ($bladeVar['product_categories'] as $key => $product_category)
					<option value="{{ $product_category->_id }}"{{ $product_category->_id == old('product_category',$bladeVar['result']->category) ? ' selected="selected"' : '' }}>{{ $product_category->name }}</option>
					@endforeach
				<option value="-1"{{ old('product_category')== -1 ? ' selected="selected"' : '' }}>Other</option>
				</select>
				<div class="other_option_wrap"></div>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="product_subcategory"><strong>Sub Category</strong></label>
				<select name="product_subcategory" class="form-control">
					<option value=""{{ empty(old('product_subcategory')) ? ' selected="selected"' : '' }}></option>
					@foreach ($bladeVar['product_subcategories'] as $key => $product_subcategory)
						<option value="{{ $product_subcategory->_id }}"{{ $product_subcategory->_id == old('product_subcategory',$bladeVar['result']->subcategory) ? ' selected="selected"' : '' }}>{{ $product_subcategory->name }}</option>
					@endforeach
				<option value="-1"{{ old('product_subcategory')== -1 ? ' selected="selected"' : '' }}>Other</option>
				</select>
				<div class="other_option_wrap2"></div>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="btntext"><strong>Button Text</strong></label>
				<input type="text" class="form-control" name="btntext" value="{{ old('btntext', $bladeVar['result']->btntext) }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="shortDescription"><strong>Short Description</strong></label>
				<textarea class="form-control" name="shortDescription">{{ old('shortDescription', $bladeVar['result']->shortDescription) }}</textarea>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="bannerPosition"><strong>Banner Position</strong></label>
				<select name="bannerPosition" class="form-control">
				    @if($bladeVar['result']->bannerPosition == 'text-center')         
						<option value="text-center" selected="selected">Default</option>          
					@else
					    <option value="text-center">Default</option>         
					@endif
					@if($bladeVar['result']->bannerPosition == 'text-left')         
						<option value="text-left" selected="selected">Left</option>          
					@else
					    <option value="text-left">Left</option>         
					@endif
					@if($bladeVar['result']->bannerPosition == 'text-right')         
						<option value="text-right" selected="selected">Right</option>          
					@else
					    <option value="text-right">Right</option>          
					@endif	
				</select>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="image"><strong>Banner Image</strong>
				 <small class="text-danger">(required)</small></label>
				<input type="file" name="image" class="form-control">
				<small class="form-control-feedback"></small>
			</div>
			 <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div> 
		</form>
	</div>
</div>