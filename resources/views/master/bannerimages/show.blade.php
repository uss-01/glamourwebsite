<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td>{{ $bladeVar['result']->id }}</td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Name</th>
		    		<td>{{ $bladeVar['result']->name }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Category</th>
		    		<td>{{ $bladeVar['result']->product_category->name }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Subcategory</th>
		    		<td>{{ $bladeVar['result']->product_subcategory->name }}</td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Button Text</th>
		    		<td>{{ $bladeVar['result']->btntext }}</td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Short Description</th>
		    		<td>{{ $bladeVar['result']->shortDescription }}</td>
				</tr>
				<tr>
		    		<th class="bg-faded">Banner Position</th>
		    		<td>
					@if($bladeVar['result']->bannerPosition == 'text-center')         
						Default                    
					@endif
					@if($bladeVar['result']->bannerPosition == 'text-left')         
						Left                
					@endif
					@if($bladeVar['result']->bannerPosition == 'text-right')         
						Right                
					@endif
					</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Image</th>
		    		<td><img src="{{ asset('images/cms').'/'.$bladeVar['result']->image }}" class="thumbnail" width="150" /></td>
		    	</tr>
		    </tbody>
    	</table>
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="{{ $bladeVar['result']->deleted_at == null ? route('bannerimages.edit', $bladeVar['result']->id) : '' }}" 
	    	data-destroy="{{ $bladeVar['result']->deleted_at == null ? route('bannerimages.destroy', $bladeVar['result']->id) : '' }}" 
	    	data-restore="{{ $bladeVar['result']->deleted_at != null ? route('bannerimages.restore', $bladeVar['result']->id) : '' }}"
	    	data-delete="{{ route('bannerimages.delete', $bladeVar['result']->id) }}">
	    </span>
	</div>
</div>	    