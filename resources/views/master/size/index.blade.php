@extends('layouts.app')
@section('content')
    <div class="container-fluid" id="pageContainer">
        <div class="row" id="pageContent">
            <div class="col-12 col-md-8 offset-md-2">
                <div class="float-right mt-2 hidden-xs-down">
                    <a href="{{ route('size.create') }}" class="btn btn-success" role="button" title="Create New Size" data-toggle="modal" data-target="#createModal"><i class="fa fa-plus"></i><span class="hidden-sm-down"> Create New</span></a>
                    <a href="{{ route('size.index') }}" class="btn btn-secondary btn-search" title="Search Size" data-search="{{ !empty(Request::input('q')) ? 'yes' : ''}}"><i class="fa fa-search"></i><span{!! !empty(Request::input('q')) ? '' : ' class="hidden-xs-up"' !!}> Close</span></a>
                    <a href="{{ route('size.index') }}" class="btn btn-secondary" role="button" title="Refresh Size List"><i class="fa fa-refresh"></i></a>
                </div>
                <div class="float-right mt-1 hidden-sm-up">
                    <a href="{{ route('size.create') }}" class="btn btn-success btn-sm" role="button" title="Create New Size" data-toggle="modal" data-target="#createModal"><i class="fa fa-plus"></i></a>
                    <a href="{{ route('size.index') }}" class="btn btn-secondary btn-sm btn-search" title="Search Size" data-search="{{ !empty(Request::input('q')) ? 'yes' : ''}}"><i class="fa fa-search"></i><span{!! !empty(Request::input('q')) ? '' : ' class="hidden-xs-up"' !!}> Close</span></a>
                    <a href="{{ route('size.index') }}" class="btn btn-secondary btn-sm" role="button" title="Refresh Size List"><i class="fa fa-refresh"></i></a>
                </div>
                <h1 class="hidden-xs-down">{{ Request::input('q') ? 'Search ' : '' }}Size <span class="text-muted h5">(total {{ number_format($bladeVar['total_count']) }})</span></h1>
                <h1 class="hidden-sm-up h3">{{ Request::input('q') ? 'Search ' : '' }}Size <br><span class="text-muted h6">(total {{ number_format($bladeVar['total_count']) }})</span></h1>
                <div class="clearfix alertArea"></div>
                <div class="card mb-2 searchContainer{{ Request::input('q') ? '' : ' hidden-xs-up'}}">
                    <div class="card-block">
                        <form method="get" action="{{ route('size.search') }}" class="m0 p0 form-search">
                            <div class="form-group mb-0">
                                <label class="form-control-label" for="q"><strong>Search by size name</strong></label>
                                <input type="text" class="form-control" name="q" value="{{ Request::input('q') }}">
                                <small class="form-text text-muted">Type size name and hit "Enter" key to search</small>
                            </div>
                        </form>
                    </div>
                </div>
                @if ($bladeVar['total_count'] > 0)
                    <table class="table table-bordered table-hover table-sm">
                        <thead class="bg-faded">
                            <tr>
                                <th class="text-center" style="width: 4rem;"><nobr><a href="{{ url()->current() }}?{{ Request::input('q') ? 'q='.Request::input('q').'&' : '' }}sort=id&dir={{ Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'desc' : 'asc' : 'asc' }}">Id</a> <i class="fa fa-{{ Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'sort-numeric-asc' : 'sort-numeric-desc' : 'sort' }}"></i></nobr></th>
                                <th><nobr><a href="{{ url()->current() }}?{{ Request::input('q') ? 'q='.Request::input('q').'&' : '' }}sort=name&dir={{ Request::input('sort') == 'name' ? Request::input('dir') == 'asc' ? 'desc' : 'asc' : 'asc' }}">Name</a> <i class="fa fa-{{ Request::input('sort') == 'name' ? Request::input('dir') == 'asc' ? 'sort-alpha-asc' : 'sort-alpha-desc' : 'sort' }}"></i></nobr></th>
                            
                                <th class="w-td-action">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($bladeVar['results'] as $rs)
                                <tr{!! $rs->deleted_at != null ? ' class="table-danger"' : '' !!}>
                                    <td class="text-center"><span{!! $rs->deleted_at != null ? ' class="badge badge-danger"' : '' !!}>{{ $rs->id }}</span></td>
                                    <td><a href="{{ route('size.show', $rs->id) }}" title="View" data-toggle="modal" data-target="#viewModal">{{ $rs->name }}</a></td>
                                   
                                    <td class="text-center text-sm-left">
                                        <div class="hidden-xs-down">
                                            <a href="{{ route('size.show', $rs->id) }}" class="btn btn-sm btn-info" title="View" data-toggle="modal" data-target="#viewModal"><i class="fa fa-eye"></i></a> 
                                            <a href="{{ route('size.edit', $rs->id) }}" class="btn btn-sm btn-info{{ $rs->deleted_at != null ? ' disabled' : '' }}" title="Edit" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i></a>
                                            @if ($rs->deleted_at != null)
                                                <a href="{{ route('size.restore', $rs->id) }}" class="btn btn-sm btn-success btn-restore" title="Restore"><i class="fa fa-undo"></i></a>
                                            @else
                                                <a href="{{ route('size.destroy', $rs->id) }}" class="btn btn-sm btn-warning btn-destroy" title="Delete temporarly"><i class="fa fa-trash"></i></a>
                                            @endif
                                            <a href="{{ route('size.delete', $rs->id) }}" class="btn btn-sm btn-danger btn-delete" title="Delete permanently"><i class="fa fa-trash"></i></a>
                                        </div>
                                        <div class="btn-group hidden-sm-up">
                                            <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Actions
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a href="{{ route('size.show', $rs->id) }}" class="dropdown-item" data-toggle="modal" data-target="#viewModal"><i class="fa fa-eye text-info"></i> View</a>
                                                <a href="{{ route('size.edit', $rs->id) }}" class="dropdown-item{{ $rs->deleted_at != null ? ' disabled' : '' }}" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil text-{{ $rs->deleted_at != null ? 'muted' : 'info' }}"></i> Edit</a>
                                                @if ($rs->deleted_at != null)
                                                    <a href="{{ route('size.restore', $rs->id) }}" class="dropdown-item btn-restore"><i class="fa fa-undo text-success"></i> Restore</a>
                                                @else
                                                    <a href="{{ route('size.destroy', $rs->id) }}" class="dropdown-item btn-destroy"><i class="fa fa-trash text-warning"></i> Delete temporarly</a>
                                                @endif
                                                <a href="{{ route('size.delete', $rs->id) }}" class="dropdown-item btn-delete"><i class="fa fa-trash text-danger"></i> Delete permanently</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-4">
                        {{ $bladeVar['results']->appends(Request::query())->links() }}
                        <small class="d-block text-center text-muted">(showing 20 results per page)</small>
                    </div>
                @else
                    <div class="alert alert-success">
                        <strong>No records found!</strong>
                        @if (!empty(Request::input('q')))
                            No matching your search criteria. 
                        @else 
                            No Sizes added yet, please click on "Create New" button to add a size. 
                        @endif
                    </div>
                @endif
                <!-- Deletetion code with Form -->
                <form class="formDelete">
                    {{ method_field('DELETE') }}
                </form>

                <!-- View modal code: START -->
                <div class="modal" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="viewModalTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="viewModalTitle">View Size</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            </div>
                            <div class="modal-footer hidden-xs-up">
                                <a href="javascript:void(0);" class="btn btn-info btn-edit" title="Edit" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i> Edit</a>
                                <a href="javascript:void(0);" class="btn btn-warning btn-destroy" title="Delete temporarly"><i class="fa fa-trash"></i></a>
                                <a href="javascript:void(0);" class="btn btn-success btn-restore" title="Restore"><i class="fa fa-undo"></i></a>
                                <a href="javascript:void(0);" class="btn btn-danger btn-delete" title="Delete permanently"><i class="fa fa-trash"></i></a>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- View modal code: END -->

                <!-- Edit modal code: START -->
                <div class="modal" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="editModalTitle">Edit Size</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            </div>
                            <!-- <div class="modal-footer hidden-xs-up">
                                <button type="button" class="btn btn-success btn-submit">Submit</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div> -->
                        </div>
                    </div>
                </div>
                <!-- Edit modal code: END -->

                <!-- Add modal code: START -->
                <div class="modal" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="createModalTitle">Add Size</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            </div>
                            <!-- <div class="modal-footer hidden-xs-up">
                                <button type="button" class="btn btn-success btn-submit">Submit</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div> -->
                        </div>
                    </div>
                </div>
                <!-- Add modal code: END -->
            </div>
        </div>
    </div>
@endsection

@section('foot')
    <script type="text/javascript">
        var pageUrl = '{{ route('size.index') }}';
        var ajaxLoadUrl = '{{ url()->full() }}';
        var pageContainer = '#pageContainer';
        var pageContent = '#pageContent';
    </script>
    <script src="{{ asset('js/jquery.form.js') }}"></script>
    <script src="{{ asset('js/gallery/bannerimages.js?v=1') }}"></script>
@endsection