<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td>{{ $bladeVar['result']->id }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Size Name</th>
		    		<td>{{ $bladeVar['result']->name }}</td>
		    	</tr>
		    </tbody>
    	</table>
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="{{ $bladeVar['result']->deleted_at == null ? route('size.edit', $bladeVar['result']->id) : '' }}" 
	    	data-destroy="{{ $bladeVar['result']->deleted_at == null ? route('size.destroy', $bladeVar['result']->id) : '' }}" 
	    	data-restore="{{ $bladeVar['result']->deleted_at != null ? route('size.restore', $bladeVar['result']->id) : '' }}"
	    	data-delete="{{ route('size.delete', $bladeVar['result']->id) }}">
	    </span>
	</div>
</div>	    