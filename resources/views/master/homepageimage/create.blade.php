<div class="row">
	<div class="col-12">
		<form action="{{ route('homepageimage.store') }}" enctype="multipart/form-data" method="POST" id="brand_form">
			<div class="form-group">
				<label class="form-control-label" for="title"><strong>Title</strong><small class="text-danger"> (required)</small></label>
				<input type="text" class="form-control" name="title" value="{{ old('title') }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="shortDescription"><strong>Short Description</strong></label>
				<textarea class="form-control" name="shortDescription">{{ old('shortDescription') }}</textarea>
				<small class="form-control-feedback"></small>
			</div> 
			<div class="form-group">
				<label class="form-control-label" for="image"><strong>Image</strong>
				<small class="text-danger"> (required)</small></label>
				<input type="file" name="image" class="form-control">
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="slidePosition"><strong>Slide Position</strong></label>
				<select name="slidePosition" class="form-control">
					<option value="slide-3">Default</option>
					<option value="slide-1">Left</option>
					<option value="slide-2 float-md-right float-none">Right</option>
				</select>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="redirect_url"><strong>Rdirect Url</strong></label>
				<input type="text" name="redirect_url" class="form-control">
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="btntext"><strong>Button Text</strong></label>
				<input type="text" name="btntext" class="form-control">
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div> 
		</form>
	</div>
</div>