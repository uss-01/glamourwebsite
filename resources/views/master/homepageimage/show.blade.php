<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td>{{ $bladeVar['result']->id }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Title</th>
		    		<td>{{ $bladeVar['result']->title }}</td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Short Description</th>
		    		<td>{{ $bladeVar['result']->shortDescription }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Image</th>
		    		<td><img src="{{ asset('images/cms').'/'.$bladeVar['result']->image }}" class="thumbnail" width="150" /></td>
				</tr>
				<tr>
		    		<th class="bg-faded">Slide Position</th>
		    		<td>
					@if($bladeVar['result']->slidePosition == 'slide-3')         
						Default                    
					@endif
					@if($bladeVar['result']->slidePosition == 'slide-1')         
						Left                
					@endif
					@if($bladeVar['result']->slidePosition == 'slide-2 float-md-right float-none')         
						Right                
					@endif
					</td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Rdirect Url</th>
		    		<td>{{ $bladeVar['result']->redirect_url }}</td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Button Text</th>
		    		<td>{{ $bladeVar['result']->btntext }}</td>
		    	</tr>
		    </tbody>
    	</table>
		<span class="modalUrls hidden-xs-up" 
	    	data-edit="{{ $bladeVar['result']->deleted_at == null ? route('homepageimage.edit', $bladeVar['result']->id) : '' }}" 
	    	data-destroy="{{ $bladeVar['result']->deleted_at == null ? route('homepageimage.destroy', $bladeVar['result']->id) : '' }}" 
	    	data-restore="{{ $bladeVar['result']->deleted_at != null ? route('homepageimage.restore', $bladeVar['result']->id) : '' }}"
	    	data-delete="{{ route('homepageimage.delete', $bladeVar['result']->id) }}">
	    </span>
	</div>
</div>	    