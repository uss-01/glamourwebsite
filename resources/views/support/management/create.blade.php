<div class="row">
	<div class="col-12">
		<form action="{{ route('management.store') }}" enctype="multipart/form-data" method="POST" id="brand_form">
		  <div class="row">
			<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="email"><strong>Email</strong><!--<small class="text-danger">(required)</small>--></label>
				<input type="text" class="form-control" name="email" value="{{ old('email') }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			</div>
			</div>
			 <div class="form-group text-right">
     			  <button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
		</form>
	</div>
</div>