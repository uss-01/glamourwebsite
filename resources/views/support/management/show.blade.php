<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td>{{ $bladeVar['result']->_id }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Email</th>
		    		<td>{{ $bladeVar['result']->email }}</td>
		    	</tr>
		    </tbody>
    	</table>
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="{{ route('management.edit', $bladeVar['result']->_id) }}"
	    	data-delete="{{ route('management.delete', $bladeVar['result']->_id) }}">
	    </span>
	</div>
</div>	    