<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td>{{ $bladeVar['result']->_id }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Name</th>
		    		<td>{{ $bladeVar['result']->name }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Email</th>
		    		<td>{{ $bladeVar['result']->email }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Phone</th>
		    		<td>{{ $bladeVar['result']->phone }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Message</th>
		    		<td>{{ strip_tags($bladeVar['result']->message) }}</td>
		    	</tr>
		    	@if(!empty($bladeVar['result']->image1))
		    	<tr>
		    		<th class="bg-faded">Image</th>
		    		<td><img src="{{ asset('images/support/lostfound').'/'.$bladeVar['result']->image1 }}" class="thumbnail" width="150" /> 
		    		</td>
				</tr>
				@endif
				@if(!empty($bladeVar['result']->image2))
				<tr>
		    		<th class="bg-faded">Image</th>
		    		<td><img src="{{ asset('images/support/lostfound').'/'.$bladeVar['result']->image2 }}" class="thumbnail" width="150" /> 
		    		</td>
				</tr>
				@endif
				@if(!empty($bladeVar['result']->image3))
                <tr>
		    		<th class="bg-faded">Image</th>
		    		<td><img src="{{ asset('images/support/lostfound').'/'.$bladeVar['result']->image3 }}" class="thumbnail" width="150" /> 
		    		</td>
		    	</tr> 
                @endif

		    	
		    </tbody>
    	</table>
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="{{ $bladeVar['result']->deleted_at == null ? route('lostfound.edit', $bladeVar['result']->_id) : '' }}" 
	    	data-destroy="{{ $bladeVar['result']->deleted_at == null ? route('lostfound.destroy', $bladeVar['result']->_id) : '' }}" 
	    	data-restore="{{ $bladeVar['result']->deleted_at != null ? route('lostfound.restore', $bladeVar['result']->_id) : '' }}"
	    	data-delete="{{ route('lostfound.delete', $bladeVar['result']->_id) }}">
	    </span>
	</div>
</div>	    