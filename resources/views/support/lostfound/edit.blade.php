<div class="row">
	<div class="col-12">
		<form action="{{ route('lostfound.update', $bladeVar['result']->_id) }}" method="POST">
			{{ method_field('PUT') }}
			 <div class="row">
			<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Name</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="name" value="{{ old('name', $bladeVar['result']->name) }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			</div>
			<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="code"><strong>Type</strong></label>
				<select name="type" class="form-control">
					<option value=""{{ empty(old('type')) ? ' selected="selected"' : '' }}></option>
				
					<option value="1"{{ 1 == old('type', $bladeVar['result']->type) ? ' selected="selected"' : '' }}>Lost</option>
				    <option value="2"{{ 2 == old('type', $bladeVar['result']->type) ? ' selected="selected"' : '' }}>Found</option>
				</select>
				<small class="form-control-feedback"></small>
			</div>
			
			</div>
			</div>
			<div class="row">
			<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="phone"><strong>Phone</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="phone" value="{{ old('phone', $bladeVar['result']->phone) }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			</div>
			<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="email"><strong>Email</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="email" value="{{ old('email', $bladeVar['result']->email) }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>
            
            </div>
            </div>
            <div class="form-group">

				<label class="form-control-label" for="image"><strong>Image</strong> <small class="text-danger">(required)</small></label>
				<input type="file" name="image" class="form-control">
				<small class="form-control-feedback"></small>
			</div>
			
			<div class="form-group">
				<label class="form-control-label" for="message"><strong>Message</strong></label>
				<textarea class="form-control"  name="message">{{ old('message',$bladeVar['result']->message) }}</textarea>
				<small class="form-control-feedback"></small>
			</div>
			 <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
			
      	</form>
    </div>
</div>
