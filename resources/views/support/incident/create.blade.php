<div class="row">
	<div class="col-12">
		<form action="{{ route('incident.store') }}" enctype="multipart/form-data" method="POST" id="brand_form">
		  <div class="row">
			<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="title"><strong>Title</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="title" value="{{ old('title') }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			</div>
			<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="image"><strong>Event Image</strong> <small class="text-danger">(required)</small></label>
				<input type="file" name="image" class="form-control">
				<small class="form-control-feedback"></small>
			</div>
			</div>
			</div>
			<div class="row">
			<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="start_date"><strong>Start Date</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control bg-ffffff" name="start_date" value="{{ old('start_date') }}" readonly>
					<input type="hidden" name="start_date_alt">
				<small class="form-control-feedback"></small>
			</div>
			</div>
			<div class="col-6">
			<div class="form-group">
				<label class="form-control-label" for="end_date"><strong>End Date</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control bg-ffffff" name="end_date" value="{{ old('end_date') }}" readonly>
					<input type="hidden" name="end_date_alt">
				<small class="form-control-feedback"></small>
			</div>
			</div>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="details"><strong>Details</strong></label>
				<textarea class="form-control" id="htmlTextEditor" name="details">{{ old('details') }}</textarea>
				<small class="form-control-feedback"></small>
			</div>
			 <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
			
        
		</form>
	</div>
</div>
<script type="text/javascript">
     $(document).ready(function() {
	$('#htmlTextEditor').summernote({
              height:300,
              dialogsInBody: true
            });
  });
</script>