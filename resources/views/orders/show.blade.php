@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2">
                <h1 class="hidden-xs-down">User Details</h1>
                <h1 class="hidden-sm-up h3">User Details</h1>
                <div class="clearfix alertArea"></div>
                <table class="table table-bordered table-sm mb-0">
                    <tbody>
                        <tr>
                            <th class="bg-faded">User Name</th>
                            <td> {{ $bladeVar['userdetail']->firstname }} {{ $bladeVar['userdetail']->lastname }}</td>
                        </tr>
                        <tr>
                            <th class="bg-faded">Contact Email</th>
                            <td> {{ $bladeVar['userdetail']->email }}</td>
                        </tr>
                        <tr>
                            <th class="bg-faded">Contact Number</th>
                            <td> {{ $bladeVar['userdetail']->phone }}</td>
                        </tr>
                        <tr>
                            <th class="bg-faded">Address</th>
                            <td> {{ $bladeVar['userdetail']->address }}</td>
                        </tr>
                        <tr>
                            <th class="bg-faded">Payment Method</th>
                            <td> {{ $bladeVar['userdetail']->payment_method }}</td>
                        </tr>
                        <tr>
                            <th class="bg-faded">Total Price</th>
                            <td>$ {{ $bladeVar['userdetail']->amount }}</td>
                        </tr>
                        <tr>
                            <th class="bg-faded">Shipping Charges</th>
                            <td>$ {{ $bladeVar['userdetail']->shippingCharge }}</td>
                        </tr>
                        <tr>
                            <th class="bg-faded">Order Date</th>
                            <td> {{ date('d F Y', strtotime($bladeVar['userdetail']->created_at)) }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="container-fluid" id="pageContainer">
        <div class="row" id="pageContent">
            <div class="col-12 col-md-8 offset-md-2">
                <h1 class="hidden-xs-down">{{ Request::input('q') ? 'Search ' : '' }}Order Details <span class="text-muted h5">(total {{ number_format($bladeVar['total_count']) }})</span></h1>
                <h1 class="hidden-sm-up h3">{{ Request::input('q') ? 'Search ' : '' }}Order Details<br><span class="text-muted h6">(total {{ number_format($bladeVar['total_count']) }})</span></h1>
                <div class="clearfix alertArea"></div>
                <div class="card mb-2 searchContainer{{ Request::input('q') ? '' : ' hidden-xs-up'}}">
                    <div class="card-block">
                        <form method="get" action="{{ route('orders.search') }}" class="m0 p0 form-search">
                            <div class="form-group mb-0">
                                <label class="form-control-label" for="q"><strong>Search by Order name</strong></label>
                                <input type="text" class="form-control" name="q" value="{{ Request::input('q') }}">
                                <small class="form-text text-muted">Type order name and hit "Enter" key to search</small>
                            </div>
                        </form>
                    </div>
                </div>
                @if ($bladeVar['total_count'] > 0)
                    <table class="table table-bordered table-hover table-sm">
                        <thead class="bg-faded">
                             <tr>
                                <th class="text-center" style="width: 4rem;"><nobr><a href="{{ url()->current() }}?{{ Request::input('q') ? 'q='.Request::input('q').'&' : '' }}sort=id&dir={{ Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'desc' : 'asc' : 'asc' }}">Id</a> <i class="fa fa-{{ Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'sort-numeric-asc' : 'sort-numeric-desc' : 'sort' }}"></i></nobr></th>
                                <th><nobr>Name</nobr></th>
                                <th><nobr>Image</nobr></th>
								<th><nobr>Details</nobr></th>
								<th><nobr>Price</nobr></th>
								<th><nobr>Quantity</nobr></th>
                                <th><nobr>Status</nobr></th>
                                <th class="w-td-action">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($bladeVar['results'] as $rs)
                            <tr{!! $rs->deleted_at != null ? ' class="table-danger"' : '' !!}>
                                    <td class="text-center"><span{!! $rs->deleted_at != null ? ' class="badge badge-danger"' : '' !!}>{{ $rs->orderUniqid }}</span></td>
									<td>{{ $rs->product_name }}</td>
									<td><img src="{{ asset('images/products/thumbnails').'/'.$rs->product_image }}" class="thumbnail" width="80" /></td>
									<td>{{ $rs->product_details }}</td>
									<td>$ {{ $rs->price }}</td>
                                    <td>{{ $rs->quantity }}</td>
                                    <td>{{ $rs->order_status }}</td>
                                    <td class="text-center text-sm-left">
                                        <div class="hidden-xs-down">
										<a href="{{ route('orders.edit', $rs->_id) }}" class="btn btn-sm btn-info{{ $rs->deleted_at != null ? ' disabled' : '' }}" title="Edit" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i></a>
                                        </div>
                                        <div class="btn-group hidden-sm-up">
                                            <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Actions
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a href="{{ route('orders.edit', $rs->_id) }}" class="dropdown-item{{ $rs->deleted_at != null ? ' disabled' : '' }}" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil text-{{ $rs->deleted_at != null ? 'muted' : 'info' }}"></i> Edit</a> 
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-4">
                        {{ $bladeVar['results']->appends(Request::query())->links() }}
                        <small class="d-block text-center text-muted">(showing 20 results per page)</small>
                    </div>
                @else
                    <div class="alert alert-success">
                        <strong>No records found!</strong>
                        @if (!empty(Request::input('q')))
                            No matching your search criteria. 
                        @else 
                            No order yet 
                        @endif
                    </div>
				@endif
				
				<!-- Edit modal code: START -->
				<div class="modal" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalTitle" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="editModalTitle">Order Details</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            </div>
                            <div class="modal-footer hidden-xs-up">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Edit modal code: END -->

            </div>
        </div>
    </div>
@endsection

@section('foot')
    <script type="text/javascript">
        var pageUrl = '{{ route('orders.index') }}';
        var ajaxLoadUrl = '{{ url()->full() }}';
        var pageContainer = '#pageContainer';
        var pageContent = '#pageContent';
    </script>
     <script src="{{ asset('js/jquery.form.js') }}"></script>
	<script src="{{ asset('js/orders.js?v=1') }}"></script>
@endsection