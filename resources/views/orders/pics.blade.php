<div class="row">
	<div class="col-12">
		<form action="{{ route('product.savePics') }}" enctype="multipart/form-data" method="POST" id="product_pics_form">
			<div class="form-group">
				<label class="form-control-label" for="image"><strong>Pics (Size 990X990 px)</strong> <small class="text-danger">(required)</small></label>
				<input type="file" name="image[]" class="form-control" multiple="">
				<small class="form-control-feedback"></small>
				<input type="hidden" name="product" value="{{ $bladeVar['result']->_id }}">
			</div>
			 <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
			
        
		</form>
	</div>

</div>
<div class="row">
<div class="col-12">
<h5>Product Images</h5>
 <div class="clearfix alertArea"></div>
</div>

@foreach ($bladeVar['productPics'] as $pic) 
<div class="col-3"> 
<div class="thumbnail">
<img src="{{ asset('images/products').'/'.$pic->image }}" width="100" />
<div class="caption text-center">
<br/>
<a href="{{ route('product.deletePic', $pic->_id) }}" class="btn btn-sm btn-danger btn-delete" title="Delete permanently"><i class="fa fa-remove"></i></a>
</div>
</div>
</div>
@endforeach

</div>