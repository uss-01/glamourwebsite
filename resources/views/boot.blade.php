@extends('layouts.app')
@section('content')
<div class="container">
	@if (session('flushcache'))
	    <div class="alert alert-ajax alert-success fade show mb-2">
	    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        {!! session('flushcache') !!}
	    </div>
	@endif

	<div class="jumbotron" style="margin-top: 13%; margin-bottom: 13%;">
		<h1 class="text-center display-4">
			<span class="text-muted d-block">Signed in as {{ Auth::user()->name }}</span>
		</h1>
	</div>
</div>
@endsection
