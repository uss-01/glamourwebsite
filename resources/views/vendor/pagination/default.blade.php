
<nav aria-label="Page navigation">
    <ul class="pagination justify-content-center pagination-sm mb-2">
        {{-- Previous Page Link --}}
        
        <li class="page-item{{ ($paginator->onFirstPage() ? ' disabled' : '') }}"><a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="page-link" title="Previous">&larr; <span class="hidden-sm-down">Previous</span></a></li>

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="page-item disabled"><a href="javascript:void(0);" class="page-link">{{ $element }}</a></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    <li class="page-item{{ ($page == $paginator->currentPage() ? ' active' : '') }}">
                        <a href="{{ $url }}" class="page-link">
                            {{ $page }}
                            @if ($page == $paginator->currentPage())
                                <span class="sr-only">(current)</span>
                            @endif
                        </a>
                    </li>
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        <li class="page-item{{ ($paginator->hasMorePages() ? '' : ' disabled') }}"><a href="{{ $paginator->nextPageUrl() }}" rel="next" class="page-link" title="Next"><span class="hidden-sm-down">Next</span> &rarr;</a></li>
    </ul>
</nav>
