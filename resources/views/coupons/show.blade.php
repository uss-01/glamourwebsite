<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td>{{ $bladeVar['result']->id }}</td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Coupon Name</th>
		    		<td>{{ $bladeVar['result']->name }}</td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Coupon Type</th>
		    		<td>{{ $bladeVar['result']->type }}</td>
				</tr>
				<tr>
		    		<th class="bg-faded">Coupon Value</th>
		    		<td>{{ $bladeVar['result']->value }}</td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Coupon Quantity</th>
		    		<td>{{ $bladeVar['result']->quantity }}</td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Coupon Expiry</th>
		    		<td>{{ $bladeVar['result']->expiry }}</td>
		    	</tr>
		    </tbody>
    	</table>
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="{{ $bladeVar['result']->deleted_at == null ? route('coupons.edit', $bladeVar['result']->id) : '' }}" 
	    	data-destroy="{{ $bladeVar['result']->deleted_at == null ? route('coupons.destroy', $bladeVar['result']->id) : '' }}" 
	    	data-restore="{{ $bladeVar['result']->deleted_at != null ? route('coupons.restore', $bladeVar['result']->id) : '' }}"
	    	data-delete="{{ route('coupons.delete', $bladeVar['result']->id) }}">
	    </span>
	</div>
</div>	    