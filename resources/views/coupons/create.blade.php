<div class="row">
	<div class="col-12">
		<form action="{{ route('coupons.store') }}" enctype="multipart/form-data" method="POST" id="brand_form">
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Coupon Name</strong><small class="text-danger"> (required)</small></label>
				<input type="text" class="form-control" name="name" value="{{ old('name') }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="product_status"><strong>Coupon Type</strong></label>
				<select name="type" class="form-control">
					<option value=""{{ empty(old('type')) ? ' selected="selected"' : '' }}></option>
					@foreach ($bladeVar['couponType'] as $key => $type)
						<option value="{{ $type }}"{{ $type == old('type') ? ' selected="selected"' : '' }}>{{ $type }}</option>
					@endforeach
				</select>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="value"><strong>Coupon Value</strong></label>
				<input type="text" class="form-control" name="value" value="{{ old('value') }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="quantity"><strong>Coupon Quantity</strong></label>
				<input type="text" class="form-control" name="quantity" value="{{ old('quantity') }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="expiry"><strong>Coupon Expiry</strong></label>
				<input type="text" class="form-control" id="expiry" name="expiry" value="{{ old('expiry') }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div> 
		</form>
	</div>
</div>
<script type="text/javascript">
$('#expiry').datetimepicker();
</script>  