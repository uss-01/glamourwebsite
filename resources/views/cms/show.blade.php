<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td>{{ $bladeVar['result']->_id }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Page Name</th>
		    		<td>{{ $bladeVar['result']->name }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Page Title</th>
		    		<td>{{ $bladeVar['result']->title }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Slug</th>
		    		<td>{{ $bladeVar['result']->slug }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Image</th>
		    		<td>@if($bladeVar['result']->image)
						<img src="{{ asset('images/cms').'/'.$bladeVar['result']->image }}" class="thumbnail" width="150" />
						@else
						No Image
						@endif
		    		</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Content</th>
		    		<td>{!! $bladeVar['result']->content !!}</td>
		    	</tr>
		    	
		    	
		    </tbody>
    	</table>
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="{{ $bladeVar['result']->deleted_at == null ? route('cms.edit', $bladeVar['result']->_id) : '' }}" 
	    	data-destroy="{{ $bladeVar['result']->deleted_at == null ? route('cms.destroy', $bladeVar['result']->_id) : '' }}" 
	    	data-restore="{{ $bladeVar['result']->deleted_at != null ? route('cms.restore', $bladeVar['result']->_id) : '' }}"
	    	data-delete="{{ route('cms.delete', $bladeVar['result']->_id) }}">
	    </span>
	</div>
</div>	    