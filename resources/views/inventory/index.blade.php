@extends('layouts.app')
@section('content')
    <div class="container-fluid" id="pageContainer">
        <div class="row" id="pageContent">
            <div class="col-12 col-md-8 offset-md-2">
                <div class="float-right mt-2 hidden-xs-down">
                    <a href="{{ route('inventory.index') }}" class="btn btn-secondary btn-search" title="Search Inventory" data-search="{{ !empty(Request::input('q')) ? 'yes' : ''}}"><i class="fa fa-search"></i><span{!! !empty(Request::input('q')) ? '' : ' class="hidden-xs-up"' !!}> Close</span></a>
                    <a href="{{ route('inventory.index') }}" class="btn btn-secondary" role="button" title="Refresh Inventory List"><i class="fa fa-refresh"></i></a>
                </div>
                <div class="float-right mt-1 hidden-sm-up">
                    <a href="{{ route('inventory.index') }}" class="btn btn-secondary btn-sm btn-search" title="Search Inventory" data-search="{{ !empty(Request::input('q')) ? 'yes' : ''}}"><i class="fa fa-search"></i><span{!! !empty(Request::input('q')) ? '' : ' class="hidden-xs-up"' !!}> Close</span></a>
                    <a href="{{ route('inventory.index') }}" class="btn btn-secondary btn-sm" role="button" title="Refresh Inventory List"><i class="fa fa-refresh"></i></a>
                </div>
                <h1 class="hidden-xs-down">{{ Request::input('q') ? 'Search ' : '' }}Inventory <span class="text-muted h5">(total {{ number_format($bladeVar['total_count']) }})</span></h1>
                <h1 class="hidden-sm-up h3">{{ Request::input('q') ? 'Search ' : '' }}Inventory <br><span class="text-muted h6">(total {{ number_format($bladeVar['total_count']) }})</span></h1>
                <div class="clearfix alertArea"></div>
                <div class="card mb-2 searchContainer{{ Request::input('q') ? '' : ' hidden-xs-up'}}">
                    <div class="card-block">
                        <form method="get" action="{{ route('inventory.search') }}" class="m0 p0 form-search">
                            <div class="form-group mb-0">
                                <label class="form-control-label" for="q"><strong>Search by Inventory name</strong></label>
                                <input type="text" class="form-control" name="q" value="{{ Request::input('q') }}">
                                <small class="form-text text-muted">Type inventory name and hit "Enter" key to search</small>
                            </div>
                        </form>
                    </div>
                </div>
                @if ($bladeVar['total_count'] > 0)
                    <table class="table table-bordered table-hover table-sm">
                        <thead class="bg-faded">
                             <tr>
                                <th class="text-center" style="width: 4rem;"><nobr><a href="{{ url()->current() }}?{{ Request::input('q') ? 'q='.Request::input('q').'&' : '' }}sort=id&dir={{ Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'desc' : 'asc' : 'asc' }}">Id</a> <i class="fa fa-{{ Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'sort-numeric-asc' : 'sort-numeric-desc' : 'sort' }}"></i></nobr></th>
                                <th><nobr>Order Created</nobr></th>
                                <th><nobr>User Name</nobr></th>
                                <th><nobr>Contact Number</nobr></th>
                                <th><nobr>Address</nobr></th>
								<th><nobr>Price</nobr></th>
                                <th class="w-td-action">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             @foreach ($bladeVar['results'] as $rs) 
                             <tr{!! $rs->deleted_at != null ? ' class="table-danger"' : '' !!}>
                                    <td class="text-center"><span{!! $rs->deleted_at != null ? ' class="badge badge-danger"' : '' !!}>{{ $rs->_id }}</span></td>
                                    <td>{{ date('d F Y', strtotime($rs->created_at)) }}</td>
                                    <td>{{ $rs->firstname }} {{ $rs->lastname }}</td>
									<td>{{ $rs->phone }}</td>
                                    <td>{{ $rs->address }}</td>
                                    <td>$ {{ $rs->amount }}</td> 
                                    <td class="text-center text-sm-left">
                                        <div class="hidden-xs-down">
                                            <a href="{{ route('inventory.show', $rs->_id) }}" class="btn btn-sm btn-info" title="View" ><i class="fa fa-eye"></i></a> 
                                        </div>
                                        <div class="btn-group hidden-sm-up">
                                            <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Actions
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a href="{{ route('inventory.show', $rs->_id) }}" class="dropdown-item"><i class="fa fa-eye text-info"></i> View</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-4">
                        {{ $bladeVar['results']->appends(Request::query())->links() }}
                        <small class="d-block text-center text-muted">(showing 20 results per page)</small>
                    </div>
                @else
                    <div class="alert alert-success">
                        <strong>No records found!</strong>
                        @if (!empty(Request::input('q')))
                            No matching your search criteria. 
                        @else 
                            No order added yet 
                        @endif
                    </div>
                @endif
                <!-- Deletetion code with Form -->
                <form class="formDelete">
                    {{ method_field('DELETE') }}
                </form>
            </div>
        </div>
    </div>
@endsection

@section('foot')
    <script type="text/javascript">
        var pageUrl = '{{ route('inventory.index') }}';
        var ajaxLoadUrl = '{{ url()->full() }}';
        var pageContainer = '#pageContainer';
        var pageContent = '#pageContent';
    </script>
     <script src="{{ asset('js/jquery.form.js') }}"></script>
	<script src="{{ asset('js/inventory.js?v=1') }}"></script>
@endsection