<div class="row">
	<div class="col-12">
		<form action="{{ route('profile.update', $bladeVar['result']->id) }}" method="POST">
			{{ method_field('PUT') }}
			 <div class="row">
				<div class="col-6">
					<div class="form-group">
						<label class="form-control-label" for="name"><strong>Name</strong> <small class="text-danger">(required)</small></label>
						<input type="text" class="form-control" name="name" value="{{ old('name', $bladeVar['result']->name) }}" autofocus>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label class="form-control-label" for="phone"><strong>Phone No.</strong> <small class="text-danger">(required)</small></label>
						<input type="text" class="form-control" name="phone" value="{{ old('phone', $bladeVar['result']->phone) }}" autofocus>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label class="form-control-label" for="email"><strong>Email</strong></label>
						<input type="text" class="form-control" name="email" value="{{ old('email', $bladeVar['result']->email) }}" readonly>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label class="form-control-label" for="title"><strong>Address</strong><small class="text-danger"></small></label>
						<input type="text" class="form-control" name="address" value="{{ old('address', $bladeVar['result']->address) }}" autofocus>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label for="type" class="form-control-label"><strong>Shops</strong></label>
						<select class="form-control" name="shop_name[]" id="shop_name" multiple="multiple">
							<?php $tenantShops = json_decode($bladeVar['result']->shop_name, true);
							   $shopsArray = array();  
							   foreach($tenantShops as $shops){
								  $shopsArray[] = $shops['_id']; 
							   }
							?>
							<?php if(empty($shopsArray)){ ?>
								<option value=""{{ empty(old('shop_name')) ? ' selected="selected"' : '' }}></option>
							<?php } ?>	
							@foreach ($bladeVar['shops'] as $key => $shop)
								@if(in_array($shop->_id, $shopsArray))
								   <option value="{{ $shop->_id }}"{{ $shop->_id == old('shop_name',$shop->_id) ? ' selected="selected"' : '' }}>{{ $shop->name }}</option>								
								@else
								   <option value="{{ $shop->_id }}"{{ $shop->_id == old('shop_name',$bladeVar['result']->shop_name) ? ' selected="selected"' : '' }}>{{ $shop->name }}</option>
								@endif
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label class="form-control-label" for="password"><strong>Password</strong><small class="text-danger"></small></label>
						<input type="password" class="form-control" name="password" value="{{ old('password') }}" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;" autofocus>
						<small class="form-control-feedback"></small>
					</div>
				</div>
            </div>
			<div class="form-group">
				<label class="form-control-label" for="details"><strong>Details</strong></label>
				<textarea class="form-control" id="htmlTextEditor" name="details">{{ old('details',$bladeVar['result']->details) }}</textarea>
				<small class="form-control-feedback"></small>
			</div>
			 <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
			
      	</form>
    </div>
</div>
<script type="text/javascript">
     $(document).ready(function() {
	$('#htmlTextEditor').summernote({
              height:300,
              dialogsInBody: true
            });
  });
</script>