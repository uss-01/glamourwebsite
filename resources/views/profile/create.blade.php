<div class="row">
	<div class="col-12">
		<form action="{{ route('profile.store') }}" enctype="multipart/form-data" method="POST" id="complaint_form">
		    <div class="row">
				<div class="col-12"> 
					<div class="form-group">
						<input type="hidden" name="id" value="{{ $bladeVar['result'] }}" >
						<label class="form-control-label" for="complaint"><strong>Complaint</strong><small class="text-danger">(required)</small></label>
						<textarea class="form-control" name="complaint">{{ old('complaint') }}</textarea>
						<small class="form-control-feedback"></small>
					</div>
				</div>				
			</div>
			<div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
		</form>
	</div>
</div>