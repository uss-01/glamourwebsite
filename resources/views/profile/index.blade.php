@section('head')
<link type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"/>
<link type="text/css" href="{{ asset('thirdparty/timepicker/jquery-ui-timepicker-addon.min.css') }}" rel="stylesheet"/>
<link type="text/css" href="{{ asset('thirdparty/editor/summernote.css') }}" rel="stylesheet"/>
@endsection
@extends('layouts.app')
@section('content')
<div class="container-fluid" id="pageContainer">
<div class="row" id="pageContent">
    <div class="col-12 col-md-8 offset-md-2">
		<div class="float-right mt-2 hidden-xs-down">
			<a href="{{ route('profile.edit', $bladeVar['result']->id) }}" class="btn btn-sm btn-info" title="Edit" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i></a> 
		</div>
		<div class="float-right mt-1 hidden-sm-up">
			<a href="{{ route('profile.edit', $bladeVar['result']->id) }}" class="dropdown-item" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil text-info"></i> Edit</a>
		</div>
		<h1 class="hidden-xs-down">Tenant Detail </h1>
		<h1 class="hidden-sm-up h3">Tenant Detail </h1>
		<div class="clearfix alertArea"></div>
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td>{{ $bladeVar['result']->id }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Name</th>
		    		<td>{{ $bladeVar['result']->name }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Email</th>
		    		<td>{{ $bladeVar['result']->email }}</td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Phone No.</th>
		    		<td>{{ $bladeVar['result']->phone }}</td>
				</tr>
				<tr>
		    		<th class="bg-faded">Address</th>
		    		<td>{{ $bladeVar['result']->address }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Details</th>
		    		<td>{{ strip_tags($bladeVar['result']->details) }}</td>
				</tr>
				<tr>
		    		<th class="bg-faded">Start Date</th>
		    		<td>{{ date('d F Y', strtotime($bladeVar['result']->start_date)) }}</td>
				</tr>
				<tr>
		    		<th class="bg-faded">End Date</th>
		    		<td>{{ date('d F Y', strtotime($bladeVar['result']->end_date)) }}</td>
		    	</tr>
		    	<tr>
					<th class="bg-faded">Shops</th>
					<td>
					<?php $shopsName = json_decode($bladeVar['result']->shop_name, true); ?>
					@foreach ($shopsName as $shopname)
					   {{ $shopname['name'] }}<br />
					@endforeach
					</td>
		    	</tr>
		    </tbody>
    	</table>
	</div>
</div>


    <div class="row" id="pageContent">
        <div class="col-12 col-md-8 offset-md-2">
            <h1 class="hidden-xs-down">Leasing Details</h1>
            <h1 class="hidden-sm-up h3">Leasing Details</h1>
            
        @if (!empty($bladeVar['leasingData']))
            <table class="table table-bordered table-hover table-sm">
                <thead class="bg-faded">
                    <tr>
                        <th class="text-center" style="width: 4rem;"><nobr><a href="{{ url()->current() }}?{{ Request::input('q') ? 'q='.Request::input('q').'&' : '' }}sort=id&dir={{ Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'desc' : 'asc' : 'asc' }}">Id</a> <i class="fa fa-{{ Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'sort-numeric-asc' : 'sort-numeric-desc' : 'sort' }}"></i></nobr></th>
                        <th><nobr>Shop Name</nobr></th>
                        <th><nobr>Rent Amount</nobr></th>
                        <th><nobr>Payment Status</nobr></th>
						<th><nobr>Payment Method</nobr></th>
						<th><nobr>Pending Amount</nobr></th>
						<th><nobr>Payment Date</nobr></th>
                    </tr>
                </thead>
                <tbody>
				    @foreach ($bladeVar['leasingData'] as $key => $leasing)
                    <tr{!! $leasing->deleted_at != null ? ' class="table-danger"' : '' !!}>
						<td class="text-center"><span{!! $leasing->deleted_at != null ? ' class="badge badge-danger"' : '' !!}>{{ $leasing->id }}</span></td>
						<?php $shopName = json_decode($leasing->shops, true); ?>
						<td>@foreach ($shopName as $shop) {{ $shop['name'] }} @endforeach</td>
						<td>QR {{ $leasing->rent_amount }}</td>
						<td>{{ $leasing->payment_status }}</td>
						<td>{{ $leasing->payment_method }}</td>
						<td>QR {{ $leasing->pending_amount }}</td>
						<td>{{ date('d F Y', strtotime($leasing->updated_at)) }}</td>
                    </tr>
                    @endforeach
                </tbody>
			</table>
			<div class="mt-4">
				{{ $bladeVar['leasingData']->appends(Request::query())->links() }}
				<small class="d-block text-center text-muted">(showing 20 results per page)</small>
			</div>
		@else
			<div class="alert alert-success">
				<strong>No records found!</strong>
				@if (!empty(Request::input('q')))
				No matching your search criteria. 
				@else 
				No Offers added yet, please click on "Create New" button to add a leasing. 
				@endif
			</div>
		@endif
        <!-- Deletetion code with Form -->
        <form class="formDelete">
            {{ method_field('DELETE') }}
        </form>

        <!-- Edit modal code: START -->
        <div class="modal" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editModalTitle">Edit Details</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                    </div>
                    <div class="modal-footer hidden-xs-up">
                 </div>
             </div>
         </div>
     </div>
     <!-- Edit modal code: END -->
</div>
</div>
</div>
@endsection

@section('foot')
<script type="text/javascript">
    var pageUrl = '{{ route('profile.index') }}';
    var ajaxLoadUrl = '{{ url()->full() }}';
    var pageContainer = '#pageContainer';
    var pageContent = '#pageContent';
</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src="{{ asset('thirdparty/timepicker/jquery-ui-timepicker-addon.min.js') }}"></script>
<script src="{{ asset('thirdparty/editor/summernote.min.js') }}"></script>
<script src="{{ asset('js/jquery.form.js') }}"></script>
<script src="{{ asset('js/profile.js?v=1') }}"></script>
@endsection