@section('head')
<link type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"/>
<link type="text/css" href="{{ asset('thirdparty/timepicker/jquery-ui-timepicker-addon.min.css') }}" rel="stylesheet"/>
<link type="text/css" href="{{ asset('thirdparty/editor/summernote.css') }}" rel="stylesheet"/>
@endsection
@extends('layouts.app')
@section('content')
<div class="container-fluid" id="pageContainer">
    <div class="row" id="pageContent">
        <div class="col-12 col-md-8 offset-md-2">
            <div class="float-right mt-2 hidden-xs-down">
                <a href="{{ route('profile.create') }}" class="btn btn-sm btn-info" title="Add" data-toggle="modal" data-target="#createModal"><i class="fa fa-plus"></i></a> 	
            </div>
            <div class="float-right mt-1 hidden-sm-up">
                <a href="{{ route('profile.create') }}" class="dropdown-item" data-toggle="modal" data-target="#createModal"><i class="fa fa-plus text-info"></i> Add</a>
            </div>
            <h1 class="hidden-xs-down">Tenant Complaints</h1>
            <h1 class="hidden-sm-up h3">Tenant Complaints</h1>
            <div class="clearfix alertArea"></div>
        @if (!empty($bladeVar['complaintsData']))
            <table class="table table-bordered table-hover table-sm">
                <thead class="bg-faded">
                    <tr>
                        <th class="text-center" style="width: 4rem;"><nobr><a href="{{ url()->current() }}?{{ Request::input('q') ? 'q='.Request::input('q').'&' : '' }}sort=id&dir={{ Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'desc' : 'asc' : 'asc' }}">Id</a> <i class="fa fa-{{ Request::input('sort') == 'id' ? Request::input('dir') == 'asc' ? 'sort-numeric-asc' : 'sort-numeric-desc' : 'sort' }}"></i></nobr></th>
                        <th><nobr>Complaint</nobr></th>
                        <th><nobr>Admin Reply</nobr></th>
                        <th><nobr>Complaint Date</nobr></th>
                        <th><nobr>Reply Date</nobr></th>
                    </tr>
                </thead>
                <tbody>
				    @foreach ($bladeVar['complaintsData'] as $key => $complaint)
                    <tr{!! $complaint->deleted_at != null ? ' class="table-danger"' : '' !!}>
						<td class="text-center"><span{!! $complaint->deleted_at != null ? ' class="badge badge-danger"' : '' !!}>{{ $complaint->id }}</span></td>
                        <td>{{ $complaint->complaint }}</td>
                        <td>{{ $complaint->reply }}</td>
                        <td>{{ date('d F Y', strtotime($complaint->created_at)) }}</td>
                        @if($complaint->updated_at > $complaint->created_at)
                        <td>{{ date('d F Y', strtotime($complaint->updated_at)) }}</td>
                        @else
                        <td></td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
			</table>
			<div class="mt-4">
				{{ $bladeVar['complaintsData']->appends(Request::query())->links() }}
				<small class="d-block text-center text-muted">(showing 20 results per page)</small>
			</div>
		@else
			<div class="alert alert-success">
				<strong>No records found!</strong>
				@if (!empty(Request::input('q')))
				No matching your search criteria. 
				@else 
				No Offers added yet, please click on "Create New" button to add a leasing. 
				@endif
			</div>
		@endif
        <!-- Deletetion code with Form -->
        <form class="formDelete">
            {{ method_field('DELETE') }}
        </form>
		<!-- Add modal code: START -->
		<div class="modal" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalTitle" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="createModalTitle">Add Complaint</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="my-5 text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
					</div>
					<div class="modal-footer hidden-xs-up">
					</div>
				</div>
			</div>
		</div>
		<!-- Add modal code: END -->


</div>
</div>
</div>
@endsection

@section('foot')
<script type="text/javascript">
    var pageUrl = '{{ route('profile.index') }}';
    var ajaxLoadUrl = '{{ url()->full() }}';
    var pageContainer = '#pageContainer';
    var pageContent = '#pageContent';
</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src="{{ asset('thirdparty/timepicker/jquery-ui-timepicker-addon.min.js') }}"></script>
<script src="{{ asset('thirdparty/editor/summernote.min.js') }}"></script>
<script src="{{ asset('js/jquery.form.js') }}"></script>
<script src="{{ asset('js/profile.js?v=1') }}"></script>
@endsection