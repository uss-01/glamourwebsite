@extends('layouts.app')
@section('header')@endsection
@section('content')
{{--
<div class="container">
  <div class="row">
    <div class="col col-sm-4 col-md-6 col-lg-4 offset-sm-4 offset-md-3 offset-lg-4 mt-4 mt-sm-5">
      <img src="{{ asset('images/logo.png') }}" alt="Tawal Mall" width="97px" class="mx-auto d-block img-fluid mb-4">
      <div class="card p-4 mt-4">
       <form class="" action="{{ route('register') }}" method="post">
          {{ csrf_field() }}
          <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
            <label for="name" class="form-control-label"><strong>Name</strong></label>
            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" autofocus>
                @if ($errors->has('name'))
                <small class="form-control-feedback">{{ $errors->first('name') }}</small>
                @endif
          </div>
          <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
            <label class="form-control-label" for="email"><strong>Email address</strong></label>
            <input type="email" class="form-control" name="email" value="{{ old('email') }}" tabindex="1" autofocus>
            @if ($errors->has('email'))
              <small class="form-control-feedback">{{ $errors->first('email') }}</small>
            @endif
          </div>
          <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
            <label class="form-control-label" for="password"><strong>Password</strong></label>
            <input type="password" class="form-control" name="password" tabindex="2">
            @if ($errors->has('password'))
              <small class="form-control-feedback">{{ $errors->first('password') }}</small>
            @endif
          </div>
          <div class="form-group{{ $errors->has('password_confirmation') ? ' has-danger' : '' }}">
            <label for="password-confirm" class="form-control-label"><strong>Confirm Password</strong></label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
            @if ($errors->has('password_confirmation'))
              <small class="form-control-feedback">{{ $errors->first('password_confirmation') }}</small>
            @endif
          </div>
          <div class="form-group">
            <label for="type" class="form-control-label">User Shop</label>
            <select class="form-control" name="shop_name[]" id="shop_name" multiple="multiple">
              <option value="0">Please Select</option>
              @foreach ($data as $shop)
               <option value="{{ $shop->_id }}">{{ $shop->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group" style="display: none;">
            <label for="type" class="form-control-label">User Type</label>
            <select class="form-control" name="type" id="type"><option value="seller">Seller</option></select>
          </div>
          <div class="form-group mb-0">
            <button type="submit" class="btn btn-primary" tabindex="4">Sign up</button>
          </div>
        </form>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
--}}
@endsection
@section('footer')
@component('auth.footer')@endcomponent
@endsection
