@extends('layouts.app')
@section('header')@endsection
@section('content')
<div class="container">
  <div class="row">
    <div class="col col-sm-4 offset-sm-4 pt-2 mt-5"> <img src="{{ asset('images/logo.png') }}" alt="Tawar Mall" width="97px" class="mx-auto d-block img-fluid mb-4">
      <h1 class="text-center mb-4"><strong>Reset Password</strong></h1>
      <div class="card p-4">
        <form class="" action="{{ route('password.reset') }}" method="post">
          {{ csrf_field() }}
          <input type="hidden" name="token" value="{{ $token }}">
          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label class="form-control-label" for="email"><strong>Email address</strong></label>
            <input type="text" class="form-control" id="email" name="email" placeholder="Email"/>
            @if ($errors->has('email')) <small class="form-control-feedback">{{ $errors->first('email') }}</small> @endif </div>
          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="form-control-label" for="email"><strong>Password</strong></label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password"/>
            @if ($errors->has('password')) <small class="form-control-feedback">{{ $errors->first('password') }}</small> @endif </div>
          <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <label class="form-control-label" for="email"><strong>Confirm Password</strong></label>
            <input type="password_confirmation" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password"/>
            @if ($errors->has('password_confirmation')) <small class="form-control-feedback">{{ $errors->first('password_confirmation') }}</small> @endif </div>
          <div>
            <button type="submit" class="btn btn-default submit">Reset Password</button>
          </div>
        </form>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
@endsection
@section('footer')
@component('auth.footer')@endcomponent
@endsection