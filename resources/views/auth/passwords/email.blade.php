@extends('layouts.app')
@section('header')@endsection
@section('content')
<div class="container">
  <div class="row">
    <div class="col col-sm-4 col-md-6 col-lg-4 offset-sm-4 offset-md-3 offset-lg-4 mt-4 mt-sm-5"> <img src="{{ asset('images/logo.png') }}" alt="Tawar Mall" width="97px" class="mx-auto d-block img-fluid mb-4">
      <h1 class="h3"><strong>Forgot your password?</strong></h1>
      <div class="px-1"> <small>Enter your email address to reset your password.</small> </div>
      <div class="card p-4 my-4"> @if (session('status'))
        <div class="alert alert-success" role="alert"> {{ session('status') }} </div>
        @endif
        <form action="{{ route('password.email') }}" method="post">
          {{ csrf_field() }}
          <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
            <label class="form-control-label" for="email"><strong>Email address</strong></label>
            <input type="email" class="form-control" name="email" value="{{ old('email') }}" tabindex="1" required autofocus>
            @if ($errors->has('email')) <small class="form-control-feedback">{{ $errors->first('email') }}</small> @endif </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
            <small>or go back to <a href="{{ route('login') }}">login</a></small> </div>
        </form>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
@endsection
@section('footer')
@component('auth.footer')@endcomponent
@endsection