@extends('layouts.app')
@section('header')@endsection
@section('content')
<div class="container">
  <div class="row">
    <div class="col col-sm-4 col-md-6 col-lg-4 offset-sm-4 offset-md-3 offset-lg-4 mt-4 mt-sm-5">
      <img src="{{ asset('images/logo.png') }}" alt="Tawal Mall" width="97px" class="mx-auto d-block img-fluid mb-4">
      <div class="card p-4 mt-4">
        <form class="" action="{{ route('login') }}" method="post">
          {{ csrf_field() }}
          <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
            <label class="form-control-label" for="email"><strong>Email address</strong></label>
            <input type="email" class="form-control" name="email" value="{{ old('email') }}" tabindex="1" autofocus>
            @if ($errors->has('email'))
              <small class="form-control-feedback">{{ $errors->first('email') }}</small>
            @endif
          </div>

          <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
            <a href="{{ route('password.request') }}" title="forgot password?" class="float-right" tabindex="5"><small>forgot your password?</small></a>
            <label class="form-control-label" for="password"><strong>Password</strong></label>
            <input type="password" class="form-control" name="password" tabindex="2">
            @if ($errors->has('password'))
              <small class="form-control-feedback">{{ $errors->first('password') }}</small>
            @endif
          </div>

          <div class="form-group mb-0">
            <button type="submit" class="btn btn-primary" tabindex="4">Sign in</button>
          </div>
        </form>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
@endsection
@section('footer')
@component('auth.footer')@endcomponent
@endsection
