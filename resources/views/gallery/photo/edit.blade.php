<div class="row">
	<div class="col-12">
		<form action="{{ route('photo.update', $bladeVar['result']->_id) }}" method="POST">
			{{ method_field('PUT') }}
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Title</strong></label>
				<input type="text" class="form-control" name="title" value="{{ old('title', $bladeVar['result']->title) }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>

            <div class="form-group">
				<label class="form-control-label" for="image"><strong>Image</strong> <small class="text-danger">(required)</small></label>
				<input type="file" name="image" class="form-control" >
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="album"><strong>Category</strong></label>
				<select name="album" class="form-control">
					<option value=""{{ empty(old('album')) ? ' selected="selected"' : '' }}></option>
				@foreach ($bladeVar['albums'] as $key => $album)
					<option value="{{ $album->_id }}"{{ $album->_id == old('album', $bladeVar['result']->album_id) ? ' selected="selected"' : '' }}>{{ $album->name }}</option>
				@endforeach
				</select>
				<small class="form-control-feedback"></small>
			</div>
			 <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
			
      	</form>
    </div>
</div>