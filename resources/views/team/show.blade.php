<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td>{{ $bladeVar['result']->id }}</td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Name</th>
		    		<td>{{ $bladeVar['result']->name }}</td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Designation</th>
		    		<td>{{ $bladeVar['result']->designation }}</td>
				</tr>
				<tr>
		    		<th class="bg-faded">Facebook Url</th>
		    		<td>{{ $bladeVar['result']->facebook_url }}</td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Twitter Url</th>
		    		<td>{{ $bladeVar['result']->twitter_url }}</td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Linkedin Url</th>
		    		<td>{{ $bladeVar['result']->linkedin_url }}</td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Google Url</th>
		    		<td>{{ $bladeVar['result']->google_url }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Image</th>
		    		<td><img src="{{ asset('images/cms').'/'.$bladeVar['result']->image }}" class="thumbnail" width="150" /></td>
		    	</tr>
		    </tbody>
    	</table>
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="{{ $bladeVar['result']->deleted_at == null ? route('team.edit', $bladeVar['result']->id) : '' }}" 
	    	data-destroy="{{ $bladeVar['result']->deleted_at == null ? route('team.destroy', $bladeVar['result']->id) : '' }}" 
	    	data-restore="{{ $bladeVar['result']->deleted_at != null ? route('team.restore', $bladeVar['result']->id) : '' }}"
	    	data-delete="{{ route('team.delete', $bladeVar['result']->id) }}">
	    </span>
	</div>
</div>	    