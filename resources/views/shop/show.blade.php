<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Shop Id</th>
		    		<td>{{ $bladeVar['result']->_id }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Name</th>
		    		<td>{{ $bladeVar['result']->name }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Category</th>
		    		<td> {{ $bladeVar['result']->category->name }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Store</th>
		    		<td>{{ $bladeVar['result']->store->name }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Email</th>
		    		<td>{{ $bladeVar['result']->email }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Phone</th>
		    		<td>{{ $bladeVar['result']->phone }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Website</th>
		    		<td>{{ $bladeVar['result']->website }}</td>
				</tr>
				<tr>
		    		<th class="bg-faded">Latitude</th>
		    		<td>{{ $bladeVar['result']->latitude }}</td>
				</tr>
				<tr>
		    		<th class="bg-faded">Longitude</th>
		    		<td>{{ $bladeVar['result']->longitude }}</td>
				</tr>
				<tr>
		    		<th class="bg-faded">Floor Level</th>
		    		<td>{{ $bladeVar['result']->floorlevel }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Address</th>
		    		<td>{{ $bladeVar['result']->address }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Details</th>
		    		<td>{!! $bladeVar['result']->details !!}</td>
				</tr>
				<tr>
		    		<th class="bg-faded">Lease Year</th>
		    		<td>{{ $bladeVar['result']->lease_year }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Shop Brands</th>
		    		<td>
                    <div class="row">
		    		@foreach ($bladeVar['selectedBrands'] as $brand) <div class="col-3"> {{ $brand->name }} </div> @endforeach
                    </div>
		    		</td>
		    	</tr>
		    </tbody>
    	</table>
    	
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="{{ $bladeVar['result']->deleted_at == null ? route('shop.edit', $bladeVar['result']->_id) : '' }}" 
	    	data-destroy="{{ $bladeVar['result']->deleted_at == null ? route('shop.destroy', $bladeVar['result']->_id) : '' }}" 
	    	data-restore="{{ $bladeVar['result']->deleted_at != null ? route('shop.restore', $bladeVar['result']->_id) : '' }}"
	    	data-delete="{{ route('shop.delete', $bladeVar['result']->_id) }}">
	    </span>
	</div>
</div>	    