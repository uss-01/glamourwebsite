<div class="row">
	<div class="col-12">
		<form action="{{ route('shop.savePics') }}" enctype="multipart/form-data" method="POST" id="shop_pics_form">
			<div class="form-group">
				<label class="form-control-label" for="image"><strong>Pics</strong> <small class="text-danger">(required)</small></label>
				<input type="file" name="image[]" class="form-control" multiple="">
				<small class="form-control-feedback"></small>
				<input type="hidden" name="shop" value="{{ $bladeVar['result']->_id }}">
			</div>
			 <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
			
        
		</form>
	</div>

</div>
<div class="row">
<div class="col-12">
<h5>Store SLides</h5>
 <div class="clearfix alertArea"></div>
</div>

@foreach ($bladeVar['shopPics'] as $pic) 
<div class="col-3"> 
<div class="thumbnail">
<img src="{{ asset('images/shops').'/'.$pic->image }}" width="100" />
<div class="caption text-center">
<br/>
<a href="{{ route('shop.deletePic', $pic->_id) }}" class="btn btn-sm btn-danger btn-delete" title="Delete permanently"><i class="fa fa-remove"></i></a>
</div>
</div>
</div>
@endforeach

</div>