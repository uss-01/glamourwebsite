<div class="row">
	<div class="col-12">
		<form action="{{ route('blogs.store') }}" method="POST">
			<div class="row">
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="blog_category"><strong>Category</strong><small class="text-danger"> (required)</small></label>
						<select name="blog_category" class="form-control">
							<option value=""{{ empty(old('blog_category')) ? ' selected="selected"' : '' }}></option>
							@foreach ($bladeVar['blog_categories'] as $key => $blog_category)
							<option value="{{ $blog_category->_id }}"{{ $blog_category->_id == old('blog_category') ? ' selected="selected"' : '' }}>{{ $blog_category->name }}</option>
							@endforeach
						</select>
						<div class="other_option_wrap"></div>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="blog_tag"><strong>Tags</strong></label>
						<select name="blog_tag" class="form-control">
							<option value=""{{ empty(old('blog_tag')) ? ' selected="selected"' : '' }}></option>
							@foreach ($bladeVar['blog_tags'] as $key => $blog_tag)
								<option value="{{ $blog_tag->id }}"{{ $blog_tag->id == old('blog_tag') ? ' selected="selected"' : '' }}>{{ $blog_tag->name }}</option>
							@endforeach
						</select>
						<div class="other_option_wrap2"></div>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="blog_status"><strong>Status</strong></label>
						<select name="blog_status" class="form-control">
							<option value=""{{ empty(old('blog_status')) ? ' selected="selected"' : '' }}></option>
							@foreach ($bladeVar['blogStatus'] as $key => $blog_status)
							<option value="{{ $blog_status }}"{{ $blog_status == old('blog_status') ? ' selected="selected"' : '' }}>{{ $blog_status }}</option>
							@endforeach
						</select>
						<small class="form-control-feedback"></small>
					</div>
				</div>
			</div>
            <div class="row">
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="name"><strong>Name</strong><small class="text-danger"> (required)</small></label>
						<input type="text" class="form-control" name="name" value="{{ old('name') }}" autofocus>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="audio_url"><strong>Audio Url</strong></label>
						<input type="text" class="form-control" name="audio_url" value="{{ old('audio_url') }}" autofocus>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="video_url"><strong>Video Url</strong></label>
						<input type="text" class="form-control" name="video_url" value="{{ old('video_url') }}" autofocus>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="image"><strong>Image (Size 1110X800 px)</strong> <small class="text-danger">(required)</small></label>
						<input type="file" name="image" class="form-control">
						<small class="form-control-feedback"></small>
					</div>
				</div>
            </div>
			<div class="row">
		        <div class="col-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" value="1" name="blog_audio" >
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><strong>Audio Blog</strong></span>
						</label>
					</div>
			    </div>
				<div class="col-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" name="blog_video" value="1" >
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><strong>Video Blog</strong></span>
						</label>
					</div>
			    </div>
				<div class="col-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" name="blog_image_slide" value="1" >
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><strong>Image Slider Blog</strong></span>
						</label>
					</div>
			    </div>
		    </div>
			<div class="form-group">
				<label class="form-control-label" for="details"><strong>Details</strong></label>
				<textarea class="form-control" id="htmlTextEditor" name="details">{{ old('details') }}</textarea>
				<small class="form-control-feedback"></small>
			</div>
			 <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
      	</form>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('#htmlTextEditor').summernote({
		height:300,
		dialogsInBody: true
    });
  });
</script>