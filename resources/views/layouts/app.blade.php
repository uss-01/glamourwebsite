<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ (isset($bladeVar['page']['title']) ? $bladeVar['page']['title'] : '') }}</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/img/favicon.ico') }}">
    <link rel="stylesheet" href="{{ asset('css/thirdparty/fontawesome.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/thirdparty/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/thirdparty/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/boot.css') }}" rel="stylesheet">
    <link href="{{ asset('css/resp.css') }}" rel="stylesheet">
    <script>
        window.webApp = {!! json_encode([
        	'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    @section('head')@show
</head>
<body{!! (isset($bladeVar['page']['bodyClass']) ? ' class="'.$bladeVar['page']['bodyClass'].'"' : '') !!}>
    @section('header')
        @unless (!Auth::check())
            <nav class="navbar navbar-toggleable-md sticky-top navbar-light bg-faded mb-3">
                <button class="navbar-toggler navbar-toggler-right rounded-0" type="button" data-toggle="collapse" data-target="#mainNav" aria-controls="mainNav" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars"></i></button>
                <a class="navbar-brand" href="{{ url('/') }}"><img width="97px" src="{{ asset('assets/img/logo.png') }}" alt="Glamour Jewellery" class="d-inline-block align-top 1hidden-xs-up"></a>
                <div class="collapse navbar-collapse" id="mainNav">
                   @if(Auth::user()->type == 'seller')

                   @else
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle{{ $bladeVar['page']['activeNav']['nav'] == 'master' ? ' active' : ''}}" href="javascript:void(0);" id="mastersDropNav" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Masters</a>
							<div class="dropdown-menu" aria-labelledby="mastersDropNav">
                                <a class="dropdown-item{{ $bladeVar['page']['activeNav']['sub'] == 'home_page_images' ? ' active' : ''}}" href="{{ route('homepageimage.index') }}">Home Slider</a>
                                <a class="dropdown-item{{ $bladeVar['page']['activeNav']['sub'] == 'banner_images' ? ' active' : ''}}" href="{{ route('bannerimages.index') }}">Banner Images</a>
                                <a class="dropdown-item{{ $bladeVar['page']['activeNav']['sub'] == 'banner_slider' ? ' active' : ''}}" href="{{ route('bannerslider.index') }}">Banner Slider</a>
                                <a class="dropdown-item{{ $bladeVar['page']['activeNav']['sub'] == 'single_banner_images' ? ' active' : ''}}" href="{{ route('singlebannerimage.index') }}">Single Banner Slider</a>
                                <a class="dropdown-item{{ $bladeVar['page']['activeNav']['sub'] == 'blog_category' ? ' active' : ''}}" href="{{ route('blog_category.index') }}">Blog Categories</a>
                                <a class="dropdown-item{{ $bladeVar['page']['activeNav']['sub'] == 'tags' ? ' active' : ''}}" href="{{ route('tags.index') }}">Blog Tags</a>
                                <a class="dropdown-item{{ $bladeVar['page']['activeNav']['sub'] == 'product_category' ? ' active' : ''}}" href="{{ route('product_category.index') }}">Product Categories</a>
                                <a class="dropdown-item{{ $bladeVar['page']['activeNav']['sub'] == 'product_subcategory' ? ' active' : ''}}" href="{{ route('product_subcategory.index') }}">Product Subcategories</a>
                                <a class="dropdown-item{{ $bladeVar['page']['activeNav']['sub'] == 'brand' ? ' active' : ''}}" href="{{ route('brand.index') }}">Product Brands</a>
                                <a class="dropdown-item{{ $bladeVar['page']['activeNav']['sub'] == 'color' ? ' active' : ''}}" href="{{ route('color.index') }}">Product Color</a>
                                <a class="dropdown-item{{ $bladeVar['page']['activeNav']['sub'] == 'size' ? ' active' : ''}}" href="{{ route('size.index') }}">Product Size</a>
                            </div>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('coupons.index') }}">Coupons</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('blogs.index') }}">Blogs</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('cms.index') }}">CMS</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('product.index') }}">Products</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('team.index') }}">Team</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('testimonial.index') }}">Testimonials</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('setting.index') }}">Site Settings</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('siteusers.index') }}">Users</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('orders.index') }}">Orders</a></li>
                    </ul>
                    <span class="navbar-text hidden-md-down"><small>Signed in as</small></span>
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="http://example.com" id="signinUserNav" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="signinUserNav">
                                <a class="dropdown-item" title="Sign out" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden-xs-up">{{ csrf_field() }}</form>
                            </div>
                        </li>
                    </ul>
                    @endif
                    
                </div>
            </nav>

            @isset($bladeVar['page']['breadcrumb'])
                @if (count($bladeVar['page']['breadcrumb']) > 0)
                    <div class="container-fluid">
                        <nav class="breadcrumb bg-faded py-2">
                            @foreach ($bladeVar['page']['breadcrumb'] as $breadcrumbKey => $breadcrumbVal)
                                @if (!empty($breadcrumbVal))
                                    <a class="breadcrumb-item" href="{{ url($breadcrumbVal) }}">{{ $breadcrumbKey }}</a>
                                @else
                                    <span class="breadcrumb-item active">{{ $breadcrumbKey }}</span>
                                @endif
                            @endforeach
                        </nav>
                    </div>
                @endif
            @endisset
        @endunless
    @show

    @yield('content')

    @section('footer')
        <div class="footer-border pt-3 mt-5 mb-3">
            <small class="d-block text-center">&copy; {{ date('Y') }} All rights reserved</small>
        </div>
    @show

    <div class="loadingScreen hidden-xs-up">
        <div class="my-5 text-center mx-auto h1" id="loadingScreenDiv"><i class="fa fa-spinner fa-spin"></i> Loading...</div>
    </div>

    <!-- Footer scripts -->
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/ajax-tether-1.4.min.js') }}" ></script>
    <script src="{{ asset('js/bootstrap-4.0.0.min.js') }}" ></script>
    <script src="{{ asset('js/boot.js?v=1') }}"></script>
    @section('foot')@show
</body>
</html>