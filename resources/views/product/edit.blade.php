<div class="row">
	<div class="col-12">
		<form action="{{ route('product.update', $bladeVar['result']->_id) }}" method="POST">
			{{ method_field('PUT') }}
			<div class="row">
			    <div class="col-12">
					<div class="form-group">
						<label class="form-control-label" for="name"><strong>Name</strong><small class="text-danger"> (required)</small></label>
						<input type="text" class="form-control" name="name" value="{{ old('name', $bladeVar['result']->name) }}" autofocus>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="product_brand"><strong>Brand</strong><small class="text-danger"> (required)</small></label>
						<select name="product_brand" class="form-control">
							<option value=""{{ empty(old('product_brand')) ? ' selected="selected"' : '' }}></option>
							@foreach ($bladeVar['product_brands'] as $key => $product_brand)
							<option value="{{ $product_brand->_id }}"{{ $product_brand->_id == old('product_brand',$bladeVar['result']->brand_id) ? ' selected="selected"' : '' }}>{{ $product_brand->name }}</option>
							@endforeach
						</select>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="product_category"><strong>Category</strong><small class="text-danger"> (required)</small></label>
						<select name="product_category" class="form-control">
							<option value=""{{ empty(old('product_category')) ? ' selected="selected"' : '' }}></option>
							@foreach ($bladeVar['product_categories'] as $key => $product_category)
							<option value="{{ $product_category->_id }}"{{ $product_category->_id == old('product_category',$bladeVar['result']->product_category_id) ? ' selected="selected"' : '' }}>{{ $product_category->name }}</option>
							@endforeach
						<option value="-1"{{ old('product_category')== -1 ? ' selected="selected"' : '' }}>Other</option>
						</select>
						<div class="other_option_wrap"></div>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="product_subcategory"><strong>Sub Category</strong></label>
						<select name="product_subcategory" class="form-control">
							<option value=""{{ empty(old('product_subcategory')) ? ' selected="selected"' : '' }}></option>
							@foreach ($bladeVar['product_subcategories'] as $key => $product_subcategory)
								<option value="{{ $product_subcategory->_id }}"{{ $product_subcategory->_id == old('product_subcategory',$bladeVar['result']->product_subcategory_id) ? ' selected="selected"' : '' }}>{{ $product_subcategory->name }}</option>
							@endforeach
						<option value="-1"{{ old('product_subcategory')== -1 ? ' selected="selected"' : '' }}>Other</option>
						</select>
						<div class="other_option_wrap2"></div>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="product_color"><strong>Color</strong></label>
						<select name="product_color" class="form-control">
							<option value=""{{ empty(old('color')) ? ' selected="selected"' : '' }}></option>
							@foreach ($bladeVar['product_colors'] as $key => $color)
							<option value="{{ $color->id }}"{{ $color->id == old('product_color', $bladeVar['result']->product_color_id) ? ' selected="selected"' : '' }}>{{ $color->name }}</option>
						@endforeach
						</select>
						
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="product_size"><strong>Size</strong></label>
						<select name="product_size" class="form-control">
							<option value=""{{ empty(old('size')) ? ' selected="selected"' : '' }}></option>
							@foreach ($bladeVar['product_sizes'] as $key => $size)
							<option value="{{ $size->id }}"{{ $size->id == old('product_size', $bladeVar['result']->product_size_id) ? ' selected="selected"' : '' }}>{{ $size->name }}</option>
						@endforeach
						</select>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<!-- <div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="product_status"><strong>Status</strong></label>
						<select name="product_status" class="form-control">
							<option value=""{{ empty(old('product_status')) ? ' selected="selected"' : '' }}></option>
							@foreach ($bladeVar['productStatus'] as $key => $product_status)
							<option value="{{ $product_status }}"{{ $product_status == old('product_status', $bladeVar['result']->product_status) ? ' selected="selected"' : '' }}>{{ $product_status }}</option>
							@endforeach
						</select>
						<small class="form-control-feedback"></small>
					</div>
				</div> -->
			</div>
            <div class="row">
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="price"><strong>Price</strong><small class="text-danger"> (required)</small></label>
						<input type="text" class="form-control" name="price" value="{{ old('price', $bladeVar['result']->price) }}" autofocus>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="discounted_price"><strong>Discounted Price</strong></label>
						<input type="text" class="form-control" name="discounted_price" value="{{ old('discounted_price', $bladeVar['result']->discounted_price) }}" autofocus>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="product_quantity"><strong>Quantity</strong></label>
						<input type="text" class="form-control" name="product_quantity" value="{{ old('product_quantity', $bladeVar['result']->product_quantity) }}" autofocus>
						<small class="form-control-feedback"></small>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="form-control-label" for="image"><strong>Image (Size 600X600 px)</strong> <small class="text-danger">(required)</small></label>
						<input type="file" name="image" class="form-control">
						<small class="form-control-feedback"></small>
					</div>
				</div>
            </div>
            
			<div class="row">
		        <div class="col-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" value="1" name="featured" {{ $bladeVar['result']->featured == 1 ? ' checked="checked"' : '' }} >
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><strong>Featured</strong></span>
						</label>
					</div>
			    </div>
				<div class="col-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" name="new_arrival" value="1" {{ $bladeVar['result']->new_arrival == 1 ? ' checked="checked"' : '' }} >
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><strong>New Arrival</strong></span>
						</label>
					</div>
			    </div>
				<div class="col-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" name="on_sale" value="1" {{ $bladeVar['result']->on_sale == 1 ? ' checked="checked"' : '' }} >
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><strong>On Sale</strong></span>
						</label>
					</div>
			    </div>
				<div class="col-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" name="hot_deal" value="1" {{ $bladeVar['result']->hot_deal == 1 ? ' checked="checked"' : '' }} >
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><strong>Hot Deal</strong></span>
						</label>
					</div>
			    </div>
				<div class="col-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" name="best_seller" value="1" {{ $bladeVar['result']->best_seller == 1 ? ' checked="checked"' : '' }}>
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><strong>Best Seller</strong></span>
						</label>
					</div>
			    </div>
			    <div class="col-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" value="1" name="add_to_cart" {{ $bladeVar['result']->add_to_cart == 1 ? ' checked="checked"' : '' }}>
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><strong>Add To Cart</strong></span>
						</label>
					</div>
			    </div>
		    </div>
			<div class="form-group">
				<label class="form-control-label" for="sort_details"><strong>Sort Details</strong></label>
				<textarea class="form-control" name="sort_details">{{ old('sort_details', $bladeVar['result']->sort_details) }}</textarea>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="details"><strong>Details</strong></label>
				<textarea class="form-control" name="details">{{ old('details', $bladeVar['result']->details) }}</textarea>
				<small class="form-control-feedback"></small>
			</div>

            <div class="form-group">
				<label class="form-control-label" for="product_variation"><strong>Product Variation</strong></label>
                <p>For product multiple color & size variation</p>   
				<?php $variations_data = json_decode($bladeVar['result']->variations); ?>
                <div id="allVariation">
				    @foreach($variations_data as $variation)
					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<label class="form-control-label" for="pcolor"><strong>Color</strong></label>
								<select name="pcolor[]" class="form-control">
									<option value=""{{ empty(old('product_color')) ? ' selected="selected"' : '' }}></option>
									@foreach ($bladeVar['product_colors'] as $key => $color)
									    <option value="{{ $color->id }}"{{ $color->id == old('pcolor[]',$variation->color) ? ' selected="selected"' : '' }}>{{ $color->name }}</option>
								    @endforeach
								</select>
								<small class="form-control-feedback"></small>
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<label class="form-control-label" for="psize"><strong>Size</strong></label>
								<select name="psize[]" class="form-control">
									<option value=""{{ empty(old('product_size')) ? ' selected="selected"' : '' }}></option>
									@foreach ($bladeVar['product_sizes'] as $key => $size)
									    <option value="{{ $size->id }}"{{ $size->id == old('psize[]',$variation->size) ? ' selected="selected"' : '' }}>{{ $size->name }}</option>
								    @endforeach
								</select>
								<small class="form-control-feedback"></small>
							</div>
						</div>
						<div class="col-4">
							<div class="form-group">
								<label class="form-control-label" for="pprice"><strong>Price</strong></label>
								<input type="number" min="0" step="any" class="form-control" name="pprice[]" value="{{ old('pprice[]', $variation->price) }}">
								<small class="form-control-feedback"></small>
							</div>
						</div>
						<div class="col-4">
							<div class="form-group">
								<label class="form-control-label" for="pdprice"><strong>Discounted Price</strong></label>
								<input type="number" min="0" step="any" class="form-control" name="pdprice[]" value="{{ old('pdprice[]',$variation->discountedPrice) }}">
								<small class="form-control-feedback"></small>
							</div>
						</div>
						<div class="col-4">
							<div class="form-group">
								<label class="form-control-label" for="pquantity"><strong>Quantity</strong></label>
								<input type="number" class="form-control" name="pquantity[]" value="{{ old('pquantity[]',$variation->quantity) }}">
								<small class="form-control-feedback"></small>
							</div>
						</div>
						<div class="col-4">
							<div class="form-group">
							    <?php $images_data = $variation->images; ?>
								<label class="form-control-label" for="images"><strong>Images (Size 600X600 px)</strong></label>
								<input type="file" name="images[]" class="form-control">
								<input type="hidden" name="imagesurl[]" class="form-control" value="{{ old('imagesurl[]',$images_data) }}">
								<small class="form-control-feedback"></small>
							</div>
						</div>
					</div>
					@endforeach
				</div>
				<div class="form-group text-right">
					<button class="btn btn-success" type="button" id="addVariation">Add Variation</button>
					<button class="btn btn-secondary removeVariation" type="button">Remove Variation</button>
				</div>
			</div>

			<div class="form-group">
				<label class="form-control-label" for="product_attributes"><strong>Attributes</strong></label>
				  <p>For multiple values use semicolon as seprator. e.g value1:value2;value3</p>
                  <?php $attribute_data = json_decode($bladeVar['result']->attribute); ?>
				<div id="allAttributes">
				@foreach($attribute_data as $attribute)
				<div class="row">
		         <div class="col-3"><div class="form-group">
					    <input type="text" class="form-control" name="attrName[]" value="{{ old('attrName[]',$attribute->name) }}" autofocus placeholder="Attribute Name">
			    </div></div>
			    <div class="col-3"><div class="form-group">
					    <input type="text" class="form-control" name="attrVal[]" value="{{ old('attrVal[]',$attribute->value) }}" autofocus placeholder="Attribute Value">
			    </div></div>
			  </div>
			  @endforeach
			  </div>  
		    <div class="form-group text-right">
						<button class="btn btn-success" type="button" id="addAttribute">Add Attribute</button>
						<button class="btn btn-secondary removeAttribute" type="button">Remove Attribute</button>
				</div>
			</div>
			 <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
      	</form>
    </div>
</div>