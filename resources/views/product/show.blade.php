<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td>{{ $bladeVar['result']->_id }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Name</th>
		    		<td>{{ $bladeVar['result']->name }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Price</th>
		    		<td>$ {{ $bladeVar['result']->price }}</td>
				</tr>
				<tr>
		    		<th class="bg-faded">Discounted Price</th>
		    		<td>$ {{ $bladeVar['result']->discounted_price }}</td>
				</tr>
				<tr>
		    		<th class="bg-faded">Quantity</th>
		    		<td>{{ $bladeVar['result']->product_quantity }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Brand</th>
		    		<td> {{ $bladeVar['result']->product_brand->name }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Category</th>
		    		<td>{{ $bladeVar['result']->product_category->name }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Sub Category</th>
		    		<td>{{ $bladeVar['result']->product_subcategory->name }}</td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Color</th>
		    		<td> {{ $bladeVar['result']->product_color->name }}</td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Size</th>
		    		<td> {{ $bladeVar['result']->product_size->name }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Image</th>
		    		<td><img src="{{ asset('images/products').'/'.$bladeVar['result']->image }}" class="thumbnail" width="150" /></td>
		    	</tr>
		    	<!-- <tr>
		    		<th class="bg-faded">Status</th>
		    		<td>{{ $bladeVar['result']->product_status }}</td>
		    	</tr> -->
				<tr>
		    		<th class="bg-faded">Sort Details</th>
		    		<td>{{ $bladeVar['result']->sort_details }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Details</th>
		    		<td>{{ $bladeVar['result']->details }}</td>
		    	</tr>
				<tr>
		    		<th class="bg-faded">Variations</th>
		    		<td>
                    <?php $variation_data = json_decode($bladeVar['result']->variations); ?>
                    @foreach($variation_data as $variation)
						@if($variation->color)
							@foreach ($bladeVar['product_colors'] as $key => $color)
								@if($color->id == $variation->color)
									Color :  {{ $color->name }}<br>
								@endif	
							@endforeach 
						@endif
						@if($variation->size)
							@foreach ($bladeVar['product_sizes'] as $key => $size)
								@if($size->id == $variation->size)
									Size :  {{ $size->name }}<br>
								@endif	
							@endforeach 
						@endif
						@if($variation->price)
							Price :  {{ $variation->price }}<br>
						@endif
						@if($variation->discountedPrice)
							Discounted Price :  {{ $variation->discountedPrice }}<br>
						@endif
						@if($variation->quantity)
							Quantity :  {{ $variation->quantity }}<br>
						@endif
						@if($variation->images)
							Images  :   <div class="thumbnail">
											<img src="{{ asset('images/products').'/'.$variation->images }}" width="100" />
										</div><br>	
						@endif
                    @endforeach
		    		</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Attributes</th>
		    		<td>
                    <?php $attribute_data = json_decode($bladeVar['result']->attribute); ?>
                    @foreach($attribute_data as $attribute)
                    @if($attribute->value !=null)
                    {{ $attribute->name }} :  {{ $attribute->value }}<br>
                    @endif
                    @endforeach
		    		</td>
		    	</tr>
		    	
		    </tbody>
    	</table>
    	
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="{{ $bladeVar['result']->deleted_at == null ? route('product.edit', $bladeVar['result']->_id) : '' }}" 
	    	data-destroy="{{ $bladeVar['result']->deleted_at == null ? route('product.destroy', $bladeVar['result']->_id) : '' }}" 
	    	data-restore="{{ $bladeVar['result']->deleted_at != null ? route('product.restore', $bladeVar['result']->_id) : '' }}"
	    	data-delete="{{ route('product.delete', $bladeVar['result']->_id) }}">
	    </span>
	</div>
</div>	    