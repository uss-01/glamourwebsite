<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Shop Id</th>
		    		<td>{{ $bladeVar['result']->_id }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Name</th>
		    		<td>{{ $bladeVar['result']->name }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Price</th>
		    		<td>$ {{ $bladeVar['result']->price }}</td>
				</tr>
				<tr>
		    		<th class="bg-faded">Discounted Price</th>
		    		<td>$ {{ $bladeVar['result']->discounted_price }}</td>
				</tr>
				<tr>
		    		<th class="bg-faded">Product Quantity</th>
		    		<td>{{ $bladeVar['result']->product_quantity }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Shop</th>
		    		<td> {{ $bladeVar['result']->shop->name }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Brand</th>
		    		<td> {{ $bladeVar['result']->brand->name }}</td>
		    	</tr>
		    	
		    	<tr>
		    		<th class="bg-faded">Product Category</th>
		    		<td>{{ $bladeVar['result']->product_category->name }}</td>
		    	</tr>
		    	
		    	<tr>
		    		<th class="bg-faded">Image</th>
		    		<td><img src="{{ asset('images/products').'/'.$bladeVar['result']->image }}" class="thumbnail" width="150" /></td>
		    	</tr>
		    	
		    	<tr>
		    		<th class="bg-faded">Details</th>
		    		<td>{{ $bladeVar['result']->details }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Attributes</th>
		    		<td>
                    <?php $attribute_data = json_decode($bladeVar['result']->attribute); ?>
                    @foreach($attribute_data as $attribute)
                    @if($attribute->value !=null)
                    {{ $attribute->name }} :  {{ $attribute->value }}<br>
                    @endif
                    @endforeach
		    		</td>
		    	</tr>
		    	
		    </tbody>
    	</table>
    	
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="{{ $bladeVar['result']->deleted_at == null ? route('products.edit', $bladeVar['result']->_id) : '' }}" 
	    	data-destroy="{{ $bladeVar['result']->deleted_at == null ? route('products.destroy', $bladeVar['result']->_id) : '' }}" 
	    	data-restore="{{ $bladeVar['result']->deleted_at != null ? route('products.restore', $bladeVar['result']->_id) : '' }}"
	    	data-delete="{{ route('products.delete', $bladeVar['result']->_id) }}">
	    </span>
	</div>
</div>	    