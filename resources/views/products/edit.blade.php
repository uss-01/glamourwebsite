<div class="row">
	<div class="col-12">
		<form action="{{ route('products.update', $bladeVar['result']->_id) }}" method="POST">
			{{ method_field('PUT') }}
			<div class="row">
			<div class="col-4">
			<div class="form-group">
				<label class="form-control-label" for="category"><strong>Shop</strong></label>
				<select name="shop" class="form-control">
					<option value=""{{ empty(old('shop')) ? ' selected="selected"' : '' }}></option>
				@foreach ($bladeVar['shopResults'] as $shop)
				    <option value="{{ $shop['shopId'] }}"{{ $shop['shopId'] == old('shop', $bladeVar['result']->shop_id) ? ' selected="selected"' : '' }}>{{ $shop['shopName'] }}</option>
				@endforeach
				</select>
				
				<small class="form-control-feedback"></small>
			</div>
			</div>
		   <div class="col-4">
		    <div class="form-group">
				<label class="form-control-label" for="code"><strong>Brand</strong></label>
				<select name="brand" class="form-control">
					<option value=""{{ empty(old('brand')) ? ' selected="selected"' : '' }}></option>
					@foreach ($bladeVar['brands'] as $key => $brand)
					<option value="{{ $brand->brand_id }}"{{ $brand->brand_id == old('brand',$bladeVar['result']->brand_id) ? ' selected="selected"' : '' }}>{{ $brand->name }}</option>
				@endforeach
				</select>
				<small class="form-control-feedback"></small>
			</div>
			
			</div>
            <div class="col-4">
		     <div class="form-group">
				<label class="form-control-label" for="code"><strong>Product Category</strong></label>
				<select name="product_category" class="form-control">
					<option value=""{{ empty(old('product_category')) ? ' selected="selected"' : '' }}></option>
					@foreach ($bladeVar['product_categories'] as $key => $product_category)
					<option value="{{ $product_category->_id }}"{{ $product_category->_id == old('product_category',$bladeVar['result']->product_category_id) ? ' selected="selected"' : '' }}>{{ $product_category->name }}</option>
				@endforeach
				<option value="-1"{{ old('product_category')== -1 ? ' selected="selected"' : '' }}>Other</option>
				</select>
				<div class="other_option_wrap"></div>
				<small class="form-control-feedback"></small>
			 </div>
			
			</div>
			</div>
			 
            <div class="row">
            <div class="col-4">
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Product name</strong><small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="name" value="{{ old('name', $bladeVar['result']->name) }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>
           </div>
           <div class="col-4">
			<div class="form-group">
				<label class="form-control-label" for="price"><strong>Price</strong><small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="price" value="{{ old('price', $bladeVar['result']->price) }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			</div>

			<div class="col-4">
				<div class="form-group">
					<label class="form-control-label" for="discounted_price"><strong>Discounted Price</strong></label>
					<input type="text" class="form-control" name="discounted_price" value="{{ old('discounted_price', $bladeVar['result']->discounted_price) }}" autofocus>
					<small class="form-control-feedback"></small>
				</div>
			</div>
			<div class="col-4">
				<div class="form-group">
					<label class="form-control-label" for="product_quantity"><strong>Product Quantity</strong></label>
					<input type="text" class="form-control" name="product_quantity" value="{{ old('product_quantity', $bladeVar['result']->product_quantity) }}" autofocus>
					<small class="form-control-feedback"></small>
				</div>
			</div>

			<div class="col-4">
			<div class="form-group">
				<label class="form-control-label" for="image"><strong>Product Image (Size 990X990 px)</strong> <small class="text-danger">(required)</small></label>
				<input type="file" name="image" class="form-control">
				<small class="form-control-feedback"></small>
			</div>
			</div>
            </div>
            
			<div class="row">
		        <div class="col-4">
			     <div class="form-group">
					<label class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" value="1" name="featured" {{ $bladeVar['result']->featured == 1 ? ' checked="checked"' : '' }} >
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description"><strong>Featured</strong></span>
					</label>
			    </div>
			    </div>
			    <div class="col-4">
			     <div class="form-group">
					<label class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" value="1" name="add_to_cart" {{ $bladeVar['result']->add_to_cart == 1 ? ' checked="checked"' : '' }}>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description"><strong>Add To Cart</strong></span>
					</label>
			    </div>
			    </div>
		    </div>
			<div class="form-group">
				<label class="form-control-label" for="details"><strong>Details</strong></label>
				<textarea class="form-control" name="details">{{ old('details', $bladeVar['result']->details) }}</textarea>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="product_attributes"><strong>Product Attributes</strong></label>
				  <p>For multiple values use semicolon as seprator. e.g value1:value2;value3</p>
          <?php $attribute_data = json_decode($bladeVar['result']->attribute); ?>
				<div id="allAttributes">
				@foreach($attribute_data as $attribute)
				<div class="row">
		      <div class="col-3"><div class="form-group">
					    <input type="text" class="form-control" name="attrName[]" value="{{ old('attrName[]',$attribute->name) }}" autofocus placeholder="Attribute Name">
			    </div></div>
			    <div class="col-3"><div class="form-group">
					    <input type="text" class="form-control" name="attrVal[]" value="{{ old('attrVal[]',$attribute->value) }}" autofocus placeholder="Attribute Value">
			    </div></div>
			  </div>
			  @endforeach
			  </div>  
		    <div class="form-group text-right">
						<button class="btn btn-success" type="button" id="addAttribute">Add Attribute</button>
						<button class="btn btn-secondary removeAttribute" type="button">Remove Attribute</button>
				</div>
			</div>
			 <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
      	</form>
    </div>
</div>