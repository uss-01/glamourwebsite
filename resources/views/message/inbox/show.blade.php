<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;">Id</th>
		    		<td>{{ $bladeVar['result']->_id }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Title</th>
		    		<td>{{ $bladeVar['result']->title }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Album</th>
		    		<td>{{ $bladeVar['result']->album->name }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Image</th>
		    		<td><img src="{{ asset('images/galleries/photos').'/'.$bladeVar['result']->image }}" class="thumbnail" width="150" /></td>
		    	</tr>
		    	
		    </tbody>
    	</table>
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="{{ $bladeVar['result']->deleted_at == null ? route('photo.edit', $bladeVar['result']->_id) : '' }}" 
	    	data-destroy="{{ $bladeVar['result']->deleted_at == null ? route('photo.destroy', $bladeVar['result']->_id) : '' }}" 
	    	data-restore="{{ $bladeVar['result']->deleted_at != null ? route('photo.restore', $bladeVar['result']->_id) : '' }}"
	    	data-delete="{{ route('photo.delete', $bladeVar['result']->_id) }}">
	    </span>
	</div>
</div>	    