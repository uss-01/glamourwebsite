<div class="row">
	<div class="col-12">
		<form action="{{ route('message.store') }}" enctype="multipart/form-data" method="POST" id="brand_form">
			<input type="hidden" name="slug" value="homePage">
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>To </strong> 
					<small class="text-danger"> : </small></label>
					<select name="member" class="form-control2">
						<option value="all">All Member</option>
						<option value="Bronze">Bronze Member</option>
						<option value="Silver">Silver Member</option>
						<option value="Gold">Gold Member</option>
						<option value="Platinum">Platinum Member</option>
					</select>
					&
					<select name="gender">
						<option value="all">All Gender</option>
						<option value="male">Male</option>
						<option value="female">Female</option>
					</select>
					&
					<select name="age">
						<option value="all">All Age</option>
						<option value="15-25">15-25 Years</option>
						<option value="25-35">25-35 Years</option>
						<option value="35-45">35-45 Years</option>
						<option value="45-55">45-55 Years</option>
					</select>
				</div>
				<div class="form-group">
					<label class="form-control-label" for="name"><strong>Subject</strong> 
						<small class="text-danger">(required)</small></label>
						<input type="text" class="form-control" name="subject" value="{{ old('subject') }}" autofocus>
						<small class="form-control-feedback"></small>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="message"><strong>Message</strong> <small class="text-danger">(required)</small></label>
						<textarea class="form-control" name="message">{{ old('message') }}</textarea>
						<!-- <input type="text" class="form-control" name="message" value="{{ old('subject') }}" autofocus> -->
						<small class="form-control-feedback"></small>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="image"><strong>Image</strong>
							<small class="text-danger">(required)</small></label>
							<input type="file" name="image" class="form-control">
							<small class="form-control-feedback"></small>
						</div>
						<div class="form-group">
							<label class="form-control-label" for="redirect_to"><strong>Redirect To</strong>
								<small class="text-danger">{{-- (required) --}}</small></label>

								<select name="redirect_to" id="redirect_to" class="form-control">
									<option value="">Select Landing Page</option>
									<option value="product">Product</option>
									<option value="event">Event</option>
									<option value="shop">Shop</option>
									<option value="offer">Offer</option>
									<option value="old souq">Old Souq</option>
									<option value="dining">Dining</option>
									<option value="entertainment">Entertainment</option>
									<option value="gallery support">Gallery Support</option>
									<option value="other">other</option>
								</select>
								<small class="form-control-feedback"></small>
							</div>
							
							<div class="form-group" id="otherRedirectName" style="display:none;">
								<label class="form-control-label" for="rdirect_id"><strong>Enter Your Own Redirect to</strong>
								 <!-- <small class="text-danger">(required)</small> --></label>
								<input type="text" name="redirect_to_name" class="form-control">
								<small class="form-control-feedback"></small>
							</div>
							
							<div class="form-group">
								<label class="form-control-label" for="rdirect_id"><strong>Rdirect Id</strong>
									<small class="text-danger">{{-- (required) --}}</small></label>
									<input type="text" name="redirect_id" class="form-control">
									<small class="form-control-feedback"></small>
								</div>
								<div class="form-group text-right">

									<button class="btn btn-success upload-image" type="submit">Send</button>
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								</div>


							</form>
						</div>
					</div>
<script type="text/javascript">
    $(document).ready(function() {
		$( "#redirect_to" ).change(function() {
		  if($( "#redirect_to" ).val() == 'other'){
			  $( "#otherRedirectName" ).show();
		  } else {
			$( "#otherRedirectName" ).hide();
		  }
		});
	});
</script>