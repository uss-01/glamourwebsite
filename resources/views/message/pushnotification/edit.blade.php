<div class="row">
	<div class="col-12">
		<form action="{{ route('album.update', $bladeVar['result']->_id) }}" method="POST">
			{{ method_field('PUT') }}
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Name</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="name" value="{{ old('name', $bladeVar['result']->name) }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>
            <div class="form-group">
				<label class="form-control-label" for="image"><strong>Album Image</strong> <small class="text-danger">(required)</small></label>
				<input type="file" name="image" class="form-control">
				<small class="form-control-feedback"></small>
			</div>
			 <div class="form-group text-right">
     			<button class="btn btn-success upload-image" type="submit">Submit</button>
     			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    		</div>
			
      	</form>
    </div>
</div>