<div class="row">
	<div class="col-12">
		<form action="{{ route('siteusers.update', $bladeVar['result']->_id) }}" method="POST">
			{{ method_field('PUT') }}
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>First Name</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="fname" value="{{ old('fname', $bladeVar['result']->fname) }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>

			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Last Name</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="lname" value="{{ old('lname', $bladeVar['result']->lname) }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>

          <div class="form-group">
				<label class="form-control-label" for="name"><strong>Gender</strong> <small class="text-danger">(required)</small></label>
				
				<label class="radio-inline">
				<input type="radio"  name="gender" value="M" autofocus> Male </label>
                <label class="radio-inline">
				<input type="radio" name="gender" value="F" autofocus> Female
				</label>
				
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Date Of Birth</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control bg-ffffff" name="dob" value="{{ old('dob',$bladeVar['result']->dob) }}" readonly>
					<input type="hidden" name="dob_alt">
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="phone"><strong>Phone</strong> <small class="text-danger">(required)</small></label>
				<input type="text" class="form-control" name="phone" value="{{ old('phone', $bladeVar['result']->phone) }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="country"><strong>Country</strong> <small class="text-danger"></small></label>
				<input type="text" class="form-control" name="country" value="{{ old('country', $bladeVar['result']->country) }}" autofocus>
				<small class="form-control-feedback"></small>
			</div>
      	</form>
    </div>
</div>