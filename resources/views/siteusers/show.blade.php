<div class="row">
	<div class="col-12">
		<table class="table table-bordered table-sm mb-0">
		    <tbody>
		    	<tr>
		    		<th class="bg-faded" style="width: 8rem;"> Id</th>
		    		<td>{{ $bladeVar['result']->_id }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">First Name</th>
		    		<td>{{ $bladeVar['result']->fname }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Last Name</th>
		    		<td>{{ strtoupper($bladeVar['result']->lname) }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Email</th>
		    		<td>{{ $bladeVar['result']->email }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Phone</th>
		    		<td>{{ $bladeVar['result']->callingCode }} {{ $bladeVar['result']->phone }}</td>
		    	</tr>
		    	<tr>
		    		<th class="bg-faded">Country</th>
		    		<td>{{ $bladeVar['result']->country }}</td>
		    	</tr>
		    </tbody>
    	</table>
    	<span class="modalUrls hidden-xs-up" 
	    	data-edit="{{ $bladeVar['result']->deleted_at == null ? route('siteusers.edit', $bladeVar['result']->_id) : '' }}" 
	    	data-destroy="{{ $bladeVar['result']->deleted_at == null ? route('siteusers.destroy', $bladeVar['result']->_id) : '' }}" 
	    	data-restore="{{ $bladeVar['result']->deleted_at != null ? route('siteusers.restore', $bladeVar['result']->_id) : '' }}"
	    	data-delete="{{ route('siteusers.delete', $bladeVar['result']->_id) }}">
	    </span>
	</div>
</div>