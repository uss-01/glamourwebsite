<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::middleware(['auth', 'web'])->group(function () {
    Route::get('/admin', 'BootController@index')->name('boot');
});

Route::get('admin/login', function () {
    return view('auth/login');
});

Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function(){ 
    //Settings - Flush Cache
    Route::get('/setting/cache/flush', function () {
        Cache::flush();
        return redirect(route('boot'))->with('flushcache', '<strong>Cache Flushed!</strong> Entire cache of the system has been flushed!');
    })->name('cache.flush');

    // Site Users
    Route::match(['get', 'post'], 'siteusers/search', 'SiteUsersController@search')->name('siteusers.search');
    Route::match(['put', 'patch'], 'siteusers/restore/{source}', 'SiteUsersController@restore')->name('siteusers.restore');
    Route::delete('siteusers/delete/{source}', 'SiteUsersController@delete')->name('siteusers.delete');
    Route::resource('siteusers', 'SiteUsersController');
    // Category
    Route::match(['get', 'post'], 'master/category/search', 'Masters\CategoryController@search')->name('category.search');
    Route::match(['put', 'patch'], 'master/category/restore/{source}', 'Masters\CategoryController@restore')->name('category.restore');
    Route::delete('master/category/delete/{source}', 'Masters\CategoryController@delete')->name('category.delete');
    Route::resource('master/category', 'Masters\CategoryController');
    // Brand
    Route::match(['get', 'post'], 'master/brand/search', 'Masters\BrandController@search')->name('brand.search');
    Route::match(['put', 'patch'], 'master/brand/restore/{source}', 'Masters\BrandController@restore')->name('brand.restore');
    Route::delete('master/brand/delete/{source}', 'Masters\BrandController@delete')->name('brand.delete');
    Route::resource('master/brand', 'Masters\BrandController');
    // Store
    Route::match(['get', 'post'], 'master/store/search', 'Masters\StoreController@search')->name('store.search');
    Route::match(['put', 'patch'], 'master/store/restore/{source}', 'Masters\StoreController@restore')->name('store.restore');
    Route::delete('master/store/delete/{source}', 'Masters\StoreController@delete')->name('store.delete');
    Route::resource('master/store', 'Masters\StoreController');
    // Store Category
    Route::match(['get', 'post'], 'master/store_category/search', 'Masters\StoreCategoryController@search')->name('store_category.search');
    Route::match(['put', 'patch'], 'master/store_category/restore/{source}', 'Masters\StoreCategoryController@restore')->name('store_category.restore');
    Route::delete('master/store_category/delete/{source}', 'Masters\StoreCategoryController@delete')->name('store_category.delete');
    Route::resource('master/store_category', 'Masters\StoreCategoryController');
    // Store Subategory
    Route::match(['get', 'post'], 'master/store_subcategory/search', 'Masters\StoreSubcategoryController@search')->name('store_subcategory.search');
    Route::match(['put', 'patch'], 'master/store_subcategory/restore/{source}', 'Masters\StoreSubcategoryController@restore')->name('store_subcategory.restore');
    Route::delete('master/store_subcategory/delete/{source}', 'Masters\StoreSubcategoryController@delete')->name('store_subcategory.delete');
    Route::resource('master/store_subcategory', 'Masters\StoreSubcategoryController');
    // Product Category
    Route::match(['get', 'post'], 'master/product_category/search', 'Masters\ProductCategoryController@search')->name('product_category.search');
    Route::match(['put', 'patch'], 'master/product_category/restore/{source}', 'Masters\ProductCategoryController@restore')->name('product_category.restore');
    Route::delete('master/product_category/delete/{source}', 'Masters\ProductCategoryController@delete')->name('product_category.delete');
    Route::resource('master/product_category', 'Masters\ProductCategoryController');
    // Product Subategory
    Route::match(['get', 'post'], 'master/product_subcategory/search', 'Masters\ProductSubcategoryController@search')->name('product_subcategory.search');
    Route::match(['put', 'patch'], 'master/product_subcategory/restore/{source}', 'Masters\ProductSubcategoryController@restore')->name('product_subcategory.restore');
    Route::delete('master/product_subcategory/delete/{source}', 'Masters\ProductSubcategoryController@delete')->name('product_subcategory.delete');
    Route::resource('master/product_subcategory', 'Masters\ProductSubcategoryController');
    // Product Color
    Route::match(['get', 'post'], 'master/color/search', 'Masters\ColorController@search')->name('color.search');
    Route::match(['put', 'patch'], 'master/color/restore/{source}', 'Masters\ColorController@restore')->name('color.restore');
    Route::delete('master/color/delete/{source}', 'Masters\ColorController@delete')->name('color.delete');
    Route::resource('master/color', 'Masters\ColorController');
    // Product Size
    Route::match(['get', 'post'], 'master/size/search', 'Masters\SizeController@search')->name('size.search');
    Route::match(['put', 'patch'], 'master/size/restore/{source}', 'Masters\SizeController@restore')->name('size.restore');
    Route::delete('master/size/delete/{source}', 'Masters\SizeController@delete')->name('size.delete');
    Route::resource('master/size', 'Masters\SizeController');
    // Product Tags
    Route::match(['get', 'post'], 'master/tags/search', 'Masters\TagsController@search')->name('tags.search');
    Route::match(['put', 'patch'], 'master/tags/restore/{source}', 'Masters\TagsController@restore')->name('tags.restore');
    Route::delete('master/tags/delete/{source}', 'Masters\TagsController@delete')->name('tags.delete');
    Route::resource('master/tags', 'Masters\TagsController');
    // Experience
    Route::match(['get', 'post'], 'master/experience/search', 'Masters\ExperienceController@search')->name('experience.search');
    Route::match(['put', 'patch'], 'master/experience/restore/{source}', 'Masters\ExperienceController@restore')->name('experience.restore');
    Route::delete('master/experience/delete/{source}', 'Masters\ExperienceController@delete')->name('experience.delete');
    Route::resource('master/experience', 'Masters\ExperienceController');
    // Blog Category
    Route::match(['get', 'post'], 'master/blog_category/search', 'Masters\BlogCategoryController@search')->name('blog_category.search');
    Route::match(['put', 'patch'], 'master/blog_category/restore/{source}', 'Masters\BlogCategoryController@restore')->name('blog_category.restore');
    Route::delete('master/blog_category/delete/{source}', 'Masters\BlogCategoryController@delete')->name('blog_category.delete');
    Route::resource('master/blog_category', 'Masters\BlogCategoryController');
    // Home Page Images
    Route::match(['get', 'post'], 'master/homepageimage/search', 'Masters\HomePageImageController@search')->name('homepageimage.search');
    Route::match(['put', 'patch'], 'master/homepageimage/restore/{source}', 'Masters\HomePageImageController@restore')->name('homepageimage.restore');
    Route::delete('master/homepageimage/delete/{source}', 'Masters\HomePageImageController@delete')->name('homepageimage.delete');
    Route::resource('master/homepageimage', 'Masters\HomePageImageController');
    // Banner Images
    Route::match(['get', 'post'], 'master/bannerimages/search', 'Masters\BannerImagesController@search')->name('bannerimages.search');
    Route::match(['put', 'patch'], 'master/bannerimages/restore/{source}', 'Masters\BannerImagesController@restore')->name('bannerimages.restore');
    Route::match(['post'], 'master/bannerimages/product_subcategory', 'Masters\BannerImagesController@product_subcategory')->name('bannerimages.product_subcategory');
    Route::delete('master/bannerimages/delete/{source}', 'Masters\BannerImagesController@delete')->name('bannerimages.delete');
    Route::resource('master/bannerimages', 'Masters\BannerImagesController');
    // Single Banner Image
    Route::match(['get', 'post'], 'master/singlebannerimage/search', 'Masters\SingleBannerImageController@search')->name('singlebannerimage.search');
    Route::match(['put', 'patch'], 'master/singlebannerimage/restore/{source}', 'Masters\SingleBannerImageController@restore')->name('singlebannerimage.restore');
    Route::match(['post'], 'master/singlebannerimage/product_subcategory', 'Masters\SingleBannerImageController@product_subcategory')->name('singlebannerimage.product_subcategory');
    Route::delete('master/singlebannerimage/delete/{source}', 'Masters\SingleBannerImageController@delete')->name('singlebannerimage.delete');
    Route::resource('master/singlebannerimage', 'Masters\SingleBannerImageController');
    // Banner Slider
    Route::match(['get', 'post'], 'master/bannerslider/search', 'Masters\BannersliderController@search')->name('bannerslider.search');
    Route::match(['put', 'patch'], 'master/bannerslider/restore/{source}', 'Masters\BannersliderController@restore')->name('bannerslider.restore');
    Route::delete('master/bannerslider/delete/{source}', 'Masters\BannersliderController@delete')->name('bannerslider.delete');
    Route::resource('master/bannerslider', 'Masters\BannersliderController');
    // Testimonial
    Route::match(['get', 'post'], 'testimonial/search', 'Testimonial\TestimonialController@search')->name('testimonial.search');
    Route::match(['put', 'patch'], 'testimonial/restore/{source}', 'Testimonial\TestimonialController@restore')->name('testimonial.restore');
    Route::delete('testimonial/delete/{source}', 'Testimonial\TestimonialController@delete')->name('testimonial.delete');
    Route::resource('testimonial', 'Testimonial\TestimonialController');
    // Team
    Route::match(['get', 'post'], 'team/search', 'Team\TeamController@search')->name('team.search');
    Route::match(['put', 'patch'], 'team/restore/{source}', 'Team\TeamController@restore')->name('team.restore');
    Route::delete('team/delete/{source}', 'Team\TeamController@delete')->name('team.delete');
    Route::resource('team', 'Team\TeamController');
    // Coupons
    Route::match(['get', 'post'], 'coupons/search', 'Coupons\CouponsController@search')->name('coupons.search');
    Route::match(['put', 'patch'], 'coupons/restore/{source}', 'Coupons\CouponsController@restore')->name('coupons.restore');
    Route::delete('coupons/delete/{source}', 'Coupons\CouponsController@delete')->name('coupons.delete');
    Route::resource('coupons', 'Coupons\CouponsController');
    // Default Master
    Route::resource('master', 'Masters\CategoryController');

    //Product ----------------------------------------------
    Route::delete('product/deletePic/{source}', 'Product\ProductController@deletePic')->name('product.deletePic');
    Route::match(['post'], 'product/savePics', 'Product\ProductController@savePics')->name('product.savePics');
    Route::match(['get', 'post'], 'product/pics/{source}', 'Product\ProductController@pics')->name('product.pics');
    Route::match(['get', 'post'], 'product/search', 'Product\ProductController@search')->name('product.search');
    Route::match(['post'], 'product/product_subcategory', 'Product\ProductController@product_subcategory')->name('product.product_subcategory');
    Route::match(['put', 'patch'], 'product/restore/{source}', 'Product\ProductController@restore')->name('product.restore');
    Route::match(['post'], 'product/shop_brands', 'Product\ProductController@shop_brands')->name('product.shop_brands');
    Route::match(['put', 'patch'], 'product/restore/{source}', 'Product\ProductController@restore')->name('product.restore');
    Route::delete('product/delete/{source}', 'Product\ProductController@delete')->name('product.delete');
    Route::resource('product', 'Product\ProductController');
    //Gallery Album
    Route::match(['get', 'post'], 'gallery/album/search', 'Gallery\AlbumController@search')->name('album.search');
    Route::match(['put', 'patch'], 'gallery/album/restore/{source}', 'Gallery\AlbumController@restore')->name('album.restore');
    Route::delete('gallery/album/delete/{source}', 'Gallery\AlbumController@delete')->name('album.delete');
    Route::resource('gallery/album', 'Gallery\AlbumController');
    // Gallery Photos
    Route::match(['get', 'post'], 'gallery/photo/search', 'Gallery\PhotoController@search')->name('photo.search');
    Route::match(['put', 'patch'], 'gallery/photo/restore/{source}', 'Gallery\PhotoController@restore')->name('photo.restore');
    Route::delete('gallery/photo/delete/{source}', 'Gallery\PhotoController@delete')->name('photo.delete');
    Route::resource('gallery/photo', 'Gallery\PhotoController');
    Route::resource('gallery', 'Gallery\AlbumController');

    //CMS
    Route::match(['get', 'post'], 'cms/search', 'CmsController@search')->name('cms.search');
    Route::match(['put', 'patch'], 'cms/restore/{source}', 'CmsController@restore')->name('cms.restore');
    Route::delete('cms/delete/{source}', 'CmsController@delete')->name('cms.delete');
    Route::resource('cms', 'CmsController');
    //Blogs   ----------------------------------------------
    Route::delete('blogs/deletePic/{source}', 'Blogs\BlogsController@deletePic')->name('blogs.deletePic');
    Route::match(['post'], 'blogs/savePics', 'Blogs\BlogsController@savePics')->name('blogs.savePics');
    Route::match(['get', 'post'], 'blogs/pics/{source}', 'Blogs\BlogsController@pics')->name('blogs.pics');
    Route::match(['get', 'post'], 'blogs/search', 'Blogs\BlogsController@search')->name('blogs.search');
    Route::match(['put', 'patch'], 'blogs/restore/{source}', 'Blogs\BlogsController@restore')->name('blogs.restore');
    Route::delete('blogs/delete/{source}', 'Blogs\BlogsController@delete')->name('blogs.delete');
    Route::resource('blogs', 'Blogs\BlogsController');
    //Support -> Management Email
    Route::match(['get', 'post'], 'management/search', 'Support\ManagementController@search')->name('management.search');
    Route::match(['put', 'patch'], 'management/restore/{source}', 'Support\ManagementController@restore')->name('management.restore');
    Route::delete('management/delete/{source}', 'Support\ManagementController@delete')->name('management.delete');
    Route::resource('management', 'Support\ManagementController');  

    // Settings
    Route::match(['put', 'patch'], 'setting/restore/{source}', 'SettingController@restore')->name('setting.restore');
    Route::delete('setting/delete/{source}', 'SettingController@delete')->name('setting.delete');
    Route::resource('setting', 'SettingController');
    //Messages
    Route::match(['get', 'post'], 'message/search', 'Message\MessageController@search')->name('message.search');
    Route::match(['put', 'patch'], 'message/restore/{source}', 'Message\MessageController@restore')->name('message.restore');
    Route::match(['get', 'post'], 'message/search', 'Message\MessageController@search')->name('message.search');
    Route::delete('message/delete/{source}', 'Message\MessageController@delete')->name('message.delete');
    Route::resource('message', 'Message\MessageController');

    //Order List
    Route::match(['get', 'post'], 'orders/search', 'Orders\OrdersController@search')->name('orders.search');
    Route::match(['get', 'post'], 'orders/orderDetailsSearch', 'Orders\OrdersController@orderDetailsSearch')->name('orders.orderDetailsSearch');
    Route::match(['get', 'post'], 'orders/show/{source}', 'Orders\OrdersController@show')->name('orders.show');
    Route::match(['get', 'post'], 'orders/updateOrderStatus', 'Orders\OrdersController@updateOrderStatus')->name('orders.updateOrderStatus');
    Route::resource('orders', 'Orders\OrdersController');

});


//Frontend
Route::resource('/', 'HomeController');
//Route::get('/home', 'HomeController@index')->name('frontend.index');
Route::get('/about-us', 'HomeController@aboutus');
Route::get('/contact-us', 'HomeController@contactus');
Route::get('/blog', 'HomeController@allblogs');
Route::get('/blog/{slug}', 'HomeController@blog_detail');
Route::get('/shop', 'HomeController@shopProducts');
Route::get('/shop/{slug}', 'HomeController@product_detail');

Route::get('/search', 'HomeController@productSearch');

Route::get('/login-register', 'HomeController@login_register');
Route::get('/account', 'HomeController@myAccount');
Route::get('/wishlist', 'HomeController@userWishlist');
Route::get('/cart', 'HomeController@userCart');
Route::get('/checkout', 'HomeController@userCheckout');
Route::post('/contactform', 'HomeController@contactform');
Route::post('/sendcomment', 'HomeController@sendcomment');
Route::post('/usersignup', 'HomeController@usersignup');
Route::post('/userlogin', 'HomeController@userlogin');
Route::post('/userlogout', 'HomeController@userlogout');
Route::post('/userforgotPassword', 'HomeController@userforgotPassword');
Route::get('/recover-password', 'HomeController@userResetPassword');
Route::post('/userRecoverPassword', 'HomeController@userRecoverPassword');
Route::post('/userDetails', 'HomeController@userDetails');
Route::post('/updatePassword', 'HomeController@updatePassword');

// E-Shopping API
Route::post('/addtowishlist', 'HomeController@addToWishlist');
Route::post('/addtocart', 'HomeController@addToCart');
Route::post('/viewcart', 'HomeController@getViewcartItems');
Route::post('/updatecart', 'HomeController@updateCart');
Route::post('/removecartitem', 'HomeController@removeCartItem');
Route::post('/applycoupon', 'HomeController@applyCoupon');
Route::post('/removecoupon', 'HomeController@removeCoupon');
Route::post('/placeorder', 'HomeController@placeOrder');
Route::post('/addressBook', 'HomeController@addressBook');
Route::post('/myOrders', 'HomeController@myOrders');
Route::get('/ordersuccess', 'HomeController@userOrdersuccess');
Route::get('/ordercancel', 'HomeController@userOrdercancel');
Route::get('/orderDetail/{slug}', 'HomeController@orderDetail');
Route::post('/orderCancel', 'HomeController@orderCancel');

// payment form
Route::get('/paypalPayment', 'HomeController@paypalPayment');  
Route::post('/paypal', 'HomeController@payWithpaypal'); 
Route::get('/status', 'HomeController@getPaymentStatus');