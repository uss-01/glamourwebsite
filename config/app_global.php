<?php

return [
    'payment_method' => [
		'cod' => 'Cash On Deliver',
		'spu' => 'Selt Pickup'
	],
	'membership_arr' => [
		'Bronze' => 'Bronze',
		'Silver' => 'Silver',
		'Gold' => 'Gold',
		'Platinum' => 'Platinum'
	],
	'no_of_visits_arr' => ['0 - 50' => '0 - 50', '51 - 200' => '51 - 200', '201 - 500' => '201 - 500', 'Above 501' => 'Above 501'],
	'no_of_hours_spent_arr' => ['0 - 25' => '0 - 25', '26 - 100' => '26 - 100', '101 - 300' => '101 - 300', 'Above 301' => 'Above 301']
];
