-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 22, 2021 at 01:08 AM
-- Server version: 5.6.40-84.0-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ushyaku6_glamour`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner_images`
--

CREATE TABLE `banner_images` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `shortDescription` text NOT NULL,
  `category` int(11) UNSIGNED NOT NULL,
  `subcategory` int(11) UNSIGNED NOT NULL,
  `bannerPosition` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `btntext` varchar(255) NOT NULL,
  `created_by` int(11) UNSIGNED DEFAULT NULL,
  `updated_by` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banner_images`
--

INSERT INTO `banner_images` (`id`, `name`, `slug`, `shortDescription`, `category`, `subcategory`, `bannerPosition`, `image`, `btntext`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'BEAUTIFUL', 'beautiful', 'Wedding <span>Rings</span>', 36, 0, 'text-right', '1589536738_wedding-rings.jpg', 'Shop Now', 7, 7, '2020-05-15 04:28:58', '2020-12-28 02:02:24', NULL),
(6, 'EARRINGS', 'earrings', 'Tangerine Floral <span>Earring</span>', 36, 36, 'text-center', '1590409720_earrings.jpg', 'Shop Now', 7, 7, '2020-05-25 06:58:40', '2020-05-25 20:52:28', NULL),
(7, 'NEW ARRIVALLS', 'new-arrivalls', 'Pearl <span>Necklaces</span>', 36, 33, 'text-center', '1590409859_new-arrivalls.jpg', 'Shop Now', 7, 7, '2020-05-25 07:00:59', '2020-05-25 20:52:31', NULL),
(8, 'NEW DESIGN', 'new-design', 'Diamond <span>Jewelry</span>', 36, 34, 'text-center', '1590409928_new-design.jpg', 'Shop Now', 7, 7, '2020-05-25 07:02:08', '2020-06-03 03:51:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banner_slider`
--

CREATE TABLE `banner_slider` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `redirect_url` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `updated_by` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banner_slider`
--

INSERT INTO `banner_slider` (`id`, `name`, `slug`, `redirect_url`, `image`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'RINGS', 'rings', '', '1590735456_rings.jpg', 7, 0, '2020-05-29 01:27:36', '2020-06-03 04:03:24', NULL),
(2, 'BRACELATES', 'bracelates', '', '1591177399_bracelates.jpg', 7, 0, '2020-06-03 04:13:19', '2020-06-03 04:13:19', NULL),
(3, 'EARRINGS', 'earrings', '', '1591177417_earrings.jpg', 7, 0, '2020-06-03 04:13:37', '2020-06-03 04:13:37', NULL),
(4, 'NECJLACES', 'necjlaces', '', '1591177432_necjlaces.jpg', 7, 0, '2020-06-03 04:13:52', '2020-06-03 04:13:52', NULL),
(5, 'PEARLS', 'pearls', '', '1591177450_pearls.jpg', 7, 0, '2020-06-03 04:14:10', '2020-06-03 04:15:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `blog_category_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `blog_tag_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `blog_status` varchar(255) NOT NULL,
  `blog_audio` tinyint(1) NOT NULL DEFAULT '0',
  `blog_video` tinyint(1) NOT NULL DEFAULT '0',
  `blog_image_slide` tinyint(1) NOT NULL DEFAULT '0',
  `audio_url` text,
  `video_url` text,
  `created_by` int(11) UNSIGNED DEFAULT NULL,
  `updated_by` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`_id`, `name`, `slug`, `details`, `image`, `blog_category_id`, `blog_tag_id`, `blog_status`, `blog_audio`, `blog_video`, `blog_image_slide`, `audio_url`, `video_url`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Celebrity Daughter Opens Up About Having Her Eye Color Changed', 'celebrity-daughter-opens-up-about-having-her-eye-color-changed', '<p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate perferendis\n                                            consequuntur illo aliquid nihil ad neque, debitis praesentium libero ullam\n                                            asperiores exercitationem deserunt inventore facilis, officiis,</p>\n                                        <blockquote>\n                                            <p>Quisque semper nunc vitae erat pellentesque, ac placerat arcu consectetur.\n                                                venenatis elit ac ultrices convallis. Duis est nisi, tincidunt ac urna sed,\n                                                cursus blandit lectus. In ullamcorper sit amet ligula ut eleifend. Proin\n                                                dictum\n                                                tempor ligula, ac feugiat metus. Sed finibus tortor eu scelerisque\n                                                scelerisque.</p>\n                                        </blockquote>\n                                        <p>aliquam maiores asperiores recusandae commodi blanditiis ipsum tempora culpa\n                                            possimus assumenda ab quidem a voluptatum quia natus? Ex neque, saepe\n                                            reiciendis\n                                            quasi velit perspiciatis error vel quas quibusdam autem nesciunt voluptas odit\n                                            quis\n                                            dignissimos eos aspernatur voluptatum est repellat. Pariatur praesentium,\n                                            corrupti\n                                            deserunt ducimus quo doloremque nostrum aspernatur saepe cupiditate sit autem\n                                            exercitationem debitis, maiores vitae perferendis nemo? Voluptas illo, animi\n                                            temporibus quod earum molestias eaque, iure rem amet autem dignissimos officia\n                                            dolores numquam distinctio esse voluptates optio pariatur aspernatur omnis?\n                                            Ipsam\n                                            qui commodi velit natus reiciendis quod quibusdam nemo eveniet similique animi!</p>', '1591163228_celebrity-daughter-opens-up-about-having-her-eye-color-changed.jpg', 2, 3, 'Published', 0, 0, 0, '', '', 7, 7, '2020-06-02 00:43:01', '2020-06-03 00:26:23', NULL),
(2, 'Children Left Home Alone For 4 Days In TV series Experiment', 'children-left-home-alone-for-4-days-in-tv-series-experiment', '<p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate perferendis                                             consequuntur illo aliquid nihil ad neque, debitis praesentium libero ullam                                             asperiores exercitationem deserunt inventore facilis, officiis,</p>                                         <blockquote>                                             <p>Quisque semper nunc vitae erat pellentesque, ac placerat arcu consectetur.                                                 venenatis elit ac ultrices convallis. Duis est nisi, tincidunt ac urna sed,                                                 cursus blandit lectus. In ullamcorper sit amet ligula ut eleifend. Proin                                                 dictum                                                 tempor ligula, ac feugiat metus. Sed finibus tortor eu scelerisque                                                 scelerisque.</p>                                         </blockquote>                                         <p>aliquam maiores asperiores recusandae commodi blanditiis ipsum tempora culpa                                             possimus assumenda ab quidem a voluptatum quia natus? Ex neque, saepe                                             reiciendis                                             quasi velit perspiciatis error vel quas quibusdam autem nesciunt voluptas odit                                             quis                                             dignissimos eos aspernatur voluptatum est repellat. Pariatur praesentium,                                             corrupti                                             deserunt ducimus quo doloremque nostrum aspernatur saepe cupiditate sit autem                                             exercitationem debitis, maiores vitae perferendis nemo? Voluptas illo, animi                                             temporibus quod earum molestias eaque, iure rem amet autem dignissimos officia                                             dolores numquam distinctio esse voluptates optio pariatur aspernatur omnis?                                             Ipsam                                             qui commodi velit natus reiciendis quod quibusdam nemo eveniet similique animi!</p>', '1591178721_children-left-home-alone-for-4-days-in-tv-series-experiment.jpg', 4, 1, 'Published', 1, 0, 0, 'https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/501298839&color=%23ff5500&auto_play=false&hide_related=true&show_comments=false&show_user=true&show_reposts=false&show_teaser=true&visual=true', '', 7, 7, '2020-06-03 04:35:21', '2020-06-08 11:08:42', NULL),
(3, 'Lotto Winner Offering Up Money To Any Man That Will Date Her', 'lotto-winner-offering-up-money-to-any-man-that-will-date-her', '<p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate perferendis                                             consequuntur illo aliquid nihil ad neque, debitis praesentium libero ullam                                             asperiores exercitationem deserunt inventore facilis, officiis,</p>                                         <blockquote>                                             <p>Quisque semper nunc vitae erat pellentesque, ac placerat arcu consectetur.                                                 venenatis elit ac ultrices convallis. Duis est nisi, tincidunt ac urna sed,                                                 cursus blandit lectus. In ullamcorper sit amet ligula ut eleifend. Proin                                                 dictum                                                 tempor ligula, ac feugiat metus. Sed finibus tortor eu scelerisque                                                 scelerisque.</p>                                         </blockquote>                                         <p>aliquam maiores asperiores recusandae commodi blanditiis ipsum tempora culpa                                             possimus assumenda ab quidem a voluptatum quia natus? Ex neque, saepe                                             reiciendis                                             quasi velit perspiciatis error vel quas quibusdam autem nesciunt voluptas odit                                             quis                                             dignissimos eos aspernatur voluptatum est repellat. Pariatur praesentium,                                             corrupti                                             deserunt ducimus quo doloremque nostrum aspernatur saepe cupiditate sit autem                                             exercitationem debitis, maiores vitae perferendis nemo? Voluptas illo, animi                                             temporibus quod earum molestias eaque, iure rem amet autem dignissimos officia                                             dolores numquam distinctio esse voluptates optio pariatur aspernatur omnis?                                             Ipsam                                             qui commodi velit natus reiciendis quod quibusdam nemo eveniet similique animi!</p>', '1591178813_lotto-winner-offering-up-money-to-any-man-that-will-date-her.jpg', 2, 3, 'Published', 0, 0, 1, '', '', 7, 7, '2020-06-03 04:36:53', '2020-06-08 11:10:32', NULL),
(4, 'People are Willing Lie When Comes Money, According to Research', 'people-are-willing-lie-when-comes-money-according-to-research', '<p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate perferendis                                             consequuntur illo aliquid nihil ad neque, debitis praesentium libero ullam                                             asperiores exercitationem deserunt inventore facilis, officiis,</p>                                         <blockquote>                                             <p>Quisque semper nunc vitae erat pellentesque, ac placerat arcu consectetur.                                                 venenatis elit ac ultrices convallis. Duis est nisi, tincidunt ac urna sed,                                                 cursus blandit lectus. In ullamcorper sit amet ligula ut eleifend. Proin                                                 dictum                                                 tempor ligula, ac feugiat metus. Sed finibus tortor eu scelerisque                                                 scelerisque.</p>                                         </blockquote>                                         <p>aliquam maiores asperiores recusandae commodi blanditiis ipsum tempora culpa                                             possimus assumenda ab quidem a voluptatum quia natus? Ex neque, saepe                                             reiciendis                                             quasi velit perspiciatis error vel quas quibusdam autem nesciunt voluptas odit                                             quis                                             dignissimos eos aspernatur voluptatum est repellat. Pariatur praesentium,                                             corrupti                                             deserunt ducimus quo doloremque nostrum aspernatur saepe cupiditate sit autem                                             exercitationem debitis, maiores vitae perferendis nemo? Voluptas illo, animi                                             temporibus quod earum molestias eaque, iure rem amet autem dignissimos officia                                             dolores numquam distinctio esse voluptates optio pariatur aspernatur omnis?                                             Ipsam                                             qui commodi velit natus reiciendis quod quibusdam nemo eveniet similique animi!</p>', '1591178884_people-are-willing-lie-when-comes-money,-according-to-research.jpg', 4, 1, 'Published', 0, 1, 0, '', 'https://www.youtube.com/embed/4qNHr0W6F0o', 7, 7, '2020-06-03 04:38:04', '2020-06-08 11:10:11', NULL),
(5, 'Romantic Love Stories Of Hollywood\'s Biggest Celebrities', 'romantic-love-stories-of-hollywoods-biggest-celebrities', '<p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate perferendis\r\n                                            consequuntur illo aliquid nihil ad neque, debitis praesentium libero ullam\r\n                                            asperiores exercitationem deserunt inventore facilis, officiis,</p>\r\n                                        <blockquote>\r\n                                            <p>Quisque semper nunc vitae erat pellentesque, ac placerat arcu consectetur.\r\n                                                venenatis elit ac ultrices convallis. Duis est nisi, tincidunt ac urna sed,\r\n                                                cursus blandit lectus. In ullamcorper sit amet ligula ut eleifend. Proin\r\n                                                dictum\r\n                                                tempor ligula, ac feugiat metus. Sed finibus tortor eu scelerisque\r\n                                                scelerisque.</p>\r\n                                        </blockquote>\r\n                                        <p>aliquam maiores asperiores recusandae commodi blanditiis ipsum tempora culpa\r\n                                            possimus assumenda ab quidem a voluptatum quia natus? Ex neque, saepe\r\n                                            reiciendis\r\n                                            quasi velit perspiciatis error vel quas quibusdam autem nesciunt voluptas odit\r\n                                            quis\r\n                                            dignissimos eos aspernatur voluptatum est repellat. Pariatur praesentium,\r\n                                            corrupti\r\n                                            deserunt ducimus quo doloremque nostrum aspernatur saepe cupiditate sit autem\r\n                                            exercitationem debitis, maiores vitae perferendis nemo? Voluptas illo, animi\r\n                                            temporibus quod earum molestias eaque, iure rem amet autem dignissimos officia\r\n                                            dolores numquam distinctio esse voluptates optio pariatur aspernatur omnis?\r\n                                            Ipsam\r\n                                            qui commodi velit natus reiciendis quod quibusdam nemo eveniet similique animi!</p>', '1591179319_romantic-love-stories-of-hollywood\'s-biggest-celebrities.jpg', 2, 1, 'Published', 0, 0, 0, '', '', 7, 7, '2020-05-03 04:39:43', '2020-06-14 12:27:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `updated_by` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`_id`, `name`, `slug`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Barber', 'barber', 7, 0, '2020-05-30 07:38:01', '2020-05-30 07:38:01', NULL),
(2, 'Fashion', 'fashion', 7, 0, '2020-05-30 07:38:53', '2020-05-30 07:38:53', NULL),
(3, 'Handbag', 'handbag', 7, 0, '2020-05-30 07:39:13', '2020-05-30 07:39:13', NULL),
(4, 'Jewelry', 'jewelry', 7, 0, '2020-05-30 07:39:33', '2020-05-30 07:39:33', NULL),
(5, 'Food', 'food', 7, 0, '2020-05-30 07:39:43', '2020-05-30 07:39:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blog_pics`
--

CREATE TABLE `blog_pics` (
  `_id` int(11) UNSIGNED NOT NULL,
  `blog_id` int(11) UNSIGNED NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `updated_by` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blog_pics`
--

INSERT INTO `blog_pics` (`_id`, `blog_id`, `image`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, '1591160253_1.jpg', 7, 0, '2020-06-02 23:27:33', '2020-06-02 23:27:33'),
(2, 1, '1591160253_2.jpg', 7, 0, '2020-06-02 23:27:33', '2020-06-02 23:27:33'),
(3, 1, '1591160253_3.jpg', 7, 0, '2020-06-02 23:27:33', '2020-06-02 23:27:33'),
(4, 1, '1591160253_4.jpg', 7, 0, '2020-06-02 23:27:33', '2020-06-02 23:27:33'),
(17, 1, '1591162986_1.jpg', 7, 0, '2020-06-03 00:13:06', '2020-06-03 00:13:06'),
(18, 2, '1591179362_1.jpg', 7, 0, '2020-06-03 04:46:02', '2020-06-03 04:46:02'),
(19, 2, '1591179362_2.jpg', 7, 0, '2020-06-03 04:46:02', '2020-06-03 04:46:02'),
(20, 2, '1591179362_3.jpg', 7, 0, '2020-06-03 04:46:02', '2020-06-03 04:46:02'),
(21, 2, '1591179362_4.jpg', 7, 0, '2020-06-03 04:46:02', '2020-06-03 04:46:02'),
(22, 3, '1591179373_1.jpg', 7, 0, '2020-06-03 04:46:13', '2020-06-03 04:46:13'),
(23, 3, '1591179373_2.jpg', 7, 0, '2020-06-03 04:46:13', '2020-06-03 04:46:13'),
(24, 3, '1591179373_3.jpg', 7, 0, '2020-06-03 04:46:13', '2020-06-03 04:46:13'),
(25, 3, '1591179373_4.jpg', 7, 0, '2020-06-03 04:46:13', '2020-06-03 04:46:13'),
(26, 4, '1591179383_1.jpg', 7, 0, '2020-06-03 04:46:23', '2020-06-03 04:46:23'),
(27, 4, '1591179383_2.jpg', 7, 0, '2020-06-03 04:46:23', '2020-06-03 04:46:23'),
(28, 4, '1591179383_3.jpg', 7, 0, '2020-06-03 04:46:23', '2020-06-03 04:46:23'),
(29, 5, '1591179395_1.jpg', 7, 0, '2020-06-03 04:46:35', '2020-06-03 04:46:35'),
(30, 5, '1591179395_2.jpg', 7, 0, '2020-06-03 04:46:35', '2020-06-03 04:46:35');

-- --------------------------------------------------------

--
-- Table structure for table `blog_tags`
--

CREATE TABLE `blog_tags` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `updated_by` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blog_tags`
--

INSERT INTO `blog_tags` (`id`, `name`, `slug`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Necklaces', 'necklaces', 7, 7, '2020-05-25 22:42:45', '2020-05-25 22:43:13', NULL),
(2, 'Earrings', 'earrings', 7, 7, '2020-05-25 22:43:08', '2020-05-25 22:43:32', NULL),
(3, 'Jewellery', 'jewellery', 7, 0, '2020-05-30 07:40:45', '2020-05-30 07:40:45', NULL),
(4, 'Camera', 'camera', 7, 0, '2020-05-30 07:41:00', '2020-05-30 07:41:00', NULL),
(5, 'Computer', 'computer', 7, 0, '2020-05-30 07:41:14', '2020-05-30 07:41:14', NULL),
(6, 'Bag', 'bag', 7, 0, '2020-05-30 07:41:28', '2020-05-30 07:41:28', NULL),
(7, 'Watch', 'watch', 7, 0, '2020-05-30 07:41:40', '2020-05-30 07:41:40', NULL),
(8, 'Smartphone', 'smartphone', 7, 0, '2020-05-30 07:41:52', '2020-05-30 07:41:52', NULL),
(9, 'Shoes', 'shoes', 7, 0, '2020-05-30 07:42:05', '2020-05-30 07:42:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms`
--

CREATE TABLE `cms` (
  `_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms`
--

INSERT INTO `cms` (`_id`, `name`, `slug`, `title`, `image`, `content`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'About Us', 'about-us', 'About Us', '1589810092_about-us.jpg', '<h5 class=\"about-sub-title\">\n                                Founded in 1986, I.D. Jewelry, LLC, a family owned &amp; operated business has become a house-hold name in states all over the USA as well as countries all over the world.\n                            </h5>\n                            <p>For those that rather the full immersion of the in store experience we welcome your company and look forward to meeting you face to face. Being located in the 47 street diamond district, known to be the largest diamond.</p>\n                            <p>Based in the heart of New York’s Diamond District, also known as the NYC diamond district, I. D. Jewelry has some of the most competitively priced in the market. It is our goal to supply our clients.</p>', 3, 7, '2017-09-09 14:19:21', '2020-06-06 21:56:05', NULL),
(3, 'Contact Us', 'contact-us', 'Contact Us', '1556276840_contact-us.jpg', '<p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum\n                                est notare quam littera gothica, quam nunc putamus parum claram anteposuerit litterarum\n                                formas human.</p>\n                            <ul>\n                                <li><i class=\"fa fa-fax\"></i> Address : No 40 Baria Sreet 133/2 NewYork City</li>\n                                <li><i class=\"fa fa-phone\"></i> E-mail: info@yourdomain.com</li>\n                                <li><i class=\"fa fa-envelope-o\"></i> +88013245657</li>\n                            </ul>\n                            <div class=\"working-time\">\n                                <h6>Working Hours</h6>\n                                <p><span>Monday – Saturday:</span>08AM – 22PM</p>\n                            </div>', 2, 7, '2018-02-02 08:20:40', '2020-05-18 08:32:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `postName` varchar(255) NOT NULL,
  `postId` int(11) NOT NULL,
  `reply` text NOT NULL,
  `replyBy` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `updated_by` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `token`, `name`, `email`, `website`, `comment`, `postName`, `postId`, `reply`, `replyBy`, `type`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'v9GosDEPwAdFiDIC0682yN8KqrQ0QDSQubJtjA4K', 'Jon doe', 'jondoe@gmail.com', 'http://localhost', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim maiores adipisci optio ex, laboriosam facilis non pariatur itaque illo sunt?', 'Romantic Love Stories Of Hollywood\'s Biggest Celebrities', 5, '', '', 'blog', 0, 0, '2020-06-17 01:24:37', '2020-06-17 01:24:37', NULL),
(2, '341MZAw0ASNBTDbJjyM4kUfxE5nmxbAECMGsvarT', 'Jonathan Scott', 'jonathan@gmail.com', 'http://localhost/laravel', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim maiores adipisci optio ex, laboriosam facilis non pariatur itaque illo sunt?', 'Romantic Love Stories Of Hollywood\'s Biggest Celebrities', 5, '', '', 'blog', 0, 0, '2020-06-17 01:41:04', '2020-06-17 01:41:04', NULL),
(3, 'wRKCaUAagNBMvteIFwfQwA5eIzcwfjUU7alwu7m4', 'Jonathan Scott', 'jondoe@gmail.com', 'http://localhost/laravel/', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate perferendis consequuntur illo aliquid nihil ad neque, debitis praesentium libero ullam asperiores exercitationem deserunt inventore facilis, officiis,', 'Lotto Winner Offering Up Money To Any Man That Will Date Her', 3, '', '', 'blog', 0, 0, '2020-06-18 12:49:33', '2020-06-18 12:49:33', NULL),
(4, 'evLlFPHuPBXBnF15eALyKwqVJ9p7zm789Qv4nvWh', 'Jon doe', 'jondoe@gmail.com', '5', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim maiores adipisci optio ex, laboriosam facilis non pariatur itaque illo sunt?', 'Exclusive silver top bracellet', 5, '', '', 'product', 0, 0, '2020-06-24 08:12:32', '2020-06-24 08:12:32', NULL),
(5, 'evLlFPHuPBXBnF15eALyKwqVJ9p7zm789Qv4nvWh', 'Jonathan Scott', 'jonathan@gmail.com', '4', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim maiores adipisci optio ex, laboriosam facilis non pariatur itaque illo sunt?', 'Exclusive silver top bracellet', 5, '', '', 'product', 0, 0, '2020-06-24 08:16:42', '2020-06-24 08:16:42', NULL),
(6, 'evLlFPHuPBXBnF15eALyKwqVJ9p7zm789Qv4nvWh', 'Erik jonson', 'jonson@gmail.com', '3', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim maiores adipisci optio ex, laboriosam facilis non pariatur itaque illo sunt?', 'Exclusive silver top bracellet', 5, '', '', 'product', 0, 0, '2020-06-24 11:00:35', '2020-06-24 11:00:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subject` text NOT NULL,
  `message` longtext NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `updated_by` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `token`, `name`, `phone`, `email`, `subject`, `message`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '', 'test', '1234567890', 'test@gmail.com', 'test', 'test', 0, 0, '2020-06-07 05:21:58', '2020-06-07 05:21:58', NULL),
(2, 'eEh8buvgvgqeHa2tqsNLmBZMHa4deG11F4BDgonl', 'test', '1234567890', 'test@gmail.com', 'test', 'test', 0, 0, '2020-06-07 05:24:11', '2020-06-07 05:24:11', NULL),
(3, 'eEh8buvgvgqeHa2tqsNLmBZMHa4deG11F4BDgonl', 'test user', '1234567890', 'test@gmail.com', 'test', 'test', 0, 0, '2020-06-07 05:33:30', '2020-06-07 05:33:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `expiry` varchar(255) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `updated_by` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `name`, `slug`, `value`, `quantity`, `type`, `expiry`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'testCoupon', 'testcoupon', '15', '100', 'Flat', '08/21/2020 12:00 AM', 7, 7, '2020-07-17 04:07:37', '2020-07-19 00:00:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `entry_exit_time`
--

CREATE TABLE `entry_exit_time` (
  `sno` int(10) UNSIGNED NOT NULL,
  `site_user_id` int(10) UNSIGNED NOT NULL,
  `entry` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_location` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event_pics`
--

CREATE TABLE `event_pics` (
  `_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `experiences`
--

CREATE TABLE `experiences` (
  `_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

CREATE TABLE `favourites` (
  `_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `floor_plans`
--

CREATE TABLE `floor_plans` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_albums`
--

CREATE TABLE `gallery_albums` (
  `_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_images`
--

CREATE TABLE `gallery_images` (
  `_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `album_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallery_images`
--

INSERT INTO `gallery_images` (`_id`, `title`, `image`, `album_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'CEO', '1504270474_ceo-1.jpg', 1, 4, NULL, '2017-09-01 12:54:34', '2017-09-01 12:54:34', NULL),
(2, 'Travel 2017', '1504874866_travel-2017-1.jpg', 2, 3, NULL, '2017-09-08 12:47:46', '2017-09-08 12:47:46', NULL),
(49, '1', '1523886329_1.jpg', 21, 4, 4, '2018-04-16 13:31:36', '2018-04-16 13:45:29', NULL),
(52, '2', '1523885549_2-1.jpg', 21, 4, NULL, '2018-04-16 13:32:29', '2018-04-16 13:32:29', NULL),
(53, '3', '1523885584_3-1.jpg', 21, 4, NULL, '2018-04-16 13:33:05', '2018-04-16 13:33:05', NULL),
(54, '4', '1523885642_4-1.jpg', 21, 4, NULL, '2018-04-16 13:34:02', '2018-04-16 13:34:02', NULL),
(57, '5', '1523885729_5-1.jpg', 21, 4, NULL, '2018-04-16 13:35:29', '2018-04-16 13:35:29', NULL),
(58, '6', '1523885779_6-1.jpg', 21, 4, NULL, '2018-04-16 13:36:19', '2018-04-16 13:36:19', NULL),
(59, '7', '1523885835_7-1.jpg', 21, 4, NULL, '2018-04-16 13:37:15', '2018-04-16 13:37:15', NULL),
(60, '8', '1523885876_8-1.jpg', 21, 4, NULL, '2018-04-16 13:37:56', '2018-04-16 13:37:56', NULL),
(61, '10', '1523886310_10-1.jpg', 21, 4, NULL, '2018-04-16 13:45:10', '2018-04-16 13:45:10', NULL),
(64, '1', '1551267492_1-1.jpg', 29, 4, NULL, '2019-02-27 11:38:12', '2019-02-27 11:38:12', NULL),
(65, '2', '1551267504_2-1.jpg', 29, 4, NULL, '2019-02-27 11:38:24', '2019-02-27 11:38:24', NULL),
(66, '3', '1551267513_3-1.jpg', 29, 4, NULL, '2019-02-27 11:38:33', '2019-02-27 11:38:33', NULL),
(67, '4', '1551267525_4-1.jpg', 29, 4, NULL, '2019-02-27 11:38:45', '2019-02-27 11:38:45', NULL),
(68, '5', '1551267537_5-1.jpg', 29, 4, NULL, '2019-02-27 11:38:57', '2019-02-27 11:38:57', NULL),
(69, '6', '1551267551_6-1.jpg', 29, 4, NULL, '2019-02-27 11:39:11', '2019-02-27 11:39:11', NULL),
(70, '7', '1551267561_7-1.jpg', 29, 4, NULL, '2019-02-27 11:39:21', '2019-02-27 11:39:21', NULL),
(71, '8', '1551267572_8-1.jpg', 29, 4, NULL, '2019-02-27 11:39:32', '2019-02-27 11:39:32', NULL),
(72, 'photo', '1551267585_photo-1.jpg', 29, 4, NULL, '2019-02-27 11:39:45', '2019-02-27 11:39:45', NULL),
(73, 'photo', '1551267597_photo-1.jpg', 29, 4, NULL, '2019-02-27 11:39:57', '2019-02-27 11:39:57', NULL),
(74, 'photo', '1551267606_photo-1.jpg', 29, 4, NULL, '2019-02-27 11:40:06', '2019-02-27 11:40:06', NULL),
(75, 'photo', '1551267616_photo-1.jpg', 29, 4, NULL, '2019-02-27 11:40:16', '2019-02-27 11:40:16', NULL),
(76, 'photo', '1551267631_photo-1.jpg', 29, 4, NULL, '2019-02-27 11:40:31', '2019-02-27 11:40:31', NULL),
(77, 'photo', '1551267641_photo-1.jpg', 29, 4, NULL, '2019-02-27 11:40:41', '2019-02-27 11:40:41', NULL),
(78, 'photo', '1551267652_photo-1.jpg', 29, 4, NULL, '2019-02-27 11:40:52', '2019-02-27 11:40:52', NULL),
(79, 'photo', '1551268245_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:50:45', '2019-02-27 11:50:45', NULL),
(80, 'photo', '1551268254_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:50:54', '2019-02-27 11:50:54', NULL),
(81, 'photo', '1551268263_photo-1.jpg', 23, 4, 4, '2019-02-27 11:51:03', '2019-02-27 11:53:16', NULL),
(82, 'photo', '1551268274_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:51:14', '2019-02-27 11:51:14', NULL),
(83, 'photo', '1551268285_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:51:25', '2019-02-27 11:51:25', NULL),
(84, 'photo', '1551268294_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:51:34', '2019-02-27 11:51:34', NULL),
(85, 'photo', '1551268304_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:51:44', '2019-02-27 11:51:44', NULL),
(86, 'photo', '1551268314_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:51:54', '2019-02-27 11:51:54', NULL),
(87, 'photo', '1551268324_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:52:04', '2019-02-27 11:52:04', NULL),
(88, 'photo', '1551268347_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:52:27', '2019-02-27 11:52:27', NULL),
(89, 'photo', '1551268357_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:52:37', '2019-02-27 11:52:37', NULL),
(90, 'photo', '1551268422_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:53:42', '2019-02-27 11:53:42', NULL),
(91, 'photo', '1551268432_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:53:52', '2019-02-27 11:53:52', NULL),
(92, 'photo', '1551268442_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:54:02', '2019-02-27 11:54:02', NULL),
(93, 'photo', '1551268454_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:54:14', '2019-02-27 11:54:14', NULL),
(94, 'photo', '1551268465_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:54:25', '2019-02-27 11:54:25', NULL),
(95, 'photo', '1551268475_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:54:35', '2019-02-27 11:54:35', NULL),
(96, 'photo', '1551268485_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:54:45', '2019-02-27 11:54:45', NULL),
(97, 'photo', '1551268496_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:54:56', '2019-02-27 11:54:56', NULL),
(98, 'photo', '1551268507_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:55:07', '2019-02-27 11:55:07', NULL),
(99, 'photo', '1551268516_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:55:16', '2019-02-27 11:55:16', NULL),
(100, 'photo', '1551268529_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:55:29', '2019-02-27 11:55:29', NULL),
(101, 'photo', '1551268542_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:55:42', '2019-02-27 11:55:42', NULL),
(102, 'photo', '1551268553_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:55:53', '2019-02-27 11:55:53', NULL),
(103, 'photo', '1551268566_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:56:06', '2019-02-27 11:56:06', NULL),
(104, 'photo', '1551268579_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:56:19', '2019-02-27 11:56:19', NULL),
(105, 'photo', '1551268592_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:56:32', '2019-02-27 11:56:32', NULL),
(106, 'photo', '1551268602_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:56:42', '2019-02-27 11:56:42', NULL),
(107, 'photo', '1551268613_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:56:53', '2019-02-27 11:56:53', NULL),
(108, 'photo', '1551268623_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:57:03', '2019-02-27 11:57:03', NULL),
(109, 'photo', '1551268633_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:57:13', '2019-02-27 11:57:13', NULL),
(110, 'photo', '1551268642_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:57:22', '2019-02-27 11:57:22', NULL),
(111, 'photo', '1551268652_photo-1.jpg', 23, 4, NULL, '2019-02-27 11:57:32', '2019-02-27 11:57:32', NULL),
(112, 'photo', '1551344947_photo-1.jpg', 30, 4, NULL, '2019-02-28 09:09:07', '2019-02-28 09:09:07', NULL),
(113, 'photo', '1551344963_photo-1.jpg', 30, 4, NULL, '2019-02-28 09:09:23', '2019-02-28 09:09:23', NULL),
(114, 'photo', '1551344974_photo-1.jpg', 30, 4, NULL, '2019-02-28 09:09:34', '2019-02-28 09:09:34', NULL),
(115, 'photo', '1551344986_photo-1.jpg', 30, 4, NULL, '2019-02-28 09:09:46', '2019-02-28 09:09:46', NULL),
(116, 'photo', '1551345006_photo-1.jpg', 30, 4, NULL, '2019-02-28 09:10:06', '2019-02-28 09:10:06', NULL),
(117, 'photo', '1551345017_photo-1.jpg', 30, 4, NULL, '2019-02-28 09:10:17', '2019-02-28 09:10:17', NULL),
(118, 'photo', '1551345026_photo-1.jpg', 30, 4, NULL, '2019-02-28 09:10:26', '2019-02-28 09:10:26', NULL),
(119, 'photo', '1551345040_photo-1.jpg', 30, 4, NULL, '2019-02-28 09:10:40', '2019-02-28 09:10:40', NULL),
(120, 'photo', '1551345054_photo-1.jpg', 30, 4, NULL, '2019-02-28 09:10:54', '2019-02-28 09:10:54', NULL),
(121, 'photo', '1551345063_photo-1.jpg', 30, 4, NULL, '2019-02-28 09:11:03', '2019-02-28 09:11:03', NULL),
(122, 'photo', '1551345072_photo-1.jpg', 30, 4, NULL, '2019-02-28 09:11:12', '2019-02-28 09:11:12', NULL),
(123, 'photo', '1551345081_photo-1.jpg', 30, 4, NULL, '2019-02-28 09:11:21', '2019-02-28 09:11:21', NULL),
(124, 'photo', '1551770032_photo-1.jpg', 31, 4, NULL, '2019-03-05 07:13:52', '2019-03-05 07:13:52', NULL),
(125, 'photo', '1551770046_photo-1.jpg', 31, 4, NULL, '2019-03-05 07:14:06', '2019-03-05 07:14:06', NULL),
(126, 'photo', '1551770086_photo-1.jpg', 31, 4, NULL, '2019-03-05 07:14:46', '2019-03-05 07:14:46', NULL),
(127, 'photo', '1551770094_photo-1.jpg', 31, 4, NULL, '2019-03-05 07:14:54', '2019-03-05 07:14:54', NULL),
(128, 'photo', '1551770103_photo-1.jpg', 31, 4, NULL, '2019-03-05 07:15:03', '2019-03-05 07:15:03', NULL),
(129, 'photo', '1551770112_photo-1.jpg', 31, 4, NULL, '2019-03-05 07:15:12', '2019-03-05 07:15:12', NULL),
(130, 'photo', '1551770120_photo-1.jpg', 31, 4, NULL, '2019-03-05 07:15:20', '2019-03-05 07:15:20', NULL),
(131, 'photo', '1551770127_photo-1.jpg', 31, 4, NULL, '2019-03-05 07:15:27', '2019-03-05 07:15:27', NULL),
(132, 'photo', '1551770134_photo-1.jpg', 31, 4, NULL, '2019-03-05 07:15:34', '2019-03-05 07:15:34', NULL),
(133, 'photo', '1551770142_photo-1.jpg', 31, 4, NULL, '2019-03-05 07:15:42', '2019-03-05 07:15:42', NULL),
(134, 'photo', '1551770150_photo-1.jpg', 31, 4, NULL, '2019-03-05 07:15:50', '2019-03-05 07:15:50', NULL),
(135, 'photo', '1551770160_photo-1.jpg', 31, 4, NULL, '2019-03-05 07:16:00', '2019-03-05 07:16:00', NULL),
(136, 'photo', '1551770173_photo-1.jpg', 31, 4, NULL, '2019-03-05 07:16:13', '2019-03-05 07:16:13', NULL),
(137, 'photo', '1551770181_photo-1.jpg', 31, 4, NULL, '2019-03-05 07:16:21', '2019-03-05 07:16:21', NULL),
(138, 'photo', '1551770191_photo-1.jpg', 31, 4, NULL, '2019-03-05 07:16:31', '2019-03-05 07:16:31', NULL),
(139, 'photo', '1551770208_photo-1.jpg', 31, 4, NULL, '2019-03-05 07:16:48', '2019-03-05 07:16:48', NULL),
(140, 'photo', '1551770220_photo-1.jpg', 31, 4, NULL, '2019-03-05 07:17:00', '2019-03-05 07:17:00', NULL),
(141, 'photo', '1551770243_photo-1.jpg', 31, 4, NULL, '2019-03-05 07:17:23', '2019-03-05 07:17:23', NULL),
(142, 'photo', '1551770251_photo-1.jpg', 31, 4, NULL, '2019-03-05 07:17:31', '2019-03-05 07:17:31', NULL),
(143, 'FW 1', '1554709334_fw-1-1.jpg', 32, 4, NULL, '2019-04-08 07:42:14', '2019-04-08 07:42:14', NULL),
(144, 'FW 2', '1554709347_fw-2-1.jpg', 32, 4, NULL, '2019-04-08 07:42:27', '2019-04-08 07:42:27', NULL),
(145, 'FW 3', '1554709358_fw-3-1.jpg', 32, 4, NULL, '2019-04-08 07:42:38', '2019-04-08 07:42:38', NULL),
(146, 'FW 4', '1554709372_fw-4-1.jpg', 32, 4, NULL, '2019-04-08 07:42:52', '2019-04-08 07:42:52', NULL),
(147, 'FW 5', '1554709385_fw-5-1.jpg', 32, 4, NULL, '2019-04-08 07:43:05', '2019-04-08 07:43:05', NULL),
(148, 'FW 6', '1554709398_fw-6-1.jpg', 32, 4, NULL, '2019-04-08 07:43:18', '2019-04-08 07:43:18', NULL),
(149, 'FW 7', '1554709408_fw-7-1.jpg', 32, 4, 4, '2019-04-08 07:43:28', '2019-04-08 07:43:45', NULL),
(150, 'FW 8', '1554709441_fw-8-1.jpg', 32, 4, NULL, '2019-04-08 07:44:01', '2019-04-08 07:44:01', NULL),
(151, 'FW 9', '1554709454_fw-9-1.jpg', 32, 4, NULL, '2019-04-08 07:44:14', '2019-04-08 07:44:14', NULL),
(152, 'FW 10', '1554709467_fw-10-1.jpg', 32, 4, NULL, '2019-04-08 07:44:27', '2019-04-08 07:44:27', NULL),
(153, 'FW 11', '1554709478_fw-11-1.jpg', 32, 4, NULL, '2019-04-08 07:44:38', '2019-04-08 07:44:38', NULL),
(154, 'FW 12', '1554709491_fw-12-1.jpg', 32, 4, NULL, '2019-04-08 07:44:51', '2019-04-08 07:44:51', NULL),
(155, 'FW 13', '1554709504_fw-13-1.jpg', 32, 4, NULL, '2019-04-08 07:45:04', '2019-04-08 07:45:04', NULL),
(156, 'FW 14', '1554709516_fw-14-1.jpg', 32, 4, NULL, '2019-04-08 07:45:16', '2019-04-08 07:45:16', NULL),
(157, 'FW 15', '1554709528_fw-15-1.jpg', 32, 4, NULL, '2019-04-08 07:45:28', '2019-04-08 07:45:28', NULL),
(158, 'FW 16', '1554709558_fw-16-1.jpg', 32, 4, NULL, '2019-04-08 07:45:58', '2019-04-08 07:45:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `incidents`
--

CREATE TABLE `incidents` (
  `_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `image3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `leasing`
--

CREATE TABLE `leasing` (
  `id` int(10) UNSIGNED NOT NULL,
  `tenant_id` int(10) UNSIGNED NOT NULL,
  `shops` text COLLATE utf8mb4_unicode_ci,
  `rent_amount` decimal(11,2) NOT NULL DEFAULT '0.00',
  `payment_status` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_method` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pending_amount` decimal(11,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `leasing_enquiries`
--

CREATE TABLE `leasing_enquiries` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `brand` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `companyName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `concept` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stores` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lost_found_items`
--

CREATE TABLE `lost_found_items` (
  `_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` tinyint(3) UNSIGNED DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `image3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `management_email`
--

CREATE TABLE `management_email` (
  `_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `master_brand`
--

CREATE TABLE `master_brand` (
  `brand_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `master_category`
--

CREATE TABLE `master_category` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `master_store`
--

CREATE TABLE `master_store` (
  `store_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `_id` int(10) UNSIGNED NOT NULL,
  `from_user_id` int(10) UNSIGNED NOT NULL,
  `to_user_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `type` char(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'offers',
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_read` tinyint(1) NOT NULL DEFAULT '0',
  `to_read` tinyint(1) NOT NULL DEFAULT '0',
  `from_del` tinyint(1) NOT NULL DEFAULT '0',
  `to_del` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `redirect_to` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect_to_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect_id` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_08_22_083824_create_site_users_table', 1),
(4, '2017_08_24_060336_create_master_category_table', 1),
(5, '2017_08_24_072513_create_master_brand_table', 1),
(6, '2017_08_25_065718_create_master_store_table', 1),
(7, '2017_08_25_074239_create_product_category_table', 1),
(8, '2017_08_25_104951_create_shops_table', 1),
(9, '2017_08_27_163449_add_token_to_site_users', 1),
(10, '2017_08_27_170812_add_columns_to_site_users', 1),
(11, '2017_08_28_112842_create_shop_brands_table', 1),
(12, '2017_08_28_134737_create_product_subcategry_table', 1),
(13, '2017_08_29_081428_create_products_table', 1),
(14, '2017_08_30_090957_drop_columns_for_site_users', 1),
(15, '2017_08_30_091126_add_columns_for_site_users', 1),
(16, '2017_08_31_055901_update_migration_shopbrands', 1),
(17, '2017_08_31_061831_drop_columns_shops', 1),
(18, '2017_08_31_062028_add_columns_shops', 1),
(19, '2017_08_31_062655_drop_columns_products', 1),
(20, '2017_08_31_062728_add_columns_products', 1),
(21, '2017_08_31_063305_create_gallery_albums_table', 1),
(22, '2017_08_31_063353_create_gallery_images_table', 1),
(23, '2017_08_31_120344_add_column_products', 1),
(24, '2017_08_31_133758_drop_column_gender_site_usere', 1),
(25, '2017_08_31_133937_add_column_gender_site_user', 1),
(26, '2017_08_31_134550_change_column_type_site_users', 1),
(27, '2017_09_01_055729_create_events_table', 1),
(28, '2017_09_01_121041_add_column_brand_category', 2),
(29, '2017_09_01_121259_add_column_master_category', 2),
(30, '2017_09_01_121514_add_column_store', 2),
(31, '2017_09_01_095927_create_cms_table', 3),
(32, '2017_09_01_131046_drop_column_category_products', 3),
(33, '2017_09_04_063827_add_column_image_cms', 4),
(34, '2017_09_04_080608_add_column_brand_id_prducts', 4),
(35, '2017_09_05_063345_create_incident_report', 5),
(36, '2017_09_08_103914_create_shop_pics', 6),
(37, '2017_09_11_072419_add_column_type_product_categries', 7),
(38, '2017_09_11_073108_add_column_type_product_subcategries', 7),
(39, '2017_09_11_094746_add_column_shops', 7),
(40, '2017_09_11_112451_add_column_logo_store', 7),
(41, '2017_09_12_064358_add_column_featured_products', 7),
(42, '2017_09_12_073627_create_product_pics_table', 7),
(43, '2017_09_12_084418_create_offers_table', 7),
(44, '2017_09_12_092536_create_experiences_table', 7),
(45, '2017_09_12_113144_add_column_attributes_product', 7),
(46, '2017_09_12_125410_create_site_user_experiences_table', 8),
(47, '2017_09_14_052655_add_column_store_offers', 8),
(48, '2017_09_14_101320_add_column_category_brands', 8),
(49, '2017_09_14_122615_change_phone_nullable_shops', 8),
(50, '2017_09_14_133924_add_column_description_brands', 8),
(51, '2017_09_15_073812_create_table_settings', 9),
(52, '2017_09_16_214456_add_column_profile_mode_site_users', 10),
(53, '2017_09_18_063537_create_event_pics_table', 11),
(54, '2017_09_18_070738_create_favriourite_table', 11),
(55, '2017_09_18_095933_drop_table_settings', 11),
(56, '2017_09_18_100549_create_table_settingsnew', 11),
(57, '2017_09_18_104053_add_column_slug_settings', 11),
(58, '2017_09_18_131925_create_lostfound_table', 11),
(59, '2017_09_20_075810_add_column_sub_title_events', 12),
(60, '2017_09_20_080043_add_column_sub_title_offers', 12),
(61, '2017_09_29_145419_add_column_images_incident', 13),
(62, '2017_11_06_060125_creat_users_cart_table', 14),
(63, '2017_11_06_082730_creat_order_details_table', 14),
(64, '2017_11_06_082739_creat_orders_table', 14),
(65, '2017_11_06_131943_create_movie_detail_table', 15),
(66, '2017_11_07_072358_add_column_upcoming_into_shops', 15),
(67, '2017_11_08_071340_create_user_join_event_table', 16),
(68, '2017_11_08_082531_create_leasing_enquiries_table', 16),
(69, '2017_12_05_105334_add_push_token_into_siteusers', 17),
(70, '2017_12_05_115735_create_messages_table', 18),
(71, '2018_02_07_095706_create_floor_plans_table', 19),
(72, '2018_02_07_115859_add_document_to_floor_plans_table', 19),
(73, '2018_02_22_085522_create_slider_images_table', 19),
(74, '2018_02_22_092146_add_imagePath_to_slider_images_table', 19),
(75, '2018_03_09_201854_add_column_in_site_users', 20),
(76, '2018_03_18_131058_add_column_in_messages_table', 21),
(77, '2018_03_18_135517_add_column_points_in_site_users', 22),
(78, '2018_03_20_020320_add_column_device_in_site_users', 23),
(79, '2018_03_22_140456_create_tenants_table', 24),
(80, '2018_03_22_191440_add_column_tenant_uid_in_shops', 24),
(81, '2018_04_06_201258_add_lat_long_in_site_users', 25),
(82, '2018_04_22_024536_add_beaconId_shops', 26);

-- --------------------------------------------------------

--
-- Table structure for table `movie_detail`
--

CREATE TABLE `movie_detail` (
  `_id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(10) UNSIGNED NOT NULL,
  `subtitle` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `2d_showtime` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `3d_showtime` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cast` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(11,2) NOT NULL,
  `shiping_fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shiping_lname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shiping_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordernote` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `coupon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ' ',
  `shippingCharge` decimal(11,2) NOT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `paypal_payment_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paypal_payer_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`_id`, `user_id`, `firstname`, `lastname`, `email`, `address`, `phone`, `amount`, `shiping_fname`, `shiping_lname`, `shiping_email`, `shipping_address`, `ordernote`, `coupon`, `shippingCharge`, `payment_method`, `created_at`, `updated_at`, `deleted_at`, `paypal_payment_id`, `paypal_payer_id`) VALUES
(1, 1, 'Dev', 'Test', 'nikunj.wxit@gmail.com', '54 Near Apollo, Indore, MP - 451001, India', '', '135.00', '', '', '', ' , ,  - , ', '', ' ', '10.00', 'Cash On Delivery', '2020-09-20 05:16:25', '2020-09-20 05:16:25', NULL, '', ''),
(2, 5, 'Test', 'Dev', 'devtest1@gmail.com', 'test , test, test - tesst, India', '', '55.00', '', '', '', ' , ,  - , ', '', ' ', '0.00', 'Paypal', '2021-01-04 00:49:54', '2021-01-04 01:20:36', NULL, 'PAYID-L7ZNNMY1697134174649954L', '8LKJQ9ZT4PV8N'),
(3, 6, 'Hariom', 'Tyagi', 'sameer6@yopmail.com', '181 Bapat indore , Indore, Madhya Pradesh - 452010, India', '8269514914', '55.00', '', '', '', ' , ,  - , ', 'This is my first order', ' ', '0.00', 'Paypal', '2021-01-21 00:36:10', '2021-01-21 00:36:10', NULL, '', ''),
(4, 6, 'Hariom', 'Tyagi', 'sameer6@yopmail.com', '181 Bapat indore , Indore, Madhya Pradesh - 452010, India', '8269514914', '55.00', '', '', '', ' , ,  - , ', 'This is my first order', ' ', '0.00', 'Paypal', '2021-01-21 00:42:47', '2021-01-21 00:44:25', NULL, 'PAYID-MAET5CQ0NU80206HN6481640', '8LKJQ9ZT4PV8N');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `orderUniqid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_seller_id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_details` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` tinyint(4) NOT NULL,
  `color` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `price` decimal(11,2) NOT NULL,
  `discounted_price` decimal(11,2) NOT NULL,
  `order_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`_id`, `order_id`, `orderUniqid`, `product_id`, `product_seller_id`, `product_name`, `product_details`, `product_image`, `quantity`, `color`, `size`, `price`, `discounted_price`, `order_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'ORD-200920690095626', 3, 0, 'Diamond Exclusive Ornament', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Ipsum metus feugiat sem, quis fermentum turpis eros eget velit. Donec ac tempus ante. Fu', '1591193861_diamond-exclusive-ornament.jpg', 1, 3, 1, '70.00', '0.00', 'pending', '2020-09-20 05:16:25', '2020-09-20 05:16:25', NULL),
(2, 1, 'ORD-2009202095561228', 4, 0, 'Handmade Golden Necklace', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Ipsum metus feugiat sem, quis fermentum turpis eros eget velit. Donec ac tempus ante. Fu', '1591192666_handmade-golden-necklace.jpg', 1, 3, 1, '55.00', '0.00', 'pending', '2020-09-20 05:16:25', '2020-09-20 05:16:25', NULL),
(3, 2, 'ORD-040121559841840', 4, 0, 'Handmade Golden Necklace', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Ipsum metus feugiat sem, quis fermentum turpis eros eget velit. Donec ac tempus ante. Fu', '1591192666_handmade-golden-necklace.jpg', 1, 3, 1, '55.00', '0.00', 'pending', '2021-01-04 00:49:54', '2021-01-04 00:49:54', NULL),
(4, 3, 'ORD-21012163811335', 4, 0, 'Handmade Golden Necklace', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Ipsum metus feugiat sem, quis fermentum turpis eros eget velit. Donec ac tempus ante. Fu', '1591192666_handmade-golden-necklace.jpg', 1, 3, 1, '55.00', '0.00', 'pending', '2021-01-21 00:36:10', '2021-01-21 00:36:10', NULL),
(5, 4, 'ORD-210121905153295', 4, 0, 'Handmade Golden Necklace', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Ipsum metus feugiat sem, quis fermentum turpis eros eget velit. Donec ac tempus ante. Fu', '1591192666_handmade-golden-necklace.jpg', 1, 3, 1, '55.00', '0.00', 'pending', '2021-01-21 00:42:47', '2021-01-21 00:42:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `privilege_customer_offers`
--

CREATE TABLE `privilege_customer_offers` (
  `_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `membership_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Bronze',
  `shop_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_of_visits` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_of_hours` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_status` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `sort_details` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(11,2) NOT NULL DEFAULT '0.00',
  `discounted_price` decimal(11,2) NOT NULL DEFAULT '0.00',
  `brand_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_category_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_subcategory_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_color_id` int(11) UNSIGNED DEFAULT NULL,
  `product_size_id` int(11) UNSIGNED DEFAULT NULL,
  `product_quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `new_arrival` tinyint(1) NOT NULL DEFAULT '0',
  `on_sale` tinyint(1) NOT NULL DEFAULT '0',
  `hot_deal` tinyint(1) NOT NULL DEFAULT '0',
  `best_seller` tinyint(1) NOT NULL DEFAULT '0',
  `add_to_cart` tinyint(1) NOT NULL DEFAULT '0',
  `attribute` text COLLATE utf8mb4_unicode_ci,
  `variations` text COLLATE utf8mb4_unicode_ci,
  `seller_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`_id`, `name`, `slug`, `product_status`, `details`, `sort_details`, `image`, `price`, `discounted_price`, `brand_id`, `product_category_id`, `product_subcategory_id`, `product_color_id`, `product_size_id`, `product_quantity`, `featured`, `new_arrival`, `on_sale`, `hot_deal`, `best_seller`, `add_to_cart`, `attribute`, `variations`, `seller_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Perfect Diamond Jewelry', 'perfect-diamond-jewelry', 'Draft', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Ipsum metus feugiat sem, quis fermentum turpis eros eget velit. Donec ac tempus ante. Fusce ultricies massa massa. Fusce aliquam, purus eget sagittis vulputate, sapien libero hendrerit est, sed commodo augue nisi non neque.Cras neque metus, consequat et blandit et, luctus a nunc. Etiam gravida vehicula tellus, in imperdiet ligula euismod eget. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam erat mi, rutrum at sollicitudin rhoncus', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Phasellus id nisi quis justo tempus mollis sed et dui. In hac habitasse platea dictumst.', '1590990052_handmade-golden-necklace-full-family-package.jpg', '90.00', '72.00', 2, 36, 34, 2, 1, 194, 1, 1, 1, 1, 1, 1, '[{\"name\":\"color\",\"value\":\"black, blue, red\"},{\"name\":\"size\",\"value\":\"L, M, S\"}]', '[{\"color\":\"2\",\"size\":\"3\",\"price\":\"90.00\",\"discountedPrice\":\"70.00\",\"quantity\":\"200\",\"images\":\"1590990053_handmade-golden-necklace-full-family-package_0.jpg\"},{\"color\":\"3\",\"size\":\"2\",\"price\":\"90.00\",\"discountedPrice\":\"70.00\",\"quantity\":\"200\",\"images\":\"1590990053_handmade-golden-necklace-full-family-package_1.jpg\"},{\"color\":\"1\",\"size\":\"4\",\"price\":\"90.00\",\"discountedPrice\":\"70.00\",\"quantity\":\"200\",\"images\":\"1590990053_handmade-golden-necklace-full-family-package_2.jpg\"}]', NULL, 7, 7, '2020-06-01 00:10:53', '2020-09-20 05:11:07', NULL),
(2, 'Citygold Exclusive Ring', 'citygold-exclusive-ring', 'Published', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Ipsum metus feugiat sem, quis fermentum turpis eros eget velit. Donec ac tempus ante. Fusce ultricies massa massa. Fusce aliquam, purus eget sagittis vulputate, sapien libero hendrerit est, sed commodo augue nisi non neque.Cras neque metus, consequat et blandit et, luctus a nunc. Etiam gravida vehicula tellus, in imperdiet ligula euismod eget. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam erat mi, rutrum at sollicitudin rhoncus', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Phasellus id nisi quis justo tempus mollis sed et dui. In hac habitasse platea dictumst.', '1591183338_citygold-exclusive-ring.jpg', '70.00', '0.00', 5, 58, 35, 1, 3, 100, 1, 1, 1, 1, 1, 1, '[{\"name\":\"color\",\"value\":\"black, blue, red\"},{\"name\":\"size\",\"value\":\"L, M, S\"}]', '[]', NULL, 7, 7, '2020-06-03 05:52:19', '2020-07-08 12:49:58', NULL),
(3, 'Diamond Exclusive Ornament', 'diamond-exclusive-ornament', 'Draft', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Ipsum metus feugiat sem, quis fermentum turpis eros eget velit. Donec ac tempus ante. Fusce ultricies massa massa. Fusce aliquam, purus eget sagittis vulputate, sapien libero hendrerit est, sed commodo augue nisi non neque.Cras neque metus, consequat et blandit et, luctus a nunc. Etiam gravida vehicula tellus, in imperdiet ligula euismod eget. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam erat mi, rutrum at sollicitudin rhoncus', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Phasellus id nisi quis justo tempus mollis sed et dui. In hac habitasse platea dictumst.', '1591193861_diamond-exclusive-ornament.jpg', '110.00', '70.00', 1, 36, 36, 3, 1, 16, 1, 1, 1, 1, 1, 1, '[{\"name\":\"color\",\"value\":\"black, blue, red\"},{\"name\":\"size\",\"value\":\"L, M, S\"}]', '[{\"color\":\"2\",\"size\":\"4\",\"price\":\"75.00\",\"discountedPrice\":\"55.00\",\"quantity\":\"40\",\"images\":\"1591184016_diamond-exclusive-ornament_0.jpg\"}]', NULL, 7, 7, '2020-06-03 06:03:36', '2020-09-20 05:16:25', NULL),
(4, 'Handmade Golden Necklace', 'handmade-golden-necklace', 'Draft', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Ipsum metus feugiat sem, quis fermentum turpis eros eget velit. Donec ac tempus ante. Fusce ultricies massa massa. Fusce aliquam, purus eget sagittis vulputate, sapien libero hendrerit est, sed commodo augue nisi non neque.Cras neque metus, consequat et blandit et, luctus a nunc. Etiam gravida vehicula tellus, in imperdiet ligula euismod eget. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam erat mi, rutrum at sollicitudin rhoncus', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Phasellus id nisi quis justo tempus mollis sed et dui. In hac habitasse platea dictumst.', '1591192666_handmade-golden-necklace.jpg', '70.00', '55.00', 1, 36, 32, 3, 1, 9, 1, 1, 1, 1, 1, 1, '[{\"name\":\"color\",\"value\":\"black, blue, red\"},{\"name\":\"size\",\"value\":\"L, M, S\"}]', '[{\"color\":\"2\",\"size\":\"3\",\"price\":\"75.00\",\"discountedPrice\":\"55.00\",\"quantity\":\"40\",\"images\":\"1591184016_diamond-exclusive-ornament_0.jpg\"}]', NULL, 7, 7, '2020-06-03 06:03:36', '2021-01-21 00:42:47', NULL),
(5, 'Exclusive silver top bracellet', 'exclusive-silver-top-bracellet', 'Draft', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Ipsum metus feugiat sem, quis fermentum turpis eros eget velit. Donec ac tempus ante. Fusce ultricies massa massa. Fusce aliquam, purus eget sagittis vulputate, sapien libero hendrerit est, sed commodo augue nisi non neque.Cras neque metus, consequat et blandit et, luctus a nunc. Etiam gravida vehicula tellus, in imperdiet ligula euismod eget. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam erat mi, rutrum at sollicitudin rhoncus', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Phasellus id nisi quis justo tempus mollis sed et dui. In hac habitasse platea dictumst.', '1591192772_exclusive-silver-top-bracellet.jpg', '65.00', '50.00', 1, 36, 35, 3, 1, 0, 1, 1, 1, 1, 1, 1, '[{\"name\":\"color\",\"value\":\"black, blue, red\"},{\"name\":\"size\",\"value\":\"L, M, S\"}]', '[{\"color\":\"2\",\"size\":\"2\",\"price\":\"75.00\",\"discountedPrice\":\"55.00\",\"quantity\":\"40\",\"images\":\"1591184016_diamond-exclusive-ornament_0.jpg\"}]', NULL, 7, 7, '2020-06-03 06:03:36', '2020-09-19 07:01:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_brand`
--

CREATE TABLE `product_brand` (
  `_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `updated_by` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_brand`
--

INSERT INTO `product_brand` (`_id`, `name`, `slug`, `image`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'NIKE', 'nike', '1591172999_nike.png', 7, 7, '2020-05-17 04:46:04', '2020-06-03 02:59:59', NULL),
(2, 'ADIDAS', 'adidas', '1591173012_adidas.png', 7, 7, '2020-05-17 04:46:18', '2020-06-03 03:00:12', NULL),
(3, 'PUMA', 'puma', '1591173024_puma.png', 7, 7, '2020-05-17 04:46:34', '2020-06-03 03:00:24', NULL),
(4, 'REEBOK', 'reebok', '1591173035_reebok.png', 7, 7, '2020-05-17 04:46:48', '2020-06-03 03:00:35', NULL),
(5, 'Apple', 'apple', '1591173049_new-balance.png', 7, 7, '2020-05-17 04:47:12', '2020-06-03 03:04:08', NULL),
(12, 'ASUS', 'asus', '1591173158_asus.png', 7, 7, '2020-06-03 03:02:38', '2020-06-03 03:04:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`_id`, `name`, `slug`, `type`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(36, 'Jewellery', 'jewellery', 'Product', 4, 7, '2018-04-09 06:43:43', '2020-06-21 11:32:31', NULL),
(58, 'Platinum', 'platinum', 'Product', 4, 7, '2020-06-24 12:53:41', '2020-06-24 12:54:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_color`
--

CREATE TABLE `product_color` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `color_code` varchar(255) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `updated_by` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_color`
--

INSERT INTO `product_color` (`id`, `name`, `slug`, `color_code`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Grey', '', '#808080', 7, 7, '2020-05-15 10:22:17', '2020-05-17 03:41:03', NULL),
(2, 'LightSteelblue', '', '#b0c4de', 7, 0, '2020-05-17 03:43:03', '2020-05-17 03:43:03', NULL),
(3, 'Darktan', '', '#aa9e78', 7, 0, '2020-05-17 03:43:39', '2020-05-17 03:43:39', NULL),
(4, 'Brown', '', '#964B00', 7, 0, '2020-05-17 03:43:59', '2020-05-17 03:43:59', NULL),
(5, 'Black', '', '#000000', 7, 0, '2020-12-23 09:00:04', '2020-12-23 09:00:04', NULL),
(6, 'Yellow', '', '#eee617', 7, 0, '2020-12-28 02:30:11', '2020-12-28 02:30:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_pics`
--

CREATE TABLE `product_pics` (
  `_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_pics`
--

INSERT INTO `product_pics` (`_id`, `product_id`, `image`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(42, 187, '1553600063_1.jpg', 4, NULL, '2019-03-26 11:34:23', '2019-03-26 11:34:23'),
(43, 187, '1553600063_2.jpg', 4, NULL, '2019-03-26 11:34:23', '2019-03-26 11:34:23'),
(44, 187, '1553600063_3.jpg', 4, NULL, '2019-03-26 11:34:23', '2019-03-26 11:34:23'),
(45, 188, '1553600667_1.jpg', 4, NULL, '2019-03-26 11:44:27', '2019-03-26 11:44:27'),
(46, 188, '1553600667_2.jpg', 4, NULL, '2019-03-26 11:44:27', '2019-03-26 11:44:27'),
(47, 188, '1553600667_3.jpg', 4, NULL, '2019-03-26 11:44:27', '2019-03-26 11:44:27'),
(48, 1, '1590990129_1.jpg', 7, NULL, '2020-06-01 00:12:09', '2020-06-01 00:12:09'),
(49, 1, '1590990129_2.jpg', 7, NULL, '2020-06-01 00:12:09', '2020-06-01 00:12:09'),
(50, 1, '1590990129_3.jpg', 7, NULL, '2020-06-01 00:12:09', '2020-06-01 00:12:09'),
(51, 1, '1590990129_4.jpg', 7, NULL, '2020-06-01 00:12:09', '2020-06-01 00:12:09'),
(52, 1, '1590990129_5.jpg', 7, NULL, '2020-06-01 00:12:09', '2020-06-01 00:12:09'),
(54, 2, '1591184047_1.jpg', 7, NULL, '2020-06-03 06:04:07', '2020-06-03 06:04:07'),
(55, 2, '1591184047_2.jpg', 7, NULL, '2020-06-03 06:04:07', '2020-06-03 06:04:07'),
(56, 2, '1591184047_3.jpg', 7, NULL, '2020-06-03 06:04:07', '2020-06-03 06:04:07'),
(57, 2, '1591184047_4.jpg', 7, NULL, '2020-06-03 06:04:07', '2020-06-03 06:04:07'),
(58, 2, '1591184047_5.jpg', 7, NULL, '2020-06-03 06:04:07', '2020-06-03 06:04:07'),
(59, 2, '1591184047_6.jpg', 7, NULL, '2020-06-03 06:04:07', '2020-06-03 06:04:07'),
(60, 3, '1591184062_1.jpg', 7, NULL, '2020-06-03 06:04:22', '2020-06-03 06:04:22'),
(61, 3, '1591184062_2.jpg', 7, NULL, '2020-06-03 06:04:22', '2020-06-03 06:04:22'),
(62, 3, '1591184062_3.jpg', 7, NULL, '2020-06-03 06:04:22', '2020-06-03 06:04:22'),
(63, 3, '1591184062_4.jpg', 7, NULL, '2020-06-03 06:04:22', '2020-06-03 06:04:22'),
(64, 3, '1591184062_5.jpg', 7, NULL, '2020-06-03 06:04:22', '2020-06-03 06:04:22'),
(65, 3, '1591184062_6.jpg', 7, NULL, '2020-06-03 06:04:22', '2020-06-03 06:04:22');

-- --------------------------------------------------------

--
-- Table structure for table `product_size`
--

CREATE TABLE `product_size` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `updated_by` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_size`
--

INSERT INTO `product_size` (`id`, `name`, `slug`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'S', 's', 7, 7, '2020-05-15 10:29:08', '2020-06-21 12:51:52', NULL),
(2, 'M', 'm', 7, 7, '2020-05-17 03:45:28', '2020-06-21 12:51:55', NULL),
(3, 'L', 'l', 7, 7, '2020-05-17 03:45:32', '2020-06-21 12:51:56', NULL),
(4, 'XL', 'xl', 7, 7, '2020-05-17 03:45:39', '2020-06-21 12:51:58', NULL),
(5, 'XXL', 'xxl', 7, 0, '2020-12-28 02:28:52', '2020-12-28 02:28:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_subcategories`
--

CREATE TABLE `product_subcategories` (
  `_id` int(10) UNSIGNED NOT NULL,
  `product_category_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_subcategories`
--

INSERT INTO `product_subcategories` (`_id`, `product_category_id`, `name`, `slug`, `type`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(28, 7, 'Clothes', '', 'Product', 7, 7, '2020-05-13 03:30:23', '2020-05-15 00:08:49', NULL),
(29, 8, 'Watches', '', 'Product', 7, 7, '2020-05-13 03:31:26', '2020-05-15 00:09:05', NULL),
(31, 8, 'Clothes', 'clothes', 'Product', NULL, NULL, '2020-05-20 09:25:08', '2020-05-20 09:25:08', NULL),
(32, 8, 'Jewellery', 'jewellery', 'Product', 7, NULL, '2020-05-20 09:32:21', '2020-05-20 09:32:21', NULL),
(33, 58, 'Necklaces', 'necklaces', 'Product', 7, 7, '2020-05-25 07:00:59', '2020-07-07 12:33:37', NULL),
(34, 36, 'Diamond', 'diamond', 'Product', 7, NULL, '2020-05-25 07:02:08', '2020-05-25 07:02:08', NULL),
(35, 36, 'Rings', 'rings', 'Product', 7, NULL, '2020-05-25 07:02:43', '2020-05-25 07:02:43', NULL),
(36, 36, 'Earring', 'earring', 'Product', 7, NULL, '2020-05-25 07:02:59', '2020-05-25 07:02:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `_id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `message` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_url` text COLLATE utf8mb4_unicode_ci,
  `twitter_url` text COLLATE utf8mb4_unicode_ci,
  `instagram_url` text COLLATE utf8mb4_unicode_ci,
  `youtube_url` text COLLATE utf8mb4_unicode_ci,
  `linkedin_url` text COLLATE utf8mb4_unicode_ci,
  `pinterest_url` text COLLATE utf8mb4_unicode_ci,
  `shipping` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`_id`, `title`, `message`, `address`, `email`, `phone`, `facebook_url`, `twitter_url`, `instagram_url`, `youtube_url`, `linkedin_url`, `pinterest_url`, `shipping`, `image`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Welcome to Glamour Jewellery online store', 'We are a team of designers and developers that create high quality wordpress, shopify, Opencart', '4710-4890 Breckinridge USA', 'demo@yourdomain.com', '(012) 800 456 789-987', 'https://www.facebook.com/', 'https://twitter.com/', 'https://www.instagram.com/', 'https://www.youtube.com/', 'https://in.linkedin.com/', NULL, 1, '1609750061_.png', NULL, 7, '2017-09-18 17:11:22', '2021-01-04 00:47:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `_id` int(10) UNSIGNED NOT NULL,
  `beacon_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tenant_uid` varchar(101) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `store_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `logo` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_category_id` int(10) UNSIGNED DEFAULT NULL,
  `store_subcategory_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `floorlevel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lease_year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upcoming` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shop_brands`
--

CREATE TABLE `shop_brands` (
  `_id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `brand_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shop_brands`
--

INSERT INTO `shop_brands` (`_id`, `shop_id`, `brand_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(55, 3, 20, 3, NULL, '2017-09-15 09:40:35', '2017-09-15 09:40:35'),
(56, 9, 20, 3, NULL, '2017-09-15 09:41:45', '2017-09-15 09:41:45'),
(57, 14, 20, 3, NULL, '2017-09-15 09:42:44', '2017-09-15 09:42:44'),
(58, 15, 21, 3, NULL, '2017-09-15 09:44:04', '2017-09-15 09:44:04'),
(59, 16, 21, 3, NULL, '2017-09-15 09:44:53', '2017-09-15 09:44:53'),
(60, 17, 21, 3, NULL, '2017-09-15 09:45:40', '2017-09-15 09:45:40'),
(61, 18, 21, 3, NULL, '2017-09-15 09:46:47', '2017-09-15 09:46:47'),
(62, 19, 22, 3, NULL, '2017-09-15 09:47:47', '2017-09-15 09:47:47'),
(63, 20, 22, 3, NULL, '2017-09-15 09:48:23', '2017-09-15 09:48:23'),
(64, 21, 22, 3, NULL, '2017-09-15 09:49:54', '2017-09-15 09:49:54'),
(86, 12, 25, 3, NULL, '2017-09-25 11:01:51', '2017-09-25 11:01:51'),
(87, 13, 25, 3, NULL, '2017-09-25 11:02:08', '2017-09-25 11:02:08'),
(179, 33, 26, 3, NULL, '2017-11-15 12:25:09', '2017-11-15 12:25:09'),
(180, 32, 27, 3, NULL, '2017-11-15 12:25:49', '2017-11-15 12:25:49'),
(398, 157, 167, 4, NULL, '2017-12-09 06:40:11', '2017-12-09 06:40:11'),
(471, 124, 167, 4, NULL, '2017-12-09 08:15:52', '2017-12-09 08:15:52'),
(486, 147, 167, 4, NULL, '2017-12-09 08:32:43', '2017-12-09 08:32:43'),
(526, 61, 29, 4, NULL, '2017-12-09 10:16:25', '2017-12-09 10:16:25'),
(527, 62, 29, 4, NULL, '2017-12-09 10:16:50', '2017-12-09 10:16:50'),
(608, 11, 25, 3, NULL, '2018-01-03 12:56:07', '2018-01-03 12:56:07'),
(1016, 252, 198, 4, NULL, '2018-02-11 07:02:38', '2018-02-11 07:02:38'),
(1029, 207, 198, 4, NULL, '2018-02-11 11:56:25', '2018-02-11 11:56:25'),
(1030, 239, 198, 4, NULL, '2018-02-11 11:56:49', '2018-02-11 11:56:49'),
(1365, 322, 205, 4, NULL, '2018-03-05 09:10:19', '2018-03-05 09:10:19'),
(2112, 429, 25, 4, NULL, '2018-10-12 14:47:49', '2018-10-12 14:47:49'),
(2114, 431, 25, 4, NULL, '2018-10-12 14:49:38', '2018-10-12 14:49:38'),
(2115, 430, 25, 4, NULL, '2018-10-13 05:46:35', '2018-10-13 05:46:35'),
(3029, 391, 263, 4, NULL, '2019-03-04 07:29:07', '2019-03-04 07:29:07'),
(3797, 458, 26, 4, NULL, '2019-03-08 09:35:38', '2019-03-08 09:35:38'),
(3827, 446, 263, 4, NULL, '2019-03-10 05:44:46', '2019-03-10 05:44:46'),
(3989, 457, 263, 4, NULL, '2019-03-23 08:19:27', '2019-03-23 08:19:27'),
(4132, 445, 263, 4, NULL, '2019-03-28 07:52:15', '2019-03-28 07:52:15'),
(4502, 444, 263, 4, NULL, '2019-04-17 13:04:23', '2019-04-17 13:04:23'),
(4770, 216, 263, 4, NULL, '2019-07-20 07:22:53', '2019-07-20 07:22:53'),
(4843, 245, 263, 4, NULL, '2019-07-20 07:35:32', '2019-07-20 07:35:32'),
(4857, 274, 263, 4, NULL, '2019-07-20 08:21:22', '2019-07-20 08:21:22'),
(5119, 369, 263, 4, NULL, '2019-07-27 06:08:19', '2019-07-27 06:08:19'),
(5121, 305, 263, 4, NULL, '2019-07-27 06:21:55', '2019-07-27 06:21:55'),
(5142, 193, 263, 4, NULL, '2019-07-27 06:23:14', '2019-07-27 06:23:14'),
(5147, 385, 264, 4, NULL, '2019-07-27 06:45:34', '2019-07-27 06:45:34'),
(5148, 442, 262, 4, NULL, '2019-07-27 06:46:04', '2019-07-27 06:46:04'),
(5149, 442, 389, 4, NULL, '2019-07-27 06:46:04', '2019-07-27 06:46:04'),
(5150, 442, 391, 4, NULL, '2019-07-27 06:46:04', '2019-07-27 06:46:04'),
(5151, 442, 390, 4, NULL, '2019-07-27 06:46:04', '2019-07-27 06:46:04'),
(5152, 442, 237, 4, NULL, '2019-07-27 06:46:04', '2019-07-27 06:46:04'),
(5153, 442, 311, 4, NULL, '2019-07-27 06:46:04', '2019-07-27 06:46:04'),
(5154, 442, 256, 4, NULL, '2019-07-27 06:46:04', '2019-07-27 06:46:04'),
(5155, 442, 251, 4, NULL, '2019-07-27 06:46:04', '2019-07-27 06:46:04'),
(5156, 442, 392, 4, NULL, '2019-07-27 06:46:04', '2019-07-27 06:46:04'),
(5157, 442, 393, 4, NULL, '2019-07-27 06:46:04', '2019-07-27 06:46:04'),
(5158, 442, 252, 4, NULL, '2019-07-27 06:46:04', '2019-07-27 06:46:04'),
(5159, 442, 259, 4, NULL, '2019-07-27 06:46:04', '2019-07-27 06:46:04'),
(5160, 442, 257, 4, NULL, '2019-07-27 06:46:04', '2019-07-27 06:46:04'),
(5161, 442, 388, 4, NULL, '2019-07-27 06:46:04', '2019-07-27 06:46:04'),
(5162, 442, 247, 4, NULL, '2019-07-27 06:46:04', '2019-07-27 06:46:04'),
(5163, 442, 258, 4, NULL, '2019-07-27 06:46:04', '2019-07-27 06:46:04'),
(5164, 370, 263, 4, NULL, '2019-07-27 06:46:28', '2019-07-27 06:46:28'),
(5166, 205, 263, 4, NULL, '2019-07-27 06:47:17', '2019-07-27 06:47:17'),
(5167, 448, 255, 4, NULL, '2019-07-27 06:47:45', '2019-07-27 06:47:45'),
(5168, 448, 235, 4, NULL, '2019-07-27 06:47:45', '2019-07-27 06:47:45'),
(5169, 448, 251, 4, NULL, '2019-07-27 06:47:45', '2019-07-27 06:47:45'),
(5170, 448, 263, 4, NULL, '2019-07-27 06:47:45', '2019-07-27 06:47:45'),
(5171, 448, 242, 4, NULL, '2019-07-27 06:47:45', '2019-07-27 06:47:45'),
(5172, 448, 307, 4, NULL, '2019-07-27 06:47:45', '2019-07-27 06:47:45'),
(5173, 448, 248, 4, NULL, '2019-07-27 06:47:45', '2019-07-27 06:47:45'),
(5174, 448, 249, 4, NULL, '2019-07-27 06:47:45', '2019-07-27 06:47:45'),
(5175, 448, 308, 4, NULL, '2019-07-27 06:47:45', '2019-07-27 06:47:45'),
(5176, 263, 263, 4, NULL, '2019-07-27 06:48:31', '2019-07-27 06:48:31'),
(5177, 271, 263, 4, NULL, '2019-07-27 06:49:02', '2019-07-27 06:49:02'),
(5178, 452, 263, 4, NULL, '2019-07-27 06:49:26', '2019-07-27 06:49:26'),
(5179, 210, 263, 4, NULL, '2019-07-27 06:57:23', '2019-07-27 06:57:23'),
(5180, 443, 233, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5181, 443, 234, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5182, 443, 235, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5183, 443, 236, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5184, 443, 237, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5185, 443, 238, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5186, 443, 240, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5187, 443, 251, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5188, 443, 241, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5189, 443, 242, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5190, 443, 244, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5191, 443, 245, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5192, 443, 246, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5193, 443, 252, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5194, 443, 247, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5195, 443, 258, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5196, 443, 248, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5197, 443, 249, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5198, 443, 254, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5199, 443, 250, 4, NULL, '2019-07-27 07:00:42', '2019-07-27 07:00:42'),
(5200, 317, 263, 4, NULL, '2019-07-27 07:01:13', '2019-07-27 07:01:13'),
(5201, 392, 367, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5202, 392, 283, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5203, 392, 361, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5204, 392, 373, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5205, 392, 362, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5206, 392, 372, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5207, 392, 264, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5208, 392, 370, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5209, 392, 375, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5210, 392, 377, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5211, 392, 378, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5212, 392, 365, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5213, 392, 366, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5214, 392, 371, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5215, 392, 379, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5216, 392, 368, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5217, 392, 369, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5218, 392, 364, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5219, 392, 376, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5220, 392, 374, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5221, 392, 363, 4, NULL, '2019-07-27 07:01:36', '2019-07-27 07:01:36'),
(5223, 222, 263, 4, NULL, '2019-07-27 07:02:23', '2019-07-27 07:02:23'),
(5224, 227, 267, 4, NULL, '2019-07-27 07:02:45', '2019-07-27 07:02:45'),
(5225, 227, 238, 4, NULL, '2019-07-27 07:02:45', '2019-07-27 07:02:45'),
(5226, 227, 241, 4, NULL, '2019-07-27 07:02:45', '2019-07-27 07:02:45'),
(5227, 227, 263, 4, NULL, '2019-07-27 07:02:45', '2019-07-27 07:02:45'),
(5228, 227, 242, 4, NULL, '2019-07-27 07:02:45', '2019-07-27 07:02:45'),
(5229, 227, 244, 4, NULL, '2019-07-27 07:02:45', '2019-07-27 07:02:45'),
(5230, 227, 245, 4, NULL, '2019-07-27 07:02:45', '2019-07-27 07:02:45'),
(5231, 227, 246, 4, NULL, '2019-07-27 07:02:45', '2019-07-27 07:02:45'),
(5232, 227, 252, 4, NULL, '2019-07-27 07:02:45', '2019-07-27 07:02:45'),
(5233, 227, 258, 4, NULL, '2019-07-27 07:02:45', '2019-07-27 07:02:45'),
(5234, 279, 263, 4, NULL, '2019-07-27 07:05:15', '2019-07-27 07:05:15'),
(5235, 372, 381, 4, NULL, '2019-07-27 07:05:37', '2019-07-27 07:05:37'),
(5236, 372, 383, 4, NULL, '2019-07-27 07:05:37', '2019-07-27 07:05:37'),
(5237, 372, 380, 4, NULL, '2019-07-27 07:05:37', '2019-07-27 07:05:37'),
(5238, 372, 382, 4, NULL, '2019-07-27 07:05:37', '2019-07-27 07:05:37'),
(5239, 372, 385, 4, NULL, '2019-07-27 07:05:37', '2019-07-27 07:05:37'),
(5240, 372, 384, 4, NULL, '2019-07-27 07:05:37', '2019-07-27 07:05:37'),
(5241, 372, 387, 4, NULL, '2019-07-27 07:05:37', '2019-07-27 07:05:37'),
(5242, 372, 386, 4, NULL, '2019-07-27 07:05:37', '2019-07-27 07:05:37'),
(5243, 244, 263, 4, NULL, '2019-07-27 07:06:00', '2019-07-27 07:06:00'),
(5247, 173, 28, 4, NULL, '2019-07-27 07:11:13', '2019-07-27 07:11:13'),
(5248, 437, 23, 4, NULL, '2019-07-27 07:11:40', '2019-07-27 07:11:40'),
(5249, 78, 23, 4, NULL, '2019-07-27 07:11:59', '2019-07-27 07:11:59'),
(5250, 77, 23, 4, NULL, '2019-07-27 07:12:49', '2019-07-27 07:12:49'),
(5251, 69, 23, 4, NULL, '2019-07-27 07:13:08', '2019-07-27 07:13:08'),
(5252, 73, 23, 4, NULL, '2019-07-27 07:13:31', '2019-07-27 07:13:31'),
(5253, 481, 28, 4, NULL, '2019-07-27 07:13:58', '2019-07-27 07:13:58'),
(5254, 71, 23, 4, NULL, '2019-07-27 07:15:00', '2019-07-27 07:15:00'),
(5256, 198, 263, 4, NULL, '2019-07-27 07:19:54', '2019-07-27 07:19:54'),
(5257, 51, 29, 4, NULL, '2019-07-27 07:20:31', '2019-07-27 07:20:31'),
(5259, 493, 263, 4, NULL, '2019-07-27 07:23:22', '2019-07-27 07:23:22'),
(5260, 316, 263, 4, NULL, '2019-07-27 07:32:31', '2019-07-27 07:32:31'),
(5261, 214, 263, 4, NULL, '2019-07-27 07:32:57', '2019-07-27 07:32:57'),
(5262, 267, 263, 4, NULL, '2019-07-27 07:33:29', '2019-07-27 07:33:29'),
(5284, 76, 23, 4, NULL, '2019-07-27 08:17:32', '2019-07-27 08:17:32'),
(5298, 455, 28, 4, NULL, '2019-07-27 08:30:09', '2019-07-27 08:30:09'),
(5302, 473, 29, 4, NULL, '2019-07-27 08:32:57', '2019-07-27 08:32:57'),
(5306, 261, 263, 4, NULL, '2019-07-27 10:31:06', '2019-07-27 10:31:06'),
(5307, 269, 263, 4, NULL, '2019-07-27 10:33:41', '2019-07-27 10:33:41'),
(5308, 186, 29, 4, NULL, '2019-07-27 10:39:09', '2019-07-27 10:39:09'),
(5309, 282, 29, 4, NULL, '2019-07-27 10:41:58', '2019-07-27 10:41:58'),
(5310, 192, 29, 4, NULL, '2019-07-27 10:42:51', '2019-07-27 10:42:51'),
(5312, 406, 29, 4, NULL, '2019-07-27 10:48:44', '2019-07-27 10:48:44'),
(5313, 63, 29, 4, NULL, '2019-07-27 10:49:09', '2019-07-27 10:49:09'),
(5314, 167, 28, 4, NULL, '2019-07-27 10:49:45', '2019-07-27 10:49:45'),
(5315, 434, 29, 4, NULL, '2019-07-27 10:50:24', '2019-07-27 10:50:24'),
(5317, 64, 29, 4, NULL, '2019-07-27 11:02:25', '2019-07-27 11:02:25'),
(5318, 382, 29, 4, NULL, '2019-07-27 11:02:44', '2019-07-27 11:02:44'),
(5319, 262, 29, 4, NULL, '2019-07-27 11:03:03', '2019-07-27 11:03:03'),
(5320, 401, 263, 4, NULL, '2019-07-27 11:04:11', '2019-07-27 11:04:11'),
(5321, 461, 29, 4, NULL, '2019-07-27 11:06:21', '2019-07-27 11:06:21'),
(5322, 265, 263, 4, NULL, '2019-07-27 11:06:55', '2019-07-27 11:06:55'),
(5323, 53, 29, 4, NULL, '2019-07-27 12:17:19', '2019-07-27 12:17:19'),
(5324, 188, 29, 4, NULL, '2019-07-27 12:24:01', '2019-07-27 12:24:01'),
(5325, 60, 29, 4, NULL, '2019-07-27 12:24:43', '2019-07-27 12:24:43'),
(5327, 24, 28, 4, NULL, '2019-07-27 12:30:38', '2019-07-27 12:30:38'),
(5329, 34, 28, 4, NULL, '2019-07-27 12:32:12', '2019-07-27 12:32:12'),
(5330, 456, 28, 4, NULL, '2019-07-27 12:32:32', '2019-07-27 12:32:32'),
(5331, 399, 28, 4, NULL, '2019-07-27 12:32:52', '2019-07-27 12:32:52'),
(5334, 253, 29, 4, NULL, '2019-07-27 12:33:53', '2019-07-27 12:33:53'),
(5335, 25, 28, 4, NULL, '2019-07-27 12:34:15', '2019-07-27 12:34:15'),
(5337, 56, 29, 4, NULL, '2019-07-27 12:36:00', '2019-07-27 12:36:00'),
(5338, 47, 28, 4, NULL, '2019-07-27 12:44:41', '2019-07-27 12:44:41'),
(5339, 65, 30, 4, NULL, '2019-07-27 12:45:19', '2019-07-27 12:45:19'),
(5340, 187, 28, 4, NULL, '2019-07-27 12:45:45', '2019-07-27 12:45:45'),
(5341, 489, 28, 4, NULL, '2019-07-27 12:47:01', '2019-07-27 12:47:01'),
(5342, 202, 263, 4, NULL, '2019-07-27 12:47:51', '2019-07-27 12:47:51'),
(5343, 36, 28, 4, NULL, '2019-07-27 12:48:21', '2019-07-27 12:48:21'),
(5345, 223, 28, 4, NULL, '2019-07-27 12:51:02', '2019-07-27 12:51:02'),
(5346, 35, 28, 4, NULL, '2019-07-27 12:51:24', '2019-07-27 12:51:24'),
(5347, 37, 28, 4, NULL, '2019-07-27 12:51:44', '2019-07-27 12:51:44'),
(5348, 433, 263, 4, NULL, '2019-07-27 12:52:14', '2019-07-27 12:52:14'),
(5350, 221, 279, 4, NULL, '2019-07-27 13:30:09', '2019-07-27 13:30:09'),
(5351, 221, 278, 4, NULL, '2019-07-27 13:30:09', '2019-07-27 13:30:09'),
(5352, 221, 210, 4, NULL, '2019-07-27 13:30:09', '2019-07-27 13:30:09'),
(5353, 221, 272, 4, NULL, '2019-07-27 13:30:09', '2019-07-27 13:30:09'),
(5354, 221, 271, 4, NULL, '2019-07-27 13:30:09', '2019-07-27 13:30:09'),
(5355, 221, 263, 4, NULL, '2019-07-27 13:30:09', '2019-07-27 13:30:09'),
(5356, 221, 222, 4, NULL, '2019-07-27 13:30:09', '2019-07-27 13:30:09'),
(5357, 221, 280, 4, NULL, '2019-07-27 13:30:09', '2019-07-27 13:30:09'),
(5358, 221, 270, 4, NULL, '2019-07-27 13:30:09', '2019-07-27 13:30:09'),
(5361, 231, 210, 4, NULL, '2019-07-27 13:30:52', '2019-07-27 13:30:52'),
(5362, 231, 263, 4, NULL, '2019-07-27 13:30:52', '2019-07-27 13:30:52'),
(5363, 236, 269, 4, NULL, '2019-07-27 13:31:13', '2019-07-27 13:31:13'),
(5364, 236, 272, 4, NULL, '2019-07-27 13:31:13', '2019-07-27 13:31:13'),
(5365, 236, 276, 4, NULL, '2019-07-27 13:31:13', '2019-07-27 13:31:13'),
(5366, 236, 277, 4, NULL, '2019-07-27 13:31:13', '2019-07-27 13:31:13'),
(5367, 236, 263, 4, NULL, '2019-07-27 13:31:13', '2019-07-27 13:31:13'),
(5368, 236, 270, 4, NULL, '2019-07-27 13:31:13', '2019-07-27 13:31:13'),
(5369, 237, 269, 4, NULL, '2019-07-27 13:31:36', '2019-07-27 13:31:36'),
(5370, 237, 272, 4, NULL, '2019-07-27 13:31:36', '2019-07-27 13:31:36'),
(5371, 237, 271, 4, NULL, '2019-07-27 13:31:36', '2019-07-27 13:31:36'),
(5372, 237, 263, 4, NULL, '2019-07-27 13:31:36', '2019-07-27 13:31:36'),
(5373, 237, 270, 4, NULL, '2019-07-27 13:31:36', '2019-07-27 13:31:36'),
(5374, 237, 306, 4, NULL, '2019-07-27 13:31:36', '2019-07-27 13:31:36'),
(5379, 283, 263, 4, NULL, '2019-07-27 13:42:03', '2019-07-27 13:42:03'),
(5381, 383, 263, 4, NULL, '2019-07-27 13:44:01', '2019-07-27 13:44:01'),
(5382, 284, 285, 4, NULL, '2019-07-27 13:44:22', '2019-07-27 13:44:22'),
(5383, 284, 286, 4, NULL, '2019-07-27 13:44:22', '2019-07-27 13:44:22'),
(5384, 284, 283, 4, NULL, '2019-07-27 13:44:22', '2019-07-27 13:44:22'),
(5385, 284, 264, 4, NULL, '2019-07-27 13:44:22', '2019-07-27 13:44:22'),
(5386, 284, 281, 4, NULL, '2019-07-27 13:44:22', '2019-07-27 13:44:22'),
(5387, 284, 284, 4, NULL, '2019-07-27 13:44:22', '2019-07-27 13:44:22'),
(5388, 284, 263, 4, NULL, '2019-07-27 13:44:22', '2019-07-27 13:44:22'),
(5389, 284, 282, 4, NULL, '2019-07-27 13:44:22', '2019-07-27 13:44:22'),
(5390, 219, 324, 4, NULL, '2019-07-27 13:45:35', '2019-07-27 13:45:35'),
(5391, 219, 325, 4, NULL, '2019-07-27 13:45:35', '2019-07-27 13:45:35'),
(5392, 219, 326, 4, NULL, '2019-07-27 13:45:35', '2019-07-27 13:45:35'),
(5393, 219, 286, 4, NULL, '2019-07-27 13:45:35', '2019-07-27 13:45:35'),
(5394, 219, 323, 4, NULL, '2019-07-27 13:45:35', '2019-07-27 13:45:35'),
(5395, 219, 322, 4, NULL, '2019-07-27 13:45:35', '2019-07-27 13:45:35'),
(5396, 219, 283, 4, NULL, '2019-07-27 13:45:35', '2019-07-27 13:45:35'),
(5397, 219, 321, 4, NULL, '2019-07-27 13:45:35', '2019-07-27 13:45:35'),
(5398, 219, 320, 4, NULL, '2019-07-27 13:45:35', '2019-07-27 13:45:35'),
(5399, 219, 319, 4, NULL, '2019-07-27 13:45:35', '2019-07-27 13:45:35'),
(5400, 219, 318, 4, NULL, '2019-07-27 13:45:35', '2019-07-27 13:45:35'),
(5401, 219, 327, 4, NULL, '2019-07-27 13:45:35', '2019-07-27 13:45:35'),
(5402, 219, 264, 4, NULL, '2019-07-27 13:45:35', '2019-07-27 13:45:35'),
(5403, 219, 263, 4, NULL, '2019-07-27 13:45:35', '2019-07-27 13:45:35'),
(5404, 240, 264, 4, NULL, '2019-07-27 13:46:23', '2019-07-27 13:46:23'),
(5405, 240, 263, 4, NULL, '2019-07-27 13:46:23', '2019-07-27 13:46:23'),
(5406, 432, 263, 4, NULL, '2019-07-27 13:46:55', '2019-07-27 13:46:55'),
(5407, 200, 263, 4, NULL, '2019-07-27 13:47:27', '2019-07-27 13:47:27'),
(5408, 460, 263, 4, NULL, '2019-07-27 13:48:06', '2019-07-27 13:48:06'),
(5409, 451, 263, 4, NULL, '2019-07-27 13:48:37', '2019-07-27 13:48:37'),
(5411, 293, 263, 4, NULL, '2019-07-27 13:49:54', '2019-07-27 13:49:54'),
(5412, 477, 263, 4, NULL, '2019-07-27 13:50:24', '2019-07-27 13:50:24'),
(5413, 308, 263, 4, NULL, '2019-07-27 13:50:53', '2019-07-27 13:50:53'),
(5414, 388, 263, 4, NULL, '2019-07-27 13:54:16', '2019-07-27 13:54:16'),
(5416, 464, 263, 4, NULL, '2019-07-27 13:54:56', '2019-07-27 13:54:56'),
(5417, 289, 263, 4, NULL, '2019-07-27 13:55:19', '2019-07-27 13:55:19'),
(5418, 485, 263, 4, NULL, '2019-07-27 13:55:39', '2019-07-27 13:55:39'),
(5419, 435, 263, 4, NULL, '2019-07-27 13:56:12', '2019-07-27 13:56:12'),
(5420, 465, 263, 4, NULL, '2019-07-27 13:56:30', '2019-07-27 13:56:30'),
(5421, 292, 263, 4, NULL, '2019-07-27 13:56:48', '2019-07-27 13:56:48'),
(5422, 213, 263, 4, NULL, '2019-07-27 13:57:09', '2019-07-27 13:57:09'),
(5423, 215, 263, 4, NULL, '2019-07-27 13:57:30', '2019-07-27 13:57:30'),
(5424, 482, 263, 4, NULL, '2019-07-27 13:57:52', '2019-07-27 13:57:52'),
(5425, 225, 263, 4, NULL, '2019-07-27 13:58:13', '2019-07-27 13:58:13'),
(5426, 249, 263, 4, NULL, '2019-07-27 13:58:33', '2019-07-27 13:58:33'),
(5427, 291, 263, 4, NULL, '2019-07-27 13:58:55', '2019-07-27 13:58:55'),
(5428, 318, 263, 4, NULL, '2019-07-27 13:59:13', '2019-07-27 13:59:13'),
(5430, 398, 263, 4, NULL, '2019-07-29 07:42:46', '2019-07-29 07:42:46'),
(5431, 229, 263, 4, NULL, '2019-07-29 08:14:23', '2019-07-29 08:14:23'),
(5432, 298, 269, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5433, 298, 336, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5434, 298, 337, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5435, 298, 333, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5436, 298, 310, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5437, 298, 335, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5438, 298, 334, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5439, 298, 272, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5440, 298, 271, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5441, 298, 342, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5442, 298, 311, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5443, 298, 312, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5444, 298, 314, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5445, 298, 339, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5446, 298, 309, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5447, 298, 340, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5448, 298, 263, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5449, 298, 332, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5450, 298, 341, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5451, 298, 313, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5452, 298, 270, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5453, 298, 315, 4, NULL, '2019-07-29 08:14:53', '2019-07-29 08:14:53'),
(5455, 285, 263, 4, NULL, '2019-07-29 08:15:58', '2019-07-29 08:15:58'),
(5456, 436, 263, 4, NULL, '2019-07-29 10:13:41', '2019-07-29 10:13:41'),
(5458, 273, 263, 4, NULL, '2019-07-29 10:16:10', '2019-07-29 10:16:10'),
(5459, 405, 357, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5460, 405, 323, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5461, 405, 358, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5462, 405, 349, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5463, 405, 344, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5464, 405, 347, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5465, 405, 353, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5466, 405, 356, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5467, 405, 355, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5468, 405, 345, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5469, 405, 354, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5470, 405, 352, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5471, 405, 281, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5472, 405, 350, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5473, 405, 343, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5474, 405, 360, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5475, 405, 223, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5476, 405, 346, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5477, 405, 227, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5478, 405, 359, 4, NULL, '2019-07-29 10:19:59', '2019-07-29 10:19:59'),
(5479, 256, 263, 4, NULL, '2019-07-29 10:20:23', '2019-07-29 10:20:23'),
(5480, 297, 263, 4, NULL, '2019-07-29 10:20:50', '2019-07-29 10:20:50'),
(5481, 301, 263, 4, NULL, '2019-07-29 10:28:05', '2019-07-29 10:28:05'),
(5482, 397, 263, 4, NULL, '2019-07-29 10:28:28', '2019-07-29 10:28:28'),
(5484, 288, 263, 4, NULL, '2019-07-29 11:16:02', '2019-07-29 11:16:02'),
(5485, 327, 263, 4, NULL, '2019-07-29 11:16:55', '2019-07-29 11:16:55'),
(5486, 386, 263, 4, NULL, '2019-07-29 11:17:57', '2019-07-29 11:17:57'),
(5487, 314, 263, 4, NULL, '2019-07-29 11:19:23', '2019-07-29 11:19:23'),
(5488, 480, 263, 4, NULL, '2019-07-29 11:21:49', '2019-07-29 11:21:49'),
(5489, 479, 263, 4, NULL, '2019-07-29 11:36:04', '2019-07-29 11:36:04'),
(5490, 394, 263, 4, NULL, '2019-07-29 11:36:24', '2019-07-29 11:36:24'),
(5491, 230, 263, 4, NULL, '2019-07-29 11:36:44', '2019-07-29 11:36:44'),
(5492, 302, 263, 4, NULL, '2019-07-29 11:37:19', '2019-07-29 11:37:19'),
(5493, 441, 203, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5494, 441, 208, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5495, 441, 303, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5496, 441, 212, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5497, 441, 213, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5498, 441, 209, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5499, 441, 214, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5500, 441, 215, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5501, 441, 216, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5502, 441, 304, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5503, 441, 220, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5504, 441, 198, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5505, 441, 221, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5506, 441, 204, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5507, 441, 207, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5508, 441, 224, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5509, 441, 225, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5510, 441, 226, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5511, 441, 305, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5512, 441, 230, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5513, 441, 232, 4, NULL, '2019-07-29 11:37:40', '2019-07-29 11:37:40'),
(5514, 233, 203, 4, NULL, '2019-07-29 11:37:59', '2019-07-29 11:37:59'),
(5515, 233, 208, 4, NULL, '2019-07-29 11:37:59', '2019-07-29 11:37:59'),
(5516, 233, 331, 4, NULL, '2019-07-29 11:37:59', '2019-07-29 11:37:59'),
(5517, 233, 329, 4, NULL, '2019-07-29 11:37:59', '2019-07-29 11:37:59'),
(5518, 233, 218, 4, NULL, '2019-07-29 11:37:59', '2019-07-29 11:37:59'),
(5519, 233, 220, 4, NULL, '2019-07-29 11:37:59', '2019-07-29 11:37:59'),
(5520, 233, 198, 4, NULL, '2019-07-29 11:37:59', '2019-07-29 11:37:59'),
(5521, 233, 204, 4, NULL, '2019-07-29 11:37:59', '2019-07-29 11:37:59'),
(5522, 233, 207, 4, NULL, '2019-07-29 11:37:59', '2019-07-29 11:37:59'),
(5523, 233, 330, 4, NULL, '2019-07-29 11:37:59', '2019-07-29 11:37:59'),
(5524, 233, 224, 4, NULL, '2019-07-29 11:37:59', '2019-07-29 11:37:59'),
(5525, 233, 225, 4, NULL, '2019-07-29 11:37:59', '2019-07-29 11:37:59'),
(5526, 233, 230, 4, NULL, '2019-07-29 11:37:59', '2019-07-29 11:37:59'),
(5527, 233, 232, 4, NULL, '2019-07-29 11:37:59', '2019-07-29 11:37:59'),
(5529, 296, 290, 4, NULL, '2019-07-29 11:38:40', '2019-07-29 11:38:40'),
(5530, 296, 288, 4, NULL, '2019-07-29 11:38:40', '2019-07-29 11:38:40'),
(5531, 296, 287, 4, NULL, '2019-07-29 11:38:40', '2019-07-29 11:38:40'),
(5532, 296, 302, 4, NULL, '2019-07-29 11:38:40', '2019-07-29 11:38:40'),
(5533, 296, 301, 4, NULL, '2019-07-29 11:38:40', '2019-07-29 11:38:40'),
(5534, 296, 292, 4, NULL, '2019-07-29 11:38:40', '2019-07-29 11:38:40'),
(5535, 296, 291, 4, NULL, '2019-07-29 11:38:40', '2019-07-29 11:38:40'),
(5536, 296, 296, 4, NULL, '2019-07-29 11:38:40', '2019-07-29 11:38:40'),
(5537, 296, 289, 4, NULL, '2019-07-29 11:38:40', '2019-07-29 11:38:40'),
(5538, 296, 299, 4, NULL, '2019-07-29 11:38:40', '2019-07-29 11:38:40'),
(5539, 296, 300, 4, NULL, '2019-07-29 11:38:40', '2019-07-29 11:38:40'),
(5540, 296, 294, 4, NULL, '2019-07-29 11:38:40', '2019-07-29 11:38:40'),
(5541, 296, 298, 4, NULL, '2019-07-29 11:38:40', '2019-07-29 11:38:40'),
(5542, 296, 295, 4, NULL, '2019-07-29 11:38:40', '2019-07-29 11:38:40'),
(5543, 296, 297, 4, NULL, '2019-07-29 11:38:40', '2019-07-29 11:38:40'),
(5544, 296, 293, 4, NULL, '2019-07-29 11:38:40', '2019-07-29 11:38:40'),
(5545, 395, 263, 4, NULL, '2019-07-29 11:39:05', '2019-07-29 11:39:05'),
(5547, 250, 263, 4, NULL, '2019-07-29 11:39:55', '2019-07-29 11:39:55'),
(5548, 204, 263, 4, NULL, '2019-07-29 11:40:19', '2019-07-29 11:40:19'),
(5549, 390, 263, 4, NULL, '2019-07-29 11:40:40', '2019-07-29 11:40:40'),
(5552, 290, 263, 4, NULL, '2019-07-29 11:44:40', '2019-07-29 11:44:40'),
(5553, 486, 263, 4, NULL, '2019-07-29 11:45:02', '2019-07-29 11:45:02'),
(5554, 381, 263, 4, NULL, '2019-07-29 11:45:24', '2019-07-29 11:45:24'),
(5555, 475, 263, 4, NULL, '2019-07-29 11:45:44', '2019-07-29 11:45:44'),
(5557, 312, 263, 4, NULL, '2019-07-29 11:47:02', '2019-07-29 11:47:02'),
(5558, 315, 263, 4, NULL, '2019-07-29 11:47:28', '2019-07-29 11:47:28'),
(5559, 309, 263, 4, NULL, '2019-07-29 11:47:52', '2019-07-29 11:47:52'),
(5560, 403, 263, 4, NULL, '2019-07-29 11:50:23', '2019-07-29 11:50:23'),
(5561, 224, 263, 4, NULL, '2019-07-29 11:51:08', '2019-07-29 11:51:08'),
(5562, 304, 263, 4, NULL, '2019-07-29 11:51:36', '2019-07-29 11:51:36'),
(5563, 299, 263, 4, NULL, '2019-07-29 11:53:36', '2019-07-29 11:53:36'),
(5564, 300, 263, 4, NULL, '2019-07-29 11:54:40', '2019-07-29 11:54:40'),
(5565, 478, 263, 4, NULL, '2019-07-29 11:55:02', '2019-07-29 11:55:02'),
(5566, 488, 263, 4, NULL, '2019-08-03 12:31:56', '2019-08-03 12:31:56'),
(5567, 495, 23, 4, NULL, '2019-08-07 05:32:08', '2019-08-07 05:32:08'),
(5569, 497, 263, 4, NULL, '2019-08-18 06:32:28', '2019-08-18 06:32:28'),
(5570, 498, 263, 4, NULL, '2019-08-18 06:36:30', '2019-08-18 06:36:30'),
(5571, 499, 263, 4, NULL, '2019-08-18 06:45:40', '2019-08-18 06:45:40'),
(5573, 447, 273, 4, NULL, '2019-08-19 05:51:12', '2019-08-19 05:51:12'),
(5574, 447, 275, 4, NULL, '2019-08-19 05:51:12', '2019-08-19 05:51:12'),
(5575, 447, 270, 4, NULL, '2019-08-19 05:51:12', '2019-08-19 05:51:12'),
(5576, 447, 274, 4, NULL, '2019-08-19 05:51:12', '2019-08-19 05:51:12'),
(5577, 228, 263, 4, NULL, '2019-08-19 05:52:38', '2019-08-19 05:52:38'),
(5578, 228, 338, 4, NULL, '2019-08-19 05:52:38', '2019-08-19 05:52:38'),
(5580, 476, 263, 4, NULL, '2019-08-24 11:21:47', '2019-08-24 11:21:47'),
(5581, 501, 263, 4, NULL, '2019-08-31 06:46:07', '2019-08-31 06:46:07'),
(5582, 463, 263, 4, NULL, '2019-09-01 05:42:41', '2019-09-01 05:42:41'),
(5584, 190, 263, 4, NULL, '2019-09-19 05:34:07', '2019-09-19 05:34:07'),
(5585, 10, 29, 4, NULL, '2019-09-19 08:20:09', '2019-09-19 08:20:09'),
(5588, 503, 263, 4, NULL, '2019-09-30 05:56:54', '2019-09-30 05:56:54'),
(5595, 506, 263, 4, NULL, '2019-09-30 06:54:30', '2019-09-30 06:54:30'),
(5596, 507, 263, 4, NULL, '2019-10-02 08:25:17', '2019-10-02 08:25:17'),
(5599, 502, 263, 4, NULL, '2019-10-09 11:12:18', '2019-10-09 11:12:18'),
(5603, 504, 263, 4, NULL, '2019-10-13 11:44:43', '2019-10-13 11:44:43'),
(5610, 508, 28, 4, NULL, '2019-10-14 06:42:28', '2019-10-14 06:42:28'),
(5611, 505, 28, 4, NULL, '2019-10-23 07:03:12', '2019-10-23 07:03:12'),
(5612, 484, 263, 4, NULL, '2019-10-23 11:56:06', '2019-10-23 11:56:06'),
(5613, 484, 394, 4, NULL, '2019-10-23 11:56:06', '2019-10-23 11:56:06'),
(5614, 509, 263, 4, NULL, '2019-10-24 06:23:13', '2019-10-24 06:23:13'),
(5615, 474, 263, 4, NULL, '2019-11-17 13:44:34', '2019-11-17 13:44:34'),
(5616, 310, 263, 4, NULL, '2019-11-24 13:05:40', '2019-11-24 13:05:40'),
(5617, 510, 263, 4, NULL, '2019-11-25 10:39:30', '2019-11-25 10:39:30'),
(5620, 494, 263, 4, NULL, '2019-12-02 06:20:52', '2019-12-02 06:20:52'),
(5621, 472, 263, 4, NULL, '2019-12-10 13:29:12', '2019-12-10 13:29:12'),
(5622, 462, 29, 4, NULL, '2019-12-10 13:35:20', '2019-12-10 13:35:20'),
(5623, 266, 263, 4, NULL, '2019-12-11 11:08:51', '2019-12-11 11:08:51'),
(5624, 440, 265, 4, NULL, '2019-12-11 11:14:00', '2019-12-11 11:14:00'),
(5625, 440, 255, 4, NULL, '2019-12-11 11:14:00', '2019-12-11 11:14:00'),
(5626, 440, 233, 4, NULL, '2019-12-11 11:14:00', '2019-12-11 11:14:00'),
(5627, 440, 234, 4, NULL, '2019-12-11 11:14:00', '2019-12-11 11:14:00'),
(5628, 440, 235, 4, NULL, '2019-12-11 11:14:00', '2019-12-11 11:14:00'),
(5629, 440, 236, 4, NULL, '2019-12-11 11:14:00', '2019-12-11 11:14:00'),
(5630, 440, 268, 4, NULL, '2019-12-11 11:14:00', '2019-12-11 11:14:00'),
(5631, 440, 237, 4, NULL, '2019-12-11 11:14:00', '2019-12-11 11:14:00'),
(5632, 440, 267, 4, NULL, '2019-12-11 11:14:00', '2019-12-11 11:14:00'),
(5633, 440, 256, 4, NULL, '2019-12-11 11:14:00', '2019-12-11 11:14:00'),
(5634, 440, 239, 4, NULL, '2019-12-11 11:14:00', '2019-12-11 11:14:00'),
(5635, 440, 241, 4, NULL, '2019-12-11 11:14:00', '2019-12-11 11:14:00'),
(5636, 440, 266, 4, NULL, '2019-12-11 11:14:00', '2019-12-11 11:14:00'),
(5637, 440, 244, 4, NULL, '2019-12-11 11:14:00', '2019-12-11 11:14:00'),
(5638, 440, 245, 4, NULL, '2019-12-11 11:14:00', '2019-12-11 11:14:00'),
(5639, 440, 247, 4, NULL, '2019-12-11 11:14:00', '2019-12-11 11:14:00'),
(5640, 440, 248, 4, NULL, '2019-12-11 11:14:00', '2019-12-11 11:14:00'),
(5641, 440, 249, 4, NULL, '2019-12-11 11:14:01', '2019-12-11 11:14:01'),
(5642, 440, 254, 4, NULL, '2019-12-11 11:14:01', '2019-12-11 11:14:01'),
(5643, 194, 263, 4, NULL, '2019-12-11 11:24:19', '2019-12-11 11:24:19'),
(5644, 43, 28, 4, NULL, '2019-12-11 11:25:38', '2019-12-11 11:25:38'),
(5645, 195, 263, 4, NULL, '2019-12-11 11:26:25', '2019-12-11 11:26:25'),
(5646, 377, 263, 4, NULL, '2019-12-11 11:29:07', '2019-12-11 11:29:07'),
(5647, 197, 263, 4, NULL, '2019-12-11 11:31:59', '2019-12-11 11:31:59'),
(5648, 286, 263, 4, NULL, '2019-12-11 11:33:07', '2019-12-11 11:33:07'),
(5649, 373, 28, 4, NULL, '2019-12-11 11:35:15', '2019-12-11 11:35:15'),
(5650, 379, 263, 4, NULL, '2019-12-11 11:36:21', '2019-12-11 11:36:21'),
(5651, 471, 263, 4, NULL, '2019-12-11 11:38:46', '2019-12-11 11:38:46'),
(5652, 483, 263, 4, NULL, '2019-12-11 11:41:34', '2019-12-11 11:41:34'),
(5653, 511, 28, 4, NULL, '2020-01-09 07:29:05', '2020-01-09 07:29:05'),
(5654, 23, 28, 4, NULL, '2020-01-14 08:45:34', '2020-01-14 08:45:34');

-- --------------------------------------------------------

--
-- Table structure for table `shop_pics`
--

CREATE TABLE `shop_pics` (
  `_id` int(10) UNSIGNED NOT NULL,
  `shop_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shop_pics`
--

INSERT INTO `shop_pics` (`_id`, `shop_id`, `image`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(7, 3, '1505319098_1.jpg', 3, NULL, '2017-09-13 16:11:38', '2017-09-13 16:11:38'),
(8, 3, '1505319098_2.jpg', 3, NULL, '2017-09-13 16:11:38', '2017-09-13 16:11:38'),
(19, 9, '1505319333_1.jpg', 3, NULL, '2017-09-13 16:15:33', '2017-09-13 16:15:33'),
(21, 1, '1505893146_1.jpg', 3, NULL, '2017-09-20 07:39:06', '2017-09-20 07:39:06'),
(22, 1, '1505893146_2.png', 3, NULL, '2017-09-20 07:39:06', '2017-09-20 07:39:06'),
(24, 5, '1505893699_2.jpg', 3, NULL, '2017-09-20 07:48:19', '2017-09-20 07:48:19'),
(25, 2, '1505893741_1.jpg', 3, NULL, '2017-09-20 07:49:01', '2017-09-20 07:49:01'),
(27, 6, '1505893842_2.jpg', 3, NULL, '2017-09-20 07:50:42', '2017-09-20 07:50:42'),
(28, 7, '1505893894_1.jpg', 3, NULL, '2017-09-20 07:51:34', '2017-09-20 07:51:34'),
(30, 8, '1505893947_1.jpg', 3, NULL, '2017-09-20 07:52:27', '2017-09-20 07:52:27'),
(33, 22, '1510655637_1.jpg', 3, NULL, '2017-11-14 10:33:57', '2017-11-14 10:33:57'),
(53, 8, '1510656656_1.jpg', 3, NULL, '2017-11-14 10:50:56', '2017-11-14 10:50:56'),
(54, 7, '1510656706_1.jpg', 3, NULL, '2017-11-14 10:51:46', '2017-11-14 10:51:46'),
(55, 6, '1510656757_1.jpg', 3, NULL, '2017-11-14 10:52:37', '2017-11-14 10:52:37'),
(56, 31, '1510669129_1.jpg', 3, NULL, '2017-11-14 14:18:49', '2017-11-14 14:18:49'),
(57, 31, '1510669129_2.jpg', 3, NULL, '2017-11-14 14:18:49', '2017-11-14 14:18:49'),
(58, 32, '1510669192_1.jpg', 3, NULL, '2017-11-14 14:19:52', '2017-11-14 14:19:52'),
(59, 33, '1510669248_1.jpg', 3, NULL, '2017-11-14 14:20:48', '2017-11-14 14:20:48'),
(60, 33, '1510669248_2.jpg', 3, NULL, '2017-11-14 14:20:48', '2017-11-14 14:20:48'),
(61, 145, '1514990010_1.jpg', 3, NULL, '2018-01-03 14:33:30', '2018-01-03 14:33:30'),
(62, 145, '1514990148_1.jpg', 3, NULL, '2018-01-03 14:35:48', '2018-01-03 14:35:48'),
(63, 106, '1517214348_1.jpg', 2, NULL, '2018-01-29 08:25:48', '2018-01-29 08:25:48'),
(64, 106, '1517214348_1.jpg', 2, NULL, '2018-01-29 08:25:48', '2018-01-29 08:25:48'),
(65, 106, '1517214350_1.jpg', 2, NULL, '2018-01-29 08:25:50', '2018-01-29 08:25:50'),
(70, 184, '1518181531_1.jpg', 3, NULL, '2018-02-09 13:05:31', '2018-02-09 13:05:31'),
(71, 184, '1518181541_1.jpg', 3, NULL, '2018-02-09 13:05:41', '2018-02-09 13:05:41'),
(72, 184, '1518181567_1.jpg', 3, NULL, '2018-02-09 13:06:07', '2018-02-09 13:06:07'),
(77, 266, '1521716871_1.jpg', 4, NULL, '2018-03-22 11:07:51', '2018-03-22 11:07:51'),
(81, 410, '1538337959_1.jpg', 2, NULL, '2018-09-30 20:05:59', '2018-09-30 20:05:59'),
(95, 77, '1552198683_1.jpg', 4, NULL, '2019-03-10 06:18:03', '2019-03-10 06:18:03'),
(96, 77, '1552199087_1.jpg', 4, NULL, '2019-03-10 06:24:47', '2019-03-10 06:24:47'),
(97, 77, '1552199093_1.jpg', 4, NULL, '2019-03-10 06:24:53', '2019-03-10 06:24:53'),
(98, 77, '1552199099_1.jpg', 4, NULL, '2019-03-10 06:24:59', '2019-03-10 06:24:59'),
(99, 77, '1552199106_1.jpg', 4, NULL, '2019-03-10 06:25:06', '2019-03-10 06:25:06'),
(105, 403, '1552199777_1.jpg', 4, NULL, '2019-03-10 06:36:17', '2019-03-10 06:36:17'),
(106, 403, '1552199782_1.jpg', 4, NULL, '2019-03-10 06:36:22', '2019-03-10 06:36:22'),
(107, 403, '1552199788_1.jpg', 4, NULL, '2019-03-10 06:36:28', '2019-03-10 06:36:28'),
(108, 403, '1552199794_1.jpg', 4, NULL, '2019-03-10 06:36:34', '2019-03-10 06:36:34'),
(148, 23, '1552215180_1.jpg', 4, NULL, '2019-03-10 10:53:00', '2019-03-10 10:53:00'),
(150, 23, '1552215193_1.jpg', 4, NULL, '2019-03-10 10:53:13', '2019-03-10 10:53:13'),
(151, 23, '1552215405_1.jpg', 4, NULL, '2019-03-10 10:56:45', '2019-03-10 10:56:45'),
(152, 23, '1552215599_1.jpg', 4, NULL, '2019-03-10 10:59:59', '2019-03-10 10:59:59'),
(153, 10, '1552215948_1.jpg', 4, NULL, '2019-03-10 11:05:48', '2019-03-10 11:05:48'),
(154, 10, '1552215953_1.jpg', 4, NULL, '2019-03-10 11:05:53', '2019-03-10 11:05:53'),
(155, 10, '1552215958_1.jpg', 4, NULL, '2019-03-10 11:05:58', '2019-03-10 11:05:58'),
(156, 10, '1552215964_1.jpg', 4, NULL, '2019-03-10 11:06:04', '2019-03-10 11:06:04'),
(157, 10, '1552215970_1.jpg', 4, NULL, '2019-03-10 11:06:10', '2019-03-10 11:06:10'),
(158, 10, '1552215976_1.jpg', 4, NULL, '2019-03-10 11:06:16', '2019-03-10 11:06:16'),
(159, 25, '1552216476_1.jpg', 4, NULL, '2019-03-10 11:14:36', '2019-03-10 11:14:36'),
(160, 25, '1552216484_1.jpg', 4, NULL, '2019-03-10 11:14:44', '2019-03-10 11:14:44'),
(161, 25, '1552216489_1.jpg', 4, NULL, '2019-03-10 11:14:49', '2019-03-10 11:14:49'),
(162, 25, '1552216495_1.jpg', 4, NULL, '2019-03-10 11:14:55', '2019-03-10 11:14:55'),
(163, 25, '1552216501_1.jpg', 4, NULL, '2019-03-10 11:15:01', '2019-03-10 11:15:01'),
(164, 25, '1552216507_1.jpg', 4, NULL, '2019-03-10 11:15:07', '2019-03-10 11:15:07'),
(165, 25, '1552216513_1.jpg', 4, NULL, '2019-03-10 11:15:13', '2019-03-10 11:15:13'),
(166, 34, '1552216975_1.jpg', 4, NULL, '2019-03-10 11:22:55', '2019-03-10 11:22:55'),
(167, 34, '1552216980_1.jpg', 4, NULL, '2019-03-10 11:23:00', '2019-03-10 11:23:00'),
(168, 34, '1552216986_1.jpg', 4, NULL, '2019-03-10 11:23:06', '2019-03-10 11:23:06'),
(169, 34, '1552216991_1.jpg', 4, NULL, '2019-03-10 11:23:11', '2019-03-10 11:23:11'),
(170, 34, '1552216996_1.jpg', 4, NULL, '2019-03-10 11:23:16', '2019-03-10 11:23:16'),
(171, 35, '1552218967_1.jpg', 4, NULL, '2019-03-10 11:56:07', '2019-03-10 11:56:07'),
(172, 35, '1552218971_1.jpg', 4, NULL, '2019-03-10 11:56:11', '2019-03-10 11:56:11'),
(173, 35, '1552218977_1.jpg', 4, NULL, '2019-03-10 11:56:17', '2019-03-10 11:56:17'),
(174, 35, '1552218983_1.jpg', 4, NULL, '2019-03-10 11:56:23', '2019-03-10 11:56:23'),
(175, 35, '1552218991_1.jpg', 4, NULL, '2019-03-10 11:56:31', '2019-03-10 11:56:31'),
(176, 35, '1552219003_1.jpg', 4, NULL, '2019-03-10 11:56:43', '2019-03-10 11:56:43'),
(177, 35, '1552219020_1.jpg', 4, NULL, '2019-03-10 11:57:00', '2019-03-10 11:57:00'),
(178, 24, '1552219367_1.jpg', 4, NULL, '2019-03-10 12:02:47', '2019-03-10 12:02:47'),
(179, 24, '1552219373_1.jpg', 4, NULL, '2019-03-10 12:02:53', '2019-03-10 12:02:53'),
(180, 24, '1552219380_1.jpg', 4, NULL, '2019-03-10 12:03:00', '2019-03-10 12:03:00'),
(181, 24, '1552219386_1.jpg', 4, NULL, '2019-03-10 12:03:06', '2019-03-10 12:03:06'),
(182, 24, '1552219391_1.jpg', 4, NULL, '2019-03-10 12:03:11', '2019-03-10 12:03:11'),
(183, 24, '1552219396_1.jpg', 4, NULL, '2019-03-10 12:03:16', '2019-03-10 12:03:16'),
(184, 444, '1552220109_1.jpg', 4, NULL, '2019-03-10 12:15:09', '2019-03-10 12:15:09'),
(185, 444, '1552220115_1.jpg', 4, NULL, '2019-03-10 12:15:15', '2019-03-10 12:15:15'),
(186, 444, '1552220120_1.jpg', 4, NULL, '2019-03-10 12:15:20', '2019-03-10 12:15:20'),
(187, 444, '1552220126_1.jpg', 4, NULL, '2019-03-10 12:15:26', '2019-03-10 12:15:26'),
(188, 43, '1552221375_1.jpg', 4, NULL, '2019-03-10 12:36:15', '2019-03-10 12:36:15'),
(189, 43, '1552221380_1.jpg', 4, NULL, '2019-03-10 12:36:20', '2019-03-10 12:36:20'),
(190, 43, '1552221385_1.jpg', 4, NULL, '2019-03-10 12:36:25', '2019-03-10 12:36:25'),
(191, 43, '1552221391_1.jpg', 4, NULL, '2019-03-10 12:36:31', '2019-03-10 12:36:31'),
(192, 43, '1552221396_1.jpg', 4, NULL, '2019-03-10 12:36:36', '2019-03-10 12:36:36'),
(193, 38, '1552222246_1.jpg', 4, NULL, '2019-03-10 12:50:46', '2019-03-10 12:50:46'),
(194, 38, '1552222253_1.jpg', 4, NULL, '2019-03-10 12:50:53', '2019-03-10 12:50:53'),
(195, 38, '1552222261_1.jpg', 4, NULL, '2019-03-10 12:51:01', '2019-03-10 12:51:01'),
(196, 38, '1552222267_1.jpg', 4, NULL, '2019-03-10 12:51:07', '2019-03-10 12:51:07'),
(197, 38, '1552222319_1.jpg', 4, NULL, '2019-03-10 12:51:59', '2019-03-10 12:51:59'),
(198, 48, '1552222873_1.jpg', 4, NULL, '2019-03-10 13:01:13', '2019-03-10 13:01:13'),
(199, 48, '1552222883_1.jpg', 4, NULL, '2019-03-10 13:01:23', '2019-03-10 13:01:23'),
(200, 48, '1552222888_1.jpg', 4, NULL, '2019-03-10 13:01:28', '2019-03-10 13:01:28'),
(201, 48, '1552222893_1.jpg', 4, NULL, '2019-03-10 13:01:33', '2019-03-10 13:01:33'),
(202, 48, '1552222898_1.jpg', 4, NULL, '2019-03-10 13:01:38', '2019-03-10 13:01:38'),
(203, 48, '1552222903_1.jpg', 4, NULL, '2019-03-10 13:01:43', '2019-03-10 13:01:43'),
(204, 47, '1552225602_1.jpg', 4, NULL, '2019-03-10 13:46:42', '2019-03-10 13:46:42'),
(205, 47, '1552225612_1.jpg', 4, NULL, '2019-03-10 13:46:52', '2019-03-10 13:46:52'),
(206, 47, '1552225618_1.jpg', 4, NULL, '2019-03-10 13:46:58', '2019-03-10 13:46:58'),
(207, 47, '1552225623_1.jpg', 4, NULL, '2019-03-10 13:47:03', '2019-03-10 13:47:03'),
(208, 47, '1552225628_1.jpg', 4, NULL, '2019-03-10 13:47:08', '2019-03-10 13:47:08'),
(209, 47, '1552225634_1.jpg', 4, NULL, '2019-03-10 13:47:14', '2019-03-10 13:47:14'),
(210, 47, '1552225640_1.jpg', 4, NULL, '2019-03-10 13:47:20', '2019-03-10 13:47:20'),
(211, 47, '1552225645_1.jpg', 4, NULL, '2019-03-10 13:47:25', '2019-03-10 13:47:25'),
(212, 49, '1552288493_1.jpg', 4, NULL, '2019-03-11 07:14:53', '2019-03-11 07:14:53'),
(213, 49, '1552288498_1.jpg', 4, NULL, '2019-03-11 07:14:58', '2019-03-11 07:14:58'),
(214, 49, '1552288503_1.jpg', 4, NULL, '2019-03-11 07:15:03', '2019-03-11 07:15:03'),
(215, 49, '1552288508_1.jpg', 4, NULL, '2019-03-11 07:15:08', '2019-03-11 07:15:08'),
(216, 49, '1552288512_1.jpg', 4, NULL, '2019-03-11 07:15:12', '2019-03-11 07:15:12'),
(217, 49, '1552288518_1.jpg', 4, NULL, '2019-03-11 07:15:18', '2019-03-11 07:15:18'),
(224, 63, '1552291513_1.jpg', 4, NULL, '2019-03-11 08:05:13', '2019-03-11 08:05:13'),
(225, 63, '1552291521_1.jpg', 4, NULL, '2019-03-11 08:05:21', '2019-03-11 08:05:21'),
(226, 63, '1552291527_1.jpg', 4, NULL, '2019-03-11 08:05:27', '2019-03-11 08:05:27'),
(227, 63, '1552291532_1.jpg', 4, NULL, '2019-03-11 08:05:32', '2019-03-11 08:05:32'),
(228, 63, '1552291540_1.jpg', 4, NULL, '2019-03-11 08:05:40', '2019-03-11 08:05:40'),
(229, 56, '1552291768_1.jpg', 4, NULL, '2019-03-11 08:09:28', '2019-03-11 08:09:28'),
(230, 56, '1552291775_1.jpg', 4, NULL, '2019-03-11 08:09:35', '2019-03-11 08:09:35'),
(231, 56, '1552291784_1.jpg', 4, NULL, '2019-03-11 08:09:44', '2019-03-11 08:09:44'),
(232, 56, '1552291790_1.jpg', 4, NULL, '2019-03-11 08:09:50', '2019-03-11 08:09:50'),
(233, 56, '1552291797_1.jpg', 4, NULL, '2019-03-11 08:09:57', '2019-03-11 08:09:57'),
(234, 51, '1552292350_1.jpg', 4, NULL, '2019-03-11 08:19:10', '2019-03-11 08:19:10'),
(235, 51, '1552292443_1.jpg', 4, NULL, '2019-03-11 08:20:43', '2019-03-11 08:20:43'),
(236, 51, '1552292449_1.jpg', 4, NULL, '2019-03-11 08:20:49', '2019-03-11 08:20:49'),
(237, 51, '1552292454_1.jpg', 4, NULL, '2019-03-11 08:20:54', '2019-03-11 08:20:54'),
(238, 51, '1552292459_1.jpg', 4, NULL, '2019-03-11 08:20:59', '2019-03-11 08:20:59'),
(239, 51, '1552292465_1.jpg', 4, NULL, '2019-03-11 08:21:05', '2019-03-11 08:21:05'),
(240, 51, '1552292469_1.jpg', 4, NULL, '2019-03-11 08:21:09', '2019-03-11 08:21:09'),
(241, 201, '1552305793_1.jpg', 4, NULL, '2019-03-11 12:03:13', '2019-03-11 12:03:13'),
(242, 201, '1552305799_1.jpg', 4, NULL, '2019-03-11 12:03:19', '2019-03-11 12:03:19'),
(243, 201, '1552305807_1.jpg', 4, NULL, '2019-03-11 12:03:27', '2019-03-11 12:03:27'),
(244, 201, '1552305813_1.jpg', 4, NULL, '2019-03-11 12:03:33', '2019-03-11 12:03:33'),
(245, 201, '1552305820_1.jpg', 4, NULL, '2019-03-11 12:03:40', '2019-03-11 12:03:40'),
(246, 201, '1552305825_1.jpg', 4, NULL, '2019-03-11 12:03:45', '2019-03-11 12:03:45'),
(247, 291, '1552306390_1.jpg', 4, NULL, '2019-03-11 12:13:10', '2019-03-11 12:13:10'),
(248, 291, '1552306396_1.jpg', 4, NULL, '2019-03-11 12:13:16', '2019-03-11 12:13:16'),
(249, 291, '1552306401_1.jpg', 4, NULL, '2019-03-11 12:13:21', '2019-03-11 12:13:21'),
(250, 291, '1552306406_1.jpg', 4, NULL, '2019-03-11 12:13:26', '2019-03-11 12:13:26'),
(251, 291, '1552306410_1.jpg', 4, NULL, '2019-03-11 12:13:30', '2019-03-11 12:13:30'),
(252, 291, '1552306416_1.jpg', 4, NULL, '2019-03-11 12:13:36', '2019-03-11 12:13:36'),
(253, 291, '1552306420_1.jpg', 4, NULL, '2019-03-11 12:13:40', '2019-03-11 12:13:40'),
(254, 263, '1552367467_1.jpg', 4, NULL, '2019-03-12 05:11:07', '2019-03-12 05:11:07'),
(255, 263, '1552367472_1.jpg', 4, NULL, '2019-03-12 05:11:12', '2019-03-12 05:11:12'),
(256, 263, '1552367480_1.jpg', 4, NULL, '2019-03-12 05:11:20', '2019-03-12 05:11:20'),
(257, 263, '1552367484_1.jpg', 4, NULL, '2019-03-12 05:11:24', '2019-03-12 05:11:24'),
(258, 263, '1552367489_1.jpg', 4, NULL, '2019-03-12 05:11:29', '2019-03-12 05:11:29'),
(259, 263, '1552367499_1.jpg', 4, NULL, '2019-03-12 05:11:39', '2019-03-12 05:11:39'),
(260, 263, '1552367508_1.jpg', 4, NULL, '2019-03-12 05:11:48', '2019-03-12 05:11:48'),
(261, 195, '1552367969_1.jpg', 4, NULL, '2019-03-12 05:19:29', '2019-03-12 05:19:29'),
(262, 195, '1552367975_1.jpg', 4, NULL, '2019-03-12 05:19:35', '2019-03-12 05:19:35'),
(263, 195, '1552367981_1.jpg', 4, NULL, '2019-03-12 05:19:41', '2019-03-12 05:19:41'),
(264, 195, '1552367987_1.jpg', 4, NULL, '2019-03-12 05:19:47', '2019-03-12 05:19:47'),
(265, 195, '1552367993_1.jpg', 4, NULL, '2019-03-12 05:19:53', '2019-03-12 05:19:53'),
(266, 195, '1552367999_1.jpg', 4, NULL, '2019-03-12 05:19:59', '2019-03-12 05:19:59'),
(267, 210, '1552368278_1.jpg', 4, NULL, '2019-03-12 05:24:38', '2019-03-12 05:24:38'),
(268, 210, '1552368283_1.jpg', 4, NULL, '2019-03-12 05:24:43', '2019-03-12 05:24:43'),
(269, 210, '1552368288_1.jpg', 4, NULL, '2019-03-12 05:24:48', '2019-03-12 05:24:48'),
(270, 210, '1552368293_1.jpg', 4, NULL, '2019-03-12 05:24:53', '2019-03-12 05:24:53'),
(271, 210, '1552368297_1.jpg', 4, NULL, '2019-03-12 05:24:57', '2019-03-12 05:24:57'),
(272, 457, '1552373017_1.jpg', 4, NULL, '2019-03-12 06:43:37', '2019-03-12 06:43:37'),
(273, 457, '1552373023_1.jpg', 4, NULL, '2019-03-12 06:43:43', '2019-03-12 06:43:43'),
(274, 457, '1552373028_1.jpg', 4, NULL, '2019-03-12 06:43:48', '2019-03-12 06:43:48'),
(279, 71, '1552385371_1.jpg', 4, NULL, '2019-03-12 10:09:31', '2019-03-12 10:09:31'),
(280, 260, '1552385834_1.jpg', 4, NULL, '2019-03-12 10:17:14', '2019-03-12 10:17:14'),
(281, 260, '1552386065_1.jpg', 4, NULL, '2019-03-12 10:21:05', '2019-03-12 10:21:05'),
(282, 260, '1552386070_1.jpg', 4, NULL, '2019-03-12 10:21:10', '2019-03-12 10:21:10'),
(283, 260, '1552386076_1.jpg', 4, NULL, '2019-03-12 10:21:16', '2019-03-12 10:21:16'),
(284, 260, '1552386082_1.jpg', 4, NULL, '2019-03-12 10:21:22', '2019-03-12 10:21:22'),
(285, 260, '1552386098_1.jpg', 4, NULL, '2019-03-12 10:21:38', '2019-03-12 10:21:38'),
(286, 237, '1552386966_1.jpg', 4, NULL, '2019-03-12 10:36:06', '2019-03-12 10:36:06'),
(287, 237, '1552386972_1.jpg', 4, NULL, '2019-03-12 10:36:12', '2019-03-12 10:36:12'),
(288, 237, '1552386978_1.jpg', 4, NULL, '2019-03-12 10:36:18', '2019-03-12 10:36:18'),
(289, 237, '1552386983_1.jpg', 4, NULL, '2019-03-12 10:36:23', '2019-03-12 10:36:23'),
(290, 237, '1552386988_1.jpg', 4, NULL, '2019-03-12 10:36:28', '2019-03-12 10:36:28'),
(291, 237, '1552386993_1.jpg', 4, NULL, '2019-03-12 10:36:33', '2019-03-12 10:36:33'),
(299, 240, '1552388334_1.jpg', 4, NULL, '2019-03-12 10:58:54', '2019-03-12 10:58:54'),
(300, 240, '1552388340_1.jpg', 4, NULL, '2019-03-12 10:59:00', '2019-03-12 10:59:00'),
(301, 240, '1552388346_1.jpg', 4, NULL, '2019-03-12 10:59:06', '2019-03-12 10:59:06'),
(302, 240, '1552388351_1.jpg', 4, NULL, '2019-03-12 10:59:11', '2019-03-12 10:59:11'),
(303, 299, '1552388601_1.jpg', 4, NULL, '2019-03-12 11:03:21', '2019-03-12 11:03:21'),
(304, 299, '1552388609_1.jpg', 4, NULL, '2019-03-12 11:03:29', '2019-03-12 11:03:29'),
(305, 299, '1552388614_1.jpg', 4, NULL, '2019-03-12 11:03:34', '2019-03-12 11:03:34'),
(306, 299, '1552388619_1.jpg', 4, NULL, '2019-03-12 11:03:39', '2019-03-12 11:03:39'),
(313, 301, '1552390956_1.jpg', 4, NULL, '2019-03-12 11:42:36', '2019-03-12 11:42:36'),
(314, 301, '1552390962_1.jpg', 4, NULL, '2019-03-12 11:42:42', '2019-03-12 11:42:42'),
(315, 301, '1552390967_1.jpg', 4, NULL, '2019-03-12 11:42:47', '2019-03-12 11:42:47'),
(316, 302, '1552391128_1.jpg', 4, NULL, '2019-03-12 11:45:28', '2019-03-12 11:45:28'),
(317, 372, '1552392910_1.jpg', 4, NULL, '2019-03-12 12:15:10', '2019-03-12 12:15:10'),
(318, 372, '1552392916_1.jpg', 4, NULL, '2019-03-12 12:15:16', '2019-03-12 12:15:16'),
(319, 372, '1552392920_1.jpg', 4, NULL, '2019-03-12 12:15:20', '2019-03-12 12:15:20'),
(320, 372, '1552392926_1.jpg', 4, NULL, '2019-03-12 12:15:26', '2019-03-12 12:15:26'),
(321, 372, '1552392932_1.jpg', 4, NULL, '2019-03-12 12:15:32', '2019-03-12 12:15:32'),
(322, 372, '1552392939_1.jpg', 4, NULL, '2019-03-12 12:15:39', '2019-03-12 12:15:39'),
(323, 269, '1552393691_1.jpg', 4, NULL, '2019-03-12 12:28:11', '2019-03-12 12:28:11'),
(324, 269, '1552393696_1.jpg', 4, NULL, '2019-03-12 12:28:16', '2019-03-12 12:28:16'),
(325, 269, '1552393701_1.jpg', 4, NULL, '2019-03-12 12:28:21', '2019-03-12 12:28:21'),
(326, 269, '1552393706_1.jpg', 4, NULL, '2019-03-12 12:28:26', '2019-03-12 12:28:26'),
(327, 269, '1552393711_1.jpg', 4, NULL, '2019-03-12 12:28:31', '2019-03-12 12:28:31'),
(328, 269, '1552393718_1.jpg', 4, NULL, '2019-03-12 12:28:38', '2019-03-12 12:28:38'),
(329, 244, '1552394250_1.jpg', 4, NULL, '2019-03-12 12:37:30', '2019-03-12 12:37:30'),
(330, 244, '1552394255_1.jpg', 4, NULL, '2019-03-12 12:37:35', '2019-03-12 12:37:35'),
(331, 244, '1552394260_1.jpg', 4, NULL, '2019-03-12 12:37:40', '2019-03-12 12:37:40'),
(332, 244, '1552394265_1.jpg', 4, NULL, '2019-03-12 12:37:45', '2019-03-12 12:37:45'),
(333, 190, '1552459502_1.jpg', 4, NULL, '2019-03-13 06:45:02', '2019-03-13 06:45:02'),
(334, 190, '1552459507_1.jpg', 4, NULL, '2019-03-13 06:45:07', '2019-03-13 06:45:07'),
(335, 190, '1552459512_1.jpg', 4, NULL, '2019-03-13 06:45:12', '2019-03-13 06:45:12'),
(336, 190, '1552459517_1.jpg', 4, NULL, '2019-03-13 06:45:17', '2019-03-13 06:45:17'),
(337, 284, '1552459861_1.jpg', 4, NULL, '2019-03-13 06:51:01', '2019-03-13 06:51:01'),
(338, 284, '1552459866_1.jpg', 4, NULL, '2019-03-13 06:51:06', '2019-03-13 06:51:06'),
(339, 284, '1552459871_1.jpg', 4, NULL, '2019-03-13 06:51:11', '2019-03-13 06:51:11'),
(340, 284, '1552459876_1.jpg', 4, NULL, '2019-03-13 06:51:16', '2019-03-13 06:51:16'),
(341, 284, '1552459880_1.jpg', 4, NULL, '2019-03-13 06:51:20', '2019-03-13 06:51:20'),
(342, 284, '1552459885_1.jpg', 4, NULL, '2019-03-13 06:51:25', '2019-03-13 06:51:25'),
(343, 284, '1552459890_1.jpg', 4, NULL, '2019-03-13 06:51:30', '2019-03-13 06:51:30'),
(344, 289, '1552460173_1.jpg', 4, NULL, '2019-03-13 06:56:13', '2019-03-13 06:56:13'),
(345, 289, '1552460181_1.jpg', 4, NULL, '2019-03-13 06:56:21', '2019-03-13 06:56:21'),
(346, 289, '1552460185_1.jpg', 4, NULL, '2019-03-13 06:56:25', '2019-03-13 06:56:25'),
(347, 289, '1552460189_1.jpg', 4, NULL, '2019-03-13 06:56:29', '2019-03-13 06:56:29'),
(348, 289, '1552460194_1.jpg', 4, NULL, '2019-03-13 06:56:34', '2019-03-13 06:56:34'),
(349, 289, '1552460201_1.jpg', 4, NULL, '2019-03-13 06:56:41', '2019-03-13 06:56:41'),
(356, 448, '1552460904_1.jpg', 4, NULL, '2019-03-13 07:08:24', '2019-03-13 07:08:24'),
(357, 448, '1552460909_1.jpg', 4, NULL, '2019-03-13 07:08:29', '2019-03-13 07:08:29'),
(358, 448, '1552460913_1.jpg', 4, NULL, '2019-03-13 07:08:33', '2019-03-13 07:08:33'),
(359, 448, '1552460918_1.jpg', 4, NULL, '2019-03-13 07:08:38', '2019-03-13 07:08:38'),
(360, 448, '1552460923_1.jpg', 4, NULL, '2019-03-13 07:08:43', '2019-03-13 07:08:43'),
(361, 292, '1552461240_1.jpg', 4, NULL, '2019-03-13 07:14:00', '2019-03-13 07:14:00'),
(362, 292, '1552461246_1.jpg', 4, NULL, '2019-03-13 07:14:06', '2019-03-13 07:14:06'),
(363, 292, '1552461251_1.jpg', 4, NULL, '2019-03-13 07:14:11', '2019-03-13 07:14:11'),
(364, 292, '1552461256_1.jpg', 4, NULL, '2019-03-13 07:14:16', '2019-03-13 07:14:16'),
(365, 292, '1552461260_1.jpg', 4, NULL, '2019-03-13 07:14:20', '2019-03-13 07:14:20'),
(366, 292, '1552461266_1.jpg', 4, NULL, '2019-03-13 07:14:26', '2019-03-13 07:14:26'),
(367, 292, '1552461270_1.jpg', 4, NULL, '2019-03-13 07:14:30', '2019-03-13 07:14:30'),
(368, 395, '1552461541_1.jpg', 4, NULL, '2019-03-13 07:19:01', '2019-03-13 07:19:01'),
(369, 395, '1552461546_1.jpg', 4, NULL, '2019-03-13 07:19:06', '2019-03-13 07:19:06'),
(370, 395, '1552461554_1.jpg', 4, NULL, '2019-03-13 07:19:14', '2019-03-13 07:19:14'),
(371, 395, '1552461559_1.jpg', 4, NULL, '2019-03-13 07:19:19', '2019-03-13 07:19:19'),
(372, 395, '1552461566_1.jpg', 4, NULL, '2019-03-13 07:19:26', '2019-03-13 07:19:26'),
(373, 395, '1552461574_1.jpg', 4, NULL, '2019-03-13 07:19:34', '2019-03-13 07:19:34'),
(374, 308, '1552461939_1.jpg', 4, NULL, '2019-03-13 07:25:39', '2019-03-13 07:25:39'),
(375, 308, '1552461945_1.jpg', 4, NULL, '2019-03-13 07:25:45', '2019-03-13 07:25:45'),
(376, 308, '1552461950_1.jpg', 4, NULL, '2019-03-13 07:25:50', '2019-03-13 07:25:50'),
(377, 308, '1552461956_1.jpg', 4, NULL, '2019-03-13 07:25:56', '2019-03-13 07:25:56'),
(378, 308, '1552461962_1.jpg', 4, NULL, '2019-03-13 07:26:02', '2019-03-13 07:26:02'),
(379, 308, '1552461969_1.jpg', 4, NULL, '2019-03-13 07:26:09', '2019-03-13 07:26:09'),
(380, 308, '1552461974_1.jpg', 4, NULL, '2019-03-13 07:26:14', '2019-03-13 07:26:14'),
(381, 303, '1552463345_1.jpg', 4, NULL, '2019-03-13 07:49:05', '2019-03-13 07:49:05'),
(382, 303, '1552463351_1.jpg', 4, NULL, '2019-03-13 07:49:11', '2019-03-13 07:49:11'),
(383, 303, '1552463357_1.jpg', 4, NULL, '2019-03-13 07:49:17', '2019-03-13 07:49:17'),
(384, 303, '1552463363_1.jpg', 4, NULL, '2019-03-13 07:49:23', '2019-03-13 07:49:23'),
(385, 303, '1552463368_1.jpg', 4, NULL, '2019-03-13 07:49:28', '2019-03-13 07:49:28'),
(386, 303, '1552463378_1.jpg', 4, NULL, '2019-03-13 07:49:38', '2019-03-13 07:49:38'),
(387, 224, '1552463780_1.jpg', 4, NULL, '2019-03-13 07:56:20', '2019-03-13 07:56:20'),
(388, 224, '1552463786_1.jpg', 4, NULL, '2019-03-13 07:56:26', '2019-03-13 07:56:26'),
(389, 224, '1552463791_1.jpg', 4, NULL, '2019-03-13 07:56:31', '2019-03-13 07:56:31'),
(390, 224, '1552463796_1.jpg', 4, NULL, '2019-03-13 07:56:36', '2019-03-13 07:56:36'),
(391, 224, '1552463801_1.jpg', 4, NULL, '2019-03-13 07:56:41', '2019-03-13 07:56:41'),
(392, 224, '1552463809_1.jpg', 4, NULL, '2019-03-13 07:56:49', '2019-03-13 07:56:49'),
(393, 225, '1552464582_1.jpg', 4, NULL, '2019-03-13 08:09:42', '2019-03-13 08:09:42'),
(394, 225, '1552464592_1.jpg', 4, NULL, '2019-03-13 08:09:52', '2019-03-13 08:09:52'),
(395, 225, '1552464598_1.jpg', 4, NULL, '2019-03-13 08:09:58', '2019-03-13 08:09:58'),
(396, 225, '1552464604_1.jpg', 4, NULL, '2019-03-13 08:10:04', '2019-03-13 08:10:04'),
(397, 225, '1552464608_1.jpg', 4, NULL, '2019-03-13 08:10:08', '2019-03-13 08:10:08'),
(398, 225, '1552464614_1.jpg', 4, NULL, '2019-03-13 08:10:14', '2019-03-13 08:10:14'),
(399, 227, '1552464934_1.jpg', 4, NULL, '2019-03-13 08:15:34', '2019-03-13 08:15:34'),
(400, 227, '1552464976_1.jpg', 4, NULL, '2019-03-13 08:16:16', '2019-03-13 08:16:16'),
(401, 227, '1552464981_1.jpg', 4, NULL, '2019-03-13 08:16:21', '2019-03-13 08:16:21'),
(402, 227, '1552464986_1.jpg', 4, NULL, '2019-03-13 08:16:26', '2019-03-13 08:16:26'),
(403, 227, '1552464990_1.jpg', 4, NULL, '2019-03-13 08:16:30', '2019-03-13 08:16:30'),
(404, 227, '1552464995_1.jpg', 4, NULL, '2019-03-13 08:16:35', '2019-03-13 08:16:35'),
(405, 267, '1552465221_1.jpg', 4, NULL, '2019-03-13 08:20:21', '2019-03-13 08:20:21'),
(406, 267, '1552465227_1.jpg', 4, NULL, '2019-03-13 08:20:27', '2019-03-13 08:20:27'),
(407, 267, '1552465232_1.jpg', 4, NULL, '2019-03-13 08:20:32', '2019-03-13 08:20:32'),
(408, 267, '1552465238_1.jpg', 4, NULL, '2019-03-13 08:20:38', '2019-03-13 08:20:38'),
(409, 267, '1552465243_1.jpg', 4, NULL, '2019-03-13 08:20:43', '2019-03-13 08:20:43'),
(410, 267, '1552465250_1.jpg', 4, NULL, '2019-03-13 08:20:50', '2019-03-13 08:20:50'),
(411, 297, '1552465514_1.jpg', 4, NULL, '2019-03-13 08:25:14', '2019-03-13 08:25:14'),
(412, 297, '1552465521_1.jpg', 4, NULL, '2019-03-13 08:25:21', '2019-03-13 08:25:21'),
(413, 297, '1552465526_1.jpg', 4, NULL, '2019-03-13 08:25:26', '2019-03-13 08:25:26'),
(414, 297, '1552465531_1.jpg', 4, NULL, '2019-03-13 08:25:31', '2019-03-13 08:25:31'),
(415, 297, '1552465536_1.jpg', 4, NULL, '2019-03-13 08:25:36', '2019-03-13 08:25:36'),
(416, 297, '1552465541_1.jpg', 4, NULL, '2019-03-13 08:25:41', '2019-03-13 08:25:41'),
(417, 297, '1552465545_1.jpg', 4, NULL, '2019-03-13 08:25:45', '2019-03-13 08:25:45'),
(418, 370, '1552479890_1.jpg', 4, NULL, '2019-03-13 12:24:50', '2019-03-13 12:24:50'),
(419, 370, '1552479913_1.jpg', 4, NULL, '2019-03-13 12:25:13', '2019-03-13 12:25:13'),
(420, 370, '1552479918_1.jpg', 4, NULL, '2019-03-13 12:25:18', '2019-03-13 12:25:18'),
(421, 370, '1552479923_1.jpg', 4, NULL, '2019-03-13 12:25:23', '2019-03-13 12:25:23'),
(422, 370, '1552479928_1.jpg', 4, NULL, '2019-03-13 12:25:28', '2019-03-13 12:25:28'),
(423, 370, '1552479932_1.jpg', 4, NULL, '2019-03-13 12:25:32', '2019-03-13 12:25:32'),
(424, 435, '1552480159_1.jpg', 4, NULL, '2019-03-13 12:29:19', '2019-03-13 12:29:19'),
(425, 435, '1552480166_1.jpg', 4, NULL, '2019-03-13 12:29:26', '2019-03-13 12:29:26'),
(426, 435, '1552480171_1.jpg', 4, NULL, '2019-03-13 12:29:31', '2019-03-13 12:29:31'),
(427, 435, '1552480177_1.jpg', 4, NULL, '2019-03-13 12:29:37', '2019-03-13 12:29:37'),
(428, 435, '1552480182_1.jpg', 4, NULL, '2019-03-13 12:29:42', '2019-03-13 12:29:42'),
(429, 253, '1552480478_1.jpg', 4, NULL, '2019-03-13 12:34:38', '2019-03-13 12:34:38'),
(430, 253, '1552480483_1.jpg', 4, NULL, '2019-03-13 12:34:43', '2019-03-13 12:34:43'),
(431, 253, '1552480488_1.jpg', 4, NULL, '2019-03-13 12:34:48', '2019-03-13 12:34:48'),
(432, 253, '1552480493_1.jpg', 4, NULL, '2019-03-13 12:34:53', '2019-03-13 12:34:53'),
(433, 253, '1552480499_1.jpg', 4, NULL, '2019-03-13 12:34:59', '2019-03-13 12:34:59'),
(434, 253, '1552480505_1.jpg', 4, NULL, '2019-03-13 12:35:05', '2019-03-13 12:35:05'),
(435, 293, '1552481012_1.jpg', 4, NULL, '2019-03-13 12:43:32', '2019-03-13 12:43:32'),
(436, 293, '1552481020_1.jpg', 4, NULL, '2019-03-13 12:43:40', '2019-03-13 12:43:40'),
(437, 293, '1552481026_1.jpg', 4, NULL, '2019-03-13 12:43:46', '2019-03-13 12:43:46'),
(438, 293, '1552481032_1.jpg', 4, NULL, '2019-03-13 12:43:52', '2019-03-13 12:43:52'),
(439, 293, '1552481038_1.jpg', 4, NULL, '2019-03-13 12:43:58', '2019-03-13 12:43:58'),
(440, 293, '1552481046_1.jpg', 4, NULL, '2019-03-13 12:44:06', '2019-03-13 12:44:06'),
(441, 293, '1552481052_1.jpg', 4, NULL, '2019-03-13 12:44:12', '2019-03-13 12:44:12'),
(442, 382, '1552483239_1.jpg', 4, NULL, '2019-03-13 13:20:39', '2019-03-13 13:20:39'),
(443, 382, '1552483245_1.jpg', 4, NULL, '2019-03-13 13:20:45', '2019-03-13 13:20:45'),
(444, 382, '1552483250_1.jpg', 4, NULL, '2019-03-13 13:20:50', '2019-03-13 13:20:50'),
(445, 382, '1552483256_1.jpg', 4, NULL, '2019-03-13 13:20:56', '2019-03-13 13:20:56'),
(446, 382, '1552483262_1.jpg', 4, NULL, '2019-03-13 13:21:02', '2019-03-13 13:21:02'),
(447, 382, '1552483268_1.jpg', 4, NULL, '2019-03-13 13:21:08', '2019-03-13 13:21:08'),
(448, 316, '1552483741_1.jpg', 4, NULL, '2019-03-13 13:29:01', '2019-03-13 13:29:01'),
(449, 316, '1552483747_1.jpg', 4, NULL, '2019-03-13 13:29:07', '2019-03-13 13:29:07'),
(450, 316, '1552483752_1.jpg', 4, NULL, '2019-03-13 13:29:12', '2019-03-13 13:29:12'),
(451, 316, '1552483756_1.jpg', 4, NULL, '2019-03-13 13:29:16', '2019-03-13 13:29:16'),
(452, 316, '1552483761_1.jpg', 4, NULL, '2019-03-13 13:29:21', '2019-03-13 13:29:21'),
(453, 316, '1552483778_1.jpg', 4, NULL, '2019-03-13 13:29:38', '2019-03-13 13:29:38'),
(454, 316, '1552483789_1.jpg', 4, NULL, '2019-03-13 13:29:49', '2019-03-13 13:29:49'),
(455, 317, '1552484094_1.jpg', 4, NULL, '2019-03-13 13:34:54', '2019-03-13 13:34:54'),
(456, 317, '1552484099_1.jpg', 4, NULL, '2019-03-13 13:34:59', '2019-03-13 13:34:59'),
(457, 317, '1552484105_1.jpg', 4, NULL, '2019-03-13 13:35:05', '2019-03-13 13:35:05'),
(458, 317, '1552484109_1.jpg', 4, NULL, '2019-03-13 13:35:09', '2019-03-13 13:35:09'),
(459, 317, '1552484114_1.jpg', 4, NULL, '2019-03-13 13:35:14', '2019-03-13 13:35:14'),
(460, 317, '1552484120_1.jpg', 4, NULL, '2019-03-13 13:35:20', '2019-03-13 13:35:20'),
(461, 327, '1552484285_1.jpg', 4, NULL, '2019-03-13 13:38:05', '2019-03-13 13:38:05'),
(462, 327, '1552484291_1.jpg', 4, NULL, '2019-03-13 13:38:11', '2019-03-13 13:38:11'),
(463, 327, '1552484296_1.jpg', 4, NULL, '2019-03-13 13:38:16', '2019-03-13 13:38:16'),
(464, 327, '1552484301_1.jpg', 4, NULL, '2019-03-13 13:38:21', '2019-03-13 13:38:21'),
(465, 202, '1552485119_1.jpg', 4, NULL, '2019-03-13 13:51:59', '2019-03-13 13:51:59'),
(466, 202, '1552485125_1.jpg', 4, NULL, '2019-03-13 13:52:05', '2019-03-13 13:52:05'),
(467, 202, '1552485131_1.jpg', 4, NULL, '2019-03-13 13:52:11', '2019-03-13 13:52:11'),
(468, 202, '1552485136_1.jpg', 4, NULL, '2019-03-13 13:52:16', '2019-03-13 13:52:16'),
(469, 202, '1552485141_1.jpg', 4, NULL, '2019-03-13 13:52:21', '2019-03-13 13:52:21'),
(470, 202, '1552485147_1.jpg', 4, NULL, '2019-03-13 13:52:27', '2019-03-13 13:52:27'),
(471, 220, '1552542536_1.jpg', 4, NULL, '2019-03-14 05:48:56', '2019-03-14 05:48:56'),
(472, 220, '1552542542_1.jpg', 4, NULL, '2019-03-14 05:49:02', '2019-03-14 05:49:02'),
(473, 220, '1552542547_1.jpg', 4, NULL, '2019-03-14 05:49:07', '2019-03-14 05:49:07'),
(474, 220, '1552542551_1.jpg', 4, NULL, '2019-03-14 05:49:11', '2019-03-14 05:49:11'),
(475, 220, '1552542557_1.jpg', 4, NULL, '2019-03-14 05:49:17', '2019-03-14 05:49:17'),
(476, 220, '1552542562_1.jpg', 4, NULL, '2019-03-14 05:49:22', '2019-03-14 05:49:22'),
(477, 233, '1552543536_1.jpg', 4, NULL, '2019-03-14 06:05:36', '2019-03-14 06:05:36'),
(478, 233, '1552543541_1.jpg', 4, NULL, '2019-03-14 06:05:41', '2019-03-14 06:05:41'),
(479, 233, '1552543545_1.jpg', 4, NULL, '2019-03-14 06:05:45', '2019-03-14 06:05:45'),
(480, 233, '1552543550_1.jpg', 4, NULL, '2019-03-14 06:05:50', '2019-03-14 06:05:50'),
(481, 233, '1552543555_1.jpg', 4, NULL, '2019-03-14 06:05:55', '2019-03-14 06:05:55'),
(482, 377, '1552552655_1.jpg', 4, NULL, '2019-03-14 08:37:35', '2019-03-14 08:37:35'),
(483, 377, '1552552683_1.jpg', 4, NULL, '2019-03-14 08:38:03', '2019-03-14 08:38:03'),
(484, 377, '1552552690_1.jpg', 4, NULL, '2019-03-14 08:38:10', '2019-03-14 08:38:10'),
(485, 377, '1552552695_1.jpg', 4, NULL, '2019-03-14 08:38:15', '2019-03-14 08:38:15'),
(486, 377, '1552552700_1.jpg', 4, NULL, '2019-03-14 08:38:20', '2019-03-14 08:38:20'),
(487, 377, '1552552707_1.jpg', 4, NULL, '2019-03-14 08:38:27', '2019-03-14 08:38:27'),
(488, 388, '1552553762_1.jpg', 4, NULL, '2019-03-14 08:56:02', '2019-03-14 08:56:02'),
(489, 388, '1552553768_1.jpg', 4, NULL, '2019-03-14 08:56:08', '2019-03-14 08:56:08'),
(490, 388, '1552553779_1.jpg', 4, NULL, '2019-03-14 08:56:19', '2019-03-14 08:56:19'),
(491, 388, '1552553786_1.jpg', 4, NULL, '2019-03-14 08:56:26', '2019-03-14 08:56:26'),
(492, 388, '1552553792_1.jpg', 4, NULL, '2019-03-14 08:56:32', '2019-03-14 08:56:32'),
(493, 388, '1552553806_1.jpg', 4, NULL, '2019-03-14 08:56:46', '2019-03-14 08:56:46'),
(494, 440, '1552554618_1.jpg', 4, NULL, '2019-03-14 09:10:18', '2019-03-14 09:10:18'),
(495, 440, '1552554625_1.jpg', 4, NULL, '2019-03-14 09:10:25', '2019-03-14 09:10:25'),
(496, 440, '1552554631_1.jpg', 4, NULL, '2019-03-14 09:10:31', '2019-03-14 09:10:31'),
(497, 440, '1552554637_1.jpg', 4, NULL, '2019-03-14 09:10:37', '2019-03-14 09:10:37'),
(498, 440, '1552554643_1.jpg', 4, NULL, '2019-03-14 09:10:43', '2019-03-14 09:10:43'),
(499, 440, '1552554655_1.jpg', 4, NULL, '2019-03-14 09:10:55', '2019-03-14 09:10:55'),
(500, 192, '1552554911_1.jpg', 4, NULL, '2019-03-14 09:15:11', '2019-03-14 09:15:11'),
(501, 192, '1552554917_1.jpg', 4, NULL, '2019-03-14 09:15:17', '2019-03-14 09:15:17'),
(502, 192, '1552554922_1.jpg', 4, NULL, '2019-03-14 09:15:22', '2019-03-14 09:15:22'),
(503, 192, '1552554928_1.jpg', 4, NULL, '2019-03-14 09:15:28', '2019-03-14 09:15:28'),
(504, 192, '1552554934_1.jpg', 4, NULL, '2019-03-14 09:15:34', '2019-03-14 09:15:34'),
(505, 192, '1552554940_1.jpg', 4, NULL, '2019-03-14 09:15:40', '2019-03-14 09:15:40'),
(506, 194, '1552555370_1.jpg', 4, NULL, '2019-03-14 09:22:50', '2019-03-14 09:22:50'),
(507, 194, '1552555377_1.jpg', 4, NULL, '2019-03-14 09:22:57', '2019-03-14 09:22:57'),
(508, 194, '1552555383_1.jpg', 4, NULL, '2019-03-14 09:23:03', '2019-03-14 09:23:03'),
(509, 194, '1552555389_1.jpg', 4, NULL, '2019-03-14 09:23:09', '2019-03-14 09:23:09'),
(510, 194, '1552555396_1.jpg', 4, NULL, '2019-03-14 09:23:16', '2019-03-14 09:23:16'),
(511, 194, '1552555405_1.jpg', 4, NULL, '2019-03-14 09:23:25', '2019-03-14 09:23:25'),
(512, 197, '1552555670_1.jpg', 4, NULL, '2019-03-14 09:27:50', '2019-03-14 09:27:50'),
(513, 197, '1552555676_1.jpg', 4, NULL, '2019-03-14 09:27:56', '2019-03-14 09:27:56'),
(514, 197, '1552555681_1.jpg', 4, NULL, '2019-03-14 09:28:01', '2019-03-14 09:28:01'),
(515, 197, '1552555686_1.jpg', 4, NULL, '2019-03-14 09:28:06', '2019-03-14 09:28:06'),
(516, 197, '1552555691_1.jpg', 4, NULL, '2019-03-14 09:28:11', '2019-03-14 09:28:11'),
(517, 197, '1552555696_1.jpg', 4, NULL, '2019-03-14 09:28:16', '2019-03-14 09:28:16'),
(518, 197, '1552555701_1.jpg', 4, NULL, '2019-03-14 09:28:21', '2019-03-14 09:28:21'),
(519, 286, '1552560387_1.jpg', 4, NULL, '2019-03-14 10:46:27', '2019-03-14 10:46:27'),
(520, 406, '1552560784_1.jpg', 4, NULL, '2019-03-14 10:53:04', '2019-03-14 10:53:04'),
(521, 406, '1552560795_1.jpg', 4, NULL, '2019-03-14 10:53:15', '2019-03-14 10:53:15'),
(522, 406, '1552560805_1.jpg', 4, NULL, '2019-03-14 10:53:25', '2019-03-14 10:53:25'),
(523, 406, '1552560811_1.jpg', 4, NULL, '2019-03-14 10:53:31', '2019-03-14 10:53:31'),
(524, 406, '1552560819_1.jpg', 4, NULL, '2019-03-14 10:53:39', '2019-03-14 10:53:39'),
(525, 406, '1552560825_1.jpg', 4, NULL, '2019-03-14 10:53:45', '2019-03-14 10:53:45'),
(526, 406, '1552560831_1.jpg', 4, NULL, '2019-03-14 10:53:51', '2019-03-14 10:53:51'),
(527, 285, '1552561125_1.jpg', 4, NULL, '2019-03-14 10:58:45', '2019-03-14 10:58:45'),
(528, 285, '1552561130_1.jpg', 4, NULL, '2019-03-14 10:58:50', '2019-03-14 10:58:50'),
(529, 285, '1552561135_1.jpg', 4, NULL, '2019-03-14 10:58:55', '2019-03-14 10:58:55'),
(530, 285, '1552561140_1.jpg', 4, NULL, '2019-03-14 10:59:00', '2019-03-14 10:59:00'),
(531, 285, '1552561145_1.jpg', 4, NULL, '2019-03-14 10:59:05', '2019-03-14 10:59:05'),
(532, 285, '1552561150_1.jpg', 4, NULL, '2019-03-14 10:59:10', '2019-03-14 10:59:10'),
(533, 373, '1552733795_1.jpg', 4, NULL, '2019-03-16 10:56:35', '2019-03-16 10:56:35'),
(534, 373, '1552733811_1.jpg', 4, NULL, '2019-03-16 10:56:51', '2019-03-16 10:56:51'),
(535, 373, '1552733817_1.jpg', 4, NULL, '2019-03-16 10:56:57', '2019-03-16 10:56:57'),
(536, 373, '1552733822_1.jpg', 4, NULL, '2019-03-16 10:57:02', '2019-03-16 10:57:02'),
(537, 373, '1552733829_1.jpg', 4, NULL, '2019-03-16 10:57:09', '2019-03-16 10:57:09'),
(538, 373, '1552733835_1.jpg', 4, NULL, '2019-03-16 10:57:15', '2019-03-16 10:57:15'),
(539, 373, '1552733844_1.jpg', 4, NULL, '2019-03-16 10:57:24', '2019-03-16 10:57:24'),
(540, 249, '1552734879_1.jpg', 4, NULL, '2019-03-16 11:14:39', '2019-03-16 11:14:39'),
(541, 249, '1552734885_1.jpg', 4, NULL, '2019-03-16 11:14:45', '2019-03-16 11:14:45'),
(542, 249, '1552734891_1.jpg', 4, NULL, '2019-03-16 11:14:51', '2019-03-16 11:14:51'),
(543, 249, '1552734896_1.jpg', 4, NULL, '2019-03-16 11:14:56', '2019-03-16 11:14:56'),
(544, 249, '1552734901_1.jpg', 4, NULL, '2019-03-16 11:15:01', '2019-03-16 11:15:01'),
(545, 249, '1552734907_1.jpg', 4, NULL, '2019-03-16 11:15:07', '2019-03-16 11:15:07'),
(546, 249, '1552734912_1.jpg', 4, NULL, '2019-03-16 11:15:12', '2019-03-16 11:15:12'),
(547, 385, '1552735265_1.jpg', 4, NULL, '2019-03-16 11:21:05', '2019-03-16 11:21:05'),
(548, 385, '1552735271_1.jpg', 4, NULL, '2019-03-16 11:21:11', '2019-03-16 11:21:11'),
(549, 385, '1552735276_1.jpg', 4, NULL, '2019-03-16 11:21:16', '2019-03-16 11:21:16'),
(550, 385, '1552735282_1.jpg', 4, NULL, '2019-03-16 11:21:22', '2019-03-16 11:21:22'),
(551, 385, '1552735288_1.jpg', 4, NULL, '2019-03-16 11:21:28', '2019-03-16 11:21:28'),
(552, 385, '1552735293_1.jpg', 4, NULL, '2019-03-16 11:21:33', '2019-03-16 11:21:33'),
(553, 385, '1552735298_1.jpg', 4, NULL, '2019-03-16 11:21:38', '2019-03-16 11:21:38'),
(554, 385, '1552735303_1.jpg', 4, NULL, '2019-03-16 11:21:43', '2019-03-16 11:21:43'),
(555, 385, '1552735309_1.jpg', 4, NULL, '2019-03-16 11:21:49', '2019-03-16 11:21:49'),
(556, 250, '1552735643_1.jpg', 4, NULL, '2019-03-16 11:27:23', '2019-03-16 11:27:23'),
(557, 250, '1552735648_1.jpg', 4, NULL, '2019-03-16 11:27:28', '2019-03-16 11:27:28'),
(558, 250, '1552735654_1.jpg', 4, NULL, '2019-03-16 11:27:34', '2019-03-16 11:27:34'),
(559, 250, '1552735660_1.jpg', 4, NULL, '2019-03-16 11:27:40', '2019-03-16 11:27:40'),
(560, 250, '1552735666_1.jpg', 4, NULL, '2019-03-16 11:27:46', '2019-03-16 11:27:46'),
(561, 250, '1552735672_1.jpg', 4, NULL, '2019-03-16 11:27:52', '2019-03-16 11:27:52'),
(562, 250, '1552735678_1.jpg', 4, NULL, '2019-03-16 11:27:58', '2019-03-16 11:27:58'),
(563, 288, '1552736108_1.jpg', 4, NULL, '2019-03-16 11:35:08', '2019-03-16 11:35:08'),
(564, 288, '1552736126_1.jpg', 4, NULL, '2019-03-16 11:35:26', '2019-03-16 11:35:26'),
(565, 288, '1552736136_1.jpg', 4, NULL, '2019-03-16 11:35:36', '2019-03-16 11:35:36'),
(566, 288, '1552736142_1.jpg', 4, NULL, '2019-03-16 11:35:42', '2019-03-16 11:35:42'),
(567, 288, '1552736147_1.jpg', 4, NULL, '2019-03-16 11:35:47', '2019-03-16 11:35:47'),
(568, 288, '1552736154_1.jpg', 4, NULL, '2019-03-16 11:35:54', '2019-03-16 11:35:54'),
(569, 386, '1552736381_1.jpg', 4, NULL, '2019-03-16 11:39:41', '2019-03-16 11:39:41'),
(570, 386, '1552736386_1.jpg', 4, NULL, '2019-03-16 11:39:46', '2019-03-16 11:39:46'),
(571, 386, '1552736393_1.jpg', 4, NULL, '2019-03-16 11:39:53', '2019-03-16 11:39:53'),
(572, 386, '1552736399_1.jpg', 4, NULL, '2019-03-16 11:39:59', '2019-03-16 11:39:59'),
(573, 386, '1552736405_1.jpg', 4, NULL, '2019-03-16 11:40:05', '2019-03-16 11:40:05'),
(574, 386, '1552736410_1.jpg', 4, NULL, '2019-03-16 11:40:10', '2019-03-16 11:40:10'),
(575, 386, '1552736453_1.jpg', 4, NULL, '2019-03-16 11:40:53', '2019-03-16 11:40:53'),
(576, 270, '1552910238_1.jpg', 4, NULL, '2019-03-18 11:57:18', '2019-03-18 11:57:18'),
(577, 270, '1552910244_1.jpg', 4, NULL, '2019-03-18 11:57:24', '2019-03-18 11:57:24'),
(578, 270, '1552910250_1.jpg', 4, NULL, '2019-03-18 11:57:30', '2019-03-18 11:57:30'),
(579, 270, '1552910254_1.jpg', 4, NULL, '2019-03-18 11:57:34', '2019-03-18 11:57:34'),
(580, 270, '1552910260_1.jpg', 4, NULL, '2019-03-18 11:57:40', '2019-03-18 11:57:40'),
(581, 270, '1552910265_1.jpg', 4, NULL, '2019-03-18 11:57:45', '2019-03-18 11:57:45'),
(582, 399, '1552910630_1.jpg', 4, NULL, '2019-03-18 12:03:50', '2019-03-18 12:03:50'),
(583, 399, '1552910637_1.jpg', 4, NULL, '2019-03-18 12:03:57', '2019-03-18 12:03:57'),
(584, 399, '1552910656_1.jpg', 4, NULL, '2019-03-18 12:04:16', '2019-03-18 12:04:16'),
(585, 399, '1552910662_1.jpg', 4, NULL, '2019-03-18 12:04:22', '2019-03-18 12:04:22'),
(586, 399, '1552910669_1.jpg', 4, NULL, '2019-03-18 12:04:29', '2019-03-18 12:04:29'),
(587, 399, '1552910675_1.jpg', 4, NULL, '2019-03-18 12:04:35', '2019-03-18 12:04:35'),
(588, 247, '1552911005_1.jpg', 4, NULL, '2019-03-18 12:10:05', '2019-03-18 12:10:05'),
(589, 247, '1552911015_1.jpg', 4, NULL, '2019-03-18 12:10:15', '2019-03-18 12:10:15'),
(590, 247, '1552911021_1.jpg', 4, NULL, '2019-03-18 12:10:21', '2019-03-18 12:10:21'),
(591, 247, '1552911027_1.jpg', 4, NULL, '2019-03-18 12:10:27', '2019-03-18 12:10:27'),
(592, 247, '1552911032_1.jpg', 4, NULL, '2019-03-18 12:10:32', '2019-03-18 12:10:32'),
(593, 247, '1552911037_1.jpg', 4, NULL, '2019-03-18 12:10:37', '2019-03-18 12:10:37'),
(594, 205, '1552911671_1.jpg', 4, NULL, '2019-03-18 12:21:11', '2019-03-18 12:21:11'),
(595, 205, '1552911678_1.jpg', 4, NULL, '2019-03-18 12:21:18', '2019-03-18 12:21:18'),
(596, 205, '1552911683_1.jpg', 4, NULL, '2019-03-18 12:21:23', '2019-03-18 12:21:23'),
(597, 205, '1552911689_1.jpg', 4, NULL, '2019-03-18 12:21:29', '2019-03-18 12:21:29'),
(598, 205, '1552911694_1.jpg', 4, NULL, '2019-03-18 12:21:34', '2019-03-18 12:21:34'),
(599, 205, '1552911699_1.jpg', 4, NULL, '2019-03-18 12:21:39', '2019-03-18 12:21:39'),
(600, 205, '1552911705_1.jpg', 4, NULL, '2019-03-18 12:21:45', '2019-03-18 12:21:45'),
(601, 205, '1552911714_1.jpg', 4, NULL, '2019-03-18 12:21:54', '2019-03-18 12:21:54'),
(602, 74, '1552913484_1.jpg', 4, NULL, '2019-03-18 12:51:24', '2019-03-18 12:51:24'),
(603, 74, '1552913493_1.jpg', 4, NULL, '2019-03-18 12:51:33', '2019-03-18 12:51:33'),
(604, 74, '1552913498_1.jpg', 4, NULL, '2019-03-18 12:51:38', '2019-03-18 12:51:38'),
(605, 74, '1552913503_1.jpg', 4, NULL, '2019-03-18 12:51:43', '2019-03-18 12:51:43'),
(606, 74, '1552913509_1.jpg', 4, NULL, '2019-03-18 12:51:49', '2019-03-18 12:51:49'),
(607, 434, '1552917276_1.jpg', 4, NULL, '2019-03-18 13:54:36', '2019-03-18 13:54:36'),
(608, 434, '1552917281_1.jpg', 4, NULL, '2019-03-18 13:54:41', '2019-03-18 13:54:41'),
(609, 434, '1552917315_1.jpg', 4, NULL, '2019-03-18 13:55:15', '2019-03-18 13:55:15'),
(610, 434, '1552917321_1.jpg', 4, NULL, '2019-03-18 13:55:21', '2019-03-18 13:55:21'),
(611, 434, '1552917325_1.jpg', 4, NULL, '2019-03-18 13:55:25', '2019-03-18 13:55:25'),
(612, 434, '1552917330_1.jpg', 4, NULL, '2019-03-18 13:55:30', '2019-03-18 13:55:30'),
(613, 206, '1552974010_1.jpg', 4, NULL, '2019-03-19 05:40:10', '2019-03-19 05:40:10'),
(614, 206, '1552974016_1.jpg', 4, NULL, '2019-03-19 05:40:16', '2019-03-19 05:40:16'),
(615, 206, '1552974059_1.jpg', 4, NULL, '2019-03-19 05:40:59', '2019-03-19 05:40:59'),
(616, 206, '1552974064_1.jpg', 4, NULL, '2019-03-19 05:41:04', '2019-03-19 05:41:04'),
(617, 206, '1552974069_1.jpg', 4, NULL, '2019-03-19 05:41:09', '2019-03-19 05:41:09'),
(618, 206, '1552974075_1.jpg', 4, NULL, '2019-03-19 05:41:15', '2019-03-19 05:41:15'),
(619, 381, '1552974900_1.jpg', 4, NULL, '2019-03-19 05:55:00', '2019-03-19 05:55:00'),
(620, 381, '1552974906_1.jpg', 4, NULL, '2019-03-19 05:55:06', '2019-03-19 05:55:06'),
(621, 381, '1552974913_1.jpg', 4, NULL, '2019-03-19 05:55:13', '2019-03-19 05:55:13'),
(622, 381, '1552974919_1.jpg', 4, NULL, '2019-03-19 05:55:19', '2019-03-19 05:55:19'),
(623, 381, '1552974924_1.jpg', 4, NULL, '2019-03-19 05:55:24', '2019-03-19 05:55:24'),
(624, 381, '1552974929_1.jpg', 4, NULL, '2019-03-19 05:55:29', '2019-03-19 05:55:29'),
(625, 374, '1552975595_1.jpg', 4, NULL, '2019-03-19 06:06:35', '2019-03-19 06:06:35'),
(626, 374, '1552975600_1.jpg', 4, NULL, '2019-03-19 06:06:40', '2019-03-19 06:06:40'),
(627, 374, '1552975605_1.jpg', 4, NULL, '2019-03-19 06:06:45', '2019-03-19 06:06:45'),
(628, 374, '1552975612_1.jpg', 4, NULL, '2019-03-19 06:06:52', '2019-03-19 06:06:52'),
(629, 374, '1552975617_1.jpg', 4, NULL, '2019-03-19 06:06:57', '2019-03-19 06:06:57'),
(630, 374, '1552975623_1.jpg', 4, NULL, '2019-03-19 06:07:03', '2019-03-19 06:07:03'),
(631, 374, '1552975629_1.jpg', 4, NULL, '2019-03-19 06:07:09', '2019-03-19 06:07:09'),
(632, 452, '1552977495_1.jpg', 4, NULL, '2019-03-19 06:38:15', '2019-03-19 06:38:15'),
(633, 452, '1552977500_1.jpg', 4, NULL, '2019-03-19 06:38:20', '2019-03-19 06:38:20'),
(634, 452, '1552977505_1.jpg', 4, NULL, '2019-03-19 06:38:25', '2019-03-19 06:38:25'),
(635, 452, '1552977510_1.jpg', 4, NULL, '2019-03-19 06:38:30', '2019-03-19 06:38:30'),
(636, 452, '1552977515_1.jpg', 4, NULL, '2019-03-19 06:38:35', '2019-03-19 06:38:35'),
(637, 452, '1552977520_1.jpg', 4, NULL, '2019-03-19 06:38:40', '2019-03-19 06:38:40'),
(638, 452, '1552977525_1.jpg', 4, NULL, '2019-03-19 06:38:45', '2019-03-19 06:38:45'),
(639, 452, '1552977530_1.jpg', 4, NULL, '2019-03-19 06:38:50', '2019-03-19 06:38:50'),
(640, 64, '1552977870_1.jpg', 4, NULL, '2019-03-19 06:44:30', '2019-03-19 06:44:30'),
(641, 64, '1552977876_1.jpg', 4, NULL, '2019-03-19 06:44:36', '2019-03-19 06:44:36'),
(642, 64, '1552977881_1.jpg', 4, NULL, '2019-03-19 06:44:41', '2019-03-19 06:44:41'),
(643, 64, '1552977886_1.jpg', 4, NULL, '2019-03-19 06:44:46', '2019-03-19 06:44:46'),
(644, 64, '1552977891_1.jpg', 4, NULL, '2019-03-19 06:44:51', '2019-03-19 06:44:51'),
(645, 64, '1552977896_1.jpg', 4, NULL, '2019-03-19 06:44:56', '2019-03-19 06:44:56'),
(646, 259, '1552978287_1.jpg', 4, NULL, '2019-03-19 06:51:27', '2019-03-19 06:51:27'),
(647, 259, '1552978293_1.jpg', 4, NULL, '2019-03-19 06:51:33', '2019-03-19 06:51:33'),
(648, 259, '1552978299_1.jpg', 4, NULL, '2019-03-19 06:51:39', '2019-03-19 06:51:39'),
(649, 259, '1552978306_1.jpg', 4, NULL, '2019-03-19 06:51:46', '2019-03-19 06:51:46'),
(650, 259, '1552978311_1.jpg', 4, NULL, '2019-03-19 06:51:51', '2019-03-19 06:51:51'),
(651, 259, '1552978316_1.jpg', 4, NULL, '2019-03-19 06:51:56', '2019-03-19 06:51:56'),
(652, 259, '1552978322_1.jpg', 4, NULL, '2019-03-19 06:52:02', '2019-03-19 06:52:02'),
(653, 436, '1552978812_1.jpg', 4, NULL, '2019-03-19 07:00:12', '2019-03-19 07:00:12'),
(654, 436, '1552978818_1.jpg', 4, NULL, '2019-03-19 07:00:18', '2019-03-19 07:00:18'),
(655, 436, '1552978825_1.jpg', 4, NULL, '2019-03-19 07:00:25', '2019-03-19 07:00:25'),
(656, 436, '1552978832_1.jpg', 4, NULL, '2019-03-19 07:00:32', '2019-03-19 07:00:32'),
(657, 436, '1552978838_1.jpg', 4, NULL, '2019-03-19 07:00:38', '2019-03-19 07:00:38'),
(658, 436, '1552978844_1.jpg', 4, NULL, '2019-03-19 07:00:44', '2019-03-19 07:00:44'),
(659, 436, '1552978849_1.jpg', 4, NULL, '2019-03-19 07:00:49', '2019-03-19 07:00:49'),
(660, 173, '1552978958_1.jpg', 4, NULL, '2019-03-19 07:02:38', '2019-03-19 07:02:38'),
(661, 173, '1552979238_1.jpg', 4, NULL, '2019-03-19 07:07:18', '2019-03-19 07:07:18'),
(662, 173, '1552979244_1.jpg', 4, NULL, '2019-03-19 07:07:24', '2019-03-19 07:07:24'),
(663, 173, '1552979249_1.jpg', 4, NULL, '2019-03-19 07:07:29', '2019-03-19 07:07:29'),
(664, 173, '1552979254_1.jpg', 4, NULL, '2019-03-19 07:07:34', '2019-03-19 07:07:34'),
(665, 173, '1552979261_1.jpg', 4, NULL, '2019-03-19 07:07:41', '2019-03-19 07:07:41'),
(666, 173, '1552979266_1.jpg', 4, NULL, '2019-03-19 07:07:46', '2019-03-19 07:07:46'),
(667, 443, '1552979613_1.jpg', 4, NULL, '2019-03-19 07:13:33', '2019-03-19 07:13:33'),
(668, 443, '1552979619_1.jpg', 4, NULL, '2019-03-19 07:13:39', '2019-03-19 07:13:39'),
(669, 443, '1552979624_1.jpg', 4, NULL, '2019-03-19 07:13:44', '2019-03-19 07:13:44'),
(670, 443, '1552979628_1.jpg', 4, NULL, '2019-03-19 07:13:48', '2019-03-19 07:13:48'),
(671, 443, '1552979633_1.jpg', 4, NULL, '2019-03-19 07:13:53', '2019-03-19 07:13:53'),
(672, 443, '1552979639_1.jpg', 4, NULL, '2019-03-19 07:13:59', '2019-03-19 07:13:59'),
(673, 443, '1552979643_1.jpg', 4, NULL, '2019-03-19 07:14:03', '2019-03-19 07:14:03'),
(674, 296, '1552980855_1.jpg', 4, NULL, '2019-03-19 07:34:15', '2019-03-19 07:34:15'),
(675, 296, '1552980860_1.jpg', 4, NULL, '2019-03-19 07:34:20', '2019-03-19 07:34:20'),
(676, 296, '1552980865_1.jpg', 4, NULL, '2019-03-19 07:34:25', '2019-03-19 07:34:25'),
(677, 296, '1552980870_1.jpg', 4, NULL, '2019-03-19 07:34:30', '2019-03-19 07:34:30'),
(678, 296, '1552980874_1.jpg', 4, NULL, '2019-03-19 07:34:34', '2019-03-19 07:34:34'),
(679, 296, '1552980879_1.jpg', 4, NULL, '2019-03-19 07:34:39', '2019-03-19 07:34:39'),
(680, 266, '1552992576_1.jpg', 4, NULL, '2019-03-19 10:49:36', '2019-03-19 10:49:36'),
(681, 266, '1552992582_1.jpg', 4, NULL, '2019-03-19 10:49:42', '2019-03-19 10:49:42'),
(682, 266, '1552992588_1.jpg', 4, NULL, '2019-03-19 10:49:48', '2019-03-19 10:49:48'),
(683, 266, '1552992593_1.jpg', 4, NULL, '2019-03-19 10:49:53', '2019-03-19 10:49:53'),
(684, 266, '1552992600_1.jpg', 4, NULL, '2019-03-19 10:50:00', '2019-03-19 10:50:00'),
(685, 266, '1552992607_1.jpg', 4, NULL, '2019-03-19 10:50:07', '2019-03-19 10:50:07'),
(686, 266, '1552992612_1.jpg', 4, NULL, '2019-03-19 10:50:12', '2019-03-19 10:50:12'),
(687, 186, '1552992918_1.jpg', 4, NULL, '2019-03-19 10:55:18', '2019-03-19 10:55:18'),
(688, 186, '1552992922_1.jpg', 4, NULL, '2019-03-19 10:55:22', '2019-03-19 10:55:22'),
(689, 186, '1552992927_1.jpg', 4, NULL, '2019-03-19 10:55:27', '2019-03-19 10:55:27'),
(690, 186, '1552992934_1.jpg', 4, NULL, '2019-03-19 10:55:34', '2019-03-19 10:55:34'),
(691, 186, '1552992939_1.jpg', 4, NULL, '2019-03-19 10:55:39', '2019-03-19 10:55:39'),
(692, 186, '1552992944_1.jpg', 4, NULL, '2019-03-19 10:55:44', '2019-03-19 10:55:44'),
(693, 282, '1552993401_1.jpg', 4, NULL, '2019-03-19 11:03:21', '2019-03-19 11:03:21'),
(694, 282, '1552993406_1.jpg', 4, NULL, '2019-03-19 11:03:26', '2019-03-19 11:03:26'),
(695, 282, '1552993411_1.jpg', 4, NULL, '2019-03-19 11:03:31', '2019-03-19 11:03:31'),
(696, 282, '1552993416_1.jpg', 4, NULL, '2019-03-19 11:03:36', '2019-03-19 11:03:36'),
(697, 282, '1552993426_1.jpg', 4, NULL, '2019-03-19 11:03:46', '2019-03-19 11:03:46'),
(698, 282, '1552993432_1.jpg', 4, NULL, '2019-03-19 11:03:52', '2019-03-19 11:03:52'),
(699, 262, '1552994239_1.jpg', 4, NULL, '2019-03-19 11:17:19', '2019-03-19 11:17:19'),
(700, 262, '1552994244_1.jpg', 4, NULL, '2019-03-19 11:17:24', '2019-03-19 11:17:24'),
(701, 262, '1552994250_1.jpg', 4, NULL, '2019-03-19 11:17:30', '2019-03-19 11:17:30'),
(702, 262, '1552994255_1.jpg', 4, NULL, '2019-03-19 11:17:35', '2019-03-19 11:17:35'),
(703, 262, '1552994265_1.jpg', 4, NULL, '2019-03-19 11:17:45', '2019-03-19 11:17:45'),
(704, 262, '1552994270_1.jpg', 4, NULL, '2019-03-19 11:17:50', '2019-03-19 11:17:50'),
(705, 262, '1552994275_1.jpg', 4, NULL, '2019-03-19 11:17:55', '2019-03-19 11:17:55'),
(706, 262, '1552994280_1.jpg', 4, NULL, '2019-03-19 11:18:00', '2019-03-19 11:18:00'),
(707, 312, '1552994779_1.jpg', 4, NULL, '2019-03-19 11:26:19', '2019-03-19 11:26:19'),
(708, 312, '1552994785_1.jpg', 4, NULL, '2019-03-19 11:26:25', '2019-03-19 11:26:25'),
(709, 312, '1552994791_1.jpg', 4, NULL, '2019-03-19 11:26:31', '2019-03-19 11:26:31'),
(710, 312, '1552994805_1.jpg', 4, NULL, '2019-03-19 11:26:45', '2019-03-19 11:26:45'),
(711, 312, '1552994811_1.jpg', 4, NULL, '2019-03-19 11:26:51', '2019-03-19 11:26:51'),
(712, 312, '1552994816_1.jpg', 4, NULL, '2019-03-19 11:26:56', '2019-03-19 11:26:56'),
(713, 315, '1552995635_1.jpg', 4, NULL, '2019-03-19 11:40:35', '2019-03-19 11:40:35'),
(714, 315, '1552995642_1.jpg', 4, NULL, '2019-03-19 11:40:42', '2019-03-19 11:40:42'),
(715, 315, '1552995648_1.jpg', 4, NULL, '2019-03-19 11:40:48', '2019-03-19 11:40:48'),
(716, 315, '1552995654_1.jpg', 4, NULL, '2019-03-19 11:40:54', '2019-03-19 11:40:54'),
(717, 315, '1552995659_1.jpg', 4, NULL, '2019-03-19 11:40:59', '2019-03-19 11:40:59'),
(718, 315, '1552995664_1.jpg', 4, NULL, '2019-03-19 11:41:04', '2019-03-19 11:41:04'),
(719, 187, '1553000658_1.jpg', 4, NULL, '2019-03-19 13:04:18', '2019-03-19 13:04:18'),
(720, 187, '1553000664_1.jpg', 4, NULL, '2019-03-19 13:04:24', '2019-03-19 13:04:24'),
(721, 187, '1553000668_1.jpg', 4, NULL, '2019-03-19 13:04:28', '2019-03-19 13:04:28'),
(722, 187, '1553000673_1.jpg', 4, NULL, '2019-03-19 13:04:33', '2019-03-19 13:04:33'),
(723, 187, '1553000683_1.jpg', 4, NULL, '2019-03-19 13:04:43', '2019-03-19 13:04:43'),
(724, 187, '1553000688_1.jpg', 4, NULL, '2019-03-19 13:04:48', '2019-03-19 13:04:48'),
(725, 187, '1553000693_1.jpg', 4, NULL, '2019-03-19 13:04:53', '2019-03-19 13:04:53'),
(726, 187, '1553000698_1.jpg', 4, NULL, '2019-03-19 13:04:58', '2019-03-19 13:04:58'),
(727, 319, '1553001017_1.jpg', 4, NULL, '2019-03-19 13:10:17', '2019-03-19 13:10:17');
INSERT INTO `shop_pics` (`_id`, `shop_id`, `image`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(728, 319, '1553001024_1.jpg', 4, NULL, '2019-03-19 13:10:24', '2019-03-19 13:10:24'),
(729, 319, '1553001031_1.jpg', 4, NULL, '2019-03-19 13:10:31', '2019-03-19 13:10:31'),
(730, 319, '1553001037_1.jpg', 4, NULL, '2019-03-19 13:10:37', '2019-03-19 13:10:37'),
(731, 319, '1553001043_1.jpg', 4, NULL, '2019-03-19 13:10:43', '2019-03-19 13:10:43'),
(732, 319, '1553001051_1.jpg', 4, NULL, '2019-03-19 13:10:51', '2019-03-19 13:10:51'),
(733, 215, '1553001572_1.jpg', 4, NULL, '2019-03-19 13:19:32', '2019-03-19 13:19:32'),
(734, 215, '1553001576_1.jpg', 4, NULL, '2019-03-19 13:19:36', '2019-03-19 13:19:36'),
(735, 215, '1553001581_1.jpg', 4, NULL, '2019-03-19 13:19:41', '2019-03-19 13:19:41'),
(736, 215, '1553001586_1.jpg', 4, NULL, '2019-03-19 13:19:46', '2019-03-19 13:19:46'),
(737, 215, '1553001591_1.jpg', 4, NULL, '2019-03-19 13:19:51', '2019-03-19 13:19:51'),
(738, 215, '1553001595_1.jpg', 4, NULL, '2019-03-19 13:19:55', '2019-03-19 13:19:55'),
(739, 215, '1553001600_1.jpg', 4, NULL, '2019-03-19 13:20:00', '2019-03-19 13:20:00'),
(740, 273, '1553002247_1.jpg', 4, NULL, '2019-03-19 13:30:47', '2019-03-19 13:30:47'),
(741, 273, '1553002253_1.jpg', 4, NULL, '2019-03-19 13:30:53', '2019-03-19 13:30:53'),
(742, 273, '1553002262_1.jpg', 4, NULL, '2019-03-19 13:31:02', '2019-03-19 13:31:02'),
(743, 273, '1553002267_1.jpg', 4, NULL, '2019-03-19 13:31:07', '2019-03-19 13:31:07'),
(744, 273, '1553002273_1.jpg', 4, NULL, '2019-03-19 13:31:13', '2019-03-19 13:31:13'),
(745, 392, '1553003091_1.jpg', 4, NULL, '2019-03-19 13:44:51', '2019-03-19 13:44:51'),
(746, 392, '1553003097_1.jpg', 4, NULL, '2019-03-19 13:44:57', '2019-03-19 13:44:57'),
(747, 392, '1553003102_1.jpg', 4, NULL, '2019-03-19 13:45:02', '2019-03-19 13:45:02'),
(748, 392, '1553003108_1.jpg', 4, NULL, '2019-03-19 13:45:08', '2019-03-19 13:45:08'),
(749, 392, '1553003114_1.jpg', 4, NULL, '2019-03-19 13:45:14', '2019-03-19 13:45:14'),
(750, 392, '1553003119_1.jpg', 4, NULL, '2019-03-19 13:45:19', '2019-03-19 13:45:19'),
(751, 392, '1553003124_1.jpg', 4, NULL, '2019-03-19 13:45:24', '2019-03-19 13:45:24'),
(752, 392, '1553003136_1.jpg', 4, NULL, '2019-03-19 13:45:36', '2019-03-19 13:45:36'),
(753, 405, '1553060078_1.jpg', 4, NULL, '2019-03-20 05:34:38', '2019-03-20 05:34:38'),
(754, 405, '1553060083_1.jpg', 4, NULL, '2019-03-20 05:34:43', '2019-03-20 05:34:43'),
(755, 405, '1553060088_1.jpg', 4, NULL, '2019-03-20 05:34:48', '2019-03-20 05:34:48'),
(756, 405, '1553060096_1.jpg', 4, NULL, '2019-03-20 05:34:56', '2019-03-20 05:34:56'),
(757, 405, '1553060102_1.jpg', 4, NULL, '2019-03-20 05:35:02', '2019-03-20 05:35:02'),
(758, 405, '1553060109_1.jpg', 4, NULL, '2019-03-20 05:35:09', '2019-03-20 05:35:09'),
(759, 405, '1553060114_1.jpg', 4, NULL, '2019-03-20 05:35:14', '2019-03-20 05:35:14'),
(760, 214, '1553061138_1.jpg', 4, NULL, '2019-03-20 05:52:18', '2019-03-20 05:52:18'),
(761, 214, '1553061144_1.jpg', 4, NULL, '2019-03-20 05:52:24', '2019-03-20 05:52:24'),
(762, 214, '1553061149_1.jpg', 4, NULL, '2019-03-20 05:52:29', '2019-03-20 05:52:29'),
(763, 214, '1553061154_1.jpg', 4, NULL, '2019-03-20 05:52:34', '2019-03-20 05:52:34'),
(764, 214, '1553061159_1.jpg', 4, NULL, '2019-03-20 05:52:39', '2019-03-20 05:52:39'),
(765, 214, '1553061165_1.jpg', 4, NULL, '2019-03-20 05:52:45', '2019-03-20 05:52:45'),
(766, 214, '1553061170_1.jpg', 4, NULL, '2019-03-20 05:52:50', '2019-03-20 05:52:50'),
(767, 394, '1553061741_1.jpg', 4, NULL, '2019-03-20 06:02:21', '2019-03-20 06:02:21'),
(768, 394, '1553061746_1.jpg', 4, NULL, '2019-03-20 06:02:26', '2019-03-20 06:02:26'),
(769, 394, '1553061751_1.jpg', 4, NULL, '2019-03-20 06:02:31', '2019-03-20 06:02:31'),
(770, 394, '1553061756_1.jpg', 4, NULL, '2019-03-20 06:02:36', '2019-03-20 06:02:36'),
(771, 394, '1553061760_1.jpg', 4, NULL, '2019-03-20 06:02:40', '2019-03-20 06:02:40'),
(772, 394, '1553061768_1.jpg', 4, NULL, '2019-03-20 06:02:48', '2019-03-20 06:02:48'),
(773, 394, '1553061773_1.jpg', 4, NULL, '2019-03-20 06:02:53', '2019-03-20 06:02:53'),
(774, 461, '1553063831_1.jpg', 4, NULL, '2019-03-20 06:37:11', '2019-03-20 06:37:11'),
(775, 461, '1553064069_1.jpg', 4, NULL, '2019-03-20 06:41:09', '2019-03-20 06:41:09'),
(776, 461, '1553064078_1.jpg', 4, NULL, '2019-03-20 06:41:18', '2019-03-20 06:41:18'),
(777, 461, '1553064083_1.jpg', 4, NULL, '2019-03-20 06:41:23', '2019-03-20 06:41:23'),
(778, 461, '1553064088_1.jpg', 4, NULL, '2019-03-20 06:41:28', '2019-03-20 06:41:28'),
(779, 461, '1553064093_1.jpg', 4, NULL, '2019-03-20 06:41:33', '2019-03-20 06:41:33'),
(780, 461, '1553064098_1.jpg', 4, NULL, '2019-03-20 06:41:38', '2019-03-20 06:41:38'),
(781, 461, '1553064104_1.jpg', 4, NULL, '2019-03-20 06:41:44', '2019-03-20 06:41:44'),
(782, 461, '1553064108_1.jpg', 4, NULL, '2019-03-20 06:41:48', '2019-03-20 06:41:48'),
(783, 461, '1553064114_1.jpg', 4, NULL, '2019-03-20 06:41:54', '2019-03-20 06:41:54'),
(784, 219, '1553064574_1.jpg', 4, NULL, '2019-03-20 06:49:34', '2019-03-20 06:49:34'),
(785, 219, '1553064581_1.jpg', 4, NULL, '2019-03-20 06:49:41', '2019-03-20 06:49:41'),
(786, 219, '1553064587_1.jpg', 4, NULL, '2019-03-20 06:49:47', '2019-03-20 06:49:47'),
(787, 219, '1553064592_1.jpg', 4, NULL, '2019-03-20 06:49:52', '2019-03-20 06:49:52'),
(788, 219, '1553064598_1.jpg', 4, NULL, '2019-03-20 06:49:58', '2019-03-20 06:49:58'),
(789, 219, '1553064603_1.jpg', 4, NULL, '2019-03-20 06:50:03', '2019-03-20 06:50:03'),
(790, 219, '1553064609_1.jpg', 4, NULL, '2019-03-20 06:50:09', '2019-03-20 06:50:09'),
(791, 451, '1553065038_1.jpg', 4, NULL, '2019-03-20 06:57:18', '2019-03-20 06:57:18'),
(792, 451, '1553065043_1.jpg', 4, NULL, '2019-03-20 06:57:23', '2019-03-20 06:57:23'),
(793, 451, '1553065047_1.jpg', 4, NULL, '2019-03-20 06:57:27', '2019-03-20 06:57:27'),
(794, 451, '1553065053_1.jpg', 4, NULL, '2019-03-20 06:57:33', '2019-03-20 06:57:33'),
(795, 451, '1553065058_1.jpg', 4, NULL, '2019-03-20 06:57:38', '2019-03-20 06:57:38'),
(796, 451, '1553065066_1.jpg', 4, NULL, '2019-03-20 06:57:46', '2019-03-20 06:57:46'),
(797, 403, '1553065489_1.jpg', 4, NULL, '2019-03-20 07:04:49', '2019-03-20 07:04:49'),
(798, 403, '1553065495_1.jpg', 4, NULL, '2019-03-20 07:04:55', '2019-03-20 07:04:55'),
(799, 403, '1553065501_1.jpg', 4, NULL, '2019-03-20 07:05:01', '2019-03-20 07:05:01'),
(800, 403, '1553065506_1.jpg', 4, NULL, '2019-03-20 07:05:06', '2019-03-20 07:05:06'),
(801, 403, '1553065511_1.jpg', 4, NULL, '2019-03-20 07:05:11', '2019-03-20 07:05:11'),
(802, 403, '1553065516_1.jpg', 4, NULL, '2019-03-20 07:05:16', '2019-03-20 07:05:16'),
(803, 69, '1553067879_1.jpg', 4, NULL, '2019-03-20 07:44:39', '2019-03-20 07:44:39'),
(804, 69, '1553067885_1.jpg', 4, NULL, '2019-03-20 07:44:45', '2019-03-20 07:44:45'),
(805, 69, '1553067889_1.jpg', 4, NULL, '2019-03-20 07:44:49', '2019-03-20 07:44:49'),
(806, 69, '1553067893_1.jpg', 4, NULL, '2019-03-20 07:44:53', '2019-03-20 07:44:53'),
(807, 69, '1553067898_1.jpg', 4, NULL, '2019-03-20 07:44:58', '2019-03-20 07:44:58'),
(808, 69, '1553067904_1.jpg', 4, NULL, '2019-03-20 07:45:04', '2019-03-20 07:45:04'),
(809, 69, '1553067909_1.jpg', 4, NULL, '2019-03-20 07:45:09', '2019-03-20 07:45:09'),
(810, 69, '1553067913_1.jpg', 4, NULL, '2019-03-20 07:45:13', '2019-03-20 07:45:13'),
(811, 256, '1553068331_1.jpg', 4, NULL, '2019-03-20 07:52:11', '2019-03-20 07:52:11'),
(812, 256, '1553068336_1.jpg', 4, NULL, '2019-03-20 07:52:16', '2019-03-20 07:52:16'),
(813, 256, '1553068340_1.jpg', 4, NULL, '2019-03-20 07:52:20', '2019-03-20 07:52:20'),
(814, 256, '1553068345_1.jpg', 4, NULL, '2019-03-20 07:52:25', '2019-03-20 07:52:25'),
(815, 256, '1553068350_1.jpg', 4, NULL, '2019-03-20 07:52:30', '2019-03-20 07:52:30'),
(816, 256, '1553068355_1.jpg', 4, NULL, '2019-03-20 07:52:35', '2019-03-20 07:52:35'),
(817, 256, '1553068359_1.jpg', 4, NULL, '2019-03-20 07:52:39', '2019-03-20 07:52:39'),
(818, 221, '1553069233_1.jpg', 4, NULL, '2019-03-20 08:07:13', '2019-03-20 08:07:13'),
(819, 221, '1553069243_1.jpg', 4, NULL, '2019-03-20 08:07:23', '2019-03-20 08:07:23'),
(820, 221, '1553069255_1.jpg', 4, NULL, '2019-03-20 08:07:35', '2019-03-20 08:07:35'),
(821, 221, '1553069263_1.jpg', 4, NULL, '2019-03-20 08:07:43', '2019-03-20 08:07:43'),
(822, 221, '1553069269_1.jpg', 4, NULL, '2019-03-20 08:07:49', '2019-03-20 08:07:49'),
(823, 221, '1553069274_1.jpg', 4, NULL, '2019-03-20 08:07:54', '2019-03-20 08:07:54'),
(824, 265, '1553069812_1.jpg', 4, NULL, '2019-03-20 08:16:52', '2019-03-20 08:16:52'),
(825, 305, '1553070104_1.jpg', 4, NULL, '2019-03-20 08:21:44', '2019-03-20 08:21:44'),
(826, 305, '1553070109_1.jpg', 4, NULL, '2019-03-20 08:21:49', '2019-03-20 08:21:49'),
(827, 305, '1553070115_1.jpg', 4, NULL, '2019-03-20 08:21:55', '2019-03-20 08:21:55'),
(828, 305, '1553070119_1.jpg', 4, NULL, '2019-03-20 08:21:59', '2019-03-20 08:21:59'),
(829, 305, '1553070124_1.jpg', 4, NULL, '2019-03-20 08:22:04', '2019-03-20 08:22:04'),
(830, 305, '1553070130_1.jpg', 4, NULL, '2019-03-20 08:22:10', '2019-03-20 08:22:10'),
(831, 53, '1553070853_1.jpg', 4, NULL, '2019-03-20 08:34:13', '2019-03-20 08:34:13'),
(832, 53, '1553070860_1.jpg', 4, NULL, '2019-03-20 08:34:20', '2019-03-20 08:34:20'),
(833, 53, '1553070984_1.jpg', 4, NULL, '2019-03-20 08:36:24', '2019-03-20 08:36:24'),
(834, 53, '1553071071_1.jpg', 4, NULL, '2019-03-20 08:37:51', '2019-03-20 08:37:51'),
(835, 53, '1553071077_1.jpg', 4, NULL, '2019-03-20 08:37:57', '2019-03-20 08:37:57'),
(836, 53, '1553071082_1.jpg', 4, NULL, '2019-03-20 08:38:02', '2019-03-20 08:38:02'),
(837, 222, '1553071666_1.jpg', 4, NULL, '2019-03-20 08:47:46', '2019-03-20 08:47:46'),
(838, 222, '1553071775_1.jpg', 4, NULL, '2019-03-20 08:49:35', '2019-03-20 08:49:35'),
(839, 222, '1553071780_1.jpg', 4, NULL, '2019-03-20 08:49:40', '2019-03-20 08:49:40'),
(840, 222, '1553071786_1.jpg', 4, NULL, '2019-03-20 08:49:46', '2019-03-20 08:49:46'),
(841, 222, '1553071791_1.jpg', 4, NULL, '2019-03-20 08:49:51', '2019-03-20 08:49:51'),
(842, 222, '1553071796_1.jpg', 4, NULL, '2019-03-20 08:49:56', '2019-03-20 08:49:56'),
(843, 222, '1553071806_1.jpg', 4, NULL, '2019-03-20 08:50:06', '2019-03-20 08:50:06'),
(844, 223, '1553080273_1.jpg', 4, NULL, '2019-03-20 11:11:13', '2019-03-20 11:11:13'),
(845, 223, '1553080279_1.jpg', 4, NULL, '2019-03-20 11:11:19', '2019-03-20 11:11:19'),
(846, 223, '1553080290_1.jpg', 4, NULL, '2019-03-20 11:11:30', '2019-03-20 11:11:30'),
(847, 223, '1553080299_1.jpg', 4, NULL, '2019-03-20 11:11:39', '2019-03-20 11:11:39'),
(848, 223, '1553080304_1.jpg', 4, NULL, '2019-03-20 11:11:44', '2019-03-20 11:11:44'),
(849, 223, '1553080310_1.jpg', 4, NULL, '2019-03-20 11:11:50', '2019-03-20 11:11:50'),
(850, 223, '1553080316_1.jpg', 4, NULL, '2019-03-20 11:11:56', '2019-03-20 11:11:56'),
(851, 228, '1553147561_1.jpg', 4, NULL, '2019-03-21 05:52:41', '2019-03-21 05:52:41'),
(852, 228, '1553147569_1.jpg', 4, NULL, '2019-03-21 05:52:49', '2019-03-21 05:52:49'),
(853, 228, '1553147575_1.jpg', 4, NULL, '2019-03-21 05:52:55', '2019-03-21 05:52:55'),
(854, 228, '1553147582_1.jpg', 4, NULL, '2019-03-21 05:53:02', '2019-03-21 05:53:02'),
(855, 228, '1553147586_1.jpg', 4, NULL, '2019-03-21 05:53:06', '2019-03-21 05:53:06'),
(856, 228, '1553147592_1.jpg', 4, NULL, '2019-03-21 05:53:12', '2019-03-21 05:53:12'),
(857, 228, '1553147598_1.jpg', 4, NULL, '2019-03-21 05:53:18', '2019-03-21 05:53:18'),
(858, 229, '1553148162_1.jpg', 4, NULL, '2019-03-21 06:02:42', '2019-03-21 06:02:42'),
(859, 229, '1553148168_1.jpg', 4, NULL, '2019-03-21 06:02:48', '2019-03-21 06:02:48'),
(860, 229, '1553148176_1.jpg', 4, NULL, '2019-03-21 06:02:56', '2019-03-21 06:02:56'),
(861, 229, '1553148182_1.jpg', 4, NULL, '2019-03-21 06:03:02', '2019-03-21 06:03:02'),
(862, 229, '1553148188_1.jpg', 4, NULL, '2019-03-21 06:03:08', '2019-03-21 06:03:08'),
(863, 229, '1553148197_1.jpg', 4, NULL, '2019-03-21 06:03:17', '2019-03-21 06:03:17'),
(864, 229, '1553148206_1.jpg', 4, NULL, '2019-03-21 06:03:26', '2019-03-21 06:03:26'),
(865, 229, '1553148211_1.jpg', 4, NULL, '2019-03-21 06:03:31', '2019-03-21 06:03:31'),
(866, 229, '1553148216_1.jpg', 4, NULL, '2019-03-21 06:03:36', '2019-03-21 06:03:36'),
(867, 229, '1553148223_1.jpg', 4, NULL, '2019-03-21 06:03:43', '2019-03-21 06:03:43'),
(868, 230, '1553149411_1.jpg', 4, NULL, '2019-03-21 06:23:31', '2019-03-21 06:23:31'),
(869, 230, '1553149422_1.jpg', 4, NULL, '2019-03-21 06:23:42', '2019-03-21 06:23:42'),
(870, 230, '1553149429_1.jpg', 4, NULL, '2019-03-21 06:23:49', '2019-03-21 06:23:49'),
(872, 230, '1553149440_1.jpg', 4, NULL, '2019-03-21 06:24:00', '2019-03-21 06:24:00'),
(873, 230, '1553149455_1.jpg', 4, NULL, '2019-03-21 06:24:15', '2019-03-21 06:24:15'),
(874, 230, '1553149461_1.jpg', 4, NULL, '2019-03-21 06:24:21', '2019-03-21 06:24:21'),
(875, 230, '1553149466_1.jpg', 4, NULL, '2019-03-21 06:24:26', '2019-03-21 06:24:26'),
(876, 230, '1553149471_1.jpg', 4, NULL, '2019-03-21 06:24:31', '2019-03-21 06:24:31'),
(877, 230, '1553149477_1.jpg', 4, NULL, '2019-03-21 06:24:37', '2019-03-21 06:24:37'),
(878, 230, '1553149482_1.jpg', 4, NULL, '2019-03-21 06:24:42', '2019-03-21 06:24:42'),
(879, 445, '1553149783_1.jpg', 4, NULL, '2019-03-21 06:29:43', '2019-03-21 06:29:43'),
(880, 445, '1553149788_1.jpg', 4, NULL, '2019-03-21 06:29:48', '2019-03-21 06:29:48'),
(881, 445, '1553149792_1.jpg', 4, NULL, '2019-03-21 06:29:52', '2019-03-21 06:29:52'),
(882, 445, '1553149807_1.jpg', 4, NULL, '2019-03-21 06:30:07', '2019-03-21 06:30:07'),
(883, 445, '1553149814_1.jpg', 4, NULL, '2019-03-21 06:30:14', '2019-03-21 06:30:14'),
(884, 73, '1553151177_1.jpg', 4, NULL, '2019-03-21 06:52:57', '2019-03-21 06:52:57'),
(885, 298, '1553154091_1.jpg', 4, NULL, '2019-03-21 07:41:31', '2019-03-21 07:41:31'),
(886, 298, '1553154147_1.jpg', 4, NULL, '2019-03-21 07:42:27', '2019-03-21 07:42:27'),
(887, 298, '1553154154_1.jpg', 4, NULL, '2019-03-21 07:42:34', '2019-03-21 07:42:34'),
(888, 298, '1553154160_1.jpg', 4, NULL, '2019-03-21 07:42:40', '2019-03-21 07:42:40'),
(889, 298, '1553154165_1.jpg', 4, NULL, '2019-03-21 07:42:45', '2019-03-21 07:42:45'),
(890, 298, '1553154170_1.jpg', 4, NULL, '2019-03-21 07:42:50', '2019-03-21 07:42:50'),
(891, 298, '1553154175_1.jpg', 4, NULL, '2019-03-21 07:42:55', '2019-03-21 07:42:55'),
(892, 298, '1553154181_1.jpg', 4, NULL, '2019-03-21 07:43:01', '2019-03-21 07:43:01'),
(893, 231, '1553154878_1.jpg', 4, NULL, '2019-03-21 07:54:38', '2019-03-21 07:54:38'),
(894, 231, '1553154882_1.jpg', 4, NULL, '2019-03-21 07:54:42', '2019-03-21 07:54:42'),
(895, 231, '1553154899_1.jpg', 4, NULL, '2019-03-21 07:54:59', '2019-03-21 07:54:59'),
(896, 231, '1553154899_2.jpg', 4, NULL, '2019-03-21 07:54:59', '2019-03-21 07:54:59'),
(897, 231, '1553154899_3.jpg', 4, NULL, '2019-03-21 07:54:59', '2019-03-21 07:54:59'),
(898, 231, '1553154899_4.jpg', 4, NULL, '2019-03-21 07:54:59', '2019-03-21 07:54:59'),
(899, 231, '1553154919_1.jpg', 4, NULL, '2019-03-21 07:55:19', '2019-03-21 07:55:19'),
(900, 188, '1553156308_1.jpg', 4, NULL, '2019-03-21 08:18:28', '2019-03-21 08:18:28'),
(901, 188, '1553156308_2.jpg', 4, NULL, '2019-03-21 08:18:28', '2019-03-21 08:18:28'),
(902, 188, '1553156308_3.jpg', 4, NULL, '2019-03-21 08:18:28', '2019-03-21 08:18:28'),
(903, 188, '1553156308_4.jpg', 4, NULL, '2019-03-21 08:18:28', '2019-03-21 08:18:28'),
(904, 188, '1553156308_5.jpg', 4, NULL, '2019-03-21 08:18:28', '2019-03-21 08:18:28'),
(905, 188, '1553156308_6.jpg', 4, NULL, '2019-03-21 08:18:28', '2019-03-21 08:18:28'),
(906, 188, '1553156308_7.jpg', 4, NULL, '2019-03-21 08:18:28', '2019-03-21 08:18:28'),
(907, 188, '1553156308_8.jpg', 4, NULL, '2019-03-21 08:18:28', '2019-03-21 08:18:28'),
(908, 188, '1553156308_9.jpg', 4, NULL, '2019-03-21 08:18:28', '2019-03-21 08:18:28'),
(909, 188, '1553156308_10.jpg', 4, NULL, '2019-03-21 08:18:28', '2019-03-21 08:18:28'),
(910, 236, '1553156736_1.jpg', 4, NULL, '2019-03-21 08:25:36', '2019-03-21 08:25:36'),
(911, 236, '1553156736_2.jpg', 4, NULL, '2019-03-21 08:25:36', '2019-03-21 08:25:36'),
(912, 236, '1553156736_3.jpg', 4, NULL, '2019-03-21 08:25:36', '2019-03-21 08:25:36'),
(913, 236, '1553156736_4.jpg', 4, NULL, '2019-03-21 08:25:36', '2019-03-21 08:25:36'),
(914, 236, '1553156736_5.jpg', 4, NULL, '2019-03-21 08:25:36', '2019-03-21 08:25:36'),
(915, 60, '1553157083_1.jpg', 4, NULL, '2019-03-21 08:31:23', '2019-03-21 08:31:23'),
(916, 60, '1553157083_2.jpg', 4, NULL, '2019-03-21 08:31:23', '2019-03-21 08:31:23'),
(917, 60, '1553157083_3.jpg', 4, NULL, '2019-03-21 08:31:23', '2019-03-21 08:31:23'),
(918, 60, '1553157083_4.jpg', 4, NULL, '2019-03-21 08:31:23', '2019-03-21 08:31:23'),
(919, 60, '1553157083_5.jpg', 4, NULL, '2019-03-21 08:31:23', '2019-03-21 08:31:23'),
(920, 60, '1553157083_6.jpg', 4, NULL, '2019-03-21 08:31:23', '2019-03-21 08:31:23'),
(921, 60, '1553157083_7.jpg', 4, NULL, '2019-03-21 08:31:23', '2019-03-21 08:31:23'),
(922, 60, '1553157083_8.jpg', 4, NULL, '2019-03-21 08:31:23', '2019-03-21 08:31:23'),
(923, 261, '1553157604_1.jpg', 4, NULL, '2019-03-21 08:40:04', '2019-03-21 08:40:04'),
(924, 261, '1553157604_2.jpg', 4, NULL, '2019-03-21 08:40:04', '2019-03-21 08:40:04'),
(925, 261, '1553157604_3.jpg', 4, NULL, '2019-03-21 08:40:04', '2019-03-21 08:40:04'),
(926, 261, '1553157604_4.jpg', 4, NULL, '2019-03-21 08:40:04', '2019-03-21 08:40:04'),
(927, 261, '1553157604_5.jpg', 4, NULL, '2019-03-21 08:40:04', '2019-03-21 08:40:04'),
(928, 261, '1553157604_6.jpg', 4, NULL, '2019-03-21 08:40:04', '2019-03-21 08:40:04'),
(929, 261, '1553157604_7.jpg', 4, NULL, '2019-03-21 08:40:04', '2019-03-21 08:40:04'),
(930, 261, '1553157604_8.jpg', 4, NULL, '2019-03-21 08:40:04', '2019-03-21 08:40:04'),
(931, 55, '1553411371_1.jpg', 4, NULL, '2019-03-24 07:09:31', '2019-03-24 07:09:31'),
(932, 55, '1553411371_2.jpg', 4, NULL, '2019-03-24 07:09:31', '2019-03-24 07:09:31'),
(933, 55, '1553411371_3.jpg', 4, NULL, '2019-03-24 07:09:31', '2019-03-24 07:09:31'),
(934, 55, '1553411371_4.jpg', 4, NULL, '2019-03-24 07:09:31', '2019-03-24 07:09:31'),
(935, 55, '1553411371_5.jpg', 4, NULL, '2019-03-24 07:09:31', '2019-03-24 07:09:31'),
(936, 55, '1553411371_6.jpg', 4, NULL, '2019-03-24 07:09:31', '2019-03-24 07:09:31'),
(937, 55, '1553411371_7.jpg', 4, NULL, '2019-03-24 07:09:31', '2019-03-24 07:09:31'),
(938, 55, '1553411371_8.jpg', 4, NULL, '2019-03-24 07:09:31', '2019-03-24 07:09:31'),
(939, 55, '1553411371_9.jpg', 4, NULL, '2019-03-24 07:09:31', '2019-03-24 07:09:31'),
(940, 55, '1553411371_10.jpg', 4, NULL, '2019-03-24 07:09:31', '2019-03-24 07:09:31'),
(941, 277, '1553411807_1.jpg', 4, NULL, '2019-03-24 07:16:47', '2019-03-24 07:16:47'),
(942, 277, '1553411807_2.jpg', 4, NULL, '2019-03-24 07:16:47', '2019-03-24 07:16:47'),
(943, 277, '1553411807_3.jpg', 4, NULL, '2019-03-24 07:16:47', '2019-03-24 07:16:47'),
(944, 277, '1553411807_4.jpg', 4, NULL, '2019-03-24 07:16:47', '2019-03-24 07:16:47'),
(945, 277, '1553411807_5.jpg', 4, NULL, '2019-03-24 07:16:47', '2019-03-24 07:16:47'),
(946, 277, '1553411807_6.jpg', 4, NULL, '2019-03-24 07:16:47', '2019-03-24 07:16:47'),
(947, 277, '1553411807_7.jpg', 4, NULL, '2019-03-24 07:16:47', '2019-03-24 07:16:47'),
(948, 277, '1553411807_8.jpg', 4, NULL, '2019-03-24 07:16:47', '2019-03-24 07:16:47'),
(949, 277, '1553411807_9.jpg', 4, NULL, '2019-03-24 07:16:47', '2019-03-24 07:16:47'),
(950, 279, '1553424864_1.jpg', 4, NULL, '2019-03-24 10:54:24', '2019-03-24 10:54:24'),
(951, 279, '1553424864_2.jpg', 4, NULL, '2019-03-24 10:54:24', '2019-03-24 10:54:24'),
(952, 279, '1553424864_3.jpg', 4, NULL, '2019-03-24 10:54:24', '2019-03-24 10:54:24'),
(953, 279, '1553424864_4.jpg', 4, NULL, '2019-03-24 10:54:24', '2019-03-24 10:54:24'),
(954, 279, '1553424864_5.jpg', 4, NULL, '2019-03-24 10:54:24', '2019-03-24 10:54:24'),
(955, 279, '1553424864_6.jpg', 4, NULL, '2019-03-24 10:54:24', '2019-03-24 10:54:24'),
(956, 279, '1553424864_7.jpg', 4, NULL, '2019-03-24 10:54:24', '2019-03-24 10:54:24'),
(957, 279, '1553424864_8.jpg', 4, NULL, '2019-03-24 10:54:24', '2019-03-24 10:54:24'),
(958, 447, '1553427079_1.jpg', 4, NULL, '2019-03-24 11:31:19', '2019-03-24 11:31:19'),
(959, 447, '1553427079_2.jpg', 4, NULL, '2019-03-24 11:31:19', '2019-03-24 11:31:19'),
(960, 447, '1553427079_3.jpg', 4, NULL, '2019-03-24 11:31:19', '2019-03-24 11:31:19'),
(961, 447, '1553427079_4.jpg', 4, NULL, '2019-03-24 11:31:19', '2019-03-24 11:31:19'),
(962, 447, '1553427079_5.jpg', 4, NULL, '2019-03-24 11:31:19', '2019-03-24 11:31:19'),
(963, 447, '1553427079_6.jpg', 4, NULL, '2019-03-24 11:31:19', '2019-03-24 11:31:19'),
(964, 447, '1553427079_7.jpg', 4, NULL, '2019-03-24 11:31:19', '2019-03-24 11:31:19'),
(965, 447, '1553427079_8.jpg', 4, NULL, '2019-03-24 11:31:19', '2019-03-24 11:31:19'),
(966, 447, '1553427079_9.jpg', 4, NULL, '2019-03-24 11:31:19', '2019-03-24 11:31:19'),
(967, 447, '1553427079_10.jpg', 4, NULL, '2019-03-24 11:31:19', '2019-03-24 11:31:19'),
(968, 300, '1553427550_1.jpg', 4, NULL, '2019-03-24 11:39:10', '2019-03-24 11:39:10'),
(969, 300, '1553427550_2.jpg', 4, NULL, '2019-03-24 11:39:10', '2019-03-24 11:39:10'),
(970, 300, '1553427550_3.jpg', 4, NULL, '2019-03-24 11:39:10', '2019-03-24 11:39:10'),
(971, 300, '1553427550_4.jpg', 4, NULL, '2019-03-24 11:39:10', '2019-03-24 11:39:10'),
(972, 300, '1553427550_5.jpg', 4, NULL, '2019-03-24 11:39:10', '2019-03-24 11:39:10'),
(973, 300, '1553427550_6.jpg', 4, NULL, '2019-03-24 11:39:10', '2019-03-24 11:39:10'),
(974, 300, '1553427550_7.jpg', 4, NULL, '2019-03-24 11:39:10', '2019-03-24 11:39:10'),
(975, 300, '1553427550_8.jpg', 4, NULL, '2019-03-24 11:39:10', '2019-03-24 11:39:10'),
(976, 300, '1553427550_9.jpg', 4, NULL, '2019-03-24 11:39:10', '2019-03-24 11:39:10'),
(977, 300, '1553427550_10.jpg', 4, NULL, '2019-03-24 11:39:10', '2019-03-24 11:39:10'),
(978, 398, '1553429220_1.jpg', 4, NULL, '2019-03-24 12:07:00', '2019-03-24 12:07:00'),
(979, 398, '1553429220_2.jpg', 4, NULL, '2019-03-24 12:07:00', '2019-03-24 12:07:00'),
(980, 398, '1553429220_3.jpg', 4, NULL, '2019-03-24 12:07:00', '2019-03-24 12:07:00'),
(981, 398, '1553429220_4.jpg', 4, NULL, '2019-03-24 12:07:00', '2019-03-24 12:07:00'),
(982, 398, '1553429220_5.jpg', 4, NULL, '2019-03-24 12:07:00', '2019-03-24 12:07:00'),
(983, 398, '1553429220_6.jpg', 4, NULL, '2019-03-24 12:07:00', '2019-03-24 12:07:00'),
(984, 398, '1553429220_7.jpg', 4, NULL, '2019-03-24 12:07:00', '2019-03-24 12:07:00'),
(985, 398, '1553429220_8.jpg', 4, NULL, '2019-03-24 12:07:00', '2019-03-24 12:07:00'),
(986, 398, '1553429220_9.jpg', 4, NULL, '2019-03-24 12:07:00', '2019-03-24 12:07:00'),
(987, 398, '1553429220_10.jpg', 4, NULL, '2019-03-24 12:07:00', '2019-03-24 12:07:00'),
(988, 398, '1553429220_11.jpg', 4, NULL, '2019-03-24 12:07:00', '2019-03-24 12:07:00'),
(989, 213, '1556458090_1.jpg', 4, NULL, '2019-04-28 13:28:10', '2019-04-28 13:28:10'),
(990, 213, '1556458090_2.jpg', 4, NULL, '2019-04-28 13:28:10', '2019-04-28 13:28:10'),
(991, 213, '1556458090_3.jpg', 4, NULL, '2019-04-28 13:28:10', '2019-04-28 13:28:10'),
(992, 213, '1556458090_4.jpg', 4, NULL, '2019-04-28 13:28:10', '2019-04-28 13:28:10'),
(993, 471, '1556534827_1.jpg', 4, NULL, '2019-04-29 10:47:07', '2019-04-29 10:47:07'),
(994, 471, '1556534827_2.jpg', 4, NULL, '2019-04-29 10:47:07', '2019-04-29 10:47:07'),
(995, 471, '1556534827_3.jpg', 4, NULL, '2019-04-29 10:47:07', '2019-04-29 10:47:07'),
(997, 460, '1558005848_1.jpg', 4, NULL, '2019-05-16 11:24:08', '2019-05-16 11:24:08'),
(998, 480, '1559134269_1.jpg', 4, NULL, '2019-05-29 12:51:09', '2019-05-29 12:51:09'),
(999, 480, '1559134279_1.jpg', 4, NULL, '2019-05-29 12:51:19', '2019-05-29 12:51:19'),
(1000, 480, '1559134279_2.jpg', 4, NULL, '2019-05-29 12:51:19', '2019-05-29 12:51:19'),
(1001, 480, '1559134279_3.jpg', 4, NULL, '2019-05-29 12:51:19', '2019-05-29 12:51:19'),
(1002, 480, '1559134279_4.jpg', 4, NULL, '2019-05-29 12:51:19', '2019-05-29 12:51:19'),
(1003, 480, '1559134279_5.jpg', 4, NULL, '2019-05-29 12:51:19', '2019-05-29 12:51:19'),
(1004, 501, '1566913875_1.jpg', 4, NULL, '2019-08-27 13:51:15', '2019-08-27 13:51:15'),
(1005, 501, '1566913875_2.jpg', 4, NULL, '2019-08-27 13:51:15', '2019-08-27 13:51:15'),
(1006, 501, '1566913875_3.jpg', 4, NULL, '2019-08-27 13:51:15', '2019-08-27 13:51:15'),
(1007, 501, '1566913875_4.jpg', 4, NULL, '2019-08-27 13:51:15', '2019-08-27 13:51:15'),
(1008, 501, '1566913875_5.jpg', 4, NULL, '2019-08-27 13:51:15', '2019-08-27 13:51:15'),
(1009, 484, '1567413283_1.jpg', 4, NULL, '2019-09-02 08:34:43', '2019-09-02 08:34:43'),
(1010, 484, '1567413283_2.jpg', 4, NULL, '2019-09-02 08:34:43', '2019-09-02 08:34:43'),
(1011, 484, '1567413293_1.jpg', 4, NULL, '2019-09-02 08:34:53', '2019-09-02 08:34:53'),
(1012, 484, '1567413293_2.jpg', 4, NULL, '2019-09-02 08:34:53', '2019-09-02 08:34:53'),
(1013, 204, '1571918635_1.jpg', 4, NULL, '2019-10-24 12:03:55', '2019-10-24 12:03:55'),
(1014, 204, '1571918635_2.jpg', 4, NULL, '2019-10-24 12:03:55', '2019-10-24 12:03:55'),
(1015, 204, '1571918635_3.jpg', 4, NULL, '2019-10-24 12:03:55', '2019-10-24 12:03:55'),
(1016, 204, '1571918635_4.jpg', 4, NULL, '2019-10-24 12:03:55', '2019-10-24 12:03:55'),
(1017, 271, '1571918810_1.jpg', 4, NULL, '2019-10-24 12:06:50', '2019-10-24 12:06:50'),
(1018, 271, '1571918810_2.jpg', 4, NULL, '2019-10-24 12:06:50', '2019-10-24 12:06:50'),
(1019, 271, '1571918810_3.jpg', 4, NULL, '2019-10-24 12:06:50', '2019-10-24 12:06:50'),
(1020, 271, '1571918810_4.jpg', 4, NULL, '2019-10-24 12:06:50', '2019-10-24 12:06:50'),
(1021, 271, '1571918810_5.jpg', 4, NULL, '2019-10-24 12:06:50', '2019-10-24 12:06:50'),
(1022, 271, '1571918810_6.jpg', 4, NULL, '2019-10-24 12:06:50', '2019-10-24 12:06:50'),
(1023, 502, '1572177846_1.jpg', 4, NULL, '2019-10-27 12:04:06', '2019-10-27 12:04:06'),
(1024, 502, '1572177846_2.jpg', 4, NULL, '2019-10-27 12:04:06', '2019-10-27 12:04:06'),
(1025, 502, '1572177846_3.jpg', 4, NULL, '2019-10-27 12:04:06', '2019-10-27 12:04:06'),
(1026, 502, '1572177846_4.jpg', 4, NULL, '2019-10-27 12:04:06', '2019-10-27 12:04:06'),
(1027, 502, '1572177846_5.jpg', 4, NULL, '2019-10-27 12:04:06', '2019-10-27 12:04:06'),
(1028, 502, '1572177846_6.jpg', 4, NULL, '2019-10-27 12:04:06', '2019-10-27 12:04:06'),
(1029, 504, '1572183307_1.jpg', 4, NULL, '2019-10-27 13:35:07', '2019-10-27 13:35:07'),
(1030, 504, '1572183307_2.jpg', 4, NULL, '2019-10-27 13:35:07', '2019-10-27 13:35:07'),
(1031, 504, '1572183307_3.jpg', 4, NULL, '2019-10-27 13:35:07', '2019-10-27 13:35:07'),
(1032, 504, '1572183307_4.jpg', 4, NULL, '2019-10-27 13:35:07', '2019-10-27 13:35:07'),
(1033, 504, '1572183307_5.jpg', 4, NULL, '2019-10-27 13:35:07', '2019-10-27 13:35:07'),
(1034, 504, '1572183307_6.jpg', 4, NULL, '2019-10-27 13:35:07', '2019-10-27 13:35:07'),
(1035, 472, '1575984560_1.jpg', 4, NULL, '2019-12-10 13:29:20', '2019-12-10 13:29:20'),
(1036, 462, '1575984967_1.jpg', 4, NULL, '2019-12-10 13:36:07', '2019-12-10 13:36:07'),
(1037, 462, '1575984967_2.jpg', 4, NULL, '2019-12-10 13:36:07', '2019-12-10 13:36:07'),
(1038, 462, '1575984967_3.jpg', 4, NULL, '2019-12-10 13:36:07', '2019-12-10 13:36:07'),
(1039, 462, '1575984967_4.jpg', 4, NULL, '2019-12-10 13:36:07', '2019-12-10 13:36:07'),
(1040, 462, '1575984967_5.jpg', 4, NULL, '2019-12-10 13:36:07', '2019-12-10 13:36:07');

-- --------------------------------------------------------

--
-- Table structure for table `single_banner_images`
--

CREATE TABLE `single_banner_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `shortDescription` text NOT NULL,
  `category` int(11) UNSIGNED NOT NULL,
  `subcategory` int(11) UNSIGNED NOT NULL,
  `bannerPosition` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `btntext` varchar(255) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `updated_by` int(11) UNSIGNED NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `single_banner_images`
--

INSERT INTO `single_banner_images` (`id`, `name`, `slug`, `shortDescription`, `category`, `subcategory`, `bannerPosition`, `image`, `btntext`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'BEAUTIFUL', 'beautiful', 'Wedding Rings', 36, 35, 'text-center', '1590837259_beautiful.jpg', 'Shop Now', 7, 0, 2020, 2020, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `site_users`
--

CREATE TABLE `site_users` (
  `_id` int(10) UNSIGNED NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `push_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `streetAddress` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `streetAddress1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ipAddress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `site_users`
--

INSERT INTO `site_users` (`_id`, `token`, `push_token`, `username`, `fname`, `lname`, `email`, `phone`, `password`, `country`, `city`, `state`, `postcode`, `streetAddress`, `streetAddress1`, `ipAddress`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '', '', 'test', 'Dev', 'Test', 'nikunj.wxit@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'India', 'Indore', 'MP', '451001', '54', 'Near Apollo', NULL, NULL, NULL, '2020-08-15 03:24:30', '2020-09-20 05:16:25', NULL),
(2, '', '', 'jalvin', '', NULL, 'jalvin.u@gmail.com', NULL, '96e79218965eb72c92a549dd5a330112', 'India', 'Chicago', 'Illinois', '60661', '333, N. Jefferson St.', '', NULL, NULL, NULL, '2020-08-19 06:23:43', '2020-08-19 06:25:44', NULL),
(3, '', '', 'Hariom Tyagi', 'Hariom', 'Tyagi', 'swiftmobile.hariom@gmail.com', NULL, 'd2f8bfa3d33f36a5cd4a5ab535849672', NULL, '', '', '', '', '', NULL, NULL, NULL, '2020-12-24 02:52:09', '2020-12-24 02:54:11', NULL),
(4, '', '', 'Test User', '', NULL, 'test111@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, '', '', '', '', '', NULL, NULL, NULL, '2021-01-02 08:38:46', '2021-01-02 08:38:46', NULL),
(5, '', '', 'Test Dev', '', NULL, 'devtest1@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'India', 'test', 'test', 'tesst', 'test', '', NULL, NULL, NULL, '2021-01-04 00:48:46', '2021-01-04 00:49:51', NULL),
(6, '', '', 'Hariom Tyagi', '', NULL, 'sameer6@yopmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'India', 'Indore', 'Madhya Pradesh', '452010', '181 Bapat indore', '', NULL, NULL, NULL, '2021-01-21 00:32:53', '2021-01-21 00:42:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `site_users_address`
--

CREATE TABLE `site_users_address` (
  `_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `site_users_coupon`
--

CREATE TABLE `site_users_coupon` (
  `id` int(11) UNSIGNED NOT NULL,
  `userId` int(11) UNSIGNED NOT NULL,
  `ord_key` varchar(255) NOT NULL,
  `coupon_name` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `coupon_value` varchar(255) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `updated_by` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `site_users_coupon`
--

INSERT INTO `site_users_coupon` (`id`, `userId`, `ord_key`, `coupon_name`, `coupon_type`, `coupon_value`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 123, '', 'testCoupon1', '', '', 7, 7, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `site_user_experiences`
--

CREATE TABLE `site_user_experiences` (
  `_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `experience_id` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `slider_images`
--

CREATE TABLE `slider_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shortDescription` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slidePosition` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `btntext` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider_images`
--

INSERT INTO `slider_images` (`id`, `title`, `slug`, `shortDescription`, `slidePosition`, `redirect_url`, `image`, `btntext`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(82, 'Grace Designer <span>Jewellery</span>', 'grace-designer-jewellery', 'Rings, Occasion Pieces, Pandora & More.', 'slide-3', 'https://sairampackersandmovers.com/jewellery_ecommerce/public/shop/handmade-golden-necklace', '1589519318_grace-designer-jewellery.jpg', 'Read More', 7, 7, '2020-05-14 08:55:26', '2020-08-15 07:49:43', NULL),
(83, 'Family Jewellery <span>Collection</span>', 'family-jewellery-collection', 'Designer Jewellery Necklaces-Bracelets-Earings', 'slide-1', 'https://sairampackersandmovers.com/jewellery_ecommerce/public/shop/diamond-exclusive-ornament', '1589519368_family-jewellery-collection.jpg', 'Read More', 7, 7, '2020-05-14 10:32:43', '2020-08-15 07:49:54', NULL),
(84, 'Diamonds Jewellery  <span>Collection</span>', 'diamonds-jewellery-collection', 'Shukra Yogam & Silver Power Silver Saving Schemes.', 'slide-2 float-md-right float-none', 'https://sairampackersandmovers.com/jewellery_ecommerce/public/shop/perfect-diamond-jewelry', '1589519407_diamonds-jewellery-collection.jpg', 'Read More', 7, 7, '2020-05-14 10:36:57', '2020-08-15 07:50:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `facebook_url` text,
  `twitter_url` text,
  `linkedin_url` text,
  `google_url` text,
  `created_by` int(11) UNSIGNED NOT NULL,
  `updated_by` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `name`, `slug`, `designation`, `image`, `facebook_url`, `twitter_url`, `linkedin_url`, `google_url`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Jonathan Scott', 'jonathan-scott', 'CEO', '1591457186_jonathan-scott.jpg', '', '', '', '', 7, 7, '2020-06-06 09:56:26', '2020-06-06 11:43:45', NULL),
(2, 'Oliver bastin', 'oliver-bastin', 'Designer', '1591457377_oliver-bastin.jpg', '', '', '', '', 7, 0, '2020-06-06 09:59:37', '2020-06-06 09:59:37', NULL),
(3, 'Erik jonson', 'erik-jonson', 'Developer', '1591457408_erik-jonson.jpg', '', '', '', '', 7, 0, '2020-06-06 10:00:08', '2020-06-06 10:00:08', NULL),
(4, 'Jon doe', 'jon-doe', 'Marketing officer', '1591457439_jon-doe.jpg', '', '', '', '', 7, 0, '2020-06-06 10:00:39', '2020-06-06 10:00:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tenantadmins`
--

CREATE TABLE `tenantadmins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenant_id` int(10) UNSIGNED NOT NULL,
  `shop_name` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tenantadmins`
--

INSERT INTO `tenantadmins` (`id`, `name`, `email`, `password`, `type`, `tenant_id`, `shop_name`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Blue Saloon', 'bluesaloon@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'seller', 7, '[{\"id\":\"405\"},{\"id\":\"443\"},{\"id\":\"448\"}]', '0e21209277d7e635417a7e0c015387df0edcc049e335b360209179bf6ecf0277', '2019-08-08 09:59:01', '2020-01-05 16:50:49', NULL),
(2, 'The Blue Group', 'obieda@tbg.qa', '$2y$10$d61DinNt39gWfkB/EERoduXy53ijEyO0Xk2m54IWB.r5JaD8NfZBW', 'seller', 9, '[{\"id\":\"441\"}]', NULL, '2019-09-11 12:29:10', '2019-09-11 12:29:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tenants`
--

CREATE TABLE `tenants` (
  `id` int(10) UNSIGNED NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_doc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `shop_name` text COLLATE utf8mb4_unicode_ci,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tenant_complaints`
--

CREATE TABLE `tenant_complaints` (
  `id` int(10) UNSIGNED NOT NULL,
  `tenant_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `complaint` text COLLATE utf8mb4_unicode_ci,
  `reply` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tenant_complaints`
--

INSERT INTO `tenant_complaints` (`id`, `tenant_id`, `admin_id`, `complaint`, `reply`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 7, 4, 'tenant complaint description', 'admin reply', 'read', '2019-09-19 06:42:52', '2019-10-05 08:38:26', NULL),
(2, 7, 4, 'tenant complaint description1', 'test data', 'read', '2019-10-05 06:22:22', '2019-11-19 12:29:26', NULL),
(3, 7, 0, 'test details', NULL, 'unread', '2019-11-20 10:48:25', '2019-11-20 10:48:25', NULL),
(4, 7, 0, 'test check', NULL, 'unread', '2019-11-21 08:57:16', '2019-11-21 08:57:16', NULL),
(5, 7, 0, 'message', NULL, 'unread', '2019-11-21 09:00:21', '2019-11-21 09:00:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `shortDescription` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `updated_by` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `name`, `slug`, `shortDescription`, `image`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'lindsy niloms', 'lindsy-niloms', 'Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque', '1590740833_lindsy-niloms.png', 7, 0, '2020-05-29 02:57:13', '2020-06-03 02:52:24', NULL),
(2, 'Daisy Millan', 'daisy-millan', 'Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque', '1590844737_daisy-millan.png', 7, 0, '2020-05-30 07:48:57', '2020-06-03 02:52:35', NULL),
(3, 'Anamika lusy', 'anamika-lusy', 'Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque', '1590844757_anamika-lusy.png', 7, 0, '2020-05-30 07:49:17', '2020-06-03 02:52:38', NULL),
(4, 'Maria Mora', 'maria-mora', 'Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque', '1590844794_maria-mora.png', 7, 0, '2020-05-30 07:49:54', '2020-06-03 02:52:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenant_id` int(10) UNSIGNED NOT NULL,
  `shop_name` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `type`, `tenant_id`, `shop_name`, `remember_token`, `created_at`, `updated_at`) VALUES
(7, 'Glamour Jewellery', 'glamour@gmail.com', '$2y$10$OpKBpvY/CC5nLGz10MpVJePybLHcEUi/sVMLHov7nXYpevzvKKx5O', 'admin', 0, '', 'hpRdgB9WCFGDLQiZ0w34oxTca3xEXBxALTe7W4O6J6NEREZ7LMnQjSsg7RRY', '2019-06-01 06:57:06', '2019-06-01 06:57:06');

-- --------------------------------------------------------

--
-- Table structure for table `users_cart`
--

CREATE TABLE `users_cart` (
  `_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `color` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `quantity` tinyint(4) NOT NULL,
  `discounted_price` decimal(11,2) NOT NULL DEFAULT '0.00',
  `price` decimal(11,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_cart`
--

INSERT INTO `users_cart` (`_id`, `user_id`, `product_id`, `color`, `size`, `quantity`, `discounted_price`, `price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 3, 2, 1, 3, 2, '0.00', '0.00', '2020-12-28 00:04:16', '2020-12-28 00:04:16', NULL),
(4, 4, 2, 1, 3, 1, '0.00', '0.00', '2021-01-02 08:38:57', '2021-01-02 08:38:57', NULL),
(6, 1, 4, 3, 1, 1, '0.00', '0.00', '2021-01-13 23:39:00', '2021-01-13 23:39:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_wishlist`
--

CREATE TABLE `users_wishlist` (
  `_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) UNSIGNED NOT NULL,
  `updated_by` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users_wishlist`
--

INSERT INTO `users_wishlist` (`_id`, `user_id`, `product_id`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(50, 123, 5, 1, 0, 0, '2020-07-18 11:30:41', '2020-07-18 11:30:41', NULL),
(51, 123, 3, 1, 0, 0, '2020-08-02 06:35:43', '2020-08-02 06:35:43', NULL),
(52, 123, 1, 1, 0, 0, '2020-08-02 06:35:47', '2020-08-02 06:35:47', NULL),
(54, 1, 3, 1, 0, 0, '2020-08-15 03:26:43', '2020-08-15 03:26:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_join_event`
--

CREATE TABLE `user_join_event` (
  `_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_join_offer`
--

CREATE TABLE `user_join_offer` (
  `_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `offer_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner_images`
--
ALTER TABLE `banner_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner_slider`
--
ALTER TABLE `banner_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `blog_pics`
--
ALTER TABLE `blog_pics`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `blog_tags`
--
ALTER TABLE `blog_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms`
--
ALTER TABLE `cms`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entry_exit_time`
--
ALTER TABLE `entry_exit_time`
  ADD PRIMARY KEY (`sno`),
  ADD KEY `user_id` (`site_user_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `event_pics`
--
ALTER TABLE `event_pics`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `experiences`
--
ALTER TABLE `experiences`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `favourites`
--
ALTER TABLE `favourites`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `floor_plans`
--
ALTER TABLE `floor_plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_albums`
--
ALTER TABLE `gallery_albums`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `gallery_images`
--
ALTER TABLE `gallery_images`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `incidents`
--
ALTER TABLE `incidents`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `leasing`
--
ALTER TABLE `leasing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leasing_enquiries`
--
ALTER TABLE `leasing_enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lost_found_items`
--
ALTER TABLE `lost_found_items`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `management_email`
--
ALTER TABLE `management_email`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `master_brand`
--
ALTER TABLE `master_brand`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `master_category`
--
ALTER TABLE `master_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `master_store`
--
ALTER TABLE `master_store`
  ADD PRIMARY KEY (`store_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movie_detail`
--
ALTER TABLE `movie_detail`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `privilege_customer_offers`
--
ALTER TABLE `privilege_customer_offers`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `product_brand`
--
ALTER TABLE `product_brand`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `product_color`
--
ALTER TABLE `product_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_pics`
--
ALTER TABLE `product_pics`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `product_size`
--
ALTER TABLE `product_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_subcategories`
--
ALTER TABLE `product_subcategories`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `shop_brands`
--
ALTER TABLE `shop_brands`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `shop_pics`
--
ALTER TABLE `shop_pics`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `single_banner_images`
--
ALTER TABLE `single_banner_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_users`
--
ALTER TABLE `site_users`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `site_users_address`
--
ALTER TABLE `site_users_address`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `site_users_coupon`
--
ALTER TABLE `site_users_coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_user_experiences`
--
ALTER TABLE `site_user_experiences`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `slider_images`
--
ALTER TABLE `slider_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenantadmins`
--
ALTER TABLE `tenantadmins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenants`
--
ALTER TABLE `tenants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tenants_uid_unique` (`uid`);

--
-- Indexes for table `tenant_complaints`
--
ALTER TABLE `tenant_complaints`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_cart`
--
ALTER TABLE `users_cart`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `users_wishlist`
--
ALTER TABLE `users_wishlist`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `user_join_event`
--
ALTER TABLE `user_join_event`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `user_join_offer`
--
ALTER TABLE `user_join_offer`
  ADD PRIMARY KEY (`_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner_images`
--
ALTER TABLE `banner_images`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `banner_slider`
--
ALTER TABLE `banner_slider`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `blog_pics`
--
ALTER TABLE `blog_pics`
  MODIFY `_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `blog_tags`
--
ALTER TABLE `blog_tags`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `cms`
--
ALTER TABLE `cms`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `entry_exit_time`
--
ALTER TABLE `entry_exit_time`
  MODIFY `sno` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_pics`
--
ALTER TABLE `event_pics`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `experiences`
--
ALTER TABLE `experiences`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `favourites`
--
ALTER TABLE `favourites`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `floor_plans`
--
ALTER TABLE `floor_plans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gallery_albums`
--
ALTER TABLE `gallery_albums`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gallery_images`
--
ALTER TABLE `gallery_images`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;
--
-- AUTO_INCREMENT for table `incidents`
--
ALTER TABLE `incidents`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `leasing`
--
ALTER TABLE `leasing`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `leasing_enquiries`
--
ALTER TABLE `leasing_enquiries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lost_found_items`
--
ALTER TABLE `lost_found_items`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `management_email`
--
ALTER TABLE `management_email`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `master_brand`
--
ALTER TABLE `master_brand`
  MODIFY `brand_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `master_category`
--
ALTER TABLE `master_category`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `master_store`
--
ALTER TABLE `master_store`
  MODIFY `store_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `movie_detail`
--
ALTER TABLE `movie_detail`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `privilege_customer_offers`
--
ALTER TABLE `privilege_customer_offers`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `product_brand`
--
ALTER TABLE `product_brand`
  MODIFY `_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `product_color`
--
ALTER TABLE `product_color`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `product_pics`
--
ALTER TABLE `product_pics`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `product_size`
--
ALTER TABLE `product_size`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `product_subcategories`
--
ALTER TABLE `product_subcategories`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_brands`
--
ALTER TABLE `shop_brands`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5655;
--
-- AUTO_INCREMENT for table `shop_pics`
--
ALTER TABLE `shop_pics`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1041;
--
-- AUTO_INCREMENT for table `single_banner_images`
--
ALTER TABLE `single_banner_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `site_users`
--
ALTER TABLE `site_users`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `site_users_address`
--
ALTER TABLE `site_users_address`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `site_users_coupon`
--
ALTER TABLE `site_users_coupon`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `site_user_experiences`
--
ALTER TABLE `site_user_experiences`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `slider_images`
--
ALTER TABLE `slider_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tenantadmins`
--
ALTER TABLE `tenantadmins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tenants`
--
ALTER TABLE `tenants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tenant_complaints`
--
ALTER TABLE `tenant_complaints`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users_cart`
--
ALTER TABLE `users_cart`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users_wishlist`
--
ALTER TABLE `users_wishlist`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `user_join_event`
--
ALTER TABLE `user_join_event`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_join_offer`
--
ALTER TABLE `user_join_offer`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
